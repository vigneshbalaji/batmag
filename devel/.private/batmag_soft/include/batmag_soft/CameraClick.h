// Generated by gencpp from file batmag_soft/CameraClick.msg
// DO NOT EDIT!


#ifndef BATMAG_SOFT_MESSAGE_CAMERACLICK_H
#define BATMAG_SOFT_MESSAGE_CAMERACLICK_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace batmag_soft
{
template <class ContainerAllocator>
struct CameraClick_
{
  typedef CameraClick_<ContainerAllocator> Type;

  CameraClick_()
    : x(0)
    , y(0)
    , button(0)
    , camera(0)  {
    }
  CameraClick_(const ContainerAllocator& _alloc)
    : x(0)
    , y(0)
    , button(0)
    , camera(0)  {
  (void)_alloc;
    }



   typedef int64_t _x_type;
  _x_type x;

   typedef int64_t _y_type;
  _y_type y;

   typedef int64_t _button_type;
  _button_type button;

   typedef int64_t _camera_type;
  _camera_type camera;





  typedef boost::shared_ptr< ::batmag_soft::CameraClick_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::batmag_soft::CameraClick_<ContainerAllocator> const> ConstPtr;

}; // struct CameraClick_

typedef ::batmag_soft::CameraClick_<std::allocator<void> > CameraClick;

typedef boost::shared_ptr< ::batmag_soft::CameraClick > CameraClickPtr;
typedef boost::shared_ptr< ::batmag_soft::CameraClick const> CameraClickConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::batmag_soft::CameraClick_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::batmag_soft::CameraClick_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace batmag_soft

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': True, 'IsMessage': True, 'HasHeader': False}
// {'sensor_msgs': ['/opt/ros/lunar/share/sensor_msgs/cmake/../msg'], 'batmag_soft': ['/home/batmag/batmag_ws/src/batmag_soft/msg'], 'geometry_msgs': ['/opt/ros/lunar/share/geometry_msgs/cmake/../msg'], 'std_msgs': ['/opt/ros/lunar/share/std_msgs/cmake/../msg'], 'actionlib_msgs': ['/opt/ros/lunar/share/actionlib_msgs/cmake/../msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::batmag_soft::CameraClick_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::batmag_soft::CameraClick_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::batmag_soft::CameraClick_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::batmag_soft::CameraClick_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::batmag_soft::CameraClick_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::batmag_soft::CameraClick_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::batmag_soft::CameraClick_<ContainerAllocator> >
{
  static const char* value()
  {
    return "490f77eb6fbf239018653a891a759f0c";
  }

  static const char* value(const ::batmag_soft::CameraClick_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x490f77eb6fbf2390ULL;
  static const uint64_t static_value2 = 0x18653a891a759f0cULL;
};

template<class ContainerAllocator>
struct DataType< ::batmag_soft::CameraClick_<ContainerAllocator> >
{
  static const char* value()
  {
    return "batmag_soft/CameraClick";
  }

  static const char* value(const ::batmag_soft::CameraClick_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::batmag_soft::CameraClick_<ContainerAllocator> >
{
  static const char* value()
  {
    return "int64 x\n\
int64 y\n\
int64 button\n\
int64 camera\n\
";
  }

  static const char* value(const ::batmag_soft::CameraClick_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::batmag_soft::CameraClick_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.x);
      stream.next(m.y);
      stream.next(m.button);
      stream.next(m.camera);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct CameraClick_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::batmag_soft::CameraClick_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::batmag_soft::CameraClick_<ContainerAllocator>& v)
  {
    s << indent << "x: ";
    Printer<int64_t>::stream(s, indent + "  ", v.x);
    s << indent << "y: ";
    Printer<int64_t>::stream(s, indent + "  ", v.y);
    s << indent << "button: ";
    Printer<int64_t>::stream(s, indent + "  ", v.button);
    s << indent << "camera: ";
    Printer<int64_t>::stream(s, indent + "  ", v.camera);
  }
};

} // namespace message_operations
} // namespace ros

#endif // BATMAG_SOFT_MESSAGE_CAMERACLICK_H
