// Generated by gencpp from file batmag_soft/SaveImage.msg
// DO NOT EDIT!


#ifndef BATMAG_SOFT_MESSAGE_SAVEIMAGE_H
#define BATMAG_SOFT_MESSAGE_SAVEIMAGE_H

#include <ros/service_traits.h>


#include <batmag_soft/SaveImageRequest.h>
#include <batmag_soft/SaveImageResponse.h>


namespace batmag_soft
{

struct SaveImage
{

typedef SaveImageRequest Request;
typedef SaveImageResponse Response;
Request request;
Response response;

typedef Request RequestType;
typedef Response ResponseType;

}; // struct SaveImage
} // namespace batmag_soft


namespace ros
{
namespace service_traits
{


template<>
struct MD5Sum< ::batmag_soft::SaveImage > {
  static const char* value()
  {
    return "358e233cde0c8a8bcfea4ce193f8fc15";
  }

  static const char* value(const ::batmag_soft::SaveImage&) { return value(); }
};

template<>
struct DataType< ::batmag_soft::SaveImage > {
  static const char* value()
  {
    return "batmag_soft/SaveImage";
  }

  static const char* value(const ::batmag_soft::SaveImage&) { return value(); }
};


// service_traits::MD5Sum< ::batmag_soft::SaveImageRequest> should match 
// service_traits::MD5Sum< ::batmag_soft::SaveImage > 
template<>
struct MD5Sum< ::batmag_soft::SaveImageRequest>
{
  static const char* value()
  {
    return MD5Sum< ::batmag_soft::SaveImage >::value();
  }
  static const char* value(const ::batmag_soft::SaveImageRequest&)
  {
    return value();
  }
};

// service_traits::DataType< ::batmag_soft::SaveImageRequest> should match 
// service_traits::DataType< ::batmag_soft::SaveImage > 
template<>
struct DataType< ::batmag_soft::SaveImageRequest>
{
  static const char* value()
  {
    return DataType< ::batmag_soft::SaveImage >::value();
  }
  static const char* value(const ::batmag_soft::SaveImageRequest&)
  {
    return value();
  }
};

// service_traits::MD5Sum< ::batmag_soft::SaveImageResponse> should match 
// service_traits::MD5Sum< ::batmag_soft::SaveImage > 
template<>
struct MD5Sum< ::batmag_soft::SaveImageResponse>
{
  static const char* value()
  {
    return MD5Sum< ::batmag_soft::SaveImage >::value();
  }
  static const char* value(const ::batmag_soft::SaveImageResponse&)
  {
    return value();
  }
};

// service_traits::DataType< ::batmag_soft::SaveImageResponse> should match 
// service_traits::DataType< ::batmag_soft::SaveImage > 
template<>
struct DataType< ::batmag_soft::SaveImageResponse>
{
  static const char* value()
  {
    return DataType< ::batmag_soft::SaveImage >::value();
  }
  static const char* value(const ::batmag_soft::SaveImageResponse&)
  {
    return value();
  }
};

} // namespace service_traits
} // namespace ros

#endif // BATMAG_SOFT_MESSAGE_SAVEIMAGE_H
