// Generated by gencpp from file batmag_soft/pmMove.msg
// DO NOT EDIT!


#ifndef BATMAG_SOFT_MESSAGE_PMMOVE_H
#define BATMAG_SOFT_MESSAGE_PMMOVE_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace batmag_soft
{
template <class ContainerAllocator>
struct pmMove_
{
  typedef pmMove_<ContainerAllocator> Type;

  pmMove_()
    : position(0.0)
    , enable(false)  {
    }
  pmMove_(const ContainerAllocator& _alloc)
    : position(0.0)
    , enable(false)  {
  (void)_alloc;
    }



   typedef double _position_type;
  _position_type position;

   typedef uint8_t _enable_type;
  _enable_type enable;





  typedef boost::shared_ptr< ::batmag_soft::pmMove_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::batmag_soft::pmMove_<ContainerAllocator> const> ConstPtr;

}; // struct pmMove_

typedef ::batmag_soft::pmMove_<std::allocator<void> > pmMove;

typedef boost::shared_ptr< ::batmag_soft::pmMove > pmMovePtr;
typedef boost::shared_ptr< ::batmag_soft::pmMove const> pmMoveConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::batmag_soft::pmMove_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::batmag_soft::pmMove_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace batmag_soft

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': True, 'IsMessage': True, 'HasHeader': False}
// {'sensor_msgs': ['/opt/ros/lunar/share/sensor_msgs/cmake/../msg'], 'batmag_soft': ['/home/batmag/batmag_ws/src/batmag_soft/msg'], 'geometry_msgs': ['/opt/ros/lunar/share/geometry_msgs/cmake/../msg'], 'std_msgs': ['/opt/ros/lunar/share/std_msgs/cmake/../msg'], 'actionlib_msgs': ['/opt/ros/lunar/share/actionlib_msgs/cmake/../msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::batmag_soft::pmMove_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::batmag_soft::pmMove_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::batmag_soft::pmMove_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::batmag_soft::pmMove_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::batmag_soft::pmMove_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::batmag_soft::pmMove_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::batmag_soft::pmMove_<ContainerAllocator> >
{
  static const char* value()
  {
    return "53037050e41b7763a8fbce1f9811be05";
  }

  static const char* value(const ::batmag_soft::pmMove_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x53037050e41b7763ULL;
  static const uint64_t static_value2 = 0xa8fbce1f9811be05ULL;
};

template<class ContainerAllocator>
struct DataType< ::batmag_soft::pmMove_<ContainerAllocator> >
{
  static const char* value()
  {
    return "batmag_soft/pmMove";
  }

  static const char* value(const ::batmag_soft::pmMove_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::batmag_soft::pmMove_<ContainerAllocator> >
{
  static const char* value()
  {
    return "float64 position\n\
bool enable\n\
";
  }

  static const char* value(const ::batmag_soft::pmMove_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::batmag_soft::pmMove_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.position);
      stream.next(m.enable);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct pmMove_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::batmag_soft::pmMove_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::batmag_soft::pmMove_<ContainerAllocator>& v)
  {
    s << indent << "position: ";
    Printer<double>::stream(s, indent + "  ", v.position);
    s << indent << "enable: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.enable);
  }
};

} // namespace message_operations
} // namespace ros

#endif // BATMAG_SOFT_MESSAGE_PMMOVE_H
