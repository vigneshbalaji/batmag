// Generated by gencpp from file batmag_soft/RecordRaw.msg
// DO NOT EDIT!


#ifndef BATMAG_SOFT_MESSAGE_RECORDRAW_H
#define BATMAG_SOFT_MESSAGE_RECORDRAW_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace batmag_soft
{
template <class ContainerAllocator>
struct RecordRaw_
{
  typedef RecordRaw_<ContainerAllocator> Type;

  RecordRaw_()
    : recordRaw(false)
    , recordWhere()  {
    }
  RecordRaw_(const ContainerAllocator& _alloc)
    : recordRaw(false)
    , recordWhere(_alloc)  {
  (void)_alloc;
    }



   typedef uint8_t _recordRaw_type;
  _recordRaw_type recordRaw;

   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _recordWhere_type;
  _recordWhere_type recordWhere;





  typedef boost::shared_ptr< ::batmag_soft::RecordRaw_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::batmag_soft::RecordRaw_<ContainerAllocator> const> ConstPtr;

}; // struct RecordRaw_

typedef ::batmag_soft::RecordRaw_<std::allocator<void> > RecordRaw;

typedef boost::shared_ptr< ::batmag_soft::RecordRaw > RecordRawPtr;
typedef boost::shared_ptr< ::batmag_soft::RecordRaw const> RecordRawConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::batmag_soft::RecordRaw_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::batmag_soft::RecordRaw_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace batmag_soft

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': False, 'IsMessage': True, 'HasHeader': False}
// {'sensor_msgs': ['/opt/ros/lunar/share/sensor_msgs/cmake/../msg'], 'batmag_soft': ['/home/batmag/batmag_ws/src/batmag_soft/msg'], 'geometry_msgs': ['/opt/ros/lunar/share/geometry_msgs/cmake/../msg'], 'std_msgs': ['/opt/ros/lunar/share/std_msgs/cmake/../msg'], 'actionlib_msgs': ['/opt/ros/lunar/share/actionlib_msgs/cmake/../msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::batmag_soft::RecordRaw_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::batmag_soft::RecordRaw_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::batmag_soft::RecordRaw_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::batmag_soft::RecordRaw_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::batmag_soft::RecordRaw_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::batmag_soft::RecordRaw_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::batmag_soft::RecordRaw_<ContainerAllocator> >
{
  static const char* value()
  {
    return "dba747aebd3c39de46937fb36819f3c1";
  }

  static const char* value(const ::batmag_soft::RecordRaw_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0xdba747aebd3c39deULL;
  static const uint64_t static_value2 = 0x46937fb36819f3c1ULL;
};

template<class ContainerAllocator>
struct DataType< ::batmag_soft::RecordRaw_<ContainerAllocator> >
{
  static const char* value()
  {
    return "batmag_soft/RecordRaw";
  }

  static const char* value(const ::batmag_soft::RecordRaw_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::batmag_soft::RecordRaw_<ContainerAllocator> >
{
  static const char* value()
  {
    return "bool recordRaw\n\
string recordWhere\n\
";
  }

  static const char* value(const ::batmag_soft::RecordRaw_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::batmag_soft::RecordRaw_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.recordRaw);
      stream.next(m.recordWhere);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct RecordRaw_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::batmag_soft::RecordRaw_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::batmag_soft::RecordRaw_<ContainerAllocator>& v)
  {
    s << indent << "recordRaw: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.recordRaw);
    s << indent << "recordWhere: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.recordWhere);
  }
};

} // namespace message_operations
} // namespace ros

#endif // BATMAG_SOFT_MESSAGE_RECORDRAW_H
