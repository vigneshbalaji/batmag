// Generated by gencpp from file batmag_soft/GUIpreferences.msg
// DO NOT EDIT!


#ifndef BATMAG_SOFT_MESSAGE_GUIPREFERENCES_H
#define BATMAG_SOFT_MESSAGE_GUIPREFERENCES_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace batmag_soft
{
template <class ContainerAllocator>
struct GUIpreferences_
{
  typedef GUIpreferences_<ContainerAllocator> Type;

  GUIpreferences_()
    : recordOn(false)
    , ctrlEn(false)
    , debugEn(false)
    , gravityEn(false)
    , gridOn(false)
    , PMposition(0.0)
    , PMenable(false)
    , omegaCtrl(false)  {
    }
  GUIpreferences_(const ContainerAllocator& _alloc)
    : recordOn(false)
    , ctrlEn(false)
    , debugEn(false)
    , gravityEn(false)
    , gridOn(false)
    , PMposition(0.0)
    , PMenable(false)
    , omegaCtrl(false)  {
  (void)_alloc;
    }



   typedef uint8_t _recordOn_type;
  _recordOn_type recordOn;

   typedef uint8_t _ctrlEn_type;
  _ctrlEn_type ctrlEn;

   typedef uint8_t _debugEn_type;
  _debugEn_type debugEn;

   typedef uint8_t _gravityEn_type;
  _gravityEn_type gravityEn;

   typedef uint8_t _gridOn_type;
  _gridOn_type gridOn;

   typedef double _PMposition_type;
  _PMposition_type PMposition;

   typedef uint8_t _PMenable_type;
  _PMenable_type PMenable;

   typedef uint8_t _omegaCtrl_type;
  _omegaCtrl_type omegaCtrl;





  typedef boost::shared_ptr< ::batmag_soft::GUIpreferences_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::batmag_soft::GUIpreferences_<ContainerAllocator> const> ConstPtr;

}; // struct GUIpreferences_

typedef ::batmag_soft::GUIpreferences_<std::allocator<void> > GUIpreferences;

typedef boost::shared_ptr< ::batmag_soft::GUIpreferences > GUIpreferencesPtr;
typedef boost::shared_ptr< ::batmag_soft::GUIpreferences const> GUIpreferencesConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::batmag_soft::GUIpreferences_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::batmag_soft::GUIpreferences_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace batmag_soft

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': True, 'IsMessage': True, 'HasHeader': False}
// {'sensor_msgs': ['/opt/ros/lunar/share/sensor_msgs/cmake/../msg'], 'batmag_soft': ['/home/batmag/batmag_ws/src/batmag_soft/msg'], 'geometry_msgs': ['/opt/ros/lunar/share/geometry_msgs/cmake/../msg'], 'std_msgs': ['/opt/ros/lunar/share/std_msgs/cmake/../msg'], 'actionlib_msgs': ['/opt/ros/lunar/share/actionlib_msgs/cmake/../msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::batmag_soft::GUIpreferences_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::batmag_soft::GUIpreferences_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::batmag_soft::GUIpreferences_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::batmag_soft::GUIpreferences_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::batmag_soft::GUIpreferences_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::batmag_soft::GUIpreferences_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::batmag_soft::GUIpreferences_<ContainerAllocator> >
{
  static const char* value()
  {
    return "30cc44c0758499a2509ba85095012fbb";
  }

  static const char* value(const ::batmag_soft::GUIpreferences_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x30cc44c0758499a2ULL;
  static const uint64_t static_value2 = 0x509ba85095012fbbULL;
};

template<class ContainerAllocator>
struct DataType< ::batmag_soft::GUIpreferences_<ContainerAllocator> >
{
  static const char* value()
  {
    return "batmag_soft/GUIpreferences";
  }

  static const char* value(const ::batmag_soft::GUIpreferences_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::batmag_soft::GUIpreferences_<ContainerAllocator> >
{
  static const char* value()
  {
    return "bool recordOn\n\
bool ctrlEn\n\
bool debugEn\n\
bool gravityEn\n\
bool gridOn\n\
float64 PMposition\n\
bool PMenable\n\
bool omegaCtrl\n\
";
  }

  static const char* value(const ::batmag_soft::GUIpreferences_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::batmag_soft::GUIpreferences_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.recordOn);
      stream.next(m.ctrlEn);
      stream.next(m.debugEn);
      stream.next(m.gravityEn);
      stream.next(m.gridOn);
      stream.next(m.PMposition);
      stream.next(m.PMenable);
      stream.next(m.omegaCtrl);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct GUIpreferences_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::batmag_soft::GUIpreferences_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::batmag_soft::GUIpreferences_<ContainerAllocator>& v)
  {
    s << indent << "recordOn: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.recordOn);
    s << indent << "ctrlEn: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.ctrlEn);
    s << indent << "debugEn: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.debugEn);
    s << indent << "gravityEn: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.gravityEn);
    s << indent << "gridOn: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.gridOn);
    s << indent << "PMposition: ";
    Printer<double>::stream(s, indent + "  ", v.PMposition);
    s << indent << "PMenable: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.PMenable);
    s << indent << "omegaCtrl: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.omegaCtrl);
  }
};

} // namespace message_operations
} // namespace ros

#endif // BATMAG_SOFT_MESSAGE_GUIPREFERENCES_H
