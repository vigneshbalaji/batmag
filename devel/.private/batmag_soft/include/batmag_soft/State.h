// Generated by gencpp from file batmag_soft/State.msg
// DO NOT EDIT!


#ifndef BATMAG_SOFT_MESSAGE_STATE_H
#define BATMAG_SOFT_MESSAGE_STATE_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace batmag_soft
{
template <class ContainerAllocator>
struct State_
{
  typedef State_<ContainerAllocator> Type;

  State_()
    : state()  {
      state.assign(0.0);
  }
  State_(const ContainerAllocator& _alloc)
    : state()  {
  (void)_alloc;
      state.assign(0.0);
  }



   typedef boost::array<double, 6>  _state_type;
  _state_type state;





  typedef boost::shared_ptr< ::batmag_soft::State_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::batmag_soft::State_<ContainerAllocator> const> ConstPtr;

}; // struct State_

typedef ::batmag_soft::State_<std::allocator<void> > State;

typedef boost::shared_ptr< ::batmag_soft::State > StatePtr;
typedef boost::shared_ptr< ::batmag_soft::State const> StateConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::batmag_soft::State_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::batmag_soft::State_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace batmag_soft

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': True, 'IsMessage': True, 'HasHeader': False}
// {'sensor_msgs': ['/opt/ros/lunar/share/sensor_msgs/cmake/../msg'], 'batmag_soft': ['/home/batmag/batmag_ws/src/batmag_soft/msg'], 'geometry_msgs': ['/opt/ros/lunar/share/geometry_msgs/cmake/../msg'], 'std_msgs': ['/opt/ros/lunar/share/std_msgs/cmake/../msg'], 'actionlib_msgs': ['/opt/ros/lunar/share/actionlib_msgs/cmake/../msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::batmag_soft::State_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::batmag_soft::State_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::batmag_soft::State_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::batmag_soft::State_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::batmag_soft::State_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::batmag_soft::State_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::batmag_soft::State_<ContainerAllocator> >
{
  static const char* value()
  {
    return "8ce282c4e012701c85e6ebf3255dfc96";
  }

  static const char* value(const ::batmag_soft::State_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x8ce282c4e012701cULL;
  static const uint64_t static_value2 = 0x85e6ebf3255dfc96ULL;
};

template<class ContainerAllocator>
struct DataType< ::batmag_soft::State_<ContainerAllocator> >
{
  static const char* value()
  {
    return "batmag_soft/State";
  }

  static const char* value(const ::batmag_soft::State_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::batmag_soft::State_<ContainerAllocator> >
{
  static const char* value()
  {
    return "float64[6] state\n\
";
  }

  static const char* value(const ::batmag_soft::State_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::batmag_soft::State_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.state);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct State_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::batmag_soft::State_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::batmag_soft::State_<ContainerAllocator>& v)
  {
    s << indent << "state[]" << std::endl;
    for (size_t i = 0; i < v.state.size(); ++i)
    {
      s << indent << "  state[" << i << "]: ";
      Printer<double>::stream(s, indent + "  ", v.state[i]);
    }
  }
};

} // namespace message_operations
} // namespace ros

#endif // BATMAG_SOFT_MESSAGE_STATE_H
