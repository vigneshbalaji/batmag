// Generated by gencpp from file batmag_soft/GetInitParametersResponse.msg
// DO NOT EDIT!


#ifndef BATMAG_SOFT_MESSAGE_GETINITPARAMETERSRESPONSE_H
#define BATMAG_SOFT_MESSAGE_GETINITPARAMETERSRESPONSE_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace batmag_soft
{
template <class ContainerAllocator>
struct GetInitParametersResponse_
{
  typedef GetInitParametersResponse_<ContainerAllocator> Type;

  GetInitParametersResponse_()
    : success(false)
    , value()
    , min()
    , max()
    , names()  {
      value.assign(0.0);

      min.assign(0.0);

      max.assign(0.0);
  }
  GetInitParametersResponse_(const ContainerAllocator& _alloc)
    : success(false)
    , value()
    , min()
    , max()
    , names()  {
      value.assign(0.0);

      min.assign(0.0);

      max.assign(0.0);

      names.assign(std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > (_alloc));
  }



   typedef uint8_t _success_type;
  _success_type success;

   typedef boost::array<double, 9>  _value_type;
  _value_type value;

   typedef boost::array<double, 9>  _min_type;
  _min_type min;

   typedef boost::array<double, 9>  _max_type;
  _max_type max;

   typedef boost::array<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > , 9>  _names_type;
  _names_type names;





  typedef boost::shared_ptr< ::batmag_soft::GetInitParametersResponse_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::batmag_soft::GetInitParametersResponse_<ContainerAllocator> const> ConstPtr;

}; // struct GetInitParametersResponse_

typedef ::batmag_soft::GetInitParametersResponse_<std::allocator<void> > GetInitParametersResponse;

typedef boost::shared_ptr< ::batmag_soft::GetInitParametersResponse > GetInitParametersResponsePtr;
typedef boost::shared_ptr< ::batmag_soft::GetInitParametersResponse const> GetInitParametersResponseConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::batmag_soft::GetInitParametersResponse_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::batmag_soft::GetInitParametersResponse_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace batmag_soft

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': False, 'IsMessage': True, 'HasHeader': False}
// {'sensor_msgs': ['/opt/ros/lunar/share/sensor_msgs/cmake/../msg'], 'batmag_soft': ['/home/batmag/batmag_ws/src/batmag_soft/msg'], 'geometry_msgs': ['/opt/ros/lunar/share/geometry_msgs/cmake/../msg'], 'std_msgs': ['/opt/ros/lunar/share/std_msgs/cmake/../msg'], 'actionlib_msgs': ['/opt/ros/lunar/share/actionlib_msgs/cmake/../msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::batmag_soft::GetInitParametersResponse_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::batmag_soft::GetInitParametersResponse_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::batmag_soft::GetInitParametersResponse_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::batmag_soft::GetInitParametersResponse_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::batmag_soft::GetInitParametersResponse_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::batmag_soft::GetInitParametersResponse_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::batmag_soft::GetInitParametersResponse_<ContainerAllocator> >
{
  static const char* value()
  {
    return "3319200937126dd91c4e01910b156533";
  }

  static const char* value(const ::batmag_soft::GetInitParametersResponse_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x3319200937126dd9ULL;
  static const uint64_t static_value2 = 0x1c4e01910b156533ULL;
};

template<class ContainerAllocator>
struct DataType< ::batmag_soft::GetInitParametersResponse_<ContainerAllocator> >
{
  static const char* value()
  {
    return "batmag_soft/GetInitParametersResponse";
  }

  static const char* value(const ::batmag_soft::GetInitParametersResponse_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::batmag_soft::GetInitParametersResponse_<ContainerAllocator> >
{
  static const char* value()
  {
    return "bool success\n\
float64[9] value\n\
float64[9] min\n\
float64[9] max\n\
string[9] names\n\
\n\
\n\
";
  }

  static const char* value(const ::batmag_soft::GetInitParametersResponse_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::batmag_soft::GetInitParametersResponse_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.success);
      stream.next(m.value);
      stream.next(m.min);
      stream.next(m.max);
      stream.next(m.names);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct GetInitParametersResponse_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::batmag_soft::GetInitParametersResponse_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::batmag_soft::GetInitParametersResponse_<ContainerAllocator>& v)
  {
    s << indent << "success: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.success);
    s << indent << "value[]" << std::endl;
    for (size_t i = 0; i < v.value.size(); ++i)
    {
      s << indent << "  value[" << i << "]: ";
      Printer<double>::stream(s, indent + "  ", v.value[i]);
    }
    s << indent << "min[]" << std::endl;
    for (size_t i = 0; i < v.min.size(); ++i)
    {
      s << indent << "  min[" << i << "]: ";
      Printer<double>::stream(s, indent + "  ", v.min[i]);
    }
    s << indent << "max[]" << std::endl;
    for (size_t i = 0; i < v.max.size(); ++i)
    {
      s << indent << "  max[" << i << "]: ";
      Printer<double>::stream(s, indent + "  ", v.max[i]);
    }
    s << indent << "names[]" << std::endl;
    for (size_t i = 0; i < v.names.size(); ++i)
    {
      s << indent << "  names[" << i << "]: ";
      Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.names[i]);
    }
  }
};

} // namespace message_operations
} // namespace ros

#endif // BATMAG_SOFT_MESSAGE_GETINITPARAMETERSRESPONSE_H
