;; Auto-generated. Do not edit!


(when (boundp 'batmag_soft::SetInitParameters)
  (if (not (find-package "BATMAG_SOFT"))
    (make-package "BATMAG_SOFT"))
  (shadow 'SetInitParameters (find-package "BATMAG_SOFT")))
(unless (find-package "BATMAG_SOFT::SETINITPARAMETERS")
  (make-package "BATMAG_SOFT::SETINITPARAMETERS"))
(unless (find-package "BATMAG_SOFT::SETINITPARAMETERSREQUEST")
  (make-package "BATMAG_SOFT::SETINITPARAMETERSREQUEST"))
(unless (find-package "BATMAG_SOFT::SETINITPARAMETERSRESPONSE")
  (make-package "BATMAG_SOFT::SETINITPARAMETERSRESPONSE"))

(in-package "ROS")





(defclass batmag_soft::SetInitParametersRequest
  :super ros::object
  :slots (_value _filled ))

(defmethod batmag_soft::SetInitParametersRequest
  (:init
   (&key
    ((:value __value) (make-array 9 :initial-element 0.0 :element-type :float))
    ((:filled __filled) 0)
    )
   (send-super :init)
   (setq _value __value)
   (setq _filled (round __filled))
   self)
  (:value
   (&optional __value)
   (if __value (setq _value __value)) _value)
  (:filled
   (&optional __filled)
   (if __filled (setq _filled __filled)) _filled)
  (:serialization-length
   ()
   (+
    ;; float64[9] _value
    (* 8    9)
    ;; int32 _filled
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64[9] _value
     (dotimes (i 9)
       (sys::poke (elt _value i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; int32 _filled
       (write-long _filled s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64[9] _value
   (dotimes (i (length _value))
     (setf (elt _value i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     )
   ;; int32 _filled
     (setq _filled (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;;
   self)
  )

(defclass batmag_soft::SetInitParametersResponse
  :super ros::object
  :slots (_done ))

(defmethod batmag_soft::SetInitParametersResponse
  (:init
   (&key
    ((:done __done) nil)
    )
   (send-super :init)
   (setq _done __done)
   self)
  (:done
   (&optional __done)
   (if __done (setq _done __done)) _done)
  (:serialization-length
   ()
   (+
    ;; bool _done
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _done
       (if _done (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _done
     (setq _done (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(defclass batmag_soft::SetInitParameters
  :super ros::object
  :slots ())

(setf (get batmag_soft::SetInitParameters :md5sum-) "90323ad5a7dc29f5397a78bc8cec0d0d")
(setf (get batmag_soft::SetInitParameters :datatype-) "batmag_soft/SetInitParameters")
(setf (get batmag_soft::SetInitParameters :request) batmag_soft::SetInitParametersRequest)
(setf (get batmag_soft::SetInitParameters :response) batmag_soft::SetInitParametersResponse)

(defmethod batmag_soft::SetInitParametersRequest
  (:response () (instance batmag_soft::SetInitParametersResponse :init)))

(setf (get batmag_soft::SetInitParametersRequest :md5sum-) "90323ad5a7dc29f5397a78bc8cec0d0d")
(setf (get batmag_soft::SetInitParametersRequest :datatype-) "batmag_soft/SetInitParametersRequest")
(setf (get batmag_soft::SetInitParametersRequest :definition-)
      "float64[9] value
int32 filled
---
bool done


")

(setf (get batmag_soft::SetInitParametersResponse :md5sum-) "90323ad5a7dc29f5397a78bc8cec0d0d")
(setf (get batmag_soft::SetInitParametersResponse :datatype-) "batmag_soft/SetInitParametersResponse")
(setf (get batmag_soft::SetInitParametersResponse :definition-)
      "float64[9] value
int32 filled
---
bool done


")



(provide :batmag_soft/SetInitParameters "90323ad5a7dc29f5397a78bc8cec0d0d")


