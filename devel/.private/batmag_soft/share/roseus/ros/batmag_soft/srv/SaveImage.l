;; Auto-generated. Do not edit!


(when (boundp 'batmag_soft::SaveImage)
  (if (not (find-package "BATMAG_SOFT"))
    (make-package "BATMAG_SOFT"))
  (shadow 'SaveImage (find-package "BATMAG_SOFT")))
(unless (find-package "BATMAG_SOFT::SAVEIMAGE")
  (make-package "BATMAG_SOFT::SAVEIMAGE"))
(unless (find-package "BATMAG_SOFT::SAVEIMAGEREQUEST")
  (make-package "BATMAG_SOFT::SAVEIMAGEREQUEST"))
(unless (find-package "BATMAG_SOFT::SAVEIMAGERESPONSE")
  (make-package "BATMAG_SOFT::SAVEIMAGERESPONSE"))

(in-package "ROS")





(defclass batmag_soft::SaveImageRequest
  :super ros::object
  :slots ())

(defmethod batmag_soft::SaveImageRequest
  (:init
   (&key
    )
   (send-super :init)
   self)
  (:serialization-length
   ()
   (+
    0
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;;
   self)
  )

(defclass batmag_soft::SaveImageResponse
  :super ros::object
  :slots (_success ))

(defmethod batmag_soft::SaveImageResponse
  (:init
   (&key
    ((:success __success) nil)
    )
   (send-super :init)
   (setq _success __success)
   self)
  (:success
   (&optional __success)
   (if __success (setq _success __success)) _success)
  (:serialization-length
   ()
   (+
    ;; bool _success
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _success
       (if _success (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _success
     (setq _success (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(defclass batmag_soft::SaveImage
  :super ros::object
  :slots ())

(setf (get batmag_soft::SaveImage :md5sum-) "358e233cde0c8a8bcfea4ce193f8fc15")
(setf (get batmag_soft::SaveImage :datatype-) "batmag_soft/SaveImage")
(setf (get batmag_soft::SaveImage :request) batmag_soft::SaveImageRequest)
(setf (get batmag_soft::SaveImage :response) batmag_soft::SaveImageResponse)

(defmethod batmag_soft::SaveImageRequest
  (:response () (instance batmag_soft::SaveImageResponse :init)))

(setf (get batmag_soft::SaveImageRequest :md5sum-) "358e233cde0c8a8bcfea4ce193f8fc15")
(setf (get batmag_soft::SaveImageRequest :datatype-) "batmag_soft/SaveImageRequest")
(setf (get batmag_soft::SaveImageRequest :definition-)
      "
---
bool success


")

(setf (get batmag_soft::SaveImageResponse :md5sum-) "358e233cde0c8a8bcfea4ce193f8fc15")
(setf (get batmag_soft::SaveImageResponse :datatype-) "batmag_soft/SaveImageResponse")
(setf (get batmag_soft::SaveImageResponse :definition-)
      "
---
bool success


")



(provide :batmag_soft/SaveImage "358e233cde0c8a8bcfea4ce193f8fc15")


