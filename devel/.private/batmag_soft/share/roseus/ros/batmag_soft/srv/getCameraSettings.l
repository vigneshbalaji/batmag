;; Auto-generated. Do not edit!


(when (boundp 'batmag_soft::getCameraSettings)
  (if (not (find-package "BATMAG_SOFT"))
    (make-package "BATMAG_SOFT"))
  (shadow 'getCameraSettings (find-package "BATMAG_SOFT")))
(unless (find-package "BATMAG_SOFT::GETCAMERASETTINGS")
  (make-package "BATMAG_SOFT::GETCAMERASETTINGS"))
(unless (find-package "BATMAG_SOFT::GETCAMERASETTINGSREQUEST")
  (make-package "BATMAG_SOFT::GETCAMERASETTINGSREQUEST"))
(unless (find-package "BATMAG_SOFT::GETCAMERASETTINGSRESPONSE")
  (make-package "BATMAG_SOFT::GETCAMERASETTINGSRESPONSE"))

(in-package "ROS")





(defclass batmag_soft::getCameraSettingsRequest
  :super ros::object
  :slots ())

(defmethod batmag_soft::getCameraSettingsRequest
  (:init
   (&key
    )
   (send-super :init)
   self)
  (:serialization-length
   ()
   (+
    0
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;;
   self)
  )

(defclass batmag_soft::getCameraSettingsResponse
  :super ros::object
  :slots (_Exposure _Brightness _Framerate _Gain _Gamma _Hue _Saturation _Sharpness _Shutter _wbb _wbr ))

(defmethod batmag_soft::getCameraSettingsResponse
  (:init
   (&key
    ((:Exposure __Exposure) 0.0)
    ((:Brightness __Brightness) 0.0)
    ((:Framerate __Framerate) 0.0)
    ((:Gain __Gain) 0.0)
    ((:Gamma __Gamma) 0.0)
    ((:Hue __Hue) 0.0)
    ((:Saturation __Saturation) 0.0)
    ((:Sharpness __Sharpness) 0.0)
    ((:Shutter __Shutter) 0.0)
    ((:wbb __wbb) 0.0)
    ((:wbr __wbr) 0.0)
    )
   (send-super :init)
   (setq _Exposure (float __Exposure))
   (setq _Brightness (float __Brightness))
   (setq _Framerate (float __Framerate))
   (setq _Gain (float __Gain))
   (setq _Gamma (float __Gamma))
   (setq _Hue (float __Hue))
   (setq _Saturation (float __Saturation))
   (setq _Sharpness (float __Sharpness))
   (setq _Shutter (float __Shutter))
   (setq _wbb (float __wbb))
   (setq _wbr (float __wbr))
   self)
  (:Exposure
   (&optional __Exposure)
   (if __Exposure (setq _Exposure __Exposure)) _Exposure)
  (:Brightness
   (&optional __Brightness)
   (if __Brightness (setq _Brightness __Brightness)) _Brightness)
  (:Framerate
   (&optional __Framerate)
   (if __Framerate (setq _Framerate __Framerate)) _Framerate)
  (:Gain
   (&optional __Gain)
   (if __Gain (setq _Gain __Gain)) _Gain)
  (:Gamma
   (&optional __Gamma)
   (if __Gamma (setq _Gamma __Gamma)) _Gamma)
  (:Hue
   (&optional __Hue)
   (if __Hue (setq _Hue __Hue)) _Hue)
  (:Saturation
   (&optional __Saturation)
   (if __Saturation (setq _Saturation __Saturation)) _Saturation)
  (:Sharpness
   (&optional __Sharpness)
   (if __Sharpness (setq _Sharpness __Sharpness)) _Sharpness)
  (:Shutter
   (&optional __Shutter)
   (if __Shutter (setq _Shutter __Shutter)) _Shutter)
  (:wbb
   (&optional __wbb)
   (if __wbb (setq _wbb __wbb)) _wbb)
  (:wbr
   (&optional __wbr)
   (if __wbr (setq _wbr __wbr)) _wbr)
  (:serialization-length
   ()
   (+
    ;; float32 _Exposure
    4
    ;; float32 _Brightness
    4
    ;; float32 _Framerate
    4
    ;; float32 _Gain
    4
    ;; float32 _Gamma
    4
    ;; float32 _Hue
    4
    ;; float32 _Saturation
    4
    ;; float32 _Sharpness
    4
    ;; float32 _Shutter
    4
    ;; float32 _wbb
    4
    ;; float32 _wbr
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32 _Exposure
       (sys::poke _Exposure (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _Brightness
       (sys::poke _Brightness (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _Framerate
       (sys::poke _Framerate (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _Gain
       (sys::poke _Gain (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _Gamma
       (sys::poke _Gamma (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _Hue
       (sys::poke _Hue (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _Saturation
       (sys::poke _Saturation (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _Sharpness
       (sys::poke _Sharpness (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _Shutter
       (sys::poke _Shutter (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _wbb
       (sys::poke _wbb (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _wbr
       (sys::poke _wbr (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32 _Exposure
     (setq _Exposure (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _Brightness
     (setq _Brightness (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _Framerate
     (setq _Framerate (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _Gain
     (setq _Gain (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _Gamma
     (setq _Gamma (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _Hue
     (setq _Hue (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _Saturation
     (setq _Saturation (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _Sharpness
     (setq _Sharpness (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _Shutter
     (setq _Shutter (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _wbb
     (setq _wbb (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _wbr
     (setq _wbr (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(defclass batmag_soft::getCameraSettings
  :super ros::object
  :slots ())

(setf (get batmag_soft::getCameraSettings :md5sum-) "8fe8cda4f5f6a5f7062c243d4b3b5fb9")
(setf (get batmag_soft::getCameraSettings :datatype-) "batmag_soft/getCameraSettings")
(setf (get batmag_soft::getCameraSettings :request) batmag_soft::getCameraSettingsRequest)
(setf (get batmag_soft::getCameraSettings :response) batmag_soft::getCameraSettingsResponse)

(defmethod batmag_soft::getCameraSettingsRequest
  (:response () (instance batmag_soft::getCameraSettingsResponse :init)))

(setf (get batmag_soft::getCameraSettingsRequest :md5sum-) "8fe8cda4f5f6a5f7062c243d4b3b5fb9")
(setf (get batmag_soft::getCameraSettingsRequest :datatype-) "batmag_soft/getCameraSettingsRequest")
(setf (get batmag_soft::getCameraSettingsRequest :definition-)
      "
---
float32 Exposure
float32 Brightness
float32 Framerate
float32 Gain
float32 Gamma
float32 Hue
float32 Saturation
float32 Sharpness
float32 Shutter
float32 wbb
float32 wbr

")

(setf (get batmag_soft::getCameraSettingsResponse :md5sum-) "8fe8cda4f5f6a5f7062c243d4b3b5fb9")
(setf (get batmag_soft::getCameraSettingsResponse :datatype-) "batmag_soft/getCameraSettingsResponse")
(setf (get batmag_soft::getCameraSettingsResponse :definition-)
      "
---
float32 Exposure
float32 Brightness
float32 Framerate
float32 Gain
float32 Gamma
float32 Hue
float32 Saturation
float32 Sharpness
float32 Shutter
float32 wbb
float32 wbr

")



(provide :batmag_soft/getCameraSettings "8fe8cda4f5f6a5f7062c243d4b3b5fb9")


