;; Auto-generated. Do not edit!


(when (boundp 'batmag_soft::ForceCurrentSettings)
  (if (not (find-package "BATMAG_SOFT"))
    (make-package "BATMAG_SOFT"))
  (shadow 'ForceCurrentSettings (find-package "BATMAG_SOFT")))
(unless (find-package "BATMAG_SOFT::FORCECURRENTSETTINGS")
  (make-package "BATMAG_SOFT::FORCECURRENTSETTINGS"))
(unless (find-package "BATMAG_SOFT::FORCECURRENTSETTINGSREQUEST")
  (make-package "BATMAG_SOFT::FORCECURRENTSETTINGSREQUEST"))
(unless (find-package "BATMAG_SOFT::FORCECURRENTSETTINGSRESPONSE")
  (make-package "BATMAG_SOFT::FORCECURRENTSETTINGSRESPONSE"))

(in-package "ROS")





(defclass batmag_soft::ForceCurrentSettingsRequest
  :super ros::object
  :slots (_isSetRequest _mur _radius _br ))

(defmethod batmag_soft::ForceCurrentSettingsRequest
  (:init
   (&key
    ((:isSetRequest __isSetRequest) nil)
    ((:mur __mur) (make-array 0 :initial-element 0.0 :element-type :float))
    ((:radius __radius) (make-array 0 :initial-element 0.0 :element-type :float))
    ((:br __br) (make-array 0 :initial-element 0.0 :element-type :float))
    )
   (send-super :init)
   (setq _isSetRequest __isSetRequest)
   (setq _mur __mur)
   (setq _radius __radius)
   (setq _br __br)
   self)
  (:isSetRequest
   (&optional __isSetRequest)
   (if __isSetRequest (setq _isSetRequest __isSetRequest)) _isSetRequest)
  (:mur
   (&optional __mur)
   (if __mur (setq _mur __mur)) _mur)
  (:radius
   (&optional __radius)
   (if __radius (setq _radius __radius)) _radius)
  (:br
   (&optional __br)
   (if __br (setq _br __br)) _br)
  (:serialization-length
   ()
   (+
    ;; bool _isSetRequest
    1
    ;; float64[] _mur
    (* 8    (length _mur)) 4
    ;; float64[] _radius
    (* 8    (length _radius)) 4
    ;; float64[] _br
    (* 8    (length _br)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _isSetRequest
       (if _isSetRequest (write-byte -1 s) (write-byte 0 s))
     ;; float64[] _mur
     (write-long (length _mur) s)
     (dotimes (i (length _mur))
       (sys::poke (elt _mur i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64[] _radius
     (write-long (length _radius) s)
     (dotimes (i (length _radius))
       (sys::poke (elt _radius i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64[] _br
     (write-long (length _br) s)
     (dotimes (i (length _br))
       (sys::poke (elt _br i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _isSetRequest
     (setq _isSetRequest (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; float64[] _mur
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _mur (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _mur i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;; float64[] _radius
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _radius (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _radius i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;; float64[] _br
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _br (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _br i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;;
   self)
  )

(defclass batmag_soft::ForceCurrentSettingsResponse
  :super ros::object
  :slots (_mur _radius _br ))

(defmethod batmag_soft::ForceCurrentSettingsResponse
  (:init
   (&key
    ((:mur __mur) (make-array 0 :initial-element 0.0 :element-type :float))
    ((:radius __radius) (make-array 0 :initial-element 0.0 :element-type :float))
    ((:br __br) (make-array 0 :initial-element 0.0 :element-type :float))
    )
   (send-super :init)
   (setq _mur __mur)
   (setq _radius __radius)
   (setq _br __br)
   self)
  (:mur
   (&optional __mur)
   (if __mur (setq _mur __mur)) _mur)
  (:radius
   (&optional __radius)
   (if __radius (setq _radius __radius)) _radius)
  (:br
   (&optional __br)
   (if __br (setq _br __br)) _br)
  (:serialization-length
   ()
   (+
    ;; float64[] _mur
    (* 8    (length _mur)) 4
    ;; float64[] _radius
    (* 8    (length _radius)) 4
    ;; float64[] _br
    (* 8    (length _br)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64[] _mur
     (write-long (length _mur) s)
     (dotimes (i (length _mur))
       (sys::poke (elt _mur i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64[] _radius
     (write-long (length _radius) s)
     (dotimes (i (length _radius))
       (sys::poke (elt _radius i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64[] _br
     (write-long (length _br) s)
     (dotimes (i (length _br))
       (sys::poke (elt _br i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64[] _mur
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _mur (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _mur i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;; float64[] _radius
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _radius (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _radius i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;; float64[] _br
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _br (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _br i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;;
   self)
  )

(defclass batmag_soft::ForceCurrentSettings
  :super ros::object
  :slots ())

(setf (get batmag_soft::ForceCurrentSettings :md5sum-) "79cfb3942928693d62410e06660df8ac")
(setf (get batmag_soft::ForceCurrentSettings :datatype-) "batmag_soft/ForceCurrentSettings")
(setf (get batmag_soft::ForceCurrentSettings :request) batmag_soft::ForceCurrentSettingsRequest)
(setf (get batmag_soft::ForceCurrentSettings :response) batmag_soft::ForceCurrentSettingsResponse)

(defmethod batmag_soft::ForceCurrentSettingsRequest
  (:response () (instance batmag_soft::ForceCurrentSettingsResponse :init)))

(setf (get batmag_soft::ForceCurrentSettingsRequest :md5sum-) "79cfb3942928693d62410e06660df8ac")
(setf (get batmag_soft::ForceCurrentSettingsRequest :datatype-) "batmag_soft/ForceCurrentSettingsRequest")
(setf (get batmag_soft::ForceCurrentSettingsRequest :definition-)
      "bool isSetRequest

float64[] mur
float64[] radius
float64[] br
---
float64[] mur
float64[] radius
float64[] br

")

(setf (get batmag_soft::ForceCurrentSettingsResponse :md5sum-) "79cfb3942928693d62410e06660df8ac")
(setf (get batmag_soft::ForceCurrentSettingsResponse :datatype-) "batmag_soft/ForceCurrentSettingsResponse")
(setf (get batmag_soft::ForceCurrentSettingsResponse :definition-)
      "bool isSetRequest

float64[] mur
float64[] radius
float64[] br
---
float64[] mur
float64[] radius
float64[] br

")



(provide :batmag_soft/ForceCurrentSettings "79cfb3942928693d62410e06660df8ac")


