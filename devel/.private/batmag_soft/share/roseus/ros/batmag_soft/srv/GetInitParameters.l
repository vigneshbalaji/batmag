;; Auto-generated. Do not edit!


(when (boundp 'batmag_soft::GetInitParameters)
  (if (not (find-package "BATMAG_SOFT"))
    (make-package "BATMAG_SOFT"))
  (shadow 'GetInitParameters (find-package "BATMAG_SOFT")))
(unless (find-package "BATMAG_SOFT::GETINITPARAMETERS")
  (make-package "BATMAG_SOFT::GETINITPARAMETERS"))
(unless (find-package "BATMAG_SOFT::GETINITPARAMETERSREQUEST")
  (make-package "BATMAG_SOFT::GETINITPARAMETERSREQUEST"))
(unless (find-package "BATMAG_SOFT::GETINITPARAMETERSRESPONSE")
  (make-package "BATMAG_SOFT::GETINITPARAMETERSRESPONSE"))

(in-package "ROS")





(defclass batmag_soft::GetInitParametersRequest
  :super ros::object
  :slots (_ctrl _agent ))

(defmethod batmag_soft::GetInitParametersRequest
  (:init
   (&key
    ((:ctrl __ctrl) "")
    ((:agent __agent) "")
    )
   (send-super :init)
   (setq _ctrl (string __ctrl))
   (setq _agent (string __agent))
   self)
  (:ctrl
   (&optional __ctrl)
   (if __ctrl (setq _ctrl __ctrl)) _ctrl)
  (:agent
   (&optional __agent)
   (if __agent (setq _agent __agent)) _agent)
  (:serialization-length
   ()
   (+
    ;; string _ctrl
    4 (length _ctrl)
    ;; string _agent
    4 (length _agent)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _ctrl
       (write-long (length _ctrl) s) (princ _ctrl s)
     ;; string _agent
       (write-long (length _agent) s) (princ _agent s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _ctrl
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _ctrl (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _agent
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _agent (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;;
   self)
  )

(defclass batmag_soft::GetInitParametersResponse
  :super ros::object
  :slots (_success _value _min _max _names ))

(defmethod batmag_soft::GetInitParametersResponse
  (:init
   (&key
    ((:success __success) nil)
    ((:value __value) (make-array 9 :initial-element 0.0 :element-type :float))
    ((:min __min) (make-array 9 :initial-element 0.0 :element-type :float))
    ((:max __max) (make-array 9 :initial-element 0.0 :element-type :float))
    ((:names __names) (let (r) (dotimes (i 9) (push "" r)) r))
    )
   (send-super :init)
   (setq _success __success)
   (setq _value __value)
   (setq _min __min)
   (setq _max __max)
   (setq _names __names)
   self)
  (:success
   (&optional __success)
   (if __success (setq _success __success)) _success)
  (:value
   (&optional __value)
   (if __value (setq _value __value)) _value)
  (:min
   (&optional __min)
   (if __min (setq _min __min)) _min)
  (:max
   (&optional __max)
   (if __max (setq _max __max)) _max)
  (:names
   (&optional __names)
   (if __names (setq _names __names)) _names)
  (:serialization-length
   ()
   (+
    ;; bool _success
    1
    ;; float64[9] _value
    (* 8    9)
    ;; float64[9] _min
    (* 8    9)
    ;; float64[9] _max
    (* 8    9)
    ;; string[9] _names
    (apply #'+ 4 (length _names)    9)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _success
       (if _success (write-byte -1 s) (write-byte 0 s))
     ;; float64[9] _value
     (dotimes (i 9)
       (sys::poke (elt _value i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64[9] _min
     (dotimes (i 9)
       (sys::poke (elt _min i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64[9] _max
     (dotimes (i 9)
       (sys::poke (elt _max i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; string[9] _names
     (write-long (length _names) s)
     (dolist (elem _names)
       (write-long (length elem) s) (princ elem s)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _success
     (setq _success (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; float64[9] _value
   (dotimes (i (length _value))
     (setf (elt _value i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     )
   ;; float64[9] _min
   (dotimes (i (length _min))
     (setf (elt _min i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     )
   ;; float64[9] _max
   (dotimes (i (length _max))
     (setf (elt _max i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     )
   ;; string[9] _names
   (dotimes (i (length _names))
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setf (elt _names i) (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
     )
   ;;
   self)
  )

(defclass batmag_soft::GetInitParameters
  :super ros::object
  :slots ())

(setf (get batmag_soft::GetInitParameters :md5sum-) "cde8ea7816229f9bd2f4733f008c24de")
(setf (get batmag_soft::GetInitParameters :datatype-) "batmag_soft/GetInitParameters")
(setf (get batmag_soft::GetInitParameters :request) batmag_soft::GetInitParametersRequest)
(setf (get batmag_soft::GetInitParameters :response) batmag_soft::GetInitParametersResponse)

(defmethod batmag_soft::GetInitParametersRequest
  (:response () (instance batmag_soft::GetInitParametersResponse :init)))

(setf (get batmag_soft::GetInitParametersRequest :md5sum-) "cde8ea7816229f9bd2f4733f008c24de")
(setf (get batmag_soft::GetInitParametersRequest :datatype-) "batmag_soft/GetInitParametersRequest")
(setf (get batmag_soft::GetInitParametersRequest :definition-)
      "string ctrl
string agent
---
bool success
float64[9] value
float64[9] min
float64[9] max
string[9] names


")

(setf (get batmag_soft::GetInitParametersResponse :md5sum-) "cde8ea7816229f9bd2f4733f008c24de")
(setf (get batmag_soft::GetInitParametersResponse :datatype-) "batmag_soft/GetInitParametersResponse")
(setf (get batmag_soft::GetInitParametersResponse :definition-)
      "string ctrl
string agent
---
bool success
float64[9] value
float64[9] min
float64[9] max
string[9] names


")



(provide :batmag_soft/GetInitParameters "cde8ea7816229f9bd2f4733f008c24de")


