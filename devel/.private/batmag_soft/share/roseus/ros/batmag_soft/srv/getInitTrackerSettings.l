;; Auto-generated. Do not edit!


(when (boundp 'batmag_soft::getInitTrackerSettings)
  (if (not (find-package "BATMAG_SOFT"))
    (make-package "BATMAG_SOFT"))
  (shadow 'getInitTrackerSettings (find-package "BATMAG_SOFT")))
(unless (find-package "BATMAG_SOFT::GETINITTRACKERSETTINGS")
  (make-package "BATMAG_SOFT::GETINITTRACKERSETTINGS"))
(unless (find-package "BATMAG_SOFT::GETINITTRACKERSETTINGSREQUEST")
  (make-package "BATMAG_SOFT::GETINITTRACKERSETTINGSREQUEST"))
(unless (find-package "BATMAG_SOFT::GETINITTRACKERSETTINGSRESPONSE")
  (make-package "BATMAG_SOFT::GETINITTRACKERSETTINGSRESPONSE"))

(in-package "ROS")





(defclass batmag_soft::getInitTrackerSettingsRequest
  :super ros::object
  :slots ())

(defmethod batmag_soft::getInitTrackerSettingsRequest
  (:init
   (&key
    )
   (send-super :init)
   self)
  (:serialization-length
   ()
   (+
    0
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;;
   self)
  )

(defclass batmag_soft::getInitTrackerSettingsResponse
  :super ros::object
  :slots (_roiSize _agentSize _maxH _minH _maxS _minS _maxV _minV _blurSize _id ))

(defmethod batmag_soft::getInitTrackerSettingsResponse
  (:init
   (&key
    ((:roiSize __roiSize) 0)
    ((:agentSize __agentSize) 0)
    ((:maxH __maxH) 0)
    ((:minH __minH) 0)
    ((:maxS __maxS) 0)
    ((:minS __minS) 0)
    ((:maxV __maxV) 0)
    ((:minV __minV) 0)
    ((:blurSize __blurSize) 0)
    ((:id __id) 0)
    )
   (send-super :init)
   (setq _roiSize (round __roiSize))
   (setq _agentSize (round __agentSize))
   (setq _maxH (round __maxH))
   (setq _minH (round __minH))
   (setq _maxS (round __maxS))
   (setq _minS (round __minS))
   (setq _maxV (round __maxV))
   (setq _minV (round __minV))
   (setq _blurSize (round __blurSize))
   (setq _id (round __id))
   self)
  (:roiSize
   (&optional __roiSize)
   (if __roiSize (setq _roiSize __roiSize)) _roiSize)
  (:agentSize
   (&optional __agentSize)
   (if __agentSize (setq _agentSize __agentSize)) _agentSize)
  (:maxH
   (&optional __maxH)
   (if __maxH (setq _maxH __maxH)) _maxH)
  (:minH
   (&optional __minH)
   (if __minH (setq _minH __minH)) _minH)
  (:maxS
   (&optional __maxS)
   (if __maxS (setq _maxS __maxS)) _maxS)
  (:minS
   (&optional __minS)
   (if __minS (setq _minS __minS)) _minS)
  (:maxV
   (&optional __maxV)
   (if __maxV (setq _maxV __maxV)) _maxV)
  (:minV
   (&optional __minV)
   (if __minV (setq _minV __minV)) _minV)
  (:blurSize
   (&optional __blurSize)
   (if __blurSize (setq _blurSize __blurSize)) _blurSize)
  (:id
   (&optional __id)
   (if __id (setq _id __id)) _id)
  (:serialization-length
   ()
   (+
    ;; int32 _roiSize
    4
    ;; int32 _agentSize
    4
    ;; int32 _maxH
    4
    ;; int32 _minH
    4
    ;; int32 _maxS
    4
    ;; int32 _minS
    4
    ;; int32 _maxV
    4
    ;; int32 _minV
    4
    ;; int32 _blurSize
    4
    ;; int8 _id
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int32 _roiSize
       (write-long _roiSize s)
     ;; int32 _agentSize
       (write-long _agentSize s)
     ;; int32 _maxH
       (write-long _maxH s)
     ;; int32 _minH
       (write-long _minH s)
     ;; int32 _maxS
       (write-long _maxS s)
     ;; int32 _minS
       (write-long _minS s)
     ;; int32 _maxV
       (write-long _maxV s)
     ;; int32 _minV
       (write-long _minV s)
     ;; int32 _blurSize
       (write-long _blurSize s)
     ;; int8 _id
       (write-byte _id s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int32 _roiSize
     (setq _roiSize (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _agentSize
     (setq _agentSize (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _maxH
     (setq _maxH (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _minH
     (setq _minH (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _maxS
     (setq _maxS (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _minS
     (setq _minS (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _maxV
     (setq _maxV (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _minV
     (setq _minV (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _blurSize
     (setq _blurSize (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int8 _id
     (setq _id (sys::peek buf ptr- :char)) (incf ptr- 1)
     (if (> _id 127) (setq _id (- _id 256)))
   ;;
   self)
  )

(defclass batmag_soft::getInitTrackerSettings
  :super ros::object
  :slots ())

(setf (get batmag_soft::getInitTrackerSettings :md5sum-) "f5a055a737dbee5ea3851522901b54b0")
(setf (get batmag_soft::getInitTrackerSettings :datatype-) "batmag_soft/getInitTrackerSettings")
(setf (get batmag_soft::getInitTrackerSettings :request) batmag_soft::getInitTrackerSettingsRequest)
(setf (get batmag_soft::getInitTrackerSettings :response) batmag_soft::getInitTrackerSettingsResponse)

(defmethod batmag_soft::getInitTrackerSettingsRequest
  (:response () (instance batmag_soft::getInitTrackerSettingsResponse :init)))

(setf (get batmag_soft::getInitTrackerSettingsRequest :md5sum-) "f5a055a737dbee5ea3851522901b54b0")
(setf (get batmag_soft::getInitTrackerSettingsRequest :datatype-) "batmag_soft/getInitTrackerSettingsRequest")
(setf (get batmag_soft::getInitTrackerSettingsRequest :definition-)
      "
---
int32 roiSize
int32 agentSize
int32 maxH
int32 minH
int32 maxS
int32 minS
int32 maxV
int32 minV
int32 blurSize
int8 id

")

(setf (get batmag_soft::getInitTrackerSettingsResponse :md5sum-) "f5a055a737dbee5ea3851522901b54b0")
(setf (get batmag_soft::getInitTrackerSettingsResponse :datatype-) "batmag_soft/getInitTrackerSettingsResponse")
(setf (get batmag_soft::getInitTrackerSettingsResponse :definition-)
      "
---
int32 roiSize
int32 agentSize
int32 maxH
int32 minH
int32 maxS
int32 minS
int32 maxV
int32 minV
int32 blurSize
int8 id

")



(provide :batmag_soft/getInitTrackerSettings "f5a055a737dbee5ea3851522901b54b0")


