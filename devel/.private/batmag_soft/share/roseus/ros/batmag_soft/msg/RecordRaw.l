;; Auto-generated. Do not edit!


(when (boundp 'batmag_soft::RecordRaw)
  (if (not (find-package "BATMAG_SOFT"))
    (make-package "BATMAG_SOFT"))
  (shadow 'RecordRaw (find-package "BATMAG_SOFT")))
(unless (find-package "BATMAG_SOFT::RECORDRAW")
  (make-package "BATMAG_SOFT::RECORDRAW"))

(in-package "ROS")
;;//! \htmlinclude RecordRaw.msg.html


(defclass batmag_soft::RecordRaw
  :super ros::object
  :slots (_recordRaw _recordWhere ))

(defmethod batmag_soft::RecordRaw
  (:init
   (&key
    ((:recordRaw __recordRaw) nil)
    ((:recordWhere __recordWhere) "")
    )
   (send-super :init)
   (setq _recordRaw __recordRaw)
   (setq _recordWhere (string __recordWhere))
   self)
  (:recordRaw
   (&optional __recordRaw)
   (if __recordRaw (setq _recordRaw __recordRaw)) _recordRaw)
  (:recordWhere
   (&optional __recordWhere)
   (if __recordWhere (setq _recordWhere __recordWhere)) _recordWhere)
  (:serialization-length
   ()
   (+
    ;; bool _recordRaw
    1
    ;; string _recordWhere
    4 (length _recordWhere)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _recordRaw
       (if _recordRaw (write-byte -1 s) (write-byte 0 s))
     ;; string _recordWhere
       (write-long (length _recordWhere) s) (princ _recordWhere s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _recordRaw
     (setq _recordRaw (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; string _recordWhere
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _recordWhere (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;;
   self)
  )

(setf (get batmag_soft::RecordRaw :md5sum-) "dba747aebd3c39de46937fb36819f3c1")
(setf (get batmag_soft::RecordRaw :datatype-) "batmag_soft/RecordRaw")
(setf (get batmag_soft::RecordRaw :definition-)
      "bool recordRaw
string recordWhere

")



(provide :batmag_soft/RecordRaw "dba747aebd3c39de46937fb36819f3c1")


