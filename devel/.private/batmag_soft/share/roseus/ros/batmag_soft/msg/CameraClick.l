;; Auto-generated. Do not edit!


(when (boundp 'batmag_soft::CameraClick)
  (if (not (find-package "BATMAG_SOFT"))
    (make-package "BATMAG_SOFT"))
  (shadow 'CameraClick (find-package "BATMAG_SOFT")))
(unless (find-package "BATMAG_SOFT::CAMERACLICK")
  (make-package "BATMAG_SOFT::CAMERACLICK"))

(in-package "ROS")
;;//! \htmlinclude CameraClick.msg.html


(defclass batmag_soft::CameraClick
  :super ros::object
  :slots (_x _y _button _camera ))

(defmethod batmag_soft::CameraClick
  (:init
   (&key
    ((:x __x) 0)
    ((:y __y) 0)
    ((:button __button) 0)
    ((:camera __camera) 0)
    )
   (send-super :init)
   (setq _x (round __x))
   (setq _y (round __y))
   (setq _button (round __button))
   (setq _camera (round __camera))
   self)
  (:x
   (&optional __x)
   (if __x (setq _x __x)) _x)
  (:y
   (&optional __y)
   (if __y (setq _y __y)) _y)
  (:button
   (&optional __button)
   (if __button (setq _button __button)) _button)
  (:camera
   (&optional __camera)
   (if __camera (setq _camera __camera)) _camera)
  (:serialization-length
   ()
   (+
    ;; int64 _x
    8
    ;; int64 _y
    8
    ;; int64 _button
    8
    ;; int64 _camera
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int64 _x
#+(or :alpha :irix6 :x86_64)
       (progn (sys::poke _x (send s :buffer) (send s :count) :long) (incf (stream-count s) 8))
#-(or :alpha :irix6 :x86_64)
       (cond ((and (class _x) (= (length (_x . bv)) 2)) ;; bignum
              (write-long (ash (elt (_x . bv) 0) 0) s)
              (write-long (ash (elt (_x . bv) 1) -1) s))
             ((and (class _x) (= (length (_x . bv)) 1)) ;; big1
              (write-long (elt (_x . bv) 0) s)
              (write-long (if (>= _x 0) 0 #xffffffff) s))
             (t                                         ;; integer
              (write-long _x s)(write-long (if (>= _x 0) 0 #xffffffff) s)))
     ;; int64 _y
#+(or :alpha :irix6 :x86_64)
       (progn (sys::poke _y (send s :buffer) (send s :count) :long) (incf (stream-count s) 8))
#-(or :alpha :irix6 :x86_64)
       (cond ((and (class _y) (= (length (_y . bv)) 2)) ;; bignum
              (write-long (ash (elt (_y . bv) 0) 0) s)
              (write-long (ash (elt (_y . bv) 1) -1) s))
             ((and (class _y) (= (length (_y . bv)) 1)) ;; big1
              (write-long (elt (_y . bv) 0) s)
              (write-long (if (>= _y 0) 0 #xffffffff) s))
             (t                                         ;; integer
              (write-long _y s)(write-long (if (>= _y 0) 0 #xffffffff) s)))
     ;; int64 _button
#+(or :alpha :irix6 :x86_64)
       (progn (sys::poke _button (send s :buffer) (send s :count) :long) (incf (stream-count s) 8))
#-(or :alpha :irix6 :x86_64)
       (cond ((and (class _button) (= (length (_button . bv)) 2)) ;; bignum
              (write-long (ash (elt (_button . bv) 0) 0) s)
              (write-long (ash (elt (_button . bv) 1) -1) s))
             ((and (class _button) (= (length (_button . bv)) 1)) ;; big1
              (write-long (elt (_button . bv) 0) s)
              (write-long (if (>= _button 0) 0 #xffffffff) s))
             (t                                         ;; integer
              (write-long _button s)(write-long (if (>= _button 0) 0 #xffffffff) s)))
     ;; int64 _camera
#+(or :alpha :irix6 :x86_64)
       (progn (sys::poke _camera (send s :buffer) (send s :count) :long) (incf (stream-count s) 8))
#-(or :alpha :irix6 :x86_64)
       (cond ((and (class _camera) (= (length (_camera . bv)) 2)) ;; bignum
              (write-long (ash (elt (_camera . bv) 0) 0) s)
              (write-long (ash (elt (_camera . bv) 1) -1) s))
             ((and (class _camera) (= (length (_camera . bv)) 1)) ;; big1
              (write-long (elt (_camera . bv) 0) s)
              (write-long (if (>= _camera 0) 0 #xffffffff) s))
             (t                                         ;; integer
              (write-long _camera s)(write-long (if (>= _camera 0) 0 #xffffffff) s)))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int64 _x
#+(or :alpha :irix6 :x86_64)
      (setf _x (prog1 (sys::peek buf ptr- :long) (incf ptr- 8)))
#-(or :alpha :irix6 :x86_64)
      (setf _x (let ((b0 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4)))
                  (b1 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4))))
              (cond ((= b1 -1) b0)
                     ((and (= b1  0)
                           (<= lisp::most-negative-fixnum b0 lisp::most-positive-fixnum))
                      b0)
                    ((= b1  0) (make-instance bignum :size 1 :bv (integer-vector b0)))
                    (t (make-instance bignum :size 2 :bv (integer-vector b0 (ash b1 1)))))))
   ;; int64 _y
#+(or :alpha :irix6 :x86_64)
      (setf _y (prog1 (sys::peek buf ptr- :long) (incf ptr- 8)))
#-(or :alpha :irix6 :x86_64)
      (setf _y (let ((b0 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4)))
                  (b1 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4))))
              (cond ((= b1 -1) b0)
                     ((and (= b1  0)
                           (<= lisp::most-negative-fixnum b0 lisp::most-positive-fixnum))
                      b0)
                    ((= b1  0) (make-instance bignum :size 1 :bv (integer-vector b0)))
                    (t (make-instance bignum :size 2 :bv (integer-vector b0 (ash b1 1)))))))
   ;; int64 _button
#+(or :alpha :irix6 :x86_64)
      (setf _button (prog1 (sys::peek buf ptr- :long) (incf ptr- 8)))
#-(or :alpha :irix6 :x86_64)
      (setf _button (let ((b0 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4)))
                  (b1 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4))))
              (cond ((= b1 -1) b0)
                     ((and (= b1  0)
                           (<= lisp::most-negative-fixnum b0 lisp::most-positive-fixnum))
                      b0)
                    ((= b1  0) (make-instance bignum :size 1 :bv (integer-vector b0)))
                    (t (make-instance bignum :size 2 :bv (integer-vector b0 (ash b1 1)))))))
   ;; int64 _camera
#+(or :alpha :irix6 :x86_64)
      (setf _camera (prog1 (sys::peek buf ptr- :long) (incf ptr- 8)))
#-(or :alpha :irix6 :x86_64)
      (setf _camera (let ((b0 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4)))
                  (b1 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4))))
              (cond ((= b1 -1) b0)
                     ((and (= b1  0)
                           (<= lisp::most-negative-fixnum b0 lisp::most-positive-fixnum))
                      b0)
                    ((= b1  0) (make-instance bignum :size 1 :bv (integer-vector b0)))
                    (t (make-instance bignum :size 2 :bv (integer-vector b0 (ash b1 1)))))))
   ;;
   self)
  )

(setf (get batmag_soft::CameraClick :md5sum-) "490f77eb6fbf239018653a891a759f0c")
(setf (get batmag_soft::CameraClick :datatype-) "batmag_soft/CameraClick")
(setf (get batmag_soft::CameraClick :definition-)
      "int64 x
int64 y
int64 button
int64 camera

")



(provide :batmag_soft/CameraClick "490f77eb6fbf239018653a891a759f0c")


