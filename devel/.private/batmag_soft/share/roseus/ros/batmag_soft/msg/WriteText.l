;; Auto-generated. Do not edit!


(when (boundp 'batmag_soft::WriteText)
  (if (not (find-package "BATMAG_SOFT"))
    (make-package "BATMAG_SOFT"))
  (shadow 'WriteText (find-package "BATMAG_SOFT")))
(unless (find-package "BATMAG_SOFT::WRITETEXT")
  (make-package "BATMAG_SOFT::WRITETEXT"))

(in-package "ROS")
;;//! \htmlinclude WriteText.msg.html


(defclass batmag_soft::WriteText
  :super ros::object
  :slots (_text ))

(defmethod batmag_soft::WriteText
  (:init
   (&key
    ((:text __text) "")
    )
   (send-super :init)
   (setq _text (string __text))
   self)
  (:text
   (&optional __text)
   (if __text (setq _text __text)) _text)
  (:serialization-length
   ()
   (+
    ;; string _text
    4 (length _text)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _text
       (write-long (length _text) s) (princ _text s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _text
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _text (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;;
   self)
  )

(setf (get batmag_soft::WriteText :md5sum-) "74697ed3d931f6eede8bf3a8dfeca160")
(setf (get batmag_soft::WriteText :datatype-) "batmag_soft/WriteText")
(setf (get batmag_soft::WriteText :definition-)
      "string text

")



(provide :batmag_soft/WriteText "74697ed3d931f6eede8bf3a8dfeca160")


