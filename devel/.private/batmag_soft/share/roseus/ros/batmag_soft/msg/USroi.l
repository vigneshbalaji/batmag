;; Auto-generated. Do not edit!


(when (boundp 'batmag_soft::USroi)
  (if (not (find-package "BATMAG_SOFT"))
    (make-package "BATMAG_SOFT"))
  (shadow 'USroi (find-package "BATMAG_SOFT")))
(unless (find-package "BATMAG_SOFT::USROI")
  (make-package "BATMAG_SOFT::USROI"))

(in-package "ROS")
;;//! \htmlinclude USroi.msg.html


(defclass batmag_soft::USroi
  :super ros::object
  :slots (_width _height _topleftx _toplefty ))

(defmethod batmag_soft::USroi
  (:init
   (&key
    ((:width __width) 0)
    ((:height __height) 0)
    ((:topleftx __topleftx) 0)
    ((:toplefty __toplefty) 0)
    )
   (send-super :init)
   (setq _width (round __width))
   (setq _height (round __height))
   (setq _topleftx (round __topleftx))
   (setq _toplefty (round __toplefty))
   self)
  (:width
   (&optional __width)
   (if __width (setq _width __width)) _width)
  (:height
   (&optional __height)
   (if __height (setq _height __height)) _height)
  (:topleftx
   (&optional __topleftx)
   (if __topleftx (setq _topleftx __topleftx)) _topleftx)
  (:toplefty
   (&optional __toplefty)
   (if __toplefty (setq _toplefty __toplefty)) _toplefty)
  (:serialization-length
   ()
   (+
    ;; int32 _width
    4
    ;; int32 _height
    4
    ;; int32 _topleftx
    4
    ;; int32 _toplefty
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int32 _width
       (write-long _width s)
     ;; int32 _height
       (write-long _height s)
     ;; int32 _topleftx
       (write-long _topleftx s)
     ;; int32 _toplefty
       (write-long _toplefty s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int32 _width
     (setq _width (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _height
     (setq _height (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _topleftx
     (setq _topleftx (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _toplefty
     (setq _toplefty (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get batmag_soft::USroi :md5sum-) "e93dbc2b5418de3cd63fd0b87d53412f")
(setf (get batmag_soft::USroi :datatype-) "batmag_soft/USroi")
(setf (get batmag_soft::USroi :definition-)
      "int32 width
int32 height
int32 topleftx
int32 toplefty

")



(provide :batmag_soft/USroi "e93dbc2b5418de3cd63fd0b87d53412f")


