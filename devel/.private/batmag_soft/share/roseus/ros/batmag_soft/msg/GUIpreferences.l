;; Auto-generated. Do not edit!


(when (boundp 'batmag_soft::GUIpreferences)
  (if (not (find-package "BATMAG_SOFT"))
    (make-package "BATMAG_SOFT"))
  (shadow 'GUIpreferences (find-package "BATMAG_SOFT")))
(unless (find-package "BATMAG_SOFT::GUIPREFERENCES")
  (make-package "BATMAG_SOFT::GUIPREFERENCES"))

(in-package "ROS")
;;//! \htmlinclude GUIpreferences.msg.html


(defclass batmag_soft::GUIpreferences
  :super ros::object
  :slots (_recordOn _ctrlEn _debugEn _gravityEn _gridOn _PMposition _PMenable _omegaCtrl ))

(defmethod batmag_soft::GUIpreferences
  (:init
   (&key
    ((:recordOn __recordOn) nil)
    ((:ctrlEn __ctrlEn) nil)
    ((:debugEn __debugEn) nil)
    ((:gravityEn __gravityEn) nil)
    ((:gridOn __gridOn) nil)
    ((:PMposition __PMposition) 0.0)
    ((:PMenable __PMenable) nil)
    ((:omegaCtrl __omegaCtrl) nil)
    )
   (send-super :init)
   (setq _recordOn __recordOn)
   (setq _ctrlEn __ctrlEn)
   (setq _debugEn __debugEn)
   (setq _gravityEn __gravityEn)
   (setq _gridOn __gridOn)
   (setq _PMposition (float __PMposition))
   (setq _PMenable __PMenable)
   (setq _omegaCtrl __omegaCtrl)
   self)
  (:recordOn
   (&optional __recordOn)
   (if __recordOn (setq _recordOn __recordOn)) _recordOn)
  (:ctrlEn
   (&optional __ctrlEn)
   (if __ctrlEn (setq _ctrlEn __ctrlEn)) _ctrlEn)
  (:debugEn
   (&optional __debugEn)
   (if __debugEn (setq _debugEn __debugEn)) _debugEn)
  (:gravityEn
   (&optional __gravityEn)
   (if __gravityEn (setq _gravityEn __gravityEn)) _gravityEn)
  (:gridOn
   (&optional __gridOn)
   (if __gridOn (setq _gridOn __gridOn)) _gridOn)
  (:PMposition
   (&optional __PMposition)
   (if __PMposition (setq _PMposition __PMposition)) _PMposition)
  (:PMenable
   (&optional __PMenable)
   (if __PMenable (setq _PMenable __PMenable)) _PMenable)
  (:omegaCtrl
   (&optional __omegaCtrl)
   (if __omegaCtrl (setq _omegaCtrl __omegaCtrl)) _omegaCtrl)
  (:serialization-length
   ()
   (+
    ;; bool _recordOn
    1
    ;; bool _ctrlEn
    1
    ;; bool _debugEn
    1
    ;; bool _gravityEn
    1
    ;; bool _gridOn
    1
    ;; float64 _PMposition
    8
    ;; bool _PMenable
    1
    ;; bool _omegaCtrl
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _recordOn
       (if _recordOn (write-byte -1 s) (write-byte 0 s))
     ;; bool _ctrlEn
       (if _ctrlEn (write-byte -1 s) (write-byte 0 s))
     ;; bool _debugEn
       (if _debugEn (write-byte -1 s) (write-byte 0 s))
     ;; bool _gravityEn
       (if _gravityEn (write-byte -1 s) (write-byte 0 s))
     ;; bool _gridOn
       (if _gridOn (write-byte -1 s) (write-byte 0 s))
     ;; float64 _PMposition
       (sys::poke _PMposition (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; bool _PMenable
       (if _PMenable (write-byte -1 s) (write-byte 0 s))
     ;; bool _omegaCtrl
       (if _omegaCtrl (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _recordOn
     (setq _recordOn (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _ctrlEn
     (setq _ctrlEn (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _debugEn
     (setq _debugEn (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _gravityEn
     (setq _gravityEn (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _gridOn
     (setq _gridOn (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; float64 _PMposition
     (setq _PMposition (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; bool _PMenable
     (setq _PMenable (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _omegaCtrl
     (setq _omegaCtrl (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get batmag_soft::GUIpreferences :md5sum-) "30cc44c0758499a2509ba85095012fbb")
(setf (get batmag_soft::GUIpreferences :datatype-) "batmag_soft/GUIpreferences")
(setf (get batmag_soft::GUIpreferences :definition-)
      "bool recordOn
bool ctrlEn
bool debugEn
bool gravityEn
bool gridOn
float64 PMposition
bool PMenable
bool omegaCtrl

")



(provide :batmag_soft/GUIpreferences "30cc44c0758499a2509ba85095012fbb")


