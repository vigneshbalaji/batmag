;; Auto-generated. Do not edit!


(when (boundp 'batmag_soft::pmMove)
  (if (not (find-package "BATMAG_SOFT"))
    (make-package "BATMAG_SOFT"))
  (shadow 'pmMove (find-package "BATMAG_SOFT")))
(unless (find-package "BATMAG_SOFT::PMMOVE")
  (make-package "BATMAG_SOFT::PMMOVE"))

(in-package "ROS")
;;//! \htmlinclude pmMove.msg.html


(defclass batmag_soft::pmMove
  :super ros::object
  :slots (_position _enable ))

(defmethod batmag_soft::pmMove
  (:init
   (&key
    ((:position __position) 0.0)
    ((:enable __enable) nil)
    )
   (send-super :init)
   (setq _position (float __position))
   (setq _enable __enable)
   self)
  (:position
   (&optional __position)
   (if __position (setq _position __position)) _position)
  (:enable
   (&optional __enable)
   (if __enable (setq _enable __enable)) _enable)
  (:serialization-length
   ()
   (+
    ;; float64 _position
    8
    ;; bool _enable
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64 _position
       (sys::poke _position (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; bool _enable
       (if _enable (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64 _position
     (setq _position (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; bool _enable
     (setq _enable (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get batmag_soft::pmMove :md5sum-) "53037050e41b7763a8fbce1f9811be05")
(setf (get batmag_soft::pmMove :datatype-) "batmag_soft/pmMove")
(setf (get batmag_soft::pmMove :definition-)
      "float64 position
bool enable

")



(provide :batmag_soft/pmMove "53037050e41b7763a8fbce1f9811be05")


