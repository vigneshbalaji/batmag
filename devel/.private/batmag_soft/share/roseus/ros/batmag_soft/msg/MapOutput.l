;; Auto-generated. Do not edit!


(when (boundp 'batmag_soft::MapOutput)
  (if (not (find-package "BATMAG_SOFT"))
    (make-package "BATMAG_SOFT"))
  (shadow 'MapOutput (find-package "BATMAG_SOFT")))
(unless (find-package "BATMAG_SOFT::MAPOUTPUT")
  (make-package "BATMAG_SOFT::MAPOUTPUT"))

(in-package "ROS")
;;//! \htmlinclude MapOutput.msg.html


(defclass batmag_soft::MapOutput
  :super ros::object
  :slots (_mapout _size ))

(defmethod batmag_soft::MapOutput
  (:init
   (&key
    ((:mapout __mapout) (make-array 10 :initial-element 0.0 :element-type :float))
    ((:size __size) 0)
    )
   (send-super :init)
   (setq _mapout __mapout)
   (setq _size (round __size))
   self)
  (:mapout
   (&optional __mapout)
   (if __mapout (setq _mapout __mapout)) _mapout)
  (:size
   (&optional __size)
   (if __size (setq _size __size)) _size)
  (:serialization-length
   ()
   (+
    ;; float64[10] _mapout
    (* 8    10)
    ;; int64 _size
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64[10] _mapout
     (dotimes (i 10)
       (sys::poke (elt _mapout i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; int64 _size
#+(or :alpha :irix6 :x86_64)
       (progn (sys::poke _size (send s :buffer) (send s :count) :long) (incf (stream-count s) 8))
#-(or :alpha :irix6 :x86_64)
       (cond ((and (class _size) (= (length (_size . bv)) 2)) ;; bignum
              (write-long (ash (elt (_size . bv) 0) 0) s)
              (write-long (ash (elt (_size . bv) 1) -1) s))
             ((and (class _size) (= (length (_size . bv)) 1)) ;; big1
              (write-long (elt (_size . bv) 0) s)
              (write-long (if (>= _size 0) 0 #xffffffff) s))
             (t                                         ;; integer
              (write-long _size s)(write-long (if (>= _size 0) 0 #xffffffff) s)))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64[10] _mapout
   (dotimes (i (length _mapout))
     (setf (elt _mapout i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     )
   ;; int64 _size
#+(or :alpha :irix6 :x86_64)
      (setf _size (prog1 (sys::peek buf ptr- :long) (incf ptr- 8)))
#-(or :alpha :irix6 :x86_64)
      (setf _size (let ((b0 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4)))
                  (b1 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4))))
              (cond ((= b1 -1) b0)
                     ((and (= b1  0)
                           (<= lisp::most-negative-fixnum b0 lisp::most-positive-fixnum))
                      b0)
                    ((= b1  0) (make-instance bignum :size 1 :bv (integer-vector b0)))
                    (t (make-instance bignum :size 2 :bv (integer-vector b0 (ash b1 1)))))))
   ;;
   self)
  )

(setf (get batmag_soft::MapOutput :md5sum-) "8796c057cb44e7e6236c77ac997c7757")
(setf (get batmag_soft::MapOutput :datatype-) "batmag_soft/MapOutput")
(setf (get batmag_soft::MapOutput :definition-)
      "float64[10] mapout
int64 size

")



(provide :batmag_soft/MapOutput "8796c057cb44e7e6236c77ac997c7757")


