;; Auto-generated. Do not edit!


(when (boundp 'batmag_soft::CtrlOutput)
  (if (not (find-package "BATMAG_SOFT"))
    (make-package "BATMAG_SOFT"))
  (shadow 'CtrlOutput (find-package "BATMAG_SOFT")))
(unless (find-package "BATMAG_SOFT::CTRLOUTPUT")
  (make-package "BATMAG_SOFT::CTRLOUTPUT"))

(in-package "ROS")
;;//! \htmlinclude CtrlOutput.msg.html


(defclass batmag_soft::CtrlOutput
  :super ros::object
  :slots (_ctrlout _size ))

(defmethod batmag_soft::CtrlOutput
  (:init
   (&key
    ((:ctrlout __ctrlout) (make-array 10 :initial-element 0.0 :element-type :float))
    ((:size __size) 0)
    )
   (send-super :init)
   (setq _ctrlout __ctrlout)
   (setq _size (round __size))
   self)
  (:ctrlout
   (&optional __ctrlout)
   (if __ctrlout (setq _ctrlout __ctrlout)) _ctrlout)
  (:size
   (&optional __size)
   (if __size (setq _size __size)) _size)
  (:serialization-length
   ()
   (+
    ;; float64[10] _ctrlout
    (* 8    10)
    ;; int64 _size
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64[10] _ctrlout
     (dotimes (i 10)
       (sys::poke (elt _ctrlout i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; int64 _size
#+(or :alpha :irix6 :x86_64)
       (progn (sys::poke _size (send s :buffer) (send s :count) :long) (incf (stream-count s) 8))
#-(or :alpha :irix6 :x86_64)
       (cond ((and (class _size) (= (length (_size . bv)) 2)) ;; bignum
              (write-long (ash (elt (_size . bv) 0) 0) s)
              (write-long (ash (elt (_size . bv) 1) -1) s))
             ((and (class _size) (= (length (_size . bv)) 1)) ;; big1
              (write-long (elt (_size . bv) 0) s)
              (write-long (if (>= _size 0) 0 #xffffffff) s))
             (t                                         ;; integer
              (write-long _size s)(write-long (if (>= _size 0) 0 #xffffffff) s)))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64[10] _ctrlout
   (dotimes (i (length _ctrlout))
     (setf (elt _ctrlout i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     )
   ;; int64 _size
#+(or :alpha :irix6 :x86_64)
      (setf _size (prog1 (sys::peek buf ptr- :long) (incf ptr- 8)))
#-(or :alpha :irix6 :x86_64)
      (setf _size (let ((b0 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4)))
                  (b1 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4))))
              (cond ((= b1 -1) b0)
                     ((and (= b1  0)
                           (<= lisp::most-negative-fixnum b0 lisp::most-positive-fixnum))
                      b0)
                    ((= b1  0) (make-instance bignum :size 1 :bv (integer-vector b0)))
                    (t (make-instance bignum :size 2 :bv (integer-vector b0 (ash b1 1)))))))
   ;;
   self)
  )

(setf (get batmag_soft::CtrlOutput :md5sum-) "eab7552313b31041534bc8457d395d24")
(setf (get batmag_soft::CtrlOutput :datatype-) "batmag_soft/CtrlOutput")
(setf (get batmag_soft::CtrlOutput :definition-)
      "float64[10] ctrlout
int64 size

")



(provide :batmag_soft/CtrlOutput "eab7552313b31041534bc8457d395d24")


