;; Auto-generated. Do not edit!


(when (boundp 'batmag_soft::ResetElmos)
  (if (not (find-package "BATMAG_SOFT"))
    (make-package "BATMAG_SOFT"))
  (shadow 'ResetElmos (find-package "BATMAG_SOFT")))
(unless (find-package "BATMAG_SOFT::RESETELMOS")
  (make-package "BATMAG_SOFT::RESETELMOS"))

(in-package "ROS")
;;//! \htmlinclude ResetElmos.msg.html


(defclass batmag_soft::ResetElmos
  :super ros::object
  :slots (_reset ))

(defmethod batmag_soft::ResetElmos
  (:init
   (&key
    ((:reset __reset) nil)
    )
   (send-super :init)
   (setq _reset __reset)
   self)
  (:reset
   (&optional __reset)
   (if __reset (setq _reset __reset)) _reset)
  (:serialization-length
   ()
   (+
    ;; bool _reset
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _reset
       (if _reset (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _reset
     (setq _reset (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get batmag_soft::ResetElmos :md5sum-) "ba4b0b221fb425ac5eaf73f71ae34971")
(setf (get batmag_soft::ResetElmos :datatype-) "batmag_soft/ResetElmos")
(setf (get batmag_soft::ResetElmos :definition-)
      "bool reset

")



(provide :batmag_soft/ResetElmos "ba4b0b221fb425ac5eaf73f71ae34971")


