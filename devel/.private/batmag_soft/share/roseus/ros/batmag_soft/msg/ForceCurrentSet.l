;; Auto-generated. Do not edit!


(when (boundp 'batmag_soft::ForceCurrentSet)
  (if (not (find-package "BATMAG_SOFT"))
    (make-package "BATMAG_SOFT"))
  (shadow 'ForceCurrentSet (find-package "BATMAG_SOFT")))
(unless (find-package "BATMAG_SOFT::FORCECURRENTSET")
  (make-package "BATMAG_SOFT::FORCECURRENTSET"))

(in-package "ROS")
;;//! \htmlinclude ForceCurrentSet.msg.html


(defclass batmag_soft::ForceCurrentSet
  :super ros::object
  :slots (_mur _radius _br _density ))

(defmethod batmag_soft::ForceCurrentSet
  (:init
   (&key
    ((:mur __mur) (make-array 0 :initial-element 0.0 :element-type :float))
    ((:radius __radius) (make-array 0 :initial-element 0.0 :element-type :float))
    ((:br __br) (make-array 0 :initial-element 0.0 :element-type :float))
    ((:density __density) 0)
    )
   (send-super :init)
   (setq _mur __mur)
   (setq _radius __radius)
   (setq _br __br)
   (setq _density (round __density))
   self)
  (:mur
   (&optional __mur)
   (if __mur (setq _mur __mur)) _mur)
  (:radius
   (&optional __radius)
   (if __radius (setq _radius __radius)) _radius)
  (:br
   (&optional __br)
   (if __br (setq _br __br)) _br)
  (:density
   (&optional __density)
   (if __density (setq _density __density)) _density)
  (:serialization-length
   ()
   (+
    ;; float64[] _mur
    (* 8    (length _mur)) 4
    ;; float64[] _radius
    (* 8    (length _radius)) 4
    ;; float64[] _br
    (* 8    (length _br)) 4
    ;; int32 _density
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64[] _mur
     (write-long (length _mur) s)
     (dotimes (i (length _mur))
       (sys::poke (elt _mur i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64[] _radius
     (write-long (length _radius) s)
     (dotimes (i (length _radius))
       (sys::poke (elt _radius i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64[] _br
     (write-long (length _br) s)
     (dotimes (i (length _br))
       (sys::poke (elt _br i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; int32 _density
       (write-long _density s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64[] _mur
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _mur (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _mur i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;; float64[] _radius
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _radius (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _radius i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;; float64[] _br
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _br (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _br i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;; int32 _density
     (setq _density (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get batmag_soft::ForceCurrentSet :md5sum-) "3a6903e8787abda9824ac514d912fce9")
(setf (get batmag_soft::ForceCurrentSet :datatype-) "batmag_soft/ForceCurrentSet")
(setf (get batmag_soft::ForceCurrentSet :definition-)
      "float64[] mur
float64[] radius
float64[] br
int32 density

")



(provide :batmag_soft/ForceCurrentSet "3a6903e8787abda9824ac514d912fce9")


