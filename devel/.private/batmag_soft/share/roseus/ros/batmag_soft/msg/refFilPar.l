;; Auto-generated. Do not edit!


(when (boundp 'batmag_soft::refFilPar)
  (if (not (find-package "BATMAG_SOFT"))
    (make-package "BATMAG_SOFT"))
  (shadow 'refFilPar (find-package "BATMAG_SOFT")))
(unless (find-package "BATMAG_SOFT::REFFILPAR")
  (make-package "BATMAG_SOFT::REFFILPAR"))

(in-package "ROS")
;;//! \htmlinclude refFilPar.msg.html


(defclass batmag_soft::refFilPar
  :super ros::object
  :slots (_tau _ub _lb _enable ))

(defmethod batmag_soft::refFilPar
  (:init
   (&key
    ((:tau __tau) 0.0)
    ((:ub __ub) 0.0)
    ((:lb __lb) 0.0)
    ((:enable __enable) nil)
    )
   (send-super :init)
   (setq _tau (float __tau))
   (setq _ub (float __ub))
   (setq _lb (float __lb))
   (setq _enable __enable)
   self)
  (:tau
   (&optional __tau)
   (if __tau (setq _tau __tau)) _tau)
  (:ub
   (&optional __ub)
   (if __ub (setq _ub __ub)) _ub)
  (:lb
   (&optional __lb)
   (if __lb (setq _lb __lb)) _lb)
  (:enable
   (&optional __enable)
   (if __enable (setq _enable __enable)) _enable)
  (:serialization-length
   ()
   (+
    ;; float64 _tau
    8
    ;; float64 _ub
    8
    ;; float64 _lb
    8
    ;; bool _enable
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64 _tau
       (sys::poke _tau (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _ub
       (sys::poke _ub (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _lb
       (sys::poke _lb (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; bool _enable
       (if _enable (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64 _tau
     (setq _tau (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _ub
     (setq _ub (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _lb
     (setq _lb (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; bool _enable
     (setq _enable (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get batmag_soft::refFilPar :md5sum-) "2a0558c57d7b112179d563789fd6bb0e")
(setf (get batmag_soft::refFilPar :datatype-) "batmag_soft/refFilPar")
(setf (get batmag_soft::refFilPar :definition-)
      "float64 tau
float64 ub
float64 lb
bool enable

")



(provide :batmag_soft/refFilPar "2a0558c57d7b112179d563789fd6bb0e")


