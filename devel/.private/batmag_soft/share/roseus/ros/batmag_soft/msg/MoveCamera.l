;; Auto-generated. Do not edit!


(when (boundp 'batmag_soft::MoveCamera)
  (if (not (find-package "BATMAG_SOFT"))
    (make-package "BATMAG_SOFT"))
  (shadow 'MoveCamera (find-package "BATMAG_SOFT")))
(unless (find-package "BATMAG_SOFT::MOVECAMERA")
  (make-package "BATMAG_SOFT::MOVECAMERA"))

(in-package "ROS")
;;//! \htmlinclude MoveCamera.msg.html


(defclass batmag_soft::MoveCamera
  :super ros::object
  :slots (_cameraSpeed _autofocusOn ))

(defmethod batmag_soft::MoveCamera
  (:init
   (&key
    ((:cameraSpeed __cameraSpeed) (make-array 2 :initial-element 0 :element-type :integer))
    ((:autofocusOn __autofocusOn) (let (r) (dotimes (i 2) (push nil r)) r))
    )
   (send-super :init)
   (setq _cameraSpeed __cameraSpeed)
   (setq _autofocusOn __autofocusOn)
   self)
  (:cameraSpeed
   (&optional __cameraSpeed)
   (if __cameraSpeed (setq _cameraSpeed __cameraSpeed)) _cameraSpeed)
  (:autofocusOn
   (&optional __autofocusOn)
   (if __autofocusOn (setq _autofocusOn __autofocusOn)) _autofocusOn)
  (:serialization-length
   ()
   (+
    ;; int64[2] _cameraSpeed
    (* 8    2)
    ;; bool[2] _autofocusOn
    (* 1    2)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int64[2] _cameraSpeed
     (dotimes (i 2)
#+(or :alpha :irix6 :x86_64)
       (progn (sys::poke (elt _cameraSpeed i) (send s :buffer) (send s :count) :long) (incf (stream-count s) 8))
#-(or :alpha :irix6 :x86_64)
       (cond ((and (class (elt _cameraSpeed i)) (= (length ((elt _cameraSpeed i) . bv)) 2)) ;; bignum
              (write-long (ash (elt ((elt _cameraSpeed i) . bv) 0) 0) s)
              (write-long (ash (elt ((elt _cameraSpeed i) . bv) 1) -1) s))
             ((and (class (elt _cameraSpeed i)) (= (length ((elt _cameraSpeed i) . bv)) 1)) ;; big1
              (write-long (elt ((elt _cameraSpeed i) . bv) 0) s)
              (write-long (if (>= (elt _cameraSpeed i) 0) 0 #xffffffff) s))
             (t                                         ;; integer
              (write-long (elt _cameraSpeed i) s)(write-long (if (>= (elt _cameraSpeed i) 0) 0 #xffffffff) s)))
       )
     ;; bool[2] _autofocusOn
     (dotimes (i 2)
       (if (elt _autofocusOn i) (write-byte -1 s) (write-byte 0 s))
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int64[2] _cameraSpeed
   (dotimes (i (length _cameraSpeed))
#+(or :alpha :irix6 :x86_64)
      (setf (elt _cameraSpeed i) (prog1 (sys::peek buf ptr- :long) (incf ptr- 8)))
#-(or :alpha :irix6 :x86_64)
      (setf (elt _cameraSpeed i) (let ((b0 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4)))
                  (b1 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4))))
              (cond ((= b1 -1) b0)
                     ((and (= b1  0)
                           (<= lisp::most-negative-fixnum b0 lisp::most-positive-fixnum))
                      b0)
                    ((= b1  0) (make-instance bignum :size 1 :bv (integer-vector b0)))
                    (t (make-instance bignum :size 2 :bv (integer-vector b0 (ash b1 1)))))))
     )
   ;; bool[2] _autofocusOn
   (dotimes (i (length _autofocusOn))
     (setf (elt _autofocusOn i) (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
     )
   ;;
   self)
  )

(setf (get batmag_soft::MoveCamera :md5sum-) "11601d08162551242a2c2c2bbcb261be")
(setf (get batmag_soft::MoveCamera :datatype-) "batmag_soft/MoveCamera")
(setf (get batmag_soft::MoveCamera :definition-)
      "int64[2] cameraSpeed
bool[2] autofocusOn
 

")



(provide :batmag_soft/MoveCamera "11601d08162551242a2c2c2bbcb261be")


