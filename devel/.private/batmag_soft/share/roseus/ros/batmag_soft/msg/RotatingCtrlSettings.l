;; Auto-generated. Do not edit!


(when (boundp 'batmag_soft::RotatingCtrlSettings)
  (if (not (find-package "BATMAG_SOFT"))
    (make-package "BATMAG_SOFT"))
  (shadow 'RotatingCtrlSettings (find-package "BATMAG_SOFT")))
(unless (find-package "BATMAG_SOFT::ROTATINGCTRLSETTINGS")
  (make-package "BATMAG_SOFT::ROTATINGCTRLSETTINGS"))

(in-package "ROS")
;;//! \htmlinclude RotatingCtrlSettings.msg.html


(defclass batmag_soft::RotatingCtrlSettings
  :super ros::object
  :slots (_freq _rotAmp _fixAmp ))

(defmethod batmag_soft::RotatingCtrlSettings
  (:init
   (&key
    ((:freq __freq) 0.0)
    ((:rotAmp __rotAmp) 0.0)
    ((:fixAmp __fixAmp) 0.0)
    )
   (send-super :init)
   (setq _freq (float __freq))
   (setq _rotAmp (float __rotAmp))
   (setq _fixAmp (float __fixAmp))
   self)
  (:freq
   (&optional __freq)
   (if __freq (setq _freq __freq)) _freq)
  (:rotAmp
   (&optional __rotAmp)
   (if __rotAmp (setq _rotAmp __rotAmp)) _rotAmp)
  (:fixAmp
   (&optional __fixAmp)
   (if __fixAmp (setq _fixAmp __fixAmp)) _fixAmp)
  (:serialization-length
   ()
   (+
    ;; float64 _freq
    8
    ;; float64 _rotAmp
    8
    ;; float64 _fixAmp
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64 _freq
       (sys::poke _freq (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _rotAmp
       (sys::poke _rotAmp (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _fixAmp
       (sys::poke _fixAmp (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64 _freq
     (setq _freq (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _rotAmp
     (setq _rotAmp (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _fixAmp
     (setq _fixAmp (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;;
   self)
  )

(setf (get batmag_soft::RotatingCtrlSettings :md5sum-) "3f7676b3fed2bc1451d91224b6f01fe4")
(setf (get batmag_soft::RotatingCtrlSettings :datatype-) "batmag_soft/RotatingCtrlSettings")
(setf (get batmag_soft::RotatingCtrlSettings :definition-)
      "float64 freq
float64 rotAmp
float64 fixAmp

")



(provide :batmag_soft/RotatingCtrlSettings "3f7676b3fed2bc1451d91224b6f01fe4")


