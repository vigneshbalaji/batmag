;; Auto-generated. Do not edit!


(when (boundp 'batmag_soft::Reference)
  (if (not (find-package "BATMAG_SOFT"))
    (make-package "BATMAG_SOFT"))
  (shadow 'Reference (find-package "BATMAG_SOFT")))
(unless (find-package "BATMAG_SOFT::REFERENCE")
  (make-package "BATMAG_SOFT::REFERENCE"))

(in-package "ROS")
;;//! \htmlinclude Reference.msg.html


(defclass batmag_soft::Reference
  :super ros::object
  :slots (_ref ))

(defmethod batmag_soft::Reference
  (:init
   (&key
    ((:ref __ref) (make-array 7 :initial-element 0.0 :element-type :float))
    )
   (send-super :init)
   (setq _ref __ref)
   self)
  (:ref
   (&optional __ref)
   (if __ref (setq _ref __ref)) _ref)
  (:serialization-length
   ()
   (+
    ;; float64[7] _ref
    (* 8    7)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64[7] _ref
     (dotimes (i 7)
       (sys::poke (elt _ref i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64[7] _ref
   (dotimes (i (length _ref))
     (setf (elt _ref i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     )
   ;;
   self)
  )

(setf (get batmag_soft::Reference :md5sum-) "137889b5ee426fb7970c1a5f01204398")
(setf (get batmag_soft::Reference :datatype-) "batmag_soft/Reference")
(setf (get batmag_soft::Reference :definition-)
      "float64[7] ref

")



(provide :batmag_soft/Reference "137889b5ee426fb7970c1a5f01204398")


