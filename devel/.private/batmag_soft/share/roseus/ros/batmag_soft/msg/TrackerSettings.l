;; Auto-generated. Do not edit!


(when (boundp 'batmag_soft::TrackerSettings)
  (if (not (find-package "BATMAG_SOFT"))
    (make-package "BATMAG_SOFT"))
  (shadow 'TrackerSettings (find-package "BATMAG_SOFT")))
(unless (find-package "BATMAG_SOFT::TRACKERSETTINGS")
  (make-package "BATMAG_SOFT::TRACKERSETTINGS"))

(in-package "ROS")
;;//! \htmlinclude TrackerSettings.msg.html


(defclass batmag_soft::TrackerSettings
  :super ros::object
  :slots (_roiSize _agentSize _maxH _minH _maxS _minS _maxV _minV _displayOn _blurSize _fixed _id _backSubOn _thisIsBack ))

(defmethod batmag_soft::TrackerSettings
  (:init
   (&key
    ((:roiSize __roiSize) 0)
    ((:agentSize __agentSize) 0)
    ((:maxH __maxH) 0)
    ((:minH __minH) 0)
    ((:maxS __maxS) 0)
    ((:minS __minS) 0)
    ((:maxV __maxV) 0)
    ((:minV __minV) 0)
    ((:displayOn __displayOn) nil)
    ((:blurSize __blurSize) 0)
    ((:fixed __fixed) nil)
    ((:id __id) 0)
    ((:backSubOn __backSubOn) nil)
    ((:thisIsBack __thisIsBack) nil)
    )
   (send-super :init)
   (setq _roiSize (round __roiSize))
   (setq _agentSize (round __agentSize))
   (setq _maxH (round __maxH))
   (setq _minH (round __minH))
   (setq _maxS (round __maxS))
   (setq _minS (round __minS))
   (setq _maxV (round __maxV))
   (setq _minV (round __minV))
   (setq _displayOn __displayOn)
   (setq _blurSize (round __blurSize))
   (setq _fixed __fixed)
   (setq _id (round __id))
   (setq _backSubOn __backSubOn)
   (setq _thisIsBack __thisIsBack)
   self)
  (:roiSize
   (&optional __roiSize)
   (if __roiSize (setq _roiSize __roiSize)) _roiSize)
  (:agentSize
   (&optional __agentSize)
   (if __agentSize (setq _agentSize __agentSize)) _agentSize)
  (:maxH
   (&optional __maxH)
   (if __maxH (setq _maxH __maxH)) _maxH)
  (:minH
   (&optional __minH)
   (if __minH (setq _minH __minH)) _minH)
  (:maxS
   (&optional __maxS)
   (if __maxS (setq _maxS __maxS)) _maxS)
  (:minS
   (&optional __minS)
   (if __minS (setq _minS __minS)) _minS)
  (:maxV
   (&optional __maxV)
   (if __maxV (setq _maxV __maxV)) _maxV)
  (:minV
   (&optional __minV)
   (if __minV (setq _minV __minV)) _minV)
  (:displayOn
   (&optional __displayOn)
   (if __displayOn (setq _displayOn __displayOn)) _displayOn)
  (:blurSize
   (&optional __blurSize)
   (if __blurSize (setq _blurSize __blurSize)) _blurSize)
  (:fixed
   (&optional __fixed)
   (if __fixed (setq _fixed __fixed)) _fixed)
  (:id
   (&optional __id)
   (if __id (setq _id __id)) _id)
  (:backSubOn
   (&optional __backSubOn)
   (if __backSubOn (setq _backSubOn __backSubOn)) _backSubOn)
  (:thisIsBack
   (&optional __thisIsBack)
   (if __thisIsBack (setq _thisIsBack __thisIsBack)) _thisIsBack)
  (:serialization-length
   ()
   (+
    ;; int32 _roiSize
    4
    ;; int32 _agentSize
    4
    ;; int32 _maxH
    4
    ;; int32 _minH
    4
    ;; int32 _maxS
    4
    ;; int32 _minS
    4
    ;; int32 _maxV
    4
    ;; int32 _minV
    4
    ;; bool _displayOn
    1
    ;; int32 _blurSize
    4
    ;; bool _fixed
    1
    ;; int8 _id
    1
    ;; bool _backSubOn
    1
    ;; bool _thisIsBack
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int32 _roiSize
       (write-long _roiSize s)
     ;; int32 _agentSize
       (write-long _agentSize s)
     ;; int32 _maxH
       (write-long _maxH s)
     ;; int32 _minH
       (write-long _minH s)
     ;; int32 _maxS
       (write-long _maxS s)
     ;; int32 _minS
       (write-long _minS s)
     ;; int32 _maxV
       (write-long _maxV s)
     ;; int32 _minV
       (write-long _minV s)
     ;; bool _displayOn
       (if _displayOn (write-byte -1 s) (write-byte 0 s))
     ;; int32 _blurSize
       (write-long _blurSize s)
     ;; bool _fixed
       (if _fixed (write-byte -1 s) (write-byte 0 s))
     ;; int8 _id
       (write-byte _id s)
     ;; bool _backSubOn
       (if _backSubOn (write-byte -1 s) (write-byte 0 s))
     ;; bool _thisIsBack
       (if _thisIsBack (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int32 _roiSize
     (setq _roiSize (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _agentSize
     (setq _agentSize (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _maxH
     (setq _maxH (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _minH
     (setq _minH (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _maxS
     (setq _maxS (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _minS
     (setq _minS (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _maxV
     (setq _maxV (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _minV
     (setq _minV (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; bool _displayOn
     (setq _displayOn (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; int32 _blurSize
     (setq _blurSize (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; bool _fixed
     (setq _fixed (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; int8 _id
     (setq _id (sys::peek buf ptr- :char)) (incf ptr- 1)
     (if (> _id 127) (setq _id (- _id 256)))
   ;; bool _backSubOn
     (setq _backSubOn (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _thisIsBack
     (setq _thisIsBack (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get batmag_soft::TrackerSettings :md5sum-) "9374f1f81b270934cdd4bf73c3f6a28a")
(setf (get batmag_soft::TrackerSettings :datatype-) "batmag_soft/TrackerSettings")
(setf (get batmag_soft::TrackerSettings :definition-)
      "int32 roiSize
int32 agentSize
int32 maxH
int32 minH
int32 maxS
int32 minS
int32 maxV
int32 minV
bool displayOn
int32 blurSize
bool fixed
int8 id
bool backSubOn
bool thisIsBack

")



(provide :batmag_soft/TrackerSettings "9374f1f81b270934cdd4bf73c3f6a28a")


