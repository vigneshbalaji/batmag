;; Auto-generated. Do not edit!


(when (boundp 'batmag_soft::StartStop)
  (if (not (find-package "BATMAG_SOFT"))
    (make-package "BATMAG_SOFT"))
  (shadow 'StartStop (find-package "BATMAG_SOFT")))
(unless (find-package "BATMAG_SOFT::STARTSTOP")
  (make-package "BATMAG_SOFT::STARTSTOP"))

(in-package "ROS")
;;//! \htmlinclude StartStop.msg.html


(defclass batmag_soft::StartStop
  :super ros::object
  :slots (_pressedStart _pressedStop _map _agent _imaging _control _zoom _isUS _usOnly ))

(defmethod batmag_soft::StartStop
  (:init
   (&key
    ((:pressedStart __pressedStart) nil)
    ((:pressedStop __pressedStop) nil)
    ((:map __map) "")
    ((:agent __agent) "")
    ((:imaging __imaging) "")
    ((:control __control) "")
    ((:zoom __zoom) 0.0)
    ((:isUS __isUS) (let (r) (dotimes (i 2) (push nil r)) r))
    ((:usOnly __usOnly) nil)
    )
   (send-super :init)
   (setq _pressedStart __pressedStart)
   (setq _pressedStop __pressedStop)
   (setq _map (string __map))
   (setq _agent (string __agent))
   (setq _imaging (string __imaging))
   (setq _control (string __control))
   (setq _zoom (float __zoom))
   (setq _isUS __isUS)
   (setq _usOnly __usOnly)
   self)
  (:pressedStart
   (&optional __pressedStart)
   (if __pressedStart (setq _pressedStart __pressedStart)) _pressedStart)
  (:pressedStop
   (&optional __pressedStop)
   (if __pressedStop (setq _pressedStop __pressedStop)) _pressedStop)
  (:map
   (&optional __map)
   (if __map (setq _map __map)) _map)
  (:agent
   (&optional __agent)
   (if __agent (setq _agent __agent)) _agent)
  (:imaging
   (&optional __imaging)
   (if __imaging (setq _imaging __imaging)) _imaging)
  (:control
   (&optional __control)
   (if __control (setq _control __control)) _control)
  (:zoom
   (&optional __zoom)
   (if __zoom (setq _zoom __zoom)) _zoom)
  (:isUS
   (&optional __isUS)
   (if __isUS (setq _isUS __isUS)) _isUS)
  (:usOnly
   (&optional __usOnly)
   (if __usOnly (setq _usOnly __usOnly)) _usOnly)
  (:serialization-length
   ()
   (+
    ;; bool _pressedStart
    1
    ;; bool _pressedStop
    1
    ;; string _map
    4 (length _map)
    ;; string _agent
    4 (length _agent)
    ;; string _imaging
    4 (length _imaging)
    ;; string _control
    4 (length _control)
    ;; float64 _zoom
    8
    ;; bool[2] _isUS
    (* 1    2)
    ;; bool _usOnly
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _pressedStart
       (if _pressedStart (write-byte -1 s) (write-byte 0 s))
     ;; bool _pressedStop
       (if _pressedStop (write-byte -1 s) (write-byte 0 s))
     ;; string _map
       (write-long (length _map) s) (princ _map s)
     ;; string _agent
       (write-long (length _agent) s) (princ _agent s)
     ;; string _imaging
       (write-long (length _imaging) s) (princ _imaging s)
     ;; string _control
       (write-long (length _control) s) (princ _control s)
     ;; float64 _zoom
       (sys::poke _zoom (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; bool[2] _isUS
     (dotimes (i 2)
       (if (elt _isUS i) (write-byte -1 s) (write-byte 0 s))
       )
     ;; bool _usOnly
       (if _usOnly (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _pressedStart
     (setq _pressedStart (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _pressedStop
     (setq _pressedStop (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; string _map
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _map (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _agent
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _agent (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _imaging
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _imaging (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _control
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _control (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; float64 _zoom
     (setq _zoom (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; bool[2] _isUS
   (dotimes (i (length _isUS))
     (setf (elt _isUS i) (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
     )
   ;; bool _usOnly
     (setq _usOnly (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get batmag_soft::StartStop :md5sum-) "fa1e23befc8b89be7ba72b2e18201c24")
(setf (get batmag_soft::StartStop :datatype-) "batmag_soft/StartStop")
(setf (get batmag_soft::StartStop :definition-)
      "bool pressedStart
bool pressedStop
string map
string agent
string imaging
string control
float64 zoom


bool[2] isUS
bool usOnly

")



(provide :batmag_soft/StartStop "fa1e23befc8b89be7ba72b2e18201c24")


