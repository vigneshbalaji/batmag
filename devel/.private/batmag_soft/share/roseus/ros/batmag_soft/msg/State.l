;; Auto-generated. Do not edit!


(when (boundp 'batmag_soft::State)
  (if (not (find-package "BATMAG_SOFT"))
    (make-package "BATMAG_SOFT"))
  (shadow 'State (find-package "BATMAG_SOFT")))
(unless (find-package "BATMAG_SOFT::STATE")
  (make-package "BATMAG_SOFT::STATE"))

(in-package "ROS")
;;//! \htmlinclude State.msg.html


(defclass batmag_soft::State
  :super ros::object
  :slots (_state ))

(defmethod batmag_soft::State
  (:init
   (&key
    ((:state __state) (make-array 6 :initial-element 0.0 :element-type :float))
    )
   (send-super :init)
   (setq _state __state)
   self)
  (:state
   (&optional __state)
   (if __state (setq _state __state)) _state)
  (:serialization-length
   ()
   (+
    ;; float64[6] _state
    (* 8    6)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64[6] _state
     (dotimes (i 6)
       (sys::poke (elt _state i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64[6] _state
   (dotimes (i (length _state))
     (setf (elt _state i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     )
   ;;
   self)
  )

(setf (get batmag_soft::State :md5sum-) "8ce282c4e012701c85e6ebf3255dfc96")
(setf (get batmag_soft::State :datatype-) "batmag_soft/State")
(setf (get batmag_soft::State :definition-)
      "float64[6] state

")



(provide :batmag_soft/State "8ce282c4e012701c85e6ebf3255dfc96")


