// Auto-generated. Do not edit!

// (in-package batmag_soft.srv)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------


//-----------------------------------------------------------

class SetInitParametersRequest {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.value = null;
      this.filled = null;
    }
    else {
      if (initObj.hasOwnProperty('value')) {
        this.value = initObj.value
      }
      else {
        this.value = new Array(9).fill(0);
      }
      if (initObj.hasOwnProperty('filled')) {
        this.filled = initObj.filled
      }
      else {
        this.filled = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type SetInitParametersRequest
    // Check that the constant length array field [value] has the right length
    if (obj.value.length !== 9) {
      throw new Error('Unable to serialize array field value - length must be 9')
    }
    // Serialize message field [value]
    bufferOffset = _arraySerializer.float64(obj.value, buffer, bufferOffset, 9);
    // Serialize message field [filled]
    bufferOffset = _serializer.int32(obj.filled, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type SetInitParametersRequest
    let len;
    let data = new SetInitParametersRequest(null);
    // Deserialize message field [value]
    data.value = _arrayDeserializer.float64(buffer, bufferOffset, 9)
    // Deserialize message field [filled]
    data.filled = _deserializer.int32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 76;
  }

  static datatype() {
    // Returns string type for a service object
    return 'batmag_soft/SetInitParametersRequest';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'b1f822e7a095af285c7b03b82d6a4b15';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float64[9] value
    int32 filled
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new SetInitParametersRequest(null);
    if (msg.value !== undefined) {
      resolved.value = msg.value;
    }
    else {
      resolved.value = new Array(9).fill(0)
    }

    if (msg.filled !== undefined) {
      resolved.filled = msg.filled;
    }
    else {
      resolved.filled = 0
    }

    return resolved;
    }
};

class SetInitParametersResponse {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.done = null;
    }
    else {
      if (initObj.hasOwnProperty('done')) {
        this.done = initObj.done
      }
      else {
        this.done = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type SetInitParametersResponse
    // Serialize message field [done]
    bufferOffset = _serializer.bool(obj.done, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type SetInitParametersResponse
    let len;
    let data = new SetInitParametersResponse(null);
    // Deserialize message field [done]
    data.done = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 1;
  }

  static datatype() {
    // Returns string type for a service object
    return 'batmag_soft/SetInitParametersResponse';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '89bb254424e4cffedbf494e7b0ddbfea';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    bool done
    
    
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new SetInitParametersResponse(null);
    if (msg.done !== undefined) {
      resolved.done = msg.done;
    }
    else {
      resolved.done = false
    }

    return resolved;
    }
};

module.exports = {
  Request: SetInitParametersRequest,
  Response: SetInitParametersResponse,
  md5sum() { return '90323ad5a7dc29f5397a78bc8cec0d0d'; },
  datatype() { return 'batmag_soft/SetInitParameters'; }
};
