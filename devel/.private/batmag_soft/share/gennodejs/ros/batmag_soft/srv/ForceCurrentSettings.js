// Auto-generated. Do not edit!

// (in-package batmag_soft.srv)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------


//-----------------------------------------------------------

class ForceCurrentSettingsRequest {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.isSetRequest = null;
      this.mur = null;
      this.radius = null;
      this.br = null;
    }
    else {
      if (initObj.hasOwnProperty('isSetRequest')) {
        this.isSetRequest = initObj.isSetRequest
      }
      else {
        this.isSetRequest = false;
      }
      if (initObj.hasOwnProperty('mur')) {
        this.mur = initObj.mur
      }
      else {
        this.mur = [];
      }
      if (initObj.hasOwnProperty('radius')) {
        this.radius = initObj.radius
      }
      else {
        this.radius = [];
      }
      if (initObj.hasOwnProperty('br')) {
        this.br = initObj.br
      }
      else {
        this.br = [];
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type ForceCurrentSettingsRequest
    // Serialize message field [isSetRequest]
    bufferOffset = _serializer.bool(obj.isSetRequest, buffer, bufferOffset);
    // Serialize message field [mur]
    bufferOffset = _arraySerializer.float64(obj.mur, buffer, bufferOffset, null);
    // Serialize message field [radius]
    bufferOffset = _arraySerializer.float64(obj.radius, buffer, bufferOffset, null);
    // Serialize message field [br]
    bufferOffset = _arraySerializer.float64(obj.br, buffer, bufferOffset, null);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type ForceCurrentSettingsRequest
    let len;
    let data = new ForceCurrentSettingsRequest(null);
    // Deserialize message field [isSetRequest]
    data.isSetRequest = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [mur]
    data.mur = _arrayDeserializer.float64(buffer, bufferOffset, null)
    // Deserialize message field [radius]
    data.radius = _arrayDeserializer.float64(buffer, bufferOffset, null)
    // Deserialize message field [br]
    data.br = _arrayDeserializer.float64(buffer, bufferOffset, null)
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += 8 * object.mur.length;
    length += 8 * object.radius.length;
    length += 8 * object.br.length;
    return length + 13;
  }

  static datatype() {
    // Returns string type for a service object
    return 'batmag_soft/ForceCurrentSettingsRequest';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '916529d7c78d8376f2d940c06ce8e5e1';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    bool isSetRequest
    
    float64[] mur
    float64[] radius
    float64[] br
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new ForceCurrentSettingsRequest(null);
    if (msg.isSetRequest !== undefined) {
      resolved.isSetRequest = msg.isSetRequest;
    }
    else {
      resolved.isSetRequest = false
    }

    if (msg.mur !== undefined) {
      resolved.mur = msg.mur;
    }
    else {
      resolved.mur = []
    }

    if (msg.radius !== undefined) {
      resolved.radius = msg.radius;
    }
    else {
      resolved.radius = []
    }

    if (msg.br !== undefined) {
      resolved.br = msg.br;
    }
    else {
      resolved.br = []
    }

    return resolved;
    }
};

class ForceCurrentSettingsResponse {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.mur = null;
      this.radius = null;
      this.br = null;
    }
    else {
      if (initObj.hasOwnProperty('mur')) {
        this.mur = initObj.mur
      }
      else {
        this.mur = [];
      }
      if (initObj.hasOwnProperty('radius')) {
        this.radius = initObj.radius
      }
      else {
        this.radius = [];
      }
      if (initObj.hasOwnProperty('br')) {
        this.br = initObj.br
      }
      else {
        this.br = [];
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type ForceCurrentSettingsResponse
    // Serialize message field [mur]
    bufferOffset = _arraySerializer.float64(obj.mur, buffer, bufferOffset, null);
    // Serialize message field [radius]
    bufferOffset = _arraySerializer.float64(obj.radius, buffer, bufferOffset, null);
    // Serialize message field [br]
    bufferOffset = _arraySerializer.float64(obj.br, buffer, bufferOffset, null);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type ForceCurrentSettingsResponse
    let len;
    let data = new ForceCurrentSettingsResponse(null);
    // Deserialize message field [mur]
    data.mur = _arrayDeserializer.float64(buffer, bufferOffset, null)
    // Deserialize message field [radius]
    data.radius = _arrayDeserializer.float64(buffer, bufferOffset, null)
    // Deserialize message field [br]
    data.br = _arrayDeserializer.float64(buffer, bufferOffset, null)
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += 8 * object.mur.length;
    length += 8 * object.radius.length;
    length += 8 * object.br.length;
    return length + 12;
  }

  static datatype() {
    // Returns string type for a service object
    return 'batmag_soft/ForceCurrentSettingsResponse';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'af1e9bc9d6c01631f7b71116ee9611a7';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float64[] mur
    float64[] radius
    float64[] br
    
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new ForceCurrentSettingsResponse(null);
    if (msg.mur !== undefined) {
      resolved.mur = msg.mur;
    }
    else {
      resolved.mur = []
    }

    if (msg.radius !== undefined) {
      resolved.radius = msg.radius;
    }
    else {
      resolved.radius = []
    }

    if (msg.br !== undefined) {
      resolved.br = msg.br;
    }
    else {
      resolved.br = []
    }

    return resolved;
    }
};

module.exports = {
  Request: ForceCurrentSettingsRequest,
  Response: ForceCurrentSettingsResponse,
  md5sum() { return '79cfb3942928693d62410e06660df8ac'; },
  datatype() { return 'batmag_soft/ForceCurrentSettings'; }
};
