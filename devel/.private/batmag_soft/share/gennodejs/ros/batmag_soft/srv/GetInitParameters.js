// Auto-generated. Do not edit!

// (in-package batmag_soft.srv)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------


//-----------------------------------------------------------

class GetInitParametersRequest {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.ctrl = null;
      this.agent = null;
    }
    else {
      if (initObj.hasOwnProperty('ctrl')) {
        this.ctrl = initObj.ctrl
      }
      else {
        this.ctrl = '';
      }
      if (initObj.hasOwnProperty('agent')) {
        this.agent = initObj.agent
      }
      else {
        this.agent = '';
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type GetInitParametersRequest
    // Serialize message field [ctrl]
    bufferOffset = _serializer.string(obj.ctrl, buffer, bufferOffset);
    // Serialize message field [agent]
    bufferOffset = _serializer.string(obj.agent, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type GetInitParametersRequest
    let len;
    let data = new GetInitParametersRequest(null);
    // Deserialize message field [ctrl]
    data.ctrl = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [agent]
    data.agent = _deserializer.string(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += object.ctrl.length;
    length += object.agent.length;
    return length + 8;
  }

  static datatype() {
    // Returns string type for a service object
    return 'batmag_soft/GetInitParametersRequest';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '7487f981b27c284c08237e6b6b5fe73a';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    string ctrl
    string agent
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new GetInitParametersRequest(null);
    if (msg.ctrl !== undefined) {
      resolved.ctrl = msg.ctrl;
    }
    else {
      resolved.ctrl = ''
    }

    if (msg.agent !== undefined) {
      resolved.agent = msg.agent;
    }
    else {
      resolved.agent = ''
    }

    return resolved;
    }
};

class GetInitParametersResponse {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.success = null;
      this.value = null;
      this.min = null;
      this.max = null;
      this.names = null;
    }
    else {
      if (initObj.hasOwnProperty('success')) {
        this.success = initObj.success
      }
      else {
        this.success = false;
      }
      if (initObj.hasOwnProperty('value')) {
        this.value = initObj.value
      }
      else {
        this.value = new Array(9).fill(0);
      }
      if (initObj.hasOwnProperty('min')) {
        this.min = initObj.min
      }
      else {
        this.min = new Array(9).fill(0);
      }
      if (initObj.hasOwnProperty('max')) {
        this.max = initObj.max
      }
      else {
        this.max = new Array(9).fill(0);
      }
      if (initObj.hasOwnProperty('names')) {
        this.names = initObj.names
      }
      else {
        this.names = new Array(9).fill(0);
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type GetInitParametersResponse
    // Serialize message field [success]
    bufferOffset = _serializer.bool(obj.success, buffer, bufferOffset);
    // Check that the constant length array field [value] has the right length
    if (obj.value.length !== 9) {
      throw new Error('Unable to serialize array field value - length must be 9')
    }
    // Serialize message field [value]
    bufferOffset = _arraySerializer.float64(obj.value, buffer, bufferOffset, 9);
    // Check that the constant length array field [min] has the right length
    if (obj.min.length !== 9) {
      throw new Error('Unable to serialize array field min - length must be 9')
    }
    // Serialize message field [min]
    bufferOffset = _arraySerializer.float64(obj.min, buffer, bufferOffset, 9);
    // Check that the constant length array field [max] has the right length
    if (obj.max.length !== 9) {
      throw new Error('Unable to serialize array field max - length must be 9')
    }
    // Serialize message field [max]
    bufferOffset = _arraySerializer.float64(obj.max, buffer, bufferOffset, 9);
    // Check that the constant length array field [names] has the right length
    if (obj.names.length !== 9) {
      throw new Error('Unable to serialize array field names - length must be 9')
    }
    // Serialize message field [names]
    bufferOffset = _arraySerializer.string(obj.names, buffer, bufferOffset, 9);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type GetInitParametersResponse
    let len;
    let data = new GetInitParametersResponse(null);
    // Deserialize message field [success]
    data.success = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [value]
    data.value = _arrayDeserializer.float64(buffer, bufferOffset, 9)
    // Deserialize message field [min]
    data.min = _arrayDeserializer.float64(buffer, bufferOffset, 9)
    // Deserialize message field [max]
    data.max = _arrayDeserializer.float64(buffer, bufferOffset, 9)
    // Deserialize message field [names]
    data.names = _arrayDeserializer.string(buffer, bufferOffset, 9)
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    object.names.forEach((val) => {
      length += 4 + val.length;
    });
    return length + 217;
  }

  static datatype() {
    // Returns string type for a service object
    return 'batmag_soft/GetInitParametersResponse';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '3319200937126dd91c4e01910b156533';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    bool success
    float64[9] value
    float64[9] min
    float64[9] max
    string[9] names
    
    
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new GetInitParametersResponse(null);
    if (msg.success !== undefined) {
      resolved.success = msg.success;
    }
    else {
      resolved.success = false
    }

    if (msg.value !== undefined) {
      resolved.value = msg.value;
    }
    else {
      resolved.value = new Array(9).fill(0)
    }

    if (msg.min !== undefined) {
      resolved.min = msg.min;
    }
    else {
      resolved.min = new Array(9).fill(0)
    }

    if (msg.max !== undefined) {
      resolved.max = msg.max;
    }
    else {
      resolved.max = new Array(9).fill(0)
    }

    if (msg.names !== undefined) {
      resolved.names = msg.names;
    }
    else {
      resolved.names = new Array(9).fill(0)
    }

    return resolved;
    }
};

module.exports = {
  Request: GetInitParametersRequest,
  Response: GetInitParametersResponse,
  md5sum() { return 'cde8ea7816229f9bd2f4733f008c24de'; },
  datatype() { return 'batmag_soft/GetInitParameters'; }
};
