// Auto-generated. Do not edit!

// (in-package batmag_soft.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class TrackerSettings {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.roiSize = null;
      this.agentSize = null;
      this.maxH = null;
      this.minH = null;
      this.maxS = null;
      this.minS = null;
      this.maxV = null;
      this.minV = null;
      this.displayOn = null;
      this.blurSize = null;
      this.fixed = null;
      this.id = null;
      this.backSubOn = null;
      this.thisIsBack = null;
    }
    else {
      if (initObj.hasOwnProperty('roiSize')) {
        this.roiSize = initObj.roiSize
      }
      else {
        this.roiSize = 0;
      }
      if (initObj.hasOwnProperty('agentSize')) {
        this.agentSize = initObj.agentSize
      }
      else {
        this.agentSize = 0;
      }
      if (initObj.hasOwnProperty('maxH')) {
        this.maxH = initObj.maxH
      }
      else {
        this.maxH = 0;
      }
      if (initObj.hasOwnProperty('minH')) {
        this.minH = initObj.minH
      }
      else {
        this.minH = 0;
      }
      if (initObj.hasOwnProperty('maxS')) {
        this.maxS = initObj.maxS
      }
      else {
        this.maxS = 0;
      }
      if (initObj.hasOwnProperty('minS')) {
        this.minS = initObj.minS
      }
      else {
        this.minS = 0;
      }
      if (initObj.hasOwnProperty('maxV')) {
        this.maxV = initObj.maxV
      }
      else {
        this.maxV = 0;
      }
      if (initObj.hasOwnProperty('minV')) {
        this.minV = initObj.minV
      }
      else {
        this.minV = 0;
      }
      if (initObj.hasOwnProperty('displayOn')) {
        this.displayOn = initObj.displayOn
      }
      else {
        this.displayOn = false;
      }
      if (initObj.hasOwnProperty('blurSize')) {
        this.blurSize = initObj.blurSize
      }
      else {
        this.blurSize = 0;
      }
      if (initObj.hasOwnProperty('fixed')) {
        this.fixed = initObj.fixed
      }
      else {
        this.fixed = false;
      }
      if (initObj.hasOwnProperty('id')) {
        this.id = initObj.id
      }
      else {
        this.id = 0;
      }
      if (initObj.hasOwnProperty('backSubOn')) {
        this.backSubOn = initObj.backSubOn
      }
      else {
        this.backSubOn = false;
      }
      if (initObj.hasOwnProperty('thisIsBack')) {
        this.thisIsBack = initObj.thisIsBack
      }
      else {
        this.thisIsBack = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type TrackerSettings
    // Serialize message field [roiSize]
    bufferOffset = _serializer.int32(obj.roiSize, buffer, bufferOffset);
    // Serialize message field [agentSize]
    bufferOffset = _serializer.int32(obj.agentSize, buffer, bufferOffset);
    // Serialize message field [maxH]
    bufferOffset = _serializer.int32(obj.maxH, buffer, bufferOffset);
    // Serialize message field [minH]
    bufferOffset = _serializer.int32(obj.minH, buffer, bufferOffset);
    // Serialize message field [maxS]
    bufferOffset = _serializer.int32(obj.maxS, buffer, bufferOffset);
    // Serialize message field [minS]
    bufferOffset = _serializer.int32(obj.minS, buffer, bufferOffset);
    // Serialize message field [maxV]
    bufferOffset = _serializer.int32(obj.maxV, buffer, bufferOffset);
    // Serialize message field [minV]
    bufferOffset = _serializer.int32(obj.minV, buffer, bufferOffset);
    // Serialize message field [displayOn]
    bufferOffset = _serializer.bool(obj.displayOn, buffer, bufferOffset);
    // Serialize message field [blurSize]
    bufferOffset = _serializer.int32(obj.blurSize, buffer, bufferOffset);
    // Serialize message field [fixed]
    bufferOffset = _serializer.bool(obj.fixed, buffer, bufferOffset);
    // Serialize message field [id]
    bufferOffset = _serializer.int8(obj.id, buffer, bufferOffset);
    // Serialize message field [backSubOn]
    bufferOffset = _serializer.bool(obj.backSubOn, buffer, bufferOffset);
    // Serialize message field [thisIsBack]
    bufferOffset = _serializer.bool(obj.thisIsBack, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type TrackerSettings
    let len;
    let data = new TrackerSettings(null);
    // Deserialize message field [roiSize]
    data.roiSize = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [agentSize]
    data.agentSize = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [maxH]
    data.maxH = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [minH]
    data.minH = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [maxS]
    data.maxS = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [minS]
    data.minS = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [maxV]
    data.maxV = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [minV]
    data.minV = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [displayOn]
    data.displayOn = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [blurSize]
    data.blurSize = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [fixed]
    data.fixed = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [id]
    data.id = _deserializer.int8(buffer, bufferOffset);
    // Deserialize message field [backSubOn]
    data.backSubOn = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [thisIsBack]
    data.thisIsBack = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 41;
  }

  static datatype() {
    // Returns string type for a message object
    return 'batmag_soft/TrackerSettings';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '9374f1f81b270934cdd4bf73c3f6a28a';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    int32 roiSize
    int32 agentSize
    int32 maxH
    int32 minH
    int32 maxS
    int32 minS
    int32 maxV
    int32 minV
    bool displayOn
    int32 blurSize
    bool fixed
    int8 id
    bool backSubOn
    bool thisIsBack
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new TrackerSettings(null);
    if (msg.roiSize !== undefined) {
      resolved.roiSize = msg.roiSize;
    }
    else {
      resolved.roiSize = 0
    }

    if (msg.agentSize !== undefined) {
      resolved.agentSize = msg.agentSize;
    }
    else {
      resolved.agentSize = 0
    }

    if (msg.maxH !== undefined) {
      resolved.maxH = msg.maxH;
    }
    else {
      resolved.maxH = 0
    }

    if (msg.minH !== undefined) {
      resolved.minH = msg.minH;
    }
    else {
      resolved.minH = 0
    }

    if (msg.maxS !== undefined) {
      resolved.maxS = msg.maxS;
    }
    else {
      resolved.maxS = 0
    }

    if (msg.minS !== undefined) {
      resolved.minS = msg.minS;
    }
    else {
      resolved.minS = 0
    }

    if (msg.maxV !== undefined) {
      resolved.maxV = msg.maxV;
    }
    else {
      resolved.maxV = 0
    }

    if (msg.minV !== undefined) {
      resolved.minV = msg.minV;
    }
    else {
      resolved.minV = 0
    }

    if (msg.displayOn !== undefined) {
      resolved.displayOn = msg.displayOn;
    }
    else {
      resolved.displayOn = false
    }

    if (msg.blurSize !== undefined) {
      resolved.blurSize = msg.blurSize;
    }
    else {
      resolved.blurSize = 0
    }

    if (msg.fixed !== undefined) {
      resolved.fixed = msg.fixed;
    }
    else {
      resolved.fixed = false
    }

    if (msg.id !== undefined) {
      resolved.id = msg.id;
    }
    else {
      resolved.id = 0
    }

    if (msg.backSubOn !== undefined) {
      resolved.backSubOn = msg.backSubOn;
    }
    else {
      resolved.backSubOn = false
    }

    if (msg.thisIsBack !== undefined) {
      resolved.thisIsBack = msg.thisIsBack;
    }
    else {
      resolved.thisIsBack = false
    }

    return resolved;
    }
};

module.exports = TrackerSettings;
