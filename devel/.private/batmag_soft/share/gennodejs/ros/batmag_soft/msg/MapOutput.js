// Auto-generated. Do not edit!

// (in-package batmag_soft.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class MapOutput {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.mapout = null;
      this.size = null;
    }
    else {
      if (initObj.hasOwnProperty('mapout')) {
        this.mapout = initObj.mapout
      }
      else {
        this.mapout = new Array(10).fill(0);
      }
      if (initObj.hasOwnProperty('size')) {
        this.size = initObj.size
      }
      else {
        this.size = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type MapOutput
    // Check that the constant length array field [mapout] has the right length
    if (obj.mapout.length !== 10) {
      throw new Error('Unable to serialize array field mapout - length must be 10')
    }
    // Serialize message field [mapout]
    bufferOffset = _arraySerializer.float64(obj.mapout, buffer, bufferOffset, 10);
    // Serialize message field [size]
    bufferOffset = _serializer.int64(obj.size, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type MapOutput
    let len;
    let data = new MapOutput(null);
    // Deserialize message field [mapout]
    data.mapout = _arrayDeserializer.float64(buffer, bufferOffset, 10)
    // Deserialize message field [size]
    data.size = _deserializer.int64(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 88;
  }

  static datatype() {
    // Returns string type for a message object
    return 'batmag_soft/MapOutput';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '8796c057cb44e7e6236c77ac997c7757';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float64[10] mapout
    int64 size
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new MapOutput(null);
    if (msg.mapout !== undefined) {
      resolved.mapout = msg.mapout;
    }
    else {
      resolved.mapout = new Array(10).fill(0)
    }

    if (msg.size !== undefined) {
      resolved.size = msg.size;
    }
    else {
      resolved.size = 0
    }

    return resolved;
    }
};

module.exports = MapOutput;
