// Auto-generated. Do not edit!

// (in-package batmag_soft.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class GUIpreferences {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.recordOn = null;
      this.ctrlEn = null;
      this.debugEn = null;
      this.gravityEn = null;
      this.gridOn = null;
      this.PMposition = null;
      this.PMenable = null;
      this.omegaCtrl = null;
    }
    else {
      if (initObj.hasOwnProperty('recordOn')) {
        this.recordOn = initObj.recordOn
      }
      else {
        this.recordOn = false;
      }
      if (initObj.hasOwnProperty('ctrlEn')) {
        this.ctrlEn = initObj.ctrlEn
      }
      else {
        this.ctrlEn = false;
      }
      if (initObj.hasOwnProperty('debugEn')) {
        this.debugEn = initObj.debugEn
      }
      else {
        this.debugEn = false;
      }
      if (initObj.hasOwnProperty('gravityEn')) {
        this.gravityEn = initObj.gravityEn
      }
      else {
        this.gravityEn = false;
      }
      if (initObj.hasOwnProperty('gridOn')) {
        this.gridOn = initObj.gridOn
      }
      else {
        this.gridOn = false;
      }
      if (initObj.hasOwnProperty('PMposition')) {
        this.PMposition = initObj.PMposition
      }
      else {
        this.PMposition = 0.0;
      }
      if (initObj.hasOwnProperty('PMenable')) {
        this.PMenable = initObj.PMenable
      }
      else {
        this.PMenable = false;
      }
      if (initObj.hasOwnProperty('omegaCtrl')) {
        this.omegaCtrl = initObj.omegaCtrl
      }
      else {
        this.omegaCtrl = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type GUIpreferences
    // Serialize message field [recordOn]
    bufferOffset = _serializer.bool(obj.recordOn, buffer, bufferOffset);
    // Serialize message field [ctrlEn]
    bufferOffset = _serializer.bool(obj.ctrlEn, buffer, bufferOffset);
    // Serialize message field [debugEn]
    bufferOffset = _serializer.bool(obj.debugEn, buffer, bufferOffset);
    // Serialize message field [gravityEn]
    bufferOffset = _serializer.bool(obj.gravityEn, buffer, bufferOffset);
    // Serialize message field [gridOn]
    bufferOffset = _serializer.bool(obj.gridOn, buffer, bufferOffset);
    // Serialize message field [PMposition]
    bufferOffset = _serializer.float64(obj.PMposition, buffer, bufferOffset);
    // Serialize message field [PMenable]
    bufferOffset = _serializer.bool(obj.PMenable, buffer, bufferOffset);
    // Serialize message field [omegaCtrl]
    bufferOffset = _serializer.bool(obj.omegaCtrl, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type GUIpreferences
    let len;
    let data = new GUIpreferences(null);
    // Deserialize message field [recordOn]
    data.recordOn = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [ctrlEn]
    data.ctrlEn = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [debugEn]
    data.debugEn = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [gravityEn]
    data.gravityEn = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [gridOn]
    data.gridOn = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [PMposition]
    data.PMposition = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [PMenable]
    data.PMenable = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [omegaCtrl]
    data.omegaCtrl = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 15;
  }

  static datatype() {
    // Returns string type for a message object
    return 'batmag_soft/GUIpreferences';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '30cc44c0758499a2509ba85095012fbb';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    bool recordOn
    bool ctrlEn
    bool debugEn
    bool gravityEn
    bool gridOn
    float64 PMposition
    bool PMenable
    bool omegaCtrl
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new GUIpreferences(null);
    if (msg.recordOn !== undefined) {
      resolved.recordOn = msg.recordOn;
    }
    else {
      resolved.recordOn = false
    }

    if (msg.ctrlEn !== undefined) {
      resolved.ctrlEn = msg.ctrlEn;
    }
    else {
      resolved.ctrlEn = false
    }

    if (msg.debugEn !== undefined) {
      resolved.debugEn = msg.debugEn;
    }
    else {
      resolved.debugEn = false
    }

    if (msg.gravityEn !== undefined) {
      resolved.gravityEn = msg.gravityEn;
    }
    else {
      resolved.gravityEn = false
    }

    if (msg.gridOn !== undefined) {
      resolved.gridOn = msg.gridOn;
    }
    else {
      resolved.gridOn = false
    }

    if (msg.PMposition !== undefined) {
      resolved.PMposition = msg.PMposition;
    }
    else {
      resolved.PMposition = 0.0
    }

    if (msg.PMenable !== undefined) {
      resolved.PMenable = msg.PMenable;
    }
    else {
      resolved.PMenable = false
    }

    if (msg.omegaCtrl !== undefined) {
      resolved.omegaCtrl = msg.omegaCtrl;
    }
    else {
      resolved.omegaCtrl = false
    }

    return resolved;
    }
};

module.exports = GUIpreferences;
