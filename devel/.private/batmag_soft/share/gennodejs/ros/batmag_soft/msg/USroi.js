// Auto-generated. Do not edit!

// (in-package batmag_soft.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class USroi {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.width = null;
      this.height = null;
      this.topleftx = null;
      this.toplefty = null;
    }
    else {
      if (initObj.hasOwnProperty('width')) {
        this.width = initObj.width
      }
      else {
        this.width = 0;
      }
      if (initObj.hasOwnProperty('height')) {
        this.height = initObj.height
      }
      else {
        this.height = 0;
      }
      if (initObj.hasOwnProperty('topleftx')) {
        this.topleftx = initObj.topleftx
      }
      else {
        this.topleftx = 0;
      }
      if (initObj.hasOwnProperty('toplefty')) {
        this.toplefty = initObj.toplefty
      }
      else {
        this.toplefty = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type USroi
    // Serialize message field [width]
    bufferOffset = _serializer.int32(obj.width, buffer, bufferOffset);
    // Serialize message field [height]
    bufferOffset = _serializer.int32(obj.height, buffer, bufferOffset);
    // Serialize message field [topleftx]
    bufferOffset = _serializer.int32(obj.topleftx, buffer, bufferOffset);
    // Serialize message field [toplefty]
    bufferOffset = _serializer.int32(obj.toplefty, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type USroi
    let len;
    let data = new USroi(null);
    // Deserialize message field [width]
    data.width = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [height]
    data.height = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [topleftx]
    data.topleftx = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [toplefty]
    data.toplefty = _deserializer.int32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 16;
  }

  static datatype() {
    // Returns string type for a message object
    return 'batmag_soft/USroi';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'e93dbc2b5418de3cd63fd0b87d53412f';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    int32 width
    int32 height
    int32 topleftx
    int32 toplefty
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new USroi(null);
    if (msg.width !== undefined) {
      resolved.width = msg.width;
    }
    else {
      resolved.width = 0
    }

    if (msg.height !== undefined) {
      resolved.height = msg.height;
    }
    else {
      resolved.height = 0
    }

    if (msg.topleftx !== undefined) {
      resolved.topleftx = msg.topleftx;
    }
    else {
      resolved.topleftx = 0
    }

    if (msg.toplefty !== undefined) {
      resolved.toplefty = msg.toplefty;
    }
    else {
      resolved.toplefty = 0
    }

    return resolved;
    }
};

module.exports = USroi;
