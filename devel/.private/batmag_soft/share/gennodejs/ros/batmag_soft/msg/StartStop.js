// Auto-generated. Do not edit!

// (in-package batmag_soft.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class StartStop {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.pressedStart = null;
      this.pressedStop = null;
      this.map = null;
      this.agent = null;
      this.imaging = null;
      this.control = null;
      this.zoom = null;
      this.isUS = null;
      this.usOnly = null;
    }
    else {
      if (initObj.hasOwnProperty('pressedStart')) {
        this.pressedStart = initObj.pressedStart
      }
      else {
        this.pressedStart = false;
      }
      if (initObj.hasOwnProperty('pressedStop')) {
        this.pressedStop = initObj.pressedStop
      }
      else {
        this.pressedStop = false;
      }
      if (initObj.hasOwnProperty('map')) {
        this.map = initObj.map
      }
      else {
        this.map = '';
      }
      if (initObj.hasOwnProperty('agent')) {
        this.agent = initObj.agent
      }
      else {
        this.agent = '';
      }
      if (initObj.hasOwnProperty('imaging')) {
        this.imaging = initObj.imaging
      }
      else {
        this.imaging = '';
      }
      if (initObj.hasOwnProperty('control')) {
        this.control = initObj.control
      }
      else {
        this.control = '';
      }
      if (initObj.hasOwnProperty('zoom')) {
        this.zoom = initObj.zoom
      }
      else {
        this.zoom = 0.0;
      }
      if (initObj.hasOwnProperty('isUS')) {
        this.isUS = initObj.isUS
      }
      else {
        this.isUS = new Array(2).fill(0);
      }
      if (initObj.hasOwnProperty('usOnly')) {
        this.usOnly = initObj.usOnly
      }
      else {
        this.usOnly = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type StartStop
    // Serialize message field [pressedStart]
    bufferOffset = _serializer.bool(obj.pressedStart, buffer, bufferOffset);
    // Serialize message field [pressedStop]
    bufferOffset = _serializer.bool(obj.pressedStop, buffer, bufferOffset);
    // Serialize message field [map]
    bufferOffset = _serializer.string(obj.map, buffer, bufferOffset);
    // Serialize message field [agent]
    bufferOffset = _serializer.string(obj.agent, buffer, bufferOffset);
    // Serialize message field [imaging]
    bufferOffset = _serializer.string(obj.imaging, buffer, bufferOffset);
    // Serialize message field [control]
    bufferOffset = _serializer.string(obj.control, buffer, bufferOffset);
    // Serialize message field [zoom]
    bufferOffset = _serializer.float64(obj.zoom, buffer, bufferOffset);
    // Check that the constant length array field [isUS] has the right length
    if (obj.isUS.length !== 2) {
      throw new Error('Unable to serialize array field isUS - length must be 2')
    }
    // Serialize message field [isUS]
    bufferOffset = _arraySerializer.bool(obj.isUS, buffer, bufferOffset, 2);
    // Serialize message field [usOnly]
    bufferOffset = _serializer.bool(obj.usOnly, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type StartStop
    let len;
    let data = new StartStop(null);
    // Deserialize message field [pressedStart]
    data.pressedStart = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [pressedStop]
    data.pressedStop = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [map]
    data.map = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [agent]
    data.agent = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [imaging]
    data.imaging = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [control]
    data.control = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [zoom]
    data.zoom = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [isUS]
    data.isUS = _arrayDeserializer.bool(buffer, bufferOffset, 2)
    // Deserialize message field [usOnly]
    data.usOnly = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += object.map.length;
    length += object.agent.length;
    length += object.imaging.length;
    length += object.control.length;
    return length + 29;
  }

  static datatype() {
    // Returns string type for a message object
    return 'batmag_soft/StartStop';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'fa1e23befc8b89be7ba72b2e18201c24';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    bool pressedStart
    bool pressedStop
    string map
    string agent
    string imaging
    string control
    float64 zoom
    
    
    bool[2] isUS
    bool usOnly
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new StartStop(null);
    if (msg.pressedStart !== undefined) {
      resolved.pressedStart = msg.pressedStart;
    }
    else {
      resolved.pressedStart = false
    }

    if (msg.pressedStop !== undefined) {
      resolved.pressedStop = msg.pressedStop;
    }
    else {
      resolved.pressedStop = false
    }

    if (msg.map !== undefined) {
      resolved.map = msg.map;
    }
    else {
      resolved.map = ''
    }

    if (msg.agent !== undefined) {
      resolved.agent = msg.agent;
    }
    else {
      resolved.agent = ''
    }

    if (msg.imaging !== undefined) {
      resolved.imaging = msg.imaging;
    }
    else {
      resolved.imaging = ''
    }

    if (msg.control !== undefined) {
      resolved.control = msg.control;
    }
    else {
      resolved.control = ''
    }

    if (msg.zoom !== undefined) {
      resolved.zoom = msg.zoom;
    }
    else {
      resolved.zoom = 0.0
    }

    if (msg.isUS !== undefined) {
      resolved.isUS = msg.isUS;
    }
    else {
      resolved.isUS = new Array(2).fill(0)
    }

    if (msg.usOnly !== undefined) {
      resolved.usOnly = msg.usOnly;
    }
    else {
      resolved.usOnly = false
    }

    return resolved;
    }
};

module.exports = StartStop;
