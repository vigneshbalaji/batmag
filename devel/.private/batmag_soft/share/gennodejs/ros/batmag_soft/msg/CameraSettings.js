// Auto-generated. Do not edit!

// (in-package batmag_soft.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class CameraSettings {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.Exposure = null;
      this.Brightness = null;
      this.Framerate = null;
      this.Gain = null;
      this.Gamma = null;
      this.Hue = null;
      this.Saturation = null;
      this.Sharpness = null;
      this.Shutter = null;
      this.wbb = null;
      this.wbr = null;
    }
    else {
      if (initObj.hasOwnProperty('Exposure')) {
        this.Exposure = initObj.Exposure
      }
      else {
        this.Exposure = 0.0;
      }
      if (initObj.hasOwnProperty('Brightness')) {
        this.Brightness = initObj.Brightness
      }
      else {
        this.Brightness = 0.0;
      }
      if (initObj.hasOwnProperty('Framerate')) {
        this.Framerate = initObj.Framerate
      }
      else {
        this.Framerate = 0.0;
      }
      if (initObj.hasOwnProperty('Gain')) {
        this.Gain = initObj.Gain
      }
      else {
        this.Gain = 0.0;
      }
      if (initObj.hasOwnProperty('Gamma')) {
        this.Gamma = initObj.Gamma
      }
      else {
        this.Gamma = 0.0;
      }
      if (initObj.hasOwnProperty('Hue')) {
        this.Hue = initObj.Hue
      }
      else {
        this.Hue = 0.0;
      }
      if (initObj.hasOwnProperty('Saturation')) {
        this.Saturation = initObj.Saturation
      }
      else {
        this.Saturation = 0.0;
      }
      if (initObj.hasOwnProperty('Sharpness')) {
        this.Sharpness = initObj.Sharpness
      }
      else {
        this.Sharpness = 0.0;
      }
      if (initObj.hasOwnProperty('Shutter')) {
        this.Shutter = initObj.Shutter
      }
      else {
        this.Shutter = 0.0;
      }
      if (initObj.hasOwnProperty('wbb')) {
        this.wbb = initObj.wbb
      }
      else {
        this.wbb = 0.0;
      }
      if (initObj.hasOwnProperty('wbr')) {
        this.wbr = initObj.wbr
      }
      else {
        this.wbr = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type CameraSettings
    // Serialize message field [Exposure]
    bufferOffset = _serializer.float32(obj.Exposure, buffer, bufferOffset);
    // Serialize message field [Brightness]
    bufferOffset = _serializer.float32(obj.Brightness, buffer, bufferOffset);
    // Serialize message field [Framerate]
    bufferOffset = _serializer.float32(obj.Framerate, buffer, bufferOffset);
    // Serialize message field [Gain]
    bufferOffset = _serializer.float32(obj.Gain, buffer, bufferOffset);
    // Serialize message field [Gamma]
    bufferOffset = _serializer.float32(obj.Gamma, buffer, bufferOffset);
    // Serialize message field [Hue]
    bufferOffset = _serializer.float32(obj.Hue, buffer, bufferOffset);
    // Serialize message field [Saturation]
    bufferOffset = _serializer.float32(obj.Saturation, buffer, bufferOffset);
    // Serialize message field [Sharpness]
    bufferOffset = _serializer.float32(obj.Sharpness, buffer, bufferOffset);
    // Serialize message field [Shutter]
    bufferOffset = _serializer.float32(obj.Shutter, buffer, bufferOffset);
    // Serialize message field [wbb]
    bufferOffset = _serializer.float32(obj.wbb, buffer, bufferOffset);
    // Serialize message field [wbr]
    bufferOffset = _serializer.float32(obj.wbr, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type CameraSettings
    let len;
    let data = new CameraSettings(null);
    // Deserialize message field [Exposure]
    data.Exposure = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [Brightness]
    data.Brightness = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [Framerate]
    data.Framerate = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [Gain]
    data.Gain = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [Gamma]
    data.Gamma = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [Hue]
    data.Hue = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [Saturation]
    data.Saturation = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [Sharpness]
    data.Sharpness = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [Shutter]
    data.Shutter = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [wbb]
    data.wbb = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [wbr]
    data.wbr = _deserializer.float32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 44;
  }

  static datatype() {
    // Returns string type for a message object
    return 'batmag_soft/CameraSettings';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '8fe8cda4f5f6a5f7062c243d4b3b5fb9';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float32 Exposure
    float32 Brightness
    float32 Framerate
    float32 Gain
    float32 Gamma
    float32 Hue
    float32 Saturation
    float32 Sharpness
    float32 Shutter
    float32 wbb
    float32 wbr
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new CameraSettings(null);
    if (msg.Exposure !== undefined) {
      resolved.Exposure = msg.Exposure;
    }
    else {
      resolved.Exposure = 0.0
    }

    if (msg.Brightness !== undefined) {
      resolved.Brightness = msg.Brightness;
    }
    else {
      resolved.Brightness = 0.0
    }

    if (msg.Framerate !== undefined) {
      resolved.Framerate = msg.Framerate;
    }
    else {
      resolved.Framerate = 0.0
    }

    if (msg.Gain !== undefined) {
      resolved.Gain = msg.Gain;
    }
    else {
      resolved.Gain = 0.0
    }

    if (msg.Gamma !== undefined) {
      resolved.Gamma = msg.Gamma;
    }
    else {
      resolved.Gamma = 0.0
    }

    if (msg.Hue !== undefined) {
      resolved.Hue = msg.Hue;
    }
    else {
      resolved.Hue = 0.0
    }

    if (msg.Saturation !== undefined) {
      resolved.Saturation = msg.Saturation;
    }
    else {
      resolved.Saturation = 0.0
    }

    if (msg.Sharpness !== undefined) {
      resolved.Sharpness = msg.Sharpness;
    }
    else {
      resolved.Sharpness = 0.0
    }

    if (msg.Shutter !== undefined) {
      resolved.Shutter = msg.Shutter;
    }
    else {
      resolved.Shutter = 0.0
    }

    if (msg.wbb !== undefined) {
      resolved.wbb = msg.wbb;
    }
    else {
      resolved.wbb = 0.0
    }

    if (msg.wbr !== undefined) {
      resolved.wbr = msg.wbr;
    }
    else {
      resolved.wbr = 0.0
    }

    return resolved;
    }
};

module.exports = CameraSettings;
