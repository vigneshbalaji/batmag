// Auto-generated. Do not edit!

// (in-package batmag_soft.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class RotatingCtrlSettings {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.freq = null;
      this.rotAmp = null;
      this.fixAmp = null;
    }
    else {
      if (initObj.hasOwnProperty('freq')) {
        this.freq = initObj.freq
      }
      else {
        this.freq = 0.0;
      }
      if (initObj.hasOwnProperty('rotAmp')) {
        this.rotAmp = initObj.rotAmp
      }
      else {
        this.rotAmp = 0.0;
      }
      if (initObj.hasOwnProperty('fixAmp')) {
        this.fixAmp = initObj.fixAmp
      }
      else {
        this.fixAmp = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type RotatingCtrlSettings
    // Serialize message field [freq]
    bufferOffset = _serializer.float64(obj.freq, buffer, bufferOffset);
    // Serialize message field [rotAmp]
    bufferOffset = _serializer.float64(obj.rotAmp, buffer, bufferOffset);
    // Serialize message field [fixAmp]
    bufferOffset = _serializer.float64(obj.fixAmp, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type RotatingCtrlSettings
    let len;
    let data = new RotatingCtrlSettings(null);
    // Deserialize message field [freq]
    data.freq = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [rotAmp]
    data.rotAmp = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [fixAmp]
    data.fixAmp = _deserializer.float64(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 24;
  }

  static datatype() {
    // Returns string type for a message object
    return 'batmag_soft/RotatingCtrlSettings';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '3f7676b3fed2bc1451d91224b6f01fe4';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float64 freq
    float64 rotAmp
    float64 fixAmp
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new RotatingCtrlSettings(null);
    if (msg.freq !== undefined) {
      resolved.freq = msg.freq;
    }
    else {
      resolved.freq = 0.0
    }

    if (msg.rotAmp !== undefined) {
      resolved.rotAmp = msg.rotAmp;
    }
    else {
      resolved.rotAmp = 0.0
    }

    if (msg.fixAmp !== undefined) {
      resolved.fixAmp = msg.fixAmp;
    }
    else {
      resolved.fixAmp = 0.0
    }

    return resolved;
    }
};

module.exports = RotatingCtrlSettings;
