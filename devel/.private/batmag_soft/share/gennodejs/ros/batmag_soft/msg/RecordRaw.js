// Auto-generated. Do not edit!

// (in-package batmag_soft.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class RecordRaw {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.recordRaw = null;
      this.recordWhere = null;
    }
    else {
      if (initObj.hasOwnProperty('recordRaw')) {
        this.recordRaw = initObj.recordRaw
      }
      else {
        this.recordRaw = false;
      }
      if (initObj.hasOwnProperty('recordWhere')) {
        this.recordWhere = initObj.recordWhere
      }
      else {
        this.recordWhere = '';
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type RecordRaw
    // Serialize message field [recordRaw]
    bufferOffset = _serializer.bool(obj.recordRaw, buffer, bufferOffset);
    // Serialize message field [recordWhere]
    bufferOffset = _serializer.string(obj.recordWhere, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type RecordRaw
    let len;
    let data = new RecordRaw(null);
    // Deserialize message field [recordRaw]
    data.recordRaw = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [recordWhere]
    data.recordWhere = _deserializer.string(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += object.recordWhere.length;
    return length + 5;
  }

  static datatype() {
    // Returns string type for a message object
    return 'batmag_soft/RecordRaw';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'dba747aebd3c39de46937fb36819f3c1';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    bool recordRaw
    string recordWhere
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new RecordRaw(null);
    if (msg.recordRaw !== undefined) {
      resolved.recordRaw = msg.recordRaw;
    }
    else {
      resolved.recordRaw = false
    }

    if (msg.recordWhere !== undefined) {
      resolved.recordWhere = msg.recordWhere;
    }
    else {
      resolved.recordWhere = ''
    }

    return resolved;
    }
};

module.exports = RecordRaw;
