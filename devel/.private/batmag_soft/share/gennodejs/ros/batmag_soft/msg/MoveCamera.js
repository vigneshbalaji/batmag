// Auto-generated. Do not edit!

// (in-package batmag_soft.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class MoveCamera {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.cameraSpeed = null;
      this.autofocusOn = null;
    }
    else {
      if (initObj.hasOwnProperty('cameraSpeed')) {
        this.cameraSpeed = initObj.cameraSpeed
      }
      else {
        this.cameraSpeed = new Array(2).fill(0);
      }
      if (initObj.hasOwnProperty('autofocusOn')) {
        this.autofocusOn = initObj.autofocusOn
      }
      else {
        this.autofocusOn = new Array(2).fill(0);
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type MoveCamera
    // Check that the constant length array field [cameraSpeed] has the right length
    if (obj.cameraSpeed.length !== 2) {
      throw new Error('Unable to serialize array field cameraSpeed - length must be 2')
    }
    // Serialize message field [cameraSpeed]
    bufferOffset = _arraySerializer.int64(obj.cameraSpeed, buffer, bufferOffset, 2);
    // Check that the constant length array field [autofocusOn] has the right length
    if (obj.autofocusOn.length !== 2) {
      throw new Error('Unable to serialize array field autofocusOn - length must be 2')
    }
    // Serialize message field [autofocusOn]
    bufferOffset = _arraySerializer.bool(obj.autofocusOn, buffer, bufferOffset, 2);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type MoveCamera
    let len;
    let data = new MoveCamera(null);
    // Deserialize message field [cameraSpeed]
    data.cameraSpeed = _arrayDeserializer.int64(buffer, bufferOffset, 2)
    // Deserialize message field [autofocusOn]
    data.autofocusOn = _arrayDeserializer.bool(buffer, bufferOffset, 2)
    return data;
  }

  static getMessageSize(object) {
    return 18;
  }

  static datatype() {
    // Returns string type for a message object
    return 'batmag_soft/MoveCamera';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '11601d08162551242a2c2c2bbcb261be';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    int64[2] cameraSpeed
    bool[2] autofocusOn
     
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new MoveCamera(null);
    if (msg.cameraSpeed !== undefined) {
      resolved.cameraSpeed = msg.cameraSpeed;
    }
    else {
      resolved.cameraSpeed = new Array(2).fill(0)
    }

    if (msg.autofocusOn !== undefined) {
      resolved.autofocusOn = msg.autofocusOn;
    }
    else {
      resolved.autofocusOn = new Array(2).fill(0)
    }

    return resolved;
    }
};

module.exports = MoveCamera;
