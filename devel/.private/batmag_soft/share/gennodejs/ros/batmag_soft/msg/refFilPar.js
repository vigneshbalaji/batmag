// Auto-generated. Do not edit!

// (in-package batmag_soft.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class refFilPar {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.tau = null;
      this.ub = null;
      this.lb = null;
      this.enable = null;
    }
    else {
      if (initObj.hasOwnProperty('tau')) {
        this.tau = initObj.tau
      }
      else {
        this.tau = 0.0;
      }
      if (initObj.hasOwnProperty('ub')) {
        this.ub = initObj.ub
      }
      else {
        this.ub = 0.0;
      }
      if (initObj.hasOwnProperty('lb')) {
        this.lb = initObj.lb
      }
      else {
        this.lb = 0.0;
      }
      if (initObj.hasOwnProperty('enable')) {
        this.enable = initObj.enable
      }
      else {
        this.enable = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type refFilPar
    // Serialize message field [tau]
    bufferOffset = _serializer.float64(obj.tau, buffer, bufferOffset);
    // Serialize message field [ub]
    bufferOffset = _serializer.float64(obj.ub, buffer, bufferOffset);
    // Serialize message field [lb]
    bufferOffset = _serializer.float64(obj.lb, buffer, bufferOffset);
    // Serialize message field [enable]
    bufferOffset = _serializer.bool(obj.enable, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type refFilPar
    let len;
    let data = new refFilPar(null);
    // Deserialize message field [tau]
    data.tau = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [ub]
    data.ub = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [lb]
    data.lb = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [enable]
    data.enable = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 25;
  }

  static datatype() {
    // Returns string type for a message object
    return 'batmag_soft/refFilPar';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '2a0558c57d7b112179d563789fd6bb0e';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float64 tau
    float64 ub
    float64 lb
    bool enable
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new refFilPar(null);
    if (msg.tau !== undefined) {
      resolved.tau = msg.tau;
    }
    else {
      resolved.tau = 0.0
    }

    if (msg.ub !== undefined) {
      resolved.ub = msg.ub;
    }
    else {
      resolved.ub = 0.0
    }

    if (msg.lb !== undefined) {
      resolved.lb = msg.lb;
    }
    else {
      resolved.lb = 0.0
    }

    if (msg.enable !== undefined) {
      resolved.enable = msg.enable;
    }
    else {
      resolved.enable = false
    }

    return resolved;
    }
};

module.exports = refFilPar;
