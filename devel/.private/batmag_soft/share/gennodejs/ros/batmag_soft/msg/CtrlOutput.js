// Auto-generated. Do not edit!

// (in-package batmag_soft.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class CtrlOutput {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.ctrlout = null;
      this.size = null;
    }
    else {
      if (initObj.hasOwnProperty('ctrlout')) {
        this.ctrlout = initObj.ctrlout
      }
      else {
        this.ctrlout = new Array(10).fill(0);
      }
      if (initObj.hasOwnProperty('size')) {
        this.size = initObj.size
      }
      else {
        this.size = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type CtrlOutput
    // Check that the constant length array field [ctrlout] has the right length
    if (obj.ctrlout.length !== 10) {
      throw new Error('Unable to serialize array field ctrlout - length must be 10')
    }
    // Serialize message field [ctrlout]
    bufferOffset = _arraySerializer.float64(obj.ctrlout, buffer, bufferOffset, 10);
    // Serialize message field [size]
    bufferOffset = _serializer.int64(obj.size, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type CtrlOutput
    let len;
    let data = new CtrlOutput(null);
    // Deserialize message field [ctrlout]
    data.ctrlout = _arrayDeserializer.float64(buffer, bufferOffset, 10)
    // Deserialize message field [size]
    data.size = _deserializer.int64(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 88;
  }

  static datatype() {
    // Returns string type for a message object
    return 'batmag_soft/CtrlOutput';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'eab7552313b31041534bc8457d395d24';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float64[10] ctrlout
    int64 size
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new CtrlOutput(null);
    if (msg.ctrlout !== undefined) {
      resolved.ctrlout = msg.ctrlout;
    }
    else {
      resolved.ctrlout = new Array(10).fill(0)
    }

    if (msg.size !== undefined) {
      resolved.size = msg.size;
    }
    else {
      resolved.size = 0
    }

    return resolved;
    }
};

module.exports = CtrlOutput;
