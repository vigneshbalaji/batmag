// Auto-generated. Do not edit!

// (in-package batmag_soft.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class ForceCurrentSet {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.mur = null;
      this.radius = null;
      this.br = null;
      this.density = null;
    }
    else {
      if (initObj.hasOwnProperty('mur')) {
        this.mur = initObj.mur
      }
      else {
        this.mur = [];
      }
      if (initObj.hasOwnProperty('radius')) {
        this.radius = initObj.radius
      }
      else {
        this.radius = [];
      }
      if (initObj.hasOwnProperty('br')) {
        this.br = initObj.br
      }
      else {
        this.br = [];
      }
      if (initObj.hasOwnProperty('density')) {
        this.density = initObj.density
      }
      else {
        this.density = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type ForceCurrentSet
    // Serialize message field [mur]
    bufferOffset = _arraySerializer.float64(obj.mur, buffer, bufferOffset, null);
    // Serialize message field [radius]
    bufferOffset = _arraySerializer.float64(obj.radius, buffer, bufferOffset, null);
    // Serialize message field [br]
    bufferOffset = _arraySerializer.float64(obj.br, buffer, bufferOffset, null);
    // Serialize message field [density]
    bufferOffset = _serializer.int32(obj.density, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type ForceCurrentSet
    let len;
    let data = new ForceCurrentSet(null);
    // Deserialize message field [mur]
    data.mur = _arrayDeserializer.float64(buffer, bufferOffset, null)
    // Deserialize message field [radius]
    data.radius = _arrayDeserializer.float64(buffer, bufferOffset, null)
    // Deserialize message field [br]
    data.br = _arrayDeserializer.float64(buffer, bufferOffset, null)
    // Deserialize message field [density]
    data.density = _deserializer.int32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += 8 * object.mur.length;
    length += 8 * object.radius.length;
    length += 8 * object.br.length;
    return length + 16;
  }

  static datatype() {
    // Returns string type for a message object
    return 'batmag_soft/ForceCurrentSet';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '3a6903e8787abda9824ac514d912fce9';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float64[] mur
    float64[] radius
    float64[] br
    int32 density
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new ForceCurrentSet(null);
    if (msg.mur !== undefined) {
      resolved.mur = msg.mur;
    }
    else {
      resolved.mur = []
    }

    if (msg.radius !== undefined) {
      resolved.radius = msg.radius;
    }
    else {
      resolved.radius = []
    }

    if (msg.br !== undefined) {
      resolved.br = msg.br;
    }
    else {
      resolved.br = []
    }

    if (msg.density !== undefined) {
      resolved.density = msg.density;
    }
    else {
      resolved.density = 0
    }

    return resolved;
    }
};

module.exports = ForceCurrentSet;
