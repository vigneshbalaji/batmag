; Auto-generated. Do not edit!


(cl:in-package batmag_soft-srv)


;//! \htmlinclude SetInitParameters-request.msg.html

(cl:defclass <SetInitParameters-request> (roslisp-msg-protocol:ros-message)
  ((value
    :reader value
    :initarg :value
    :type (cl:vector cl:float)
   :initform (cl:make-array 9 :element-type 'cl:float :initial-element 0.0))
   (filled
    :reader filled
    :initarg :filled
    :type cl:integer
    :initform 0))
)

(cl:defclass SetInitParameters-request (<SetInitParameters-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <SetInitParameters-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'SetInitParameters-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name batmag_soft-srv:<SetInitParameters-request> is deprecated: use batmag_soft-srv:SetInitParameters-request instead.")))

(cl:ensure-generic-function 'value-val :lambda-list '(m))
(cl:defmethod value-val ((m <SetInitParameters-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-srv:value-val is deprecated.  Use batmag_soft-srv:value instead.")
  (value m))

(cl:ensure-generic-function 'filled-val :lambda-list '(m))
(cl:defmethod filled-val ((m <SetInitParameters-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-srv:filled-val is deprecated.  Use batmag_soft-srv:filled instead.")
  (filled m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <SetInitParameters-request>) ostream)
  "Serializes a message object of type '<SetInitParameters-request>"
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-double-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream)))
   (cl:slot-value msg 'value))
  (cl:let* ((signed (cl:slot-value msg 'filled)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <SetInitParameters-request>) istream)
  "Deserializes a message object of type '<SetInitParameters-request>"
  (cl:setf (cl:slot-value msg 'value) (cl:make-array 9))
  (cl:let ((vals (cl:slot-value msg 'value)))
    (cl:dotimes (i 9)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-double-float-bits bits)))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'filled) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<SetInitParameters-request>)))
  "Returns string type for a service object of type '<SetInitParameters-request>"
  "batmag_soft/SetInitParametersRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'SetInitParameters-request)))
  "Returns string type for a service object of type 'SetInitParameters-request"
  "batmag_soft/SetInitParametersRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<SetInitParameters-request>)))
  "Returns md5sum for a message object of type '<SetInitParameters-request>"
  "90323ad5a7dc29f5397a78bc8cec0d0d")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'SetInitParameters-request)))
  "Returns md5sum for a message object of type 'SetInitParameters-request"
  "90323ad5a7dc29f5397a78bc8cec0d0d")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<SetInitParameters-request>)))
  "Returns full string definition for message of type '<SetInitParameters-request>"
  (cl:format cl:nil "float64[9] value~%int32 filled~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'SetInitParameters-request)))
  "Returns full string definition for message of type 'SetInitParameters-request"
  (cl:format cl:nil "float64[9] value~%int32 filled~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <SetInitParameters-request>))
  (cl:+ 0
     0 (cl:reduce #'cl:+ (cl:slot-value msg 'value) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 8)))
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <SetInitParameters-request>))
  "Converts a ROS message object to a list"
  (cl:list 'SetInitParameters-request
    (cl:cons ':value (value msg))
    (cl:cons ':filled (filled msg))
))
;//! \htmlinclude SetInitParameters-response.msg.html

(cl:defclass <SetInitParameters-response> (roslisp-msg-protocol:ros-message)
  ((done
    :reader done
    :initarg :done
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass SetInitParameters-response (<SetInitParameters-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <SetInitParameters-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'SetInitParameters-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name batmag_soft-srv:<SetInitParameters-response> is deprecated: use batmag_soft-srv:SetInitParameters-response instead.")))

(cl:ensure-generic-function 'done-val :lambda-list '(m))
(cl:defmethod done-val ((m <SetInitParameters-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-srv:done-val is deprecated.  Use batmag_soft-srv:done instead.")
  (done m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <SetInitParameters-response>) ostream)
  "Serializes a message object of type '<SetInitParameters-response>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'done) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <SetInitParameters-response>) istream)
  "Deserializes a message object of type '<SetInitParameters-response>"
    (cl:setf (cl:slot-value msg 'done) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<SetInitParameters-response>)))
  "Returns string type for a service object of type '<SetInitParameters-response>"
  "batmag_soft/SetInitParametersResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'SetInitParameters-response)))
  "Returns string type for a service object of type 'SetInitParameters-response"
  "batmag_soft/SetInitParametersResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<SetInitParameters-response>)))
  "Returns md5sum for a message object of type '<SetInitParameters-response>"
  "90323ad5a7dc29f5397a78bc8cec0d0d")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'SetInitParameters-response)))
  "Returns md5sum for a message object of type 'SetInitParameters-response"
  "90323ad5a7dc29f5397a78bc8cec0d0d")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<SetInitParameters-response>)))
  "Returns full string definition for message of type '<SetInitParameters-response>"
  (cl:format cl:nil "bool done~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'SetInitParameters-response)))
  "Returns full string definition for message of type 'SetInitParameters-response"
  (cl:format cl:nil "bool done~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <SetInitParameters-response>))
  (cl:+ 0
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <SetInitParameters-response>))
  "Converts a ROS message object to a list"
  (cl:list 'SetInitParameters-response
    (cl:cons ':done (done msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'SetInitParameters)))
  'SetInitParameters-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'SetInitParameters)))
  'SetInitParameters-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'SetInitParameters)))
  "Returns string type for a service object of type '<SetInitParameters>"
  "batmag_soft/SetInitParameters")