(cl:in-package batmag_soft-srv)
(cl:export '(CTRL-VAL
          CTRL
          AGENT-VAL
          AGENT
          SUCCESS-VAL
          SUCCESS
          VALUE-VAL
          VALUE
          MIN-VAL
          MIN
          MAX-VAL
          MAX
          NAMES-VAL
          NAMES
))