; Auto-generated. Do not edit!


(cl:in-package batmag_soft-srv)


;//! \htmlinclude SaveImage-request.msg.html

(cl:defclass <SaveImage-request> (roslisp-msg-protocol:ros-message)
  ()
)

(cl:defclass SaveImage-request (<SaveImage-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <SaveImage-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'SaveImage-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name batmag_soft-srv:<SaveImage-request> is deprecated: use batmag_soft-srv:SaveImage-request instead.")))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <SaveImage-request>) ostream)
  "Serializes a message object of type '<SaveImage-request>"
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <SaveImage-request>) istream)
  "Deserializes a message object of type '<SaveImage-request>"
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<SaveImage-request>)))
  "Returns string type for a service object of type '<SaveImage-request>"
  "batmag_soft/SaveImageRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'SaveImage-request)))
  "Returns string type for a service object of type 'SaveImage-request"
  "batmag_soft/SaveImageRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<SaveImage-request>)))
  "Returns md5sum for a message object of type '<SaveImage-request>"
  "358e233cde0c8a8bcfea4ce193f8fc15")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'SaveImage-request)))
  "Returns md5sum for a message object of type 'SaveImage-request"
  "358e233cde0c8a8bcfea4ce193f8fc15")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<SaveImage-request>)))
  "Returns full string definition for message of type '<SaveImage-request>"
  (cl:format cl:nil "~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'SaveImage-request)))
  "Returns full string definition for message of type 'SaveImage-request"
  (cl:format cl:nil "~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <SaveImage-request>))
  (cl:+ 0
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <SaveImage-request>))
  "Converts a ROS message object to a list"
  (cl:list 'SaveImage-request
))
;//! \htmlinclude SaveImage-response.msg.html

(cl:defclass <SaveImage-response> (roslisp-msg-protocol:ros-message)
  ((success
    :reader success
    :initarg :success
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass SaveImage-response (<SaveImage-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <SaveImage-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'SaveImage-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name batmag_soft-srv:<SaveImage-response> is deprecated: use batmag_soft-srv:SaveImage-response instead.")))

(cl:ensure-generic-function 'success-val :lambda-list '(m))
(cl:defmethod success-val ((m <SaveImage-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-srv:success-val is deprecated.  Use batmag_soft-srv:success instead.")
  (success m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <SaveImage-response>) ostream)
  "Serializes a message object of type '<SaveImage-response>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'success) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <SaveImage-response>) istream)
  "Deserializes a message object of type '<SaveImage-response>"
    (cl:setf (cl:slot-value msg 'success) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<SaveImage-response>)))
  "Returns string type for a service object of type '<SaveImage-response>"
  "batmag_soft/SaveImageResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'SaveImage-response)))
  "Returns string type for a service object of type 'SaveImage-response"
  "batmag_soft/SaveImageResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<SaveImage-response>)))
  "Returns md5sum for a message object of type '<SaveImage-response>"
  "358e233cde0c8a8bcfea4ce193f8fc15")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'SaveImage-response)))
  "Returns md5sum for a message object of type 'SaveImage-response"
  "358e233cde0c8a8bcfea4ce193f8fc15")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<SaveImage-response>)))
  "Returns full string definition for message of type '<SaveImage-response>"
  (cl:format cl:nil "bool success~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'SaveImage-response)))
  "Returns full string definition for message of type 'SaveImage-response"
  (cl:format cl:nil "bool success~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <SaveImage-response>))
  (cl:+ 0
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <SaveImage-response>))
  "Converts a ROS message object to a list"
  (cl:list 'SaveImage-response
    (cl:cons ':success (success msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'SaveImage)))
  'SaveImage-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'SaveImage)))
  'SaveImage-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'SaveImage)))
  "Returns string type for a service object of type '<SaveImage>"
  "batmag_soft/SaveImage")