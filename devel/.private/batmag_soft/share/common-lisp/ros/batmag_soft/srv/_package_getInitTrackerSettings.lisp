(cl:in-package batmag_soft-srv)
(cl:export '(ROISIZE-VAL
          ROISIZE
          AGENTSIZE-VAL
          AGENTSIZE
          MAXH-VAL
          MAXH
          MINH-VAL
          MINH
          MAXS-VAL
          MAXS
          MINS-VAL
          MINS
          MAXV-VAL
          MAXV
          MINV-VAL
          MINV
          BLURSIZE-VAL
          BLURSIZE
          ID-VAL
          ID
))