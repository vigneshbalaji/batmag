; Auto-generated. Do not edit!


(cl:in-package batmag_soft-srv)


;//! \htmlinclude GetInitParameters-request.msg.html

(cl:defclass <GetInitParameters-request> (roslisp-msg-protocol:ros-message)
  ((ctrl
    :reader ctrl
    :initarg :ctrl
    :type cl:string
    :initform "")
   (agent
    :reader agent
    :initarg :agent
    :type cl:string
    :initform ""))
)

(cl:defclass GetInitParameters-request (<GetInitParameters-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <GetInitParameters-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'GetInitParameters-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name batmag_soft-srv:<GetInitParameters-request> is deprecated: use batmag_soft-srv:GetInitParameters-request instead.")))

(cl:ensure-generic-function 'ctrl-val :lambda-list '(m))
(cl:defmethod ctrl-val ((m <GetInitParameters-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-srv:ctrl-val is deprecated.  Use batmag_soft-srv:ctrl instead.")
  (ctrl m))

(cl:ensure-generic-function 'agent-val :lambda-list '(m))
(cl:defmethod agent-val ((m <GetInitParameters-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-srv:agent-val is deprecated.  Use batmag_soft-srv:agent instead.")
  (agent m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <GetInitParameters-request>) ostream)
  "Serializes a message object of type '<GetInitParameters-request>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'ctrl))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'ctrl))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'agent))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'agent))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <GetInitParameters-request>) istream)
  "Deserializes a message object of type '<GetInitParameters-request>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'ctrl) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'ctrl) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'agent) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'agent) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<GetInitParameters-request>)))
  "Returns string type for a service object of type '<GetInitParameters-request>"
  "batmag_soft/GetInitParametersRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'GetInitParameters-request)))
  "Returns string type for a service object of type 'GetInitParameters-request"
  "batmag_soft/GetInitParametersRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<GetInitParameters-request>)))
  "Returns md5sum for a message object of type '<GetInitParameters-request>"
  "cde8ea7816229f9bd2f4733f008c24de")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'GetInitParameters-request)))
  "Returns md5sum for a message object of type 'GetInitParameters-request"
  "cde8ea7816229f9bd2f4733f008c24de")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<GetInitParameters-request>)))
  "Returns full string definition for message of type '<GetInitParameters-request>"
  (cl:format cl:nil "string ctrl~%string agent~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'GetInitParameters-request)))
  "Returns full string definition for message of type 'GetInitParameters-request"
  (cl:format cl:nil "string ctrl~%string agent~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <GetInitParameters-request>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'ctrl))
     4 (cl:length (cl:slot-value msg 'agent))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <GetInitParameters-request>))
  "Converts a ROS message object to a list"
  (cl:list 'GetInitParameters-request
    (cl:cons ':ctrl (ctrl msg))
    (cl:cons ':agent (agent msg))
))
;//! \htmlinclude GetInitParameters-response.msg.html

(cl:defclass <GetInitParameters-response> (roslisp-msg-protocol:ros-message)
  ((success
    :reader success
    :initarg :success
    :type cl:boolean
    :initform cl:nil)
   (value
    :reader value
    :initarg :value
    :type (cl:vector cl:float)
   :initform (cl:make-array 9 :element-type 'cl:float :initial-element 0.0))
   (min
    :reader min
    :initarg :min
    :type (cl:vector cl:float)
   :initform (cl:make-array 9 :element-type 'cl:float :initial-element 0.0))
   (max
    :reader max
    :initarg :max
    :type (cl:vector cl:float)
   :initform (cl:make-array 9 :element-type 'cl:float :initial-element 0.0))
   (names
    :reader names
    :initarg :names
    :type (cl:vector cl:string)
   :initform (cl:make-array 9 :element-type 'cl:string :initial-element "")))
)

(cl:defclass GetInitParameters-response (<GetInitParameters-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <GetInitParameters-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'GetInitParameters-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name batmag_soft-srv:<GetInitParameters-response> is deprecated: use batmag_soft-srv:GetInitParameters-response instead.")))

(cl:ensure-generic-function 'success-val :lambda-list '(m))
(cl:defmethod success-val ((m <GetInitParameters-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-srv:success-val is deprecated.  Use batmag_soft-srv:success instead.")
  (success m))

(cl:ensure-generic-function 'value-val :lambda-list '(m))
(cl:defmethod value-val ((m <GetInitParameters-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-srv:value-val is deprecated.  Use batmag_soft-srv:value instead.")
  (value m))

(cl:ensure-generic-function 'min-val :lambda-list '(m))
(cl:defmethod min-val ((m <GetInitParameters-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-srv:min-val is deprecated.  Use batmag_soft-srv:min instead.")
  (min m))

(cl:ensure-generic-function 'max-val :lambda-list '(m))
(cl:defmethod max-val ((m <GetInitParameters-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-srv:max-val is deprecated.  Use batmag_soft-srv:max instead.")
  (max m))

(cl:ensure-generic-function 'names-val :lambda-list '(m))
(cl:defmethod names-val ((m <GetInitParameters-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-srv:names-val is deprecated.  Use batmag_soft-srv:names instead.")
  (names m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <GetInitParameters-response>) ostream)
  "Serializes a message object of type '<GetInitParameters-response>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'success) 1 0)) ostream)
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-double-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream)))
   (cl:slot-value msg 'value))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-double-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream)))
   (cl:slot-value msg 'min))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-double-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream)))
   (cl:slot-value msg 'max))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((__ros_str_len (cl:length ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) ele))
   (cl:slot-value msg 'names))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <GetInitParameters-response>) istream)
  "Deserializes a message object of type '<GetInitParameters-response>"
    (cl:setf (cl:slot-value msg 'success) (cl:not (cl:zerop (cl:read-byte istream))))
  (cl:setf (cl:slot-value msg 'value) (cl:make-array 9))
  (cl:let ((vals (cl:slot-value msg 'value)))
    (cl:dotimes (i 9)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-double-float-bits bits)))))
  (cl:setf (cl:slot-value msg 'min) (cl:make-array 9))
  (cl:let ((vals (cl:slot-value msg 'min)))
    (cl:dotimes (i 9)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-double-float-bits bits)))))
  (cl:setf (cl:slot-value msg 'max) (cl:make-array 9))
  (cl:let ((vals (cl:slot-value msg 'max)))
    (cl:dotimes (i 9)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-double-float-bits bits)))))
  (cl:setf (cl:slot-value msg 'names) (cl:make-array 9))
  (cl:let ((vals (cl:slot-value msg 'names)))
    (cl:dotimes (i 9)
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:aref vals i) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:aref vals i) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<GetInitParameters-response>)))
  "Returns string type for a service object of type '<GetInitParameters-response>"
  "batmag_soft/GetInitParametersResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'GetInitParameters-response)))
  "Returns string type for a service object of type 'GetInitParameters-response"
  "batmag_soft/GetInitParametersResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<GetInitParameters-response>)))
  "Returns md5sum for a message object of type '<GetInitParameters-response>"
  "cde8ea7816229f9bd2f4733f008c24de")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'GetInitParameters-response)))
  "Returns md5sum for a message object of type 'GetInitParameters-response"
  "cde8ea7816229f9bd2f4733f008c24de")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<GetInitParameters-response>)))
  "Returns full string definition for message of type '<GetInitParameters-response>"
  (cl:format cl:nil "bool success~%float64[9] value~%float64[9] min~%float64[9] max~%string[9] names~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'GetInitParameters-response)))
  "Returns full string definition for message of type 'GetInitParameters-response"
  (cl:format cl:nil "bool success~%float64[9] value~%float64[9] min~%float64[9] max~%string[9] names~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <GetInitParameters-response>))
  (cl:+ 0
     1
     0 (cl:reduce #'cl:+ (cl:slot-value msg 'value) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 8)))
     0 (cl:reduce #'cl:+ (cl:slot-value msg 'min) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 8)))
     0 (cl:reduce #'cl:+ (cl:slot-value msg 'max) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 8)))
     0 (cl:reduce #'cl:+ (cl:slot-value msg 'names) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 4 (cl:length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <GetInitParameters-response>))
  "Converts a ROS message object to a list"
  (cl:list 'GetInitParameters-response
    (cl:cons ':success (success msg))
    (cl:cons ':value (value msg))
    (cl:cons ':min (min msg))
    (cl:cons ':max (max msg))
    (cl:cons ':names (names msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'GetInitParameters)))
  'GetInitParameters-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'GetInitParameters)))
  'GetInitParameters-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'GetInitParameters)))
  "Returns string type for a service object of type '<GetInitParameters>"
  "batmag_soft/GetInitParameters")