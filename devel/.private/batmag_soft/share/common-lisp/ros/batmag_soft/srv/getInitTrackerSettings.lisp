; Auto-generated. Do not edit!


(cl:in-package batmag_soft-srv)


;//! \htmlinclude getInitTrackerSettings-request.msg.html

(cl:defclass <getInitTrackerSettings-request> (roslisp-msg-protocol:ros-message)
  ()
)

(cl:defclass getInitTrackerSettings-request (<getInitTrackerSettings-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <getInitTrackerSettings-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'getInitTrackerSettings-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name batmag_soft-srv:<getInitTrackerSettings-request> is deprecated: use batmag_soft-srv:getInitTrackerSettings-request instead.")))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <getInitTrackerSettings-request>) ostream)
  "Serializes a message object of type '<getInitTrackerSettings-request>"
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <getInitTrackerSettings-request>) istream)
  "Deserializes a message object of type '<getInitTrackerSettings-request>"
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<getInitTrackerSettings-request>)))
  "Returns string type for a service object of type '<getInitTrackerSettings-request>"
  "batmag_soft/getInitTrackerSettingsRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'getInitTrackerSettings-request)))
  "Returns string type for a service object of type 'getInitTrackerSettings-request"
  "batmag_soft/getInitTrackerSettingsRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<getInitTrackerSettings-request>)))
  "Returns md5sum for a message object of type '<getInitTrackerSettings-request>"
  "f5a055a737dbee5ea3851522901b54b0")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'getInitTrackerSettings-request)))
  "Returns md5sum for a message object of type 'getInitTrackerSettings-request"
  "f5a055a737dbee5ea3851522901b54b0")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<getInitTrackerSettings-request>)))
  "Returns full string definition for message of type '<getInitTrackerSettings-request>"
  (cl:format cl:nil "~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'getInitTrackerSettings-request)))
  "Returns full string definition for message of type 'getInitTrackerSettings-request"
  (cl:format cl:nil "~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <getInitTrackerSettings-request>))
  (cl:+ 0
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <getInitTrackerSettings-request>))
  "Converts a ROS message object to a list"
  (cl:list 'getInitTrackerSettings-request
))
;//! \htmlinclude getInitTrackerSettings-response.msg.html

(cl:defclass <getInitTrackerSettings-response> (roslisp-msg-protocol:ros-message)
  ((roiSize
    :reader roiSize
    :initarg :roiSize
    :type cl:integer
    :initform 0)
   (agentSize
    :reader agentSize
    :initarg :agentSize
    :type cl:integer
    :initform 0)
   (maxH
    :reader maxH
    :initarg :maxH
    :type cl:integer
    :initform 0)
   (minH
    :reader minH
    :initarg :minH
    :type cl:integer
    :initform 0)
   (maxS
    :reader maxS
    :initarg :maxS
    :type cl:integer
    :initform 0)
   (minS
    :reader minS
    :initarg :minS
    :type cl:integer
    :initform 0)
   (maxV
    :reader maxV
    :initarg :maxV
    :type cl:integer
    :initform 0)
   (minV
    :reader minV
    :initarg :minV
    :type cl:integer
    :initform 0)
   (blurSize
    :reader blurSize
    :initarg :blurSize
    :type cl:integer
    :initform 0)
   (id
    :reader id
    :initarg :id
    :type cl:fixnum
    :initform 0))
)

(cl:defclass getInitTrackerSettings-response (<getInitTrackerSettings-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <getInitTrackerSettings-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'getInitTrackerSettings-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name batmag_soft-srv:<getInitTrackerSettings-response> is deprecated: use batmag_soft-srv:getInitTrackerSettings-response instead.")))

(cl:ensure-generic-function 'roiSize-val :lambda-list '(m))
(cl:defmethod roiSize-val ((m <getInitTrackerSettings-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-srv:roiSize-val is deprecated.  Use batmag_soft-srv:roiSize instead.")
  (roiSize m))

(cl:ensure-generic-function 'agentSize-val :lambda-list '(m))
(cl:defmethod agentSize-val ((m <getInitTrackerSettings-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-srv:agentSize-val is deprecated.  Use batmag_soft-srv:agentSize instead.")
  (agentSize m))

(cl:ensure-generic-function 'maxH-val :lambda-list '(m))
(cl:defmethod maxH-val ((m <getInitTrackerSettings-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-srv:maxH-val is deprecated.  Use batmag_soft-srv:maxH instead.")
  (maxH m))

(cl:ensure-generic-function 'minH-val :lambda-list '(m))
(cl:defmethod minH-val ((m <getInitTrackerSettings-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-srv:minH-val is deprecated.  Use batmag_soft-srv:minH instead.")
  (minH m))

(cl:ensure-generic-function 'maxS-val :lambda-list '(m))
(cl:defmethod maxS-val ((m <getInitTrackerSettings-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-srv:maxS-val is deprecated.  Use batmag_soft-srv:maxS instead.")
  (maxS m))

(cl:ensure-generic-function 'minS-val :lambda-list '(m))
(cl:defmethod minS-val ((m <getInitTrackerSettings-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-srv:minS-val is deprecated.  Use batmag_soft-srv:minS instead.")
  (minS m))

(cl:ensure-generic-function 'maxV-val :lambda-list '(m))
(cl:defmethod maxV-val ((m <getInitTrackerSettings-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-srv:maxV-val is deprecated.  Use batmag_soft-srv:maxV instead.")
  (maxV m))

(cl:ensure-generic-function 'minV-val :lambda-list '(m))
(cl:defmethod minV-val ((m <getInitTrackerSettings-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-srv:minV-val is deprecated.  Use batmag_soft-srv:minV instead.")
  (minV m))

(cl:ensure-generic-function 'blurSize-val :lambda-list '(m))
(cl:defmethod blurSize-val ((m <getInitTrackerSettings-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-srv:blurSize-val is deprecated.  Use batmag_soft-srv:blurSize instead.")
  (blurSize m))

(cl:ensure-generic-function 'id-val :lambda-list '(m))
(cl:defmethod id-val ((m <getInitTrackerSettings-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-srv:id-val is deprecated.  Use batmag_soft-srv:id instead.")
  (id m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <getInitTrackerSettings-response>) ostream)
  "Serializes a message object of type '<getInitTrackerSettings-response>"
  (cl:let* ((signed (cl:slot-value msg 'roiSize)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'agentSize)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'maxH)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'minH)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'maxS)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'minS)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'maxV)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'minV)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'blurSize)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'id)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 256) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <getInitTrackerSettings-response>) istream)
  "Deserializes a message object of type '<getInitTrackerSettings-response>"
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'roiSize) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'agentSize) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'maxH) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'minH) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'maxS) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'minS) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'maxV) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'minV) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'blurSize) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'id) (cl:if (cl:< unsigned 128) unsigned (cl:- unsigned 256))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<getInitTrackerSettings-response>)))
  "Returns string type for a service object of type '<getInitTrackerSettings-response>"
  "batmag_soft/getInitTrackerSettingsResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'getInitTrackerSettings-response)))
  "Returns string type for a service object of type 'getInitTrackerSettings-response"
  "batmag_soft/getInitTrackerSettingsResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<getInitTrackerSettings-response>)))
  "Returns md5sum for a message object of type '<getInitTrackerSettings-response>"
  "f5a055a737dbee5ea3851522901b54b0")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'getInitTrackerSettings-response)))
  "Returns md5sum for a message object of type 'getInitTrackerSettings-response"
  "f5a055a737dbee5ea3851522901b54b0")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<getInitTrackerSettings-response>)))
  "Returns full string definition for message of type '<getInitTrackerSettings-response>"
  (cl:format cl:nil "int32 roiSize~%int32 agentSize~%int32 maxH~%int32 minH~%int32 maxS~%int32 minS~%int32 maxV~%int32 minV~%int32 blurSize~%int8 id~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'getInitTrackerSettings-response)))
  "Returns full string definition for message of type 'getInitTrackerSettings-response"
  (cl:format cl:nil "int32 roiSize~%int32 agentSize~%int32 maxH~%int32 minH~%int32 maxS~%int32 minS~%int32 maxV~%int32 minV~%int32 blurSize~%int8 id~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <getInitTrackerSettings-response>))
  (cl:+ 0
     4
     4
     4
     4
     4
     4
     4
     4
     4
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <getInitTrackerSettings-response>))
  "Converts a ROS message object to a list"
  (cl:list 'getInitTrackerSettings-response
    (cl:cons ':roiSize (roiSize msg))
    (cl:cons ':agentSize (agentSize msg))
    (cl:cons ':maxH (maxH msg))
    (cl:cons ':minH (minH msg))
    (cl:cons ':maxS (maxS msg))
    (cl:cons ':minS (minS msg))
    (cl:cons ':maxV (maxV msg))
    (cl:cons ':minV (minV msg))
    (cl:cons ':blurSize (blurSize msg))
    (cl:cons ':id (id msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'getInitTrackerSettings)))
  'getInitTrackerSettings-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'getInitTrackerSettings)))
  'getInitTrackerSettings-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'getInitTrackerSettings)))
  "Returns string type for a service object of type '<getInitTrackerSettings>"
  "batmag_soft/getInitTrackerSettings")