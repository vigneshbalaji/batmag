(cl:in-package batmag_soft-srv)
(cl:export '(EXPOSURE-VAL
          EXPOSURE
          BRIGHTNESS-VAL
          BRIGHTNESS
          FRAMERATE-VAL
          FRAMERATE
          GAIN-VAL
          GAIN
          GAMMA-VAL
          GAMMA
          HUE-VAL
          HUE
          SATURATION-VAL
          SATURATION
          SHARPNESS-VAL
          SHARPNESS
          SHUTTER-VAL
          SHUTTER
          WBB-VAL
          WBB
          WBR-VAL
          WBR
))