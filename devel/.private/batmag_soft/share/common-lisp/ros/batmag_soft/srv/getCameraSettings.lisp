; Auto-generated. Do not edit!


(cl:in-package batmag_soft-srv)


;//! \htmlinclude getCameraSettings-request.msg.html

(cl:defclass <getCameraSettings-request> (roslisp-msg-protocol:ros-message)
  ()
)

(cl:defclass getCameraSettings-request (<getCameraSettings-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <getCameraSettings-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'getCameraSettings-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name batmag_soft-srv:<getCameraSettings-request> is deprecated: use batmag_soft-srv:getCameraSettings-request instead.")))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <getCameraSettings-request>) ostream)
  "Serializes a message object of type '<getCameraSettings-request>"
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <getCameraSettings-request>) istream)
  "Deserializes a message object of type '<getCameraSettings-request>"
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<getCameraSettings-request>)))
  "Returns string type for a service object of type '<getCameraSettings-request>"
  "batmag_soft/getCameraSettingsRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'getCameraSettings-request)))
  "Returns string type for a service object of type 'getCameraSettings-request"
  "batmag_soft/getCameraSettingsRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<getCameraSettings-request>)))
  "Returns md5sum for a message object of type '<getCameraSettings-request>"
  "8fe8cda4f5f6a5f7062c243d4b3b5fb9")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'getCameraSettings-request)))
  "Returns md5sum for a message object of type 'getCameraSettings-request"
  "8fe8cda4f5f6a5f7062c243d4b3b5fb9")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<getCameraSettings-request>)))
  "Returns full string definition for message of type '<getCameraSettings-request>"
  (cl:format cl:nil "~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'getCameraSettings-request)))
  "Returns full string definition for message of type 'getCameraSettings-request"
  (cl:format cl:nil "~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <getCameraSettings-request>))
  (cl:+ 0
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <getCameraSettings-request>))
  "Converts a ROS message object to a list"
  (cl:list 'getCameraSettings-request
))
;//! \htmlinclude getCameraSettings-response.msg.html

(cl:defclass <getCameraSettings-response> (roslisp-msg-protocol:ros-message)
  ((Exposure
    :reader Exposure
    :initarg :Exposure
    :type cl:float
    :initform 0.0)
   (Brightness
    :reader Brightness
    :initarg :Brightness
    :type cl:float
    :initform 0.0)
   (Framerate
    :reader Framerate
    :initarg :Framerate
    :type cl:float
    :initform 0.0)
   (Gain
    :reader Gain
    :initarg :Gain
    :type cl:float
    :initform 0.0)
   (Gamma
    :reader Gamma
    :initarg :Gamma
    :type cl:float
    :initform 0.0)
   (Hue
    :reader Hue
    :initarg :Hue
    :type cl:float
    :initform 0.0)
   (Saturation
    :reader Saturation
    :initarg :Saturation
    :type cl:float
    :initform 0.0)
   (Sharpness
    :reader Sharpness
    :initarg :Sharpness
    :type cl:float
    :initform 0.0)
   (Shutter
    :reader Shutter
    :initarg :Shutter
    :type cl:float
    :initform 0.0)
   (wbb
    :reader wbb
    :initarg :wbb
    :type cl:float
    :initform 0.0)
   (wbr
    :reader wbr
    :initarg :wbr
    :type cl:float
    :initform 0.0))
)

(cl:defclass getCameraSettings-response (<getCameraSettings-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <getCameraSettings-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'getCameraSettings-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name batmag_soft-srv:<getCameraSettings-response> is deprecated: use batmag_soft-srv:getCameraSettings-response instead.")))

(cl:ensure-generic-function 'Exposure-val :lambda-list '(m))
(cl:defmethod Exposure-val ((m <getCameraSettings-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-srv:Exposure-val is deprecated.  Use batmag_soft-srv:Exposure instead.")
  (Exposure m))

(cl:ensure-generic-function 'Brightness-val :lambda-list '(m))
(cl:defmethod Brightness-val ((m <getCameraSettings-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-srv:Brightness-val is deprecated.  Use batmag_soft-srv:Brightness instead.")
  (Brightness m))

(cl:ensure-generic-function 'Framerate-val :lambda-list '(m))
(cl:defmethod Framerate-val ((m <getCameraSettings-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-srv:Framerate-val is deprecated.  Use batmag_soft-srv:Framerate instead.")
  (Framerate m))

(cl:ensure-generic-function 'Gain-val :lambda-list '(m))
(cl:defmethod Gain-val ((m <getCameraSettings-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-srv:Gain-val is deprecated.  Use batmag_soft-srv:Gain instead.")
  (Gain m))

(cl:ensure-generic-function 'Gamma-val :lambda-list '(m))
(cl:defmethod Gamma-val ((m <getCameraSettings-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-srv:Gamma-val is deprecated.  Use batmag_soft-srv:Gamma instead.")
  (Gamma m))

(cl:ensure-generic-function 'Hue-val :lambda-list '(m))
(cl:defmethod Hue-val ((m <getCameraSettings-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-srv:Hue-val is deprecated.  Use batmag_soft-srv:Hue instead.")
  (Hue m))

(cl:ensure-generic-function 'Saturation-val :lambda-list '(m))
(cl:defmethod Saturation-val ((m <getCameraSettings-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-srv:Saturation-val is deprecated.  Use batmag_soft-srv:Saturation instead.")
  (Saturation m))

(cl:ensure-generic-function 'Sharpness-val :lambda-list '(m))
(cl:defmethod Sharpness-val ((m <getCameraSettings-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-srv:Sharpness-val is deprecated.  Use batmag_soft-srv:Sharpness instead.")
  (Sharpness m))

(cl:ensure-generic-function 'Shutter-val :lambda-list '(m))
(cl:defmethod Shutter-val ((m <getCameraSettings-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-srv:Shutter-val is deprecated.  Use batmag_soft-srv:Shutter instead.")
  (Shutter m))

(cl:ensure-generic-function 'wbb-val :lambda-list '(m))
(cl:defmethod wbb-val ((m <getCameraSettings-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-srv:wbb-val is deprecated.  Use batmag_soft-srv:wbb instead.")
  (wbb m))

(cl:ensure-generic-function 'wbr-val :lambda-list '(m))
(cl:defmethod wbr-val ((m <getCameraSettings-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-srv:wbr-val is deprecated.  Use batmag_soft-srv:wbr instead.")
  (wbr m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <getCameraSettings-response>) ostream)
  "Serializes a message object of type '<getCameraSettings-response>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'Exposure))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'Brightness))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'Framerate))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'Gain))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'Gamma))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'Hue))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'Saturation))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'Sharpness))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'Shutter))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'wbb))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'wbr))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <getCameraSettings-response>) istream)
  "Deserializes a message object of type '<getCameraSettings-response>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'Exposure) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'Brightness) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'Framerate) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'Gain) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'Gamma) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'Hue) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'Saturation) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'Sharpness) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'Shutter) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'wbb) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'wbr) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<getCameraSettings-response>)))
  "Returns string type for a service object of type '<getCameraSettings-response>"
  "batmag_soft/getCameraSettingsResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'getCameraSettings-response)))
  "Returns string type for a service object of type 'getCameraSettings-response"
  "batmag_soft/getCameraSettingsResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<getCameraSettings-response>)))
  "Returns md5sum for a message object of type '<getCameraSettings-response>"
  "8fe8cda4f5f6a5f7062c243d4b3b5fb9")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'getCameraSettings-response)))
  "Returns md5sum for a message object of type 'getCameraSettings-response"
  "8fe8cda4f5f6a5f7062c243d4b3b5fb9")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<getCameraSettings-response>)))
  "Returns full string definition for message of type '<getCameraSettings-response>"
  (cl:format cl:nil "float32 Exposure~%float32 Brightness~%float32 Framerate~%float32 Gain~%float32 Gamma~%float32 Hue~%float32 Saturation~%float32 Sharpness~%float32 Shutter~%float32 wbb~%float32 wbr~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'getCameraSettings-response)))
  "Returns full string definition for message of type 'getCameraSettings-response"
  (cl:format cl:nil "float32 Exposure~%float32 Brightness~%float32 Framerate~%float32 Gain~%float32 Gamma~%float32 Hue~%float32 Saturation~%float32 Sharpness~%float32 Shutter~%float32 wbb~%float32 wbr~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <getCameraSettings-response>))
  (cl:+ 0
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <getCameraSettings-response>))
  "Converts a ROS message object to a list"
  (cl:list 'getCameraSettings-response
    (cl:cons ':Exposure (Exposure msg))
    (cl:cons ':Brightness (Brightness msg))
    (cl:cons ':Framerate (Framerate msg))
    (cl:cons ':Gain (Gain msg))
    (cl:cons ':Gamma (Gamma msg))
    (cl:cons ':Hue (Hue msg))
    (cl:cons ':Saturation (Saturation msg))
    (cl:cons ':Sharpness (Sharpness msg))
    (cl:cons ':Shutter (Shutter msg))
    (cl:cons ':wbb (wbb msg))
    (cl:cons ':wbr (wbr msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'getCameraSettings)))
  'getCameraSettings-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'getCameraSettings)))
  'getCameraSettings-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'getCameraSettings)))
  "Returns string type for a service object of type '<getCameraSettings>"
  "batmag_soft/getCameraSettings")