; Auto-generated. Do not edit!


(cl:in-package batmag_soft-msg)


;//! \htmlinclude RecordRaw.msg.html

(cl:defclass <RecordRaw> (roslisp-msg-protocol:ros-message)
  ((recordRaw
    :reader recordRaw
    :initarg :recordRaw
    :type cl:boolean
    :initform cl:nil)
   (recordWhere
    :reader recordWhere
    :initarg :recordWhere
    :type cl:string
    :initform ""))
)

(cl:defclass RecordRaw (<RecordRaw>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <RecordRaw>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'RecordRaw)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name batmag_soft-msg:<RecordRaw> is deprecated: use batmag_soft-msg:RecordRaw instead.")))

(cl:ensure-generic-function 'recordRaw-val :lambda-list '(m))
(cl:defmethod recordRaw-val ((m <RecordRaw>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:recordRaw-val is deprecated.  Use batmag_soft-msg:recordRaw instead.")
  (recordRaw m))

(cl:ensure-generic-function 'recordWhere-val :lambda-list '(m))
(cl:defmethod recordWhere-val ((m <RecordRaw>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:recordWhere-val is deprecated.  Use batmag_soft-msg:recordWhere instead.")
  (recordWhere m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <RecordRaw>) ostream)
  "Serializes a message object of type '<RecordRaw>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'recordRaw) 1 0)) ostream)
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'recordWhere))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'recordWhere))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <RecordRaw>) istream)
  "Deserializes a message object of type '<RecordRaw>"
    (cl:setf (cl:slot-value msg 'recordRaw) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'recordWhere) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'recordWhere) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<RecordRaw>)))
  "Returns string type for a message object of type '<RecordRaw>"
  "batmag_soft/RecordRaw")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'RecordRaw)))
  "Returns string type for a message object of type 'RecordRaw"
  "batmag_soft/RecordRaw")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<RecordRaw>)))
  "Returns md5sum for a message object of type '<RecordRaw>"
  "dba747aebd3c39de46937fb36819f3c1")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'RecordRaw)))
  "Returns md5sum for a message object of type 'RecordRaw"
  "dba747aebd3c39de46937fb36819f3c1")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<RecordRaw>)))
  "Returns full string definition for message of type '<RecordRaw>"
  (cl:format cl:nil "bool recordRaw~%string recordWhere~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'RecordRaw)))
  "Returns full string definition for message of type 'RecordRaw"
  (cl:format cl:nil "bool recordRaw~%string recordWhere~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <RecordRaw>))
  (cl:+ 0
     1
     4 (cl:length (cl:slot-value msg 'recordWhere))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <RecordRaw>))
  "Converts a ROS message object to a list"
  (cl:list 'RecordRaw
    (cl:cons ':recordRaw (recordRaw msg))
    (cl:cons ':recordWhere (recordWhere msg))
))
