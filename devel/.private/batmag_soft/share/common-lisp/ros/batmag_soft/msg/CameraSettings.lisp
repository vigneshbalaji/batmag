; Auto-generated. Do not edit!


(cl:in-package batmag_soft-msg)


;//! \htmlinclude CameraSettings.msg.html

(cl:defclass <CameraSettings> (roslisp-msg-protocol:ros-message)
  ((Exposure
    :reader Exposure
    :initarg :Exposure
    :type cl:float
    :initform 0.0)
   (Brightness
    :reader Brightness
    :initarg :Brightness
    :type cl:float
    :initform 0.0)
   (Framerate
    :reader Framerate
    :initarg :Framerate
    :type cl:float
    :initform 0.0)
   (Gain
    :reader Gain
    :initarg :Gain
    :type cl:float
    :initform 0.0)
   (Gamma
    :reader Gamma
    :initarg :Gamma
    :type cl:float
    :initform 0.0)
   (Hue
    :reader Hue
    :initarg :Hue
    :type cl:float
    :initform 0.0)
   (Saturation
    :reader Saturation
    :initarg :Saturation
    :type cl:float
    :initform 0.0)
   (Sharpness
    :reader Sharpness
    :initarg :Sharpness
    :type cl:float
    :initform 0.0)
   (Shutter
    :reader Shutter
    :initarg :Shutter
    :type cl:float
    :initform 0.0)
   (wbb
    :reader wbb
    :initarg :wbb
    :type cl:float
    :initform 0.0)
   (wbr
    :reader wbr
    :initarg :wbr
    :type cl:float
    :initform 0.0))
)

(cl:defclass CameraSettings (<CameraSettings>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <CameraSettings>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'CameraSettings)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name batmag_soft-msg:<CameraSettings> is deprecated: use batmag_soft-msg:CameraSettings instead.")))

(cl:ensure-generic-function 'Exposure-val :lambda-list '(m))
(cl:defmethod Exposure-val ((m <CameraSettings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:Exposure-val is deprecated.  Use batmag_soft-msg:Exposure instead.")
  (Exposure m))

(cl:ensure-generic-function 'Brightness-val :lambda-list '(m))
(cl:defmethod Brightness-val ((m <CameraSettings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:Brightness-val is deprecated.  Use batmag_soft-msg:Brightness instead.")
  (Brightness m))

(cl:ensure-generic-function 'Framerate-val :lambda-list '(m))
(cl:defmethod Framerate-val ((m <CameraSettings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:Framerate-val is deprecated.  Use batmag_soft-msg:Framerate instead.")
  (Framerate m))

(cl:ensure-generic-function 'Gain-val :lambda-list '(m))
(cl:defmethod Gain-val ((m <CameraSettings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:Gain-val is deprecated.  Use batmag_soft-msg:Gain instead.")
  (Gain m))

(cl:ensure-generic-function 'Gamma-val :lambda-list '(m))
(cl:defmethod Gamma-val ((m <CameraSettings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:Gamma-val is deprecated.  Use batmag_soft-msg:Gamma instead.")
  (Gamma m))

(cl:ensure-generic-function 'Hue-val :lambda-list '(m))
(cl:defmethod Hue-val ((m <CameraSettings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:Hue-val is deprecated.  Use batmag_soft-msg:Hue instead.")
  (Hue m))

(cl:ensure-generic-function 'Saturation-val :lambda-list '(m))
(cl:defmethod Saturation-val ((m <CameraSettings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:Saturation-val is deprecated.  Use batmag_soft-msg:Saturation instead.")
  (Saturation m))

(cl:ensure-generic-function 'Sharpness-val :lambda-list '(m))
(cl:defmethod Sharpness-val ((m <CameraSettings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:Sharpness-val is deprecated.  Use batmag_soft-msg:Sharpness instead.")
  (Sharpness m))

(cl:ensure-generic-function 'Shutter-val :lambda-list '(m))
(cl:defmethod Shutter-val ((m <CameraSettings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:Shutter-val is deprecated.  Use batmag_soft-msg:Shutter instead.")
  (Shutter m))

(cl:ensure-generic-function 'wbb-val :lambda-list '(m))
(cl:defmethod wbb-val ((m <CameraSettings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:wbb-val is deprecated.  Use batmag_soft-msg:wbb instead.")
  (wbb m))

(cl:ensure-generic-function 'wbr-val :lambda-list '(m))
(cl:defmethod wbr-val ((m <CameraSettings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:wbr-val is deprecated.  Use batmag_soft-msg:wbr instead.")
  (wbr m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <CameraSettings>) ostream)
  "Serializes a message object of type '<CameraSettings>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'Exposure))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'Brightness))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'Framerate))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'Gain))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'Gamma))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'Hue))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'Saturation))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'Sharpness))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'Shutter))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'wbb))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'wbr))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <CameraSettings>) istream)
  "Deserializes a message object of type '<CameraSettings>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'Exposure) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'Brightness) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'Framerate) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'Gain) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'Gamma) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'Hue) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'Saturation) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'Sharpness) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'Shutter) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'wbb) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'wbr) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<CameraSettings>)))
  "Returns string type for a message object of type '<CameraSettings>"
  "batmag_soft/CameraSettings")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'CameraSettings)))
  "Returns string type for a message object of type 'CameraSettings"
  "batmag_soft/CameraSettings")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<CameraSettings>)))
  "Returns md5sum for a message object of type '<CameraSettings>"
  "8fe8cda4f5f6a5f7062c243d4b3b5fb9")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'CameraSettings)))
  "Returns md5sum for a message object of type 'CameraSettings"
  "8fe8cda4f5f6a5f7062c243d4b3b5fb9")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<CameraSettings>)))
  "Returns full string definition for message of type '<CameraSettings>"
  (cl:format cl:nil "float32 Exposure~%float32 Brightness~%float32 Framerate~%float32 Gain~%float32 Gamma~%float32 Hue~%float32 Saturation~%float32 Sharpness~%float32 Shutter~%float32 wbb~%float32 wbr~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'CameraSettings)))
  "Returns full string definition for message of type 'CameraSettings"
  (cl:format cl:nil "float32 Exposure~%float32 Brightness~%float32 Framerate~%float32 Gain~%float32 Gamma~%float32 Hue~%float32 Saturation~%float32 Sharpness~%float32 Shutter~%float32 wbb~%float32 wbr~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <CameraSettings>))
  (cl:+ 0
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <CameraSettings>))
  "Converts a ROS message object to a list"
  (cl:list 'CameraSettings
    (cl:cons ':Exposure (Exposure msg))
    (cl:cons ':Brightness (Brightness msg))
    (cl:cons ':Framerate (Framerate msg))
    (cl:cons ':Gain (Gain msg))
    (cl:cons ':Gamma (Gamma msg))
    (cl:cons ':Hue (Hue msg))
    (cl:cons ':Saturation (Saturation msg))
    (cl:cons ':Sharpness (Sharpness msg))
    (cl:cons ':Shutter (Shutter msg))
    (cl:cons ':wbb (wbb msg))
    (cl:cons ':wbr (wbr msg))
))
