; Auto-generated. Do not edit!


(cl:in-package batmag_soft-msg)


;//! \htmlinclude ResetElmos.msg.html

(cl:defclass <ResetElmos> (roslisp-msg-protocol:ros-message)
  ((reset
    :reader reset
    :initarg :reset
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass ResetElmos (<ResetElmos>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <ResetElmos>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'ResetElmos)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name batmag_soft-msg:<ResetElmos> is deprecated: use batmag_soft-msg:ResetElmos instead.")))

(cl:ensure-generic-function 'reset-val :lambda-list '(m))
(cl:defmethod reset-val ((m <ResetElmos>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:reset-val is deprecated.  Use batmag_soft-msg:reset instead.")
  (reset m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <ResetElmos>) ostream)
  "Serializes a message object of type '<ResetElmos>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'reset) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <ResetElmos>) istream)
  "Deserializes a message object of type '<ResetElmos>"
    (cl:setf (cl:slot-value msg 'reset) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<ResetElmos>)))
  "Returns string type for a message object of type '<ResetElmos>"
  "batmag_soft/ResetElmos")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'ResetElmos)))
  "Returns string type for a message object of type 'ResetElmos"
  "batmag_soft/ResetElmos")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<ResetElmos>)))
  "Returns md5sum for a message object of type '<ResetElmos>"
  "ba4b0b221fb425ac5eaf73f71ae34971")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'ResetElmos)))
  "Returns md5sum for a message object of type 'ResetElmos"
  "ba4b0b221fb425ac5eaf73f71ae34971")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<ResetElmos>)))
  "Returns full string definition for message of type '<ResetElmos>"
  (cl:format cl:nil "bool reset~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'ResetElmos)))
  "Returns full string definition for message of type 'ResetElmos"
  (cl:format cl:nil "bool reset~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <ResetElmos>))
  (cl:+ 0
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <ResetElmos>))
  "Converts a ROS message object to a list"
  (cl:list 'ResetElmos
    (cl:cons ':reset (reset msg))
))
