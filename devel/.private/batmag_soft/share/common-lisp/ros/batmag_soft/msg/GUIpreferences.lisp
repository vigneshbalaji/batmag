; Auto-generated. Do not edit!


(cl:in-package batmag_soft-msg)


;//! \htmlinclude GUIpreferences.msg.html

(cl:defclass <GUIpreferences> (roslisp-msg-protocol:ros-message)
  ((recordOn
    :reader recordOn
    :initarg :recordOn
    :type cl:boolean
    :initform cl:nil)
   (ctrlEn
    :reader ctrlEn
    :initarg :ctrlEn
    :type cl:boolean
    :initform cl:nil)
   (debugEn
    :reader debugEn
    :initarg :debugEn
    :type cl:boolean
    :initform cl:nil)
   (gravityEn
    :reader gravityEn
    :initarg :gravityEn
    :type cl:boolean
    :initform cl:nil)
   (gridOn
    :reader gridOn
    :initarg :gridOn
    :type cl:boolean
    :initform cl:nil)
   (PMposition
    :reader PMposition
    :initarg :PMposition
    :type cl:float
    :initform 0.0)
   (PMenable
    :reader PMenable
    :initarg :PMenable
    :type cl:boolean
    :initform cl:nil)
   (omegaCtrl
    :reader omegaCtrl
    :initarg :omegaCtrl
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass GUIpreferences (<GUIpreferences>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <GUIpreferences>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'GUIpreferences)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name batmag_soft-msg:<GUIpreferences> is deprecated: use batmag_soft-msg:GUIpreferences instead.")))

(cl:ensure-generic-function 'recordOn-val :lambda-list '(m))
(cl:defmethod recordOn-val ((m <GUIpreferences>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:recordOn-val is deprecated.  Use batmag_soft-msg:recordOn instead.")
  (recordOn m))

(cl:ensure-generic-function 'ctrlEn-val :lambda-list '(m))
(cl:defmethod ctrlEn-val ((m <GUIpreferences>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:ctrlEn-val is deprecated.  Use batmag_soft-msg:ctrlEn instead.")
  (ctrlEn m))

(cl:ensure-generic-function 'debugEn-val :lambda-list '(m))
(cl:defmethod debugEn-val ((m <GUIpreferences>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:debugEn-val is deprecated.  Use batmag_soft-msg:debugEn instead.")
  (debugEn m))

(cl:ensure-generic-function 'gravityEn-val :lambda-list '(m))
(cl:defmethod gravityEn-val ((m <GUIpreferences>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:gravityEn-val is deprecated.  Use batmag_soft-msg:gravityEn instead.")
  (gravityEn m))

(cl:ensure-generic-function 'gridOn-val :lambda-list '(m))
(cl:defmethod gridOn-val ((m <GUIpreferences>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:gridOn-val is deprecated.  Use batmag_soft-msg:gridOn instead.")
  (gridOn m))

(cl:ensure-generic-function 'PMposition-val :lambda-list '(m))
(cl:defmethod PMposition-val ((m <GUIpreferences>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:PMposition-val is deprecated.  Use batmag_soft-msg:PMposition instead.")
  (PMposition m))

(cl:ensure-generic-function 'PMenable-val :lambda-list '(m))
(cl:defmethod PMenable-val ((m <GUIpreferences>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:PMenable-val is deprecated.  Use batmag_soft-msg:PMenable instead.")
  (PMenable m))

(cl:ensure-generic-function 'omegaCtrl-val :lambda-list '(m))
(cl:defmethod omegaCtrl-val ((m <GUIpreferences>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:omegaCtrl-val is deprecated.  Use batmag_soft-msg:omegaCtrl instead.")
  (omegaCtrl m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <GUIpreferences>) ostream)
  "Serializes a message object of type '<GUIpreferences>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'recordOn) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'ctrlEn) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'debugEn) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'gravityEn) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'gridOn) 1 0)) ostream)
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'PMposition))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'PMenable) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'omegaCtrl) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <GUIpreferences>) istream)
  "Deserializes a message object of type '<GUIpreferences>"
    (cl:setf (cl:slot-value msg 'recordOn) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'ctrlEn) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'debugEn) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'gravityEn) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'gridOn) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'PMposition) (roslisp-utils:decode-double-float-bits bits)))
    (cl:setf (cl:slot-value msg 'PMenable) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'omegaCtrl) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<GUIpreferences>)))
  "Returns string type for a message object of type '<GUIpreferences>"
  "batmag_soft/GUIpreferences")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'GUIpreferences)))
  "Returns string type for a message object of type 'GUIpreferences"
  "batmag_soft/GUIpreferences")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<GUIpreferences>)))
  "Returns md5sum for a message object of type '<GUIpreferences>"
  "30cc44c0758499a2509ba85095012fbb")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'GUIpreferences)))
  "Returns md5sum for a message object of type 'GUIpreferences"
  "30cc44c0758499a2509ba85095012fbb")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<GUIpreferences>)))
  "Returns full string definition for message of type '<GUIpreferences>"
  (cl:format cl:nil "bool recordOn~%bool ctrlEn~%bool debugEn~%bool gravityEn~%bool gridOn~%float64 PMposition~%bool PMenable~%bool omegaCtrl~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'GUIpreferences)))
  "Returns full string definition for message of type 'GUIpreferences"
  (cl:format cl:nil "bool recordOn~%bool ctrlEn~%bool debugEn~%bool gravityEn~%bool gridOn~%float64 PMposition~%bool PMenable~%bool omegaCtrl~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <GUIpreferences>))
  (cl:+ 0
     1
     1
     1
     1
     1
     8
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <GUIpreferences>))
  "Converts a ROS message object to a list"
  (cl:list 'GUIpreferences
    (cl:cons ':recordOn (recordOn msg))
    (cl:cons ':ctrlEn (ctrlEn msg))
    (cl:cons ':debugEn (debugEn msg))
    (cl:cons ':gravityEn (gravityEn msg))
    (cl:cons ':gridOn (gridOn msg))
    (cl:cons ':PMposition (PMposition msg))
    (cl:cons ':PMenable (PMenable msg))
    (cl:cons ':omegaCtrl (omegaCtrl msg))
))
