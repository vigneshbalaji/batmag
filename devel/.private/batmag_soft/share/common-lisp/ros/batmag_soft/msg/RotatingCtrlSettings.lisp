; Auto-generated. Do not edit!


(cl:in-package batmag_soft-msg)


;//! \htmlinclude RotatingCtrlSettings.msg.html

(cl:defclass <RotatingCtrlSettings> (roslisp-msg-protocol:ros-message)
  ((freq
    :reader freq
    :initarg :freq
    :type cl:float
    :initform 0.0)
   (rotAmp
    :reader rotAmp
    :initarg :rotAmp
    :type cl:float
    :initform 0.0)
   (fixAmp
    :reader fixAmp
    :initarg :fixAmp
    :type cl:float
    :initform 0.0))
)

(cl:defclass RotatingCtrlSettings (<RotatingCtrlSettings>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <RotatingCtrlSettings>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'RotatingCtrlSettings)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name batmag_soft-msg:<RotatingCtrlSettings> is deprecated: use batmag_soft-msg:RotatingCtrlSettings instead.")))

(cl:ensure-generic-function 'freq-val :lambda-list '(m))
(cl:defmethod freq-val ((m <RotatingCtrlSettings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:freq-val is deprecated.  Use batmag_soft-msg:freq instead.")
  (freq m))

(cl:ensure-generic-function 'rotAmp-val :lambda-list '(m))
(cl:defmethod rotAmp-val ((m <RotatingCtrlSettings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:rotAmp-val is deprecated.  Use batmag_soft-msg:rotAmp instead.")
  (rotAmp m))

(cl:ensure-generic-function 'fixAmp-val :lambda-list '(m))
(cl:defmethod fixAmp-val ((m <RotatingCtrlSettings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:fixAmp-val is deprecated.  Use batmag_soft-msg:fixAmp instead.")
  (fixAmp m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <RotatingCtrlSettings>) ostream)
  "Serializes a message object of type '<RotatingCtrlSettings>"
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'freq))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'rotAmp))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'fixAmp))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <RotatingCtrlSettings>) istream)
  "Deserializes a message object of type '<RotatingCtrlSettings>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'freq) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'rotAmp) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'fixAmp) (roslisp-utils:decode-double-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<RotatingCtrlSettings>)))
  "Returns string type for a message object of type '<RotatingCtrlSettings>"
  "batmag_soft/RotatingCtrlSettings")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'RotatingCtrlSettings)))
  "Returns string type for a message object of type 'RotatingCtrlSettings"
  "batmag_soft/RotatingCtrlSettings")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<RotatingCtrlSettings>)))
  "Returns md5sum for a message object of type '<RotatingCtrlSettings>"
  "3f7676b3fed2bc1451d91224b6f01fe4")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'RotatingCtrlSettings)))
  "Returns md5sum for a message object of type 'RotatingCtrlSettings"
  "3f7676b3fed2bc1451d91224b6f01fe4")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<RotatingCtrlSettings>)))
  "Returns full string definition for message of type '<RotatingCtrlSettings>"
  (cl:format cl:nil "float64 freq~%float64 rotAmp~%float64 fixAmp~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'RotatingCtrlSettings)))
  "Returns full string definition for message of type 'RotatingCtrlSettings"
  (cl:format cl:nil "float64 freq~%float64 rotAmp~%float64 fixAmp~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <RotatingCtrlSettings>))
  (cl:+ 0
     8
     8
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <RotatingCtrlSettings>))
  "Converts a ROS message object to a list"
  (cl:list 'RotatingCtrlSettings
    (cl:cons ':freq (freq msg))
    (cl:cons ':rotAmp (rotAmp msg))
    (cl:cons ':fixAmp (fixAmp msg))
))
