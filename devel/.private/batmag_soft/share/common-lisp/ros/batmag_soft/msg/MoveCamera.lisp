; Auto-generated. Do not edit!


(cl:in-package batmag_soft-msg)


;//! \htmlinclude MoveCamera.msg.html

(cl:defclass <MoveCamera> (roslisp-msg-protocol:ros-message)
  ((cameraSpeed
    :reader cameraSpeed
    :initarg :cameraSpeed
    :type (cl:vector cl:integer)
   :initform (cl:make-array 2 :element-type 'cl:integer :initial-element 0))
   (autofocusOn
    :reader autofocusOn
    :initarg :autofocusOn
    :type (cl:vector cl:boolean)
   :initform (cl:make-array 2 :element-type 'cl:boolean :initial-element cl:nil)))
)

(cl:defclass MoveCamera (<MoveCamera>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <MoveCamera>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'MoveCamera)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name batmag_soft-msg:<MoveCamera> is deprecated: use batmag_soft-msg:MoveCamera instead.")))

(cl:ensure-generic-function 'cameraSpeed-val :lambda-list '(m))
(cl:defmethod cameraSpeed-val ((m <MoveCamera>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:cameraSpeed-val is deprecated.  Use batmag_soft-msg:cameraSpeed instead.")
  (cameraSpeed m))

(cl:ensure-generic-function 'autofocusOn-val :lambda-list '(m))
(cl:defmethod autofocusOn-val ((m <MoveCamera>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:autofocusOn-val is deprecated.  Use batmag_soft-msg:autofocusOn instead.")
  (autofocusOn m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <MoveCamera>) ostream)
  "Serializes a message object of type '<MoveCamera>"
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let* ((signed ele) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 18446744073709551616) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) unsigned) ostream)
    ))
   (cl:slot-value msg 'cameraSpeed))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if ele 1 0)) ostream))
   (cl:slot-value msg 'autofocusOn))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <MoveCamera>) istream)
  "Deserializes a message object of type '<MoveCamera>"
  (cl:setf (cl:slot-value msg 'cameraSpeed) (cl:make-array 2))
  (cl:let ((vals (cl:slot-value msg 'cameraSpeed)))
    (cl:dotimes (i 2)
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) unsigned) (cl:read-byte istream))
      (cl:setf (cl:aref vals i) (cl:if (cl:< unsigned 9223372036854775808) unsigned (cl:- unsigned 18446744073709551616))))))
  (cl:setf (cl:slot-value msg 'autofocusOn) (cl:make-array 2))
  (cl:let ((vals (cl:slot-value msg 'autofocusOn)))
    (cl:dotimes (i 2)
    (cl:setf (cl:aref vals i) (cl:not (cl:zerop (cl:read-byte istream))))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<MoveCamera>)))
  "Returns string type for a message object of type '<MoveCamera>"
  "batmag_soft/MoveCamera")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'MoveCamera)))
  "Returns string type for a message object of type 'MoveCamera"
  "batmag_soft/MoveCamera")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<MoveCamera>)))
  "Returns md5sum for a message object of type '<MoveCamera>"
  "11601d08162551242a2c2c2bbcb261be")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'MoveCamera)))
  "Returns md5sum for a message object of type 'MoveCamera"
  "11601d08162551242a2c2c2bbcb261be")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<MoveCamera>)))
  "Returns full string definition for message of type '<MoveCamera>"
  (cl:format cl:nil "int64[2] cameraSpeed~%bool[2] autofocusOn~% ~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'MoveCamera)))
  "Returns full string definition for message of type 'MoveCamera"
  (cl:format cl:nil "int64[2] cameraSpeed~%bool[2] autofocusOn~% ~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <MoveCamera>))
  (cl:+ 0
     0 (cl:reduce #'cl:+ (cl:slot-value msg 'cameraSpeed) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 8)))
     0 (cl:reduce #'cl:+ (cl:slot-value msg 'autofocusOn) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 1)))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <MoveCamera>))
  "Converts a ROS message object to a list"
  (cl:list 'MoveCamera
    (cl:cons ':cameraSpeed (cameraSpeed msg))
    (cl:cons ':autofocusOn (autofocusOn msg))
))
