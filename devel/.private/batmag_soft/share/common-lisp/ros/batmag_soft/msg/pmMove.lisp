; Auto-generated. Do not edit!


(cl:in-package batmag_soft-msg)


;//! \htmlinclude pmMove.msg.html

(cl:defclass <pmMove> (roslisp-msg-protocol:ros-message)
  ((position
    :reader position
    :initarg :position
    :type cl:float
    :initform 0.0)
   (enable
    :reader enable
    :initarg :enable
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass pmMove (<pmMove>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <pmMove>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'pmMove)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name batmag_soft-msg:<pmMove> is deprecated: use batmag_soft-msg:pmMove instead.")))

(cl:ensure-generic-function 'position-val :lambda-list '(m))
(cl:defmethod position-val ((m <pmMove>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:position-val is deprecated.  Use batmag_soft-msg:position instead.")
  (position m))

(cl:ensure-generic-function 'enable-val :lambda-list '(m))
(cl:defmethod enable-val ((m <pmMove>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:enable-val is deprecated.  Use batmag_soft-msg:enable instead.")
  (enable m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <pmMove>) ostream)
  "Serializes a message object of type '<pmMove>"
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'position))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'enable) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <pmMove>) istream)
  "Deserializes a message object of type '<pmMove>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'position) (roslisp-utils:decode-double-float-bits bits)))
    (cl:setf (cl:slot-value msg 'enable) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<pmMove>)))
  "Returns string type for a message object of type '<pmMove>"
  "batmag_soft/pmMove")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'pmMove)))
  "Returns string type for a message object of type 'pmMove"
  "batmag_soft/pmMove")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<pmMove>)))
  "Returns md5sum for a message object of type '<pmMove>"
  "53037050e41b7763a8fbce1f9811be05")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'pmMove)))
  "Returns md5sum for a message object of type 'pmMove"
  "53037050e41b7763a8fbce1f9811be05")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<pmMove>)))
  "Returns full string definition for message of type '<pmMove>"
  (cl:format cl:nil "float64 position~%bool enable~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'pmMove)))
  "Returns full string definition for message of type 'pmMove"
  (cl:format cl:nil "float64 position~%bool enable~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <pmMove>))
  (cl:+ 0
     8
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <pmMove>))
  "Converts a ROS message object to a list"
  (cl:list 'pmMove
    (cl:cons ':position (position msg))
    (cl:cons ':enable (enable msg))
))
