; Auto-generated. Do not edit!


(cl:in-package batmag_soft-msg)


;//! \htmlinclude USroi.msg.html

(cl:defclass <USroi> (roslisp-msg-protocol:ros-message)
  ((width
    :reader width
    :initarg :width
    :type cl:integer
    :initform 0)
   (height
    :reader height
    :initarg :height
    :type cl:integer
    :initform 0)
   (topleftx
    :reader topleftx
    :initarg :topleftx
    :type cl:integer
    :initform 0)
   (toplefty
    :reader toplefty
    :initarg :toplefty
    :type cl:integer
    :initform 0))
)

(cl:defclass USroi (<USroi>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <USroi>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'USroi)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name batmag_soft-msg:<USroi> is deprecated: use batmag_soft-msg:USroi instead.")))

(cl:ensure-generic-function 'width-val :lambda-list '(m))
(cl:defmethod width-val ((m <USroi>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:width-val is deprecated.  Use batmag_soft-msg:width instead.")
  (width m))

(cl:ensure-generic-function 'height-val :lambda-list '(m))
(cl:defmethod height-val ((m <USroi>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:height-val is deprecated.  Use batmag_soft-msg:height instead.")
  (height m))

(cl:ensure-generic-function 'topleftx-val :lambda-list '(m))
(cl:defmethod topleftx-val ((m <USroi>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:topleftx-val is deprecated.  Use batmag_soft-msg:topleftx instead.")
  (topleftx m))

(cl:ensure-generic-function 'toplefty-val :lambda-list '(m))
(cl:defmethod toplefty-val ((m <USroi>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:toplefty-val is deprecated.  Use batmag_soft-msg:toplefty instead.")
  (toplefty m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <USroi>) ostream)
  "Serializes a message object of type '<USroi>"
  (cl:let* ((signed (cl:slot-value msg 'width)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'height)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'topleftx)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'toplefty)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <USroi>) istream)
  "Deserializes a message object of type '<USroi>"
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'width) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'height) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'topleftx) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'toplefty) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<USroi>)))
  "Returns string type for a message object of type '<USroi>"
  "batmag_soft/USroi")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'USroi)))
  "Returns string type for a message object of type 'USroi"
  "batmag_soft/USroi")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<USroi>)))
  "Returns md5sum for a message object of type '<USroi>"
  "e93dbc2b5418de3cd63fd0b87d53412f")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'USroi)))
  "Returns md5sum for a message object of type 'USroi"
  "e93dbc2b5418de3cd63fd0b87d53412f")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<USroi>)))
  "Returns full string definition for message of type '<USroi>"
  (cl:format cl:nil "int32 width~%int32 height~%int32 topleftx~%int32 toplefty~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'USroi)))
  "Returns full string definition for message of type 'USroi"
  (cl:format cl:nil "int32 width~%int32 height~%int32 topleftx~%int32 toplefty~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <USroi>))
  (cl:+ 0
     4
     4
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <USroi>))
  "Converts a ROS message object to a list"
  (cl:list 'USroi
    (cl:cons ':width (width msg))
    (cl:cons ':height (height msg))
    (cl:cons ':topleftx (topleftx msg))
    (cl:cons ':toplefty (toplefty msg))
))
