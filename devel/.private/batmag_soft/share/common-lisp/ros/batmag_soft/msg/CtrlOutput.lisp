; Auto-generated. Do not edit!


(cl:in-package batmag_soft-msg)


;//! \htmlinclude CtrlOutput.msg.html

(cl:defclass <CtrlOutput> (roslisp-msg-protocol:ros-message)
  ((ctrlout
    :reader ctrlout
    :initarg :ctrlout
    :type (cl:vector cl:float)
   :initform (cl:make-array 10 :element-type 'cl:float :initial-element 0.0))
   (size
    :reader size
    :initarg :size
    :type cl:integer
    :initform 0))
)

(cl:defclass CtrlOutput (<CtrlOutput>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <CtrlOutput>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'CtrlOutput)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name batmag_soft-msg:<CtrlOutput> is deprecated: use batmag_soft-msg:CtrlOutput instead.")))

(cl:ensure-generic-function 'ctrlout-val :lambda-list '(m))
(cl:defmethod ctrlout-val ((m <CtrlOutput>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:ctrlout-val is deprecated.  Use batmag_soft-msg:ctrlout instead.")
  (ctrlout m))

(cl:ensure-generic-function 'size-val :lambda-list '(m))
(cl:defmethod size-val ((m <CtrlOutput>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:size-val is deprecated.  Use batmag_soft-msg:size instead.")
  (size m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <CtrlOutput>) ostream)
  "Serializes a message object of type '<CtrlOutput>"
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-double-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream)))
   (cl:slot-value msg 'ctrlout))
  (cl:let* ((signed (cl:slot-value msg 'size)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 18446744073709551616) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <CtrlOutput>) istream)
  "Deserializes a message object of type '<CtrlOutput>"
  (cl:setf (cl:slot-value msg 'ctrlout) (cl:make-array 10))
  (cl:let ((vals (cl:slot-value msg 'ctrlout)))
    (cl:dotimes (i 10)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-double-float-bits bits)))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'size) (cl:if (cl:< unsigned 9223372036854775808) unsigned (cl:- unsigned 18446744073709551616))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<CtrlOutput>)))
  "Returns string type for a message object of type '<CtrlOutput>"
  "batmag_soft/CtrlOutput")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'CtrlOutput)))
  "Returns string type for a message object of type 'CtrlOutput"
  "batmag_soft/CtrlOutput")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<CtrlOutput>)))
  "Returns md5sum for a message object of type '<CtrlOutput>"
  "eab7552313b31041534bc8457d395d24")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'CtrlOutput)))
  "Returns md5sum for a message object of type 'CtrlOutput"
  "eab7552313b31041534bc8457d395d24")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<CtrlOutput>)))
  "Returns full string definition for message of type '<CtrlOutput>"
  (cl:format cl:nil "float64[10] ctrlout~%int64 size~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'CtrlOutput)))
  "Returns full string definition for message of type 'CtrlOutput"
  (cl:format cl:nil "float64[10] ctrlout~%int64 size~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <CtrlOutput>))
  (cl:+ 0
     0 (cl:reduce #'cl:+ (cl:slot-value msg 'ctrlout) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 8)))
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <CtrlOutput>))
  "Converts a ROS message object to a list"
  (cl:list 'CtrlOutput
    (cl:cons ':ctrlout (ctrlout msg))
    (cl:cons ':size (size msg))
))
