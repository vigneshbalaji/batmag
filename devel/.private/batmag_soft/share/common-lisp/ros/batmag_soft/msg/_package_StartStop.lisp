(cl:in-package batmag_soft-msg)
(cl:export '(PRESSEDSTART-VAL
          PRESSEDSTART
          PRESSEDSTOP-VAL
          PRESSEDSTOP
          MAP-VAL
          MAP
          AGENT-VAL
          AGENT
          IMAGING-VAL
          IMAGING
          CONTROL-VAL
          CONTROL
          ZOOM-VAL
          ZOOM
          ISUS-VAL
          ISUS
          USONLY-VAL
          USONLY
))