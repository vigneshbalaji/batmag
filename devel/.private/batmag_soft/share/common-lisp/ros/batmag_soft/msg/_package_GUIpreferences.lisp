(cl:in-package batmag_soft-msg)
(cl:export '(RECORDON-VAL
          RECORDON
          CTRLEN-VAL
          CTRLEN
          DEBUGEN-VAL
          DEBUGEN
          GRAVITYEN-VAL
          GRAVITYEN
          GRIDON-VAL
          GRIDON
          PMPOSITION-VAL
          PMPOSITION
          PMENABLE-VAL
          PMENABLE
          OMEGACTRL-VAL
          OMEGACTRL
))