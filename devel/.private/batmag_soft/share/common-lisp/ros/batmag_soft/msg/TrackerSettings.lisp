; Auto-generated. Do not edit!


(cl:in-package batmag_soft-msg)


;//! \htmlinclude TrackerSettings.msg.html

(cl:defclass <TrackerSettings> (roslisp-msg-protocol:ros-message)
  ((roiSize
    :reader roiSize
    :initarg :roiSize
    :type cl:integer
    :initform 0)
   (agentSize
    :reader agentSize
    :initarg :agentSize
    :type cl:integer
    :initform 0)
   (maxH
    :reader maxH
    :initarg :maxH
    :type cl:integer
    :initform 0)
   (minH
    :reader minH
    :initarg :minH
    :type cl:integer
    :initform 0)
   (maxS
    :reader maxS
    :initarg :maxS
    :type cl:integer
    :initform 0)
   (minS
    :reader minS
    :initarg :minS
    :type cl:integer
    :initform 0)
   (maxV
    :reader maxV
    :initarg :maxV
    :type cl:integer
    :initform 0)
   (minV
    :reader minV
    :initarg :minV
    :type cl:integer
    :initform 0)
   (displayOn
    :reader displayOn
    :initarg :displayOn
    :type cl:boolean
    :initform cl:nil)
   (blurSize
    :reader blurSize
    :initarg :blurSize
    :type cl:integer
    :initform 0)
   (fixed
    :reader fixed
    :initarg :fixed
    :type cl:boolean
    :initform cl:nil)
   (id
    :reader id
    :initarg :id
    :type cl:fixnum
    :initform 0)
   (backSubOn
    :reader backSubOn
    :initarg :backSubOn
    :type cl:boolean
    :initform cl:nil)
   (thisIsBack
    :reader thisIsBack
    :initarg :thisIsBack
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass TrackerSettings (<TrackerSettings>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <TrackerSettings>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'TrackerSettings)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name batmag_soft-msg:<TrackerSettings> is deprecated: use batmag_soft-msg:TrackerSettings instead.")))

(cl:ensure-generic-function 'roiSize-val :lambda-list '(m))
(cl:defmethod roiSize-val ((m <TrackerSettings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:roiSize-val is deprecated.  Use batmag_soft-msg:roiSize instead.")
  (roiSize m))

(cl:ensure-generic-function 'agentSize-val :lambda-list '(m))
(cl:defmethod agentSize-val ((m <TrackerSettings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:agentSize-val is deprecated.  Use batmag_soft-msg:agentSize instead.")
  (agentSize m))

(cl:ensure-generic-function 'maxH-val :lambda-list '(m))
(cl:defmethod maxH-val ((m <TrackerSettings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:maxH-val is deprecated.  Use batmag_soft-msg:maxH instead.")
  (maxH m))

(cl:ensure-generic-function 'minH-val :lambda-list '(m))
(cl:defmethod minH-val ((m <TrackerSettings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:minH-val is deprecated.  Use batmag_soft-msg:minH instead.")
  (minH m))

(cl:ensure-generic-function 'maxS-val :lambda-list '(m))
(cl:defmethod maxS-val ((m <TrackerSettings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:maxS-val is deprecated.  Use batmag_soft-msg:maxS instead.")
  (maxS m))

(cl:ensure-generic-function 'minS-val :lambda-list '(m))
(cl:defmethod minS-val ((m <TrackerSettings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:minS-val is deprecated.  Use batmag_soft-msg:minS instead.")
  (minS m))

(cl:ensure-generic-function 'maxV-val :lambda-list '(m))
(cl:defmethod maxV-val ((m <TrackerSettings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:maxV-val is deprecated.  Use batmag_soft-msg:maxV instead.")
  (maxV m))

(cl:ensure-generic-function 'minV-val :lambda-list '(m))
(cl:defmethod minV-val ((m <TrackerSettings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:minV-val is deprecated.  Use batmag_soft-msg:minV instead.")
  (minV m))

(cl:ensure-generic-function 'displayOn-val :lambda-list '(m))
(cl:defmethod displayOn-val ((m <TrackerSettings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:displayOn-val is deprecated.  Use batmag_soft-msg:displayOn instead.")
  (displayOn m))

(cl:ensure-generic-function 'blurSize-val :lambda-list '(m))
(cl:defmethod blurSize-val ((m <TrackerSettings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:blurSize-val is deprecated.  Use batmag_soft-msg:blurSize instead.")
  (blurSize m))

(cl:ensure-generic-function 'fixed-val :lambda-list '(m))
(cl:defmethod fixed-val ((m <TrackerSettings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:fixed-val is deprecated.  Use batmag_soft-msg:fixed instead.")
  (fixed m))

(cl:ensure-generic-function 'id-val :lambda-list '(m))
(cl:defmethod id-val ((m <TrackerSettings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:id-val is deprecated.  Use batmag_soft-msg:id instead.")
  (id m))

(cl:ensure-generic-function 'backSubOn-val :lambda-list '(m))
(cl:defmethod backSubOn-val ((m <TrackerSettings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:backSubOn-val is deprecated.  Use batmag_soft-msg:backSubOn instead.")
  (backSubOn m))

(cl:ensure-generic-function 'thisIsBack-val :lambda-list '(m))
(cl:defmethod thisIsBack-val ((m <TrackerSettings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:thisIsBack-val is deprecated.  Use batmag_soft-msg:thisIsBack instead.")
  (thisIsBack m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <TrackerSettings>) ostream)
  "Serializes a message object of type '<TrackerSettings>"
  (cl:let* ((signed (cl:slot-value msg 'roiSize)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'agentSize)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'maxH)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'minH)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'maxS)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'minS)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'maxV)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'minV)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'displayOn) 1 0)) ostream)
  (cl:let* ((signed (cl:slot-value msg 'blurSize)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'fixed) 1 0)) ostream)
  (cl:let* ((signed (cl:slot-value msg 'id)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 256) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    )
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'backSubOn) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'thisIsBack) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <TrackerSettings>) istream)
  "Deserializes a message object of type '<TrackerSettings>"
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'roiSize) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'agentSize) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'maxH) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'minH) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'maxS) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'minS) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'maxV) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'minV) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:setf (cl:slot-value msg 'displayOn) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'blurSize) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:setf (cl:slot-value msg 'fixed) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'id) (cl:if (cl:< unsigned 128) unsigned (cl:- unsigned 256))))
    (cl:setf (cl:slot-value msg 'backSubOn) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'thisIsBack) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<TrackerSettings>)))
  "Returns string type for a message object of type '<TrackerSettings>"
  "batmag_soft/TrackerSettings")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'TrackerSettings)))
  "Returns string type for a message object of type 'TrackerSettings"
  "batmag_soft/TrackerSettings")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<TrackerSettings>)))
  "Returns md5sum for a message object of type '<TrackerSettings>"
  "9374f1f81b270934cdd4bf73c3f6a28a")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'TrackerSettings)))
  "Returns md5sum for a message object of type 'TrackerSettings"
  "9374f1f81b270934cdd4bf73c3f6a28a")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<TrackerSettings>)))
  "Returns full string definition for message of type '<TrackerSettings>"
  (cl:format cl:nil "int32 roiSize~%int32 agentSize~%int32 maxH~%int32 minH~%int32 maxS~%int32 minS~%int32 maxV~%int32 minV~%bool displayOn~%int32 blurSize~%bool fixed~%int8 id~%bool backSubOn~%bool thisIsBack~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'TrackerSettings)))
  "Returns full string definition for message of type 'TrackerSettings"
  (cl:format cl:nil "int32 roiSize~%int32 agentSize~%int32 maxH~%int32 minH~%int32 maxS~%int32 minS~%int32 maxV~%int32 minV~%bool displayOn~%int32 blurSize~%bool fixed~%int8 id~%bool backSubOn~%bool thisIsBack~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <TrackerSettings>))
  (cl:+ 0
     4
     4
     4
     4
     4
     4
     4
     4
     1
     4
     1
     1
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <TrackerSettings>))
  "Converts a ROS message object to a list"
  (cl:list 'TrackerSettings
    (cl:cons ':roiSize (roiSize msg))
    (cl:cons ':agentSize (agentSize msg))
    (cl:cons ':maxH (maxH msg))
    (cl:cons ':minH (minH msg))
    (cl:cons ':maxS (maxS msg))
    (cl:cons ':minS (minS msg))
    (cl:cons ':maxV (maxV msg))
    (cl:cons ':minV (minV msg))
    (cl:cons ':displayOn (displayOn msg))
    (cl:cons ':blurSize (blurSize msg))
    (cl:cons ':fixed (fixed msg))
    (cl:cons ':id (id msg))
    (cl:cons ':backSubOn (backSubOn msg))
    (cl:cons ':thisIsBack (thisIsBack msg))
))
