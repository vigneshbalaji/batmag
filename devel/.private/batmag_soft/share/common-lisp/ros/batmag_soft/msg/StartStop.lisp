; Auto-generated. Do not edit!


(cl:in-package batmag_soft-msg)


;//! \htmlinclude StartStop.msg.html

(cl:defclass <StartStop> (roslisp-msg-protocol:ros-message)
  ((pressedStart
    :reader pressedStart
    :initarg :pressedStart
    :type cl:boolean
    :initform cl:nil)
   (pressedStop
    :reader pressedStop
    :initarg :pressedStop
    :type cl:boolean
    :initform cl:nil)
   (map
    :reader map
    :initarg :map
    :type cl:string
    :initform "")
   (agent
    :reader agent
    :initarg :agent
    :type cl:string
    :initform "")
   (imaging
    :reader imaging
    :initarg :imaging
    :type cl:string
    :initform "")
   (control
    :reader control
    :initarg :control
    :type cl:string
    :initform "")
   (zoom
    :reader zoom
    :initarg :zoom
    :type cl:float
    :initform 0.0)
   (isUS
    :reader isUS
    :initarg :isUS
    :type (cl:vector cl:boolean)
   :initform (cl:make-array 2 :element-type 'cl:boolean :initial-element cl:nil))
   (usOnly
    :reader usOnly
    :initarg :usOnly
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass StartStop (<StartStop>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <StartStop>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'StartStop)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name batmag_soft-msg:<StartStop> is deprecated: use batmag_soft-msg:StartStop instead.")))

(cl:ensure-generic-function 'pressedStart-val :lambda-list '(m))
(cl:defmethod pressedStart-val ((m <StartStop>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:pressedStart-val is deprecated.  Use batmag_soft-msg:pressedStart instead.")
  (pressedStart m))

(cl:ensure-generic-function 'pressedStop-val :lambda-list '(m))
(cl:defmethod pressedStop-val ((m <StartStop>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:pressedStop-val is deprecated.  Use batmag_soft-msg:pressedStop instead.")
  (pressedStop m))

(cl:ensure-generic-function 'map-val :lambda-list '(m))
(cl:defmethod map-val ((m <StartStop>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:map-val is deprecated.  Use batmag_soft-msg:map instead.")
  (map m))

(cl:ensure-generic-function 'agent-val :lambda-list '(m))
(cl:defmethod agent-val ((m <StartStop>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:agent-val is deprecated.  Use batmag_soft-msg:agent instead.")
  (agent m))

(cl:ensure-generic-function 'imaging-val :lambda-list '(m))
(cl:defmethod imaging-val ((m <StartStop>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:imaging-val is deprecated.  Use batmag_soft-msg:imaging instead.")
  (imaging m))

(cl:ensure-generic-function 'control-val :lambda-list '(m))
(cl:defmethod control-val ((m <StartStop>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:control-val is deprecated.  Use batmag_soft-msg:control instead.")
  (control m))

(cl:ensure-generic-function 'zoom-val :lambda-list '(m))
(cl:defmethod zoom-val ((m <StartStop>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:zoom-val is deprecated.  Use batmag_soft-msg:zoom instead.")
  (zoom m))

(cl:ensure-generic-function 'isUS-val :lambda-list '(m))
(cl:defmethod isUS-val ((m <StartStop>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:isUS-val is deprecated.  Use batmag_soft-msg:isUS instead.")
  (isUS m))

(cl:ensure-generic-function 'usOnly-val :lambda-list '(m))
(cl:defmethod usOnly-val ((m <StartStop>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader batmag_soft-msg:usOnly-val is deprecated.  Use batmag_soft-msg:usOnly instead.")
  (usOnly m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <StartStop>) ostream)
  "Serializes a message object of type '<StartStop>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'pressedStart) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'pressedStop) 1 0)) ostream)
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'map))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'map))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'agent))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'agent))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'imaging))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'imaging))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'control))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'control))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'zoom))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if ele 1 0)) ostream))
   (cl:slot-value msg 'isUS))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'usOnly) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <StartStop>) istream)
  "Deserializes a message object of type '<StartStop>"
    (cl:setf (cl:slot-value msg 'pressedStart) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'pressedStop) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'map) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'map) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'agent) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'agent) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'imaging) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'imaging) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'control) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'control) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'zoom) (roslisp-utils:decode-double-float-bits bits)))
  (cl:setf (cl:slot-value msg 'isUS) (cl:make-array 2))
  (cl:let ((vals (cl:slot-value msg 'isUS)))
    (cl:dotimes (i 2)
    (cl:setf (cl:aref vals i) (cl:not (cl:zerop (cl:read-byte istream))))))
    (cl:setf (cl:slot-value msg 'usOnly) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<StartStop>)))
  "Returns string type for a message object of type '<StartStop>"
  "batmag_soft/StartStop")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'StartStop)))
  "Returns string type for a message object of type 'StartStop"
  "batmag_soft/StartStop")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<StartStop>)))
  "Returns md5sum for a message object of type '<StartStop>"
  "fa1e23befc8b89be7ba72b2e18201c24")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'StartStop)))
  "Returns md5sum for a message object of type 'StartStop"
  "fa1e23befc8b89be7ba72b2e18201c24")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<StartStop>)))
  "Returns full string definition for message of type '<StartStop>"
  (cl:format cl:nil "bool pressedStart~%bool pressedStop~%string map~%string agent~%string imaging~%string control~%float64 zoom~%~%~%bool[2] isUS~%bool usOnly~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'StartStop)))
  "Returns full string definition for message of type 'StartStop"
  (cl:format cl:nil "bool pressedStart~%bool pressedStop~%string map~%string agent~%string imaging~%string control~%float64 zoom~%~%~%bool[2] isUS~%bool usOnly~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <StartStop>))
  (cl:+ 0
     1
     1
     4 (cl:length (cl:slot-value msg 'map))
     4 (cl:length (cl:slot-value msg 'agent))
     4 (cl:length (cl:slot-value msg 'imaging))
     4 (cl:length (cl:slot-value msg 'control))
     8
     0 (cl:reduce #'cl:+ (cl:slot-value msg 'isUS) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 1)))
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <StartStop>))
  "Converts a ROS message object to a list"
  (cl:list 'StartStop
    (cl:cons ':pressedStart (pressedStart msg))
    (cl:cons ':pressedStop (pressedStop msg))
    (cl:cons ':map (map msg))
    (cl:cons ':agent (agent msg))
    (cl:cons ':imaging (imaging msg))
    (cl:cons ':control (control msg))
    (cl:cons ':zoom (zoom msg))
    (cl:cons ':isUS (isUS msg))
    (cl:cons ':usOnly (usOnly msg))
))
