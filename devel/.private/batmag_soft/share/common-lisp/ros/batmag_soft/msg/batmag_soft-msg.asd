
(cl:in-package :asdf)

(defsystem "batmag_soft-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "CameraClick" :depends-on ("_package_CameraClick"))
    (:file "_package_CameraClick" :depends-on ("_package"))
    (:file "CameraSettings" :depends-on ("_package_CameraSettings"))
    (:file "_package_CameraSettings" :depends-on ("_package"))
    (:file "CtrlOutput" :depends-on ("_package_CtrlOutput"))
    (:file "_package_CtrlOutput" :depends-on ("_package"))
    (:file "ForceCurrentSet" :depends-on ("_package_ForceCurrentSet"))
    (:file "_package_ForceCurrentSet" :depends-on ("_package"))
    (:file "GUIpreferences" :depends-on ("_package_GUIpreferences"))
    (:file "_package_GUIpreferences" :depends-on ("_package"))
    (:file "MapOutput" :depends-on ("_package_MapOutput"))
    (:file "_package_MapOutput" :depends-on ("_package"))
    (:file "MoveCamera" :depends-on ("_package_MoveCamera"))
    (:file "_package_MoveCamera" :depends-on ("_package"))
    (:file "RecordRaw" :depends-on ("_package_RecordRaw"))
    (:file "_package_RecordRaw" :depends-on ("_package"))
    (:file "Reference" :depends-on ("_package_Reference"))
    (:file "_package_Reference" :depends-on ("_package"))
    (:file "ResetElmos" :depends-on ("_package_ResetElmos"))
    (:file "_package_ResetElmos" :depends-on ("_package"))
    (:file "RotatingCtrlSettings" :depends-on ("_package_RotatingCtrlSettings"))
    (:file "_package_RotatingCtrlSettings" :depends-on ("_package"))
    (:file "StartStop" :depends-on ("_package_StartStop"))
    (:file "_package_StartStop" :depends-on ("_package"))
    (:file "State" :depends-on ("_package_State"))
    (:file "_package_State" :depends-on ("_package"))
    (:file "TrackerSettings" :depends-on ("_package_TrackerSettings"))
    (:file "_package_TrackerSettings" :depends-on ("_package"))
    (:file "USroi" :depends-on ("_package_USroi"))
    (:file "_package_USroi" :depends-on ("_package"))
    (:file "WriteText" :depends-on ("_package_WriteText"))
    (:file "_package_WriteText" :depends-on ("_package"))
    (:file "pmMove" :depends-on ("_package_pmMove"))
    (:file "_package_pmMove" :depends-on ("_package"))
    (:file "refFilPar" :depends-on ("_package_refFilPar"))
    (:file "_package_refFilPar" :depends-on ("_package"))
  ))