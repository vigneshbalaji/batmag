/********************************************************************************
** Form generated from reading UI file 'frequencyctrlwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FREQUENCYCTRLWINDOW_H
#define UI_FREQUENCYCTRLWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>
#include "cqtopencvviewergl.h"

QT_BEGIN_NAMESPACE

class Ui_FrequencyCtrlWindow
{
public:
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label;
    QDoubleSpinBox *double_SpinBox_1;
    QDoubleSpinBox *double_SpinBox_2;
    QDoubleSpinBox *double_SpinBox_3;
    QPushButton *invertRotation_pushButton;
    QLabel *rot_label;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QPushButton *set_pushButton;
    QDialogButtonBox *buttonBox;
    CQtOpenCVViewerGl *graphicsWid_0;
    CQtOpenCVViewerGl *graphicsWid_1;

    void setupUi(QDialog *FrequencyCtrlWindow)
    {
        if (FrequencyCtrlWindow->objectName().isEmpty())
            FrequencyCtrlWindow->setObjectName(QStringLiteral("FrequencyCtrlWindow"));
        FrequencyCtrlWindow->resize(753, 289);
        gridLayoutWidget = new QWidget(FrequencyCtrlWindow);
        gridLayoutWidget->setObjectName(QStringLiteral("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(29, 19, 341, 181));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        label_2 = new QLabel(gridLayoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        label_3 = new QLabel(gridLayoutWidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout->addWidget(label_3, 2, 0, 1, 1);

        label = new QLabel(gridLayoutWidget);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        double_SpinBox_1 = new QDoubleSpinBox(gridLayoutWidget);
        double_SpinBox_1->setObjectName(QStringLiteral("double_SpinBox_1"));
        double_SpinBox_1->setValue(1);

        gridLayout->addWidget(double_SpinBox_1, 0, 1, 1, 1);

        double_SpinBox_2 = new QDoubleSpinBox(gridLayoutWidget);
        double_SpinBox_2->setObjectName(QStringLiteral("double_SpinBox_2"));
        double_SpinBox_2->setMinimum(-99);
        double_SpinBox_2->setValue(1);

        gridLayout->addWidget(double_SpinBox_2, 1, 1, 1, 1);

        double_SpinBox_3 = new QDoubleSpinBox(gridLayoutWidget);
        double_SpinBox_3->setObjectName(QStringLiteral("double_SpinBox_3"));
        double_SpinBox_3->setMinimum(-99);
        double_SpinBox_3->setValue(1);

        gridLayout->addWidget(double_SpinBox_3, 2, 1, 1, 1);

        invertRotation_pushButton = new QPushButton(gridLayoutWidget);
        invertRotation_pushButton->setObjectName(QStringLiteral("invertRotation_pushButton"));

        gridLayout->addWidget(invertRotation_pushButton, 3, 0, 1, 1);

        rot_label = new QLabel(gridLayoutWidget);
        rot_label->setObjectName(QStringLiteral("rot_label"));

        gridLayout->addWidget(rot_label, 3, 1, 1, 1);

        horizontalLayoutWidget = new QWidget(FrequencyCtrlWindow);
        horizontalLayoutWidget->setObjectName(QStringLiteral("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(30, 210, 341, 41));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        set_pushButton = new QPushButton(horizontalLayoutWidget);
        set_pushButton->setObjectName(QStringLiteral("set_pushButton"));

        horizontalLayout->addWidget(set_pushButton);

        buttonBox = new QDialogButtonBox(horizontalLayoutWidget);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        horizontalLayout->addWidget(buttonBox);

        graphicsWid_0 = new CQtOpenCVViewerGl(FrequencyCtrlWindow);
        graphicsWid_0->setObjectName(QStringLiteral("graphicsWid_0"));
        graphicsWid_0->setGeometry(QRect(400, 50, 150, 150));
        graphicsWid_1 = new CQtOpenCVViewerGl(FrequencyCtrlWindow);
        graphicsWid_1->setObjectName(QStringLiteral("graphicsWid_1"));
        graphicsWid_1->setGeometry(QRect(570, 50, 150, 150));

        retranslateUi(FrequencyCtrlWindow);
        QObject::connect(buttonBox, SIGNAL(accepted()), FrequencyCtrlWindow, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), FrequencyCtrlWindow, SLOT(reject()));

        QMetaObject::connectSlotsByName(FrequencyCtrlWindow);
    } // setupUi

    void retranslateUi(QDialog *FrequencyCtrlWindow)
    {
        FrequencyCtrlWindow->setWindowTitle(QApplication::translate("FrequencyCtrlWindow", "Dialog", Q_NULLPTR));
        label_2->setText(QApplication::translate("FrequencyCtrlWindow", "Rotating Field Amplitude [mT] T", Q_NULLPTR));
        label_3->setText(QApplication::translate("FrequencyCtrlWindow", "Fixed Field Amplitude [mT] T", Q_NULLPTR));
        label->setText(QApplication::translate("FrequencyCtrlWindow", "Frequency [Hz]: T", Q_NULLPTR));
        invertRotation_pushButton->setText(QApplication::translate("FrequencyCtrlWindow", "Invert Rotation", Q_NULLPTR));
        rot_label->setText(QApplication::translate("FrequencyCtrlWindow", "TextLabel", Q_NULLPTR));
        set_pushButton->setText(QApplication::translate("FrequencyCtrlWindow", "Set", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class FrequencyCtrlWindow: public Ui_FrequencyCtrlWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FREQUENCYCTRLWINDOW_H
