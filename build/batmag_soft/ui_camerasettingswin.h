/********************************************************************************
** Form generated from reading UI file 'camerasettingswin.ui'
**
** Created by: Qt User Interface Compiler version 5.9.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CAMERASETTINGSWIN_H
#define UI_CAMERASETTINGSWIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CameraSettingsWin
{
public:
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QCheckBox *immediate_checkBox;
    QPushButton *apply_Button;
    QPushButton *close_Button;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QLabel *label;
    QLabel *label_6;
    QLabel *label_9;
    QLabel *label_8;
    QDoubleSpinBox *exposure_doubleSpinBox;
    QLabel *label_3;
    QDoubleSpinBox *brightness_doubleSpinBox;
    QLabel *label_7;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_2;
    QLabel *label_10;
    QLabel *label_11;
    QDoubleSpinBox *sharp_doubleSpinBox;
    QDoubleSpinBox *hue_doubleSpinBox;
    QDoubleSpinBox *sat_doubleSpinBox;
    QDoubleSpinBox *gamma_doubleSpinBox;
    QDoubleSpinBox *shutter_doubleSpinBox;
    QDoubleSpinBox *gain_doubleSpinBox;
    QDoubleSpinBox *framerate_doubleSpinBox;
    QDoubleSpinBox *wbb_doubleSpinBox;
    QDoubleSpinBox *wbr_doubleSpinBox;

    void setupUi(QDialog *CameraSettingsWin)
    {
        if (CameraSettingsWin->objectName().isEmpty())
            CameraSettingsWin->setObjectName(QStringLiteral("CameraSettingsWin"));
        CameraSettingsWin->resize(400, 374);
        horizontalLayoutWidget = new QWidget(CameraSettingsWin);
        horizontalLayoutWidget->setObjectName(QStringLiteral("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(10, 330, 381, 31));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        immediate_checkBox = new QCheckBox(horizontalLayoutWidget);
        immediate_checkBox->setObjectName(QStringLiteral("immediate_checkBox"));

        horizontalLayout->addWidget(immediate_checkBox);

        apply_Button = new QPushButton(horizontalLayoutWidget);
        apply_Button->setObjectName(QStringLiteral("apply_Button"));

        horizontalLayout->addWidget(apply_Button);

        close_Button = new QPushButton(horizontalLayoutWidget);
        close_Button->setObjectName(QStringLiteral("close_Button"));

        horizontalLayout->addWidget(close_Button);

        gridLayoutWidget = new QWidget(CameraSettingsWin);
        gridLayoutWidget->setObjectName(QStringLiteral("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(10, 10, 381, 321));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(gridLayoutWidget);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        label_6 = new QLabel(gridLayoutWidget);
        label_6->setObjectName(QStringLiteral("label_6"));

        gridLayout->addWidget(label_6, 5, 0, 1, 1);

        label_9 = new QLabel(gridLayoutWidget);
        label_9->setObjectName(QStringLiteral("label_9"));

        gridLayout->addWidget(label_9, 8, 0, 1, 1);

        label_8 = new QLabel(gridLayoutWidget);
        label_8->setObjectName(QStringLiteral("label_8"));

        gridLayout->addWidget(label_8, 7, 0, 1, 1);

        exposure_doubleSpinBox = new QDoubleSpinBox(gridLayoutWidget);
        exposure_doubleSpinBox->setObjectName(QStringLiteral("exposure_doubleSpinBox"));
        exposure_doubleSpinBox->setMinimum(-20);
        exposure_doubleSpinBox->setMaximum(20);
        exposure_doubleSpinBox->setSingleStep(0.1);

        gridLayout->addWidget(exposure_doubleSpinBox, 1, 1, 1, 1);

        label_3 = new QLabel(gridLayoutWidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout->addWidget(label_3, 2, 0, 1, 1);

        brightness_doubleSpinBox = new QDoubleSpinBox(gridLayoutWidget);
        brightness_doubleSpinBox->setObjectName(QStringLiteral("brightness_doubleSpinBox"));
        brightness_doubleSpinBox->setMinimum(-2);
        brightness_doubleSpinBox->setMaximum(30);

        gridLayout->addWidget(brightness_doubleSpinBox, 0, 1, 1, 1);

        label_7 = new QLabel(gridLayoutWidget);
        label_7->setObjectName(QStringLiteral("label_7"));

        gridLayout->addWidget(label_7, 6, 0, 1, 1);

        label_4 = new QLabel(gridLayoutWidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout->addWidget(label_4, 3, 0, 1, 1);

        label_5 = new QLabel(gridLayoutWidget);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout->addWidget(label_5, 4, 0, 1, 1);

        label_2 = new QLabel(gridLayoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        label_10 = new QLabel(gridLayoutWidget);
        label_10->setObjectName(QStringLiteral("label_10"));

        gridLayout->addWidget(label_10, 9, 0, 1, 1);

        label_11 = new QLabel(gridLayoutWidget);
        label_11->setObjectName(QStringLiteral("label_11"));

        gridLayout->addWidget(label_11, 10, 0, 1, 1);

        sharp_doubleSpinBox = new QDoubleSpinBox(gridLayoutWidget);
        sharp_doubleSpinBox->setObjectName(QStringLiteral("sharp_doubleSpinBox"));
        sharp_doubleSpinBox->setMinimum(-10);
        sharp_doubleSpinBox->setMaximum(5000);

        gridLayout->addWidget(sharp_doubleSpinBox, 2, 1, 1, 1);

        hue_doubleSpinBox = new QDoubleSpinBox(gridLayoutWidget);
        hue_doubleSpinBox->setObjectName(QStringLiteral("hue_doubleSpinBox"));
        hue_doubleSpinBox->setMinimum(-200);
        hue_doubleSpinBox->setMaximum(200);

        gridLayout->addWidget(hue_doubleSpinBox, 3, 1, 1, 1);

        sat_doubleSpinBox = new QDoubleSpinBox(gridLayoutWidget);
        sat_doubleSpinBox->setObjectName(QStringLiteral("sat_doubleSpinBox"));
        sat_doubleSpinBox->setMinimum(-10);
        sat_doubleSpinBox->setMaximum(5000);

        gridLayout->addWidget(sat_doubleSpinBox, 4, 1, 1, 1);

        gamma_doubleSpinBox = new QDoubleSpinBox(gridLayoutWidget);
        gamma_doubleSpinBox->setObjectName(QStringLiteral("gamma_doubleSpinBox"));
        gamma_doubleSpinBox->setMinimum(-1);

        gridLayout->addWidget(gamma_doubleSpinBox, 5, 1, 1, 1);

        shutter_doubleSpinBox = new QDoubleSpinBox(gridLayoutWidget);
        shutter_doubleSpinBox->setObjectName(QStringLiteral("shutter_doubleSpinBox"));
        shutter_doubleSpinBox->setMinimum(-1);
        shutter_doubleSpinBox->setMaximum(500);

        gridLayout->addWidget(shutter_doubleSpinBox, 6, 1, 1, 1);

        gain_doubleSpinBox = new QDoubleSpinBox(gridLayoutWidget);
        gain_doubleSpinBox->setObjectName(QStringLiteral("gain_doubleSpinBox"));
        gain_doubleSpinBox->setMinimum(-1);
        gain_doubleSpinBox->setMaximum(20);

        gridLayout->addWidget(gain_doubleSpinBox, 7, 1, 1, 1);

        framerate_doubleSpinBox = new QDoubleSpinBox(gridLayoutWidget);
        framerate_doubleSpinBox->setObjectName(QStringLiteral("framerate_doubleSpinBox"));
        framerate_doubleSpinBox->setMinimum(-1);
        framerate_doubleSpinBox->setMaximum(90);

        gridLayout->addWidget(framerate_doubleSpinBox, 8, 1, 1, 1);

        wbb_doubleSpinBox = new QDoubleSpinBox(gridLayoutWidget);
        wbb_doubleSpinBox->setObjectName(QStringLiteral("wbb_doubleSpinBox"));
        wbb_doubleSpinBox->setMinimum(-1);
        wbb_doubleSpinBox->setMaximum(1024);

        gridLayout->addWidget(wbb_doubleSpinBox, 9, 1, 1, 1);

        wbr_doubleSpinBox = new QDoubleSpinBox(gridLayoutWidget);
        wbr_doubleSpinBox->setObjectName(QStringLiteral("wbr_doubleSpinBox"));
        wbr_doubleSpinBox->setMinimum(-1);
        wbr_doubleSpinBox->setMaximum(1024);

        gridLayout->addWidget(wbr_doubleSpinBox, 10, 1, 1, 1);


        retranslateUi(CameraSettingsWin);

        QMetaObject::connectSlotsByName(CameraSettingsWin);
    } // setupUi

    void retranslateUi(QDialog *CameraSettingsWin)
    {
        CameraSettingsWin->setWindowTitle(QApplication::translate("CameraSettingsWin", "Dialog", Q_NULLPTR));
        immediate_checkBox->setText(QApplication::translate("CameraSettingsWin", "Apply immediately", Q_NULLPTR));
        apply_Button->setText(QApplication::translate("CameraSettingsWin", "Apply", Q_NULLPTR));
        close_Button->setText(QApplication::translate("CameraSettingsWin", "Close", Q_NULLPTR));
        label->setText(QApplication::translate("CameraSettingsWin", "Brightness", Q_NULLPTR));
        label_6->setText(QApplication::translate("CameraSettingsWin", "Gamma", Q_NULLPTR));
        label_9->setText(QApplication::translate("CameraSettingsWin", "Frame Rate", Q_NULLPTR));
        label_8->setText(QApplication::translate("CameraSettingsWin", "Gain", Q_NULLPTR));
        label_3->setText(QApplication::translate("CameraSettingsWin", "Sharpness", Q_NULLPTR));
        label_7->setText(QApplication::translate("CameraSettingsWin", "Shutter", Q_NULLPTR));
        label_4->setText(QApplication::translate("CameraSettingsWin", "Hue", Q_NULLPTR));
        label_5->setText(QApplication::translate("CameraSettingsWin", "Saturation", Q_NULLPTR));
        label_2->setText(QApplication::translate("CameraSettingsWin", "Exposure", Q_NULLPTR));
        label_10->setText(QApplication::translate("CameraSettingsWin", "White Balance (Blue)", Q_NULLPTR));
        label_11->setText(QApplication::translate("CameraSettingsWin", "White Balance (Red)", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class CameraSettingsWin: public Ui_CameraSettingsWin {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CAMERASETTINGSWIN_H
