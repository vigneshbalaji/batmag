#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/batmag/batmag_ws/devel/.private/batmag_soft:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/batmag/batmag_ws/devel/.private/batmag_soft/lib:/home/batmag/batmag_ws/devel/lib:/opt/ros/lunar/lib:/opt/ros/lunar/lib/x86_64-linux-gnu:/opt/qt59/lib/x86_64-linux-gnu:/opt/qt59/lib:/opt/qt59/lib/x86_64-linux-gnu:/opt/qt59/lib"
export PATH="/opt/ros/lunar/bin:/opt/qt59/bin:/usr/bin:/opt/qt59/bin:/home/batmag/anaconda2/bin:/home/batmag/anaconda2/condabin:/opt/qt59/bin:/home/batmag/bin:/home/batmag/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin"
export PKG_CONFIG_PATH="/home/batmag/batmag_ws/devel/.private/batmag_soft/lib/pkgconfig:/home/batmag/batmag_ws/devel/lib/pkgconfig:/opt/ros/lunar/lib/pkgconfig:/opt/ros/lunar/lib/x86_64-linux-gnu/pkgconfig:/opt/qt59/lib/pkgconfig:/opt/qt59/lib/pkgconfig"
export PWD="/home/batmag/batmag_ws/build/batmag_soft"
export PYTHONPATH="/home/batmag/batmag_ws/devel/.private/batmag_soft/lib/python2.7/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/batmag/batmag_ws/devel/.private/batmag_soft/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/batmag/batmag_ws/src/batmag_soft:$ROS_PACKAGE_PATH"