/****************************************************************************
** Meta object code from reading C++ file 'roisettings.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/batmag_soft/src/gui/roisettings.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'roisettings.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_ROIsettings_t {
    QByteArrayData data[36];
    char stringdata0[840];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ROIsettings_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ROIsettings_t qt_meta_stringdata_ROIsettings = {
    {
QT_MOC_LITERAL(0, 0, 11), // "ROIsettings"
QT_MOC_LITERAL(1, 12, 11), // "newRoiImage"
QT_MOC_LITERAL(2, 24, 0), // ""
QT_MOC_LITERAL(3, 25, 9), // "cameraNum"
QT_MOC_LITERAL(4, 35, 10), // "newBsImage"
QT_MOC_LITERAL(5, 46, 14), // "on_newRoiImage"
QT_MOC_LITERAL(6, 61, 6), // "camNum"
QT_MOC_LITERAL(7, 68, 13), // "on_newBsImage"
QT_MOC_LITERAL(8, 82, 21), // "on_pushButton_clicked"
QT_MOC_LITERAL(9, 104, 30), // "on_sizeROI_Slider_valueChanged"
QT_MOC_LITERAL(10, 135, 5), // "value"
QT_MOC_LITERAL(11, 141, 32), // "on_agentSize_Slider_valueChanged"
QT_MOC_LITERAL(12, 174, 29), // "on_maxHue_Slider_valueChanged"
QT_MOC_LITERAL(13, 204, 29), // "on_minHue_Slider_valueChanged"
QT_MOC_LITERAL(14, 234, 29), // "on_maxVal_Slider_valueChanged"
QT_MOC_LITERAL(15, 264, 29), // "on_minVal_Slider_valueChanged"
QT_MOC_LITERAL(16, 294, 29), // "on_maxSat_Slider_valueChanged"
QT_MOC_LITERAL(17, 324, 29), // "on_minSat_Slider_valueChanged"
QT_MOC_LITERAL(18, 354, 27), // "on_blur_Slider_valueChanged"
QT_MOC_LITERAL(19, 382, 32), // "on_sizeROI_Slider_2_valueChanged"
QT_MOC_LITERAL(20, 415, 34), // "on_agentSize_Slider_2_valueCh..."
QT_MOC_LITERAL(21, 450, 31), // "on_maxHue_Slider_2_valueChanged"
QT_MOC_LITERAL(22, 482, 31), // "on_minHue_Slider_2_valueChanged"
QT_MOC_LITERAL(23, 514, 31), // "on_maxVal_Slider_2_valueChanged"
QT_MOC_LITERAL(24, 546, 31), // "on_minVal_Slider_2_valueChanged"
QT_MOC_LITERAL(25, 578, 31), // "on_maxSat_Slider_2_valueChanged"
QT_MOC_LITERAL(26, 610, 31), // "on_minSat_Slider_2_valueChanged"
QT_MOC_LITERAL(27, 642, 29), // "on_blur_Slider_2_valueChanged"
QT_MOC_LITERAL(28, 672, 25), // "on_fixed_checkBox_toggled"
QT_MOC_LITERAL(29, 698, 7), // "checked"
QT_MOC_LITERAL(30, 706, 27), // "on_fixed_checkBox_2_toggled"
QT_MOC_LITERAL(31, 734, 21), // "on_bs0_button_pressed"
QT_MOC_LITERAL(32, 756, 21), // "on_bs1_button_pressed"
QT_MOC_LITERAL(33, 778, 19), // "on_bs0check_toggled"
QT_MOC_LITERAL(34, 798, 19), // "on_bs1check_toggled"
QT_MOC_LITERAL(35, 818, 21) // "on_pushButton_pressed"

    },
    "ROIsettings\0newRoiImage\0\0cameraNum\0"
    "newBsImage\0on_newRoiImage\0camNum\0"
    "on_newBsImage\0on_pushButton_clicked\0"
    "on_sizeROI_Slider_valueChanged\0value\0"
    "on_agentSize_Slider_valueChanged\0"
    "on_maxHue_Slider_valueChanged\0"
    "on_minHue_Slider_valueChanged\0"
    "on_maxVal_Slider_valueChanged\0"
    "on_minVal_Slider_valueChanged\0"
    "on_maxSat_Slider_valueChanged\0"
    "on_minSat_Slider_valueChanged\0"
    "on_blur_Slider_valueChanged\0"
    "on_sizeROI_Slider_2_valueChanged\0"
    "on_agentSize_Slider_2_valueChanged\0"
    "on_maxHue_Slider_2_valueChanged\0"
    "on_minHue_Slider_2_valueChanged\0"
    "on_maxVal_Slider_2_valueChanged\0"
    "on_minVal_Slider_2_valueChanged\0"
    "on_maxSat_Slider_2_valueChanged\0"
    "on_minSat_Slider_2_valueChanged\0"
    "on_blur_Slider_2_valueChanged\0"
    "on_fixed_checkBox_toggled\0checked\0"
    "on_fixed_checkBox_2_toggled\0"
    "on_bs0_button_pressed\0on_bs1_button_pressed\0"
    "on_bs0check_toggled\0on_bs1check_toggled\0"
    "on_pushButton_pressed"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ROIsettings[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      30,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  164,    2, 0x06 /* Public */,
       4,    1,  167,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    1,  170,    2, 0x08 /* Private */,
       7,    1,  173,    2, 0x08 /* Private */,
       8,    0,  176,    2, 0x08 /* Private */,
       9,    1,  177,    2, 0x08 /* Private */,
      11,    1,  180,    2, 0x08 /* Private */,
      12,    1,  183,    2, 0x08 /* Private */,
      13,    1,  186,    2, 0x08 /* Private */,
      14,    1,  189,    2, 0x08 /* Private */,
      15,    1,  192,    2, 0x08 /* Private */,
      16,    1,  195,    2, 0x08 /* Private */,
      17,    1,  198,    2, 0x08 /* Private */,
      18,    1,  201,    2, 0x08 /* Private */,
      19,    1,  204,    2, 0x08 /* Private */,
      20,    1,  207,    2, 0x08 /* Private */,
      21,    1,  210,    2, 0x08 /* Private */,
      22,    1,  213,    2, 0x08 /* Private */,
      23,    1,  216,    2, 0x08 /* Private */,
      24,    1,  219,    2, 0x08 /* Private */,
      25,    1,  222,    2, 0x08 /* Private */,
      26,    1,  225,    2, 0x08 /* Private */,
      27,    1,  228,    2, 0x08 /* Private */,
      28,    1,  231,    2, 0x08 /* Private */,
      30,    1,  234,    2, 0x08 /* Private */,
      31,    0,  237,    2, 0x08 /* Private */,
      32,    0,  238,    2, 0x08 /* Private */,
      33,    1,  239,    2, 0x08 /* Private */,
      34,    1,  242,    2, 0x08 /* Private */,
      35,    0,  245,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    6,
    QMetaType::Void, QMetaType::Int,    6,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void, QMetaType::Bool,   29,
    QMetaType::Void, QMetaType::Bool,   29,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   29,
    QMetaType::Void, QMetaType::Bool,   29,
    QMetaType::Void,

       0        // eod
};

void ROIsettings::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ROIsettings *_t = static_cast<ROIsettings *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->newRoiImage((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->newBsImage((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->on_newRoiImage((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->on_newBsImage((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->on_pushButton_clicked(); break;
        case 5: _t->on_sizeROI_Slider_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->on_agentSize_Slider_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->on_maxHue_Slider_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->on_minHue_Slider_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->on_maxVal_Slider_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->on_minVal_Slider_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: _t->on_maxSat_Slider_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: _t->on_minSat_Slider_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: _t->on_blur_Slider_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 14: _t->on_sizeROI_Slider_2_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 15: _t->on_agentSize_Slider_2_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 16: _t->on_maxHue_Slider_2_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 17: _t->on_minHue_Slider_2_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 18: _t->on_maxVal_Slider_2_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 19: _t->on_minVal_Slider_2_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 20: _t->on_maxSat_Slider_2_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 21: _t->on_minSat_Slider_2_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 22: _t->on_blur_Slider_2_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 23: _t->on_fixed_checkBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 24: _t->on_fixed_checkBox_2_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 25: _t->on_bs0_button_pressed(); break;
        case 26: _t->on_bs1_button_pressed(); break;
        case 27: _t->on_bs0check_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 28: _t->on_bs1check_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 29: _t->on_pushButton_pressed(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (ROIsettings::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ROIsettings::newRoiImage)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (ROIsettings::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ROIsettings::newBsImage)) {
                *result = 1;
                return;
            }
        }
    }
}

const QMetaObject ROIsettings::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_ROIsettings.data,
      qt_meta_data_ROIsettings,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *ROIsettings::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ROIsettings::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ROIsettings.stringdata0))
        return static_cast<void*>(const_cast< ROIsettings*>(this));
    return QDialog::qt_metacast(_clname);
}

int ROIsettings::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 30)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 30;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 30)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 30;
    }
    return _id;
}

// SIGNAL 0
void ROIsettings::newRoiImage(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void ROIsettings::newBsImage(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
