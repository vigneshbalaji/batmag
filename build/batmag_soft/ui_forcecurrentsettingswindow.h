/********************************************************************************
** Form generated from reading UI file 'forcecurrentsettingswindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORCECURRENTSETTINGSWINDOW_H
#define UI_FORCECURRENTSETTINGSWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ForceCurrentSettingsWindow
{
public:
    QPushButton *ok_button;
    QPushButton *ApplyButton;
    QFrame *frame;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QLabel *label_2;
    QDoubleSpinBox *permeability_doubleSpinBox;
    QSlider *permeability_horizontalSlider;
    QDoubleSpinBox *radius_doubleSpinBox;
    QLabel *label;
    QDoubleSpinBox *br_doubleSpinBox;
    QSlider *radius_horizontalSlider;
    QSlider *br_horizontalSlider;
    QLabel *label_3;
    QSlider *density_horizontalSlider;
    QLabel *label_9;
    QFrame *line;
    QLabel *label_4;
    QSpinBox *density_spinBox;
    QFrame *frame_2;
    QWidget *gridLayoutWidget_2;
    QGridLayout *gridLayout_2;
    QDoubleSpinBox *br_doubleSpinBox_2;
    QSlider *radius_horizontalSlider_2;
    QSlider *br_horizontalSlider_2;
    QLabel *label_6;
    QDoubleSpinBox *permeability_doubleSpinBox_2;
    QSlider *permeability_horizontalSlider_2;
    QLabel *label_8;
    QDoubleSpinBox *radius_doubleSpinBox_2;
    QFrame *line_2;
    QLabel *label_7;
    QLabel *label_5;
    QCheckBox *diffAgent_checkBox;
    QCheckBox *applyImmediately_checkBox;

    void setupUi(QDialog *ForceCurrentSettingsWindow)
    {
        if (ForceCurrentSettingsWindow->objectName().isEmpty())
            ForceCurrentSettingsWindow->setObjectName(QStringLiteral("ForceCurrentSettingsWindow"));
        ForceCurrentSettingsWindow->resize(705, 738);
        ok_button = new QPushButton(ForceCurrentSettingsWindow);
        ok_button->setObjectName(QStringLiteral("ok_button"));
        ok_button->setGeometry(QRect(600, 290, 80, 22));
        ApplyButton = new QPushButton(ForceCurrentSettingsWindow);
        ApplyButton->setObjectName(QStringLiteral("ApplyButton"));
        ApplyButton->setGeometry(QRect(600, 380, 80, 22));
        frame = new QFrame(ForceCurrentSettingsWindow);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setGeometry(QRect(20, 20, 511, 311));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        gridLayoutWidget = new QWidget(frame);
        gridLayoutWidget->setObjectName(QStringLiteral("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(10, 10, 491, 291));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        label_2 = new QLabel(gridLayoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 1, 2, 1, 1);

        permeability_doubleSpinBox = new QDoubleSpinBox(gridLayoutWidget);
        permeability_doubleSpinBox->setObjectName(QStringLiteral("permeability_doubleSpinBox"));
        permeability_doubleSpinBox->setSingleStep(0.1);
        permeability_doubleSpinBox->setValue(0.6);

        gridLayout->addWidget(permeability_doubleSpinBox, 0, 4, 1, 1);

        permeability_horizontalSlider = new QSlider(gridLayoutWidget);
        permeability_horizontalSlider->setObjectName(QStringLiteral("permeability_horizontalSlider"));
        permeability_horizontalSlider->setMaximum(5000);
        permeability_horizontalSlider->setValue(1000);
        permeability_horizontalSlider->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(permeability_horizontalSlider, 0, 3, 1, 1);

        radius_doubleSpinBox = new QDoubleSpinBox(gridLayoutWidget);
        radius_doubleSpinBox->setObjectName(QStringLiteral("radius_doubleSpinBox"));
        radius_doubleSpinBox->setSingleStep(0.1);
        radius_doubleSpinBox->setValue(0.4);

        gridLayout->addWidget(radius_doubleSpinBox, 1, 4, 1, 1);

        label = new QLabel(gridLayoutWidget);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 2, 1, 1);

        br_doubleSpinBox = new QDoubleSpinBox(gridLayoutWidget);
        br_doubleSpinBox->setObjectName(QStringLiteral("br_doubleSpinBox"));
        br_doubleSpinBox->setSingleStep(0.1);
        br_doubleSpinBox->setValue(1);

        gridLayout->addWidget(br_doubleSpinBox, 2, 4, 1, 1);

        radius_horizontalSlider = new QSlider(gridLayoutWidget);
        radius_horizontalSlider->setObjectName(QStringLiteral("radius_horizontalSlider"));
        radius_horizontalSlider->setMaximum(5000);
        radius_horizontalSlider->setValue(1000);
        radius_horizontalSlider->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(radius_horizontalSlider, 1, 3, 1, 1);

        br_horizontalSlider = new QSlider(gridLayoutWidget);
        br_horizontalSlider->setObjectName(QStringLiteral("br_horizontalSlider"));
        br_horizontalSlider->setMaximum(50000);
        br_horizontalSlider->setValue(1000);
        br_horizontalSlider->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(br_horizontalSlider, 2, 3, 1, 1);

        label_3 = new QLabel(gridLayoutWidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout->addWidget(label_3, 2, 2, 1, 1);

        density_horizontalSlider = new QSlider(gridLayoutWidget);
        density_horizontalSlider->setObjectName(QStringLiteral("density_horizontalSlider"));
        density_horizontalSlider->setMaximum(15000);
        density_horizontalSlider->setValue(6680);
        density_horizontalSlider->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(density_horizontalSlider, 3, 3, 1, 1);

        label_9 = new QLabel(gridLayoutWidget);
        label_9->setObjectName(QStringLiteral("label_9"));

        gridLayout->addWidget(label_9, 3, 2, 1, 1);

        line = new QFrame(gridLayoutWidget);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(line, 0, 1, 4, 1);

        label_4 = new QLabel(gridLayoutWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setScaledContents(false);
        label_4->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_4, 0, 0, 4, 1);

        density_spinBox = new QSpinBox(gridLayoutWidget);
        density_spinBox->setObjectName(QStringLiteral("density_spinBox"));
        density_spinBox->setMaximum(15000);
        density_spinBox->setValue(6680);

        gridLayout->addWidget(density_spinBox, 3, 4, 1, 1);

        frame_2 = new QFrame(ForceCurrentSettingsWindow);
        frame_2->setObjectName(QStringLiteral("frame_2"));
        frame_2->setGeometry(QRect(20, 400, 511, 311));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        gridLayoutWidget_2 = new QWidget(frame_2);
        gridLayoutWidget_2->setObjectName(QStringLiteral("gridLayoutWidget_2"));
        gridLayoutWidget_2->setGeometry(QRect(10, 10, 491, 291));
        gridLayout_2 = new QGridLayout(gridLayoutWidget_2);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        br_doubleSpinBox_2 = new QDoubleSpinBox(gridLayoutWidget_2);
        br_doubleSpinBox_2->setObjectName(QStringLiteral("br_doubleSpinBox_2"));
        br_doubleSpinBox_2->setSingleStep(0.1);
        br_doubleSpinBox_2->setValue(1);

        gridLayout_2->addWidget(br_doubleSpinBox_2, 2, 4, 1, 1);

        radius_horizontalSlider_2 = new QSlider(gridLayoutWidget_2);
        radius_horizontalSlider_2->setObjectName(QStringLiteral("radius_horizontalSlider_2"));
        radius_horizontalSlider_2->setMaximum(5000);
        radius_horizontalSlider_2->setValue(1000);
        radius_horizontalSlider_2->setOrientation(Qt::Horizontal);

        gridLayout_2->addWidget(radius_horizontalSlider_2, 1, 3, 1, 1);

        br_horizontalSlider_2 = new QSlider(gridLayoutWidget_2);
        br_horizontalSlider_2->setObjectName(QStringLiteral("br_horizontalSlider_2"));
        br_horizontalSlider_2->setMaximum(50000);
        br_horizontalSlider_2->setValue(1000);
        br_horizontalSlider_2->setOrientation(Qt::Horizontal);

        gridLayout_2->addWidget(br_horizontalSlider_2, 2, 3, 1, 1);

        label_6 = new QLabel(gridLayoutWidget_2);
        label_6->setObjectName(QStringLiteral("label_6"));

        gridLayout_2->addWidget(label_6, 2, 2, 1, 1);

        permeability_doubleSpinBox_2 = new QDoubleSpinBox(gridLayoutWidget_2);
        permeability_doubleSpinBox_2->setObjectName(QStringLiteral("permeability_doubleSpinBox_2"));
        permeability_doubleSpinBox_2->setSingleStep(0.1);
        permeability_doubleSpinBox_2->setValue(1);

        gridLayout_2->addWidget(permeability_doubleSpinBox_2, 0, 4, 1, 1);

        permeability_horizontalSlider_2 = new QSlider(gridLayoutWidget_2);
        permeability_horizontalSlider_2->setObjectName(QStringLiteral("permeability_horizontalSlider_2"));
        permeability_horizontalSlider_2->setMaximum(5000);
        permeability_horizontalSlider_2->setValue(1000);
        permeability_horizontalSlider_2->setOrientation(Qt::Horizontal);

        gridLayout_2->addWidget(permeability_horizontalSlider_2, 0, 3, 1, 1);

        label_8 = new QLabel(gridLayoutWidget_2);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setScaledContents(false);
        label_8->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_8, 0, 0, 3, 1);

        radius_doubleSpinBox_2 = new QDoubleSpinBox(gridLayoutWidget_2);
        radius_doubleSpinBox_2->setObjectName(QStringLiteral("radius_doubleSpinBox_2"));
        radius_doubleSpinBox_2->setSingleStep(0.1);
        radius_doubleSpinBox_2->setValue(1);

        gridLayout_2->addWidget(radius_doubleSpinBox_2, 1, 4, 1, 1);

        line_2 = new QFrame(gridLayoutWidget_2);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setFrameShape(QFrame::VLine);
        line_2->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line_2, 0, 1, 3, 1);

        label_7 = new QLabel(gridLayoutWidget_2);
        label_7->setObjectName(QStringLiteral("label_7"));

        gridLayout_2->addWidget(label_7, 1, 2, 1, 1);

        label_5 = new QLabel(gridLayoutWidget_2);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout_2->addWidget(label_5, 0, 2, 1, 1);

        diffAgent_checkBox = new QCheckBox(ForceCurrentSettingsWindow);
        diffAgent_checkBox->setObjectName(QStringLiteral("diffAgent_checkBox"));
        diffAgent_checkBox->setGeometry(QRect(220, 350, 141, 20));
        applyImmediately_checkBox = new QCheckBox(ForceCurrentSettingsWindow);
        applyImmediately_checkBox->setObjectName(QStringLiteral("applyImmediately_checkBox"));
        applyImmediately_checkBox->setGeometry(QRect(554, 330, 131, 31));

        retranslateUi(ForceCurrentSettingsWindow);
        QObject::connect(density_horizontalSlider, SIGNAL(valueChanged(int)), density_spinBox, SLOT(setValue(int)));
        QObject::connect(density_spinBox, SIGNAL(valueChanged(int)), density_horizontalSlider, SLOT(setValue(int)));

        QMetaObject::connectSlotsByName(ForceCurrentSettingsWindow);
    } // setupUi

    void retranslateUi(QDialog *ForceCurrentSettingsWindow)
    {
        ForceCurrentSettingsWindow->setWindowTitle(QApplication::translate("ForceCurrentSettingsWindow", "Dialog", Q_NULLPTR));
        ok_button->setText(QApplication::translate("ForceCurrentSettingsWindow", "Ok", Q_NULLPTR));
        ApplyButton->setText(QApplication::translate("ForceCurrentSettingsWindow", "Apply", Q_NULLPTR));
        label_2->setText(QApplication::translate("ForceCurrentSettingsWindow", "<html><head/><body><p align=\"center\">Radius</p><p align=\"center\">[tenth of mm]</p></body></html>", Q_NULLPTR));
        label->setText(QApplication::translate("ForceCurrentSettingsWindow", "<html><head/><body><p>Permeability (\302\265<span style=\" vertical-align:sub;\">r</span>)</p></body></html>", Q_NULLPTR));
        label_3->setText(QApplication::translate("ForceCurrentSettingsWindow", "<html><head/><body><p align=\"center\">Res. Mag. (B<span style=\" vertical-align:sub;\">r</span>)</p><p align=\"center\">[\302\265T]</p></body></html>", Q_NULLPTR));
        label_9->setText(QApplication::translate("ForceCurrentSettingsWindow", "<html><head/><body><p>Density [Kd/m<span style=\" vertical-align:super;\">3</span>]</p></body></html>", Q_NULLPTR));
        label_4->setText(QApplication::translate("ForceCurrentSettingsWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt; font-weight:600;\">A</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt; font-weight:600;\">g</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt; font-weight:600;\">e</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0p"
                        "x;\"><span style=\" font-size:16pt; font-weight:600;\">n</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt; font-weight:600;\">t </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt; font-weight:600;\">1</span></p></body></html>", Q_NULLPTR));
        label_6->setText(QApplication::translate("ForceCurrentSettingsWindow", "<html><head/><body><p align=\"center\">Res. Mag. (B<span style=\" vertical-align:sub;\">r</span>)</p><p align=\"center\">[\302\265T]</p></body></html>", Q_NULLPTR));
        label_8->setText(QApplication::translate("ForceCurrentSettingsWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt; font-weight:600;\">A</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt; font-weight:600;\">g</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt; font-weight:600;\">e</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0p"
                        "x;\"><span style=\" font-size:16pt; font-weight:600;\">n</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt; font-weight:600;\">t </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt; font-weight:600;\">2</span></p></body></html>", Q_NULLPTR));
        label_7->setText(QApplication::translate("ForceCurrentSettingsWindow", "<html><head/><body><p align=\"center\">Radius [mm]</p></body></html>", Q_NULLPTR));
        label_5->setText(QApplication::translate("ForceCurrentSettingsWindow", "<html><head/><body><p>Permeability (\302\265<span style=\" vertical-align:sub;\">r</span>)</p></body></html>", Q_NULLPTR));
        diffAgent_checkBox->setText(QApplication::translate("ForceCurrentSettingsWindow", "Different Agents", Q_NULLPTR));
        applyImmediately_checkBox->setText(QApplication::translate("ForceCurrentSettingsWindow", "Apply Immediately", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class ForceCurrentSettingsWindow: public Ui_ForceCurrentSettingsWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORCECURRENTSETTINGSWINDOW_H
