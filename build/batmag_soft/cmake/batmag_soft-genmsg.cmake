# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "batmag_soft: 18 messages, 6 services")

set(MSG_I_FLAGS "-Ibatmag_soft:/home/batmag/batmag_ws/src/batmag_soft/msg;-Istd_msgs:/opt/ros/lunar/share/std_msgs/cmake/../msg;-Iactionlib_msgs:/opt/ros/lunar/share/actionlib_msgs/cmake/../msg;-Isensor_msgs:/opt/ros/lunar/share/sensor_msgs/cmake/../msg;-Igeometry_msgs:/opt/ros/lunar/share/geometry_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(batmag_soft_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/refFilPar.msg" NAME_WE)
add_custom_target(_batmag_soft_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "batmag_soft" "/home/batmag/batmag_ws/src/batmag_soft/msg/refFilPar.msg" ""
)

get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/USroi.msg" NAME_WE)
add_custom_target(_batmag_soft_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "batmag_soft" "/home/batmag/batmag_ws/src/batmag_soft/msg/USroi.msg" ""
)

get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/RecordRaw.msg" NAME_WE)
add_custom_target(_batmag_soft_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "batmag_soft" "/home/batmag/batmag_ws/src/batmag_soft/msg/RecordRaw.msg" ""
)

get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/srv/SaveImage.srv" NAME_WE)
add_custom_target(_batmag_soft_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "batmag_soft" "/home/batmag/batmag_ws/src/batmag_soft/srv/SaveImage.srv" ""
)

get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/Reference.msg" NAME_WE)
add_custom_target(_batmag_soft_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "batmag_soft" "/home/batmag/batmag_ws/src/batmag_soft/msg/Reference.msg" ""
)

get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/ResetElmos.msg" NAME_WE)
add_custom_target(_batmag_soft_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "batmag_soft" "/home/batmag/batmag_ws/src/batmag_soft/msg/ResetElmos.msg" ""
)

get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/srv/getInitTrackerSettings.srv" NAME_WE)
add_custom_target(_batmag_soft_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "batmag_soft" "/home/batmag/batmag_ws/src/batmag_soft/srv/getInitTrackerSettings.srv" ""
)

get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/pmMove.msg" NAME_WE)
add_custom_target(_batmag_soft_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "batmag_soft" "/home/batmag/batmag_ws/src/batmag_soft/msg/pmMove.msg" ""
)

get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/CameraClick.msg" NAME_WE)
add_custom_target(_batmag_soft_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "batmag_soft" "/home/batmag/batmag_ws/src/batmag_soft/msg/CameraClick.msg" ""
)

get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/MoveCamera.msg" NAME_WE)
add_custom_target(_batmag_soft_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "batmag_soft" "/home/batmag/batmag_ws/src/batmag_soft/msg/MoveCamera.msg" ""
)

get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/TrackerSettings.msg" NAME_WE)
add_custom_target(_batmag_soft_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "batmag_soft" "/home/batmag/batmag_ws/src/batmag_soft/msg/TrackerSettings.msg" ""
)

get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/CtrlOutput.msg" NAME_WE)
add_custom_target(_batmag_soft_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "batmag_soft" "/home/batmag/batmag_ws/src/batmag_soft/msg/CtrlOutput.msg" ""
)

get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/WriteText.msg" NAME_WE)
add_custom_target(_batmag_soft_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "batmag_soft" "/home/batmag/batmag_ws/src/batmag_soft/msg/WriteText.msg" ""
)

get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/MapOutput.msg" NAME_WE)
add_custom_target(_batmag_soft_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "batmag_soft" "/home/batmag/batmag_ws/src/batmag_soft/msg/MapOutput.msg" ""
)

get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/srv/GetInitParameters.srv" NAME_WE)
add_custom_target(_batmag_soft_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "batmag_soft" "/home/batmag/batmag_ws/src/batmag_soft/srv/GetInitParameters.srv" ""
)

get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/GUIpreferences.msg" NAME_WE)
add_custom_target(_batmag_soft_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "batmag_soft" "/home/batmag/batmag_ws/src/batmag_soft/msg/GUIpreferences.msg" ""
)

get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/StartStop.msg" NAME_WE)
add_custom_target(_batmag_soft_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "batmag_soft" "/home/batmag/batmag_ws/src/batmag_soft/msg/StartStop.msg" ""
)

get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/RotatingCtrlSettings.msg" NAME_WE)
add_custom_target(_batmag_soft_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "batmag_soft" "/home/batmag/batmag_ws/src/batmag_soft/msg/RotatingCtrlSettings.msg" ""
)

get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/srv/ForceCurrentSettings.srv" NAME_WE)
add_custom_target(_batmag_soft_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "batmag_soft" "/home/batmag/batmag_ws/src/batmag_soft/srv/ForceCurrentSettings.srv" ""
)

get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/State.msg" NAME_WE)
add_custom_target(_batmag_soft_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "batmag_soft" "/home/batmag/batmag_ws/src/batmag_soft/msg/State.msg" ""
)

get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/srv/SetInitParameters.srv" NAME_WE)
add_custom_target(_batmag_soft_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "batmag_soft" "/home/batmag/batmag_ws/src/batmag_soft/srv/SetInitParameters.srv" ""
)

get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/CameraSettings.msg" NAME_WE)
add_custom_target(_batmag_soft_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "batmag_soft" "/home/batmag/batmag_ws/src/batmag_soft/msg/CameraSettings.msg" ""
)

get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/ForceCurrentSet.msg" NAME_WE)
add_custom_target(_batmag_soft_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "batmag_soft" "/home/batmag/batmag_ws/src/batmag_soft/msg/ForceCurrentSet.msg" ""
)

get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/srv/getCameraSettings.srv" NAME_WE)
add_custom_target(_batmag_soft_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "batmag_soft" "/home/batmag/batmag_ws/src/batmag_soft/srv/getCameraSettings.srv" ""
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/CameraClick.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/batmag_soft
)
_generate_msg_cpp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/USroi.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/batmag_soft
)
_generate_msg_cpp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/MapOutput.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/batmag_soft
)
_generate_msg_cpp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/RecordRaw.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/batmag_soft
)
_generate_msg_cpp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/MoveCamera.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/batmag_soft
)
_generate_msg_cpp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/GUIpreferences.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/batmag_soft
)
_generate_msg_cpp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/StartStop.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/batmag_soft
)
_generate_msg_cpp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/TrackerSettings.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/batmag_soft
)
_generate_msg_cpp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/Reference.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/batmag_soft
)
_generate_msg_cpp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/WriteText.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/batmag_soft
)
_generate_msg_cpp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/ResetElmos.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/batmag_soft
)
_generate_msg_cpp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/refFilPar.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/batmag_soft
)
_generate_msg_cpp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/CameraSettings.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/batmag_soft
)
_generate_msg_cpp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/State.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/batmag_soft
)
_generate_msg_cpp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/ForceCurrentSet.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/batmag_soft
)
_generate_msg_cpp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/CtrlOutput.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/batmag_soft
)
_generate_msg_cpp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/RotatingCtrlSettings.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/batmag_soft
)
_generate_msg_cpp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/pmMove.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/batmag_soft
)

### Generating Services
_generate_srv_cpp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/srv/SetInitParameters.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/batmag_soft
)
_generate_srv_cpp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/srv/GetInitParameters.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/batmag_soft
)
_generate_srv_cpp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/srv/SaveImage.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/batmag_soft
)
_generate_srv_cpp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/srv/ForceCurrentSettings.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/batmag_soft
)
_generate_srv_cpp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/srv/getInitTrackerSettings.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/batmag_soft
)
_generate_srv_cpp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/srv/getCameraSettings.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/batmag_soft
)

### Generating Module File
_generate_module_cpp(batmag_soft
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/batmag_soft
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(batmag_soft_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(batmag_soft_generate_messages batmag_soft_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/refFilPar.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_cpp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/USroi.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_cpp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/RecordRaw.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_cpp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/srv/SaveImage.srv" NAME_WE)
add_dependencies(batmag_soft_generate_messages_cpp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/Reference.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_cpp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/ResetElmos.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_cpp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/srv/getInitTrackerSettings.srv" NAME_WE)
add_dependencies(batmag_soft_generate_messages_cpp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/pmMove.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_cpp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/CameraClick.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_cpp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/MoveCamera.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_cpp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/TrackerSettings.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_cpp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/CtrlOutput.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_cpp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/WriteText.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_cpp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/MapOutput.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_cpp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/srv/GetInitParameters.srv" NAME_WE)
add_dependencies(batmag_soft_generate_messages_cpp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/GUIpreferences.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_cpp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/StartStop.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_cpp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/RotatingCtrlSettings.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_cpp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/srv/ForceCurrentSettings.srv" NAME_WE)
add_dependencies(batmag_soft_generate_messages_cpp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/State.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_cpp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/srv/SetInitParameters.srv" NAME_WE)
add_dependencies(batmag_soft_generate_messages_cpp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/CameraSettings.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_cpp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/ForceCurrentSet.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_cpp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/srv/getCameraSettings.srv" NAME_WE)
add_dependencies(batmag_soft_generate_messages_cpp _batmag_soft_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(batmag_soft_gencpp)
add_dependencies(batmag_soft_gencpp batmag_soft_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS batmag_soft_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/CameraClick.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/batmag_soft
)
_generate_msg_eus(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/USroi.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/batmag_soft
)
_generate_msg_eus(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/MapOutput.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/batmag_soft
)
_generate_msg_eus(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/RecordRaw.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/batmag_soft
)
_generate_msg_eus(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/MoveCamera.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/batmag_soft
)
_generate_msg_eus(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/GUIpreferences.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/batmag_soft
)
_generate_msg_eus(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/StartStop.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/batmag_soft
)
_generate_msg_eus(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/TrackerSettings.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/batmag_soft
)
_generate_msg_eus(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/Reference.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/batmag_soft
)
_generate_msg_eus(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/WriteText.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/batmag_soft
)
_generate_msg_eus(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/ResetElmos.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/batmag_soft
)
_generate_msg_eus(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/refFilPar.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/batmag_soft
)
_generate_msg_eus(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/CameraSettings.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/batmag_soft
)
_generate_msg_eus(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/State.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/batmag_soft
)
_generate_msg_eus(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/ForceCurrentSet.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/batmag_soft
)
_generate_msg_eus(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/CtrlOutput.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/batmag_soft
)
_generate_msg_eus(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/RotatingCtrlSettings.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/batmag_soft
)
_generate_msg_eus(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/pmMove.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/batmag_soft
)

### Generating Services
_generate_srv_eus(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/srv/SetInitParameters.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/batmag_soft
)
_generate_srv_eus(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/srv/GetInitParameters.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/batmag_soft
)
_generate_srv_eus(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/srv/SaveImage.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/batmag_soft
)
_generate_srv_eus(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/srv/ForceCurrentSettings.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/batmag_soft
)
_generate_srv_eus(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/srv/getInitTrackerSettings.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/batmag_soft
)
_generate_srv_eus(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/srv/getCameraSettings.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/batmag_soft
)

### Generating Module File
_generate_module_eus(batmag_soft
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/batmag_soft
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(batmag_soft_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(batmag_soft_generate_messages batmag_soft_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/refFilPar.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_eus _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/USroi.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_eus _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/RecordRaw.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_eus _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/srv/SaveImage.srv" NAME_WE)
add_dependencies(batmag_soft_generate_messages_eus _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/Reference.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_eus _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/ResetElmos.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_eus _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/srv/getInitTrackerSettings.srv" NAME_WE)
add_dependencies(batmag_soft_generate_messages_eus _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/pmMove.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_eus _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/CameraClick.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_eus _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/MoveCamera.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_eus _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/TrackerSettings.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_eus _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/CtrlOutput.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_eus _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/WriteText.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_eus _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/MapOutput.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_eus _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/srv/GetInitParameters.srv" NAME_WE)
add_dependencies(batmag_soft_generate_messages_eus _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/GUIpreferences.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_eus _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/StartStop.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_eus _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/RotatingCtrlSettings.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_eus _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/srv/ForceCurrentSettings.srv" NAME_WE)
add_dependencies(batmag_soft_generate_messages_eus _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/State.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_eus _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/srv/SetInitParameters.srv" NAME_WE)
add_dependencies(batmag_soft_generate_messages_eus _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/CameraSettings.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_eus _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/ForceCurrentSet.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_eus _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/srv/getCameraSettings.srv" NAME_WE)
add_dependencies(batmag_soft_generate_messages_eus _batmag_soft_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(batmag_soft_geneus)
add_dependencies(batmag_soft_geneus batmag_soft_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS batmag_soft_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/CameraClick.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/batmag_soft
)
_generate_msg_lisp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/USroi.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/batmag_soft
)
_generate_msg_lisp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/MapOutput.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/batmag_soft
)
_generate_msg_lisp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/RecordRaw.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/batmag_soft
)
_generate_msg_lisp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/MoveCamera.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/batmag_soft
)
_generate_msg_lisp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/GUIpreferences.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/batmag_soft
)
_generate_msg_lisp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/StartStop.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/batmag_soft
)
_generate_msg_lisp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/TrackerSettings.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/batmag_soft
)
_generate_msg_lisp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/Reference.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/batmag_soft
)
_generate_msg_lisp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/WriteText.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/batmag_soft
)
_generate_msg_lisp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/ResetElmos.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/batmag_soft
)
_generate_msg_lisp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/refFilPar.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/batmag_soft
)
_generate_msg_lisp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/CameraSettings.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/batmag_soft
)
_generate_msg_lisp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/State.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/batmag_soft
)
_generate_msg_lisp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/ForceCurrentSet.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/batmag_soft
)
_generate_msg_lisp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/CtrlOutput.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/batmag_soft
)
_generate_msg_lisp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/RotatingCtrlSettings.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/batmag_soft
)
_generate_msg_lisp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/pmMove.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/batmag_soft
)

### Generating Services
_generate_srv_lisp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/srv/SetInitParameters.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/batmag_soft
)
_generate_srv_lisp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/srv/GetInitParameters.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/batmag_soft
)
_generate_srv_lisp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/srv/SaveImage.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/batmag_soft
)
_generate_srv_lisp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/srv/ForceCurrentSettings.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/batmag_soft
)
_generate_srv_lisp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/srv/getInitTrackerSettings.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/batmag_soft
)
_generate_srv_lisp(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/srv/getCameraSettings.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/batmag_soft
)

### Generating Module File
_generate_module_lisp(batmag_soft
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/batmag_soft
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(batmag_soft_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(batmag_soft_generate_messages batmag_soft_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/refFilPar.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_lisp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/USroi.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_lisp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/RecordRaw.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_lisp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/srv/SaveImage.srv" NAME_WE)
add_dependencies(batmag_soft_generate_messages_lisp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/Reference.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_lisp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/ResetElmos.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_lisp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/srv/getInitTrackerSettings.srv" NAME_WE)
add_dependencies(batmag_soft_generate_messages_lisp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/pmMove.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_lisp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/CameraClick.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_lisp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/MoveCamera.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_lisp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/TrackerSettings.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_lisp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/CtrlOutput.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_lisp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/WriteText.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_lisp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/MapOutput.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_lisp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/srv/GetInitParameters.srv" NAME_WE)
add_dependencies(batmag_soft_generate_messages_lisp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/GUIpreferences.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_lisp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/StartStop.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_lisp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/RotatingCtrlSettings.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_lisp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/srv/ForceCurrentSettings.srv" NAME_WE)
add_dependencies(batmag_soft_generate_messages_lisp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/State.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_lisp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/srv/SetInitParameters.srv" NAME_WE)
add_dependencies(batmag_soft_generate_messages_lisp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/CameraSettings.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_lisp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/ForceCurrentSet.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_lisp _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/srv/getCameraSettings.srv" NAME_WE)
add_dependencies(batmag_soft_generate_messages_lisp _batmag_soft_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(batmag_soft_genlisp)
add_dependencies(batmag_soft_genlisp batmag_soft_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS batmag_soft_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/CameraClick.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/batmag_soft
)
_generate_msg_nodejs(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/USroi.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/batmag_soft
)
_generate_msg_nodejs(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/MapOutput.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/batmag_soft
)
_generate_msg_nodejs(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/RecordRaw.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/batmag_soft
)
_generate_msg_nodejs(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/MoveCamera.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/batmag_soft
)
_generate_msg_nodejs(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/GUIpreferences.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/batmag_soft
)
_generate_msg_nodejs(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/StartStop.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/batmag_soft
)
_generate_msg_nodejs(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/TrackerSettings.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/batmag_soft
)
_generate_msg_nodejs(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/Reference.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/batmag_soft
)
_generate_msg_nodejs(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/WriteText.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/batmag_soft
)
_generate_msg_nodejs(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/ResetElmos.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/batmag_soft
)
_generate_msg_nodejs(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/refFilPar.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/batmag_soft
)
_generate_msg_nodejs(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/CameraSettings.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/batmag_soft
)
_generate_msg_nodejs(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/State.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/batmag_soft
)
_generate_msg_nodejs(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/ForceCurrentSet.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/batmag_soft
)
_generate_msg_nodejs(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/CtrlOutput.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/batmag_soft
)
_generate_msg_nodejs(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/RotatingCtrlSettings.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/batmag_soft
)
_generate_msg_nodejs(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/pmMove.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/batmag_soft
)

### Generating Services
_generate_srv_nodejs(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/srv/SetInitParameters.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/batmag_soft
)
_generate_srv_nodejs(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/srv/GetInitParameters.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/batmag_soft
)
_generate_srv_nodejs(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/srv/SaveImage.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/batmag_soft
)
_generate_srv_nodejs(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/srv/ForceCurrentSettings.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/batmag_soft
)
_generate_srv_nodejs(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/srv/getInitTrackerSettings.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/batmag_soft
)
_generate_srv_nodejs(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/srv/getCameraSettings.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/batmag_soft
)

### Generating Module File
_generate_module_nodejs(batmag_soft
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/batmag_soft
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(batmag_soft_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(batmag_soft_generate_messages batmag_soft_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/refFilPar.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_nodejs _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/USroi.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_nodejs _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/RecordRaw.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_nodejs _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/srv/SaveImage.srv" NAME_WE)
add_dependencies(batmag_soft_generate_messages_nodejs _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/Reference.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_nodejs _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/ResetElmos.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_nodejs _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/srv/getInitTrackerSettings.srv" NAME_WE)
add_dependencies(batmag_soft_generate_messages_nodejs _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/pmMove.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_nodejs _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/CameraClick.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_nodejs _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/MoveCamera.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_nodejs _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/TrackerSettings.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_nodejs _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/CtrlOutput.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_nodejs _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/WriteText.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_nodejs _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/MapOutput.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_nodejs _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/srv/GetInitParameters.srv" NAME_WE)
add_dependencies(batmag_soft_generate_messages_nodejs _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/GUIpreferences.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_nodejs _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/StartStop.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_nodejs _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/RotatingCtrlSettings.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_nodejs _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/srv/ForceCurrentSettings.srv" NAME_WE)
add_dependencies(batmag_soft_generate_messages_nodejs _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/State.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_nodejs _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/srv/SetInitParameters.srv" NAME_WE)
add_dependencies(batmag_soft_generate_messages_nodejs _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/CameraSettings.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_nodejs _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/ForceCurrentSet.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_nodejs _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/srv/getCameraSettings.srv" NAME_WE)
add_dependencies(batmag_soft_generate_messages_nodejs _batmag_soft_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(batmag_soft_gennodejs)
add_dependencies(batmag_soft_gennodejs batmag_soft_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS batmag_soft_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/CameraClick.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/batmag_soft
)
_generate_msg_py(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/USroi.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/batmag_soft
)
_generate_msg_py(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/MapOutput.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/batmag_soft
)
_generate_msg_py(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/RecordRaw.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/batmag_soft
)
_generate_msg_py(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/MoveCamera.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/batmag_soft
)
_generate_msg_py(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/GUIpreferences.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/batmag_soft
)
_generate_msg_py(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/StartStop.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/batmag_soft
)
_generate_msg_py(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/TrackerSettings.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/batmag_soft
)
_generate_msg_py(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/Reference.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/batmag_soft
)
_generate_msg_py(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/WriteText.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/batmag_soft
)
_generate_msg_py(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/ResetElmos.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/batmag_soft
)
_generate_msg_py(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/refFilPar.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/batmag_soft
)
_generate_msg_py(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/CameraSettings.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/batmag_soft
)
_generate_msg_py(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/State.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/batmag_soft
)
_generate_msg_py(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/ForceCurrentSet.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/batmag_soft
)
_generate_msg_py(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/CtrlOutput.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/batmag_soft
)
_generate_msg_py(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/RotatingCtrlSettings.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/batmag_soft
)
_generate_msg_py(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/msg/pmMove.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/batmag_soft
)

### Generating Services
_generate_srv_py(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/srv/SetInitParameters.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/batmag_soft
)
_generate_srv_py(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/srv/GetInitParameters.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/batmag_soft
)
_generate_srv_py(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/srv/SaveImage.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/batmag_soft
)
_generate_srv_py(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/srv/ForceCurrentSettings.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/batmag_soft
)
_generate_srv_py(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/srv/getInitTrackerSettings.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/batmag_soft
)
_generate_srv_py(batmag_soft
  "/home/batmag/batmag_ws/src/batmag_soft/srv/getCameraSettings.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/batmag_soft
)

### Generating Module File
_generate_module_py(batmag_soft
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/batmag_soft
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(batmag_soft_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(batmag_soft_generate_messages batmag_soft_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/refFilPar.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_py _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/USroi.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_py _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/RecordRaw.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_py _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/srv/SaveImage.srv" NAME_WE)
add_dependencies(batmag_soft_generate_messages_py _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/Reference.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_py _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/ResetElmos.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_py _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/srv/getInitTrackerSettings.srv" NAME_WE)
add_dependencies(batmag_soft_generate_messages_py _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/pmMove.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_py _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/CameraClick.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_py _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/MoveCamera.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_py _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/TrackerSettings.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_py _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/CtrlOutput.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_py _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/WriteText.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_py _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/MapOutput.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_py _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/srv/GetInitParameters.srv" NAME_WE)
add_dependencies(batmag_soft_generate_messages_py _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/GUIpreferences.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_py _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/StartStop.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_py _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/RotatingCtrlSettings.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_py _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/srv/ForceCurrentSettings.srv" NAME_WE)
add_dependencies(batmag_soft_generate_messages_py _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/State.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_py _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/srv/SetInitParameters.srv" NAME_WE)
add_dependencies(batmag_soft_generate_messages_py _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/CameraSettings.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_py _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/msg/ForceCurrentSet.msg" NAME_WE)
add_dependencies(batmag_soft_generate_messages_py _batmag_soft_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/batmag/batmag_ws/src/batmag_soft/srv/getCameraSettings.srv" NAME_WE)
add_dependencies(batmag_soft_generate_messages_py _batmag_soft_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(batmag_soft_genpy)
add_dependencies(batmag_soft_genpy batmag_soft_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS batmag_soft_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/batmag_soft)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/batmag_soft
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(batmag_soft_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()
if(TARGET actionlib_msgs_generate_messages_cpp)
  add_dependencies(batmag_soft_generate_messages_cpp actionlib_msgs_generate_messages_cpp)
endif()
if(TARGET sensor_msgs_generate_messages_cpp)
  add_dependencies(batmag_soft_generate_messages_cpp sensor_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/batmag_soft)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/batmag_soft
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(batmag_soft_generate_messages_eus std_msgs_generate_messages_eus)
endif()
if(TARGET actionlib_msgs_generate_messages_eus)
  add_dependencies(batmag_soft_generate_messages_eus actionlib_msgs_generate_messages_eus)
endif()
if(TARGET sensor_msgs_generate_messages_eus)
  add_dependencies(batmag_soft_generate_messages_eus sensor_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/batmag_soft)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/batmag_soft
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(batmag_soft_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()
if(TARGET actionlib_msgs_generate_messages_lisp)
  add_dependencies(batmag_soft_generate_messages_lisp actionlib_msgs_generate_messages_lisp)
endif()
if(TARGET sensor_msgs_generate_messages_lisp)
  add_dependencies(batmag_soft_generate_messages_lisp sensor_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/batmag_soft)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/batmag_soft
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(batmag_soft_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()
if(TARGET actionlib_msgs_generate_messages_nodejs)
  add_dependencies(batmag_soft_generate_messages_nodejs actionlib_msgs_generate_messages_nodejs)
endif()
if(TARGET sensor_msgs_generate_messages_nodejs)
  add_dependencies(batmag_soft_generate_messages_nodejs sensor_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/batmag_soft)
  install(CODE "execute_process(COMMAND \"/usr/bin/python\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/batmag_soft\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/batmag_soft
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(batmag_soft_generate_messages_py std_msgs_generate_messages_py)
endif()
if(TARGET actionlib_msgs_generate_messages_py)
  add_dependencies(batmag_soft_generate_messages_py actionlib_msgs_generate_messages_py)
endif()
if(TARGET sensor_msgs_generate_messages_py)
  add_dependencies(batmag_soft_generate_messages_py sensor_msgs_generate_messages_py)
endif()
