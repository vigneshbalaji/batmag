# generated from genmsg/cmake/pkg-genmsg.context.in

messages_str = "/home/batmag/batmag_ws/src/batmag_soft/msg/MapOutput.msg;/home/batmag/batmag_ws/src/batmag_soft/msg/CtrlOutput.msg;/home/batmag/batmag_ws/src/batmag_soft/msg/StartStop.msg;/home/batmag/batmag_ws/src/batmag_soft/msg/Reference.msg;/home/batmag/batmag_ws/src/batmag_soft/msg/State.msg;/home/batmag/batmag_ws/src/batmag_soft/msg/CameraClick.msg;/home/batmag/batmag_ws/src/batmag_soft/msg/MoveCamera.msg;/home/batmag/batmag_ws/src/batmag_soft/msg/GUIpreferences.msg;/home/batmag/batmag_ws/src/batmag_soft/msg/RecordRaw.msg;/home/batmag/batmag_ws/src/batmag_soft/msg/WriteText.msg;/home/batmag/batmag_ws/src/batmag_soft/msg/USroi.msg;/home/batmag/batmag_ws/src/batmag_soft/msg/pmMove.msg;/home/batmag/batmag_ws/src/batmag_soft/msg/TrackerSettings.msg;/home/batmag/batmag_ws/src/batmag_soft/msg/refFilPar.msg;/home/batmag/batmag_ws/src/batmag_soft/msg/CameraSettings.msg;/home/batmag/batmag_ws/src/batmag_soft/msg/ForceCurrentSet.msg;/home/batmag/batmag_ws/src/batmag_soft/msg/ResetElmos.msg;/home/batmag/batmag_ws/src/batmag_soft/msg/RotatingCtrlSettings.msg"
services_str = "/home/batmag/batmag_ws/src/batmag_soft/srv/GetInitParameters.srv;/home/batmag/batmag_ws/src/batmag_soft/srv/SetInitParameters.srv;/home/batmag/batmag_ws/src/batmag_soft/srv/getInitTrackerSettings.srv;/home/batmag/batmag_ws/src/batmag_soft/srv/SaveImage.srv;/home/batmag/batmag_ws/src/batmag_soft/srv/getCameraSettings.srv;/home/batmag/batmag_ws/src/batmag_soft/srv/ForceCurrentSettings.srv"
pkg_name = "batmag_soft"
dependencies_str = "std_msgs;actionlib_msgs;sensor_msgs"
langs = "gencpp;geneus;genlisp;gennodejs;genpy"
dep_include_paths_str = "batmag_soft;/home/batmag/batmag_ws/src/batmag_soft/msg;std_msgs;/opt/ros/lunar/share/std_msgs/cmake/../msg;actionlib_msgs;/opt/ros/lunar/share/actionlib_msgs/cmake/../msg;sensor_msgs;/opt/ros/lunar/share/sensor_msgs/cmake/../msg;geometry_msgs;/opt/ros/lunar/share/geometry_msgs/cmake/../msg"
PYTHON_EXECUTABLE = "/usr/bin/python"
package_has_static_sources = '' == 'TRUE'
genmsg_check_deps_script = "/opt/ros/lunar/share/genmsg/cmake/../../../lib/genmsg/genmsg_check_deps.py"
