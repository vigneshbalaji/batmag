/****************************************************************************
** Meta object code from reading C++ file 'camerasettingswin.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/batmag_soft/src/gui/camerasettingswin.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'camerasettingswin.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_CameraSettingsWin_t {
    QByteArrayData data[16];
    char stringdata0[473];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CameraSettingsWin_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CameraSettingsWin_t qt_meta_stringdata_CameraSettingsWin = {
    {
QT_MOC_LITERAL(0, 0, 17), // "CameraSettingsWin"
QT_MOC_LITERAL(1, 18, 40), // "on_brightness_doubleSpinBox_v..."
QT_MOC_LITERAL(2, 59, 0), // ""
QT_MOC_LITERAL(3, 60, 4), // "arg1"
QT_MOC_LITERAL(4, 65, 38), // "on_exposure_doubleSpinBox_val..."
QT_MOC_LITERAL(5, 104, 35), // "on_sharp_doubleSpinBox_valueC..."
QT_MOC_LITERAL(6, 140, 33), // "on_hue_doubleSpinBox_valueCha..."
QT_MOC_LITERAL(7, 174, 33), // "on_sat_doubleSpinBox_valueCha..."
QT_MOC_LITERAL(8, 208, 35), // "on_gamma_doubleSpinBox_valueC..."
QT_MOC_LITERAL(9, 244, 37), // "on_shutter_doubleSpinBox_valu..."
QT_MOC_LITERAL(10, 282, 34), // "on_gain_doubleSpinBox_valueCh..."
QT_MOC_LITERAL(11, 317, 39), // "on_framerate_doubleSpinBox_va..."
QT_MOC_LITERAL(12, 357, 33), // "on_wbb_doubleSpinBox_valueCha..."
QT_MOC_LITERAL(13, 391, 33), // "on_wbr_doubleSpinBox_valueCha..."
QT_MOC_LITERAL(14, 425, 23), // "on_apply_Button_pressed"
QT_MOC_LITERAL(15, 449, 23) // "on_close_Button_pressed"

    },
    "CameraSettingsWin\0"
    "on_brightness_doubleSpinBox_valueChanged\0"
    "\0arg1\0on_exposure_doubleSpinBox_valueChanged\0"
    "on_sharp_doubleSpinBox_valueChanged\0"
    "on_hue_doubleSpinBox_valueChanged\0"
    "on_sat_doubleSpinBox_valueChanged\0"
    "on_gamma_doubleSpinBox_valueChanged\0"
    "on_shutter_doubleSpinBox_valueChanged\0"
    "on_gain_doubleSpinBox_valueChanged\0"
    "on_framerate_doubleSpinBox_valueChanged\0"
    "on_wbb_doubleSpinBox_valueChanged\0"
    "on_wbr_doubleSpinBox_valueChanged\0"
    "on_apply_Button_pressed\0on_close_Button_pressed"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CameraSettingsWin[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   79,    2, 0x08 /* Private */,
       4,    1,   82,    2, 0x08 /* Private */,
       5,    1,   85,    2, 0x08 /* Private */,
       6,    1,   88,    2, 0x08 /* Private */,
       7,    1,   91,    2, 0x08 /* Private */,
       8,    1,   94,    2, 0x08 /* Private */,
       9,    1,   97,    2, 0x08 /* Private */,
      10,    1,  100,    2, 0x08 /* Private */,
      11,    1,  103,    2, 0x08 /* Private */,
      12,    1,  106,    2, 0x08 /* Private */,
      13,    1,  109,    2, 0x08 /* Private */,
      14,    0,  112,    2, 0x08 /* Private */,
      15,    0,  113,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void CameraSettingsWin::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        CameraSettingsWin *_t = static_cast<CameraSettingsWin *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_brightness_doubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 1: _t->on_exposure_doubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 2: _t->on_sharp_doubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 3: _t->on_hue_doubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 4: _t->on_sat_doubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 5: _t->on_gamma_doubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 6: _t->on_shutter_doubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 7: _t->on_gain_doubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 8: _t->on_framerate_doubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 9: _t->on_wbb_doubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 10: _t->on_wbr_doubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 11: _t->on_apply_Button_pressed(); break;
        case 12: _t->on_close_Button_pressed(); break;
        default: ;
        }
    }
}

const QMetaObject CameraSettingsWin::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_CameraSettingsWin.data,
      qt_meta_data_CameraSettingsWin,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *CameraSettingsWin::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CameraSettingsWin::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CameraSettingsWin.stringdata0))
        return static_cast<void*>(const_cast< CameraSettingsWin*>(this));
    return QDialog::qt_metacast(_clname);
}

int CameraSettingsWin::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 13;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
