/****************************************************************************
** Meta object code from reading C++ file 'forcecurrentsettingswindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/batmag_soft/src/gui/forcecurrentsettingswindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'forcecurrentsettingswindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_ForceCurrentSettingsWindow_t {
    QByteArrayData data[21];
    char stringdata0[635];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ForceCurrentSettingsWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ForceCurrentSettingsWindow_t qt_meta_stringdata_ForceCurrentSettingsWindow = {
    {
QT_MOC_LITERAL(0, 0, 26), // "ForceCurrentSettingsWindow"
QT_MOC_LITERAL(1, 27, 42), // "on_permeability_doubleSpinBox..."
QT_MOC_LITERAL(2, 70, 0), // ""
QT_MOC_LITERAL(3, 71, 4), // "arg1"
QT_MOC_LITERAL(4, 76, 45), // "on_permeability_horizontalSli..."
QT_MOC_LITERAL(5, 122, 5), // "value"
QT_MOC_LITERAL(6, 128, 39), // "on_radius_horizontalSlider_va..."
QT_MOC_LITERAL(7, 168, 36), // "on_radius_doubleSpinBox_value..."
QT_MOC_LITERAL(8, 205, 32), // "on_br_doubleSpinBox_valueChanged"
QT_MOC_LITERAL(9, 238, 35), // "on_br_horizontalSlider_valueC..."
QT_MOC_LITERAL(10, 274, 44), // "on_permeability_doubleSpinBox..."
QT_MOC_LITERAL(11, 319, 47), // "on_permeability_horizontalSli..."
QT_MOC_LITERAL(12, 367, 41), // "on_radius_horizontalSlider_2_..."
QT_MOC_LITERAL(13, 409, 38), // "on_radius_doubleSpinBox_2_val..."
QT_MOC_LITERAL(14, 448, 34), // "on_br_doubleSpinBox_2_valueCh..."
QT_MOC_LITERAL(15, 483, 37), // "on_br_horizontalSlider_2_valu..."
QT_MOC_LITERAL(16, 521, 29), // "on_diffAgent_checkBox_toggled"
QT_MOC_LITERAL(17, 551, 7), // "checked"
QT_MOC_LITERAL(18, 559, 20), // "on_ok_button_clicked"
QT_MOC_LITERAL(19, 580, 22), // "on_ApplyButton_clicked"
QT_MOC_LITERAL(20, 603, 31) // "on_density_spinBox_valueChanged"

    },
    "ForceCurrentSettingsWindow\0"
    "on_permeability_doubleSpinBox_valueChanged\0"
    "\0arg1\0on_permeability_horizontalSlider_valueChanged\0"
    "value\0on_radius_horizontalSlider_valueChanged\0"
    "on_radius_doubleSpinBox_valueChanged\0"
    "on_br_doubleSpinBox_valueChanged\0"
    "on_br_horizontalSlider_valueChanged\0"
    "on_permeability_doubleSpinBox_2_valueChanged\0"
    "on_permeability_horizontalSlider_2_valueChanged\0"
    "on_radius_horizontalSlider_2_valueChanged\0"
    "on_radius_doubleSpinBox_2_valueChanged\0"
    "on_br_doubleSpinBox_2_valueChanged\0"
    "on_br_horizontalSlider_2_valueChanged\0"
    "on_diffAgent_checkBox_toggled\0checked\0"
    "on_ok_button_clicked\0on_ApplyButton_clicked\0"
    "on_density_spinBox_valueChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ForceCurrentSettingsWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   94,    2, 0x08 /* Private */,
       4,    1,   97,    2, 0x08 /* Private */,
       6,    1,  100,    2, 0x08 /* Private */,
       7,    1,  103,    2, 0x08 /* Private */,
       8,    1,  106,    2, 0x08 /* Private */,
       9,    1,  109,    2, 0x08 /* Private */,
      10,    1,  112,    2, 0x08 /* Private */,
      11,    1,  115,    2, 0x08 /* Private */,
      12,    1,  118,    2, 0x08 /* Private */,
      13,    1,  121,    2, 0x08 /* Private */,
      14,    1,  124,    2, 0x08 /* Private */,
      15,    1,  127,    2, 0x08 /* Private */,
      16,    1,  130,    2, 0x08 /* Private */,
      18,    0,  133,    2, 0x08 /* Private */,
      19,    0,  134,    2, 0x08 /* Private */,
      20,    1,  135,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Bool,   17,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    3,

       0        // eod
};

void ForceCurrentSettingsWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ForceCurrentSettingsWindow *_t = static_cast<ForceCurrentSettingsWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_permeability_doubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 1: _t->on_permeability_horizontalSlider_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->on_radius_horizontalSlider_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->on_radius_doubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 4: _t->on_br_doubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 5: _t->on_br_horizontalSlider_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->on_permeability_doubleSpinBox_2_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 7: _t->on_permeability_horizontalSlider_2_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->on_radius_horizontalSlider_2_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->on_radius_doubleSpinBox_2_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 10: _t->on_br_doubleSpinBox_2_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 11: _t->on_br_horizontalSlider_2_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: _t->on_diffAgent_checkBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 13: _t->on_ok_button_clicked(); break;
        case 14: _t->on_ApplyButton_clicked(); break;
        case 15: _t->on_density_spinBox_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject ForceCurrentSettingsWindow::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_ForceCurrentSettingsWindow.data,
      qt_meta_data_ForceCurrentSettingsWindow,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *ForceCurrentSettingsWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ForceCurrentSettingsWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ForceCurrentSettingsWindow.stringdata0))
        return static_cast<void*>(const_cast< ForceCurrentSettingsWindow*>(this));
    return QDialog::qt_metacast(_clname);
}

int ForceCurrentSettingsWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 16)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 16;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
