/********************************************************************************
** Form generated from reading UI file 'secondscreenimage.ui'
**
** Created by: Qt User Interface Compiler version 5.9.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SECONDSCREENIMAGE_H
#define UI_SECONDSCREENIMAGE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>
#include "cqtopencvviewergl.h"

QT_BEGIN_NAMESPACE

class Ui_SecondScreenImage
{
public:
    QWidget *centralwidget;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QFrame *frame;
    CQtOpenCVViewerGl *camera0View2;
    QFrame *frame_2;
    CQtOpenCVViewerGl *camera1View2;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *SecondScreenImage)
    {
        if (SecondScreenImage->objectName().isEmpty())
            SecondScreenImage->setObjectName(QStringLiteral("SecondScreenImage"));
        SecondScreenImage->resize(1920, 1000);
        centralwidget = new QWidget(SecondScreenImage);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        gridLayoutWidget = new QWidget(centralwidget);
        gridLayoutWidget->setObjectName(QStringLiteral("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(-1, -1, 1921, 951));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        frame = new QFrame(gridLayoutWidget);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        camera0View2 = new CQtOpenCVViewerGl(frame);
        camera0View2->setObjectName(QStringLiteral("camera0View2"));
        camera0View2->setGeometry(QRect(0, 0, 950, 950));

        gridLayout->addWidget(frame, 0, 0, 1, 1);

        frame_2 = new QFrame(gridLayoutWidget);
        frame_2->setObjectName(QStringLiteral("frame_2"));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        camera1View2 = new CQtOpenCVViewerGl(frame_2);
        camera1View2->setObjectName(QStringLiteral("camera1View2"));
        camera1View2->setGeometry(QRect(-1, 0, 950, 950));

        gridLayout->addWidget(frame_2, 0, 1, 1, 1);

        SecondScreenImage->setCentralWidget(centralwidget);
        menubar = new QMenuBar(SecondScreenImage);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 1920, 19));
        SecondScreenImage->setMenuBar(menubar);
        statusbar = new QStatusBar(SecondScreenImage);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        SecondScreenImage->setStatusBar(statusbar);

        retranslateUi(SecondScreenImage);

        QMetaObject::connectSlotsByName(SecondScreenImage);
    } // setupUi

    void retranslateUi(QMainWindow *SecondScreenImage)
    {
        SecondScreenImage->setWindowTitle(QApplication::translate("SecondScreenImage", "SecondWindow", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class SecondScreenImage: public Ui_SecondScreenImage {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SECONDSCREENIMAGE_H
