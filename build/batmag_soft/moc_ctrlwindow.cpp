/****************************************************************************
** Meta object code from reading C++ file 'ctrlwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/batmag_soft/src/gui/ctrlwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ctrlwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_CtrlWindow_t {
    QByteArrayData data[100];
    char stringdata0[2445];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CtrlWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CtrlWindow_t qt_meta_stringdata_CtrlWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "CtrlWindow"
QT_MOC_LITERAL(1, 11, 14), // "newCameraImage"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 9), // "cameraNum"
QT_MOC_LITERAL(4, 37, 18), // "newCurrentCurrents"
QT_MOC_LITERAL(5, 56, 19), // "on_camera_0_clicked"
QT_MOC_LITERAL(6, 76, 1), // "x"
QT_MOC_LITERAL(7, 78, 1), // "y"
QT_MOC_LITERAL(8, 80, 15), // "Qt::MouseButton"
QT_MOC_LITERAL(9, 96, 6), // "button"
QT_MOC_LITERAL(10, 103, 19), // "on_camera_1_clicked"
QT_MOC_LITERAL(11, 123, 17), // "on_newCameraImage"
QT_MOC_LITERAL(12, 141, 6), // "camNum"
QT_MOC_LITERAL(13, 148, 21), // "on_stopButton_clicked"
QT_MOC_LITERAL(14, 170, 23), // "on_stopButton_2_clicked"
QT_MOC_LITERAL(15, 194, 10), // "moveCamMsg"
QT_MOC_LITERAL(16, 205, 21), // "on_NewCurrentCurrents"
QT_MOC_LITERAL(17, 227, 21), // "allowCurrentBarUpdate"
QT_MOC_LITERAL(18, 249, 10), // "setSliders"
QT_MOC_LITERAL(19, 260, 19), // "std::vector<double>"
QT_MOC_LITERAL(20, 280, 5), // "value"
QT_MOC_LITERAL(21, 286, 3), // "max"
QT_MOC_LITERAL(22, 290, 3), // "min"
QT_MOC_LITERAL(23, 294, 24), // "std::vector<std::string>"
QT_MOC_LITERAL(24, 319, 5), // "names"
QT_MOC_LITERAL(25, 325, 28), // "on_paramSlider1_valueChanged"
QT_MOC_LITERAL(26, 354, 26), // "on_paramSpin1_valueChanged"
QT_MOC_LITERAL(27, 381, 4), // "arg1"
QT_MOC_LITERAL(28, 386, 28), // "on_paramSlider2_valueChanged"
QT_MOC_LITERAL(29, 415, 26), // "on_paramSpin2_valueChanged"
QT_MOC_LITERAL(30, 442, 28), // "on_paramSlider3_valueChanged"
QT_MOC_LITERAL(31, 471, 26), // "on_paramSpin3_valueChanged"
QT_MOC_LITERAL(32, 498, 28), // "on_paramSlider4_valueChanged"
QT_MOC_LITERAL(33, 527, 26), // "on_paramSpin4_valueChanged"
QT_MOC_LITERAL(34, 554, 28), // "on_paramSlider5_valueChanged"
QT_MOC_LITERAL(35, 583, 26), // "on_paramSpin5_valueChanged"
QT_MOC_LITERAL(36, 610, 28), // "on_paramSlider6_valueChanged"
QT_MOC_LITERAL(37, 639, 26), // "on_paramSpin6_valueChanged"
QT_MOC_LITERAL(38, 666, 28), // "on_paramSlider7_valueChanged"
QT_MOC_LITERAL(39, 695, 26), // "on_paramSpin7_valueChanged"
QT_MOC_LITERAL(40, 722, 28), // "on_paramSlider8_valueChanged"
QT_MOC_LITERAL(41, 751, 26), // "on_paramSpin8_valueChanged"
QT_MOC_LITERAL(42, 778, 28), // "on_paramSlider9_valueChanged"
QT_MOC_LITERAL(43, 807, 26), // "on_paramSpin9_valueChanged"
QT_MOC_LITERAL(44, 834, 22), // "on_startButton_clicked"
QT_MOC_LITERAL(45, 857, 42), // "on_controlAlgorithmList_curre..."
QT_MOC_LITERAL(46, 900, 16), // "QListWidgetItem*"
QT_MOC_LITERAL(47, 917, 7), // "current"
QT_MOC_LITERAL(48, 925, 8), // "previous"
QT_MOC_LITERAL(49, 934, 21), // "setParametersFromResp"
QT_MOC_LITERAL(50, 956, 30), // "batmag_soft::GetInitParameters"
QT_MOC_LITERAL(51, 987, 3), // "srv"
QT_MOC_LITERAL(52, 991, 17), // "sendSetParameters"
QT_MOC_LITERAL(53, 1009, 31), // "on_agentList_currentItemChanged"
QT_MOC_LITERAL(54, 1041, 11), // "publishStop"
QT_MOC_LITERAL(55, 1053, 12), // "publishStart"
QT_MOC_LITERAL(56, 1066, 28), // "on_camera0Speed_valueChanged"
QT_MOC_LITERAL(57, 1095, 28), // "on_camera1Speed_valueChanged"
QT_MOC_LITERAL(58, 1124, 30), // "on_camera0Speed_sliderReleased"
QT_MOC_LITERAL(59, 1155, 30), // "on_camera1Speed_sliderReleased"
QT_MOC_LITERAL(60, 1186, 30), // "on_autofocus0_checkBox_toggled"
QT_MOC_LITERAL(61, 1217, 7), // "checked"
QT_MOC_LITERAL(62, 1225, 30), // "on_autofocus1_checkBox_toggled"
QT_MOC_LITERAL(63, 1256, 26), // "on_record_checkBox_toggled"
QT_MOC_LITERAL(64, 1283, 38), // "on_forceCurrentList_currentIt..."
QT_MOC_LITERAL(65, 1322, 27), // "on_ctrl_en_checkBox_toggled"
QT_MOC_LITERAL(66, 1350, 33), // "on_zoomDoubleSpinBox_valueCha..."
QT_MOC_LITERAL(67, 1384, 29), // "on_recordRaw_checkBox_toggled"
QT_MOC_LITERAL(68, 1414, 29), // "on_secondWindowButton_clicked"
QT_MOC_LITERAL(69, 1444, 39), // "on_Cur1Debug_doubleSpinBox_va..."
QT_MOC_LITERAL(70, 1484, 39), // "on_Cur2Debug_doubleSpinBox_va..."
QT_MOC_LITERAL(71, 1524, 39), // "on_Cur3Debug_doubleSpinBox_va..."
QT_MOC_LITERAL(72, 1564, 39), // "on_Cur4Debug_doubleSpinBox_va..."
QT_MOC_LITERAL(73, 1604, 39), // "on_Cur5Debug_doubleSpinBox_va..."
QT_MOC_LITERAL(74, 1644, 39), // "on_Cur6Debug_doubleSpinBox_va..."
QT_MOC_LITERAL(75, 1684, 39), // "on_Cur7Debug_doubleSpinBox_va..."
QT_MOC_LITERAL(76, 1724, 39), // "on_Cur8Debug_doubleSpinBox_va..."
QT_MOC_LITERAL(77, 1764, 39), // "on_Cur9Debug_doubleSpinBox_va..."
QT_MOC_LITERAL(78, 1804, 26), // "on_deb_en_checkBox_toggled"
QT_MOC_LITERAL(79, 1831, 18), // "sinusoidalCurDebug"
QT_MOC_LITERAL(80, 1850, 37), // "on_imagingModeList_currentIte..."
QT_MOC_LITERAL(81, 1888, 31), // "on_usROI_x_spinBox_valueChanged"
QT_MOC_LITERAL(82, 1920, 31), // "on_usROI_y_spinBox_valueChanged"
QT_MOC_LITERAL(83, 1952, 36), // "on_usROI_height_spinBox_value..."
QT_MOC_LITERAL(84, 1989, 35), // "on_usROI_width_spinBox_valueC..."
QT_MOC_LITERAL(85, 2025, 32), // "on_Pm_doubleSpinBox_valueChanged"
QT_MOC_LITERAL(86, 2058, 19), // "on_PMenable_toggled"
QT_MOC_LITERAL(87, 2078, 21), // "on_pushButton_clicked"
QT_MOC_LITERAL(88, 2100, 29), // "on_gravityEn_checkBox_toggled"
QT_MOC_LITERAL(89, 2130, 30), // "on_refTau_SpinBox_valueChanged"
QT_MOC_LITERAL(90, 2161, 29), // "on_refUb_SpinBox_valueChanged"
QT_MOC_LITERAL(91, 2191, 29), // "on_refLb_SpinBox_valueChanged"
QT_MOC_LITERAL(92, 2221, 27), // "on_saveImage_button_clicked"
QT_MOC_LITERAL(93, 2249, 22), // "on_grid_Button_clicked"
QT_MOC_LITERAL(94, 2272, 31), // "on_cameraSet_pushButton_pressed"
QT_MOC_LITERAL(95, 2304, 26), // "on_forceCur_button_clicked"
QT_MOC_LITERAL(96, 2331, 31), // "on_resetElmo_pushButton_pressed"
QT_MOC_LITERAL(97, 2363, 30), // "on_ctrlSettings_button_pressed"
QT_MOC_LITERAL(98, 2394, 25), // "on_omega_checkBox_toggled"
QT_MOC_LITERAL(99, 2420, 24) // "on_Contro_Button_clicked"

    },
    "CtrlWindow\0newCameraImage\0\0cameraNum\0"
    "newCurrentCurrents\0on_camera_0_clicked\0"
    "x\0y\0Qt::MouseButton\0button\0"
    "on_camera_1_clicked\0on_newCameraImage\0"
    "camNum\0on_stopButton_clicked\0"
    "on_stopButton_2_clicked\0moveCamMsg\0"
    "on_NewCurrentCurrents\0allowCurrentBarUpdate\0"
    "setSliders\0std::vector<double>\0value\0"
    "max\0min\0std::vector<std::string>\0names\0"
    "on_paramSlider1_valueChanged\0"
    "on_paramSpin1_valueChanged\0arg1\0"
    "on_paramSlider2_valueChanged\0"
    "on_paramSpin2_valueChanged\0"
    "on_paramSlider3_valueChanged\0"
    "on_paramSpin3_valueChanged\0"
    "on_paramSlider4_valueChanged\0"
    "on_paramSpin4_valueChanged\0"
    "on_paramSlider5_valueChanged\0"
    "on_paramSpin5_valueChanged\0"
    "on_paramSlider6_valueChanged\0"
    "on_paramSpin6_valueChanged\0"
    "on_paramSlider7_valueChanged\0"
    "on_paramSpin7_valueChanged\0"
    "on_paramSlider8_valueChanged\0"
    "on_paramSpin8_valueChanged\0"
    "on_paramSlider9_valueChanged\0"
    "on_paramSpin9_valueChanged\0"
    "on_startButton_clicked\0"
    "on_controlAlgorithmList_currentItemChanged\0"
    "QListWidgetItem*\0current\0previous\0"
    "setParametersFromResp\0"
    "batmag_soft::GetInitParameters\0srv\0"
    "sendSetParameters\0on_agentList_currentItemChanged\0"
    "publishStop\0publishStart\0"
    "on_camera0Speed_valueChanged\0"
    "on_camera1Speed_valueChanged\0"
    "on_camera0Speed_sliderReleased\0"
    "on_camera1Speed_sliderReleased\0"
    "on_autofocus0_checkBox_toggled\0checked\0"
    "on_autofocus1_checkBox_toggled\0"
    "on_record_checkBox_toggled\0"
    "on_forceCurrentList_currentItemChanged\0"
    "on_ctrl_en_checkBox_toggled\0"
    "on_zoomDoubleSpinBox_valueChanged\0"
    "on_recordRaw_checkBox_toggled\0"
    "on_secondWindowButton_clicked\0"
    "on_Cur1Debug_doubleSpinBox_valueChanged\0"
    "on_Cur2Debug_doubleSpinBox_valueChanged\0"
    "on_Cur3Debug_doubleSpinBox_valueChanged\0"
    "on_Cur4Debug_doubleSpinBox_valueChanged\0"
    "on_Cur5Debug_doubleSpinBox_valueChanged\0"
    "on_Cur6Debug_doubleSpinBox_valueChanged\0"
    "on_Cur7Debug_doubleSpinBox_valueChanged\0"
    "on_Cur8Debug_doubleSpinBox_valueChanged\0"
    "on_Cur9Debug_doubleSpinBox_valueChanged\0"
    "on_deb_en_checkBox_toggled\0"
    "sinusoidalCurDebug\0"
    "on_imagingModeList_currentItemChanged\0"
    "on_usROI_x_spinBox_valueChanged\0"
    "on_usROI_y_spinBox_valueChanged\0"
    "on_usROI_height_spinBox_valueChanged\0"
    "on_usROI_width_spinBox_valueChanged\0"
    "on_Pm_doubleSpinBox_valueChanged\0"
    "on_PMenable_toggled\0on_pushButton_clicked\0"
    "on_gravityEn_checkBox_toggled\0"
    "on_refTau_SpinBox_valueChanged\0"
    "on_refUb_SpinBox_valueChanged\0"
    "on_refLb_SpinBox_valueChanged\0"
    "on_saveImage_button_clicked\0"
    "on_grid_Button_clicked\0"
    "on_cameraSet_pushButton_pressed\0"
    "on_forceCur_button_clicked\0"
    "on_resetElmo_pushButton_pressed\0"
    "on_ctrlSettings_button_pressed\0"
    "on_omega_checkBox_toggled\0"
    "on_Contro_Button_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CtrlWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      80,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  414,    2, 0x06 /* Public */,
       4,    0,  417,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    3,  418,    2, 0x08 /* Private */,
      10,    3,  425,    2, 0x08 /* Private */,
      11,    1,  432,    2, 0x08 /* Private */,
      13,    0,  435,    2, 0x08 /* Private */,
      14,    0,  436,    2, 0x08 /* Private */,
      15,    0,  437,    2, 0x08 /* Private */,
      16,    0,  438,    2, 0x08 /* Private */,
      17,    0,  439,    2, 0x08 /* Private */,
      18,    3,  440,    2, 0x08 /* Private */,
      18,    4,  447,    2, 0x08 /* Private */,
      25,    1,  456,    2, 0x08 /* Private */,
      26,    1,  459,    2, 0x08 /* Private */,
      28,    1,  462,    2, 0x08 /* Private */,
      29,    1,  465,    2, 0x08 /* Private */,
      30,    1,  468,    2, 0x08 /* Private */,
      31,    1,  471,    2, 0x08 /* Private */,
      32,    1,  474,    2, 0x08 /* Private */,
      33,    1,  477,    2, 0x08 /* Private */,
      34,    1,  480,    2, 0x08 /* Private */,
      35,    1,  483,    2, 0x08 /* Private */,
      36,    1,  486,    2, 0x08 /* Private */,
      37,    1,  489,    2, 0x08 /* Private */,
      38,    1,  492,    2, 0x08 /* Private */,
      39,    1,  495,    2, 0x08 /* Private */,
      40,    1,  498,    2, 0x08 /* Private */,
      41,    1,  501,    2, 0x08 /* Private */,
      42,    1,  504,    2, 0x08 /* Private */,
      43,    1,  507,    2, 0x08 /* Private */,
      44,    0,  510,    2, 0x08 /* Private */,
      45,    2,  511,    2, 0x08 /* Private */,
      49,    1,  516,    2, 0x08 /* Private */,
      52,    0,  519,    2, 0x08 /* Private */,
      53,    2,  520,    2, 0x08 /* Private */,
      54,    0,  525,    2, 0x08 /* Private */,
      55,    0,  526,    2, 0x08 /* Private */,
      56,    1,  527,    2, 0x08 /* Private */,
      57,    1,  530,    2, 0x08 /* Private */,
      58,    0,  533,    2, 0x08 /* Private */,
      59,    0,  534,    2, 0x08 /* Private */,
      60,    1,  535,    2, 0x08 /* Private */,
      62,    1,  538,    2, 0x08 /* Private */,
      63,    1,  541,    2, 0x08 /* Private */,
      64,    2,  544,    2, 0x08 /* Private */,
      65,    1,  549,    2, 0x08 /* Private */,
      66,    1,  552,    2, 0x08 /* Private */,
      67,    1,  555,    2, 0x08 /* Private */,
      68,    0,  558,    2, 0x08 /* Private */,
      69,    1,  559,    2, 0x08 /* Private */,
      70,    1,  562,    2, 0x08 /* Private */,
      71,    1,  565,    2, 0x08 /* Private */,
      72,    1,  568,    2, 0x08 /* Private */,
      73,    1,  571,    2, 0x08 /* Private */,
      74,    1,  574,    2, 0x08 /* Private */,
      75,    1,  577,    2, 0x08 /* Private */,
      76,    1,  580,    2, 0x08 /* Private */,
      77,    1,  583,    2, 0x08 /* Private */,
      78,    1,  586,    2, 0x08 /* Private */,
      79,    0,  589,    2, 0x08 /* Private */,
      80,    2,  590,    2, 0x08 /* Private */,
      81,    1,  595,    2, 0x08 /* Private */,
      82,    1,  598,    2, 0x08 /* Private */,
      83,    1,  601,    2, 0x08 /* Private */,
      84,    1,  604,    2, 0x08 /* Private */,
      85,    1,  607,    2, 0x08 /* Private */,
      86,    1,  610,    2, 0x08 /* Private */,
      87,    0,  613,    2, 0x08 /* Private */,
      88,    1,  614,    2, 0x08 /* Private */,
      89,    1,  617,    2, 0x08 /* Private */,
      90,    1,  620,    2, 0x08 /* Private */,
      91,    1,  623,    2, 0x08 /* Private */,
      92,    0,  626,    2, 0x08 /* Private */,
      93,    0,  627,    2, 0x08 /* Private */,
      94,    0,  628,    2, 0x08 /* Private */,
      95,    0,  629,    2, 0x08 /* Private */,
      96,    0,  630,    2, 0x08 /* Private */,
      97,    0,  631,    2, 0x08 /* Private */,
      98,    1,  632,    2, 0x08 /* Private */,
      99,    0,  635,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::Int, QMetaType::Int, 0x80000000 | 8,    6,    7,    9,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, 0x80000000 | 8,    6,    7,    9,
    QMetaType::Void, QMetaType::Int,   12,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 19, 0x80000000 | 19, 0x80000000 | 19,   20,   21,   22,
    QMetaType::Void, 0x80000000 | 19, 0x80000000 | 19, 0x80000000 | 19, 0x80000000 | 23,   20,   21,   22,   24,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void, QMetaType::Double,   27,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void, QMetaType::Double,   27,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void, QMetaType::Double,   27,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void, QMetaType::Double,   27,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void, QMetaType::Double,   27,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void, QMetaType::Double,   27,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void, QMetaType::Double,   27,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void, QMetaType::Double,   27,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void, QMetaType::Double,   27,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 46, 0x80000000 | 46,   47,   48,
    QMetaType::Void, 0x80000000 | 50,   51,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 46, 0x80000000 | 46,   47,   48,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   61,
    QMetaType::Void, QMetaType::Bool,   61,
    QMetaType::Void, QMetaType::Bool,   61,
    QMetaType::Void, 0x80000000 | 46, 0x80000000 | 46,   47,   48,
    QMetaType::Void, QMetaType::Bool,   61,
    QMetaType::Void, QMetaType::Double,   27,
    QMetaType::Void, QMetaType::Bool,   61,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,   27,
    QMetaType::Void, QMetaType::Double,   27,
    QMetaType::Void, QMetaType::Double,   27,
    QMetaType::Void, QMetaType::Double,   27,
    QMetaType::Void, QMetaType::Double,   27,
    QMetaType::Void, QMetaType::Double,   27,
    QMetaType::Void, QMetaType::Double,   27,
    QMetaType::Void, QMetaType::Double,   27,
    QMetaType::Void, QMetaType::Double,   27,
    QMetaType::Void, QMetaType::Bool,   61,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 46, 0x80000000 | 46,   47,   48,
    QMetaType::Void, QMetaType::Int,   27,
    QMetaType::Void, QMetaType::Int,   27,
    QMetaType::Void, QMetaType::Int,   27,
    QMetaType::Void, QMetaType::Int,   27,
    QMetaType::Void, QMetaType::Double,   27,
    QMetaType::Void, QMetaType::Bool,   61,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   61,
    QMetaType::Void, QMetaType::Double,   27,
    QMetaType::Void, QMetaType::Double,   27,
    QMetaType::Void, QMetaType::Double,   27,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   61,
    QMetaType::Void,

       0        // eod
};

void CtrlWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        CtrlWindow *_t = static_cast<CtrlWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->newCameraImage((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->newCurrentCurrents(); break;
        case 2: _t->on_camera_0_clicked((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< Qt::MouseButton(*)>(_a[3]))); break;
        case 3: _t->on_camera_1_clicked((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< Qt::MouseButton(*)>(_a[3]))); break;
        case 4: _t->on_newCameraImage((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->on_stopButton_clicked(); break;
        case 6: _t->on_stopButton_2_clicked(); break;
        case 7: _t->moveCamMsg(); break;
        case 8: _t->on_NewCurrentCurrents(); break;
        case 9: _t->allowCurrentBarUpdate(); break;
        case 10: _t->setSliders((*reinterpret_cast< const std::vector<double>(*)>(_a[1])),(*reinterpret_cast< const std::vector<double>(*)>(_a[2])),(*reinterpret_cast< const std::vector<double>(*)>(_a[3]))); break;
        case 11: _t->setSliders((*reinterpret_cast< const std::vector<double>(*)>(_a[1])),(*reinterpret_cast< const std::vector<double>(*)>(_a[2])),(*reinterpret_cast< const std::vector<double>(*)>(_a[3])),(*reinterpret_cast< const std::vector<std::string>(*)>(_a[4]))); break;
        case 12: _t->on_paramSlider1_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: _t->on_paramSpin1_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 14: _t->on_paramSlider2_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 15: _t->on_paramSpin2_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 16: _t->on_paramSlider3_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 17: _t->on_paramSpin3_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 18: _t->on_paramSlider4_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 19: _t->on_paramSpin4_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 20: _t->on_paramSlider5_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 21: _t->on_paramSpin5_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 22: _t->on_paramSlider6_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 23: _t->on_paramSpin6_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 24: _t->on_paramSlider7_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 25: _t->on_paramSpin7_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 26: _t->on_paramSlider8_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 27: _t->on_paramSpin8_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 28: _t->on_paramSlider9_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 29: _t->on_paramSpin9_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 30: _t->on_startButton_clicked(); break;
        case 31: _t->on_controlAlgorithmList_currentItemChanged((*reinterpret_cast< QListWidgetItem*(*)>(_a[1])),(*reinterpret_cast< QListWidgetItem*(*)>(_a[2]))); break;
        case 32: _t->setParametersFromResp((*reinterpret_cast< batmag_soft::GetInitParameters(*)>(_a[1]))); break;
        case 33: _t->sendSetParameters(); break;
        case 34: _t->on_agentList_currentItemChanged((*reinterpret_cast< QListWidgetItem*(*)>(_a[1])),(*reinterpret_cast< QListWidgetItem*(*)>(_a[2]))); break;
        case 35: _t->publishStop(); break;
        case 36: _t->publishStart(); break;
        case 37: _t->on_camera0Speed_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 38: _t->on_camera1Speed_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 39: _t->on_camera0Speed_sliderReleased(); break;
        case 40: _t->on_camera1Speed_sliderReleased(); break;
        case 41: _t->on_autofocus0_checkBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 42: _t->on_autofocus1_checkBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 43: _t->on_record_checkBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 44: _t->on_forceCurrentList_currentItemChanged((*reinterpret_cast< QListWidgetItem*(*)>(_a[1])),(*reinterpret_cast< QListWidgetItem*(*)>(_a[2]))); break;
        case 45: _t->on_ctrl_en_checkBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 46: _t->on_zoomDoubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 47: _t->on_recordRaw_checkBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 48: _t->on_secondWindowButton_clicked(); break;
        case 49: _t->on_Cur1Debug_doubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 50: _t->on_Cur2Debug_doubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 51: _t->on_Cur3Debug_doubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 52: _t->on_Cur4Debug_doubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 53: _t->on_Cur5Debug_doubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 54: _t->on_Cur6Debug_doubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 55: _t->on_Cur7Debug_doubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 56: _t->on_Cur8Debug_doubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 57: _t->on_Cur9Debug_doubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 58: _t->on_deb_en_checkBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 59: _t->sinusoidalCurDebug(); break;
        case 60: _t->on_imagingModeList_currentItemChanged((*reinterpret_cast< QListWidgetItem*(*)>(_a[1])),(*reinterpret_cast< QListWidgetItem*(*)>(_a[2]))); break;
        case 61: _t->on_usROI_x_spinBox_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 62: _t->on_usROI_y_spinBox_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 63: _t->on_usROI_height_spinBox_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 64: _t->on_usROI_width_spinBox_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 65: _t->on_Pm_doubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 66: _t->on_PMenable_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 67: _t->on_pushButton_clicked(); break;
        case 68: _t->on_gravityEn_checkBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 69: _t->on_refTau_SpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 70: _t->on_refUb_SpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 71: _t->on_refLb_SpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 72: _t->on_saveImage_button_clicked(); break;
        case 73: _t->on_grid_Button_clicked(); break;
        case 74: _t->on_cameraSet_pushButton_pressed(); break;
        case 75: _t->on_forceCur_button_clicked(); break;
        case 76: _t->on_resetElmo_pushButton_pressed(); break;
        case 77: _t->on_ctrlSettings_button_pressed(); break;
        case 78: _t->on_omega_checkBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 79: _t->on_Contro_Button_clicked(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (CtrlWindow::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CtrlWindow::newCameraImage)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (CtrlWindow::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CtrlWindow::newCurrentCurrents)) {
                *result = 1;
                return;
            }
        }
    }
}

const QMetaObject CtrlWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_CtrlWindow.data,
      qt_meta_data_CtrlWindow,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *CtrlWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CtrlWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CtrlWindow.stringdata0))
        return static_cast<void*>(const_cast< CtrlWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int CtrlWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 80)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 80;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 80)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 80;
    }
    return _id;
}

// SIGNAL 0
void CtrlWindow::newCameraImage(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void CtrlWindow::newCurrentCurrents()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
