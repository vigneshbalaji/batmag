/****************************************************************************
** Meta object code from reading C++ file 'secondscreenimage.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/batmag_soft/src/gui/secondscreenimage.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'secondscreenimage.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_SecondScreenImage_t {
    QByteArrayData data[12];
    char stringdata0[136];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SecondScreenImage_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SecondScreenImage_t qt_meta_stringdata_SecondScreenImage = {
    {
QT_MOC_LITERAL(0, 0, 17), // "SecondScreenImage"
QT_MOC_LITERAL(1, 18, 14), // "newCameraImage"
QT_MOC_LITERAL(2, 33, 0), // ""
QT_MOC_LITERAL(3, 34, 9), // "cameraNum"
QT_MOC_LITERAL(4, 44, 17), // "on_newCameraImage"
QT_MOC_LITERAL(5, 62, 6), // "camNum"
QT_MOC_LITERAL(6, 69, 19), // "on_camera_0_clicked"
QT_MOC_LITERAL(7, 89, 1), // "x"
QT_MOC_LITERAL(8, 91, 1), // "y"
QT_MOC_LITERAL(9, 93, 15), // "Qt::MouseButton"
QT_MOC_LITERAL(10, 109, 6), // "button"
QT_MOC_LITERAL(11, 116, 19) // "on_camera_1_clicked"

    },
    "SecondScreenImage\0newCameraImage\0\0"
    "cameraNum\0on_newCameraImage\0camNum\0"
    "on_camera_0_clicked\0x\0y\0Qt::MouseButton\0"
    "button\0on_camera_1_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SecondScreenImage[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   34,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       4,    1,   37,    2, 0x08 /* Private */,
       6,    3,   40,    2, 0x08 /* Private */,
      11,    3,   47,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int,    3,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, 0x80000000 | 9,    7,    8,   10,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, 0x80000000 | 9,    7,    8,   10,

       0        // eod
};

void SecondScreenImage::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        SecondScreenImage *_t = static_cast<SecondScreenImage *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->newCameraImage((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->on_newCameraImage((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->on_camera_0_clicked((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< Qt::MouseButton(*)>(_a[3]))); break;
        case 3: _t->on_camera_1_clicked((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< Qt::MouseButton(*)>(_a[3]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (SecondScreenImage::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&SecondScreenImage::newCameraImage)) {
                *result = 0;
                return;
            }
        }
    }
}

const QMetaObject SecondScreenImage::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_SecondScreenImage.data,
      qt_meta_data_SecondScreenImage,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *SecondScreenImage::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SecondScreenImage::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SecondScreenImage.stringdata0))
        return static_cast<void*>(const_cast< SecondScreenImage*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int SecondScreenImage::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void SecondScreenImage::newCameraImage(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
