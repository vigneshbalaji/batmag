/********************************************************************************
** Form generated from reading UI file 'ctrlwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CTRLWINDOW_H
#define UI_CTRLWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "cqtopencvviewergl.h"

QT_BEGIN_NAMESPACE

class Ui_CtrlWindow
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QTabWidget *tabWidget;
    QWidget *initTab;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *buttonsVertical;
    QPushButton *startButton;
    QPushButton *stopButton_2;
    QWidget *gridLayoutWidget_2;
    QGridLayout *listsGrid;
    QLabel *imagingModeLabel;
    QListWidget *controlAlgorithmList;
    QListWidget *imagingModeList;
    QLabel *controlAlgorithmLabel;
    QLabel *agentLabel;
    QListWidget *agentList;
    QListWidget *forceCurrentList;
    QLabel *controlAlgorithmLabel_2;
    QWidget *gridLayoutWidget;
    QGridLayout *parameterGrid;
    QSlider *paramSlider2;
    QSlider *paramSlider1;
    QLabel *parameterLabel4;
    QLabel *parameterLabel2;
    QLabel *parameterLabel9;
    QLabel *parameterLabel8;
    QLabel *parameterLabel5;
    QLabel *parameterLabel3;
    QLabel *parameterLabel6;
    QSlider *paramSlider5;
    QSlider *paramSlider7;
    QSlider *paramSlider8;
    QSlider *paramSlider6;
    QDoubleSpinBox *paramSpin1;
    QSlider *paramSlider4;
    QSlider *paramSlider3;
    QLabel *parameterLabel1;
    QLabel *parameterLabel7;
    QSlider *paramSlider9;
    QDoubleSpinBox *paramSpin2;
    QDoubleSpinBox *paramSpin3;
    QDoubleSpinBox *paramSpin5;
    QDoubleSpinBox *paramSpin4;
    QDoubleSpinBox *paramSpin6;
    QDoubleSpinBox *paramSpin7;
    QDoubleSpinBox *paramSpin8;
    QDoubleSpinBox *paramSpin9;
    QTextBrowser *textBox;
    QWidget *gridLayoutWidget_10;
    QGridLayout *gridLayout_9;
    QLabel *label_44;
    QLabel *label;
    QDoubleSpinBox *zoomDoubleSpinBox;
    QComboBox *finalLens_comboBox;
    QWidget *ctrlTab;
    QPushButton *stopButton;
    QFrame *frame;
    QWidget *gridLayoutWidget_3;
    QGridLayout *gridLayout_2;
    CQtOpenCVViewerGl *camera0View;
    QFrame *frame_2;
    QWidget *gridLayoutWidget_4;
    QGridLayout *gridLayout_3;
    CQtOpenCVViewerGl *camera1View;
    QWidget *gridLayoutWidget_5;
    QGridLayout *gridLayout_4;
    QProgressBar *coil_progressBar_5;
    QProgressBar *coil_progressBar_9;
    QProgressBar *coil_progressBar_1;
    QProgressBar *coil_progressBar_3;
    QProgressBar *coil_progressBar_4;
    QProgressBar *coil_progressBar_7;
    QProgressBar *coil_progressBar_8;
    QProgressBar *coil_progressBar_6;
    QProgressBar *coil_progressBar_2;
    QCheckBox *ctrl_en_checkBox;
    QCheckBox *record_checkBox;
    QCheckBox *recordRaw_checkBox;
    QCheckBox *gravityEn_checkBox;
    QCheckBox *omega_checkBox;
    QLabel *label_2;
    QFrame *frame_3;
    QWidget *gridLayoutWidget_7;
    QGridLayout *gridLayout_6;
    QLabel *label_39;
    QLabel *label_37;
    QSpinBox *usROI_x_spinBox;
    QLabel *label_35;
    QLabel *label_36;
    QSpinBox *usROI_y_spinBox;
    QLabel *label_38;
    QSpinBox *usROI_height_spinBox;
    QSpinBox *usROI_width_spinBox;
    QFrame *line;
    QWidget *gridLayoutWidget_8;
    QGridLayout *gridLayout_7;
    QCheckBox *PMenable;
    QProgressBar *PermMag_progressBar_2;
    QDoubleSpinBox *Pm_doubleSpinBox;
    QFrame *line_2;
    QPushButton *pushButton;
    QWidget *gridLayoutWidget_9;
    QGridLayout *gridLayout_8;
    QLabel *label_41;
    QLabel *label_40;
    QDoubleSpinBox *refUb_SpinBox;
    QDoubleSpinBox *refTau_SpinBox;
    QLabel *label_42;
    QLabel *label_43;
    QDoubleSpinBox *refLb_SpinBox;
    QPushButton *grid_Button;
    QFrame *line_4;
    QWidget *verticalLayoutWidget_3;
    QVBoxLayout *verticalLayout_2;
    QPushButton *cameraSet_pushButton;
    QPushButton *saveImage_button;
    QPushButton *forceCur_button;
    QPushButton *resetElmo_pushButton;
    QPushButton *ctrlSettings_button;
    QSlider *camera0Speed;
    QLabel *label_3;
    QSlider *camera1Speed;
    QCheckBox *autofocus0_checkBox;
    QCheckBox *autofocus1_checkBox;
    QWidget *verticalLayoutWidget_2;
    QVBoxLayout *verticalLayout;
    QLabel *label_4;
    QTextBrowser *ctrl_textBrowser;
    QWidget *Debug;
    QWidget *gridLayoutWidget_6;
    QGridLayout *gridLayout_5;
    QLabel *label_18;
    QDoubleSpinBox *frequencySpinBox9;
    QDoubleSpinBox *frequencySpinBox3;
    QLabel *label_23;
    QDoubleSpinBox *frequencySpinBox5;
    QProgressBar *coil_progressBar_4deb;
    QProgressBar *coil_progressBar_9deb;
    QProgressBar *coil_progressBar_1deb;
    QProgressBar *coil_progressBar_7deb;
    QProgressBar *coil_progressBar_8deb;
    QProgressBar *coil_progressBar_2deb;
    QProgressBar *coil_progressBar_5deb;
    QCheckBox *record_checkBox_2;
    QCheckBox *recordRaw_checkBox_2;
    QProgressBar *coil_progressBar_6deb;
    QDoubleSpinBox *Cur1Debug_doubleSpinBox;
    QDoubleSpinBox *Cur2Debug_doubleSpinBox;
    QProgressBar *coil_progressBar_3deb;
    QDoubleSpinBox *Cur7Debug_doubleSpinBox;
    QDoubleSpinBox *Cur9Debug_doubleSpinBox;
    QDoubleSpinBox *Cur8Debug_doubleSpinBox;
    QDoubleSpinBox *Cur5Debug_doubleSpinBox;
    QDoubleSpinBox *Cur6Debug_doubleSpinBox;
    QDoubleSpinBox *Cur3Debug_doubleSpinBox;
    QDoubleSpinBox *Cur4Debug_doubleSpinBox;
    QCheckBox *deb_en_checkBox;
    QDoubleSpinBox *AC5_SpinBox;
    QDoubleSpinBox *AC2_SpinBox;
    QLabel *label_5;
    QLabel *label_7;
    QLabel *label_6;
    QLabel *label_10;
    QLabel *label_13;
    QLabel *label_8;
    QLabel *label_9;
    QLabel *label_11;
    QLabel *label_12;
    QDoubleSpinBox *AC1_SpinBox;
    QDoubleSpinBox *AC3_SpinBox;
    QDoubleSpinBox *AC4_SpinBox;
    QDoubleSpinBox *AC6_SpinBox;
    QDoubleSpinBox *AC7_SpinBox;
    QLabel *label_14;
    QDoubleSpinBox *AC8_SpinBox;
    QLabel *label_15;
    QDoubleSpinBox *AC9_SpinBox;
    QLabel *label_16;
    QLabel *label_17;
    QLabel *label_19;
    QLabel *label_22;
    QLabel *label_20;
    QLabel *label_21;
    QDoubleSpinBox *frequencySpinBox1;
    QDoubleSpinBox *frequencySpinBox2;
    QDoubleSpinBox *frequencySpinBox6;
    QDoubleSpinBox *frequencySpinBox4;
    QDoubleSpinBox *frequencySpinBox8;
    QDoubleSpinBox *frequencySpinBox7;
    QLabel *label_26;
    QLabel *label_24;
    QLabel *label_25;
    QLabel *label_27;
    QLabel *label_28;
    QLabel *label_30;
    QLabel *label_31;
    QLabel *label_29;
    QLabel *label_32;
    QDoubleSpinBox *PmDebug_doubleSpinBox;
    QProgressBar *PermMag_progressBar;
    QDoubleSpinBox *AC10_SpinBox;
    QLabel *label_33;
    QDoubleSpinBox *frequencySpinBox10;
    QLabel *label_34;
    QPushButton *Contro_Button;
    QMenuBar *menuBar;
    QStatusBar *statusBar;
    QToolBar *mainToolBar;

    void setupUi(QMainWindow *CtrlWindow)
    {
        if (CtrlWindow->objectName().isEmpty())
            CtrlWindow->setObjectName(QStringLiteral("CtrlWindow"));
        CtrlWindow->resize(1850, 1100);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(CtrlWindow->sizePolicy().hasHeightForWidth());
        CtrlWindow->setSizePolicy(sizePolicy);
        centralWidget = new QWidget(CtrlWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        sizePolicy.setHeightForWidth(tabWidget->sizePolicy().hasHeightForWidth());
        tabWidget->setSizePolicy(sizePolicy);
        tabWidget->setTabPosition(QTabWidget::North);
        tabWidget->setTabShape(QTabWidget::Rounded);
        initTab = new QWidget();
        initTab->setObjectName(QStringLiteral("initTab"));
        verticalLayoutWidget = new QWidget(initTab);
        verticalLayoutWidget->setObjectName(QStringLiteral("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(1740, 0, 101, 71));
        buttonsVertical = new QVBoxLayout(verticalLayoutWidget);
        buttonsVertical->setSpacing(10);
        buttonsVertical->setContentsMargins(11, 11, 11, 11);
        buttonsVertical->setObjectName(QStringLiteral("buttonsVertical"));
        buttonsVertical->setContentsMargins(0, 0, 0, 0);
        startButton = new QPushButton(verticalLayoutWidget);
        startButton->setObjectName(QStringLiteral("startButton"));
        startButton->setEnabled(true);
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::MinimumExpanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(startButton->sizePolicy().hasHeightForWidth());
        startButton->setSizePolicy(sizePolicy1);
        QFont font;
        font.setPointSize(11);
        startButton->setFont(font);
        startButton->setIconSize(QSize(16, 16));

        buttonsVertical->addWidget(startButton);

        stopButton_2 = new QPushButton(verticalLayoutWidget);
        stopButton_2->setObjectName(QStringLiteral("stopButton_2"));
        sizePolicy1.setHeightForWidth(stopButton_2->sizePolicy().hasHeightForWidth());
        stopButton_2->setSizePolicy(sizePolicy1);
        stopButton_2->setFont(font);

        buttonsVertical->addWidget(stopButton_2);

        gridLayoutWidget_2 = new QWidget(initTab);
        gridLayoutWidget_2->setObjectName(QStringLiteral("gridLayoutWidget_2"));
        gridLayoutWidget_2->setGeometry(QRect(-1, -1, 1591, 591));
        listsGrid = new QGridLayout(gridLayoutWidget_2);
        listsGrid->setSpacing(20);
        listsGrid->setContentsMargins(11, 11, 11, 11);
        listsGrid->setObjectName(QStringLiteral("listsGrid"));
        listsGrid->setContentsMargins(20, 20, 20, 20);
        imagingModeLabel = new QLabel(gridLayoutWidget_2);
        imagingModeLabel->setObjectName(QStringLiteral("imagingModeLabel"));
        QFont font1;
        font1.setPointSize(16);
        imagingModeLabel->setFont(font1);
        imagingModeLabel->setAlignment(Qt::AlignCenter);

        listsGrid->addWidget(imagingModeLabel, 0, 1, 1, 1);

        controlAlgorithmList = new QListWidget(gridLayoutWidget_2);
        new QListWidgetItem(controlAlgorithmList);
        new QListWidgetItem(controlAlgorithmList);
        new QListWidgetItem(controlAlgorithmList);
        controlAlgorithmList->setObjectName(QStringLiteral("controlAlgorithmList"));
        controlAlgorithmList->setFont(font);

        listsGrid->addWidget(controlAlgorithmList, 1, 2, 1, 1);

        imagingModeList = new QListWidget(gridLayoutWidget_2);
        new QListWidgetItem(imagingModeList);
        new QListWidgetItem(imagingModeList);
        new QListWidgetItem(imagingModeList);
        new QListWidgetItem(imagingModeList);
        imagingModeList->setObjectName(QStringLiteral("imagingModeList"));
        imagingModeList->setFont(font);

        listsGrid->addWidget(imagingModeList, 1, 1, 1, 1);

        controlAlgorithmLabel = new QLabel(gridLayoutWidget_2);
        controlAlgorithmLabel->setObjectName(QStringLiteral("controlAlgorithmLabel"));
        controlAlgorithmLabel->setFont(font1);
        controlAlgorithmLabel->setAlignment(Qt::AlignCenter);

        listsGrid->addWidget(controlAlgorithmLabel, 0, 2, 1, 1);

        agentLabel = new QLabel(gridLayoutWidget_2);
        agentLabel->setObjectName(QStringLiteral("agentLabel"));
        agentLabel->setFont(font1);
        agentLabel->setAlignment(Qt::AlignCenter);

        listsGrid->addWidget(agentLabel, 0, 0, 1, 1);

        agentList = new QListWidget(gridLayoutWidget_2);
        new QListWidgetItem(agentList);
        new QListWidgetItem(agentList);
        new QListWidgetItem(agentList);
        new QListWidgetItem(agentList);
        new QListWidgetItem(agentList);
        new QListWidgetItem(agentList);
        new QListWidgetItem(agentList);
        agentList->setObjectName(QStringLiteral("agentList"));
        agentList->setFont(font);
        agentList->setFrameShape(QFrame::StyledPanel);
        agentList->setFrameShadow(QFrame::Sunken);

        listsGrid->addWidget(agentList, 1, 0, 1, 1);

        forceCurrentList = new QListWidget(gridLayoutWidget_2);
        new QListWidgetItem(forceCurrentList);
        new QListWidgetItem(forceCurrentList);
        new QListWidgetItem(forceCurrentList);
        new QListWidgetItem(forceCurrentList);
        new QListWidgetItem(forceCurrentList);
        forceCurrentList->setObjectName(QStringLiteral("forceCurrentList"));
        forceCurrentList->setFont(font);

        listsGrid->addWidget(forceCurrentList, 1, 3, 1, 1);

        controlAlgorithmLabel_2 = new QLabel(gridLayoutWidget_2);
        controlAlgorithmLabel_2->setObjectName(QStringLiteral("controlAlgorithmLabel_2"));
        controlAlgorithmLabel_2->setFont(font1);
        controlAlgorithmLabel_2->setAlignment(Qt::AlignCenter);

        listsGrid->addWidget(controlAlgorithmLabel_2, 0, 3, 1, 1);

        gridLayoutWidget = new QWidget(initTab);
        gridLayoutWidget->setObjectName(QStringLiteral("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(0, 590, 1151, 331));
        parameterGrid = new QGridLayout(gridLayoutWidget);
        parameterGrid->setSpacing(6);
        parameterGrid->setContentsMargins(11, 11, 11, 11);
        parameterGrid->setObjectName(QStringLiteral("parameterGrid"));
        parameterGrid->setContentsMargins(0, 0, 0, 0);
        paramSlider2 = new QSlider(gridLayoutWidget);
        paramSlider2->setObjectName(QStringLiteral("paramSlider2"));
        paramSlider2->setEnabled(false);
        paramSlider2->setOrientation(Qt::Horizontal);

        parameterGrid->addWidget(paramSlider2, 1, 1, 1, 1);

        paramSlider1 = new QSlider(gridLayoutWidget);
        paramSlider1->setObjectName(QStringLiteral("paramSlider1"));
        paramSlider1->setEnabled(false);
        paramSlider1->setOrientation(Qt::Horizontal);

        parameterGrid->addWidget(paramSlider1, 0, 1, 1, 1);

        parameterLabel4 = new QLabel(gridLayoutWidget);
        parameterLabel4->setObjectName(QStringLiteral("parameterLabel4"));
        QFont font2;
        font2.setPointSize(12);
        parameterLabel4->setFont(font2);
        parameterLabel4->setAlignment(Qt::AlignCenter);

        parameterGrid->addWidget(parameterLabel4, 3, 0, 1, 1);

        parameterLabel2 = new QLabel(gridLayoutWidget);
        parameterLabel2->setObjectName(QStringLiteral("parameterLabel2"));
        parameterLabel2->setFont(font2);
        parameterLabel2->setAlignment(Qt::AlignCenter);

        parameterGrid->addWidget(parameterLabel2, 1, 0, 1, 1);

        parameterLabel9 = new QLabel(gridLayoutWidget);
        parameterLabel9->setObjectName(QStringLiteral("parameterLabel9"));
        parameterLabel9->setFont(font2);
        parameterLabel9->setAlignment(Qt::AlignCenter);

        parameterGrid->addWidget(parameterLabel9, 8, 0, 1, 1);

        parameterLabel8 = new QLabel(gridLayoutWidget);
        parameterLabel8->setObjectName(QStringLiteral("parameterLabel8"));
        parameterLabel8->setFont(font2);
        parameterLabel8->setAlignment(Qt::AlignCenter);

        parameterGrid->addWidget(parameterLabel8, 7, 0, 1, 1);

        parameterLabel5 = new QLabel(gridLayoutWidget);
        parameterLabel5->setObjectName(QStringLiteral("parameterLabel5"));
        parameterLabel5->setFont(font2);
        parameterLabel5->setAlignment(Qt::AlignCenter);

        parameterGrid->addWidget(parameterLabel5, 4, 0, 1, 1);

        parameterLabel3 = new QLabel(gridLayoutWidget);
        parameterLabel3->setObjectName(QStringLiteral("parameterLabel3"));
        parameterLabel3->setFont(font2);
        parameterLabel3->setAlignment(Qt::AlignCenter);

        parameterGrid->addWidget(parameterLabel3, 2, 0, 1, 1);

        parameterLabel6 = new QLabel(gridLayoutWidget);
        parameterLabel6->setObjectName(QStringLiteral("parameterLabel6"));
        parameterLabel6->setFont(font2);
        parameterLabel6->setAlignment(Qt::AlignCenter);

        parameterGrid->addWidget(parameterLabel6, 5, 0, 1, 1);

        paramSlider5 = new QSlider(gridLayoutWidget);
        paramSlider5->setObjectName(QStringLiteral("paramSlider5"));
        paramSlider5->setEnabled(false);
        paramSlider5->setOrientation(Qt::Horizontal);

        parameterGrid->addWidget(paramSlider5, 4, 1, 1, 1);

        paramSlider7 = new QSlider(gridLayoutWidget);
        paramSlider7->setObjectName(QStringLiteral("paramSlider7"));
        paramSlider7->setEnabled(false);
        paramSlider7->setOrientation(Qt::Horizontal);

        parameterGrid->addWidget(paramSlider7, 6, 1, 1, 1);

        paramSlider8 = new QSlider(gridLayoutWidget);
        paramSlider8->setObjectName(QStringLiteral("paramSlider8"));
        paramSlider8->setEnabled(false);
        paramSlider8->setOrientation(Qt::Horizontal);

        parameterGrid->addWidget(paramSlider8, 7, 1, 1, 1);

        paramSlider6 = new QSlider(gridLayoutWidget);
        paramSlider6->setObjectName(QStringLiteral("paramSlider6"));
        paramSlider6->setEnabled(false);
        paramSlider6->setOrientation(Qt::Horizontal);

        parameterGrid->addWidget(paramSlider6, 5, 1, 1, 1);

        paramSpin1 = new QDoubleSpinBox(gridLayoutWidget);
        paramSpin1->setObjectName(QStringLiteral("paramSpin1"));

        parameterGrid->addWidget(paramSpin1, 0, 2, 1, 1);

        paramSlider4 = new QSlider(gridLayoutWidget);
        paramSlider4->setObjectName(QStringLiteral("paramSlider4"));
        paramSlider4->setEnabled(false);
        paramSlider4->setOrientation(Qt::Horizontal);

        parameterGrid->addWidget(paramSlider4, 3, 1, 1, 1);

        paramSlider3 = new QSlider(gridLayoutWidget);
        paramSlider3->setObjectName(QStringLiteral("paramSlider3"));
        paramSlider3->setEnabled(false);
        paramSlider3->setOrientation(Qt::Horizontal);

        parameterGrid->addWidget(paramSlider3, 2, 1, 1, 1);

        parameterLabel1 = new QLabel(gridLayoutWidget);
        parameterLabel1->setObjectName(QStringLiteral("parameterLabel1"));
        parameterLabel1->setFont(font2);
        parameterLabel1->setAlignment(Qt::AlignCenter);

        parameterGrid->addWidget(parameterLabel1, 0, 0, 1, 1);

        parameterLabel7 = new QLabel(gridLayoutWidget);
        parameterLabel7->setObjectName(QStringLiteral("parameterLabel7"));
        parameterLabel7->setFont(font2);
        parameterLabel7->setAlignment(Qt::AlignCenter);

        parameterGrid->addWidget(parameterLabel7, 6, 0, 1, 1);

        paramSlider9 = new QSlider(gridLayoutWidget);
        paramSlider9->setObjectName(QStringLiteral("paramSlider9"));
        paramSlider9->setEnabled(false);
        paramSlider9->setOrientation(Qt::Horizontal);

        parameterGrid->addWidget(paramSlider9, 8, 1, 1, 1);

        paramSpin2 = new QDoubleSpinBox(gridLayoutWidget);
        paramSpin2->setObjectName(QStringLiteral("paramSpin2"));

        parameterGrid->addWidget(paramSpin2, 1, 2, 1, 1);

        paramSpin3 = new QDoubleSpinBox(gridLayoutWidget);
        paramSpin3->setObjectName(QStringLiteral("paramSpin3"));

        parameterGrid->addWidget(paramSpin3, 2, 2, 1, 1);

        paramSpin5 = new QDoubleSpinBox(gridLayoutWidget);
        paramSpin5->setObjectName(QStringLiteral("paramSpin5"));

        parameterGrid->addWidget(paramSpin5, 4, 2, 1, 1);

        paramSpin4 = new QDoubleSpinBox(gridLayoutWidget);
        paramSpin4->setObjectName(QStringLiteral("paramSpin4"));

        parameterGrid->addWidget(paramSpin4, 3, 2, 1, 1);

        paramSpin6 = new QDoubleSpinBox(gridLayoutWidget);
        paramSpin6->setObjectName(QStringLiteral("paramSpin6"));

        parameterGrid->addWidget(paramSpin6, 5, 2, 1, 1);

        paramSpin7 = new QDoubleSpinBox(gridLayoutWidget);
        paramSpin7->setObjectName(QStringLiteral("paramSpin7"));

        parameterGrid->addWidget(paramSpin7, 6, 2, 1, 1);

        paramSpin8 = new QDoubleSpinBox(gridLayoutWidget);
        paramSpin8->setObjectName(QStringLiteral("paramSpin8"));

        parameterGrid->addWidget(paramSpin8, 7, 2, 1, 1);

        paramSpin9 = new QDoubleSpinBox(gridLayoutWidget);
        paramSpin9->setObjectName(QStringLiteral("paramSpin9"));

        parameterGrid->addWidget(paramSpin9, 8, 2, 1, 1);

        textBox = new QTextBrowser(initTab);
        textBox->setObjectName(QStringLiteral("textBox"));
        textBox->setGeometry(QRect(1160, 590, 531, 321));
        gridLayoutWidget_10 = new QWidget(initTab);
        gridLayoutWidget_10->setObjectName(QStringLiteral("gridLayoutWidget_10"));
        gridLayoutWidget_10->setGeometry(QRect(1600, 90, 241, 81));
        gridLayout_9 = new QGridLayout(gridLayoutWidget_10);
        gridLayout_9->setSpacing(6);
        gridLayout_9->setContentsMargins(11, 11, 11, 11);
        gridLayout_9->setObjectName(QStringLiteral("gridLayout_9"));
        gridLayout_9->setContentsMargins(0, 0, 0, 0);
        label_44 = new QLabel(gridLayoutWidget_10);
        label_44->setObjectName(QStringLiteral("label_44"));
        label_44->setFont(font2);

        gridLayout_9->addWidget(label_44, 0, 0, 1, 1);

        label = new QLabel(gridLayoutWidget_10);
        label->setObjectName(QStringLiteral("label"));
        label->setFont(font2);

        gridLayout_9->addWidget(label, 1, 0, 1, 1);

        zoomDoubleSpinBox = new QDoubleSpinBox(gridLayoutWidget_10);
        zoomDoubleSpinBox->setObjectName(QStringLiteral("zoomDoubleSpinBox"));
        zoomDoubleSpinBox->setMinimum(0.43);
        zoomDoubleSpinBox->setMaximum(3);
        zoomDoubleSpinBox->setSingleStep(0.1);
        zoomDoubleSpinBox->setValue(0.43);

        gridLayout_9->addWidget(zoomDoubleSpinBox, 0, 1, 1, 1);

        finalLens_comboBox = new QComboBox(gridLayoutWidget_10);
        finalLens_comboBox->setObjectName(QStringLiteral("finalLens_comboBox"));

        gridLayout_9->addWidget(finalLens_comboBox, 1, 1, 1, 1);

        tabWidget->addTab(initTab, QString());
        ctrlTab = new QWidget();
        ctrlTab->setObjectName(QStringLiteral("ctrlTab"));
        stopButton = new QPushButton(ctrlTab);
        stopButton->setObjectName(QStringLiteral("stopButton"));
        stopButton->setGeometry(QRect(1750, 0, 91, 31));
        stopButton->setFont(font);
        frame = new QFrame(ctrlTab);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setGeometry(QRect(230, 210, 800, 800));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        gridLayoutWidget_3 = new QWidget(frame);
        gridLayoutWidget_3->setObjectName(QStringLiteral("gridLayoutWidget_3"));
        gridLayoutWidget_3->setGeometry(QRect(-1, 0, 801, 801));
        gridLayout_2 = new QGridLayout(gridLayoutWidget_3);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        camera0View = new CQtOpenCVViewerGl(gridLayoutWidget_3);
        camera0View->setObjectName(QStringLiteral("camera0View"));

        gridLayout_2->addWidget(camera0View, 0, 0, 1, 1);

        frame_2 = new QFrame(ctrlTab);
        frame_2->setObjectName(QStringLiteral("frame_2"));
        frame_2->setGeometry(QRect(1040, 210, 800, 800));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        gridLayoutWidget_4 = new QWidget(frame_2);
        gridLayoutWidget_4->setObjectName(QStringLiteral("gridLayoutWidget_4"));
        gridLayoutWidget_4->setGeometry(QRect(0, 0, 801, 801));
        gridLayout_3 = new QGridLayout(gridLayoutWidget_4);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        gridLayout_3->setContentsMargins(0, 0, 0, 0);
        camera1View = new CQtOpenCVViewerGl(gridLayoutWidget_4);
        camera1View->setObjectName(QStringLiteral("camera1View"));

        gridLayout_3->addWidget(camera1View, 0, 0, 1, 1);

        gridLayoutWidget_5 = new QWidget(ctrlTab);
        gridLayoutWidget_5->setObjectName(QStringLiteral("gridLayoutWidget_5"));
        gridLayoutWidget_5->setGeometry(QRect(1440, 30, 395, 161));
        gridLayout_4 = new QGridLayout(gridLayoutWidget_5);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        gridLayout_4->setContentsMargins(0, 0, 0, 0);
        coil_progressBar_5 = new QProgressBar(gridLayoutWidget_5);
        coil_progressBar_5->setObjectName(QStringLiteral("coil_progressBar_5"));
        coil_progressBar_5->setMaximum(10000);
        coil_progressBar_5->setValue(0);

        gridLayout_4->addWidget(coil_progressBar_5, 1, 2, 1, 1);

        coil_progressBar_9 = new QProgressBar(gridLayoutWidget_5);
        coil_progressBar_9->setObjectName(QStringLiteral("coil_progressBar_9"));
        coil_progressBar_9->setMaximum(10000);
        coil_progressBar_9->setValue(0);

        gridLayout_4->addWidget(coil_progressBar_9, 2, 3, 1, 1);

        coil_progressBar_1 = new QProgressBar(gridLayoutWidget_5);
        coil_progressBar_1->setObjectName(QStringLiteral("coil_progressBar_1"));
        coil_progressBar_1->setMaximum(10000);
        coil_progressBar_1->setValue(0);

        gridLayout_4->addWidget(coil_progressBar_1, 0, 1, 1, 1);

        coil_progressBar_3 = new QProgressBar(gridLayoutWidget_5);
        coil_progressBar_3->setObjectName(QStringLiteral("coil_progressBar_3"));
        coil_progressBar_3->setMaximum(10000);
        coil_progressBar_3->setValue(0);

        gridLayout_4->addWidget(coil_progressBar_3, 0, 3, 1, 1);

        coil_progressBar_4 = new QProgressBar(gridLayoutWidget_5);
        coil_progressBar_4->setObjectName(QStringLiteral("coil_progressBar_4"));
        coil_progressBar_4->setMaximum(10000);
        coil_progressBar_4->setValue(0);

        gridLayout_4->addWidget(coil_progressBar_4, 1, 1, 1, 1);

        coil_progressBar_7 = new QProgressBar(gridLayoutWidget_5);
        coil_progressBar_7->setObjectName(QStringLiteral("coil_progressBar_7"));
        coil_progressBar_7->setMaximum(10000);
        coil_progressBar_7->setValue(0);

        gridLayout_4->addWidget(coil_progressBar_7, 2, 1, 1, 1);

        coil_progressBar_8 = new QProgressBar(gridLayoutWidget_5);
        coil_progressBar_8->setObjectName(QStringLiteral("coil_progressBar_8"));
        coil_progressBar_8->setMaximum(10000);
        coil_progressBar_8->setValue(0);

        gridLayout_4->addWidget(coil_progressBar_8, 2, 2, 1, 1);

        coil_progressBar_6 = new QProgressBar(gridLayoutWidget_5);
        coil_progressBar_6->setObjectName(QStringLiteral("coil_progressBar_6"));
        coil_progressBar_6->setMaximum(10000);
        coil_progressBar_6->setValue(0);

        gridLayout_4->addWidget(coil_progressBar_6, 1, 3, 1, 1);

        coil_progressBar_2 = new QProgressBar(gridLayoutWidget_5);
        coil_progressBar_2->setObjectName(QStringLiteral("coil_progressBar_2"));
        coil_progressBar_2->setMaximum(10000);
        coil_progressBar_2->setValue(0);

        gridLayout_4->addWidget(coil_progressBar_2, 0, 2, 1, 1);

        ctrl_en_checkBox = new QCheckBox(gridLayoutWidget_5);
        ctrl_en_checkBox->setObjectName(QStringLiteral("ctrl_en_checkBox"));

        gridLayout_4->addWidget(ctrl_en_checkBox, 3, 1, 1, 1);

        record_checkBox = new QCheckBox(gridLayoutWidget_5);
        record_checkBox->setObjectName(QStringLiteral("record_checkBox"));

        gridLayout_4->addWidget(record_checkBox, 3, 2, 1, 1);

        recordRaw_checkBox = new QCheckBox(gridLayoutWidget_5);
        recordRaw_checkBox->setObjectName(QStringLiteral("recordRaw_checkBox"));

        gridLayout_4->addWidget(recordRaw_checkBox, 3, 3, 1, 1);

        gravityEn_checkBox = new QCheckBox(gridLayoutWidget_5);
        gravityEn_checkBox->setObjectName(QStringLiteral("gravityEn_checkBox"));

        gridLayout_4->addWidget(gravityEn_checkBox, 0, 0, 1, 1);

        omega_checkBox = new QCheckBox(gridLayoutWidget_5);
        omega_checkBox->setObjectName(QStringLiteral("omega_checkBox"));

        gridLayout_4->addWidget(omega_checkBox, 2, 0, 1, 1);

        label_2 = new QLabel(ctrlTab);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(580, 160, 101, 21));
        label_2->setFont(font1);
        frame_3 = new QFrame(ctrlTab);
        frame_3->setObjectName(QStringLiteral("frame_3"));
        frame_3->setGeometry(QRect(10, 0, 211, 1001));
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);
        gridLayoutWidget_7 = new QWidget(frame_3);
        gridLayoutWidget_7->setObjectName(QStringLiteral("gridLayoutWidget_7"));
        gridLayoutWidget_7->setGeometry(QRect(0, 0, 211, 151));
        gridLayout_6 = new QGridLayout(gridLayoutWidget_7);
        gridLayout_6->setSpacing(6);
        gridLayout_6->setContentsMargins(11, 11, 11, 11);
        gridLayout_6->setObjectName(QStringLiteral("gridLayout_6"));
        gridLayout_6->setContentsMargins(0, 0, 0, 0);
        label_39 = new QLabel(gridLayoutWidget_7);
        label_39->setObjectName(QStringLiteral("label_39"));

        gridLayout_6->addWidget(label_39, 4, 0, 1, 1);

        label_37 = new QLabel(gridLayoutWidget_7);
        label_37->setObjectName(QStringLiteral("label_37"));

        gridLayout_6->addWidget(label_37, 2, 0, 1, 1);

        usROI_x_spinBox = new QSpinBox(gridLayoutWidget_7);
        usROI_x_spinBox->setObjectName(QStringLiteral("usROI_x_spinBox"));
        usROI_x_spinBox->setMaximum(1023);
        usROI_x_spinBox->setValue(413);

        gridLayout_6->addWidget(usROI_x_spinBox, 1, 1, 1, 1);

        label_35 = new QLabel(gridLayoutWidget_7);
        label_35->setObjectName(QStringLiteral("label_35"));
        label_35->setFont(font2);
        label_35->setAlignment(Qt::AlignCenter);

        gridLayout_6->addWidget(label_35, 0, 0, 1, 2);

        label_36 = new QLabel(gridLayoutWidget_7);
        label_36->setObjectName(QStringLiteral("label_36"));

        gridLayout_6->addWidget(label_36, 1, 0, 1, 1);

        usROI_y_spinBox = new QSpinBox(gridLayoutWidget_7);
        usROI_y_spinBox->setObjectName(QStringLiteral("usROI_y_spinBox"));
        usROI_y_spinBox->setMaximum(1023);
        usROI_y_spinBox->setValue(56);

        gridLayout_6->addWidget(usROI_y_spinBox, 2, 1, 1, 1);

        label_38 = new QLabel(gridLayoutWidget_7);
        label_38->setObjectName(QStringLiteral("label_38"));

        gridLayout_6->addWidget(label_38, 3, 0, 1, 1);

        usROI_height_spinBox = new QSpinBox(gridLayoutWidget_7);
        usROI_height_spinBox->setObjectName(QStringLiteral("usROI_height_spinBox"));
        usROI_height_spinBox->setMaximum(1024);
        usROI_height_spinBox->setValue(537);

        gridLayout_6->addWidget(usROI_height_spinBox, 3, 1, 1, 1);

        usROI_width_spinBox = new QSpinBox(gridLayoutWidget_7);
        usROI_width_spinBox->setObjectName(QStringLiteral("usROI_width_spinBox"));
        usROI_width_spinBox->setMaximum(1024);
        usROI_width_spinBox->setValue(570);

        gridLayout_6->addWidget(usROI_width_spinBox, 4, 1, 1, 1);

        line = new QFrame(frame_3);
        line->setObjectName(QStringLiteral("line"));
        line->setGeometry(QRect(10, 150, 191, 20));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);
        gridLayoutWidget_8 = new QWidget(frame_3);
        gridLayoutWidget_8->setObjectName(QStringLiteral("gridLayoutWidget_8"));
        gridLayoutWidget_8->setGeometry(QRect(0, 170, 211, 82));
        gridLayout_7 = new QGridLayout(gridLayoutWidget_8);
        gridLayout_7->setSpacing(6);
        gridLayout_7->setContentsMargins(11, 11, 11, 11);
        gridLayout_7->setObjectName(QStringLiteral("gridLayout_7"));
        gridLayout_7->setContentsMargins(0, 0, 0, 0);
        PMenable = new QCheckBox(gridLayoutWidget_8);
        PMenable->setObjectName(QStringLiteral("PMenable"));

        gridLayout_7->addWidget(PMenable, 1, 1, 1, 1);

        PermMag_progressBar_2 = new QProgressBar(gridLayoutWidget_8);
        PermMag_progressBar_2->setObjectName(QStringLiteral("PermMag_progressBar_2"));
        PermMag_progressBar_2->setMaximum(150);
        PermMag_progressBar_2->setValue(24);

        gridLayout_7->addWidget(PermMag_progressBar_2, 0, 0, 1, 2);

        Pm_doubleSpinBox = new QDoubleSpinBox(gridLayoutWidget_8);
        Pm_doubleSpinBox->setObjectName(QStringLiteral("Pm_doubleSpinBox"));
        Pm_doubleSpinBox->setMaximum(150);
        Pm_doubleSpinBox->setSingleStep(0.01);
        Pm_doubleSpinBox->setValue(1);

        gridLayout_7->addWidget(Pm_doubleSpinBox, 1, 0, 1, 1);

        line_2 = new QFrame(frame_3);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setGeometry(QRect(10, 250, 191, 20));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);
        pushButton = new QPushButton(frame_3);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(9, 270, 191, 22));
        gridLayoutWidget_9 = new QWidget(frame_3);
        gridLayoutWidget_9->setObjectName(QStringLiteral("gridLayoutWidget_9"));
        gridLayoutWidget_9->setGeometry(QRect(0, 340, 211, 103));
        gridLayout_8 = new QGridLayout(gridLayoutWidget_9);
        gridLayout_8->setSpacing(6);
        gridLayout_8->setContentsMargins(11, 11, 11, 11);
        gridLayout_8->setObjectName(QStringLiteral("gridLayout_8"));
        gridLayout_8->setContentsMargins(0, 0, 0, 0);
        label_41 = new QLabel(gridLayoutWidget_9);
        label_41->setObjectName(QStringLiteral("label_41"));

        gridLayout_8->addWidget(label_41, 1, 0, 1, 1);

        label_40 = new QLabel(gridLayoutWidget_9);
        label_40->setObjectName(QStringLiteral("label_40"));
        label_40->setAlignment(Qt::AlignCenter);

        gridLayout_8->addWidget(label_40, 0, 0, 1, 2);

        refUb_SpinBox = new QDoubleSpinBox(gridLayoutWidget_9);
        refUb_SpinBox->setObjectName(QStringLiteral("refUb_SpinBox"));
        refUb_SpinBox->setSingleStep(0.1);
        refUb_SpinBox->setValue(1.5);

        gridLayout_8->addWidget(refUb_SpinBox, 2, 1, 1, 1);

        refTau_SpinBox = new QDoubleSpinBox(gridLayoutWidget_9);
        refTau_SpinBox->setObjectName(QStringLiteral("refTau_SpinBox"));
        refTau_SpinBox->setSingleStep(0.1);
        refTau_SpinBox->setValue(0.5);

        gridLayout_8->addWidget(refTau_SpinBox, 1, 1, 1, 1);

        label_42 = new QLabel(gridLayoutWidget_9);
        label_42->setObjectName(QStringLiteral("label_42"));

        gridLayout_8->addWidget(label_42, 2, 0, 1, 1);

        label_43 = new QLabel(gridLayoutWidget_9);
        label_43->setObjectName(QStringLiteral("label_43"));

        gridLayout_8->addWidget(label_43, 3, 0, 1, 1);

        refLb_SpinBox = new QDoubleSpinBox(gridLayoutWidget_9);
        refLb_SpinBox->setObjectName(QStringLiteral("refLb_SpinBox"));
        refLb_SpinBox->setSingleStep(0.1);
        refLb_SpinBox->setValue(0.3);

        gridLayout_8->addWidget(refLb_SpinBox, 3, 1, 1, 1);

        grid_Button = new QPushButton(frame_3);
        grid_Button->setObjectName(QStringLiteral("grid_Button"));
        grid_Button->setGeometry(QRect(10, 300, 191, 22));
        line_4 = new QFrame(frame_3);
        line_4->setObjectName(QStringLiteral("line_4"));
        line_4->setGeometry(QRect(10, 320, 191, 20));
        line_4->setFrameShape(QFrame::HLine);
        line_4->setFrameShadow(QFrame::Sunken);
        verticalLayoutWidget_3 = new QWidget(frame_3);
        verticalLayoutWidget_3->setObjectName(QStringLiteral("verticalLayoutWidget_3"));
        verticalLayoutWidget_3->setGeometry(QRect(9, 450, 191, 136));
        verticalLayout_2 = new QVBoxLayout(verticalLayoutWidget_3);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        cameraSet_pushButton = new QPushButton(verticalLayoutWidget_3);
        cameraSet_pushButton->setObjectName(QStringLiteral("cameraSet_pushButton"));

        verticalLayout_2->addWidget(cameraSet_pushButton);

        saveImage_button = new QPushButton(verticalLayoutWidget_3);
        saveImage_button->setObjectName(QStringLiteral("saveImage_button"));

        verticalLayout_2->addWidget(saveImage_button);

        forceCur_button = new QPushButton(verticalLayoutWidget_3);
        forceCur_button->setObjectName(QStringLiteral("forceCur_button"));

        verticalLayout_2->addWidget(forceCur_button);

        resetElmo_pushButton = new QPushButton(verticalLayoutWidget_3);
        resetElmo_pushButton->setObjectName(QStringLiteral("resetElmo_pushButton"));

        verticalLayout_2->addWidget(resetElmo_pushButton);

        ctrlSettings_button = new QPushButton(verticalLayoutWidget_3);
        ctrlSettings_button->setObjectName(QStringLiteral("ctrlSettings_button"));

        verticalLayout_2->addWidget(ctrlSettings_button);

        camera0Speed = new QSlider(ctrlTab);
        camera0Speed->setObjectName(QStringLiteral("camera0Speed"));
        camera0Speed->setGeometry(QRect(230, 180, 801, 21));
        camera0Speed->setMinimum(-100000);
        camera0Speed->setMaximum(100000);
        camera0Speed->setOrientation(Qt::Horizontal);
        label_3 = new QLabel(ctrlTab);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(1390, 160, 101, 21));
        label_3->setFont(font1);
        camera1Speed = new QSlider(ctrlTab);
        camera1Speed->setObjectName(QStringLiteral("camera1Speed"));
        camera1Speed->setGeometry(QRect(1040, 180, 801, 21));
        camera1Speed->setMinimum(-100000);
        camera1Speed->setMaximum(100000);
        camera1Speed->setOrientation(Qt::Horizontal);
        camera1Speed->setTickPosition(QSlider::NoTicks);
        camera1Speed->setTickInterval(50);
        autofocus0_checkBox = new QCheckBox(ctrlTab);
        autofocus0_checkBox->setObjectName(QStringLiteral("autofocus0_checkBox"));
        autofocus0_checkBox->setGeometry(QRect(230, 160, 151, 20));
        autofocus1_checkBox = new QCheckBox(ctrlTab);
        autofocus1_checkBox->setObjectName(QStringLiteral("autofocus1_checkBox"));
        autofocus1_checkBox->setGeometry(QRect(1040, 160, 151, 20));
        verticalLayoutWidget_2 = new QWidget(ctrlTab);
        verticalLayoutWidget_2->setObjectName(QStringLiteral("verticalLayoutWidget_2"));
        verticalLayoutWidget_2->setGeometry(QRect(229, 9, 801, 141));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget_2);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        label_4 = new QLabel(verticalLayoutWidget_2);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setLayoutDirection(Qt::LeftToRight);
        label_4->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_4);

        ctrl_textBrowser = new QTextBrowser(verticalLayoutWidget_2);
        ctrl_textBrowser->setObjectName(QStringLiteral("ctrl_textBrowser"));
        ctrl_textBrowser->setFrameShadow(QFrame::Sunken);
        ctrl_textBrowser->setOverwriteMode(true);

        verticalLayout->addWidget(ctrl_textBrowser);

        tabWidget->addTab(ctrlTab, QString());
        Debug = new QWidget();
        Debug->setObjectName(QStringLiteral("Debug"));
        gridLayoutWidget_6 = new QWidget(Debug);
        gridLayoutWidget_6->setObjectName(QStringLiteral("gridLayoutWidget_6"));
        gridLayoutWidget_6->setGeometry(QRect(0, 20, 455, 403));
        gridLayout_5 = new QGridLayout(gridLayoutWidget_6);
        gridLayout_5->setSpacing(6);
        gridLayout_5->setContentsMargins(11, 11, 11, 11);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        gridLayout_5->setContentsMargins(0, 0, 0, 0);
        label_18 = new QLabel(gridLayoutWidget_6);
        label_18->setObjectName(QStringLiteral("label_18"));

        gridLayout_5->addWidget(label_18, 5, 5, 1, 1);

        frequencySpinBox9 = new QDoubleSpinBox(gridLayoutWidget_6);
        frequencySpinBox9->setObjectName(QStringLiteral("frequencySpinBox9"));

        gridLayout_5->addWidget(frequencySpinBox9, 11, 6, 1, 1);

        frequencySpinBox3 = new QDoubleSpinBox(gridLayoutWidget_6);
        frequencySpinBox3->setObjectName(QStringLiteral("frequencySpinBox3"));

        gridLayout_5->addWidget(frequencySpinBox3, 2, 6, 1, 1);

        label_23 = new QLabel(gridLayoutWidget_6);
        label_23->setObjectName(QStringLiteral("label_23"));

        gridLayout_5->addWidget(label_23, 0, 7, 1, 1);

        frequencySpinBox5 = new QDoubleSpinBox(gridLayoutWidget_6);
        frequencySpinBox5->setObjectName(QStringLiteral("frequencySpinBox5"));

        gridLayout_5->addWidget(frequencySpinBox5, 5, 6, 1, 1);

        coil_progressBar_4deb = new QProgressBar(gridLayoutWidget_6);
        coil_progressBar_4deb->setObjectName(QStringLiteral("coil_progressBar_4deb"));
        coil_progressBar_4deb->setMaximum(10000);
        coil_progressBar_4deb->setValue(0);

        gridLayout_5->addWidget(coil_progressBar_4deb, 4, 1, 1, 1);

        coil_progressBar_9deb = new QProgressBar(gridLayoutWidget_6);
        coil_progressBar_9deb->setObjectName(QStringLiteral("coil_progressBar_9deb"));
        coil_progressBar_9deb->setMaximum(10000);
        coil_progressBar_9deb->setValue(0);

        gridLayout_5->addWidget(coil_progressBar_9deb, 11, 1, 1, 1);

        coil_progressBar_1deb = new QProgressBar(gridLayoutWidget_6);
        coil_progressBar_1deb->setObjectName(QStringLiteral("coil_progressBar_1deb"));
        coil_progressBar_1deb->setMaximum(10000);
        coil_progressBar_1deb->setValue(0);

        gridLayout_5->addWidget(coil_progressBar_1deb, 0, 1, 1, 1);

        coil_progressBar_7deb = new QProgressBar(gridLayoutWidget_6);
        coil_progressBar_7deb->setObjectName(QStringLiteral("coil_progressBar_7deb"));
        coil_progressBar_7deb->setMaximum(10000);
        coil_progressBar_7deb->setValue(0);

        gridLayout_5->addWidget(coil_progressBar_7deb, 9, 1, 1, 1);

        coil_progressBar_8deb = new QProgressBar(gridLayoutWidget_6);
        coil_progressBar_8deb->setObjectName(QStringLiteral("coil_progressBar_8deb"));
        coil_progressBar_8deb->setMaximum(10000);
        coil_progressBar_8deb->setValue(0);

        gridLayout_5->addWidget(coil_progressBar_8deb, 10, 1, 1, 1);

        coil_progressBar_2deb = new QProgressBar(gridLayoutWidget_6);
        coil_progressBar_2deb->setObjectName(QStringLiteral("coil_progressBar_2deb"));
        coil_progressBar_2deb->setMaximum(10000);
        coil_progressBar_2deb->setValue(0);

        gridLayout_5->addWidget(coil_progressBar_2deb, 1, 1, 1, 1);

        coil_progressBar_5deb = new QProgressBar(gridLayoutWidget_6);
        coil_progressBar_5deb->setObjectName(QStringLiteral("coil_progressBar_5deb"));
        coil_progressBar_5deb->setMaximum(10000);
        coil_progressBar_5deb->setValue(0);

        gridLayout_5->addWidget(coil_progressBar_5deb, 5, 1, 1, 1);

        record_checkBox_2 = new QCheckBox(gridLayoutWidget_6);
        record_checkBox_2->setObjectName(QStringLiteral("record_checkBox_2"));

        gridLayout_5->addWidget(record_checkBox_2, 15, 1, 1, 1);

        recordRaw_checkBox_2 = new QCheckBox(gridLayoutWidget_6);
        recordRaw_checkBox_2->setObjectName(QStringLiteral("recordRaw_checkBox_2"));

        gridLayout_5->addWidget(recordRaw_checkBox_2, 16, 1, 1, 1);

        coil_progressBar_6deb = new QProgressBar(gridLayoutWidget_6);
        coil_progressBar_6deb->setObjectName(QStringLiteral("coil_progressBar_6deb"));
        coil_progressBar_6deb->setMaximum(10000);
        coil_progressBar_6deb->setValue(0);

        gridLayout_5->addWidget(coil_progressBar_6deb, 7, 1, 1, 1);

        Cur1Debug_doubleSpinBox = new QDoubleSpinBox(gridLayoutWidget_6);
        Cur1Debug_doubleSpinBox->setObjectName(QStringLiteral("Cur1Debug_doubleSpinBox"));
        Cur1Debug_doubleSpinBox->setMinimum(-10);
        Cur1Debug_doubleSpinBox->setMaximum(10);
        Cur1Debug_doubleSpinBox->setSingleStep(0.1);

        gridLayout_5->addWidget(Cur1Debug_doubleSpinBox, 0, 2, 1, 1);

        Cur2Debug_doubleSpinBox = new QDoubleSpinBox(gridLayoutWidget_6);
        Cur2Debug_doubleSpinBox->setObjectName(QStringLiteral("Cur2Debug_doubleSpinBox"));
        Cur2Debug_doubleSpinBox->setMinimum(-10);
        Cur2Debug_doubleSpinBox->setMaximum(10);
        Cur2Debug_doubleSpinBox->setSingleStep(0.1);

        gridLayout_5->addWidget(Cur2Debug_doubleSpinBox, 1, 2, 1, 1);

        coil_progressBar_3deb = new QProgressBar(gridLayoutWidget_6);
        coil_progressBar_3deb->setObjectName(QStringLiteral("coil_progressBar_3deb"));
        coil_progressBar_3deb->setMaximum(10000);
        coil_progressBar_3deb->setValue(0);

        gridLayout_5->addWidget(coil_progressBar_3deb, 2, 1, 1, 1);

        Cur7Debug_doubleSpinBox = new QDoubleSpinBox(gridLayoutWidget_6);
        Cur7Debug_doubleSpinBox->setObjectName(QStringLiteral("Cur7Debug_doubleSpinBox"));
        Cur7Debug_doubleSpinBox->setMinimum(-10);
        Cur7Debug_doubleSpinBox->setMaximum(5);
        Cur7Debug_doubleSpinBox->setSingleStep(0.1);

        gridLayout_5->addWidget(Cur7Debug_doubleSpinBox, 9, 2, 1, 1);

        Cur9Debug_doubleSpinBox = new QDoubleSpinBox(gridLayoutWidget_6);
        Cur9Debug_doubleSpinBox->setObjectName(QStringLiteral("Cur9Debug_doubleSpinBox"));
        Cur9Debug_doubleSpinBox->setMinimum(-10);
        Cur9Debug_doubleSpinBox->setMaximum(5);
        Cur9Debug_doubleSpinBox->setSingleStep(0.1);

        gridLayout_5->addWidget(Cur9Debug_doubleSpinBox, 11, 2, 1, 1);

        Cur8Debug_doubleSpinBox = new QDoubleSpinBox(gridLayoutWidget_6);
        Cur8Debug_doubleSpinBox->setObjectName(QStringLiteral("Cur8Debug_doubleSpinBox"));
        Cur8Debug_doubleSpinBox->setMinimum(-10);
        Cur8Debug_doubleSpinBox->setMaximum(5);
        Cur8Debug_doubleSpinBox->setSingleStep(0.1);

        gridLayout_5->addWidget(Cur8Debug_doubleSpinBox, 10, 2, 1, 1);

        Cur5Debug_doubleSpinBox = new QDoubleSpinBox(gridLayoutWidget_6);
        Cur5Debug_doubleSpinBox->setObjectName(QStringLiteral("Cur5Debug_doubleSpinBox"));
        Cur5Debug_doubleSpinBox->setMinimum(-10);
        Cur5Debug_doubleSpinBox->setMaximum(5);
        Cur5Debug_doubleSpinBox->setSingleStep(0.1);

        gridLayout_5->addWidget(Cur5Debug_doubleSpinBox, 5, 2, 1, 1);

        Cur6Debug_doubleSpinBox = new QDoubleSpinBox(gridLayoutWidget_6);
        Cur6Debug_doubleSpinBox->setObjectName(QStringLiteral("Cur6Debug_doubleSpinBox"));
        Cur6Debug_doubleSpinBox->setMinimum(-10);
        Cur6Debug_doubleSpinBox->setMaximum(5);
        Cur6Debug_doubleSpinBox->setSingleStep(0.1);

        gridLayout_5->addWidget(Cur6Debug_doubleSpinBox, 7, 2, 1, 1);

        Cur3Debug_doubleSpinBox = new QDoubleSpinBox(gridLayoutWidget_6);
        Cur3Debug_doubleSpinBox->setObjectName(QStringLiteral("Cur3Debug_doubleSpinBox"));
        Cur3Debug_doubleSpinBox->setMinimum(-10);
        Cur3Debug_doubleSpinBox->setMaximum(10);
        Cur3Debug_doubleSpinBox->setSingleStep(0.1);

        gridLayout_5->addWidget(Cur3Debug_doubleSpinBox, 2, 2, 1, 1);

        Cur4Debug_doubleSpinBox = new QDoubleSpinBox(gridLayoutWidget_6);
        Cur4Debug_doubleSpinBox->setObjectName(QStringLiteral("Cur4Debug_doubleSpinBox"));
        Cur4Debug_doubleSpinBox->setMinimum(-10);
        Cur4Debug_doubleSpinBox->setMaximum(10);
        Cur4Debug_doubleSpinBox->setSingleStep(0.1);

        gridLayout_5->addWidget(Cur4Debug_doubleSpinBox, 4, 2, 1, 1);

        deb_en_checkBox = new QCheckBox(gridLayoutWidget_6);
        deb_en_checkBox->setObjectName(QStringLiteral("deb_en_checkBox"));

        gridLayout_5->addWidget(deb_en_checkBox, 13, 1, 1, 1);

        AC5_SpinBox = new QDoubleSpinBox(gridLayoutWidget_6);
        AC5_SpinBox->setObjectName(QStringLiteral("AC5_SpinBox"));
        AC5_SpinBox->setMinimum(-5);
        AC5_SpinBox->setMaximum(5);
        AC5_SpinBox->setSingleStep(0.1);

        gridLayout_5->addWidget(AC5_SpinBox, 5, 4, 1, 1);

        AC2_SpinBox = new QDoubleSpinBox(gridLayoutWidget_6);
        AC2_SpinBox->setObjectName(QStringLiteral("AC2_SpinBox"));
        AC2_SpinBox->setMinimum(-5);
        AC2_SpinBox->setMaximum(5);
        AC2_SpinBox->setSingleStep(0.1);

        gridLayout_5->addWidget(AC2_SpinBox, 1, 4, 1, 1);

        label_5 = new QLabel(gridLayoutWidget_6);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout_5->addWidget(label_5, 0, 3, 1, 1);

        label_7 = new QLabel(gridLayoutWidget_6);
        label_7->setObjectName(QStringLiteral("label_7"));

        gridLayout_5->addWidget(label_7, 1, 3, 1, 1);

        label_6 = new QLabel(gridLayoutWidget_6);
        label_6->setObjectName(QStringLiteral("label_6"));

        gridLayout_5->addWidget(label_6, 2, 3, 1, 1);

        label_10 = new QLabel(gridLayoutWidget_6);
        label_10->setObjectName(QStringLiteral("label_10"));

        gridLayout_5->addWidget(label_10, 4, 3, 1, 1);

        label_13 = new QLabel(gridLayoutWidget_6);
        label_13->setObjectName(QStringLiteral("label_13"));

        gridLayout_5->addWidget(label_13, 11, 3, 1, 1);

        label_8 = new QLabel(gridLayoutWidget_6);
        label_8->setObjectName(QStringLiteral("label_8"));

        gridLayout_5->addWidget(label_8, 7, 3, 1, 1);

        label_9 = new QLabel(gridLayoutWidget_6);
        label_9->setObjectName(QStringLiteral("label_9"));

        gridLayout_5->addWidget(label_9, 5, 3, 1, 1);

        label_11 = new QLabel(gridLayoutWidget_6);
        label_11->setObjectName(QStringLiteral("label_11"));

        gridLayout_5->addWidget(label_11, 10, 3, 1, 1);

        label_12 = new QLabel(gridLayoutWidget_6);
        label_12->setObjectName(QStringLiteral("label_12"));

        gridLayout_5->addWidget(label_12, 9, 3, 1, 1);

        AC1_SpinBox = new QDoubleSpinBox(gridLayoutWidget_6);
        AC1_SpinBox->setObjectName(QStringLiteral("AC1_SpinBox"));
        AC1_SpinBox->setMinimum(-5);
        AC1_SpinBox->setMaximum(5);

        gridLayout_5->addWidget(AC1_SpinBox, 0, 4, 1, 1);

        AC3_SpinBox = new QDoubleSpinBox(gridLayoutWidget_6);
        AC3_SpinBox->setObjectName(QStringLiteral("AC3_SpinBox"));
        AC3_SpinBox->setMinimum(-5);
        AC3_SpinBox->setMaximum(5);
        AC3_SpinBox->setSingleStep(0.1);

        gridLayout_5->addWidget(AC3_SpinBox, 2, 4, 1, 1);

        AC4_SpinBox = new QDoubleSpinBox(gridLayoutWidget_6);
        AC4_SpinBox->setObjectName(QStringLiteral("AC4_SpinBox"));
        AC4_SpinBox->setMinimum(-5);
        AC4_SpinBox->setMaximum(5);
        AC4_SpinBox->setSingleStep(0.1);

        gridLayout_5->addWidget(AC4_SpinBox, 4, 4, 1, 1);

        AC6_SpinBox = new QDoubleSpinBox(gridLayoutWidget_6);
        AC6_SpinBox->setObjectName(QStringLiteral("AC6_SpinBox"));
        AC6_SpinBox->setMinimum(-5);
        AC6_SpinBox->setMaximum(5);
        AC6_SpinBox->setSingleStep(0.1);

        gridLayout_5->addWidget(AC6_SpinBox, 7, 4, 1, 1);

        AC7_SpinBox = new QDoubleSpinBox(gridLayoutWidget_6);
        AC7_SpinBox->setObjectName(QStringLiteral("AC7_SpinBox"));
        AC7_SpinBox->setMinimum(-5);
        AC7_SpinBox->setMaximum(5);
        AC7_SpinBox->setSingleStep(0.1);

        gridLayout_5->addWidget(AC7_SpinBox, 9, 4, 1, 1);

        label_14 = new QLabel(gridLayoutWidget_6);
        label_14->setObjectName(QStringLiteral("label_14"));

        gridLayout_5->addWidget(label_14, 0, 5, 1, 1);

        AC8_SpinBox = new QDoubleSpinBox(gridLayoutWidget_6);
        AC8_SpinBox->setObjectName(QStringLiteral("AC8_SpinBox"));
        AC8_SpinBox->setMinimum(-5);
        AC8_SpinBox->setMaximum(5);
        AC8_SpinBox->setSingleStep(0.1);

        gridLayout_5->addWidget(AC8_SpinBox, 10, 4, 1, 1);

        label_15 = new QLabel(gridLayoutWidget_6);
        label_15->setObjectName(QStringLiteral("label_15"));

        gridLayout_5->addWidget(label_15, 1, 5, 1, 1);

        AC9_SpinBox = new QDoubleSpinBox(gridLayoutWidget_6);
        AC9_SpinBox->setObjectName(QStringLiteral("AC9_SpinBox"));
        AC9_SpinBox->setMinimum(-5);
        AC9_SpinBox->setMaximum(5);
        AC9_SpinBox->setSingleStep(0.1);

        gridLayout_5->addWidget(AC9_SpinBox, 11, 4, 1, 1);

        label_16 = new QLabel(gridLayoutWidget_6);
        label_16->setObjectName(QStringLiteral("label_16"));

        gridLayout_5->addWidget(label_16, 2, 5, 1, 1);

        label_17 = new QLabel(gridLayoutWidget_6);
        label_17->setObjectName(QStringLiteral("label_17"));

        gridLayout_5->addWidget(label_17, 4, 5, 1, 1);

        label_19 = new QLabel(gridLayoutWidget_6);
        label_19->setObjectName(QStringLiteral("label_19"));

        gridLayout_5->addWidget(label_19, 7, 5, 1, 1);

        label_22 = new QLabel(gridLayoutWidget_6);
        label_22->setObjectName(QStringLiteral("label_22"));

        gridLayout_5->addWidget(label_22, 11, 5, 1, 1);

        label_20 = new QLabel(gridLayoutWidget_6);
        label_20->setObjectName(QStringLiteral("label_20"));

        gridLayout_5->addWidget(label_20, 9, 5, 1, 1);

        label_21 = new QLabel(gridLayoutWidget_6);
        label_21->setObjectName(QStringLiteral("label_21"));

        gridLayout_5->addWidget(label_21, 10, 5, 1, 1);

        frequencySpinBox1 = new QDoubleSpinBox(gridLayoutWidget_6);
        frequencySpinBox1->setObjectName(QStringLiteral("frequencySpinBox1"));

        gridLayout_5->addWidget(frequencySpinBox1, 0, 6, 1, 1);

        frequencySpinBox2 = new QDoubleSpinBox(gridLayoutWidget_6);
        frequencySpinBox2->setObjectName(QStringLiteral("frequencySpinBox2"));

        gridLayout_5->addWidget(frequencySpinBox2, 1, 6, 1, 1);

        frequencySpinBox6 = new QDoubleSpinBox(gridLayoutWidget_6);
        frequencySpinBox6->setObjectName(QStringLiteral("frequencySpinBox6"));

        gridLayout_5->addWidget(frequencySpinBox6, 7, 6, 1, 1);

        frequencySpinBox4 = new QDoubleSpinBox(gridLayoutWidget_6);
        frequencySpinBox4->setObjectName(QStringLiteral("frequencySpinBox4"));

        gridLayout_5->addWidget(frequencySpinBox4, 4, 6, 1, 1);

        frequencySpinBox8 = new QDoubleSpinBox(gridLayoutWidget_6);
        frequencySpinBox8->setObjectName(QStringLiteral("frequencySpinBox8"));

        gridLayout_5->addWidget(frequencySpinBox8, 10, 6, 1, 1);

        frequencySpinBox7 = new QDoubleSpinBox(gridLayoutWidget_6);
        frequencySpinBox7->setObjectName(QStringLiteral("frequencySpinBox7"));

        gridLayout_5->addWidget(frequencySpinBox7, 9, 6, 1, 1);

        label_26 = new QLabel(gridLayoutWidget_6);
        label_26->setObjectName(QStringLiteral("label_26"));

        gridLayout_5->addWidget(label_26, 1, 7, 1, 1);

        label_24 = new QLabel(gridLayoutWidget_6);
        label_24->setObjectName(QStringLiteral("label_24"));

        gridLayout_5->addWidget(label_24, 2, 7, 1, 1);

        label_25 = new QLabel(gridLayoutWidget_6);
        label_25->setObjectName(QStringLiteral("label_25"));

        gridLayout_5->addWidget(label_25, 4, 7, 1, 1);

        label_27 = new QLabel(gridLayoutWidget_6);
        label_27->setObjectName(QStringLiteral("label_27"));

        gridLayout_5->addWidget(label_27, 5, 7, 1, 1);

        label_28 = new QLabel(gridLayoutWidget_6);
        label_28->setObjectName(QStringLiteral("label_28"));

        gridLayout_5->addWidget(label_28, 9, 7, 1, 1);

        label_30 = new QLabel(gridLayoutWidget_6);
        label_30->setObjectName(QStringLiteral("label_30"));

        gridLayout_5->addWidget(label_30, 7, 7, 1, 1);

        label_31 = new QLabel(gridLayoutWidget_6);
        label_31->setObjectName(QStringLiteral("label_31"));

        gridLayout_5->addWidget(label_31, 10, 7, 1, 1);

        label_29 = new QLabel(gridLayoutWidget_6);
        label_29->setObjectName(QStringLiteral("label_29"));

        gridLayout_5->addWidget(label_29, 11, 7, 1, 1);

        label_32 = new QLabel(gridLayoutWidget_6);
        label_32->setObjectName(QStringLiteral("label_32"));

        gridLayout_5->addWidget(label_32, 12, 3, 1, 1);

        PmDebug_doubleSpinBox = new QDoubleSpinBox(gridLayoutWidget_6);
        PmDebug_doubleSpinBox->setObjectName(QStringLiteral("PmDebug_doubleSpinBox"));
        PmDebug_doubleSpinBox->setMinimum(0);
        PmDebug_doubleSpinBox->setMaximum(150);
        PmDebug_doubleSpinBox->setSingleStep(0.1);
        PmDebug_doubleSpinBox->setValue(1);

        gridLayout_5->addWidget(PmDebug_doubleSpinBox, 12, 2, 1, 1);

        PermMag_progressBar = new QProgressBar(gridLayoutWidget_6);
        PermMag_progressBar->setObjectName(QStringLiteral("PermMag_progressBar"));
        PermMag_progressBar->setMaximum(150);
        PermMag_progressBar->setValue(0);

        gridLayout_5->addWidget(PermMag_progressBar, 12, 1, 1, 1);

        AC10_SpinBox = new QDoubleSpinBox(gridLayoutWidget_6);
        AC10_SpinBox->setObjectName(QStringLiteral("AC10_SpinBox"));
        AC10_SpinBox->setMinimum(0);
        AC10_SpinBox->setMaximum(50);
        AC10_SpinBox->setSingleStep(0.1);

        gridLayout_5->addWidget(AC10_SpinBox, 12, 4, 1, 1);

        label_33 = new QLabel(gridLayoutWidget_6);
        label_33->setObjectName(QStringLiteral("label_33"));

        gridLayout_5->addWidget(label_33, 12, 5, 1, 1);

        frequencySpinBox10 = new QDoubleSpinBox(gridLayoutWidget_6);
        frequencySpinBox10->setObjectName(QStringLiteral("frequencySpinBox10"));

        gridLayout_5->addWidget(frequencySpinBox10, 12, 6, 1, 1);

        label_34 = new QLabel(gridLayoutWidget_6);
        label_34->setObjectName(QStringLiteral("label_34"));

        gridLayout_5->addWidget(label_34, 12, 7, 1, 1);

        Contro_Button = new QPushButton(Debug);
        Contro_Button->setObjectName(QStringLiteral("Contro_Button"));
        Contro_Button->setGeometry(QRect(670, 110, 271, 151));
        tabWidget->addTab(Debug, QString());

        gridLayout->addWidget(tabWidget, 0, 0, 1, 1);

        CtrlWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(CtrlWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1850, 19));
        CtrlWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(CtrlWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        CtrlWindow->setStatusBar(statusBar);
        mainToolBar = new QToolBar(CtrlWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        CtrlWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        QWidget::setTabOrder(agentList, imagingModeList);
        QWidget::setTabOrder(imagingModeList, controlAlgorithmList);
        QWidget::setTabOrder(controlAlgorithmList, forceCurrentList);
        QWidget::setTabOrder(forceCurrentList, paramSlider1);
        QWidget::setTabOrder(paramSlider1, paramSpin1);
        QWidget::setTabOrder(paramSpin1, paramSlider2);
        QWidget::setTabOrder(paramSlider2, paramSpin2);
        QWidget::setTabOrder(paramSpin2, paramSlider3);
        QWidget::setTabOrder(paramSlider3, paramSpin3);
        QWidget::setTabOrder(paramSpin3, paramSlider4);
        QWidget::setTabOrder(paramSlider4, paramSpin4);
        QWidget::setTabOrder(paramSpin4, paramSlider5);
        QWidget::setTabOrder(paramSlider5, paramSpin5);
        QWidget::setTabOrder(paramSpin5, paramSlider6);
        QWidget::setTabOrder(paramSlider6, paramSpin6);
        QWidget::setTabOrder(paramSpin6, paramSlider7);
        QWidget::setTabOrder(paramSlider7, paramSpin7);
        QWidget::setTabOrder(paramSpin7, paramSlider8);
        QWidget::setTabOrder(paramSlider8, paramSpin8);
        QWidget::setTabOrder(paramSpin8, paramSlider9);
        QWidget::setTabOrder(paramSlider9, paramSpin9);
        QWidget::setTabOrder(paramSpin9, textBox);
        QWidget::setTabOrder(textBox, zoomDoubleSpinBox);
        QWidget::setTabOrder(zoomDoubleSpinBox, startButton);
        QWidget::setTabOrder(startButton, stopButton_2);
        QWidget::setTabOrder(stopButton_2, autofocus0_checkBox);
        QWidget::setTabOrder(autofocus0_checkBox, autofocus1_checkBox);
        QWidget::setTabOrder(autofocus1_checkBox, camera0Speed);
        QWidget::setTabOrder(camera0Speed, camera1Speed);
        QWidget::setTabOrder(camera1Speed, ctrl_en_checkBox);
        QWidget::setTabOrder(ctrl_en_checkBox, record_checkBox);
        QWidget::setTabOrder(record_checkBox, recordRaw_checkBox);
        QWidget::setTabOrder(recordRaw_checkBox, stopButton);
        QWidget::setTabOrder(stopButton, ctrl_textBrowser);
        QWidget::setTabOrder(ctrl_textBrowser, Cur1Debug_doubleSpinBox);
        QWidget::setTabOrder(Cur1Debug_doubleSpinBox, AC1_SpinBox);
        QWidget::setTabOrder(AC1_SpinBox, frequencySpinBox1);
        QWidget::setTabOrder(frequencySpinBox1, Cur2Debug_doubleSpinBox);
        QWidget::setTabOrder(Cur2Debug_doubleSpinBox, AC2_SpinBox);
        QWidget::setTabOrder(AC2_SpinBox, frequencySpinBox2);
        QWidget::setTabOrder(frequencySpinBox2, Cur3Debug_doubleSpinBox);
        QWidget::setTabOrder(Cur3Debug_doubleSpinBox, AC3_SpinBox);
        QWidget::setTabOrder(AC3_SpinBox, frequencySpinBox3);
        QWidget::setTabOrder(frequencySpinBox3, Cur4Debug_doubleSpinBox);
        QWidget::setTabOrder(Cur4Debug_doubleSpinBox, AC4_SpinBox);
        QWidget::setTabOrder(AC4_SpinBox, frequencySpinBox4);
        QWidget::setTabOrder(frequencySpinBox4, Cur5Debug_doubleSpinBox);
        QWidget::setTabOrder(Cur5Debug_doubleSpinBox, AC5_SpinBox);
        QWidget::setTabOrder(AC5_SpinBox, frequencySpinBox5);
        QWidget::setTabOrder(frequencySpinBox5, Cur6Debug_doubleSpinBox);
        QWidget::setTabOrder(Cur6Debug_doubleSpinBox, AC6_SpinBox);
        QWidget::setTabOrder(AC6_SpinBox, frequencySpinBox6);
        QWidget::setTabOrder(frequencySpinBox6, Cur7Debug_doubleSpinBox);
        QWidget::setTabOrder(Cur7Debug_doubleSpinBox, AC7_SpinBox);
        QWidget::setTabOrder(AC7_SpinBox, frequencySpinBox7);
        QWidget::setTabOrder(frequencySpinBox7, Cur8Debug_doubleSpinBox);
        QWidget::setTabOrder(Cur8Debug_doubleSpinBox, AC8_SpinBox);
        QWidget::setTabOrder(AC8_SpinBox, frequencySpinBox8);
        QWidget::setTabOrder(frequencySpinBox8, Cur9Debug_doubleSpinBox);
        QWidget::setTabOrder(Cur9Debug_doubleSpinBox, AC9_SpinBox);
        QWidget::setTabOrder(AC9_SpinBox, frequencySpinBox9);
        QWidget::setTabOrder(frequencySpinBox9, deb_en_checkBox);
        QWidget::setTabOrder(deb_en_checkBox, record_checkBox_2);
        QWidget::setTabOrder(record_checkBox_2, recordRaw_checkBox_2);

        retranslateUi(CtrlWindow);
        QObject::connect(coil_progressBar_1, SIGNAL(valueChanged(int)), coil_progressBar_1deb, SLOT(setValue(int)));
        QObject::connect(coil_progressBar_2, SIGNAL(valueChanged(int)), coil_progressBar_2deb, SLOT(setValue(int)));
        QObject::connect(coil_progressBar_3, SIGNAL(valueChanged(int)), coil_progressBar_3deb, SLOT(setValue(int)));
        QObject::connect(coil_progressBar_4, SIGNAL(valueChanged(int)), coil_progressBar_4deb, SLOT(setValue(int)));
        QObject::connect(coil_progressBar_5, SIGNAL(valueChanged(int)), coil_progressBar_5deb, SLOT(setValue(int)));
        QObject::connect(coil_progressBar_6, SIGNAL(valueChanged(int)), coil_progressBar_6deb, SLOT(setValue(int)));
        QObject::connect(coil_progressBar_7, SIGNAL(valueChanged(int)), coil_progressBar_7deb, SLOT(setValue(int)));
        QObject::connect(coil_progressBar_8, SIGNAL(valueChanged(int)), coil_progressBar_8deb, SLOT(setValue(int)));
        QObject::connect(coil_progressBar_9, SIGNAL(valueChanged(int)), coil_progressBar_9deb, SLOT(setValue(int)));

        tabWidget->setCurrentIndex(2);


        QMetaObject::connectSlotsByName(CtrlWindow);
    } // setupUi

    void retranslateUi(QMainWindow *CtrlWindow)
    {
        CtrlWindow->setWindowTitle(QApplication::translate("CtrlWindow", "MainWindow", Q_NULLPTR));
        startButton->setText(QApplication::translate("CtrlWindow", "Start", Q_NULLPTR));
        stopButton_2->setText(QApplication::translate("CtrlWindow", "Stop", Q_NULLPTR));
        imagingModeLabel->setText(QApplication::translate("CtrlWindow", "Imaging Mode", Q_NULLPTR));

        const bool __sortingEnabled = controlAlgorithmList->isSortingEnabled();
        controlAlgorithmList->setSortingEnabled(false);
        QListWidgetItem *___qlistwidgetitem = controlAlgorithmList->item(0);
        ___qlistwidgetitem->setText(QApplication::translate("CtrlWindow", "PID3D", Q_NULLPTR));
        QListWidgetItem *___qlistwidgetitem1 = controlAlgorithmList->item(1);
        ___qlistwidgetitem1->setText(QApplication::translate("CtrlWindow", "PID5D", Q_NULLPTR));
        QListWidgetItem *___qlistwidgetitem2 = controlAlgorithmList->item(2);
        ___qlistwidgetitem2->setText(QApplication::translate("CtrlWindow", "Rotating Field", Q_NULLPTR));
        controlAlgorithmList->setSortingEnabled(__sortingEnabled);


        const bool __sortingEnabled1 = imagingModeList->isSortingEnabled();
        imagingModeList->setSortingEnabled(false);
        QListWidgetItem *___qlistwidgetitem3 = imagingModeList->item(0);
        ___qlistwidgetitem3->setText(QApplication::translate("CtrlWindow", "2 Cameras", Q_NULLPTR));
        QListWidgetItem *___qlistwidgetitem4 = imagingModeList->item(1);
        ___qlistwidgetitem4->setText(QApplication::translate("CtrlWindow", "Camera 0 & Ultrasound", Q_NULLPTR));
        QListWidgetItem *___qlistwidgetitem5 = imagingModeList->item(2);
        ___qlistwidgetitem5->setText(QApplication::translate("CtrlWindow", "Camera 1 & Ultrasound", Q_NULLPTR));
        QListWidgetItem *___qlistwidgetitem6 = imagingModeList->item(3);
        ___qlistwidgetitem6->setText(QApplication::translate("CtrlWindow", "Ultrasound Only (2D)", Q_NULLPTR));
        imagingModeList->setSortingEnabled(__sortingEnabled1);

        controlAlgorithmLabel->setText(QApplication::translate("CtrlWindow", "Control Algorithm", Q_NULLPTR));
        agentLabel->setText(QApplication::translate("CtrlWindow", "Agent", Q_NULLPTR));

        const bool __sortingEnabled2 = agentList->isSortingEnabled();
        agentList->setSortingEnabled(false);
        QListWidgetItem *___qlistwidgetitem7 = agentList->item(0);
        ___qlistwidgetitem7->setText(QApplication::translate("CtrlWindow", "Microparticle", Q_NULLPTR));
        QListWidgetItem *___qlistwidgetitem8 = agentList->item(1);
        ___qlistwidgetitem8->setText(QApplication::translate("CtrlWindow", "Hydrogel Gripper", Q_NULLPTR));
        QListWidgetItem *___qlistwidgetitem9 = agentList->item(2);
        ___qlistwidgetitem9->setText(QApplication::translate("CtrlWindow", "Microjet", Q_NULLPTR));
        QListWidgetItem *___qlistwidgetitem10 = agentList->item(3);
        ___qlistwidgetitem10->setText(QApplication::translate("CtrlWindow", "Metallic Gripper", Q_NULLPTR));
        QListWidgetItem *___qlistwidgetitem11 = agentList->item(4);
        ___qlistwidgetitem11->setText(QApplication::translate("CtrlWindow", "1mm Spherical PM", Q_NULLPTR));
        QListWidgetItem *___qlistwidgetitem12 = agentList->item(5);
        ___qlistwidgetitem12->setText(QApplication::translate("CtrlWindow", "CircClip", Q_NULLPTR));
        QListWidgetItem *___qlistwidgetitem13 = agentList->item(6);
        ___qlistwidgetitem13->setText(QApplication::translate("CtrlWindow", "Microsphere", Q_NULLPTR));
        agentList->setSortingEnabled(__sortingEnabled2);


        const bool __sortingEnabled3 = forceCurrentList->isSortingEnabled();
        forceCurrentList->setSortingEnabled(false);
        QListWidgetItem *___qlistwidgetitem14 = forceCurrentList->item(0);
        ___qlistwidgetitem14->setText(QApplication::translate("CtrlWindow", "Vector Map", Q_NULLPTR));
        QListWidgetItem *___qlistwidgetitem15 = forceCurrentList->item(1);
        ___qlistwidgetitem15->setText(QApplication::translate("CtrlWindow", "Bspline Map", Q_NULLPTR));
        QListWidgetItem *___qlistwidgetitem16 = forceCurrentList->item(2);
        ___qlistwidgetitem16->setText(QApplication::translate("CtrlWindow", "NL Optimization", Q_NULLPTR));
        QListWidgetItem *___qlistwidgetitem17 = forceCurrentList->item(3);
        ___qlistwidgetitem17->setText(QApplication::translate("CtrlWindow", "Linear Map", Q_NULLPTR));
        QListWidgetItem *___qlistwidgetitem18 = forceCurrentList->item(4);
        ___qlistwidgetitem18->setText(QApplication::translate("CtrlWindow", "Field Only", Q_NULLPTR));
        forceCurrentList->setSortingEnabled(__sortingEnabled3);

        controlAlgorithmLabel_2->setText(QApplication::translate("CtrlWindow", "Force Current Map", Q_NULLPTR));
        parameterLabel4->setText(QApplication::translate("CtrlWindow", "Parameter4", Q_NULLPTR));
        parameterLabel2->setText(QApplication::translate("CtrlWindow", "Parameter2", Q_NULLPTR));
        parameterLabel9->setText(QApplication::translate("CtrlWindow", "Parameter9", Q_NULLPTR));
        parameterLabel8->setText(QApplication::translate("CtrlWindow", "Parameter8", Q_NULLPTR));
        parameterLabel5->setText(QApplication::translate("CtrlWindow", "Parameter5", Q_NULLPTR));
        parameterLabel3->setText(QApplication::translate("CtrlWindow", "Parameter3", Q_NULLPTR));
        parameterLabel6->setText(QApplication::translate("CtrlWindow", "Parameter6", Q_NULLPTR));
        parameterLabel1->setText(QApplication::translate("CtrlWindow", "Parameter1", Q_NULLPTR));
        parameterLabel7->setText(QApplication::translate("CtrlWindow", "Parameter7", Q_NULLPTR));
        label_44->setText(QApplication::translate("CtrlWindow", "Zoom:", Q_NULLPTR));
        label->setText(QApplication::translate("CtrlWindow", "Final Lens:", Q_NULLPTR));
        finalLens_comboBox->clear();
        finalLens_comboBox->insertItems(0, QStringList()
         << QApplication::translate("CtrlWindow", "1x", Q_NULLPTR)
         << QApplication::translate("CtrlWindow", "4x", Q_NULLPTR)
        );
        tabWidget->setTabText(tabWidget->indexOf(initTab), QApplication::translate("CtrlWindow", "Initialization", Q_NULLPTR));
        stopButton->setText(QApplication::translate("CtrlWindow", "Stop", Q_NULLPTR));
        coil_progressBar_5->setFormat(QApplication::translate("CtrlWindow", "Cur 5: %v mA", Q_NULLPTR));
        coil_progressBar_9->setFormat(QApplication::translate("CtrlWindow", "Cur 9: %v mA", Q_NULLPTR));
        coil_progressBar_1->setFormat(QApplication::translate("CtrlWindow", "Cur 1: %v mA", Q_NULLPTR));
        coil_progressBar_3->setFormat(QApplication::translate("CtrlWindow", "Cur 3: %v mA", Q_NULLPTR));
        coil_progressBar_4->setFormat(QApplication::translate("CtrlWindow", "Cur 4: %v mA", Q_NULLPTR));
        coil_progressBar_7->setFormat(QApplication::translate("CtrlWindow", "Cur 7: %v mA", Q_NULLPTR));
        coil_progressBar_8->setFormat(QApplication::translate("CtrlWindow", "Cur 8: %v mA", Q_NULLPTR));
        coil_progressBar_6->setFormat(QApplication::translate("CtrlWindow", "Cur 6: %v mA", Q_NULLPTR));
        coil_progressBar_2->setFormat(QApplication::translate("CtrlWindow", "Cur 2: %v mA", Q_NULLPTR));
        ctrl_en_checkBox->setText(QApplication::translate("CtrlWindow", "Ctrl Enable", Q_NULLPTR));
        record_checkBox->setText(QApplication::translate("CtrlWindow", "Record", Q_NULLPTR));
        recordRaw_checkBox->setText(QApplication::translate("CtrlWindow", "Record Raw", Q_NULLPTR));
        gravityEn_checkBox->setText(QApplication::translate("CtrlWindow", "Gravity En", Q_NULLPTR));
        omega_checkBox->setText(QApplication::translate("CtrlWindow", "Omega", Q_NULLPTR));
        label_2->setText(QApplication::translate("CtrlWindow", "Camera 0", Q_NULLPTR));
        label_39->setText(QApplication::translate("CtrlWindow", "Width: ", Q_NULLPTR));
        label_37->setText(QApplication::translate("CtrlWindow", "Top Left y:", Q_NULLPTR));
        label_35->setText(QApplication::translate("CtrlWindow", "Ultrasound ROI", Q_NULLPTR));
        label_36->setText(QApplication::translate("CtrlWindow", "TopLeft x:", Q_NULLPTR));
        label_38->setText(QApplication::translate("CtrlWindow", "Height:", Q_NULLPTR));
        PMenable->setText(QApplication::translate("CtrlWindow", "PM enable", Q_NULLPTR));
        PermMag_progressBar_2->setFormat(QApplication::translate("CtrlWindow", "Perm Magnet: %v mm", Q_NULLPTR));
        Pm_doubleSpinBox->setSuffix(QString());
        pushButton->setText(QApplication::translate("CtrlWindow", "Tracker Settings", Q_NULLPTR));
        label_41->setText(QApplication::translate("CtrlWindow", "Tau (Distance based)", Q_NULLPTR));
        label_40->setText(QApplication::translate("CtrlWindow", "Ref. Filter", Q_NULLPTR));
        label_42->setText(QApplication::translate("CtrlWindow", "Upper Bound", Q_NULLPTR));
        label_43->setText(QApplication::translate("CtrlWindow", "Lower Bound", Q_NULLPTR));
        grid_Button->setText(QApplication::translate("CtrlWindow", "Grid On/Off", Q_NULLPTR));
        cameraSet_pushButton->setText(QApplication::translate("CtrlWindow", "Camera Settings", Q_NULLPTR));
        saveImage_button->setText(QApplication::translate("CtrlWindow", "Save Camera Images", Q_NULLPTR));
        forceCur_button->setText(QApplication::translate("CtrlWindow", "Force2Current Settings", Q_NULLPTR));
        resetElmo_pushButton->setText(QApplication::translate("CtrlWindow", "Reset Coils Elmos", Q_NULLPTR));
        ctrlSettings_button->setText(QApplication::translate("CtrlWindow", "Ctrl Settings", Q_NULLPTR));
        label_3->setText(QApplication::translate("CtrlWindow", "Camera 1", Q_NULLPTR));
        autofocus0_checkBox->setText(QApplication::translate("CtrlWindow", "Autofocus Camera 0", Q_NULLPTR));
        autofocus1_checkBox->setText(QApplication::translate("CtrlWindow", "Autofocus Camera 1", Q_NULLPTR));
        label_4->setText(QApplication::translate("CtrlWindow", "Running Info", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(ctrlTab), QApplication::translate("CtrlWindow", "Control", Q_NULLPTR));
        label_18->setText(QApplication::translate("CtrlWindow", "@", Q_NULLPTR));
        label_23->setText(QApplication::translate("CtrlWindow", "Hz", Q_NULLPTR));
        coil_progressBar_4deb->setFormat(QApplication::translate("CtrlWindow", "Cur 4: %v mA", Q_NULLPTR));
        coil_progressBar_9deb->setFormat(QApplication::translate("CtrlWindow", "Cur 9: %v mA", Q_NULLPTR));
        coil_progressBar_1deb->setFormat(QApplication::translate("CtrlWindow", "Cur 1: %v mA", Q_NULLPTR));
        coil_progressBar_7deb->setFormat(QApplication::translate("CtrlWindow", "Cur 7: %v mA", Q_NULLPTR));
        coil_progressBar_8deb->setFormat(QApplication::translate("CtrlWindow", "Cur 8: %v mA", Q_NULLPTR));
        coil_progressBar_2deb->setFormat(QApplication::translate("CtrlWindow", "Cur 2: %v mA", Q_NULLPTR));
        coil_progressBar_5deb->setFormat(QApplication::translate("CtrlWindow", "Cur 5: %v mA", Q_NULLPTR));
        record_checkBox_2->setText(QApplication::translate("CtrlWindow", "Record", Q_NULLPTR));
        recordRaw_checkBox_2->setText(QApplication::translate("CtrlWindow", "Record Raw", Q_NULLPTR));
        coil_progressBar_6deb->setFormat(QApplication::translate("CtrlWindow", "Cur 6: %v mA", Q_NULLPTR));
        coil_progressBar_3deb->setFormat(QApplication::translate("CtrlWindow", "Cur 3: %v mA", Q_NULLPTR));
        deb_en_checkBox->setText(QApplication::translate("CtrlWindow", "Debug Enable", Q_NULLPTR));
        label_5->setText(QApplication::translate("CtrlWindow", "+", Q_NULLPTR));
        label_7->setText(QApplication::translate("CtrlWindow", "+", Q_NULLPTR));
        label_6->setText(QApplication::translate("CtrlWindow", "+", Q_NULLPTR));
        label_10->setText(QApplication::translate("CtrlWindow", "+", Q_NULLPTR));
        label_13->setText(QApplication::translate("CtrlWindow", "+", Q_NULLPTR));
        label_8->setText(QApplication::translate("CtrlWindow", "+", Q_NULLPTR));
        label_9->setText(QApplication::translate("CtrlWindow", "+", Q_NULLPTR));
        label_11->setText(QApplication::translate("CtrlWindow", "+", Q_NULLPTR));
        label_12->setText(QApplication::translate("CtrlWindow", "+", Q_NULLPTR));
        label_14->setText(QApplication::translate("CtrlWindow", "@", Q_NULLPTR));
        label_15->setText(QApplication::translate("CtrlWindow", "@", Q_NULLPTR));
        label_16->setText(QApplication::translate("CtrlWindow", "@", Q_NULLPTR));
        label_17->setText(QApplication::translate("CtrlWindow", "@", Q_NULLPTR));
        label_19->setText(QApplication::translate("CtrlWindow", "@", Q_NULLPTR));
        label_22->setText(QApplication::translate("CtrlWindow", "@", Q_NULLPTR));
        label_20->setText(QApplication::translate("CtrlWindow", "@", Q_NULLPTR));
        label_21->setText(QApplication::translate("CtrlWindow", "@", Q_NULLPTR));
        label_26->setText(QApplication::translate("CtrlWindow", "Hz", Q_NULLPTR));
        label_24->setText(QApplication::translate("CtrlWindow", "Hz", Q_NULLPTR));
        label_25->setText(QApplication::translate("CtrlWindow", "Hz", Q_NULLPTR));
        label_27->setText(QApplication::translate("CtrlWindow", "Hz", Q_NULLPTR));
        label_28->setText(QApplication::translate("CtrlWindow", "Hz", Q_NULLPTR));
        label_30->setText(QApplication::translate("CtrlWindow", "Hz", Q_NULLPTR));
        label_31->setText(QApplication::translate("CtrlWindow", "Hz", Q_NULLPTR));
        label_29->setText(QApplication::translate("CtrlWindow", "Hz", Q_NULLPTR));
        label_32->setText(QApplication::translate("CtrlWindow", "+", Q_NULLPTR));
        PermMag_progressBar->setFormat(QApplication::translate("CtrlWindow", "Perm Magnet: %v mm", Q_NULLPTR));
        label_33->setText(QApplication::translate("CtrlWindow", "@", Q_NULLPTR));
        label_34->setText(QApplication::translate("CtrlWindow", "Hz", Q_NULLPTR));
        Contro_Button->setText(QApplication::translate("CtrlWindow", "Start control stuff new mode", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(Debug), QApplication::translate("CtrlWindow", "Debug", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class CtrlWindow: public Ui_CtrlWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CTRLWINDOW_H
