/********************************************************************************
** Form generated from reading UI file 'roisettings.ui'
**
** Created by: Qt User Interface Compiler version 5.9.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ROISETTINGS_H
#define UI_ROISETTINGS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QWidget>
#include "cqtopencvviewergl.h"

QT_BEGIN_NAMESPACE

class Ui_ROIsettings
{
public:
    QWidget *gridLayoutWidget_9;
    QGridLayout *gridLayout_8;
    QSlider *maxSat_Slider;
    QSlider *minHue_Slider;
    QSlider *maxHue_Slider;
    QLabel *label_3;
    QLabel *label_5;
    QLabel *label_42;
    QLabel *label_7;
    QSlider *sizeROI_Slider_2;
    QLabel *label_10;
    QLabel *label_40;
    QHBoxLayout *horizontalLayout;
    QPushButton *bs0_button;
    QCheckBox *bs0check;
    QCheckBox *fixed_checkBox;
    QSlider *blur_Slider;
    QLabel *label_41;
    QLabel *label;
    QSlider *blur_Slider_2;
    QSlider *minHue_Slider_2;
    QSlider *minSat_Slider_2;
    QSlider *minSat_Slider;
    QSlider *maxSat_Slider_2;
    QSlider *minVal_Slider;
    QSlider *maxHue_Slider_2;
    QLabel *label_4;
    QSlider *maxVal_Slider;
    QLabel *label_43;
    QLabel *label_9;
    QSlider *minVal_Slider_2;
    QSlider *sizeROI_Slider;
    QLabel *label_2;
    QLabel *label_8;
    QLabel *label_6;
    QSlider *agentSize_Slider;
    QSlider *maxVal_Slider_2;
    QSlider *agentSize_Slider_2;
    QHBoxLayout *horizontalLayout_4;
    QPushButton *bs1_button;
    QCheckBox *bs1check;
    QCheckBox *fixed_checkBox_2;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QFrame *frame;
    CQtOpenCVViewerGl *roi0Image;
    QFrame *frame_2;
    CQtOpenCVViewerGl *roi1Image;
    QPushButton *pushButton;
    QWidget *gridLayoutWidget_2;
    QGridLayout *gridLayout_2;
    QFrame *frame_3;
    CQtOpenCVViewerGl *bs0Image;
    QFrame *frame_4;
    CQtOpenCVViewerGl *bs1Image;

    void setupUi(QDialog *ROIsettings)
    {
        if (ROIsettings->objectName().isEmpty())
            ROIsettings->setObjectName(QStringLiteral("ROIsettings"));
        ROIsettings->resize(965, 1148);
        gridLayoutWidget_9 = new QWidget(ROIsettings);
        gridLayoutWidget_9->setObjectName(QStringLiteral("gridLayoutWidget_9"));
        gridLayoutWidget_9->setGeometry(QRect(70, 10, 711, 241));
        gridLayout_8 = new QGridLayout(gridLayoutWidget_9);
        gridLayout_8->setObjectName(QStringLiteral("gridLayout_8"));
        gridLayout_8->setContentsMargins(0, 0, 0, 0);
        maxSat_Slider = new QSlider(gridLayoutWidget_9);
        maxSat_Slider->setObjectName(QStringLiteral("maxSat_Slider"));
        maxSat_Slider->setMaximum(255);
        maxSat_Slider->setValue(255);
        maxSat_Slider->setOrientation(Qt::Horizontal);

        gridLayout_8->addWidget(maxSat_Slider, 11, 2, 1, 1);

        minHue_Slider = new QSlider(gridLayoutWidget_9);
        minHue_Slider->setObjectName(QStringLiteral("minHue_Slider"));
        minHue_Slider->setMaximum(255);
        minHue_Slider->setOrientation(Qt::Horizontal);

        gridLayout_8->addWidget(minHue_Slider, 6, 2, 1, 1);

        maxHue_Slider = new QSlider(gridLayoutWidget_9);
        maxHue_Slider->setObjectName(QStringLiteral("maxHue_Slider"));
        maxHue_Slider->setMaximum(255);
        maxHue_Slider->setValue(255);
        maxHue_Slider->setOrientation(Qt::Horizontal);

        gridLayout_8->addWidget(maxHue_Slider, 5, 2, 1, 1);

        label_3 = new QLabel(gridLayoutWidget_9);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout_8->addWidget(label_3, 8, 1, 1, 1);

        label_5 = new QLabel(gridLayoutWidget_9);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout_8->addWidget(label_5, 9, 1, 1, 1);

        label_42 = new QLabel(gridLayoutWidget_9);
        label_42->setObjectName(QStringLiteral("label_42"));

        gridLayout_8->addWidget(label_42, 8, 0, 2, 1);

        label_7 = new QLabel(gridLayoutWidget_9);
        label_7->setObjectName(QStringLiteral("label_7"));

        gridLayout_8->addWidget(label_7, 4, 0, 1, 2);

        sizeROI_Slider_2 = new QSlider(gridLayoutWidget_9);
        sizeROI_Slider_2->setObjectName(QStringLiteral("sizeROI_Slider_2"));
        sizeROI_Slider_2->setMaximum(1200);
        sizeROI_Slider_2->setValue(400);
        sizeROI_Slider_2->setOrientation(Qt::Horizontal);

        gridLayout_8->addWidget(sizeROI_Slider_2, 3, 3, 1, 1);

        label_10 = new QLabel(gridLayoutWidget_9);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setAlignment(Qt::AlignCenter);

        gridLayout_8->addWidget(label_10, 0, 3, 1, 1);

        label_40 = new QLabel(gridLayoutWidget_9);
        label_40->setObjectName(QStringLiteral("label_40"));
        label_40->setFrameShape(QFrame::NoFrame);

        gridLayout_8->addWidget(label_40, 3, 0, 1, 2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        bs0_button = new QPushButton(gridLayoutWidget_9);
        bs0_button->setObjectName(QStringLiteral("bs0_button"));

        horizontalLayout->addWidget(bs0_button);

        bs0check = new QCheckBox(gridLayoutWidget_9);
        bs0check->setObjectName(QStringLiteral("bs0check"));

        horizontalLayout->addWidget(bs0check);

        fixed_checkBox = new QCheckBox(gridLayoutWidget_9);
        fixed_checkBox->setObjectName(QStringLiteral("fixed_checkBox"));

        horizontalLayout->addWidget(fixed_checkBox);


        gridLayout_8->addLayout(horizontalLayout, 14, 1, 1, 2);

        blur_Slider = new QSlider(gridLayoutWidget_9);
        blur_Slider->setObjectName(QStringLiteral("blur_Slider"));
        blur_Slider->setOrientation(Qt::Horizontal);

        gridLayout_8->addWidget(blur_Slider, 13, 1, 1, 2);

        label_41 = new QLabel(gridLayoutWidget_9);
        label_41->setObjectName(QStringLiteral("label_41"));

        gridLayout_8->addWidget(label_41, 5, 0, 2, 1);

        label = new QLabel(gridLayoutWidget_9);
        label->setObjectName(QStringLiteral("label"));

        gridLayout_8->addWidget(label, 5, 1, 1, 1);

        blur_Slider_2 = new QSlider(gridLayoutWidget_9);
        blur_Slider_2->setObjectName(QStringLiteral("blur_Slider_2"));
        blur_Slider_2->setOrientation(Qt::Horizontal);

        gridLayout_8->addWidget(blur_Slider_2, 13, 3, 1, 1);

        minHue_Slider_2 = new QSlider(gridLayoutWidget_9);
        minHue_Slider_2->setObjectName(QStringLiteral("minHue_Slider_2"));
        minHue_Slider_2->setMaximum(255);
        minHue_Slider_2->setOrientation(Qt::Horizontal);

        gridLayout_8->addWidget(minHue_Slider_2, 6, 3, 1, 1);

        minSat_Slider_2 = new QSlider(gridLayoutWidget_9);
        minSat_Slider_2->setObjectName(QStringLiteral("minSat_Slider_2"));
        minSat_Slider_2->setMaximum(255);
        minSat_Slider_2->setValue(0);
        minSat_Slider_2->setOrientation(Qt::Horizontal);

        gridLayout_8->addWidget(minSat_Slider_2, 12, 3, 1, 1);

        minSat_Slider = new QSlider(gridLayoutWidget_9);
        minSat_Slider->setObjectName(QStringLiteral("minSat_Slider"));
        minSat_Slider->setMaximum(255);
        minSat_Slider->setValue(0);
        minSat_Slider->setOrientation(Qt::Horizontal);

        gridLayout_8->addWidget(minSat_Slider, 12, 2, 1, 1);

        maxSat_Slider_2 = new QSlider(gridLayoutWidget_9);
        maxSat_Slider_2->setObjectName(QStringLiteral("maxSat_Slider_2"));
        maxSat_Slider_2->setMaximum(255);
        maxSat_Slider_2->setValue(255);
        maxSat_Slider_2->setOrientation(Qt::Horizontal);

        gridLayout_8->addWidget(maxSat_Slider_2, 11, 3, 1, 1);

        minVal_Slider = new QSlider(gridLayoutWidget_9);
        minVal_Slider->setObjectName(QStringLiteral("minVal_Slider"));
        minVal_Slider->setMaximum(255);
        minVal_Slider->setOrientation(Qt::Horizontal);

        gridLayout_8->addWidget(minVal_Slider, 9, 2, 1, 1);

        maxHue_Slider_2 = new QSlider(gridLayoutWidget_9);
        maxHue_Slider_2->setObjectName(QStringLiteral("maxHue_Slider_2"));
        maxHue_Slider_2->setMaximum(255);
        maxHue_Slider_2->setValue(255);
        maxHue_Slider_2->setOrientation(Qt::Horizontal);

        gridLayout_8->addWidget(maxHue_Slider_2, 5, 3, 1, 1);

        label_4 = new QLabel(gridLayoutWidget_9);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout_8->addWidget(label_4, 11, 1, 1, 1);

        maxVal_Slider = new QSlider(gridLayoutWidget_9);
        maxVal_Slider->setObjectName(QStringLiteral("maxVal_Slider"));
        maxVal_Slider->setMaximum(255);
        maxVal_Slider->setValue(255);
        maxVal_Slider->setOrientation(Qt::Horizontal);

        gridLayout_8->addWidget(maxVal_Slider, 8, 2, 1, 1);

        label_43 = new QLabel(gridLayoutWidget_9);
        label_43->setObjectName(QStringLiteral("label_43"));

        gridLayout_8->addWidget(label_43, 11, 0, 2, 1);

        label_9 = new QLabel(gridLayoutWidget_9);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setAlignment(Qt::AlignCenter);

        gridLayout_8->addWidget(label_9, 0, 2, 1, 1);

        minVal_Slider_2 = new QSlider(gridLayoutWidget_9);
        minVal_Slider_2->setObjectName(QStringLiteral("minVal_Slider_2"));
        minVal_Slider_2->setMaximum(255);
        minVal_Slider_2->setOrientation(Qt::Horizontal);

        gridLayout_8->addWidget(minVal_Slider_2, 9, 3, 1, 1);

        sizeROI_Slider = new QSlider(gridLayoutWidget_9);
        sizeROI_Slider->setObjectName(QStringLiteral("sizeROI_Slider"));
        sizeROI_Slider->setMaximum(1200);
        sizeROI_Slider->setValue(400);
        sizeROI_Slider->setOrientation(Qt::Horizontal);

        gridLayout_8->addWidget(sizeROI_Slider, 3, 2, 1, 1);

        label_2 = new QLabel(gridLayoutWidget_9);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout_8->addWidget(label_2, 6, 1, 1, 1);

        label_8 = new QLabel(gridLayoutWidget_9);
        label_8->setObjectName(QStringLiteral("label_8"));

        gridLayout_8->addWidget(label_8, 13, 0, 1, 1);

        label_6 = new QLabel(gridLayoutWidget_9);
        label_6->setObjectName(QStringLiteral("label_6"));

        gridLayout_8->addWidget(label_6, 12, 1, 1, 1);

        agentSize_Slider = new QSlider(gridLayoutWidget_9);
        agentSize_Slider->setObjectName(QStringLiteral("agentSize_Slider"));
        agentSize_Slider->setMinimum(0);
        agentSize_Slider->setMaximum(1200);
        agentSize_Slider->setValue(250);
        agentSize_Slider->setOrientation(Qt::Horizontal);

        gridLayout_8->addWidget(agentSize_Slider, 4, 2, 1, 1);

        maxVal_Slider_2 = new QSlider(gridLayoutWidget_9);
        maxVal_Slider_2->setObjectName(QStringLiteral("maxVal_Slider_2"));
        maxVal_Slider_2->setMaximum(255);
        maxVal_Slider_2->setValue(255);
        maxVal_Slider_2->setOrientation(Qt::Horizontal);

        gridLayout_8->addWidget(maxVal_Slider_2, 8, 3, 1, 1);

        agentSize_Slider_2 = new QSlider(gridLayoutWidget_9);
        agentSize_Slider_2->setObjectName(QStringLiteral("agentSize_Slider_2"));
        agentSize_Slider_2->setMinimum(0);
        agentSize_Slider_2->setMaximum(1200);
        agentSize_Slider_2->setValue(250);
        agentSize_Slider_2->setOrientation(Qt::Horizontal);

        gridLayout_8->addWidget(agentSize_Slider_2, 4, 3, 1, 1);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        bs1_button = new QPushButton(gridLayoutWidget_9);
        bs1_button->setObjectName(QStringLiteral("bs1_button"));

        horizontalLayout_4->addWidget(bs1_button);

        bs1check = new QCheckBox(gridLayoutWidget_9);
        bs1check->setObjectName(QStringLiteral("bs1check"));

        horizontalLayout_4->addWidget(bs1check);

        fixed_checkBox_2 = new QCheckBox(gridLayoutWidget_9);
        fixed_checkBox_2->setObjectName(QStringLiteral("fixed_checkBox_2"));

        horizontalLayout_4->addWidget(fixed_checkBox_2);


        gridLayout_8->addLayout(horizontalLayout_4, 14, 3, 1, 1);

        gridLayoutWidget = new QWidget(ROIsettings);
        gridLayoutWidget->setObjectName(QStringLiteral("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(70, 260, 821, 431));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        frame = new QFrame(gridLayoutWidget);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        roi0Image = new CQtOpenCVViewerGl(frame);
        roi0Image->setObjectName(QStringLiteral("roi0Image"));
        roi0Image->setEnabled(true);
        roi0Image->setGeometry(QRect(0, 0, 400, 400));

        gridLayout->addWidget(frame, 0, 0, 1, 1);

        frame_2 = new QFrame(gridLayoutWidget);
        frame_2->setObjectName(QStringLiteral("frame_2"));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        roi1Image = new CQtOpenCVViewerGl(frame_2);
        roi1Image->setObjectName(QStringLiteral("roi1Image"));
        roi1Image->setGeometry(QRect(0, 0, 400, 400));

        gridLayout->addWidget(frame_2, 0, 1, 1, 1);

        pushButton = new QPushButton(ROIsettings);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(800, 30, 80, 22));
        gridLayoutWidget_2 = new QWidget(ROIsettings);
        gridLayoutWidget_2->setObjectName(QStringLiteral("gridLayoutWidget_2"));
        gridLayoutWidget_2->setGeometry(QRect(70, 700, 821, 431));
        gridLayout_2 = new QGridLayout(gridLayoutWidget_2);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        frame_3 = new QFrame(gridLayoutWidget_2);
        frame_3->setObjectName(QStringLiteral("frame_3"));
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);
        bs0Image = new CQtOpenCVViewerGl(frame_3);
        bs0Image->setObjectName(QStringLiteral("bs0Image"));
        bs0Image->setEnabled(true);
        bs0Image->setGeometry(QRect(0, 0, 400, 400));

        gridLayout_2->addWidget(frame_3, 0, 0, 1, 1);

        frame_4 = new QFrame(gridLayoutWidget_2);
        frame_4->setObjectName(QStringLiteral("frame_4"));
        frame_4->setFrameShape(QFrame::StyledPanel);
        frame_4->setFrameShadow(QFrame::Raised);
        bs1Image = new CQtOpenCVViewerGl(frame_4);
        bs1Image->setObjectName(QStringLiteral("bs1Image"));
        bs1Image->setGeometry(QRect(0, 0, 400, 400));

        gridLayout_2->addWidget(frame_4, 0, 1, 1, 1);


        retranslateUi(ROIsettings);

        QMetaObject::connectSlotsByName(ROIsettings);
    } // setupUi

    void retranslateUi(QDialog *ROIsettings)
    {
        ROIsettings->setWindowTitle(QApplication::translate("ROIsettings", "Dialog", Q_NULLPTR));
        label_3->setText(QApplication::translate("ROIsettings", "Max", Q_NULLPTR));
        label_5->setText(QApplication::translate("ROIsettings", "Min", Q_NULLPTR));
        label_42->setText(QApplication::translate("ROIsettings", "Value", Q_NULLPTR));
        label_7->setText(QApplication::translate("ROIsettings", "Agent Size", Q_NULLPTR));
        label_10->setText(QApplication::translate("ROIsettings", "Tracker 1", Q_NULLPTR));
        label_40->setText(QApplication::translate("ROIsettings", "ROI size", Q_NULLPTR));
        bs0_button->setText(QApplication::translate("ROIsettings", "BackSub 0", Q_NULLPTR));
        bs0check->setText(QApplication::translate("ROIsettings", "BackSub", Q_NULLPTR));
        fixed_checkBox->setText(QApplication::translate("ROIsettings", "Fixed", Q_NULLPTR));
        label_41->setText(QApplication::translate("ROIsettings", "Hue", Q_NULLPTR));
        label->setText(QApplication::translate("ROIsettings", "Max", Q_NULLPTR));
        label_4->setText(QApplication::translate("ROIsettings", "Max", Q_NULLPTR));
        label_43->setText(QApplication::translate("ROIsettings", "Saturation", Q_NULLPTR));
        label_9->setText(QApplication::translate("ROIsettings", "Tracker 0", Q_NULLPTR));
        label_2->setText(QApplication::translate("ROIsettings", "Min", Q_NULLPTR));
        label_8->setText(QApplication::translate("ROIsettings", "Blur Size", Q_NULLPTR));
        label_6->setText(QApplication::translate("ROIsettings", "Min", Q_NULLPTR));
        bs1_button->setText(QApplication::translate("ROIsettings", "BackSub 1", Q_NULLPTR));
        bs1check->setText(QApplication::translate("ROIsettings", "BackSub", Q_NULLPTR));
        fixed_checkBox_2->setText(QApplication::translate("ROIsettings", "Fixed", Q_NULLPTR));
        pushButton->setText(QApplication::translate("ROIsettings", "Done", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class ROIsettings: public Ui_ROIsettings {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ROISETTINGS_H
