# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/batmag/batmag_ws/src/batmag_soft/src/tracker/ColorTracker/TrackerColor.cpp" "/home/batmag/batmag_ws/build/batmag_soft/CMakeFiles/tracker.dir/src/tracker/ColorTracker/TrackerColor.cpp.o"
  "/home/batmag/batmag_ws/src/batmag_soft/src/tracker/ColorTracker/TrackerColorGeneral.cpp" "/home/batmag/batmag_ws/build/batmag_soft/CMakeFiles/tracker.dir/src/tracker/ColorTracker/TrackerColorGeneral.cpp.o"
  "/home/batmag/batmag_ws/src/batmag_soft/src/tracker/ColorTracker/TrackerColorHydrogelGripper.cpp" "/home/batmag/batmag_ws/build/batmag_soft/CMakeFiles/tracker.dir/src/tracker/ColorTracker/TrackerColorHydrogelGripper.cpp.o"
  "/home/batmag/batmag_ws/src/batmag_soft/src/tracker/ColorTracker/TrackerColorMetalGripper.cpp" "/home/batmag/batmag_ws/build/batmag_soft/CMakeFiles/tracker.dir/src/tracker/ColorTracker/TrackerColorMetalGripper.cpp.o"
  "/home/batmag/batmag_ws/src/batmag_soft/src/tracker/ColorTracker/TrackerColorUS.cpp" "/home/batmag/batmag_ws/build/batmag_soft/CMakeFiles/tracker.dir/src/tracker/ColorTracker/TrackerColorUS.cpp.o"
  "/home/batmag/batmag_ws/src/batmag_soft/src/tracker/ColorTracker/trackercolorcircclip.cpp" "/home/batmag/batmag_ws/build/batmag_soft/CMakeFiles/tracker.dir/src/tracker/ColorTracker/trackercolorcircclip.cpp.o"
  "/home/batmag/batmag_ws/src/batmag_soft/src/tracker/ColorTracker/trackercolorpm1mm.cpp" "/home/batmag/batmag_ws/build/batmag_soft/CMakeFiles/tracker.dir/src/tracker/ColorTracker/trackercolorpm1mm.cpp.o"
  "/home/batmag/batmag_ws/src/batmag_soft/src/tracker/Tracker.cpp" "/home/batmag/batmag_ws/build/batmag_soft/CMakeFiles/tracker.dir/src/tracker/Tracker.cpp.o"
  "/home/batmag/batmag_ws/src/batmag_soft/src/tracker/Triangulation/Triangulation.cpp" "/home/batmag/batmag_ws/build/batmag_soft/CMakeFiles/tracker.dir/src/tracker/Triangulation/Triangulation.cpp.o"
  "/home/batmag/batmag_ws/src/batmag_soft/src/tracker/imageprocessing.cpp" "/home/batmag/batmag_ws/build/batmag_soft/CMakeFiles/tracker.dir/src/tracker/imageprocessing.cpp.o"
  "/home/batmag/batmag_ws/src/batmag_soft/src/tracker/main.cpp" "/home/batmag/batmag_ws/build/batmag_soft/CMakeFiles/tracker.dir/src/tracker/main.cpp.o"
  "/home/batmag/batmag_ws/src/batmag_soft/src/tracker/utilityComputerVisionFunction.cpp" "/home/batmag/batmag_ws/build/batmag_soft/CMakeFiles/tracker.dir/src/tracker/utilityComputerVisionFunction.cpp.o"
  "/home/batmag/batmag_ws/src/batmag_soft/src/typeHeader.cpp" "/home/batmag/batmag_ws/build/batmag_soft/CMakeFiles/tracker.dir/src/typeHeader.cpp.o"
  "/home/batmag/batmag_ws/build/batmag_soft/tracker_automoc.cpp" "/home/batmag/batmag_ws/build/batmag_soft/CMakeFiles/tracker.dir/tracker_automoc.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_NO_DEBUG"
  "QT_WIDGETS_LIB"
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_PACKAGE_NAME=\"batmag_soft\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "."
  "/home/batmag/batmag_ws/src/batmag_soft"
  "/home/batmag/batmag_ws/devel/.private/batmag_soft/include"
  "/usr/include/eigen3"
  "/home/batmag/ExtraLibrary/mosek/8/tools/platform/linux64x86/h"
  "/usr/include/roboptim"
  "/opt/ros/lunar/include/opencv-3.3.1-dev"
  "/opt/ros/lunar/include/opencv-3.3.1-dev/opencv"
  "/home/batmag/batmag_ws/src/batmag_soft/libs/epiphan/include"
  "/home/batmag/batmag_ws/src/batmag_soft/include"
  "/opt/ros/lunar/include"
  "/opt/ros/lunar/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  "/home/batmag/batmag_ws/src/batmag_soft/src/gui"
  "/opt/qt59/include"
  "/opt/qt59/include/QtWidgets"
  "/opt/qt59/include/QtGui"
  "/opt/qt59/include/QtCore"
  "/opt/qt59/./mkspecs/linux-g++"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
