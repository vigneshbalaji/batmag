file(REMOVE_RECURSE
  "controlnode_automoc.cpp"
  "gui_automoc.cpp"
  "forceCurrentMap_automoc.cpp"
  "canInterface_automoc.cpp"
  "ultrasound_automoc.cpp"
  "ptGreyCameras_automoc.cpp"
  "logger_automoc.cpp"
  "tracker_automoc.cpp"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/batmag_soft_genpy.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
