# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/batmag/batmag_ws/build/batmag_soft/ptGreyCameras_automoc.cpp" "/home/batmag/batmag_ws/build/batmag_soft/CMakeFiles/ptGreyCameras.dir/ptGreyCameras_automoc.cpp.o"
  "/home/batmag/batmag_ws/src/batmag_soft/src/ptGreyCameras/camera_param.cpp" "/home/batmag/batmag_ws/build/batmag_soft/CMakeFiles/ptGreyCameras.dir/src/ptGreyCameras/camera_param.cpp.o"
  "/home/batmag/batmag_ws/src/batmag_soft/src/ptGreyCameras/main.cpp" "/home/batmag/batmag_ws/build/batmag_soft/CMakeFiles/ptGreyCameras.dir/src/ptGreyCameras/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_WIDGETS_LIB"
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_PACKAGE_NAME=\"batmag_soft\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "."
  "/home/batmag/batmag_ws/src/batmag_soft"
  "/home/batmag/batmag_ws/devel/.private/batmag_soft/include"
  "/usr/include/eigen3"
  "/home/batmag/ExtraLibrary/mosek/8/tools/platform/linux64x86/h"
  "/usr/include/roboptim"
  "/opt/ros/lunar/include/opencv-3.3.1-dev"
  "/opt/ros/lunar/include/opencv-3.3.1-dev/opencv"
  "/home/batmag/batmag_ws/src/batmag_soft/libs/epiphan/include"
  "/home/batmag/batmag_ws/src/batmag_soft/include"
  "/opt/ros/lunar/include"
  "/opt/ros/lunar/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  "/home/batmag/batmag_ws/src/batmag_soft/src/gui"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
