#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/batmag/batmag_ws/devel/.private/catkin_tools_prebuild:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/opt/ros/lunar/lib:/opt/ros/lunar/lib/x86_64-linux-gnu:/opt/qt59/lib/x86_64-linux-gnu:/opt/qt59/lib:/opt/qt59/lib/x86_64-linux-gnu:/opt/qt59/lib:/opt/qt59/lib/x86_64-linux-gnu:/opt/qt59/lib"
export PATH="/opt/ros/lunar/bin:/opt/qt59/bin:/opt/qt59/bin:/opt/qt59/bin:/home/batmag/bin:/home/batmag/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin"
export PKG_CONFIG_PATH="/opt/ros/lunar/lib/pkgconfig:/opt/ros/lunar/lib/x86_64-linux-gnu/pkgconfig:/opt/qt59/lib/pkgconfig:/opt/qt59/lib/pkgconfig:/opt/qt59/lib/pkgconfig"
export PWD="/home/batmag/batmag_ws/build/catkin_tools_prebuild"
export PYTHONPATH="/opt/ros/lunar/lib/python2.7/dist-packages"
export ROSLISP_PACKAGE_DIRECTORIES="/home/batmag/batmag_ws/devel/.private/catkin_tools_prebuild/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/batmag/batmag_ws/build/catkin_tools_prebuild:$ROS_PACKAGE_PATH"