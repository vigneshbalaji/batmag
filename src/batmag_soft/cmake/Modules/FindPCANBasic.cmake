# - Try to find peak-linux driver
# Once done, this will define
#
#  ZeroMQ_FOUND - system has pcan
#  ZeroMQ_INCLUDE_DIRS - the libpcan include directories

include(LibFindMacros)

IF(UNIX)
# Include dir
    find_path(PCANBasic_INCLUDE_DIR
            NAMES PCANBasic.h pcan.h
            PATHS /usr/include ${PCAN_PKGCONF_INCLUDE_DIRS}
            )
#Library Path
    find_library(PCANBasic_LIBRARY
	    NAMES libpcanbasic.so
	    PATHS /usr/lib /usr/local/lib
	    )
ENDIF()

# Set the include dir variables and the libraries and let libfind_process do the rest.
# NOTE: Singular variables for this library, plural for libraries this this lib depends on.
    set(PCANBasic_PROCESS_INCLUDES PCANBasic_INCLUDE_DIR PCANBasic_INCLUDE_DIRS)
    set(PCANBasic_PROCESS_LIBS PCANBasic_LIBRARY PCANBasic_LIBRARIES)
libfind_process(PCANBasic)
