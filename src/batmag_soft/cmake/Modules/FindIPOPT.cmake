#
# Try to find IPOPT
# Once done this will define
#
# IPOPT_FOUND           - system has IPOPT
# IPOPT_INCLUDE_DIRS    - the IPOPT include directories
# IPOPT_LIBRARIES       - Link these to use IPOPT
#

FIND_PATH(IPOPT_INCLUDE_DIR IPOPT.h fusion.h monty.h
  PATHS /usr/include /usr/include 
    )

SET(SEARCH_PATHS "${IPOPT_INCLUDE_DIR}" "${IPOPT_INCLUDE_DIR}/../bin" "${IPOPT_INCLUDE_DIR}/lib")

#set(IPOPT_LIBRARIES) 
FIND_LIBRARY(IPOPT_LIBRARIES
         NAMES         libfusion64
         HINTS         "${IPOPT_DIR}"
         PATH_SUFFIXES "${IPOPT_TOOLS_SUFFIX}/bin"
         DOC           "IPOPT link library."
         NO_DEFAULT_PATH
     ) #NAMES libfusion64 libIPOPT64  PATHS ${SEARCH_PATHS} NO_DEFAULT_PATH DPATH_SUFFIXES a lib dylib)

if(IPOPT_LIBRARIES AND IPOPT_INCLUDE_DIR)
message(STATUS "Found IPOPT Link targets: ${IPOPT_LIBRARIES}")
set(IPOPT_FOUND TRUE)
endif(IPOPT_LIBRARIES AND IPOPT_INCLUDE_DIR)

IF (IPOPT_FOUND)
   message(STATUS "Found IPOPT dir: ${IPOPT_INCLUDE_DIR}")
   SET(IPOPT_INCLUDE_DIRS ${IPOPT_INCLUDE_DIR} )
   SET(IPOPT_LIBRARIES ${IPOPT_LIBRARIES} )

ELSE (IPOPT_FOUND)
    #add_definitions(-DIGL_NO_IPOPT)
    #message(WARNING "could NOT find IPOPT")
ENDIF (IPOPT_FOUND)
