#
# Try to find FUSION64
# Once done this will define
#
# FUSION64_FOUND           - system has FUSION64
# FUSION64_INCLUDE_DIRS    - the FUSION64 include directories
# FUSION64_LIBRARIES       - Link these to use FUSION64
#

FIND_PATH(FUSION64_INCLUDE_DIR mosek.h fusion.h monty.h
  PATHS /home/batmag/ExtraLibrary/mosek/8/tools/platform/linux64x86/h
    )

SET(SEARCH_PATHS "${FUSION64_INCLUDE_DIR}" "${FUSION64_INCLUDE_DIR}/../bin" "${FUSION64_INCLUDE_DIR}/lib")

#set(FUSION64_LIBRARIES) 
FIND_LIBRARY(FUSION64_LIBRARIES
         NAMES         libfusion64.so libmosek64.so
         PATHS	       ${SEARCH_PATHS}
         DOC           "FUSION64 link library."
         NO_DEFAULT_PATH
	 DPATH_SUFFIXES a lib dylib
     ) #NAMES libfusion64 libmosek64  PATHS ${SEARCH_PATHS} NO_DEFAULT_PATH DPATH_SUFFIXES a lib dylib)

if(FUSION64_LIBRARIES AND FUSION64_INCLUDE_DIR)
message(STATUS "Found fusion64 Link targets: ${FUSION64_LIBRARIES}")
set(FUSION64_FOUND TRUE)
endif(FUSION64_LIBRARIES AND FUSION64_INCLUDE_DIR)

IF (FUSION64_FOUND)
   message(STATUS "Found FUSION64 dir: ${FUSION64_INCLUDE_DIR}")
   SET(FUSION64_INCLUDE_DIRS ${FUSION64_INCLUDE_DIR} )
   SET(FUSION64_LIBRARIES ${FUSION64_LIBRARIES} )

ELSE (FUSION64_FOUND)
    #add_definitions(-DIGL_NO_FUSION64)
    #message(WARNING "could NOT find FUSION64")
ENDIF (FUSION64_FOUND)
