#
# Try to find ROBOPTIM
# Once done this will define
#
# ROBOPTIM_FOUND           - system has ROBOPTIM
# ROBOPTIM_INCLUDE_DIRS    - the ROBOPTIM include directories
# ROBOPTIM_LIBRARIES       - Link these to use ROBOPTIM
#

FIND_PATH(ROBOPTIM_INCLUDE_DIR core.hh ltdl.h
  PATHS /usr/include/roboptim /usr/include/roboptim/core /usr/include/ /usr/include/libltdl
    )


#set(ROBOPTIM_LIBRARIES) 
FIND_LIBRARY(ROBOPTIM_LIBRARIES
         NAMES         libroboptim-core.so libltdl.so liblapack.so
         PATHS	       /usr/lib /usr/local/lib /usr/lib/x86_64-linux-gnu /usr/lib/x86_64-linux-gnu/roboptim-core
     ) #NAMES libROBOPTIM libmosek64  PATHS ${SEARCH_PATHS} NO_DEFAULT_PATH DPATH_SUFFIXES a lib dylib)

# Dummy plug-in.
FIND_LIBRARY(ROBOPTIM_DUMMY_PLUGIN_LIB
         NAMES  roboptim-core-plugin-dummy.so 
         PATH /usr/lib/x86_64-linux-gnu/roboptim-core
         )

# IPOPT plug-in.
FIND_LIBRARY(ROBOPTIM_IPOPT_PLUGIN_LIB
         NAMES   roboptim-core-plugin-ipopt.so
         PATH /usr/lib/x86_64-linux-gnu/roboptim-core
         )


FIND_LIBRARY(ROBOPTIM_IPOPT_PLUGIN_TD_LIB
         NAMES  roboptim-core-plugin-ipopt-td.so
         PATH /usr/lib/x86_64-linux-gnu/roboptim-core
         )

FIND_LIBRARY(ROBOPTIM_IPOPT_SPARSE_PLUGIN_LIB
         NAMES  roboptim-core-plugin-ipopt-sparse.so
         PATH /usr/lib/x86_64-linux-gnu/roboptim-core
         )




if(ROBOPTIM_LIBRARIES AND ROBOPTIM_INCLUDE_DIR AND ROBOPTIM_PLUGINS_LIB AND ROBOPTIM_DUMMY_PLUGIN_LIB AND ROBOPTIM_IPOPT_PLUGIN_LIB AND ROBOPTIM_IPOPT_PLUGIN_TD_LIB AND ROBOPTIM_IPOPT_SPARSE_PLUGIN_LIB)
message(STATUS "Found ROBOPTIM Link targets: ${ROBOPTIM_LIBRARIES}")
set(ROBOPTIM_FOUND TRUE)
endif(ROBOPTIM_LIBRARIES AND ROBOPTIM_INCLUDE_DIR AND ROBOPTIM_PLUGINS_LIB AND ROBOPTIM_DUMMY_PLUGIN_LIB AND ROBOPTIM_IPOPT_PLUGIN_LIB AND ROBOPTIM_IPOPT_PLUGIN_TD_LIB AND ROBOPTIM_IPOPT_SPARSE_PLUGIN_LIB)


IF (ROBOPTIM_FOUND)
   message(STATUS "Found ROBOPTIM dir: ${ROBOPTIM_INCLUDE_DIR}")
   SET(ROBOPTIM_INCLUDE_DIRS ${ROBOPTIM_INCLUDE_DIR})

   SET(ROBOPTIM_LIBRARIES ${ROBOPTIM_LIBRARIES} ${ROBOPTIM_DUMMY_PLUGIN_LIB} ${ROBOPTIM_IPOPT_PLUGIN_LIB} ${ROBOPTIM_IPOPT_PLUGIN_TD_LIB} ${ROBOPTIM_IPOPT_SPARSE_PLUGIN_LIB})

ELSE (ROBOPTIM_FOUND)
    #add_definitions(-DIGL_NO_ROBOPTIM)
    message(WARNING "could NOT find ROBOPTIM")
ENDIF (ROBOPTIM_FOUND)
