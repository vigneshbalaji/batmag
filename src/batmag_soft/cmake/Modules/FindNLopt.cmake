#
# Try to find NLOPT
# Once done this will define
#
# NLOPT_FOUND           - system has NLOPT
# NLOPT_INCLUDE_DIRS    - the NLOPT include directories
# NLOPT_LIBRARIES       - Link these to use NLOPT
#

FIND_PATH(NLOPT_INCLUDE_DIR nlopt.hpp nlopt.h
  PATHS /usr/include
    )


#set(NLOPT_LIBRARIES) 
FIND_LIBRARY(NLOPT_LIBRARIES
         NAMES         libnlopt.so
         PATHS	       /usr/lib /usr/local/lib /usr/lib/x86_64-linux-gnu
     ) #NAMES libNLOPT libmosek64  PATHS ${SEARCH_PATHS} NO_DEFAULT_PATH DPATH_SUFFIXES a lib dylib)

if(NLOPT_LIBRARIES AND NLOPT_INCLUDE_DIR)
message(STATUS "Found NLOPT Link targets: ${NLOPT_LIBRARIES}")
set(NLOPT_FOUND TRUE)
endif(NLOPT_LIBRARIES AND NLOPT_INCLUDE_DIR)

IF (NLOPT_FOUND)
   message(STATUS "Found NLOPT dir: ${NLOPT_INCLUDE_DIR}")
   SET(NLOPT_INCLUDE_DIRS ${NLOPT_INCLUDE_DIR} )
   SET(NLOPT_LIBRARIES ${NLOPT_LIBRARIES} )

ELSE (NLOPT_FOUND)
    #add_definitions(-DIGL_NO_NLOPT)
    message(WARNING "could NOT find NLOPT")
ENDIF (NLOPT_FOUND)
