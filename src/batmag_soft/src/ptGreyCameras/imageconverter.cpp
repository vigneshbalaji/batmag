#include "imageconverter.h"



ImageConverter::ImageConverter(ros::NodeHandle nodehandle)
{
    nh=nodehandle;
    it= new image_transport::ImageTransport(nodehandle);
    // Subscribe to input video feed and publish output video feed
    image_pub_cam0 = it->advertise("camera0", 1);
    image_pub_cam1 = it->advertise("camera1", 1);

    cv::namedWindow(OPENCV_WINDOW);
}

ImageConverter::~ImageConverter()
{
    cv::destroyWindow(OPENCV_WINDOW);
    delete it;
}

void ImageConverter::imageCb(const sensor_msgs::ImageConstPtr& msg, int CameraNumber)
{
    cv_bridge::CvImagePtr cv_ptr;
    try
    {
        cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }

    // Draw an example circle on the video stream
    if (cv_ptr->image.rows > 60 && cv_ptr->image.cols > 60)
        cv::circle(cv_ptr->image, cv::Point(50, 50), 10, CV_RGB(255,0,0));

    // Update GUI Window
    cv::imshow(OPENCV_WINDOW, cv_ptr->image);
    cv::waitKey(3);

    // Output modified video stream
    image_pub_cam0.publish(cv_ptr->toImageMsg());
}
