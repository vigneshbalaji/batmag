#include "ros/duration.h"
#include "ros/spinner.h"
#include "ros/ros.h"

#include "camera_param.h"

#include <boost/function.hpp>
#include <boost/bind.hpp>
#include <flycapture/FlyCapture2.h>
#include <string>
#include <stdlib.h>
#include <time.h>

#include "batmag_soft/StartStop.h"
#include "batmag_soft/CameraClick.h"
#include "batmag_soft/RecordRaw.h"
#include "batmag_soft/SaveImage.h"
#include "batmag_soft/GUIpreferences.h"
#include "batmag_soft/getCameraSettings.h"
#include "batmag_soft/CameraSettings.h"


#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

//The Grasshopper3 are in Format7
//The pixel format is PIXEL_FORMAT_RAW8 (8 bit raw data output of sensor)
//With image width and height of 2048



using namespace FlyCapture2;
using namespace std;


bool saveImageArr[2]={false, false};


cameraParams mCamParams;


void PrintBuildInfo()
{
    FC2Version fc2Version;
    Utilities::GetLibraryVersion( &fc2Version );

    ostringstream version;
    version << "FlyCapture2 library version: " << fc2Version.major << "." << fc2Version.minor << "." << fc2Version.type << "." << fc2Version.build;
    ROS_INFO_STREAM(version.str() );

    ostringstream timeStamp;
    timeStamp << "Application build date: " << __DATE__ << " " << __TIME__;
    ROS_INFO_STREAM(timeStamp.str() << endl);
}

void pressedStart(const batmag_soft::StartStop::ConstPtr& msg, bool & startPressed, bool & stopPressed)
{
    startPressed=msg->pressedStart;
    stopPressed=msg->pressedStop;
    if(startPressed)
        ROS_INFO_STREAM("Start was pressed");
    if(stopPressed)
        ROS_INFO_STREAM("Stop was pressed");
}

void recordRawCb(const batmag_soft::RecordRaw::ConstPtr& msg, bool & record, string & recordDir)
{
    record=msg->recordRaw;
    recordDir.append(msg->recordWhere);
}

bool processSaveImage (batmag_soft::SaveImage::Request  &req,
                       batmag_soft::SaveImage::Response &res)
{

    saveImageArr[0]=true;
    saveImageArr[1]=true;

    return true;
}

void guiPrefCb(const batmag_soft::GUIpreferences::ConstPtr& msg, bool & grid)
{
    grid=msg->gridOn;
}

void PrintCameraInfo( CameraInfo* pCamInfo )
{
    ROS_INFO_STREAM("*** CAMERA INFORMATION ***");
    ROS_INFO_STREAM("Serial number -" << pCamInfo->serialNumber);
    ROS_INFO_STREAM("Camera model - " << pCamInfo->modelName);
    ROS_INFO_STREAM("Camera vendor - " << pCamInfo->vendorName);
    ROS_INFO_STREAM("Sensor - " << pCamInfo->sensorInfo);
    ROS_INFO_STREAM("Resolution - " << pCamInfo->sensorResolution);
    ROS_INFO_STREAM("Firmware version - " << pCamInfo->firmwareVersion);
    ROS_INFO_STREAM("Firmware build time - " << pCamInfo->firmwareBuildTime << endl);


}

void PrintError( Error error )
{
    error.PrintErrorTrace();
}

bool getSettings(batmag_soft::getCameraSettings::Request &req, batmag_soft::getCameraSettings::Response &res)
{
    res.Brightness=mCamParams.cameraBrightness;
    res.Exposure=mCamParams.cameraAutoExposure;
    res.Framerate=mCamParams.cameraFrameRate;
    res.Gain=mCamParams.cameraGain;
    res.Gamma=mCamParams.cameraGamma;
    res.Hue=mCamParams.cameraHue;
    res.Saturation=mCamParams.cameraSaturation;
    res.Sharpness=mCamParams.cameraSharpness;
    res.Shutter=mCamParams.cameraShutter;
    res.wbb=mCamParams.cameraWhiteBalanceBlue;
    res.wbr=mCamParams.cameraWhiteBalanceRed;

    return true;
}

void setSettings (batmag_soft::CameraSettings::ConstPtr msg, Camera ** mCam)
{
    mCamParams.cameraBrightness=msg->Brightness;
    mCamParams.cameraAutoExposure=msg->Exposure;
    mCamParams.cameraFrameRate=msg->Framerate;
    mCamParams.cameraGain=msg->Gain;
    mCamParams.cameraGamma=msg->Gamma;
    mCamParams.cameraHue=msg->Hue;
    mCamParams.cameraSaturation=msg->Saturation;
    mCamParams.cameraSharpness=msg->Sharpness;
    mCamParams.cameraShutter=msg->Shutter;
    mCamParams.cameraWhiteBalanceBlue=msg->wbb;
    mCamParams.cameraWhiteBalanceRed=msg->wbr;

    for(int i=0; i<2;++i)
    {
        mCam[i]->StopCapture();
        mCamParams.setCameraParams(*mCam[i]);
        mCam[i]->StartCapture();
    }
}

int main(int argc, char** argv)
{
    //Create rosnode
    ros::init(argc, argv, "Cameras_node");
    ros::NodeHandle n;

    ROS_INFO_STREAM("The camera node was created 25Hz");
    double framerate=25;//Maximum framerate=90
    ros::Rate captureRate(framerate);

    //Recorder Stuff
    bool recordOn=false, prevRec=false, gridOn=false;
    AVIRecorder myRecorder[2];
    MJPGOption recordOption;
    recordOption.frameRate = framerate;
    recordOption.quality = 100;

    time_t now=time(0);
    mCamParams.cameraFrameRate=framerate;

    std::string recordDir= "/home/batmag/BatMag_Recordings/Videos/Raw/", whenDir;
    std::string imageDir= "/home/batmag/BatMag_Recordings/Pictures/";


    //Subscribe to Start/Stop messages
    bool startPressed=false, stopPressed=false; //startPressed is not used but a while !pressed loop can be used to show images only after it has been pressed
    ros::Subscriber startStopSub = n.subscribe<batmag_soft::StartStop>("gui_start_stop", 1, std::bind(pressedStart, std::placeholders::_1, std::ref(startPressed), std::ref(stopPressed)));
    ros::Subscriber recordRawSub = n.subscribe<batmag_soft::RecordRaw>("record_raw", 1, std::bind(recordRawCb, std::placeholders::_1, std::ref(recordOn), std::ref(whenDir)));
    ros::Subscriber guiPrefSub=n.subscribe<batmag_soft::GUIpreferences>("gui_preferences", 1, std::bind(guiPrefCb, std::placeholders::_1, std::ref(gridOn)));

    //Subscribe to save image and get camera settings server
    ros::ServiceServer saveImageServer = n.advertiseService ("save_image", processSaveImage);
    ros::ServiceServer getSettingsServer= n.advertiseService ("get_camera_settings", getSettings);

    //Image transporter
    image_transport::ImageTransport it(n);

    // Publish output video feed
    image_transport::Publisher image_pub_cam0 = it.advertise("camera0", 1);
    image_transport::Publisher image_pub_cam1 = it.advertise("camera1", 1);

    PrintBuildInfo();

    Error error;

    BusManager busMgr;
    unsigned int numCameras;
    error = busMgr.GetNumOfCameras(&numCameras);
    if (error != PGRERROR_OK)
    {
        PrintError( error );
        return -1;
    }

    ROS_INFO_STREAM("Number of cameras detected: " << numCameras);

    if ( numCameras < 1 )
    {
        ROS_ERROR_STREAM("Insufficient number of cameras... press Enter to exit."); ;
        cin.ignore();
        return -1;
    }

    Camera** ppCameras = new Camera*[numCameras];



    // Connect to all detected cameras and attempt to set them to
    // a common video mode and frame rate
    for ( unsigned int i = 0; i < numCameras; i++)
    {
        ppCameras[i] = new Camera();

        PGRGuid guid;
        unsigned int serialNumber;
        if(i==0) serialNumber=16025882;
        else serialNumber=16025886;
        error = busMgr.GetCameraFromSerialNumber(serialNumber, &guid );

        //        error = busMgr.GetCameraFromIndex(i, &guid );
        //        if (error != PGRERROR_OK)
        //        {
        //            PrintError( error );
        //            return -1;
        //        }

        // Connect to a camera
        error = ppCameras[i]->Connect(  &guid );
        if (error != PGRERROR_OK)
        {
            PrintError( error );
            return -1;
        }

        // Get the camera information
        CameraInfo camInfo;
        error = ppCameras[i]->GetCameraInfo( &camInfo );
        if (error != PGRERROR_OK)
        {
            PrintError( error );
            return -1;
        }

        PrintCameraInfo(&camInfo);

        // Get the camera configuration
        FC2Config config;
        error =  ppCameras[i]->GetConfiguration( &config );
        if (error != PGRERROR_OK)
        {
            PrintError( error );
            return -1;
        }

        // Set the number of driver buffers used to 10.
        config.numBuffers = 10;

        // Set the camera configuration
        error = ppCameras[i]->SetConfiguration( &config );
        if (error != PGRERROR_OK)
        {
            PrintError( error );
            return -1;
        }
    }

    // Set all cameras to a specific mode and frame rate so they
    // can be synchronized
    //    unsigned int myPacketSize;
    //    float myPercentage;
    //    Format7ImageSettings myImageSettings;

    //Set both cameras in the same configuration
    //    ppCameras[0]->GetFormat7Configuration(&myImageSettings,&myPacketSize,&myPercentage );

    for ( unsigned int i = 0; i < numCameras; i++)
    {

        //        error = ppCameras[i]->SetFormat7Configuration(&myImageSettings,myPacketSize );
        //        if (error != PGRERROR_OK)
        //        {
        //            PrintError( error );
        //            ROS_ERROR_STREAM("Error starting cameras. ");
        //            ROS_ERROR_STREAM("Press Enter to exit. ");

        //            cin.ignore();
        //            return -1;
        //        }
        if(!mCamParams.setCameraParams(*ppCameras[i]))
        {
            PrintError( error );
            ROS_ERROR_STREAM("Error setting camera parameters. ");
            ROS_ERROR_STREAM("Press Enter to exit. ");

            cin.ignore();
            return -1;
        }
    }


    ros::Subscriber cameraSetting=n.subscribe<batmag_soft::CameraSettings>("camera_settings",1, std::bind(setSettings, std::placeholders::_1,ppCameras));

    //    error = Camera::StartSyncCapture( numCameras, (const Camera**)ppCameras );
    //Start the cameras
    for ( unsigned int i = 0; i < numCameras; i++ )
    {
        error=ppCameras[i]->StartCapture();
        if (error != PGRERROR_OK)
        {
            PrintError( error );
            ROS_ERROR_STREAM("Error starting cameras. ");
            ROS_ERROR_STREAM("Press Enter to exit. ");

            cin.ignore();
            return -1;
        }
    }

    //Capture images from both cameras

    int j[2];
    j[0]=0; j[1]=0;

    //    Creating images for cameras
    Image rawImage, rgbImage;
    cv_bridge::CvImage out_msg;
    cv::Mat outImage;


    ROS_INFO_STREAM("Starting to capture frames");
    while(ros::ok() && !stopPressed)
    {
        // Display the timestamps for all cameras to show that the image
        // capture is synchronized for each image
        for ( unsigned int i = 0; i < numCameras; i++ )
        {

            //            ROS_DEBUG_STREAM("Retriving buffer");
            error = ppCameras[i]->RetrieveBuffer( &rawImage );
            if ( error != PGRERROR_OK )
            {
                std::cout << "capture error" << std::endl;
                continue;
            }
            
            //Initialize recorders
            if(recordOn==true && prevRec==false)
            {
                string filename[2];
                filename[0]=recordDir+whenDir+"-Camera0";
                filename[1]=recordDir+whenDir+"-Camera1";
                for(int j=0; j<2;++j)
                {
                    error=myRecorder[j].AVIOpen(filename[j].c_str(), &recordOption );
                    if ( error != PGRERROR_OK )
                    {
                        std::cout << "capture error" << std::endl;
                        continue;
                    }
                }
                prevRec=true;
            }


            //Avoid overriding videos
            if(recordOn==false && prevRec==true)
                prevRec=false;

            if(recordOn)
            {
                error= myRecorder[i].AVIAppend(&rawImage);
                if ( error != PGRERROR_OK )
                {
                    std::cout << "capture error" << std::endl;
                    continue;
                }
            }



            ROS_DEBUG_STREAM("Converting to bgr");
            rawImage.Convert( FlyCapture2::PIXEL_FORMAT_BGR, &rgbImage );

            if(saveImageArr[i])
            {
                now=time(0);

                tm *ltm = localtime(&now);

                std::string filename=imageDir+"Y"+std::to_string(ltm->tm_year)+"M"+std::to_string(ltm->tm_mon)+"D"+std::to_string(ltm->tm_mday)+"."+std::to_string(ltm->tm_hour)+"."+std::to_string(ltm->tm_min)+"."+std::to_string(ltm->tm_sec)+"Camera"+std::to_string(i)+".png";


                error=rgbImage.Save(filename.c_str());
                if ( error != PGRERROR_OK )
                {
                    std::cout << "capture error" << std::endl;
                    continue;
                }

                ROS_INFO_STREAM(filename.c_str());


                saveImageArr[i]=false;
            }


            ROS_DEBUG_STREAM("Converting to Mat");

            // convert to OpenCV Mat
            unsigned int rowBytes = (double)rgbImage.GetReceivedDataSize()/(double)rgbImage.GetRows();
            outImage = cv::Mat(rgbImage.GetRows(), rgbImage.GetCols(), CV_8UC3, rgbImage.GetData(),rowBytes);

            if(gridOn)
            {
                //           Cross in the center of the raw image
                line(outImage, cv::Point2d(0, 1023), cv::Point2d(2047, 1023), CV_RGB(0, 255, 0), 1);
                line(outImage, cv::Point2d(1023, 0), cv::Point2d(1023, 2047), CV_RGB(0, 255, 0), 1);

                //Second one


                line(outImage, cv::Point2d(0, 0), cv::Point2d(2047, 2047), CV_RGB(0, 255, 0), 1);
                line(outImage, cv::Point2d(2047, 0), cv::Point2d(0, 2047), CV_RGB(0, 255, 0), 1);
            }


            // Here I do not set timestamp and tf frame for the message

            ROS_DEBUG_STREAM("Filling message");
            out_msg.encoding = sensor_msgs::image_encodings::BGR8; // Or whatever
            out_msg.image    = outImage; // Your cv::Mat
            ROS_DEBUG_STREAM("Publishing Cam " << i << " - Frame " << ++j[i]);

            switch (i){
            case 0:
                image_pub_cam0.publish(out_msg.toImageMsg());
                break;
            case 1:
                image_pub_cam1.publish(out_msg.toImageMsg());
                break;
            default:
                ROS_ERROR_STREAM("The selected camera does not exist!");
                break;
            }

            //DEBUGGING
            //            cv::Mat cvImage= cv_bridge::toCvCopy(out_msg.toImageMsg(), sensor_msgs::image_encodings::BGR8)->image;

            //            cv::imshow("image", cvImage);
            //            key = cv::waitKey(30);
        }
        ros::spinOnce();
        captureRate.sleep();
    }

    for ( unsigned int i = 0; i < numCameras; i++ )
    {
        ppCameras[i]->StopCapture();
        ppCameras[i]->Disconnect();
        delete ppCameras[i];
        if(prevRec) myRecorder[i].AVIClose();
    }

    delete [] ppCameras;

    ROS_DEBUG_STREAM("Exiting");


    return 0;
}
