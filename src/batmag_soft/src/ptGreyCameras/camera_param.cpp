#include "camera_param.h"

bool cameraParams::setCameraParams(Camera& camera){
    // Query for available Format 7 modes
    Format7Info cameraInfo;
    bool supported;
    cameraInfo.mode = cameraMode;
    error = camera.GetFormat7Info(&cameraInfo,&supported);
    if (error != PGRERROR_OK)
    {
        PrintError(error);
        return false;
    }
    PrintFormat7Capabilities(&cameraInfo);
    // Check format and mode
    if ( (cameraPixelFormat & cameraInfo.pixelFormatBitField) == 0 )
    {
        cout << "Pixel format and/or mode is not supported\n" << endl;
        return false;
    }

    //Set (Camera Video Modes) parameters as specified in cameraParams
    Format7ImageSettings cameraImageSettings;
    cameraImageSettings.mode = cameraMode;
    cameraImageSettings.pixelFormat = cameraPixelFormat;
    if(cameraOffsetX < 0){
        cameraImageSettings.offsetX = 0;
    }
    else{
        cameraImageSettings.offsetX = cameraOffsetX;
    }
    if(cameraOffsetY < 0){
        cameraImageSettings.offsetY = 0;
    }
    else{
        cameraImageSettings.offsetY = cameraOffsetY;
    }
    if(cameraImageWidth < 0){
        cameraImageSettings.width = cameraInfo.maxWidth;
    }
    else{
        cameraImageSettings.width = cameraImageWidth;
    }
    if(cameraImageHeight < 0){
        cameraImageSettings.height = cameraInfo.maxHeight;
    }
    else{
        cameraImageSettings.height = cameraImageHeight;
    }

    // Validate the settings to make sure that they are valid
    bool valid;
    Format7PacketInfo cameraPacketInfo;
    error = camera.ValidateFormat7Settings(&cameraImageSettings,&valid,&cameraPacketInfo);
    if (error != PGRERROR_OK)
    {
        PrintError(error);
        return false;
    }
    if (!valid)
    {
        // Settings are not valid
        ROS_INFO("Format7 settings are not valid\n");
        return false;
    }

    // Set the settings to the camera
    if(cameraPacketSize<0)
        cameraPacketSize=cameraPacketInfo.recommendedBytesPerPacket;
    error = camera.SetFormat7Configuration(&cameraImageSettings,cameraPacketSize);
    if (error != PGRERROR_OK)
    {
        ROS_INFO_STREAM("Error setting Packet size");
        PrintError(error);
        return false;
    }

    //Set (Camera Settings) parameters as specified in cameraParams
    if(cameraAutoExposure < 0){
        setCameraAutoExposure(&camera,0,true,true,true);
    }
    else{
        setCameraAutoExposure(&camera,cameraAutoExposure);
    }

    if(cameraBrightness < 0){
        setCameraBrightness(&camera,0.0,true);
    }
    else{
        setCameraBrightness(&camera,cameraBrightness);
    }

    if(cameraFrameRate < 0){
        setCameraFrameRate(&camera,0,true,true,true);
    }
    else{
        setCameraFrameRate(&camera,cameraFrameRate);
    }

    if(cameraGain < 0){
        setCameraGain(&camera,0,true,true);
    }
    else{
        setCameraGain(&camera,cameraGain);
    }

    if(cameraGamma < 0){
        setCameraGamma(&camera,1.0,true);
    }
    else{
        setCameraGamma(&camera,cameraGamma);
    }

    if(cameraHue < 0){
        setCameraHue(&camera,0.0,true);
    }
    else{
        setCameraHue(&camera,cameraHue);
    }

    if(cameraSaturation < 0){
        setCameraSaturation(&camera,100.0,true);
    }
    else{
        setCameraSaturation(&camera,cameraSaturation);
    }

    if(cameraSharpness < 0){
        setCameraSharpness(&camera,1024);
    }
    else{
        setCameraSharpness(&camera,cameraSharpness);
    }

    if(cameraShutter < 0){
        setCameraShutter(&camera,0,true,true);
    }
    else{
        setCameraShutter(&camera,cameraShutter);
    }

    if(cameraWhiteBalanceRed < 0 || cameraWhiteBalanceBlue < 0){
        setCameraWhiteBalance(&camera,0,0,true,true);
    }
    else{
        setCameraWhiteBalance(&camera,cameraWhiteBalanceRed,cameraWhiteBalanceBlue);
    }

    return true;
}

void cameraParams::PrintError( Error error )
{
    error.PrintErrorTrace();
}

void cameraParams::setCameraAutoExposure(Camera* cam, const float val, bool onOff, bool autoManualMode, bool absControl){
    //Declare a Property struct.
    Property prop;
    //Define the property to adjust.
    prop.type = AUTO_EXPOSURE;
    //Ensure the property is on.
    prop.onOff = onOff;
    //Ensure auto-adjust mode is off.
    prop.autoManualMode = autoManualMode;
    //Ensure the property is set up to use absolute value control.
    prop.absControl = absControl;
    //Set the absolute value of auto exposure to val EV.
    prop.absValue = val;
    //Set the property.
    error = cam->SetProperty(&prop);

    if(error != PGRERROR_OK){
        PrintError(error);
    }
}

void cameraParams::setCameraBrightness(Camera* cam, const float val, bool absControl){
    //Declare a Property struct.
    Property prop;
    //Define the property to adjust.
    prop.type = BRIGHTNESS;
    //Ensure the property is set up to use absolute value control.
    prop.absControl = absControl;
    //Set the absolute value of brightness to val%.
    prop.absValue = val;
    //Set the property.
    error = cam->SetProperty(&prop);

    if(error != PGRERROR_OK){
        PrintError(error);
    }
}

void cameraParams::setCameraFrameRate(Camera* cam, const float val, bool onOff, bool autoManualMode, bool absControl){
    //Declare a Property struct.
    Property prop;
    //Define the property to adjust.
    prop.type = FRAME_RATE;
    //Ensure the property is on.
    prop.onOff = onOff;
    //Ensure auto-adjust mode is off.
    prop.autoManualMode = autoManualMode;
    //Ensure the property is set up to use absolute value control.
    prop.absControl = absControl;
    //Set the frame rate to val frames per second.
    prop.absValue = val;
    //Set the property.
    error = cam->SetProperty(&prop);

    if(error != PGRERROR_OK){
        PrintError(error);
    }
}

void cameraParams::setCameraGain(Camera* cam, const float val, bool autoManualMode, bool absControl){
    //Declare a Property struct.
    Property prop;
    //Define the property to adjust.
    prop.type = GAIN;
    //Ensure auto-adjust mode is off.
    prop.autoManualMode = autoManualMode;
    //Ensure the property is set up to use absolute value control.
    prop.absControl = absControl;
    //Set the absolute value of gain to val dB.
    prop.absValue = val;
    //Set the property.
    error = cam->SetProperty(&prop);

    if(error != PGRERROR_OK){
        PrintError(error);
    }
}

void cameraParams::setCameraGamma(Camera* cam, const float val, bool absControl){
    //Declare a Property struct.
    Property prop;
    //Define the property to adjust.
    prop.type = GAMMA;
    //Ensure the property is set up to use absolute value control.
    prop.absControl = absControl;
    //Set the absolute value of gamma to val
    prop.absValue = val;
    //Set the property.
    error = cam->SetProperty(&prop);

    if(error != PGRERROR_OK){
        PrintError(error);
    }
}

void cameraParams::setCameraHue(Camera* cam, const float val, bool absControl){
    //Declare a Property struct.
    Property prop;
    //Define the property to adjust.
    prop.type = HUE;
    //Ensure the property is set up to use absolute value control.
    prop.absControl = absControl;
    //Set the absolute value of hue to val degrees
    prop.absValue = val;
    //Set the property.
    error = cam->SetProperty(&prop);

    if(error != PGRERROR_OK){
        PrintError(error);
    }
}

void cameraParams::setCameraSaturation(Camera* cam, const float val, bool absControl){
    //Declare a Property struct.
    Property prop;
    //Define the property to adjust.
    prop.type = HUE;
    //Ensure the property is set up to use absolute value control.
    prop.absControl = absControl;
    //Set the absolute value of saturation to val%
    prop.absValue = val;
    //Set the property.
    error = cam->SetProperty(&prop);

    if(error != PGRERROR_OK){
        PrintError(error);
    }
}

void cameraParams::setCameraSharpness(Camera* cam, unsigned int val){
    //Declare a Property struct.
    Property prop;
    //Define the property to adjust.
    prop.type = SHARPNESS;
    //Set the value of sharpness to val.
    prop.valueA = val;
    //Set the property.
    error = cam->SetProperty(&prop);

    if(error != PGRERROR_OK){
        PrintError(error);
    }
}

void cameraParams::setCameraShutter(Camera* cam, const float val, bool autoManualMode, bool absControl){
    //Declare a Property struct.
    Property prop;
    //Define the property to adjust.
    prop.type = SHUTTER;
    //Ensure auto-adjust mode is off.
    prop.autoManualMode = autoManualMode;
    //Ensure the property is set up to use absolute value control.
    prop.absControl = absControl;
    //Set the absolute value of shutter to val ms.
    prop.absValue = val;
    //Set the property.
    error = cam->SetProperty(&prop);

    if(error != PGRERROR_OK){
        PrintError(error);
    }
}

void cameraParams::setCameraWhiteBalance(Camera* cam, const unsigned int valA, const unsigned int valB, bool onOff, bool autoManualMode){
    //Declare a Property struct.
    Property prop;
    //Define the property to adjust.
    prop.type = WHITE_BALANCE;
    //Ensure the property is on.
    prop.onOff = onOff;
    //Ensure auto-adjust mode is off.
    prop.autoManualMode = autoManualMode;
    //Set the white balance red channel to valA.
    prop.valueA = valA;
    //Set the white balance blue channel to valB.
    prop.valueB = valB;
    //Set the property.
    error = cam->SetProperty(&prop);

    if(error != PGRERROR_OK){
        PrintError(error);
    }
}

void cameraParams::PrintBuildInfo()
{
    FC2Version fc2Version;
    Utilities::GetLibraryVersion(&fc2Version);
    char version[128];
    ROS_INFO(
        version,
        "FlyCapture2 library version: %d.%d.%d.%d\n",
        fc2Version.major, fc2Version.minor, fc2Version.type, fc2Version.build );

    ROS_INFO( "%s", version );

    char timeStamp[512];
    ROS_INFO( timeStamp, "Application build date: %s %s\n\n", __DATE__, __TIME__ );

    ROS_INFO( "%s", timeStamp );
}

void cameraParams::PrintCameraInfo(CameraInfo* pCamInfo)
{
    ROS_INFO(
        "\n*** CAMERA INFORMATION ***\n"
        "Serial number - %u\n"
        "Camera model - %s\n"
        "Camera vendor - %s\n"
        "Sensor - %s\n"
        "Resolution - %s\n"
        "Firmware version - %s\n"
        "Firmware build time - %s\n\n",
        pCamInfo->serialNumber,
        pCamInfo->modelName,
        pCamInfo->vendorName,
        pCamInfo->sensorInfo,
        pCamInfo->sensorResolution,
        pCamInfo->firmwareVersion,
        pCamInfo->firmwareBuildTime);
    fflush(stdout);
}

void cameraParams::PrintFormat7Capabilities(Format7Info* fmt7Info)
{
    ROS_INFO(
        "Max image pixels: (%u, %u)\n"
        "Image Unit size: (%u, %u)\n"
        "Offset Unit size: (%u, %u)\n"
        "Pixel format bitfield: 0x%08x\n"
        "Packet size: %u\n"
        "Minimum packet size: %u\n"
        "Maximum packet size: %u\n"
        "Percentage: %.3f\n\n",
        fmt7Info->maxWidth,
        fmt7Info->maxHeight,
        fmt7Info->imageHStepSize,
        fmt7Info->imageVStepSize,
        fmt7Info->offsetHStepSize,
        fmt7Info->offsetVStepSize,
        fmt7Info->pixelFormatBitField,
        fmt7Info->packetSize,
        fmt7Info->minPacketSize,
        fmt7Info->maxPacketSize,
        fmt7Info->percentage);
    fflush(stdout);
}

void cameraParams::PrintCameraParams(){
    ROS_INFO(
        "Camera Mode: %d\n"
        "CameraPixelFormat: 0x%08x\n"
        "CameraOffsetX: %d\n"
        "CameraOffsetY: %d\n"
        "CameraImageWidth: %d\n"
        "CameraImageHeight: %d\n"
        "CameraAutoExposure: %.3f\n"
        "CameraBrightness: %.3f\n"
        "CameraFrameRate: %.3f\n"
        "CameraGain: %.3f\n"
        "CameraGamma: %.3f\n"
        "CameraHue: %.3f\n"
        "CameraSaturation: %.3f\n"
        "CameraSharpness: %d\n"
        "CameraShutter: %.3f\n"
        "CameraWhiteBalanceRed: %d\n"
        "CameraWhiteBalanceBlue: %d\n\n",
        cameraMode,
        cameraPixelFormat,
        cameraOffsetX,
        cameraOffsetY,
        cameraImageWidth,
        cameraImageHeight,
        cameraAutoExposure,
        cameraBrightness,
        cameraFrameRate,
        cameraGain,
        cameraGamma,
        cameraHue,
        cameraSaturation,
        cameraSharpness,
        cameraShutter,
        cameraWhiteBalanceRed,
        cameraWhiteBalanceBlue);
    fflush(stdout);
}
