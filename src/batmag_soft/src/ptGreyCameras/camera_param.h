#pragma once

#include "ros/ros.h"

#include <flycapture/FlyCapture2.h>

using namespace FlyCapture2;
using namespace std;

struct cameraParams{
    Error error;

    //Camera video modes
    Mode cameraMode = MODE_0;
    PixelFormat cameraPixelFormat = PIXEL_FORMAT_RAW8;

    //Set to negative value to use default/auto settings
    int cameraOffsetX = -1;
    int cameraOffsetY = -1;
    int cameraImageWidth = -1;
    int cameraImageHeight = -1;

    //Camera settings
    uint cameraPacketSize=48048;
    float cameraAutoExposure = -1;
    float cameraBrightness = -1;
    float cameraFrameRate = -1;
    float cameraGain = -1;
    float cameraGamma = -1;
    float cameraHue = -1;
    float cameraSaturation = -1;
    int cameraSharpness = -1;
    float cameraShutter = -1;
    //Computed from flycap
    int cameraWhiteBalanceRed = 640;
    int cameraWhiteBalanceBlue = 575;

    // Prototypes
    void PrintBuildInfo();
    void PrintCameraInfo(FlyCapture2::CameraInfo*);
    void PrintFormat7Capabilities(Format7Info*);
    void PrintError(Error error);


    bool setCameraParams(Camera&);
    void setCameraAutoExposure(Camera *cam, const float val, bool onOff=true, bool autoManualMode=false, bool absControl=true);
    void setCameraBrightness(Camera *cam, const float val, bool absControl=true);
    void setCameraFrameRate(Camera *cam, const float val, bool onOff=true, bool autoManualMode=false, bool absControl=true);
    void setCameraGain(Camera *cam, const float val, bool autoManualMode=false, bool absControl=true);
    void setCameraGamma(Camera *cam, const float val, bool absControl=true);
    void setCameraHue(Camera *cam, const float val, bool absControl=true);
    void setCameraSaturation(Camera *cam, const float val, bool absControl=true);
    void setCameraSharpness(Camera *cam, const unsigned int);
    void setCameraShutter(Camera *cam, const float val, bool autoManualMode=false, bool absControl=true);
    void setCameraWhiteBalance(Camera *cam, const unsigned int valA, const unsigned int valB, bool onOff=true, bool autoManualMode=false);
    void PrintCameraParams();
};
