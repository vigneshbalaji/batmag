#pragma once

#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <string>

class ImageConverter
{
public:
    ImageConverter(ros::NodeHandle nh);

    virtual ~ImageConverter();

    void imageCb(const sensor_msgs::ImageConstPtr& msg, int CameraNumber);

    const std::string OPENCV_WINDOW  = "Image window";
    image_transport::Publisher image_pub_cam0;
    image_transport::Publisher image_pub_cam1;

    image_transport::ImageTransport * it;
    ros::NodeHandle nh;


};
