#pragma once

#include <Eigen/StdVector>
#include <Eigen/Core>

#include "controller.h"

class Observer
{
public:
    Observer();

    void iterativeLearningObserver(const bool& init, const Eigen::Vector3d &measPos, Eigen::Vector3d &estPos, Eigen::Vector3d &estVel);

private:

    const double density = 1300;
    const double dyn_visc = 1.002e3;
    double mass, damping;

    Eigen::Vector3d prevEstPos, prevEstVel;

    // compute default values of mass and damping
    void compute_mass_damping();


    // ILO parameters
    double posCorrGainILO = 30.0, velCorrGainILO = 2.5, velDelGainILO = 1.0;

};
