#include "pid3d.h"


PID3D::PID3D(agentType agent)
{
    maxIntForce << 10, 10, 10;
    Tt = 0.5;
    satForce << 10, 10, 10;
    switch (agent)
    {
    case microparticle:
        // set parameters for microparticle
        ROS_INFO("Setting parameters for Microparticles");
        Kp << 0.6, 0.6, 0.6;
        Ki << 0, 0, 0.08;
        Kd << 0, 0, 0.025;
        paramInit=true;


        break;

    case hydroGripper:
        // set parameters for hydrogel
        ROS_INFO("Setting parameters for Hydrogel");

        if (true)//TODO Here a variable for open/closed should be put (or a function to change the parameters)
        {
            Kp << 0.1, 0.1, 0.1;
            Ki << 0.08, 0.08, 0.08;
            Kd << 0, 0, 0;
        }
        else
        {
            Kp << 0.1, 0.1, 0;
            Ki << 0, 0, 0;
            Kd << 0, 0, 0;
        }
        //This paramInit aviods faults if the constructor is not properly called
        paramInit=true;
        break;
    case metallicGripper:

        ROS_INFO("Setting parameters for Metallic Grippers");

        if (true)//TODO Here a variable for open/closed should be put (or a function to change the parameters)
        {
            Kp << 0.6, 0.6, 0.6;
            Ki << 0, 0, 0.08;
            Kd << 0, 0, 0.025;
        }
        else
        {
            Kp << 0.1, 0.1, 0;
            Ki << 0, 0, 0;
            Kd << 0, 0, 0;
        }

        paramInit=true;
        break;

    case sphericalPM1mm:

            ROS_INFO("Setting parameters for 1mm spherical permanent magnets");
        Kp << 0.06, 0.06, 0.06;
        Ki << 0.00, 0.00, 0.12;
        Kd << 0, 0, 0.01;


        paramInit=true;

        break;

    default:
        paramInit=false;
        break;


    }

}

PID3D::PID3D(agentType agent, vector<double> values) : PID3D(agent)
{
    ROS_INFO("Using user provided parameters");

    setInitInterfaceParameters(values);

    // initialize time
    nowT = boost::posix_time::microsec_clock::local_time();
    prevT = nowT;
}

PID3D::~PID3D()
{
    //This destructor is super chill
}

void PID3D::setInitInterfaceParameters(const vector<double>& values)
{
    // Assign Parameters to Vectors
    setProportionalGain(values[0],values[1],values[2]);
    setIntegralGain(values[3],values[4],values[5]);
    setDerivativeGain(values[6],values[7],values[8]);

    ROS_INFO_STREAM("P is :"<< values[0]<<" , I is "<<values[1]<<" , D is "<<values[2] );

}

bool PID3D::getInitInterfaceParameters(vector<double> &value, vector<double> &max, vector<double> &min, vector<string> &names)
{
    if(paramInit)
    {
        ROS_INFO_STREAM("Everything is ok before initilizing value");
        value.resize(9);
        value={Kp[0],Kp[1], Kp[2],
               Ki[0],Ki[1],Ki[2],
               Kd[0], Kd[1], Kd[2]};

        ROS_INFO_STREAM("Everything is ok after initilizing value");
        max.resize(9);
        min.resize(9);
        for(int i=0; i<9;i++)
        {
            max[i]=value[i]*1.5;
            min[i]=value[i]*0.5;
        }
        names={"Px", "Py", "Pz",
               "Ix", "Iy", "Iz",
               "Dx","Dy","Dz"};
    }
    return paramInit;
}

void PID3D::setProportionalGain(const double& x, const double& y, const double& z)
{
    Kp << x, y, z;
}

void PID3D::setDerivativeGain(const double& x, const double& y, const double& z)
{
    Kd << x, y, z;
}

void PID3D::setIntegralGain(const double& x, const double& y, const double& z)
{
    Ki << x, y, z;
}

//Checks if the Integral action is over limits
void PID3D::saturateIntegralForces()
{
    diffForce = maxIntForce.cwiseAbs()-Ui.cwiseAbs();
    if(diffForce.minCoeff()<0)
    {//Find which direction is over limit, if there is a limit...
        if(diffForce(0)<0 && maxIntForce(0))
            //...and saturate it
            Ui(0)=copysign(maxIntForce(0), Ui(0));

        if(diffForce(1)<0 && maxIntForce(1))
            Ui(1)=copysign(maxIntForce(1), Ui(1));

        if(diffForce(2)<0 && maxIntForce(2))
            Ui(2)=copysign(maxIntForce(2), Ui(2));
    }
}

std::vector<double> PID3D::evaluate()
{
    Vector3d posin={state[0],state[1],state[2]}, setin={reference[0],reference[1],reference[2]}, result;
    result=evaluate(posin,setin);
    std::vector<double> stdResult={result[0], result[1], result[2]};
    return stdResult;
}

Vector3d PID3D::evaluate(const Vector3d& posin,const Vector3d& setin)
{
    //Check if err has been defined and if so assign its value to previous error(same with pprev)
    if (err.any())
        preverr = err;

    if (preverr.any())
        ppreverr = preverr;

    if (prevSetp!=setin)
        Ui={0, 0, 0};
    // error 3x1 vector
    err = setin-posin;

    ROS_DEBUG_STREAM("The error is "<< err);

    // Compute time
    nowT = boost::posix_time::microsec_clock::local_time();
    diff = nowT-prevT;
    sampleTime = diff.total_milliseconds()*0.001; // seconds!
    prevT = nowT;

    //        // Alper filter for error
    //        float c;
    //        if (cutoff_frequency == -1)
    //        {
    //            c = 1.0; // Default to a cut-off frequency at one-fourth of the sampling rate
    //        }
    //        else
    //        {
    //            c = 1/tan((cutoff_frequency*6.2832)*sampleTime/2);
    //        }

    // do separately for x, y, z
    for (unsigned i = 0; i < 3; i++)
    {
        // Proportional Action
        Up[i] = Kp[i]*err[i];

        // Integral Action
        Ui[i] += Ki[i]*err[i]*sampleTime;
        ROS_DEBUG_STREAM("The integral gain is: "<< Ki<<"\n The sampling time is: "<< sampleTime<<"\n The integral action is"<< Ui);
        saturateIntegralForces();
        ROS_DEBUG_STREAM("The integral action after saturation is"<< Ui);



        //Derivative Action
        if (sampleTime > 0)
            Ud[i] = Kd[i]*(err[i]-preverr[i])/sampleTime;
    }

    // Total
    Utot = Ui + Ud + Up;


    ROS_DEBUG_STREAM("Total control action: "<<Utot<<"\n The Proportional action is: "<< Up<<"\n The integral action is: "<< Ui<<"\n The derivative action is: "<< Ud);



    //        ///////////////////////////////////////////////////////////
    //        //         UNCOMMENT FOR TRADITIONAL SATURATION    ///////
    //        ///////////////////////////////////////////////////////////
    //        if (diffsat.minCoeff()<0)
    //        {
    //            if (diffsat(0)<0)
    //                Utot(0) = copysign(satForce(0), Ui(0));

    //            if (diffsat(1)<0)
    //                Utot(1) = copysign(satForce(1), Ui(1));

    //            if (diffsat(2)<0)
    //                Utot(2) = copysign(satForce(2), Ui(2));
    //        }
    //        ////////////////////////////////////////////////////////////
    //        /////////////////////////////////////////////////////////////

    saturateForces(Utot);



    ROS_DEBUG_STREAM("The saturated forces are: "<<Utot);



    return Utot;
}

void PID3D::resetTimer(void)
{
    prevT = boost::posix_time::microsec_clock::local_time();
}


//This saturation technique preserves the direction of the control action in the vector space
//essentially coupling the motion in different directions

void PID3D::saturateForces(Vector3d & ctrlAction){
    int minCoeffIndex;

    //Find where the control action saturates the most
    Vector3d diff=satForce.cwiseAbs()-ctrlAction.cwiseAbs();

    if(diff.minCoeff(&minCoeffIndex)<0 && ctrlAction[minCoeffIndex]!=0 && satForce[minCoeffIndex]!=0){

        //Compute the ratio between the max saturation and the maximum prescribed force
        double ratio=satForce[minCoeffIndex]/ctrlAction[minCoeffIndex];
        ROS_DEBUG_STREAM("THE RATIO IS: "<< ratio);

        //And scale everything with respect to that
        ctrlAction=ctrlAction*fabs(ratio);
    }
}
