#include "controlinit.h"

ControlInit::ControlInit()
{
    //Not much to do here
}

ControlInit::~ControlInit()
{
    //All these destructors are so chill
}

//This function processes the init request of getting parameters when a control type is selected
bool ControlInit::processGetMessage(batmag_soft::GetInitParameters::Request  &req,
                                batmag_soft::GetInitParameters::Response &res)
{
    vector<double> value,max, min;
    vector<string> names;
    ctrlParameters.clear();

    ROS_INFO_STREAM("In processGetMessage");

    res.success=getInterfaceParameters(req.ctrl, req.agent, value, max, min, names);
    if(res.success)
    {


        ROS_INFO_STREAM("Got Control Parameters");
        ctrlParameters=value;
        for(int i=0; i<value.size(); i++)
        {
            res.value[i]=value[i];
            res.max[i]=max[i];
            res.min[i]=min[i];
            if(!names[i].empty()) res.names[i]=names[i];
            ROS_INFO_STREAM("The value for "<<res.names[i]<< " is "<<res.value[i]<<" with max "<<res.max[i]<<" and min "<<res.min[i]);
        }
    }

    return res.success;
}

bool ControlInit::processSetMessage(batmag_soft::SetInitParameters::Request  &req,
                                batmag_soft::SetInitParameters::Response &res)
{
    vector<double> value;
    //ctrlParameters was already initialized when getInitParam was called
    value.resize(ctrlParameters.size());
    for(int i=0; i<ctrlParameters.size(); i++)
    {
        value[i]=req.value[i];
    }
    setInterfaceParameter(value);
    res.done=true;
    return res.done;

}


//Function to or return the prameters for the controller
boost::shared_ptr<Controller> ControlInit::selectCtrl(bool startPressed)
{
    boost::shared_ptr<Controller> initCtrl;
    //Start pressed tells the system if we are initializing the control or just getting its standard parameters
    if(!startPressed){
        //If we are only getting the standard parameters we only give the agent type...
        switch (useCtrl){

        case PID3Dtype:
            initCtrl=boost::make_shared<PID3D>(PID3D(useAgent));
            break;

        case Rotatingtype:
            initCtrl=boost::make_shared<RotatingField>(RotatingField(useAgent));
            break;

        case PID5Dtype:
            initCtrl=boost::make_shared<PID5D>(PID5D(useAgent));
            break;

        default:
            return false;
        }
    }

    else//...we also provide the user-given parameters
    {
        switch (useCtrl){

        case PID3Dtype:
            initCtrl= boost::make_shared<PID3D>(PID3D(useAgent,ctrlParameters));
            break;

        case Rotatingtype:
            initCtrl=boost::make_shared<RotatingField>(RotatingField(useAgent,ctrlParameters));
            break;

        case PID5Dtype:
            initCtrl=boost::make_shared<PID5D>(PID5D(useAgent, ctrlParameters));
            break;

        default:
            return false;
        }
    }
    return initCtrl;

}

//This function returns the deafult parameters for a certain control
bool ControlInit::getInterfaceParameters(string selectedCtrl, string selectedAgent, vector<double> &value, vector<double> &max, vector<double> &min, vector<string> &names)
{
    boost::shared_ptr<Controller> initCtrl;
    useCtrl=setUseCtrl(selectedCtrl);
    useAgent=setUseAgent(selectedAgent);

    ROS_INFO_STREAM("Using control #"<<useCtrl<<" with agent #"<<useAgent);

    initCtrl=selectCtrl(false);
    return initCtrl->getInitInterfaceParameters(value, max, min, names);
}

//This function sets the parameters to the ones set by the user in the GUI, returns true if successful
bool ControlInit::setInterfaceParameter(const vector<double> & value)
{
    if(ctrlParameters.size())
    {
        ctrlParameters.clear();
        ctrlParameters=value;
        return true;
    }
    else
        return false;
}


boost::shared_ptr<Controller> ControlInit::InitializeController(){
    boost::shared_ptr<Controller> myController=  selectCtrl(true);
    return myController;
}

//---------------------------------------------------------------------//
//----IF YOU CREATE A NEW CONTROLLER OR ROBOT YOU MUST ADD IT HERE-----//
//---------------------------------------------------------------------//

ctrlType ControlInit::setUseCtrl(string selectedCtrl)
{
    if(selectedCtrl=="PID3D") return PID3Dtype;

    if(selectedCtrl=="PID5D") return PID5Dtype;

    if(selectedCtrl=="Rotating Field") return Rotatingtype;

    else
    {
        ROS_ERROR("There selected control does not exist");
        return Errortype;
    }

}


//--------------------------------------------------------------//
//--------------------------------------------------------------//
