#pragma once
#include <boost/date_time/posix_time/posix_time.hpp>

#include "ros/ros.h"
#include "controller.h"
#include "../typeHeader.h"

using namespace Eigen;
using std::vector;
using std::string;



class PID3D : public Controller
{
public:
    //The PID can also be
    PID3D(agentType);
    PID3D(agentType agent, vector<double>);
    virtual ~PID3D();

    Vector3d evaluate(const Vector3d&, const Vector3d&);
    std::vector<double>  evaluate();

    bool getInitInterfaceParameters(vector<double> &value, vector<double> &max, vector<double> &min, vector<string> &names);

protected:

    virtual void setInitInterfaceParameters(const vector<double>&);


private:

    virtual void resetTimer(void);

    virtual void setProportionalGain(const double&, const double&, const double&);
    virtual void setDerivativeGain(const double&, const double&, const double&);
    virtual void setIntegralGain(const double&, const double&, const double&);
    void saturateForces(Vector3d &ctrlAction);

    void saturateIntegralForces();
//    void dynamicsaturation();

    //Parameters
    bool paramInit=false;
    Vector3d Kp, Ki, Kd;
    Vector3d maxIntForce, diffForce;
    Vector3d satForce,  diffsat, deriv;

    double Tt;

    //Variables
    Vector3d pos, prevSetp, err, preverr, ppreverr;

    //Control actions
    Vector3d Utot, Up, Ui, Ud;

    // Alper filter for error
    vector<Vector3d> filtered_error;
    double cutoff_frequency;
    Vector3d loop_counter;
    vector<Vector3d> error_derivative, filtered_error_derivative;

    // Time
    boost::posix_time::ptime nowT, prevT;
    boost::posix_time::time_duration diff;

    double sampleTime;


};
