#include <functional>

#include "ros/duration.h"
#include "ros/spinner.h"
#include "ros/ros.h"
#include "batmag_soft/GetInitParameters.h"
#include "batmag_soft/SetInitParameters.h"
#include "batmag_soft/StartStop.h"
#include "batmag_soft/CtrlOutput.h"
#include "batmag_soft/Reference.h"
#include "batmag_soft/State.h"
#include "batmag_soft/GUIpreferences.h"
#include "std_msgs/String.h"
#include "geometry_msgs/Twist.h"
#include "pid3d.h"
#include "controlinit.h"
#include "../typeHeader.h"

#include <boost/function.hpp>
#include <boost/bind.hpp>

using boost::shared_ptr;

void pressedStart(const batmag_soft::StartStop::ConstPtr& msg, bool & startPressed, bool & stopPressed)
{
    startPressed=msg->pressedStart;
    stopPressed=msg->pressedStop;

}

void guiPrefCb(const batmag_soft::GUIpreferences::ConstPtr& msg, bool & ctrlEn, bool & omegaCtrl)
{
    ctrlEn=msg->ctrlEn;
}

int main(int argc, char *argv[])
{
    ROS_INFO_STREAM("The control node has started");
    //Create the ros node and the handle
    ros::init(argc, argv, "control_node");
    ros::NodeHandle n;


    //Frequency of this node
    ros::Rate spinRate(kControlFrequency);

    boost::shared_ptr<Controller> myController;


    //Wait for the node to be online
    while(!ros::ok())
        usleep(500);

    //Define Classes
    ControlInit myControlInit;

    //Defining callbacks

    //Advertise the parameters services
    ros::ServiceServer getInitParamServer = n.advertiseService ("get_init_parameters", &ControlInit::processGetMessage, &myControlInit);
    ros::ServiceServer setInitParamServer = n.advertiseService ("set_init_parameters", &ControlInit::processSetMessage, &myControlInit);

    //Subscribe to Start/Stop messages
    bool startPressed=false, stopPressed=false;
    ros::Subscriber startStopSub = n.subscribe<batmag_soft::StartStop>("gui_start_stop", 1, std::bind(pressedStart, std::placeholders::_1, std::ref(startPressed), std::ref(stopPressed)));
    //Subscribe to GUI preferences
    bool ctrlEnable=false;
    bool omegaCtrl=false;
    ros::Subscriber guiPref= n.subscribe<batmag_soft::GUIpreferences>("gui_preferences", 1, std::bind(guiPrefCb, std::placeholders::_1, std::ref(ctrlEnable), std::ref(omegaCtrl)));

    while(!startPressed && ros::ok() && !stopPressed)
    {
        ros::spinOnce();
        spinRate.sleep();
    }

    if(!stopPressed)
    {
        //Start an asynchronous spinner active on as many cores as the cpu has
        ros::AsyncSpinner myAsyncSpinner(0);
        myAsyncSpinner.start();
        //From here we don't spin at spinFrequency anymore

        myController= myControlInit.InitializeController();

        ROS_INFO_STREAM("The control was Initialized");


        //Subscribe to messages on reference and state
        ros::Subscriber referenceSub = n.subscribe<batmag_soft::Reference>("reference", 1, &Controller::setRefFromMsg, myController);
        ros::Subscriber stateSub = n.subscribe<batmag_soft::State>("state", 1, &Controller::setStateFromMsg, myController);

        ROS_INFO_STREAM("Subscribed to ref and state");
        //Start publishing control actions

        //Advertise control output message
        ros::Publisher ctrlOutputPub=n.advertise<batmag_soft::CtrlOutput>("ctrl_output", 1);
        std::vector<double> ctrlAction;
        batmag_soft::CtrlOutput ctrlOutMsg;

        //Servers for changing Parameters
        ros::ServiceServer getParamServer = n.advertiseService ("get_ctrl_parameters", &Controller::getParamFromMsg, myController);
        ros::ServiceServer setParamServer = n.advertiseService ("set_ctrl_parameters", &Controller::setParamFromMsg, myController);

        while(!stopPressed && ros::ok()){
            if(ctrlEnable)
            {
                ctrlAction=myController->evaluate();
                ctrlOutMsg.size=ctrlAction.size();
                ROS_DEBUG_STREAM("Sent Control Action is: ");
                for (int i=0;i<ctrlAction.size(); i++)
                {
                    ctrlOutMsg.ctrlout[i]=ctrlAction[i];

                    ROS_DEBUG_STREAM(ctrlAction[i]);
                }
                ctrlOutputPub.publish(ctrlOutMsg);
                ROS_DEBUG_STREAM("Sending ctrl reference");
            }
            spinRate.sleep();
        }


        myAsyncSpinner.stop();

    }


}

//ultrasound holder

