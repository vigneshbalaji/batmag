#include "rotatingfield.h"

RotatingField::RotatingField(agentType agent)
{
    switch (agent)
    {//You can set stuff here but there's no need for it ATM
    case microparticle:
    case hydroGripper:
    case metallicGripper:
    default:

        frequency=1;
        amplitudeRotField=10e-3;
        amplitudeFixedField=1e-3;

        paramInit=true;
        break;

    };
}


RotatingField::RotatingField(agentType agent, vector<double> values)
{
    setInitInterfaceParameters(values);
    // initialize time
    nowT = boost::posix_time::microsec_clock::local_time();
    initT = nowT;

}

RotatingField::~RotatingField()
{
    //This destructor is super chill
}

std::vector<double> RotatingField::evaluate()
{
    Vector3d posin={state[0],state[1],state[2]}, setin={reference[0],reference[1],reference[2]}, result;
    result=evaluate(posin,setin);
    std::vector<double> stdResult={result[0], result[1], result[2]};
    return stdResult;
}



Vector3d RotatingField::evaluate(const Vector3d& posin,const Vector3d& setin)
{


    nowT = boost::posix_time::microsec_clock::local_time();
    diffT = nowT-initT;
    secTime = diffT.total_milliseconds()*0.001; // seconds!

    if(clockwise)
    unRotF={amplitudeFixedField, amplitudeRotField*sin(2*PI*frequency*secTime), amplitudeRotField*cos(2*PI*frequency*secTime)};
    else
        unRotF={amplitudeFixedField, amplitudeRotField*cos(2*PI*frequency*secTime), amplitudeRotField*sin(2*PI*frequency*secTime)};


    rotF=rotateField(posin, setin, unRotF);

    ROS_DEBUG_STREAM("Final field is: "<<rotF);
    return rotF;

}

void RotatingField::setInitInterfaceParameters(const vector<double>& values)
{
    // Assign Parameters to Vectors
    setFrequency(values[0]);
    setAmplitudeRotField(values[1]*1e-3);
    setAmplitudeFixedField(values[2]*1e-3);
    setClockwise(values[3]);

    ROS_INFO_STREAM("Frequency is :"<< frequency<<"\n The amplitude of the rotating field is: "<<amplitudeRotField<<"\n The amplitude of the fixed field is: "<<amplitudeFixedField );

}

bool RotatingField::getInitInterfaceParameters(vector<double> &value, vector<double> &max, vector<double> &min, vector<string> &names)
{
    if(paramInit)
    {
        ROS_DEBUG_STREAM("Everything is ok before initilizing value");
        value.resize(3);
        max.resize(3);
        min.resize(3);
        names.resize(3);

        value={frequency, amplitudeRotField*1000, amplitudeFixedField*1000};

        ROS_DEBUG_STREAM("Everything is ok after initilizing value");
        for(int i=0; i<3;i++)
        {
            max[i]=100;
            min[i]=0;
        }
        names={"Frequency [Hz]", "RotField [mT]", "FixField [mT]"};
    }
    return paramInit;
}

Vector3d RotatingField::rotateField(const Vector3d& pos, const  Vector3d& ref, const Vector3d& unRotField)
{

    ROS_DEBUG_STREAM("Starting to compute matrices");
    diff=ref-pos;//B

    normDiff=diff/diff.norm();

    ufield<< 1,0,0;//A

    vProj=ufield;//vector projection

    vRej=normDiff-ufield.dot(normDiff)*ufield;//vector rejection

    cProd=normDiff.cross(ufield);//product

    u=vProj; //normalized projection
    v=vRej/vRej.norm();//normalized rejection
    w=cProd/cProd.norm();//normalized cross product


    matF<< u,v,w;

    ROS_DEBUG_STREAM("F is: "<< matF<<"\n while u is: "<<u<<"\n v is: "<<v<<"\n w is:"<<w);
    matF=matF.inverse().eval();

    matG<<ufield.dot(normDiff), -cProd.norm(),     0,
            cProd.norm()  ,  ufield.dot(normDiff), 0,
            0,                       0,        1;

    matR=matF.inverse()*matG*matF;

    ROS_DEBUG_STREAM("Determinant of U is: "<< matR.determinant());
    return matR*unRotField;

}


void RotatingField::resetTimer()
{
    initT = boost::posix_time::microsec_clock::local_time();
}



