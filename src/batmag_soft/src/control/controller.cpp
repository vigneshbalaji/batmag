#include "controller.h"
#include "ros/ros.h"

Controller::Controller(){
}

Controller::~Controller(){
    //Nothing to do here
}


void Controller::setStateFromMsg(const batmag_soft::State::ConstPtr& msg){
    std::vector<double> s;
    s.resize(6);
    for(int i=0; i<6; i++)
        s[i]=msg->state[i];
    setState(s);
    ROS_DEBUG_STREAM("State set at x: "<<state[0]<<", y: "<<state[1]<<", z: "<<state[2]);


}

void Controller::setRefFromMsg(const batmag_soft::Reference::ConstPtr& msg){
    std::vector<double> ref;
    ref.resize(6);
    for(int i=0; i<6; i++)
        ref[i]=msg->ref[i];
    setReference(ref);
    ROS_DEBUG_STREAM("Reference set at x: "<<ref[0]<<", y: "<<ref[1]<<", z: "<<ref[2]);

}


bool Controller::setParamFromMsg(batmag_soft::SetInitParameters::Request  &req,
                                 batmag_soft::SetInitParameters::Response &res)
{
    vector<double> value;
    //ctrlParameters was already initialized when getInitParam was called
    value.resize(req.value.size());
    for(int i=0; i<req.value.size(); i++)
    {
        value[i]=req.value[i];
    }
    setInitInterfaceParameters(value);

    return true;

}

bool Controller::getParamFromMsg(batmag_soft::GetInitParameters::Request  &req,
                                 batmag_soft::GetInitParameters::Response &res)
{
    vector<double> value,max, min;
    vector<string> names;

    getInitInterfaceParameters(value, max, min, names);

    for(int i=0; i<value.size(); i++)
    {
        res.value[i]=value[i];
        res.max[i]=max[i];
        res.min[i]=min[i];
        if(!names[i].empty())
            res.names[i]=names[i];
        ROS_INFO_STREAM("The value for "<<res.names[i]<< " is "<<res.value[i]<<" with max "<<res.max[i]<<" and min "<<res.min[i]);
    }

    return true;
}
