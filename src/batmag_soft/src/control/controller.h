#pragma once

#include <string>
#include <vector>


#include <Eigen/StdVector>
#include <Eigen/Core>

#include "ros/ros.h"
#include "../typeHeader.h"

#include "batmag_soft/Reference.h"
#include "batmag_soft/State.h"

#include "batmag_soft/GetInitParameters.h"
#include "batmag_soft/SetInitParameters.h"

#include "geometry_msgs/Twist.h"

using std::vector;
using std::string;


const double kControlFrequency=40;

//This class is purely abstact and only provides an interface. Please, stick to it

class Controller
{
public:
    Controller();
    virtual ~Controller();

    virtual std::vector<double>  evaluate() = 0;

    //These functions get and set the init interface parameters
    virtual bool getInitInterfaceParameters(vector<double> &value, vector<double> &max, vector<double> &min, vector<string> &names) =0;

    virtual void setStateFromMsg(const batmag_soft::State::ConstPtr&);
    virtual void setRefFromMsg(const batmag_soft::Reference::ConstPtr&);

    virtual bool getParamFromMsg(batmag_soft::GetInitParameters::Request  &req,
                                 batmag_soft::GetInitParameters::Response &res);

    virtual bool setParamFromMsg(batmag_soft::SetInitParameters::Request  &req,
                                 batmag_soft::SetInitParameters::Response &res);


protected:

    virtual void setInitInterfaceParameters(const vector<double>&) =0;


    //    //Sets and Gets//

    //Current state (position, angle, speed) returns true if successful
    virtual bool setState(const vector<double>& cs){
        state=cs;
        return true;
    }

    virtual void getState(vector<double>& cs){
        cs=state;
    }

    //Reference returns true if successful
    virtual bool setReference(const vector<double>& ref){
        reference=ref;
        return true;
    }

    virtual void getReference(vector<double> & ref){
        ref=reference;
    }




protected:

    //    vector<double> state,reference;

    //    double sampleTime;


    //----------------------------------------------//
    //------------Controllers Parameters------------//
    //----------------------------------------------//

    vector<double> ctrlParameters, state, reference;


private:






};
