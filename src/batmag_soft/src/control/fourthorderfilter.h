#pragma once

#include <Eigen/Dense>
#include <eigen3/unsupported/Eigen/MatrixFunctions>
#include <math.h>


#include "ros/ros.h"
#include "controller.h"



using namespace Eigen;

class FourthOrderFilter
{
public:
    FourthOrderFilter(double samplingTime);

    //Used to change the current/initial state
    void resetInitialState(double pos, double vel=0, double acc=0, double jerk=0);

    void update(const double finalRef, double &posRef, double &velRef, double &accRef);

    inline void setSamplingTime(double sampTime){ts=sampTime;}


private:
    //Used to change the time constant tau
    void setTimeConstant(double timeConst, double upperBound=1.2, double lowerBound=0.3);

    //Recomputes the matrices if tau is changed
    void recompMatrices();


    void exactDiscretization();

    Eigen::Vector4d state, prevState;
    double tau, fRef;// tau AKA time constant

    //State space Matrices
    Eigen::Matrix<double, 4,4> A, Ad, Idn;
    Eigen::Matrix<double, 4,1> B, Bd;
    Eigen::Matrix<double, 1,4> C;

    //sampling time
    double ts;




};
