#include "observer.h"

Observer::Observer()
{

}

void Observer::iterativeLearningObserver(const bool& init, const Eigen::Vector3d& measPos, Eigen::Vector3d& estPos, Eigen::Vector3d& estVel)
{
    // continuous-time representation of the Iterative Learning Observer (ILO)
    //
    // initialize the delayed signal term at time instant 0
    if(!init)
    {
        // compute the initial velocity estimate
        estVel = velCorrGainILO*(measPos - prevEstPos);
    }
    else
    {
        // compute the velocity estimate after the initial one
        estVel = velDelGainILO*prevEstVel + velCorrGainILO*(measPos - prevEstPos);
    }
    // compute the estimated position term the delay from previous iteration is considered the same as the sampling time
    estPos = prevEstPos + (1/kControlFrequency)*(posCorrGainILO*(measPos - prevEstPos) + estVel);

    prevEstVel=estVel;
    prevEstPos=estPos;
}
