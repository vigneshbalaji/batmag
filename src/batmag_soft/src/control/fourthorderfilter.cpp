#include "fourthorderfilter.h"

FourthOrderFilter::FourthOrderFilter(double samplingTime)
{
    ts=kControlFrequency;
    for(int i=0; i<4; i++)
        state[i]=0;
    tau=10;


    void exact_discretization();

}

void FourthOrderFilter::update(const double finalRef, double &posRef, double &velRef, double &accRef)
{
    fRef=finalRef;
    prevState=state;
    state=Ad*prevState+Bd*fRef;

    posRef=state[0];
    velRef=state[1];
    accRef=state[2];

}


void FourthOrderFilter::resetInitialState(double pos, double vel, double acc, double jerk)
{
    state[0]=pos;
    state[1]=vel;
    state[2]=acc;
    state[3]= jerk;
}


void FourthOrderFilter::recompMatrices()
{
    A<<     0, 1, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1,
            -1/pow(tau,4), -4/pow(tau,3), -6/pow(tau,2), -4/tau;

    B<<     0,
            0,
            0,
            1/pow(tau,4);
    exactDiscretization();

}

void FourthOrderFilter::setTimeConstant(double timeConst, double upperBound, double lowerBound)
{

    if(timeConst<lowerBound)
    {

        tau=lowerBound;
    }
    else if (timeConst>upperBound)
    {
        tau=upperBound;

    }
    else if(timeConst>=lowerBound && timeConst<=upperBound)
    {
        tau=timeConst;
    }
    recompMatrices();
}


void FourthOrderFilter::exactDiscretization()
{
    // compute the exact discretization of the A matrix
    // multiply with the sampling time
    Ad = (A*ts).exp();

    // compute the exact discretization of the B matrix
    // define the identity matrix
    int A_size = A.rows();
    MatrixXd Idn(A_size,A_size);
    Idn = Matrix4d::Identity();

    // compute the discretized version
    Bd = A.inverse()*(Ad - Idn)*B;
}
