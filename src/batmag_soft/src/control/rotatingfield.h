#pragma once
#include <boost/date_time/posix_time/posix_time.hpp>

#include "ros/ros.h"
#include "controller.h"
#include "../typeHeader.h"
#include "batmag_soft/RotatingCtrlSettings.h"
#include <math.h>
#include <Eigen/Dense>

#define PI 3.14159265

using namespace Eigen;
using std::vector;
using std::string;


class RotatingField: public Controller
{
public:
    RotatingField(agentType agent);
    RotatingField(agentType agent, vector<double> values);
    virtual ~RotatingField();

    Vector3d evaluate(const Vector3d&, const Vector3d&);
    std::vector<double>  evaluate();

    bool getInitInterfaceParameters(vector<double> &value, vector<double> &max, vector<double> &min, vector<string> &names);

    virtual inline void setFrequency(double value){frequency=value;}
    virtual inline void setAmplitudeRotField(double value){amplitudeRotField=value;}
    virtual inline void setAmplitudeFixedField(double value){amplitudeFixedField=value;}
    virtual inline void setClockwise(double value){clockwise=value;}
    ros::NodeHandle n;
    ros::Subscriber getRotFieldSettings;
protected:

    virtual void setInitInterfaceParameters(const vector<double>&);
    virtual void resetTimer();

private:

    //Variables
    double frequency, amplitudeRotField, amplitudeFixedField;
    bool paramInit;
    Vector3d unRotF, rotF;
    bool clockwise;

    //Rotating Function
    Vector3d normDiff,normField,diff, ufield, vProj, vRej, cProd, u,v,w;
    double r,t,p;
    Matrix3d matR, matF,matG;

    // Time
    boost::posix_time::ptime nowT, initT;
    boost::posix_time::time_duration diffT;

    double secTime;


    //Functions
    Vector3d rotateField(const Vector3d& pos, const  Vector3d& ref, const Vector3d& unRotField);



};

