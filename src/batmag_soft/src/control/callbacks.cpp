#include "callbacks.h"

Callbacks::Callbacks(std::shared_ptr<bool> prS): startpressed(prS)
{

}

Callbacks::~Callbacks()
{

}

void Callbacks::pressedStart(const batmag_soft::StartStop::ConstPtr& msg)
{
    *startpressed=msg->pressedStart;

    ROS_INFO_STREAM("I got start was pressed");
}
