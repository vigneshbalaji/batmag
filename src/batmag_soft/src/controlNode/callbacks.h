#pragma once

#include "batmag_soft/GetInitParameters.h"
#include "batmag_soft/SetInitParameters.h"
#include "batmag_soft/StartStop.h"
#include "ros/ros.h"

class Callbacks
{
public:
    Callbacks(std::shared_ptr<bool>);
    virtual ~Callbacks();

    void pressedStart(const batmag_soft::StartStop::ConstPtr& msg);

private:
    std::shared_ptr<bool> startpressed;

};
