#pragma once

#include <string>

using std::string;

enum ctrlType{
    PID3Dtype,
    PPCtype,
    Errortype
};

enum agentType{
    microparticle,
    microjet,
    hydroGripper,
    metallicGripper
};

agentType setUseAgent(string selectedAgent);
