#pragma once

#include <vector>
#include <string>

#include "ros/ros.h"
#include "batmag_soft/GetInitParameters.h"
#include "batmag_soft/SetInitParameters.h"
#include "typeHeader.h"
#include "pid3d.h"

//------------------------------------------------------------------------------------//
//----IF YOU CREATE A NEW CONTROLLER OR AGENT YOU MUST ADD IT HERE AND IN setUseCtrl--//
//-----------The enumerator selects possible controllers------------------------------//
//------------------------------------------------------------------------------------//

using std::string;
using std::vector;

//--------------------------------------------------------------//
//--------------------------------------------------------------//

class ControlInit
{
public:
    ControlInit();
    virtual ~ControlInit();

    /**
     * @brief getInterfaceParameters This function returns the deafult parameters for a certain control
     * @param selectedCtrl Ctrl selected by the user
     * @param selectedAgent
     * @param value
     * @param max
     * @param min
     * @param names
     * @return
     */
    virtual bool getInterfaceParameters(string selectedCtrl, string selectedAgent, vector<double> &value, vector<double> &max, vector<double> &min, vector<string> &names);

    //This function sets the parameters to the ones set by the user in the GUI, returns true if successful
    virtual bool setInterfaceParameter(const vector<double> &value);

    virtual bool processGetMessage(batmag_soft::GetInitParameters::Request  &req,
                                batmag_soft::GetInitParameters::Response &res);

    virtual bool processSetMessage(batmag_soft::SetInitParameters::Request  &req,
                                batmag_soft::SetInitParameters::Response &res);

    boost::shared_ptr<Controller> InitializeController();


protected:

    boost::shared_ptr<Controller> selectCtrl(bool startPressed);

    ctrlType useCtrl;
    agentType useAgent;

    ctrlType setUseCtrl(string selectedCtrl);
//    agentType setUseAgent(string selectedAgent);

    vector<double> ctrlParameters;

};
