
/*
 * Author: Stefano Scheggi
 * @2016
 */

#pragma once

#ifdef _WIN32
#include <windows.h> // for Sleep only
#else
#include <unistd.h> // for usleep obly
#endif

const int BLUR_SIZE = 9;
const int THRESHOLD_NOTUPSIDEDOWN = 25;
const int THRESHOLD_UPSIDEDOWN = 100;
const int THRESHOLD_MASK = 128;
const int line_length = 7;
const int line_thickness = 2;
const int SMALL_CONTOUR_THRESHOLD  = 30;

// Tracker interface
#include "tracker.h"

using namespace std;
using namespace cv;

class TrackerMetallicCamera:public Tracker
{
    // computation time
    boost::posix_time::ptime nowTime, prevTime;
    boost::posix_time::time_duration diffTime;
   
	vector<Point4f> mCenters;

	bool mUpsideDown; // true if facing upward (light brown), false if facing downward (dark brown)
	bool mFacingChecked; // true if facecing-dierection is checked, maintain that state for the duration of the program

public:
    TrackerMetallicCamera();
    virtual ~TrackerMetallicCamera();
    
    inline vector<Point4f> getCenters(void) const { return mCenters; };
    bool processFrame(const Mat&, Mat&, Point2f&) override;
};
