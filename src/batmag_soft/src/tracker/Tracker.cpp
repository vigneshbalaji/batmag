/*
* Author: Stefano Scheggi
* @2016
*
* This is the interface for the trackers
* When adding a new tracker, inheritage from this class and implement
* the method processFrame
*/

#include "Tracker.h"

Tracker::Tracker() : m_bProcessStarted(false)
{}

Tracker::Tracker(const Tracker& other)
{
    m_DetectRoi = other.m_DetectRoi;
    m_ImgRoi = other.m_ImgRoi;
    m_bProcessStarted = other.m_bProcessStarted;
}

Tracker& Tracker::operator=(const Tracker& other)
{
    m_DetectRoi = other.m_DetectRoi;
    m_ImgRoi = other.m_ImgRoi;
    m_bProcessStarted = other.m_bProcessStarted;
    
    return *this;
}

void Tracker::checkROI(const int w, const int h, cv::Rect& roi)
{
    // Horizontal check
    roi.x = (roi.x < 0) ? 0 : roi.x;
    roi.x = ((roi.x + roi.width) >= w) ? w - roi.width : roi.x;
    
    // Vertical Check
    roi.y = (roi.y < 0) ? 0 : roi.y;
    roi.y = ((roi.y + roi.height) >= h) ? h - roi.height : roi.y;
}
