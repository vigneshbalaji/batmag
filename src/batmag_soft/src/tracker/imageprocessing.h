#pragma once

#include "../typeHeader.h"
#include <mutex>

//ROS
#include "ros/duration.h"
#include "ros/spinner.h"
#include "ros/ros.h"

//To bind callbacks and parameters
#include <boost/function.hpp>
#include <boost/bind.hpp>
#include <boost/signals2.hpp>

//For Image massages conversion
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>


#include "batmag_soft/Reference.h"
#include "batmag_soft/CameraClick.h"
#include "batmag_soft/State.h"
#include "batmag_soft/WriteText.h"
#include "batmag_soft/USroi.h"
#include "batmag_soft/GUIpreferences.h"
#include "geometry_msgs/Twist.h"


//Triangulation
#include "Triangulation/Triangulation.h"

//Tracker kinds
#include "ColorTracker/TrackerColorMetalGripper.h"
#include "ColorTracker/TrackerColorHydrogelGripper.h"
#include "ColorTracker/TrackerColorUS.h"
#include "ColorTracker/TrackerColorGeneral.h"
#include "ColorTracker/trackercolorpm1mm.h"
#include "ColorTracker/trackercolorcircclip.h"

//Time library
#include <boost/date_time/posix_time/posix_time.hpp>

using cv::Point3f;
using cv::Point3d;
using cv::Point2f;
using cv::Mat;

//Optic stuff
const int kGUIframerate=25;
const double kGUIImageScale=799.0/2048.0;
const unsigned int kImageSize=2048;
const double kSensorSize=0.0055;//size of a camera sensor in mm. Pixel size at zoom 1x

//Tracker parameters

const int camera_image_roi_size=500;
const int camera_image_detection_size=170;

const int us_image_roi_size=200;
const int us_image_detection_size=150;

//Number of Agents

const int kNumOfAgents=1;

//reference line parameters
int const ref_line_length=20;
int const ref_line_thickness=2;
const cv::Scalar ref_line_color(0,0,255);//RED

enum imageType
{
    US,
    camera
};

class ImageProcessing
{
public:
    ImageProcessing(ros::NodeHandle,agentType, imageType [], double selectedZoom, bool usOnly=false);
    virtual ~ImageProcessing();

    cv::Mat cameraImage[2], trackerOutputImage[2], processingImage[2], scaledImage[2];


protected:
    void on_newImage(int);

    //US stuff
    unsigned int usHeight=537, usWidth=570, usX=413, usY=56;
    double usDepth=4.5; //Chosen depth on the US machine
    double usXpx2mm, usYpx2mm;
    void setUSpixelSize();


    Point3d pxRef[kNumOfAgents], mmReference[kNumOfAgents];
    Point2f camPxRef[2][kNumOfAgents];//Has to be a float

    std::shared_ptr<Tracker> trackerCamera[2][kNumOfAgents];

    std::mutex mutex, usMutex;

    double zoom;

    double imageScaling[2]={kGUIImageScale, kGUIImageScale}; //Default is the scaling for cameras
    double xOffset[2]={0,0}, yOffset[2]={0,0};
    //Functions

    double pixelsTomm(const double &pixelPos);
    double mmTopixels(const double & mmPos);
    double pixelsTommUS(const double & pixelPos, bool isXaxis);

    //ImageType

    imageType mImageType[2];
    bool us2D;

    //ros handle
    ros::NodeHandle n;
    ros::AsyncSpinner myAsyncSpinner;

    ros::Subscriber cameraClickSub, usRoiSub;
    ros::Publisher statePub, referencePub;


    Triangulation mTriangulator;

    //Writing stuff

    ros::Publisher writePub;
    batmag_soft::WriteText myTextReq;

    //    Messages

    batmag_soft::State mState;

    batmag_soft::Reference refMsg;

    //Image transport parameters
    image_transport::ImageTransport it;
    image_transport::Subscriber imageCamSub[2];
    image_transport::Publisher image_pub_cam0;
    image_transport::Publisher image_pub_cam1;


    //Callbacks

    void clickCb(const batmag_soft::CameraClickConstPtr &msg);
    void imageCb0(const sensor_msgs::ImageConstPtr& msg);
    void imageCb1(const sensor_msgs::ImageConstPtr& msg);
    void imageMessageCallback(const sensor_msgs::ImageConstPtr &msg, int cameraNum);
    void usRoiCb(const batmag_soft::USroi::ConstPtr& msg);
    void guiPrefCb(const batmag_soft::GUIpreferences::ConstPtr & msg);

    //    Says if the z coordinate is taken from camera 0 or camera 1
    bool camera0Priority= true;

    //Stores recorded position for cameras (Has to be a float)
    Point2f camPosition[2][kNumOfAgents];


    boost::signals2::signal<void (int nc)> newImageSig;
    std::shared_ptr<Tracker> selectTracker(agentType agent, imageType imaging);

    // Time to avoid framerates times for both cameras
    boost::posix_time::ptime nowT[2], prevT[2];
    //The could be just one diff but then it'd need a mutex
    boost::posix_time::time_duration diff[2];
    double sampleTime[2];

    //Draw Reference Function
    void drawReference(const cv::Mat&, cv::Mat&, cv::Point2f&, cv::Scalar color=ref_line_color);


    //OMEGA Stuff

    ros::Subscriber omegaSub, guiPrefSub;

    double omegaScale=10;

    bool omegaCtrl;

    void omegaMsgCb(const geometry_msgs::Twist::ConstPtr &msg);

};
