#include "imageprocessing.h"


ImageProcessing::ImageProcessing(ros::NodeHandle nh, agentType agent, imageType typesOfImages[], double selectedZoom, bool usOnly): n(nh), it(nh), myAsyncSpinner(0), zoom(selectedZoom), us2D(usOnly)
{

    mImageType[0]=typesOfImages[0];
    mImageType[1]=typesOfImages[1];
    //Here you have to include a subscription to the configuration message
    //Depending on the kind of agent

    setUSpixelSize();

    // Create a signal to avoid blocking callbacks
    newImageSig.connect(boost::bind(&ImageProcessing::on_newImage, this, _1));
    //    ROS_DEBUG_STREAM("Slot to signal connection returned "<<res);

    //TODO add possibility to also select imaging type (US or camera)5
    for(int i=0; i<2; ++i)
        for(int k=0; k<kNumOfAgents; ++k)
            trackerCamera[i][k]=selectTracker(agent, typesOfImages[i]);

    trackerCamera[0][0]->setId(0);
    trackerCamera[1][0]->setId(1);
    mTriangulator.setZoom(zoom);

    //Create a writing publisher
    writePub= n.advertise<batmag_soft::WriteText>("write_text",1);

    //Listen to camera messages
    cameraClickSub=n.subscribe("camera_click",1,&ImageProcessing::clickCb,this);
    usRoiSub= n.subscribe<batmag_soft::USroi>("us_roi", 1, &ImageProcessing::usRoiCb, this);

    //To avoid overhead the images are not processed at a fixed frequency but as soon as they arrive
    //Hence the frequency of the tracker is the same as the cameras'
    if(typesOfImages[0]==imageType::US)
    {
        imageCamSub[0]=it.subscribe("us_image", 1, &ImageProcessing::imageCb0, this);
        ROS_WARN_STREAM("Default US mode selected on 0");
    }
    else
        imageCamSub[0]=it.subscribe("camera0", 1, &ImageProcessing::imageCb0, this);
    if(typesOfImages[1]==imageType::US)
    {
        imageCamSub[1]=it.subscribe("us_image", 1, &ImageProcessing::imageCb1, this);

        ROS_WARN_STREAM("Default US mode selected on 1");
    }
    else
        imageCamSub[1]=it.subscribe("camera1", 1, &ImageProcessing::imageCb1, this);

    //Publish references
    referencePub=n.advertise<batmag_soft::Reference>("reference",1);
    statePub=n.advertise<batmag_soft::State>("state",1);

    //Omega subscriber
    omegaSub=n.subscribe<geometry_msgs::Twist>("/microgripper/reference",1,&ImageProcessing::omegaMsgCb, this);
    guiPrefSub=n.subscribe<batmag_soft::GUIpreferences>("gui_preferences",1,&ImageProcessing::guiPrefCb, this);


    // Publish output video feed
    image_pub_cam0 = it.advertise("trackerOut0", 1);
    image_pub_cam1 = it.advertise("trackerOut1", 1);

    for(int k=0; k<kNumOfAgents; ++k)
    {
        camPxRef[0][k]={0,0};camPxRef[1][k]={0,0};
        pxRef[k]={-1, -1, -1};
        mmReference[k]={-1, -1, -1};
    }

    // initialize time
    for(int i=0; i<2;++i)
    {
        nowT[i] = boost::posix_time::microsec_clock::local_time();
        prevT[i] = nowT[i];
    }

    myAsyncSpinner.start();

}

ImageProcessing::~ImageProcessing()
{
    for (int k=0; k<kNumOfAgents; ++k)
    {
        trackerCamera[0][k]->~Tracker();
        trackerCamera[1][k]->~Tracker();
    }
    myAsyncSpinner.stop();
}


void ImageProcessing::usRoiCb(const batmag_soft::USroi::ConstPtr& msg)
{
    usMutex.lock();
    usWidth=msg->width;
    usHeight=msg->height;
    usX=msg->topleftx;
    usY=msg->toplefty;
    usMutex.unlock();
}


void ImageProcessing::drawReference(const Mat& inFrame, Mat& outFrame, Point2f& ref, cv::Scalar color)
{
    outFrame=inFrame;
    line(outFrame, cv::Point2d(-ref_line_length + ref.x, ref.y), cv::Point2d(ref_line_length + ref.x, ref.y), color, ref_line_thickness, 8, 0); //Horizontal line of center cross
    line(outFrame, cv::Point2d(ref.x, -ref_line_length + ref.y), cv::Point2d(ref.x, ref_line_length + ref.y), color, ref_line_thickness, 8, 0); //Vertical line of center cross

}

std::shared_ptr<Tracker> ImageProcessing::selectTracker(agentType agent, imageType imaging)
{
    std::shared_ptr<Tracker> myTracker;
    //TODO add the imaging kind
    switch (imaging)
    {
    case camera:

        switch (agent)
        {
        //    case microparticle:
        //        myTracker=std::make_shared<TrackerMicroparticlesCamera>();
        //        break;
        case metallicGripper:
            myTracker=std::make_shared<TrackerColorMetalGripper>();

            myTracker->setImageRoiSize(camera_image_roi_size, camera_image_roi_size);
            myTracker->setDetectionRoiSize(camera_image_detection_size, camera_image_detection_size);
            ROS_INFO_STREAM("Tracker Metallic Grippers started");
            break;

        case hydroGripper://TODO Implement the right tracker here
            myTracker=std::make_shared<TrackerColorHydrogelGripper>();


            myTracker->setImageRoiSize(camera_image_roi_size, camera_image_roi_size);
            myTracker->setDetectionRoiSize(camera_image_detection_size, camera_image_detection_size);
            ROS_INFO_STREAM("Tracker Hydrogel Grippers started");

            break;
            //    case microjet:
            //        myTracker=std::make_shared<TrackerMicrojetsCamera>();
            //        break;

        case sphericalPM1mm:

            myTracker=std::make_shared<TrackerColorPM1mm>();

            myTracker->setImageRoiSize(camera_image_roi_size, camera_image_roi_size);
            myTracker->setDetectionRoiSize(camera_image_detection_size, camera_image_detection_size);
            ROS_INFO_STREAM("PM 1mm Tracker started");
            break;

        case microsphere:

            myTracker=std::make_shared<TrackerColorPM1mm>();

            myTracker->setImageRoiSize(camera_image_roi_size, camera_image_roi_size);
            myTracker->setDetectionRoiSize(camera_image_detection_size, camera_image_detection_size);
            ROS_INFO_STREAM("PM 1mm Tracker started");
            break;

        case circClip:

            myTracker=std::make_shared<TrackerColorCircClip>();

            myTracker->setImageRoiSize(720,720);
            myTracker->setDetectionRoiSize(camera_image_detection_size, camera_image_detection_size);
            ROS_INFO_STREAM("CircClip Tracker started");
            break;



        default:

            myTracker=std::make_shared<TrackerColorGeneral>();

            myTracker->setImageRoiSize(camera_image_roi_size, camera_image_roi_size);
            myTracker->setDetectionRoiSize(camera_image_detection_size, camera_image_detection_size);

            ROS_WARN_STREAM("Default camera tracker selected");
            break;
        }
        break;

    case US:
        switch (agent)
        {
        default:
            myTracker=std::make_shared<TrackerColorUS>();

            myTracker->setImageRoiSize(us_image_roi_size, us_image_roi_size);
            myTracker->setDetectionRoiSize(us_image_detection_size, us_image_detection_size);
        }

        ROS_WARN_STREAM("Default US mode selected");
        break;

    }
    return myTracker;


}

//converts pixels in mm
double ImageProcessing::pixelsTomm(const double & pixelPos)
{
    double mmPos=(pixelPos-kImageSize/2)*kSensorSize/zoom;
    return mmPos;
}

double ImageProcessing::mmTopixels(const double & mmPos)
{
    double pixelPos=mmPos*zoom/kSensorSize+(kImageSize/2);
    return pixelPos;
}

void ImageProcessing::setUSpixelSize()
{
    if(usDepth==4)
    {
        usXpx2mm=1/5.103;
        usYpx2mm=1/6.328;
    }

    if(usDepth==4.5)
    {
        usXpx2mm=1/5.02;
        usYpx2mm=1/6.15;
    }

    if(usDepth==5)
    {
        usXpx2mm=1/4.5;
        usYpx2mm=1/5.55;
    }

    if(usDepth==6)
    {
        usXpx2mm=1/3.756;
        usYpx2mm=1/4.608;
    }

    if(usDepth==7)
    {
        usXpx2mm=1/3.226;
        usYpx2mm=1/4;
    }

    if(usDepth==8)
    {
        usXpx2mm=1/3.014;
        usYpx2mm=1/3.712;
    }

}


double ImageProcessing::pixelsTommUS(const double & pixelPos, bool isXaxis)
{
    double mmPos;
    if(isXaxis)
        mmPos=(pixelPos-usWidth/2)*usXpx2mm;//TODO you might have to add (maxUSwidth/2-usX) or something like it hereb
    else
        mmPos=(usHeight/2-pixelPos)*usYpx2mm;

    return mmPos;
}

//Callback for new images from camera

void ImageProcessing::imageCb0(const sensor_msgs::ImageConstPtr& msg){
    imageMessageCallback(msg, 0);
}

void ImageProcessing::imageCb1(const sensor_msgs::ImageConstPtr& msg){
    imageMessageCallback(msg, 1);
}

void ImageProcessing::imageMessageCallback(const sensor_msgs::ImageConstPtr &msg, int cameraNum)
{
    ROS_DEBUG_STREAM("Receiving image from camera: "<< cameraNum);
    try
    {
        mutex.lock();
        cameraImage[cameraNum]=cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8)->image;
        mutex.unlock();
        //        on_newImage(cameraNum);
        newImageSig(cameraNum);

        //        emit newImage(cameraNum); //I'd love if this worked
        ROS_DEBUG_STREAM("Emmited the signal"<< cameraNum);
    }
    catch (cv_bridge::Exception& e)
    {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }

}

void ImageProcessing::on_newImage(int cameraNum){
    int i=cameraNum;
    ROS_DEBUG_STREAM("Processing the received image. i is "<<i<<"and usOnly is "<< us2D);
    if(cameraImage[i].cols>2 && cameraImage[i].cols>2 && (!us2D || i==0))//Final condition avoids tracking on the camera image when 2D ultrasound tracking is used
    {

        mutex.lock();

        for(int k=0; k<kNumOfAgents; ++k)
        {
            trackerCamera[i][k]->processFrame(cameraImage[i],processingImage[i], camPosition[i][k]);
        }

        //Copy images
        processingImage[0] = cameraImage[0];
        processingImage[1] = cameraImage[1];

        for(int k=0; k<kNumOfAgents; ++k)
        {
            if (k==0)
                trackerCamera[i][k]->drawDetectionRoi(processingImage[i], CV_RGB(0, 255, 0), 5);
            else
                trackerCamera[i][k]->drawDetectionRoi(processingImage[i], CV_RGB(0, 204, 204), 5);

            if (k==0) //Draw a green reference for first agent
                drawReference(processingImage[i],trackerOutputImage[i],camPxRef[i][k], CV_RGB(0, 255, 0));
            else//Draw blue reference for second agent
            {
                drawReference(processingImage[i],trackerOutputImage[i],camPxRef[i][k], CV_RGB(0, 204, 204));
                ROS_DEBUG_STREAM("Writing second reference for camera"<< i);
            }

        }
        ROS_DEBUG_STREAM("Tracker frame processed");


        //        if(camera0Priority)
        //            position={camPosition[0].x, camPosition[1].x, camPosition[0].y};
        //        else
        //            position={camPosition[0].x, camPosition[1].x, camPosition[1].y};


        if(!us2D)
        {
            if(mImageType[0]==camera && mImageType[1]==camera)
            {
                mState.state[0]=pixelsTomm(camPosition[0][0].x);//TODO update state so that all agents are considered
                mState.state[1]=pixelsTomm(camPosition[1][0].x);
                mState.state[2]=(pixelsTomm(kImageSize-camPosition[0][0].y)+pixelsTomm(kImageSize-camPosition[1][0].y))/2;
            }
            else if(mImageType[0]==US)
            {
                usMutex.lock();
                mState.state[0]=pixelsTommUS(camPosition[0][0].x, true);//TODO update state so that all agents are considered
                mState.state[1]=pixelsTommUS(camPosition[0][0].y, false);//TODO update state so that all agents are considered
                mState.state[2]=pixelsTomm(kImageSize-camPosition[1][0].y);
                usMutex.unlock();
            }
            else if(mImageType[1]==US)
            {
                usMutex.lock();
                mState.state[0]=pixelsTommUS(camPosition[1][0].x,true);
                mState.state[1]=pixelsTommUS(camPosition[1][0].y, false);
                mState.state[2]=pixelsTomm(kImageSize-camPosition[0][0].y);
                usMutex.unlock();
            }
            if(kNumOfAgents>1) //Also consider the other agent
            {
                mState.state[3]=pixelsTomm(camPosition[0][1].x);//TODO update state so that all agents are considered
                mState.state[4]=pixelsTomm(camPosition[1][1].x);
                mState.state[5]=pixelsTomm(kImageSize-camPosition[0][1].y);
            }
        }
        else //If it's 2D US tracking the reference for z should be set to zero
            //TODO maybe substitute this with same reference as z position for proper force current mapping
        {
            mState.state[0]=pixelsTommUS(camPosition[0][0].x, true);
            mState.state[1]=pixelsTommUS(camPosition[0][0].y, false);
            mState.state[2]=0;
        }


        //        mTriangulator.linearTriangulation(camPosition[0], camPosition[1], position);

        ROS_DEBUG_STREAM("Image from camera :"<<i<<"\n Current position x: "<<mState.state[0]<<" y: "<<mState.state[1]<<" z: "<<mState.state[2]);


        statePub.publish(mState);

        mutex.unlock();


        // Compute time
        nowT[i] = boost::posix_time::microsec_clock::local_time();
        diff[i] = nowT[i]-prevT[i];
        sampleTime[i] = diff[i].total_milliseconds()*0.001; // seconds!
        if(sampleTime[i]>(1/kGUIframerate))
        {
            prevT[i] = nowT[i];


            //resize the image to avoid useless overhead
            if(mImageType[i]==US) //adapt scaling depending on the dynamically changingin US ROI
            {
                imageScaling[i]=min((double)799.0/trackerOutputImage[i].cols, (double)799.0/trackerOutputImage[i].rows);

                ROS_DEBUG_STREAM(i<<" US scaling is "<< imageScaling[i]);
            }
            resize(trackerOutputImage[i], scaledImage[i],cv::Size(0,0), imageScaling[i],imageScaling[i], cv::INTER_LINEAR); //INTER_NEAREST MIGHT BE LIGHTER


            //Outputting message for the GUI
            cv_bridge::CvImage out_msg;

            out_msg.encoding = sensor_msgs::image_encodings::BGR8; // Or whatever
            out_msg.image    = scaledImage[i]; // Your cv::Mat scaled image

            ROS_DEBUG_STREAM("Time from last message is: "<< sampleTime[i]);

            switch (i){
            case 0:
                mutex.lock();
                image_pub_cam0.publish(out_msg.toImageMsg());
                ROS_DEBUG_STREAM("Sending image from tracker 0.");
                mutex.unlock();
                break;
            case 1:
                mutex.lock();
                image_pub_cam1.publish(out_msg.toImageMsg());
                ROS_DEBUG_STREAM("Sending image from tracker 1");
                mutex.unlock();
                break;
            default:
                ROS_ERROR_STREAM("The selected camera does not exist!");
                break;
            }
        }
    }
    else
    {
        if(!us2D)
            ROS_ERROR_STREAM("The camera images haven't been initialized.");

    }

}

void ImageProcessing::clickCb(const batmag_soft::CameraClickConstPtr &msg)
{

    int x,y;

    ROS_DEBUG_STREAM("Detected camera CallBack on camera"<< msg->camera);

    //If the click is a right one set the position...
    if(msg->button==1 || msg->button==9)
    {
        int agentN=0;//Set agent number to 0 as default
        if(msg->button==9 && kNumOfAgents>1)
            agentN=1;

        ROS_DEBUG_STREAM("Setting initial ROI for x: "<<x<<" and y: "<< y);
        //Set the postion depending on the camera image that was clicked
        if(msg->camera == 0)
        {

            mutex.lock();


            //The user clicked on the scaled image, so the position has to be de-scaled
            x=msg->x/imageScaling[0];
            y=msg->y/imageScaling[0];


            trackerCamera[0][agentN]->setCenter(cv::Point(x,y));
            //            //If camera priority0= true take the z coordinate from cam0
            //            if(camera0Priority)
            //                position={(float)msg->x, position.y, (float)msg->y};

            //            else
            //                //                Else take it form cam1
            //                position={(float)msg->x, position.y, position.z};
            mutex.unlock();
        }
        else
            if(msg->camera ==1)
            {
                mutex.lock();


                //The user clicked on the scaled image, so the position has to be de-scaled
                x=msg->x/imageScaling[1];
                y=msg->y/imageScaling[1];

                trackerCamera[1][agentN]->setCenter(cv::Point(x,y));

                //                if(camera0Priority)
                //                    position={position.x,(float) msg->x, position.z};
                //                else
                //                    position={position.x, (float)msg->x, (float)msg->y};
                mutex.unlock();
            }

        //        Publish a message about the updated position

        //                mutex.lock();
        //                myTextReq.text=(std::string)"Position set to x: "+ std::to_string(position.x)+" y: "+std::to_string(position.y)+" z: "+std::to_string(position.z)+" mm";
        //                writePub.publish(myTextReq);
        //                mutex.unlock();
    }
    else//Set the reference
        if(msg->button==2 || msg->button==8  && !omegaCtrl) //TODO ADD POSSIBLE OTHER BUTTONS
        {
            mutex.lock();
            int agentN=0;//Set agent number to 0 as default
            if(msg->button==8 && kNumOfAgents>1)
                agentN=1;

            //Here the z is always taken from the clicked camera (no matter priority)
            //Also here the user clicked on the scaled image, so the position has to be de-scaled

            if(us2D)//Procedure for 2D Ultrasound tracking
            {
                usMutex.lock();
                if(msg->camera == 0) //Only read meassages on the us image
                {

                    pxRef[agentN]={(float)msg->x/imageScaling[0], (float)msg->y/imageScaling[0],0};
                    camPxRef[0][agentN]={pxRef[agentN].x, pxRef[agentN].y};
                }
                usMutex.unlock();

            }
            else //Standard procedure for cameras
            {
                if(msg->camera == 0)
                {
                    pxRef[agentN]={(float)msg->x/imageScaling[0], pxRef[agentN].y, (float)msg->y/imageScaling[0]};
                }
                else
                    if(msg->camera==1)
                    {
                        pxRef[agentN]={pxRef[agentN].x,(float) msg->x/imageScaling[1], (float) msg->y/imageScaling[1]};
                    }

                camPxRef[0][agentN]={pxRef[agentN].x, pxRef[agentN].z};
                camPxRef[1][agentN]={pxRef[agentN].y, pxRef[agentN].z};
            }
            //The last number of a reference is always a negative number.
            //In this way the receiver knows when the array ends
            mmReference[agentN].x=pixelsTomm(pxRef[agentN].x);
            mmReference[agentN].y=pixelsTomm(pxRef[agentN].y);
            if(!us2D)
                mmReference[agentN].z=pixelsTomm(kImageSize-pxRef[agentN].z);//The Z-axis in the images reference frame and in the workspace reference frame are pointing in opposite directions (180 deg rotation around X)
            else
                mmReference[agentN].z=0;


            //The last number of a reference is always a negative number.
            //In this way the receiver knows when the array ends



            //TODO triangulate the reference


            mutex.unlock();
            refMsg.ref[agentN*3]=mmReference[agentN].x;
            refMsg.ref[agentN*3+1]=mmReference[agentN].y;
            refMsg.ref[agentN*3+2]=mmReference[agentN].z;
            refMsg.ref[kNumOfAgents*3]=-1;//Set final reference to -1 so the system knows the array is over

            mutex.lock();
            referencePub.publish(refMsg);
            mutex.unlock();

            ROS_INFO_STREAM("Current reference x: "<<pxRef[agentN].x<<" y: "<<pxRef[agentN].y<<" z: "<<pxRef[agentN].z<<" px");
            ROS_INFO_STREAM("Current reference x: "<<mmReference[agentN].x<<" y: "<<mmReference[agentN].y<<" z: "<<mmReference[agentN].z<<" mm");

            //            //Publish a message about the updated reference
            //This is nice but seems to make the system crash
            //            mutex.lock();
            //            myTextReq.text=(std::string)"Reference set to x: "+ std::to_string(msg.ref[0])+" y: "+std::to_string(msg.ref[1])+" z: "+std::to_string(msg.ref[2])+" mm";
            //            writePub.publish(myTextReq);
            //            mutex.unlock();
        }
}

void ImageProcessing::omegaMsgCb(const geometry_msgs::Twist::ConstPtr & msg)
{
    if(omegaCtrl)
    {
        refMsg.ref[0]=msg->linear.x*omegaScale;
        refMsg.ref[1]=msg->linear.y*omegaScale;
        refMsg.ref[2]=msg->linear.z*omegaScale;

        refMsg.ref[3]=msg->angular.x*omegaScale;
        refMsg.ref[4]=msg->angular.y*omegaScale;
        refMsg.ref[5]=msg->angular.z*omegaScale;

        camPxRef[0][0]={mmTopixels(refMsg.ref[0]), mmTopixels(refMsg.ref[2])};
        camPxRef[0][1]={mmTopixels(refMsg.ref[1]), mmTopixels(refMsg.ref[2])};
    mutex.lock();
    referencePub.publish(refMsg);
    mutex.unlock();
    }
}

void ImageProcessing::guiPrefCb(const batmag_soft::GUIpreferences::ConstPtr & msg)
{
    ROS_INFO_STREAM("Gui Pref CB");
    omegaCtrl=msg->omegaCtrl;
}

//void Tracker3D::pxTomm(Point3f & pxPos, Point3f & mmPos)
//{
//    mmPos=zoom*pxTomm;
//}
