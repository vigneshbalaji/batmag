
/*
 * Author: Stefano Scheggi, Gert van de Steeg
 * @2016
 */

#include "trackerMicroparticlesCamera.h"
#include <boost/preprocessor.hpp>
#include <boost/timer.hpp>

#include <map>
#include <vector>
#include <algorithm>
#include <fstream>
#include <time.h>

//Enum bwmorph operations
//http://stackoverflow.com/questions/5093460/how-to-convert-an-enum-type-variable-to-a-string
#define X_DEFINE_ENUM_WITH_STRING_CONVERSIONS_TOSTRING_CASE(r, data, elem)				\
    case elem : return BOOST_PP_STRINGIZE(elem);

//Enum to String
#define DEFINE_ENUM_WITH_STRING_CONVERSIONS(name, enumerators)							\
	enum name {																			\
		BOOST_PP_SEQ_ENUM(enumerators)													\
	};																					\
																						\
	inline char* EnumToString(name v) {													\
		switch (v) {																	\
			BOOST_PP_SEQ_FOR_EACH(X_DEFINE_ENUM_WITH_STRING_CONVERSIONS_TOSTRING_CASE,name,enumerators)\
            default: return "[Unknown " BOOST_PP_STRINGIZE(name) "]";					\
		}																				\
	}

DEFINE_ENUM_WITH_STRING_CONVERSIONS(opDefs, (opShrink)(opSpur)(opThin)(opThicken)(opSkeleton)(opClose)(opOpen)(opDilate)(opErode)(opDiag)(opEndpoints)(opBothat)(opBranchpoints)(opBridge)(opClean)(opFatten)(opFill)(opHbreak)(opMajority)(opRemove)(opPerim4)(opPerim8));


TrackerMicroparticlesCamera::TrackerMicroparticlesCamera() : mProcessStarted(false), mFrameCounter(0)
{
    // used to calculate the computation time
    nowTime = boost::posix_time::microsec_clock::local_time();
	prevTime = nowTime;

//	// Vignette removal, due to imadjust()
//	// Read vignette params (obtained from Matlab), it is an image of the estimated vignette
//	FileStorage fsParams("VignetteParams.yml", FileStorage::READ);
//	if (!fsParams.isOpened())
//	{
//		cout << "Failed to open YML file." << endl;
//	}
//	fsParams["vignette_params"] >> this->vignette_params;
//	fsParams.release();

#if USE_MULTITHREADING
	mActualFrame = nullptr;
	mProcessThread = nullptr; 
	mVideoBuffer = nullptr;
#endif
}

TrackerMicroparticlesCamera::~TrackerMicroparticlesCamera()
{
#if USE_MULTITHREADING
	if (mProcessThread != nullptr)
	{
		// how to stop and destroy the thread
		mProcessThread->join();
		cout << "mProcessThread->join()...ok\n";
		delete mProcessThread;
		mProcessThread = nullptr;
		cout << "mProcessThread deleted...ok" << endl;
	}
#endif
}

#if USE_MULTITHREADING
void TrackerMicroparticlesCamera::startThread(void)
{
	// destroy previous thread and create a new one
	if (mProcessThread != nullptr)
	{
		//cout << "mProcessThread deleted...ok" << endl;
		delete mProcessThread;
		mProcessThread = nullptr;
	}

	// create a new one
	//cout << "mProcessThread created...ok" << endl;
	mIsThreadRunning = true;
	mProcessThread = new boost::thread(boost::bind(&TrackerMicroparticlesCamera::processFrameThread, this));
}

void TrackerMicroparticlesCamera::processFrameThread(void)
{
	Mat tmpOutFrame, tmpFrameIn;
	Point2f tmpCenter;
    Point2f tmpCenterDraw;

	mComputationTime.clear();

	nowTime = boost::posix_time::microsec_clock::local_time();
	prevTime = nowTime;

	while (mIsThreadRunning)
	{
		nowTime = boost::posix_time::microsec_clock::local_time();
		diffTime = nowTime - prevTime;

		if (diffTime.total_milliseconds() >= mParamsThread->mSamplingTime) // milliseconds
		{
			// store the computation time for debugging
            if (mParamsThread->mSaveSamplingTime)
            {
                mSamplingTime.push_back(static_cast<double>(diffTime.total_milliseconds()));
            }
            
            //// just for testing, it just visualize the input, uncomment this line and comment the processFrame
            //mProcessMutex.lock();
            //*mOutFrameThread = *(&mVideoBuffer->at(*mActualFrame - mInitialFrame));
            //mProcessMutex.unlock();

			// make sure that we have the requested video
			// since we are loading frames while processing them, it may happen that we 
			// try to process a video frame which is not still loaded, if the tracking sampling time is small
			// in this case just restart teh processing from the first frame
			// if data is saved is better to load teh whole video before and then process
			if ((*mActualFrame - mInitialFrame) >= mVideoBuffer->size())
			{
				*mActualFrame = *(&mInitialFrame);
                // here is possible to add delays which simulate the grabbing time
			}

			// get the video frame from the buffer
            tmpFrameIn = mVideoBuffer->at(*mActualFrame - mInitialFrame).clone();

            // process the frame
			processFrame(tmpFrameIn, tmpOutFrame, *mParamsThread, tmpCenter);
            tmpCenterDraw.x = tmpCenter.x;
            tmpCenterDraw.y = tmpCenter.y;

            // update the shared variables
            mProcessMutex.lock();
			*mOutFrameThread = *(&tmpOutFrame);
            *mOutCenterThread = *(&tmpCenterDraw);
            mProcessMutex.unlock();

			// update the loop time
			prevTime = nowTime;
		}
	}
}
#endif

bool TrackerMicroparticlesCamera::processFrame(const Mat& inFrame, Mat& outFrame, Point2f& outCenter)
{
	//DataTypes http://ninghang.blogspot.nl/2012/11/list-of-mat-type-in-opencv.html
	//Computation time
    prevTime = boost::posix_time::microsec_clock::local_time();

    boost::posix_time::ptime prevProcessTime, nowProcessTime;
    boost::posix_time::time_duration diffProcessTime;

	bool isDetected = false;

	/**************** FILL STUFF FROM HERE ***********************/
    /* --- START PROCESSING --- */
	double min, max;
    cv::Point min_loc, max_loc;
    Rect_<float> ROI = mTrackerParameters.mInitialROI;
    Mat img_rgb, img_gray, img_LoG, img_bin, kernel;
    img_rgb = inFrame.clone();

    //Apply ROI
#if USE_ROI
    //Check image bounds, if OutOfBounds, temporarily decrease ROI dimensions for this frame
    if (ROI.x + ROI.width > img_rgb.cols)
    {
        ROI.width = floor(img_rgb.cols - ROI.x);
    }
    else if (ROI.x < 0)
    {
        if (ROI.x + ROI.width / 2 <= 0){
            ROI.width = 1;
        }
        else{
            ROI.width = 2 * floor(ROI.x + ROI.width / 2);
        }
        ROI.x = 0;
    }

    if (ROI.y + ROI.height > img_rgb.rows)
    {
        ROI.height = floor(img_rgb.rows - ROI.y);
    }
    else if (ROI.y < 0)
    {
        if (ROI.y + ROI.height / 2 <= 0){
            ROI.height = 1;
        }
        else{
            ROI.height = 2 * floor(ROI.y + ROI.height / 2);
        }
        ROI.y = 0;
    }
    img_gray = img_rgb(ROI); //NOTE img_gray is still a color image here (converted to gray later)
#endif

    //Get ROI location with respect to larger/parent image
    Point offset;
    Size wholesize;
    img_gray.locateROI(wholesize, offset);

    //Convert to double grayscale image
    cv::cvtColor(img_gray, img_gray, CV_RGB2GRAY);

    //Contrast-scale adjust (in Matlab: img_gray-min(img_gray(:)))./max(img_gray(:)-min(img_gray(:)))
    img_gray.convertTo(img_gray, CV_32F);
    scale_matrix(img_gray, img_gray);

    //Laplacian of Gaussian (LoG) filter
	//(Gradual) Edge detector
    kernel = LOGkernel(KERNEL_SIZE_LOG, KERNEL_SIGMA_LOG); //Calc LoG filter kernel
    cv::filter2D(img_gray, img_LoG, -1, kernel, Point(-1, -1), 0, BORDER_DEFAULT);

#if PLOT_DEBUG == 1 //Scale to make visible in figure
        scale_matrix(img_LoG, img_LoG);
		imshow("LoG", img_LoG);
#endif

    //Threshold
    //minMaxLoc(img_LoG, &min, &max, &min_loc, &max_loc);
    minMaxLoc(img_LoG, &min, &max);
    img_bin = img_LoG > max*0.7; //70% of the max level

#if PLOT_DEBUG == 1
        imshow("img_bin", img_bin);
#endif

    //Get contours
    vector<vector<Point>> contours;
    findContours(img_bin/*.clone()*/, contours, {}, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE, Point(0, 0)); //Connected components

    vector<Point2f>centroids(contours.size());
    vector<float>radius(contours.size());
    for( unsigned int i = 0; i < contours.size(); i++ ){
         minEnclosingCircle(contours[i], centroids[i], radius[i]);
    }

    //Calculate distances from last known jet coordinate
    Point2d last_coord;
    vector<double> distances(centroids.size());
    if (mTrackerParameters.mPreviousCenter == Point2f(-1, -1)){
        last_coord = Point2d(ROI.x + ROI_WIDTH / 2, ROI.y + ROI_HEIGHT / 2);
    }
    else{
        last_coord = mTrackerParameters.mPreviousCenter;
    }

    for (unsigned int i = 0; i < centroids.size(); i++){
        distances[i] = sqrt(pow(last_coord.x - (centroids[i].x + offset.x), 2) + pow(last_coord.y - (centroids[i].y + offset.y), 2));
    }

    //Pick final particle centroid and set new ROI location
    if (!distances.empty()){//not empty == at least one particle found, else if no particles found, use 'old' data for next frame (do not update any value), coordinate will remain stationary
        //Get minimum
        minMaxLoc(distances, &min, &max, &min_loc, &max_loc);

        //Update coordinates to global
        mTrackerParameters.mPreviousCenter = Point2d(centroids[min_loc.x].x + offset.x, centroids[min_loc.x].y + offset.y);
        mTrackerParameters.mInitialROI = Rect_<double>(mTrackerParameters.mPreviousCenter.x - ROI_WIDTH / 2, mTrackerParameters.mPreviousCenter.y - ROI_HEIGHT / 2, ROI_WIDTH, ROI_HEIGHT);

        particleRadius = radius[min_loc.x];

        isDetected = true;
        }
    else{
//        cout << "Empty" << endl;
    }

	/* --- END PROCESSING --- */

    /* --- START PLOT --- */
//    Scalar line_color(0, 255, 0);
	//Final particle centroid (in green)
    line(img_rgb, Point2d(-LINE_LENGTH + mTrackerParameters.mPreviousCenter.x, mTrackerParameters.mPreviousCenter.y), Point2d(LINE_LENGTH + mTrackerParameters.mPreviousCenter.x, mTrackerParameters.mPreviousCenter.y), line_color, LINE_THICKNESS, 8, 0); //Horizontal line of center cross
    line(img_rgb, Point2d(mTrackerParameters.mPreviousCenter.x, -LINE_LENGTH + mTrackerParameters.mPreviousCenter.y), Point2d(mTrackerParameters.mPreviousCenter.x, LINE_LENGTH + mTrackerParameters.mPreviousCenter.y), line_color, LINE_THICKNESS, 8, 0); //Vertical line of center cross

    /* --- END PLOT --- */

	//Out
    //scale_mat(img_LoG, img_LoG);
	//cout << (int)img_bin_log_thin.type() << endl;
	outFrame = img_rgb;
    outCenter = Point2f((float)mTrackerParameters.mPreviousCenter.x, (float)mTrackerParameters.mPreviousCenter.y);

    /**************** TO HERE ***********************/
        
    //Computation time
    nowTime = boost::posix_time::microsec_clock::local_time();
    diffTime = nowTime - prevTime;
    
#if SHOW_COMPUTATION_TIME
    cout << "processFrame: " << diffTime.total_milliseconds() << " ms" << endl;
#endif
    
    //Increase frame counter for buffered vectors mCenters and mComputationTime
    mFrameCounter++;
    
    //Boolean check variable
    mProcessStarted = true;
    
    return isDetected;
}
