
/*
 * Authors: Stefano Scheggi, Gert van de Steeg
 * @2016
 */

#pragma once

#ifdef _WIN32
#include <windows.h> // for Sleep only
#else
#include <unistd.h> // for usleep obly
#endif

// defines
//For drawing figures nicely
#define DRAW_COL_START 20
#define DRAW_ROW_START 20
#define DRAW_COL_MARGIN 40
#define DRAW_ROW_MARGIN 50
#define DRAW_COL_0 DRAW_COL_START
#define DRAW_COL_1 DRAW_COL_0 + 1024 + DRAW_COL_MARGIN //Consider image width
#define DRAW_COL_2 DRAW_COL_1 + ROI_WIDTH + DRAW_COL_MARGIN
#define DRAW_COL_3 DRAW_COL_2 + ROI_WIDTH + DRAW_COL_MARGIN
#define DRAW_ROW_1 DRAW_ROW_START
#define DRAW_ROW_2 DRAW_ROW_1 + ROI_HEIGHT + DRAW_ROW_MARGIN
#define DRAW_ROW_3 DRAW_ROW_2 + ROI_HEIGHT + DRAW_ROW_MARGIN
#define DRAW_ROW_4 DRAW_ROW_3 + ROI_HEIGHT + DRAW_ROW_MARGIN
#define DRAW_ROW_5 DRAW_ROW_4 + ROI_HEIGHT + DRAW_ROW_MARGIN

#define MAX_JET_LENGTH_MORPH 38 //Maximum length of a jet (for the morph method); experimetally determined
#define MAX_JET_LENGTH_BLOB 50 //Maximum length of a jet (for the blob method); experimetally determined
#define MAX_CENTER_JUMP MAX_JET_LENGTH_BLOB / 2 //Maximum difference in center-coordinate-distance (in pixels) between frames; half of the max jet length; experimentally / guesstimate
#define ECCENTRICITY_THRESHOLD 0.95 //0=circle, 1=line; experimetally determined

// some definition for the processFrame
#define VIGNETTE_FILE "../Tracker/VignetteParams.yml"
#define VIGNETTE_SHIFT 1
#define KERNEL_SIZE_LOG 40
#define KERNEL_SIGMA_LOG 5
#define PLOT_DEBUG 0
#define USE_ROI 1
#define LINE_LENGTH 7
#define LINE_THICKNESS 2
#define MIN_COUNTOUR_SIZE 10
#define DRAW_ALL_BLOBS 0
#define DRAW_ORIENTATION 1
#define ORIENTATION_LINE_LENGTH 20
#define DRAW_CIRCLE_PATH 1 // 21.10.2016 Added by Alper Denasi to draw the reference circular path

// Tracker interface
#include "tracker.h"

// basic stuff
#include <map>
#include <algorithm>

// boost
#include <boost/preprocessor.hpp>
#include <boost/timer.hpp>

// Utility functions
#include "utilityComputerVisionFunction.h"

//// For angle unwrap
//#include "../ppc.h"

using namespace std;
using namespace cv;

class TrackerMicrojetsCamera:public Tracker
{
    // computation time
    boost::posix_time::ptime nowComputationTime, prevComputationTime;
	boost::posix_time::time_duration diffComputationTime;
	vector<Point2f> mCenters;

	//// This is only for DEBUG
	//vector<size_t> mNumBlobs;
	//vector<Point2f> mCenterBlobs;
	//vector<double> mOrientation;
//    PPC ppc; // used only for angle unwrap, put that function outside that class!!
	double mNextOrientation;
	double mActualOrientation; // unwrapped
	double mPreviousOrientationTracker; // -PI and PI radians
    double mPreviousOrientation; // unwrapped previous orientation
	bool mInitializeUnwrap;
    double mDrawCircleRadius;
    double mDrawCircleCenterX;
    double mDrawCircleCenterY;

	// Matlab Look Up Table (LUT) indicating the 3x3 structering elements (strel) used for spur detection
	Mat LUT_spur = (Mat_<float>(1, 512) << 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

	// Weigths kernel to determine which strels were present in the image
	Mat strel_weights = (Mat_<int>(3, 3) << 1, 8, 64, 2, 16, 128, 4, 32, 256); //Already rotated by 180 degrees, because filter2D does corr() instead of conv()

	// Vignette removal, due to imadjust()
	// Read vignette params (obtained from Matlab), it is an image of the estimated vignette
	Mat vignette_params;

	void computeBlobOrientation(vector<vector<Point> >&, vector<double>&, vector<double>&, vector<double>&, vector<double>&);
	void removeShapeBasedOnEccentricity(vector<vector<Point> >&, vector<double>&, vector<double>&, vector<double>&, vector<Scalar>&);
	void removeShapeBasedOnEccentricity(vector<vector<Point> >&, vector<double>&, vector<double>&, vector<double>&);
	
	void mapOrientationBetweenPiMinusPi(double&, double&, double&);
	void unwrapOrientation(double&, double&, double&);

public:
    TrackerMicrojetsCamera();
    ~TrackerMicrojetsCamera();

	inline Mat get_strel_weights(){ return strel_weights; }
	inline Mat get_LUT_spur(){ return LUT_spur; }
	inline Mat get_vignette_params(){ return vignette_params; }
	inline double getOrientation(){ return mActualOrientation; }

    inline void resetOrientation(){ mPreviousOrientation = 0; mPreviousOrientationTracker = 0; mActualOrientation = 0; mInitializeUnwrap = true;}

        bool processFrame(const Mat&, Mat&, Point2f&) override;
	bool saveNumBlobsToFile(const char*) const;
	void invertOrientation(void);
};
