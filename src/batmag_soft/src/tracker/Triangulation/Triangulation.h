/*
* Triangulation functions
* For now it considers only 2 cameras
*
* Author: Stefano Scheggi@2017
*/

#pragma once

#include <opencv2/opencv.hpp>

class Triangulation
{
    cv::Matx44d m_mat4Rt= {    0,   0,   1,     focalDistance,
                               0,   1,   0,     0,
                               -1,   0,  0,     0,
                               0,   0,   0,     1}; // homogeneous matrix between first and second camera
    cv::Matx44d m_matRtInv; // inverse

    // calib mat 0 - TODO update with real values
    cv::Matx44d m_mat4Calib0={focalDistance, 0,              1024,    0,
                             0,              focalDistance,  1024,    0,
                             0,              0,              1,       0,
                             0,              0,              0,       1},
    // calib mat 1 - TODO update with real values
    m_mat4Calib1={   focalDistance,       0,               1024,  0,
                     0,                   focalDistance,   1024,  0,
                     0,                   0,               1,     0,
                     0,                   0,               0,     1};

    cv::Matx44d m_mat4CamToSetup={ 1,   0,   0,     focalDistance,
                                   0,   0,   -1,     0,
                                   0,   1,   0,     0,
                                   0,   0,   0,     1};


private:

    double focalDistance=190;
    double zoom=1;
    double const  kCamPixelSize=0.0055; //mm size of pixel in the camera sensor
    double pixelSize=0.0055; //mm size of pixel in the image

    
public:
    Triangulation();

    void setCalibMtx0(cv::Matx44d value){ m_mat4Calib0 = value; }

    void setCalibMtx1(cv::Matx44d value){ m_mat4Calib1 = value; }

    void setRtMatx(cv::Matx44d value){ m_mat4Rt = value; m_matRtInv = m_mat4Rt.inv(); }

    inline void toSetupFrame(cv::Matx41d & point){point=m_mat4CamToSetup*point;}
    //Set the actual camera zoom
    void setZoom(double z);

    //Update matrices when parameters are changed
    void recomputeMatrices();

//    actual Triangulation function
    void linearTriangulation(const cv::Point2f&, const cv::Point2f&, cv::Point3d&);
};
