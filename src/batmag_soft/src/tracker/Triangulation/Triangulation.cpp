/*
* Triangulation function
*
* Author: Stefano Scheggi@2017
*/

#include "Triangulation.h"

Triangulation::Triangulation()
{
    m_mat4Rt = cv::Matx44d::eye();
    m_mat4Calib0 = cv::Matx44d::eye();
    m_mat4Calib1 = cv::Matx44d::eye();
}

void Triangulation::linearTriangulation(const cv::Point2f& pntIn0,
                                        const cv::Point2f& pntIn1,
                                        cv::Point3d& pntOut)
{
    /* -- 3d Structure computation -- Linear triangulation method
     *  (Sect.12.2 in Zisserman I)
     *
     * From projection matrices and point in the image plane obtain
     * the reconstruction of 3D environment.
     * "Linear Triangulation method" in
     */

    
    cv::Matx44d PM0 = m_mat4Calib0;
    cv::Matx44d PM1 = m_mat4Calib1 * m_matRtInv;
    
    cv::Matx14d p0_0 = PM0.row(0);
    cv::Matx14d p0_1 = PM0.row(1);
    cv::Matx14d p0_2 = PM0.row(2);
    
    cv::Matx14d p1_0 = PM1.row(0);
    cv::Matx14d p1_1 = PM1.row(1);
    cv::Matx14d p1_2 = PM1.row(2);
    
    // These are in pixels, because we are using the rows of P and Pp,
    // which contain K1 and K2.
    double x0 = static_cast<double>(pntIn0.x);
    double y0 = static_cast<double>(pntIn0.y);
    double x1 = static_cast<double>(pntIn1.x);
    double y1 = static_cast<double>(pntIn1.y);
    
    cv::Matx14d row1 = x0*p0_2 - p0_0;
    cv::Matx14d row2 = y0*p0_2 - p0_1;
    cv::Matx14d row3 = x1*p1_2 - p1_0;
    cv::Matx14d row4 = y1*p1_2 - p1_1;
    
    cv::Matx44d A;
    A(0, 0) = row1(0); A(0, 1) = row1(1); A(0, 2) = row1(2); A(0, 3) = row1(3);
    A(1, 0) = row2(0); A(1, 1) = row2(1); A(1, 2) = row2(2); A(1, 3) = row2(3);
    A(2, 0) = row3(0); A(2, 1) = row3(1); A(2, 2) = row3(2); A(2, 3) = row3(3);
    A(3, 0) = row4(0); A(3, 1) = row4(1); A(3, 2) = row4(2); A(3, 3) = row4(3);
    
    // use cv Mat for svd
    cv::Matx44d AA = A.t()*A;
    cv::Mat AAmat = cv::Mat(AA);
    cv::Mat S, U, V, TV;
    cv::SVDecomp(AAmat, S, U, V);

    TV=V.t(); // V is transposed

    cv::Matx41d pnt3D = TV.col(3);

    // normalize points
    pnt3D(0)= pnt3D(0)/pnt3D(3);
    pnt3D(1)= pnt3D(1)/pnt3D(3);
    pnt3D(2)= pnt3D(2)/pnt3D(3);
    pnt3D(3)= pnt3D(3)/pnt3D(3);

    //convert them to setup frame
    toSetupFrame(pnt3D);

    //spit them out
    pntOut.x = pnt3D(0);
    pntOut.y = pnt3D(1);
    pntOut.z = pnt3D(2);

}


void Triangulation::setZoom(double z)
{
    zoom=z;
    pixelSize=kCamPixelSize/zoom;//mm per pixel
    recomputeMatrices();
}


void Triangulation::recomputeMatrices()
{
    m_mat4Rt= {    0,   0,   1,     focalDistance/pixelSize,//#focal distance in pixels
                               0,   1,   0,     0,
                               -1,   0,  0,     focalDistance/pixelSize,
                               0,   0,   0,     1}; // homogeneous matrix between first and second camera

    m_matRtInv = m_mat4Rt.inv();

    // calib mat 0 - Update changing the pixel size!
    m_mat4Calib0={focalDistance/pixelSize,   0,                        1024,    0,
                  0,              focalDistance/pixelSize,  1024,    0,
                  0,              0,                          1,     0,
                  0,              0,                          0,       1};
    // calib mat 1 - Update changing the pixel size!
    m_mat4Calib1={   focalDistance/pixelSize,       0,                       1024,  0,
                     0,                           focalDistance/pixelSize,   1024,  0,
                     0,                            0,                        1,     0,
                     0,                            0,                        0,     1};

    m_mat4CamToSetup={ 1,   0,   0,     focalDistance,
                       0,   0,   -1,     0,
                       0,   1,   0,     0,
                       0,   0,   0,     1};

}
