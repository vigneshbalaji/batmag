
/*
 * Author: Stefano Scheggi
 * @2016
 */

#pragma once

#ifdef _WIN32
#include <windows.h> // for Sleep only
#else
#include <unistd.h> // for usleep obly
#endif

// basic stuff
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

// opencv stuff
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

// only to compute time to segment the frame
#include <boost/date_time/posix_time/posix_time.hpp>

// multithreading
#include <boost/thread.hpp>

// include Configuration file
#include "configuration.h"
#include "tracker.h"

using namespace std;
using namespace cv;

//TODO STEFANO: Apparently you have to fix this

class TrackerHydrogelUS: public Tracker
{
    // computation time
    boost::posix_time::ptime nowTime, prevTime;
    boost::posix_time::time_duration diffTime;
    vector<double> mComputationTime; // stores milliseconds used to process each frame
    vector<double> mSamplingTime; // stores milliseconds of the real sampling time at each loop
    
	int mFrameCounter;
    bool mProcessStarted;
    Rect mActualProcessROI;
    KalmanFilter mKF;
    vector<Point4f> mCenters;

    int removeSmallBlobs(const double&, const double&, Mat&);
    void updateROI(const Rect&, const Rect&, const Point2f&, Rect&);
    void KalmanInit(const Point4f&, const double&);
    void KalmanProcess(const Point4f&, Point4f&, Point4f&);
	void elapsedTime(void);

#if USE_MULTITHREADING
	// multithreading for tracking
	bool mIsThreadRunning;
	boost::thread *mProcessThread;
    vector<Mat> *mVideoBuffer;
	Mat mInFrameThread, *mOutFrameThread;
	Point2f *mOutCenterThread;
	int *mActualFrame, mInitialFrame;
	TrackerParameters *mParamsThread;
	void processFrameThread(void);
#endif

public:
    TrackerHydrogelUS();
    ~TrackerHydrogelUS();
    
    inline vector<Point4f> getCenters(void) const { return mCenters; };
	inline void setIsProcessStarted(bool value){ mProcessStarted = value; };
    bool processFrame(const Mat&, Mat&, Point4f&);
    bool saveCentersToFile(const char*) const;
    bool saveComputationTimeToFile(const char*) const;
    bool saveSamplingTimeToFile(const char*) const;

#if USE_MULTITHREADING
    boost::mutex mProcessMutex;
	// multithreading
	void startThread(void);
	inline void stopThread(const bool value){ mIsThreadRunning = !value; };
	// most of these functions link a pointer of this class to some variables 
	// this is used to manage things fast
	inline void setParameters(TrackerParameters &value){ mParamsThread = &value; };
	inline void setOutputFrame(Mat &value){ mOutFrameThread = &value; };
    inline void setFrameToGrab(int &value){ mActualFrame = &value; };
    inline void setVideoBuffer(vector<Mat> &value){ mVideoBuffer = &value; };
	inline void setInitialFrame(int value){ mInitialFrame = value; };
	inline void setTrackedCenter(Point2f &value){ mOutCenterThread = &value; };
#endif
};
