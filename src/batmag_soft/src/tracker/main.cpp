#include <functional>
#include <mutex>

//ROS
#include "ros/duration.h"
#include "ros/spinner.h"
#include "ros/ros.h"

//To bind callbacks and parameters
#include <boost/function.hpp>
#include <boost/bind.hpp>

//For Image massages conversion
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

#include "imageprocessing.h"

//Messages
#include "batmag_soft/StartStop.h"

static std::mutex myMutex;

using boost::shared_ptr;



void pressedStart(const batmag_soft::StartStop::ConstPtr& msg, bool & startPressed, bool & stopPressed, agentType & agent, imageType mImage[], double & zoom, bool & usO)
{
    startPressed=msg->pressedStart;
    stopPressed=msg->pressedStop;
    agent=setUseAgent(msg->agent);
    zoom=msg->zoom;
    usO=msg->usOnly;
    //This has to be said by the gui
    for(int i=0; i<2;++i)
        if(msg->isUS[i])
            mImage[i]=US;
        else
            mImage[i]=camera;
}



int main(int argc, char *argv[])
{
    ROS_INFO_STREAM("The tracking node has started");
    //Create the ros node and the handle
    ros::init(argc, argv, "tracker_node");
    ros::NodeHandle n;

    //Frequency of this node
    const double trackFrequency=25;
    ros::Rate spinRate(trackFrequency);

    agentType mAgent;
    imageType mImageType[2];
    double zoom;
    bool usOnly;

    while(!ros::ok())
        usleep(500);

    //Subscribe to Start/Stop messages
    bool startPressed=false, stopPressed=false;
    ros::Subscriber startStopSub = n.subscribe<batmag_soft::StartStop>("gui_start_stop", 1, std::bind(pressedStart, std::placeholders::_1, std::ref(startPressed), std::ref(stopPressed), std::ref(mAgent), std::ref(mImageType), std::ref(zoom), std::ref(usOnly)));

    while(!startPressed && !stopPressed && ros::ok())
    {
    ros::spinOnce();
    spinRate.sleep();
    }

    ImageProcessing mImageProcessing(n,mAgent,mImageType,zoom, usOnly);

    //Start an asynchronous spinner active on as many cores as the cpu has
    ros::AsyncSpinner myAsyncSpinner(0);

    while(ros::ok() && !stopPressed)
        {
            spinRate.sleep();
        }

    mImageProcessing.~ImageProcessing();

    myAsyncSpinner.stop();


}
