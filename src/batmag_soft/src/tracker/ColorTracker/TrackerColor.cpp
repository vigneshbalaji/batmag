
/*
 * Author: Stefano Scheggi
 * @2017
 * This is the interface for the color trackers
 * When adding a new color tracker, inheritage from this class and implement
 * the method detectColor
 */

#include "TrackerColor.h"

TrackerColor::TrackerColor() : m_iHistogramSize(16),
    m_bUpdateHistogram(false), m_iUpdateHistogramCounter(0),
    m_iUpdateHistogramValue(0), it(n)
{
    m_iHistogramRange[0] = 0;
    m_iHistogramRange[1] = 180;
    
    m_fPtrHistRanges = m_iHistogramRange;
    
    m_DetectRoi = cv::Rect(0, 0, 100, 100);
    m_ImgRoi = cv::Rect(0, 0, 100, 100);


    setSub=n.subscribe<batmag_soft::TrackerSettings> ("tracker_settings",1, &TrackerColor::settingsCb, this);


    //    m_BackSub=cv::createBackgroundSubtractorKNN();



}

bool TrackerColor::processSettingsReq(batmag_soft::getInitTrackerSettings::Request &req,
                                      batmag_soft::getInitTrackerSettings::Response &res)
{
    res.maxH=m_iHueMax;
    res.minH=m_iHueMin;

    res.maxS=m_iSaturationMax;
    res.minS=m_iSaturationMin;

    res.maxV=m_iValueMax;
    res.minV=m_iValueMin;

    res.agentSize=m_DetectRoi.width;
    res.roiSize=m_ImgRoi.width;

    res.blurSize=(int)m_blurSize/2;

    return true;


}

void TrackerColor::setId(int value)
{
    id=value;


    if(id==0)
    {
        roiPub=it.advertise("roi0",1);
        bsPub=it.advertise("bs0",1);
        mServer= n.advertiseService("get_tracker_settings0", &TrackerColor::processSettingsReq, this);
        ROS_INFO_STREAM("Advertising roi0");
    }
    else
        if(id==1)
        {
            roiPub=it.advertise("roi1",1);
            bsPub=it.advertise("bs1",1);
            mServer= n.advertiseService("get_tracker_settings1", &TrackerColor::processSettingsReq, this);
            ROS_INFO_STREAM("Advertising roi1");
        }


    ros::Publisher initPub=n.advertise<batmag_soft::TrackerSettings> ("tracker_settings",1);
    batmag_soft::TrackerSettings initMsg;
    {
        initMsg.maxH=m_iHueMax;
        initMsg.minH=m_iHueMin;

        initMsg.maxS=m_iSaturationMax;
        initMsg.minS=m_iSaturationMin;

        initMsg.maxV=m_iValueMax;
        initMsg.minV=m_iValueMin;

        initMsg.agentSize=m_DetectRoi.width;
        initMsg.roiSize=m_ImgRoi.width;

        initMsg.blurSize=(int)m_blurSize/2;

        initPub.publish(initMsg);
    }

    initPub.shutdown();



}


void TrackerColor::settingsCb(const batmag_soft::TrackerSettings::ConstPtr & msg)
{

    if(msg->id==id)
    {
        m_iSaturationMin = msg->minS;
        m_iSaturationMax = msg->maxS;
        m_iHueMin = msg->minH;
        m_iHueMax = msg->maxH;
        m_iValueMin = msg->minV;
        m_iValueMax = msg->maxV;
        m_isBackground = msg->thisIsBack;
        m_backgroundSub=msg->backSubOn;

        if(m_isBackground)
            saveBg=true;


        m_blurSize=msg->blurSize*2+1;

        setDetectionRoiSize(msg->agentSize, msg->agentSize);
        setImageRoiSize(msg->roiSize, msg->roiSize);


        displayROI=msg->displayOn;
        fixed=msg->fixed;

        ROS_INFO_STREAM("Current tracker #"<<id<<" settings: \n"<<
                        "Hue: "<< m_iHueMin<<" - "<<m_iHueMax<< "\n"<<
                        "Saturation: "<< m_iSaturationMin<<" - "<<m_iSaturationMax<< "\n"<<
                        "Value: "<< m_iValueMin<<" - "<<m_iValueMax<< "\n"<<
                        "Blur size: "<< m_blurSize<<
                        "\n ROI width: "<<  m_ImgRoi.width<<"\n Agent size: "<<m_DetectRoi.width);
    }

}




TrackerColor::TrackerColor(const TrackerColor& other) :
    Tracker(other), it(n)
{
    // add here copy constructor code for derived class
    m_iSaturationMin = other.m_iSaturationMin;
    m_iSaturationMax = other.m_iSaturationMax;
    m_iHueMin = other.m_iHueMin;
    m_iHueMax = other.m_iHueMax;
    m_iValueMin = other.m_iValueMin;
    m_iValueMax = other.m_iValueMax;
    
    m_iHistogramSize = other.m_iHistogramSize;
    memcpy(m_iHistogramRange, other.m_iHistogramRange, 2*sizeof(*m_iHistogramRange));
    m_DetectRoi = other.m_DetectRoi;
    m_ImgRoi = other.m_ImgRoi;
    m_TrackWindow = other.m_TrackWindow;
    m_TrackBox = other.m_TrackBox;
    m_Hist = other.m_Hist;
    m_bUpdateHistogram = other.m_bUpdateHistogram;
    m_iUpdateHistogramCounter = other.m_iUpdateHistogramCounter;
    m_iUpdateHistogramValue = other.m_iUpdateHistogramValue;

    //    roiPub=other.roiPub;
}

TrackerColor& TrackerColor::operator=(const TrackerColor& other)
{
    // call copy assignment of base class
    Tracker::operator=(other);

    // add here copy assignment code for derived class
    m_iSaturationMin = other.m_iSaturationMin;
    m_iSaturationMax = other.m_iSaturationMax;
    m_iHueMin = other.m_iHueMin;
    m_iHueMax = other.m_iHueMax;
    m_iValueMin = other.m_iValueMin;
    m_iValueMax = other.m_iValueMax;
    
    m_iHistogramSize = other.m_iHistogramSize;
    memcpy(m_iHistogramRange, other.m_iHistogramRange, 2*sizeof(*m_iHistogramRange));
    m_DetectRoi = other.m_DetectRoi;
    m_ImgRoi = other.m_ImgRoi;
    m_TrackWindow = other.m_TrackWindow;
    m_TrackBox = other.m_TrackBox;
    m_Hist = other.m_Hist;
    m_bUpdateHistogram = other.m_bUpdateHistogram;
    m_iUpdateHistogramCounter = other.m_iUpdateHistogramCounter;
    m_iUpdateHistogramValue = other.m_iUpdateHistogramValue;


    //    roiPub=other.roiPub;
    
    return *this;
}

bool TrackerColor::processFrame(const cv::Mat& inFrame, cv::Mat& outFrame, cv::Point2f& outCenter)
{
    if(saveBg)
    {
        inFrame.copyTo(m_Background);
        saveBg=false;

    }
    bool isDetected = false;

    if(!fixed)
    {
        // work on a smaller image
        // work on an roi, check that the roi is inside the image
        checkROI(inFrame.cols, inFrame.rows, m_ImgRoi);
        cv::Mat frameinroi;
        if(m_backgroundSub && displayROI && id>=0)
        {
            cv::subtract(inFrame(m_ImgRoi),m_Background(m_ImgRoi),frameinroi);
            //            m_BackSub->apply(inFrame, m_fgMask);
            //            inFrame(m_ImgRoi).copyTo(frameinroi, m_fgMask(m_ImgRoi));
            cv_bridge::CvImage bs_msg;
            bs_msg.encoding = sensor_msgs::image_encodings::TYPE_8UC3;
            cv::resize(frameinroi, bs_msg.image, cv::Size(399,399));
            bsPub.publish(bs_msg.toImageMsg());
            //            ROS_INFO_STREAM("Sent image with subtracted background");

        }
        else
            frameinroi= inFrame(m_ImgRoi);





        // update the detection roi
        // detection roi is wrt the small image frameinroi
        cv::Rect detectroi = m_DetectRoi;
        detectroi.x -= m_ImgRoi.x;
        detectroi.y -= m_ImgRoi.y;
        checkROI(frameinroi.cols, frameinroi.rows, detectroi);

        // color detection
        cv::Mat hsv;
        cv::cvtColor(frameinroi, hsv, CV_BGR2HSV);
        cv::Mat hsvroi(hsv, detectroi);

        cv::GaussianBlur( hsvroi, hsvroi, cv::Size( m_blurSize, m_blurSize ), 0, 0 );

        // This part depends on the agents
        cv::Mat mask;
        detectColor(hsv, mask);


        //    if(displayROI && id>=0)
        //    {
        cv_bridge::CvImage out_msg;
        out_msg.encoding = sensor_msgs::image_encodings::TYPE_8UC1;
        out_msg.image=mask;


        roiPub.publish(out_msg.toImageMsg());

        ROS_DEBUG_STREAM("Streaming ROI "<< id);
        //    }



        // get hue channel
        cv::Mat hue;
        hue.create(hsv.size(), hsv.depth());
        // source, dimension source, dest, dimension dest, pair assoc, number of pairs
        // get hue
        int ch[] = {0, 0};
        mixChannels(&hsv, 1, &hue, 1, ch, 1);

        // Object has been selected by user, set up CAMShift search properties once
        // do this part if the tracker is not initialized or if the tracker has
        // to update the histogram online
        if ((!m_bProcessStarted) ||
                (m_bUpdateHistogram && m_iUpdateHistogramCounter >= m_iUpdateHistogramValue))
        {
            cv::Mat roi(hue, detectroi);
            cv::Mat maskroi(mask, detectroi);

            cv::calcHist(&roi, 1, 0, maskroi, m_Hist, 1, &m_iHistogramSize, &m_fPtrHistRanges);
            cv::normalize(m_Hist, m_Hist, 0, 255, cv::NORM_MINMAX);

            m_iUpdateHistogramCounter = 0;

            m_bProcessStarted = true;
        }

        m_TrackWindow = detectroi;

        // Perform BackProjection
        cv::Mat backproj;
        cv::calcBackProject(&hue, 1, 0, m_Hist, backproj, &m_fPtrHistRanges);
        backproj &= mask;

        if (m_TrackWindow.area() <= 1)
        {
            int cols = backproj.cols, rows = backproj.rows, r = (MIN(cols, rows) + 5)/6;
            m_TrackWindow = cv::Rect(m_TrackWindow.x - r, m_TrackWindow.y - r,
                                     m_TrackWindow.x + r, m_TrackWindow.y + r) &
                    cv::Rect(0, 0, cols, rows);
        }
        else
        {
            if(cv::sum(backproj)[0]>10)
            {
                // Perform CamShift
                m_TrackBox = cv::CamShift(backproj, m_TrackWindow, cv::TermCriteria(cv::TermCriteria::EPS |
                                                                                    cv::TermCriteria::COUNT,
                                                                                    10, 1));
                ROS_DEBUG_STREAM("CamShift");
            isDetected = true;
            detectroi.x = m_TrackBox.center.x - detectroi.width*0.5f;
            detectroi.y = m_TrackBox.center.y - detectroi.height*0.5f;
            }
        }

        if(cv::sum(backproj)[0]>10)
        {

        // convert from image roi to initial image dimension
        m_DetectRoi = detectroi;
        m_DetectRoi.x += m_ImgRoi.x;
        m_DetectRoi.y += m_ImgRoi.y;



        // update trackbox
        m_TrackBox.center.x += m_ImgRoi.x;
        m_TrackBox.center.y += m_ImgRoi.y;


        // update testroi for last!!!
        m_ImgRoi.x = (m_DetectRoi.x + m_DetectRoi.width/2) - m_ImgRoi.width*0.5f;
        m_ImgRoi.y = (m_DetectRoi.y + m_DetectRoi.height/2) - m_ImgRoi.height*0.5f;

        // set the center in the original image dimension
        if (m_TrackWindow.area() <= 1)
        {
            // use detection roi
            outCenter.x = m_DetectRoi.x + m_DetectRoi.width/2;
            outCenter.y = m_DetectRoi.y + m_DetectRoi.height/2;
        }
        else
        {
            outCenter = m_TrackBox.center;
        }
        }

        m_iUpdateHistogramCounter++;

    }
    else
    {
        m_TrackBox.center.x =m_DetectRoi.x;
        m_TrackBox.center.y =m_DetectRoi.y;
        outCenter = m_TrackBox.center;
        isDetected = true;
    }
    return isDetected;
}
