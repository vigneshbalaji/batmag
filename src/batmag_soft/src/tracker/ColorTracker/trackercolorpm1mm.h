#pragma once

// TrackerColor interface
#include "TrackerColor.h"

using namespace std;

class TrackerColorPM1mm:public TrackerColor
{
    void detectColor(const cv::Mat& framein, cv::Mat& maskout) override;
public:
    TrackerColorPM1mm();
};
