#include "TrackerColorHydrogelGripper.h"

TrackerColorHydrogelGripper::TrackerColorHydrogelGripper()
{

// HSV threshold for the selected color
    // for metallic grippers
    m_iSaturationMin = 100;
    m_iSaturationMax = 155;
    m_iHueMin = 0;
    m_iHueMax = 256;
    m_iValueMin = 0;
    m_iValueMax = 110;
}

void TrackerColorHydrogelGripper::detectColor(const cv::Mat& framein, cv::Mat& maskout)
{
    // This part depends on the agents, for different agents
    // this part needs to be updated to track the right color
    cv::Mat maskS;

    cv::inRange(framein, cv::Scalar(0, m_iSaturationMin, 0),
                cv::Scalar(256, m_iSaturationMax, 256), maskS);
    maskout = maskS;
    // This part depends on the agents, for different agents
}
