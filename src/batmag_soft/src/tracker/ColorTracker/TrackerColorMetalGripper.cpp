
/*
 * Author: Stefano Scheggi
 * @2017
 */

#include "TrackerColorMetalGripper.h"

TrackerColorMetalGripper::TrackerColorMetalGripper()
{
    // HSV threshold for the selected color
    // for metallic grippers
    m_iSaturationMin = 90;
    m_iSaturationMax = 256;
    m_iHueMin = 0;
    m_iHueMax = 256;
    m_iValueMin = 0;
    m_iValueMax = 110;
}

void TrackerColorMetalGripper::detectColor(const cv::Mat& framein, cv::Mat& maskout)
{
    // This part depends on the agents, for different agents
    // this part needs to be updated to track the right color
    cv::Mat maskS, maskV;
    
    cv::inRange(framein, cv::Scalar(0, m_iSaturationMin, 0),
                cv::Scalar(256, m_iSaturationMax, 256), maskS);
    cv::inRange(framein, cv::Scalar(0, 0, m_iValueMin),
                cv::Scalar(256, 256, m_iValueMax), maskV);
    
    maskout = maskS | maskV;
    // This part depends on the agents, for different agents
}