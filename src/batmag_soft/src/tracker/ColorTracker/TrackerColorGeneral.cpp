
/*
 * Author: Stefano Scheggi
 * @2017
 */

#include "TrackerColorGeneral.h"

TrackerColorGeneral::TrackerColorGeneral()
{}

void TrackerColorGeneral::detectColor(const cv::Mat& framein, cv::Mat& maskout)
{
    maskout = cv::Mat(framein.rows, framein.cols, CV_8UC1, cv::Scalar(255));
}