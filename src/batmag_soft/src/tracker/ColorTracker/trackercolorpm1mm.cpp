#include "trackercolorpm1mm.h"

TrackerColorPM1mm::TrackerColorPM1mm()
{
    // HSV threshold for the selected color
    // for metallic grippers
    m_iHueMin = 0;
    m_iHueMax = 25.6;

    m_iSaturationMin = 51;
    m_iSaturationMax = 153.6;


    m_iValueMin = 205;
    m_iValueMax = 256;
}

void TrackerColorPM1mm::detectColor(const cv::Mat& framein, cv::Mat& maskout)
{
    // This part depends on the agents, for different agents
    // this part needs to be updated to track the right color
    cv::Mat maskS, maskV,maskH;

    cv::inRange(framein, cv::Scalar(m_iHueMin, m_iSaturationMin, m_iValueMin),
                cv::Scalar(m_iHueMax, m_iSaturationMax, m_iValueMax), maskout);
    // This part depends on the agents, for different agents
}
