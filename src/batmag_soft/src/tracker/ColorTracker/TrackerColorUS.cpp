
/*
 * Author: Stefano Scheggi
 * @2017
 */

#include "TrackerColorUS.h"

TrackerColorUS::TrackerColorUS()
{
    // HSV threshold for the selected color
    // for metallic grippers
    m_iSaturationMin = 90;
}

void TrackerColorUS::detectColor(const cv::Mat& framein, cv::Mat& maskout)
{
    // This part depends on the agents, for different agents
    // this part needs to be updated to track the right color
    std::vector<cv::Mat> channels(3);
    
    // split the RGB channels, "channels"
    cv::split(framein, channels);
    
    // binary thresholding on Red channel (channels[2] is the red channel)
    cv::threshold(channels[2], maskout, m_iSaturationMin, 255, 0);
    // This part depends on the agents, for different agents
}