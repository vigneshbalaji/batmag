#include "trackercolorcircclip.h"

TrackerColorCircClip::TrackerColorCircClip()
{
    // HSV threshold for the selected color
    // for metallic grippers
    m_iHueMin = 0;
    m_iHueMax = 16;

    m_iSaturationMin = 33;
    m_iSaturationMax = 105;


    m_iValueMin = 46;
    m_iValueMax = 123;

    m_blurSize = 1;
}

void TrackerColorCircClip::detectColor(const cv::Mat& framein, cv::Mat& maskout)
{
    // This part depends on the agents, for different agents
    // this part needs to be updated to track the right color
    cv::Mat maskS, maskV,maskH;

    cv::inRange(framein, cv::Scalar(m_iHueMin, m_iSaturationMin, m_iValueMin),
                cv::Scalar(m_iHueMax, m_iSaturationMax, m_iValueMax), maskout);
    // This part depends on the agents, for different agents
}

//[ INFO] [1511285029.101009941]: Current tracker settings:
//Hue: 0 - 37
//Saturation: 124 - 162
//Value: 234 - 255
//Blur size: 1
// ROI width: 502
// Agent size: 124
