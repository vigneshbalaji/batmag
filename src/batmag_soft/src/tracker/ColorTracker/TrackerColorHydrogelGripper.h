#pragma once


#include "TrackerColor.h"

class TrackerColorHydrogelGripper:public TrackerColor
{
    void detectColor(const cv::Mat& framein, cv::Mat& maskout) override;

public:
    TrackerColorHydrogelGripper();
};

