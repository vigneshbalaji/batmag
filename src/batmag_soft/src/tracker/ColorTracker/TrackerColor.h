
/*
 * Author: Stefano Scheggi
 * @2017
 *
 * This is the interface for the color trackers
 * When adding a new color tracker, inheritage from this class and implement
 * the method detectColor (based on CamShift method)
 */

#pragma once

// Tracker interface
#include "../Tracker.h"


//dennis stuff
#include "image_transport/image_transport.h"
#include "cv_bridge/cv_bridge.h"
//

using namespace std;

// CamShift tracker
class TrackerColor:public Tracker
{
protected:
    // timers
    chronoTimer m_timerStart, m_timerEnd;
    
    // HSV threshold for the selected color
    int m_iSaturationMin, m_iSaturationMax;
    int m_iHueMin, m_iHueMax;
    int m_iValueMin, m_iValueMax; 
    int m_blurSize=5;
    bool m_isBackground=false, m_backgroundSub=false, saveBg=false;
    
    int m_iHistogramSize;
    float m_iHistogramRange[2];
    const float* m_fPtrHistRanges = nullptr; 
    cv::Rect m_TrackWindow;
    cv::RotatedRect m_TrackBox;
    cv::Mat m_Hist;
    cv::Mat m_Background;
    bool m_bUpdateHistogram;
    int m_iUpdateHistogramCounter, m_iUpdateHistogramValue;

    virtual void detectColor(const cv::Mat& framein, cv::Mat& maskout) = 0;


    //ROS stuff
    ros::NodeHandle n;

    ros::Subscriber setSub;
    ros::ServiceServer mServer;
    bool displayROI=false, fixed=false;

    bool processSettingsReq(batmag_soft::getInitTrackerSettings::Request &req,
                            batmag_soft::getInitTrackerSettings::Response &res);

    void settingsCb(const batmag_soft::TrackerSettings::ConstPtr & msg);

    image_transport::ImageTransport it;
    image_transport::Publisher roiPub, bsPub;

    
public:
    TrackerColor();
    virtual ~TrackerColor(){}
    TrackerColor(const TrackerColor&);
    TrackerColor& operator=(const TrackerColor&);

    virtual void setId(int value) override;

    
	bool processFrame(const cv::Mat&, cv::Mat&, cv::Point2f&) override;
    
    void setUpdateHistogram(bool value){ m_bUpdateHistogram = value; }
    void setUpdateHistogramValue(int value){ m_iUpdateHistogramValue = value; }
    
    cv::RotatedRect getTrackBox(){ return m_TrackBox; }
};
