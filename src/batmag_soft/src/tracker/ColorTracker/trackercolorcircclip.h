#pragma once


#include "TrackerColor.h"

class TrackerColorCircClip: public TrackerColor
{
    void detectColor(const cv::Mat& framein, cv::Mat& maskout) override;

public:
    TrackerColorCircClip();
};
