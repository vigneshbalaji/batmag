
/*
 * Author: Stefano Scheggi
 * @2017
 * 
 * Tracker for metallic grippers based on CamShift method
 */

#pragma once

// TrackerColor interface
#include "TrackerColor.h"

using namespace std;

class TrackerColorGeneral:public TrackerColor
{
    void detectColor(const cv::Mat& framein, cv::Mat& maskout) override;
    
public:
    TrackerColorGeneral();
};