/*
* Author: Stefano Scheggi
* @2016
*
* This is the interface for the trackers
* When adding a new tracker, inheritage from this class and implement
* the method processFrame
*/

#include "tracker.h"

Tracker::Tracker() : mProcessStarted(false)
{
#if USE_MULTITHREADING
	mActualFrame = nullptr;
	mProcessThread = nullptr; 
	mVideoBuffer = nullptr;
#endif
}

Tracker::~Tracker()
{
#if USE_MULTITHREADING
	if (mProcessThread != nullptr)
	{
		// how to stop and destroy the thread
		mProcessThread->join();
		cout << "mProcessThread->join()...ok\n";
		delete mProcessThread;
		mProcessThread = nullptr;
		cout << "mProcessThread deleted...ok" << endl;
	}
#endif
}

bool Tracker::drawReference(const Mat& inFrame, Mat& outFrame, Point2f& ref)
{
    outFrame=inFrame;
    line(outFrame, Point2d(-ref_line_length + ref.x, ref.y), Point2d(ref_line_length + ref.x, ref.y), ref_line_color, ref_line_thickness, 8, 0); //Horizontal line of center cross
    line(outFrame, Point2d(ref.x, -ref_line_length + ref.y), Point2d(ref.x, ref_line_length + ref.y), ref_line_color, ref_line_thickness, 8, 0); //Vertical line of center cross

    return true;

}

#if USE_MULTITHREADING
void Tracker::startThread(void)
{
	// destroy previous thread and create a new one
	if (mProcessThread != nullptr)
	{
		//cout << "mProcessThread deleted...ok" << endl;
		delete mProcessThread;
		mProcessThread = nullptr;
	}

	// create a new one
	mIsThreadRunning = true;
	mProcessThread = new boost::thread(boost::bind(&Tracker::processFrameThread, this));
	cout << "mProcessThread created...ok" << endl;
}

void Tracker::processFrameThread(void)
{
	Mat tmpOutFrame, tmpFrameIn;
	Point2f tmpCenter;
	Point2f tmpCenterDraw;

	boost::posix_time::ptime nowTime, prevTime;
	boost::posix_time::time_duration diffTime;

	mComputationTime.clear();

	nowTime = boost::posix_time::microsec_clock::local_time();
	prevTime = nowTime;

#if INCREMENT_FRAME_IN_THREAD == 1
	int counter = mInitialFrame;
#endif

//// REMOVE
//#if INCREMENT_FRAME_IN_THREAD == 1
//	counter = mInitialFrame;
//#endif

	while (mIsThreadRunning)
	{
		nowTime = boost::posix_time::microsec_clock::local_time();
		diffTime = nowTime - prevTime;

		if (diffTime.total_milliseconds() >= mParamsThread->mSamplingTime) // milliseconds
		{
#if INCREMENT_FRAME_IN_THREAD == 1
			// make sure that we have the requested video
			if ((counter - mInitialFrame) >= mVideoBuffer->size())
			{
				// update the counter for the main loop
				*mActualFrame = static_cast<int>(mVideoBuffer->size());
				//cout << "break *mActualFrame: " << *mActualFrame << endl;
				break;
			}

			// get the video frame from the buffer
			tmpFrameIn = mVideoBuffer->at(counter - mInitialFrame).clone();

			// here is possible to add delays which simulate the grabbing time

			// process the frame
			processFrame(tmpFrameIn, tmpOutFrame, *mParamsThread, tmpCenter);
			tmpCenterDraw.x = tmpCenter.x;
			tmpCenterDraw.y = tmpCenter.y;

			// update the shared variables
			mProcessMutex.lock();
			// update processed frame to teh main loop
			*mOutFrameThread = *(&tmpOutFrame);
			// update the detected center to the main loop
			*mOutCenterThread = *(&tmpCenterDraw);
			// update the counter for the main loop (visualization, etc...)
			*mActualFrame = ++counter;
			mProcessMutex.unlock();

			// store the computation time for debugging
			if (mParamsThread->mSaveSamplingTime)
			{
				mSamplingTime.push_back(static_cast<double>(diffTime.total_milliseconds()));
			}

			// update the loop time
			prevTime = nowTime;
#else            
			// make sure that we have the requested video
			// since we are loading frames while processing them, it may happen that we 
			// try to process a video frame which is not still loaded, if the tracking sampling time is small
			// in this case just restart teh processing from the first frame
			// if data is saved is better to load teh whole video before and then process
			if ((*mActualFrame - mInitialFrame) >= mVideoBuffer->size())
			{
				*mActualFrame = *(&mInitialFrame);
			}

			// get the video frame from the buffer
			tmpFrameIn = mVideoBuffer->at(*mActualFrame - mInitialFrame).clone();

			// here is possible to add delays which simulate the grabbing time

			// process the frame
			processFrame(tmpFrameIn, tmpOutFrame, *mParamsThread, tmpCenter);
			tmpCenterDraw.x = tmpCenter.x;
			tmpCenterDraw.y = tmpCenter.y;

			// update the shared variables
			mProcessMutex.lock();
			*mOutFrameThread = *(&tmpOutFrame);
			*mOutCenterThread = *(&tmpCenterDraw);
			mProcessMutex.unlock();

			// store the computation time for debugging
			if (mParamsThread->mSaveSamplingTime)
			{
				mSamplingTime.push_back(static_cast<double>(diffTime.total_milliseconds()));
			}

			// update the loop time
			prevTime = nowTime;
#endif
		}
	}
}
#endif

void Tracker::updateROI(const Rect& oldROI, const Rect& imageROI, const Point2f& center, Rect& newROI)
{
    newROI = oldROI;
	newROI.x = oldROI.x + static_cast<int>(center.x - oldROI.width*0.5f);
	newROI.y = oldROI.y + static_cast<int>(center.y - oldROI.height*0.5f);
    
    // check if the ROI is inside the image or not, if not update properly
    if (newROI.x < 0)
    {
        newROI.x = 0;
    }
    if (newROI.x + newROI.width > imageROI.width)
    {
        int diff = newROI.x + newROI.width - imageROI.width;
        newROI.x -= diff;
    }
    
    if (newROI.y < 0)
    {
        newROI.y = 0;
    }
    if (newROI.y + newROI.height > imageROI.height)
    {
        int diff = newROI.y + newROI.height - imageROI.height;
        newROI.x -= diff;
    }
}

bool Tracker::saveCentersToFile(const char* filename) const
{
    cout << "saveCentersToFile...";
    if (mCenters.size() > 0)
    {
        ofstream outFile;
        outFile.open (filename);
        for (unsigned i = 0; i < mCenters.size(); i++)
        {
			outFile << mCenters[i].x << " " << mCenters[i].y << "\n"; // " " << mCenters[i].z << " " << mCenters[i].w << "\n";
        }
        outFile.close();
        cout << "done" << endl;
        return true;
    }
    else
    {
        cout << "mCenters is empty, enable save centers from params" << endl;
        return false;
    }
}

bool Tracker::saveComputationTimeToFile(const char* filename) const
{
    if (mComputationTime.size() > 0)
    {
        ofstream outFile;
        outFile.open (filename);
        for (unsigned i = 0; i < mComputationTime.size(); i++)
        {
            outFile << mComputationTime[i] << "\n";
        }
        outFile.close();
        return true;
    }
    else
    {
        cout << "TrackerHydrogelUS::mComputationTime is empty, enable save computation time from params" << endl;
        return false;
    }
}

bool Tracker::saveSamplingTimeToFile(const char* filename) const
{
    if (mSamplingTime.size() > 0)
    {
        ofstream outFile;
        outFile.open (filename);
        for (unsigned i = 0; i < mSamplingTime.size(); i++)
        {
            outFile << mSamplingTime[i] << "\n";
        }
        outFile.close();
        return true;
    }
    else
    {
        cout << "TrackerHydrogelUS::mSamplingTime is empty, enable save sampling time from params" << endl;
        return false;
    }
}
