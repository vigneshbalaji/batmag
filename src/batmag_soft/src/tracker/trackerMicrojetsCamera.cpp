
/*
 * Author: Stefano Scheggi, Gert van de Steeg
 * @2016
 */

#include "TrackerMicrojetsCamera.h"

TrackerMicrojetsCamera::TrackerMicrojetsCamera() : mActualOrientation(0), mPreviousOrientationTracker(0),
    mPreviousOrientation(0), mInitializeUnwrap(true)
{
	mProcessStarted = false;

	// used to calculate the computation time
	nowComputationTime = boost::posix_time::microsec_clock::local_time();
	prevComputationTime = nowComputationTime;

	// Vignette removal, due to imadjust()
	// Read vignette params (obtained from Matlab), it is an image of the estimated vignette
	FileStorage fsParams(VIGNETTE_FILE, FileStorage::READ);
	if (!fsParams.isOpened())
	{
		cout << "Failed to open YML file." << endl;
	}
	fsParams["vignette_params"] >> this->vignette_params;
	fsParams.release();
// convert
    convertmetres2pixels(REFERENCE_CIRCLE_RADIUS,mDrawCircleRadius);
    convertmetres2pixels(REFERENCE_CIRCLE_CENTER_X,mDrawCircleCenterX);
    convertmetres2pixels(REFERENCE_CIRCLE_CENTER_Y,mDrawCircleCenterY);
}

TrackerMicrojetsCamera::~TrackerMicrojetsCamera()
{
	//// DEBUG
	//// save to file
	//ofstream outFile;
	//outFile.open("orientation.txt");
	//for (unsigned i = 0; i < mOrientation.size(); i++)
	//{
	//	outFile << mOrientation[i] << "\n";
	//}
}

void TrackerMicrojetsCamera::invertOrientation(void)
{
	if (mActualOrientation + M_PI < M_PI)
		mActualOrientation += M_PI;
	else if (mActualOrientation - M_PI > -M_PI)
		mActualOrientation -= M_PI;

    mPreviousOrientationTracker = mActualOrientation;
    mPreviousOrientation = mActualOrientation;

    mInitializeUnwrap = true;
}

// unwrapOrientation(mPreviousOrientationTracker, orientation_combined[min_loc.x], mActualOrientation);
void TrackerMicrojetsCamera::unwrapOrientation(double& previousAngle, double& actualAngle, double& outputAngle)
{
	// UNWRAP THE ANGLE 
	double mapped_angle;

//	if (mInitializeUnwrap)
//	{
//        previousAngle = actualAngle;
//	}
    //cout << "previousAngle: " << previousAngle * 180.0 / M_PI << endl;

	// MAP FROM -90 and 90 to -180 and 180
	// previousAngle should be -180 and 180
	// actualAngle should be -90 and 90
	// mapped_angle should be -180 and 180
	mapOrientationBetweenPiMinusPi(previousAngle, actualAngle, mapped_angle);
	previousAngle = mapped_angle;

    //cout << "mapped_angle: " << mapped_angle * 180.0 / M_PI << endl;

	//outputAngle = mapped_angle;

	// UNWRAP THE ANGLE
	// set and initialize the previous and corrected path angles
	if (mInitializeUnwrap)
	{
		mPreviousOrientation = mapped_angle;
		mNextOrientation = mapped_angle;
		mInitializeUnwrap = false;
	}
	else
	{
		mPreviousOrientation = mNextOrientation;
	}

    ppc.angle_unwrap(mPreviousOrientation, mapped_angle, mNextOrientation);
	outputAngle = mNextOrientation;

    // unwrapped angle
    //cout << "mNextOrientation: " << mNextOrientation * 180.0 / M_PI << endl;
}

void TrackerMicrojetsCamera::mapOrientationBetweenPiMinusPi(double& previousAngle, double& actualAngle, double& outputAngle)
{
	// Take angles between -90 and 90 and map them between -180 and 180
	if (fabs(previousAngle - actualAngle) > M_PI / 2)
	{
		double ang1 = actualAngle - M_PI;
		double ang2 = actualAngle  + M_PI;

		if (fabs(ang1 - previousAngle) < fabs(ang2 - previousAngle))
		{
			outputAngle = ang1;
		}
		else
			outputAngle = ang2;
	}
	else
		outputAngle = actualAngle;

	if (outputAngle > M_PI)
		outputAngle -= 2 * M_PI;
	else if (outputAngle < -M_PI)
		outputAngle += 2 * M_PI;
}

void TrackerMicrojetsCamera::removeShapeBasedOnEccentricity(vector<vector<Point> >& contours, vector<double>& eccentricity,
	vector<double>& orientation, vector<double>& majorAxisLength)
{
	// Remove circular shaped and (too) long contours
	// TODO Line shaped contour (or 'child') inside circular shaped contour will probably not work right, use hierarchy from findContours() to find 'child' contours and remove accordingly
	if (eccentricity.size() > 0)
	{
		vector<unsigned char> isLineShape;
		compare(eccentricity, ECCENTRICITY_THRESHOLD, isLineShape, CMP_GT);
		for (unsigned i = 0; i < contours.size();)
		{
			if (isLineShape[i] == 0 || majorAxisLength[i] > MAX_JET_LENGTH_BLOB)
			{
				//Not a line shape or too long, so remove
				contours.erase(contours.begin() + i); // is this needed?
				isLineShape.erase(isLineShape.begin() + i);
				majorAxisLength.erase(majorAxisLength.begin() + i);
				orientation.erase(orientation.begin() + i);
				eccentricity.erase(eccentricity.begin() + i);
			}
			else
			{
				i++;
			}
		}
	}
}

void TrackerMicrojetsCamera::removeShapeBasedOnEccentricity(vector<vector<Point> >& contours, vector<double>& eccentricity, 
	vector<double>& orientation, vector<double>& majorAxisLength, vector<Scalar>& centroids)
{
	// Remove circular shaped and (too) long contours
	// TODO Line shaped contour (or 'child') inside circular shaped contour will probably not work right, use hierarchy from findContours() to find 'child' contours and remove accordingly
	if (eccentricity.size() > 0)
	{
		vector<unsigned char> isLineShape;
		compare(eccentricity, ECCENTRICITY_THRESHOLD, isLineShape, CMP_GT);
		for (unsigned i = 0; i < contours.size();)
		{
			if (isLineShape[i] == 0 || majorAxisLength[i] > MAX_JET_LENGTH_BLOB)
			{
				//Not a line shape or too long, so remove
				contours.erase(contours.begin() + i); // is this needed?
				isLineShape.erase(isLineShape.begin() + i);

				majorAxisLength.erase(majorAxisLength.begin() + i);
				orientation.erase(orientation.begin() + i);
				eccentricity.erase(eccentricity.begin() + i);
				centroids.erase(centroids.begin() + i);
			}
			else
			{
				i++;
			}
		}
	}
}

void TrackerMicrojetsCamera::computeBlobOrientation(vector<vector<Point> >& contoursIn, vector<double>& minorAxisOut, 
	vector<double>& majorAxisOut, vector<double>& eccentricityOut, vector<double>& orientationOut)
{
	// Calculate eccentricity and orientation (from Matlab regionprops()) 
	vector<vector<double>> contoursX_morph(contoursIn.size()), contoursY_morph(contoursIn.size());
	vector<double> contoursXY_morph;

	double uxy_morph, uxx_morph, uyy_morph, common_morph, num_morph, den_morph;
	
	for (unsigned c = 0; c < contoursIn.size(); c++)
	{
		contoursX_morph[c].resize(contoursIn[c].size());
		contoursY_morph[c].resize(contoursIn[c].size());
		contoursXY_morph.resize(contoursIn[c].size());

		//Separate coordinates in X and Y vector
		for (unsigned i = 0; i < contoursIn[c].size(); i++)
		{
			contoursX_morph[c][i] = contoursIn[c][i].x;
			contoursY_morph[c][i] = contoursIn[c][i].y;
		}

		//Subtract the mean from X and Y
		subtract(contoursX_morph[c], mean(contoursX_morph[c])[0], contoursX_morph[c]);
		subtract(contoursY_morph[c], mean(contoursY_morph[c])[0], contoursY_morph[c]);

		//Calculate normalized second central moments
		multiply(contoursX_morph[c], contoursY_morph[c], contoursXY_morph, 1, -1);
		uxy_morph = ((sum(contoursXY_morph)[0]) / contoursXY_morph.size());

		pow(contoursX_morph[c], 2, contoursX_morph[c]);
		pow(contoursY_morph[c], 2, contoursY_morph[c]);
		uxx_morph = ((sum(contoursX_morph[c])[0]) / contoursX_morph[c].size()) + 0.0833333; // (1 / 12);
		uyy_morph = ((sum(contoursY_morph[c])[0]) / contoursY_morph[c].size()) + 0.0833333; // (1 / 12);

		//Calculate properties
		//common = sqrt(pow(uxx - uyy, 2) + 4 * pow(uxy, 2));
		//majorAxisLength[c] = 2 * sqrt(2) * sqrt(uxx + uyy + common);
		//minorAxisLength[c] = 2 * sqrt(2) * sqrt(uxx + uyy - common);
		//eccentricity[c] = (2 * sqrt(pow(majorAxisLength[c] / 2, 2) - pow(minorAxisLength[c] / 2, 2))) / majorAxisLength[c]; //sqrt(1 - ((pow(minorAxisLength[c]/2,2))/(pow(majorAxisLength[c]/2,2))));
		common_morph = sqrt(pow(uxx_morph - uyy_morph, 2) + 4 * uxy_morph * uxy_morph);
		majorAxisOut[c] = 2.828427 * sqrt(uxx_morph + uyy_morph + common_morph); // 2.828427 = 2*sqrt(2)
		minorAxisOut[c] = 2.828427 * sqrt(uxx_morph + uyy_morph - common_morph); // 2.828427 = 2*sqrt(2)
		eccentricityOut[c] = (2 * sqrt(pow(majorAxisOut[c] / 2, 2) - pow(minorAxisOut[c] / 2, 2))) / majorAxisOut[c]; //sqrt(1 - ((pow(minorAxisLength[c]/2,2))/(pow(majorAxisLength[c]/2,2))));


		//Calc orientation (ranging from -90 to 90 degrees)
		if (uxx_morph > uyy_morph)
		{
			num_morph = 2 * uxy_morph;
			den_morph = uxx_morph - uyy_morph + common_morph;
		}
		else
		{
			num_morph = uyy_morph - uxx_morph + sqrt(pow(uyy_morph - uxx_morph, 2) + 4 * pow(uxy_morph, 2));
			den_morph = 2 * uxy_morph;
		}

		if (num_morph == 0 && den_morph == 0)
		{
			orientationOut[c] = 0;
		}
		else
		{
			orientationOut[c] = atan(num_morph / den_morph);
		}
	}
}

bool TrackerMicrojetsCamera::processFrame(const Mat& inFrame, Mat& outFrame, TrackerParameters& params, Point2f& outCenter)
{
	//DataTypes http://ninghang.blogspot.nl/2012/11/list-of-mat-type-in-opencv.html
	//Computation time
	prevComputationTime = boost::posix_time::microsec_clock::local_time();

	bool isDetected = false;
	

	/* --- START PRE-PROCESS --- */
	// Prepare the image by applying: 
	// ROI
	// enhancing contrast
	// remove vignette
	// LoG filter 
	// binarization

	Rect_<float> ROI = params.mInitialROI;
	Mat img_rgb, img_gray, img_orig, img_contrast, img_LoG, kernel, img_vignetteless, BW, BW_high, BW_highest;
	img_rgb = inFrame.clone();

	// Convert to grayscale image
	cv::cvtColor(img_rgb, img_orig, CV_RGB2GRAY);

	// Apply ROI
#if USE_ROI == 1
    //Check image bounds, if OutOfBounds, temporarily decrease ROI dimensions for this frame
    if (ROI.x + ROI.width > img_orig.cols)
    {
        ROI.width = floor(img_orig.cols - ROI.x);
    }
    else if (ROI.x < 0)
    {
        ROI.width = 2*floor(ROI.x+ROI.width/2);
        ROI.x = 0;
    }

    if (ROI.y + ROI.height > img_orig.rows)
    {
        ROI.height = floor(img_orig.rows - ROI.y);
    }
    else if (ROI.y < 0)
    {
        ROI.height = 2*floor(ROI.y+ROI.height/2);
        ROI.y = 0;
    }
    img_gray = img_orig(ROI);
#else
		img_gray = img_orig;
#endif

	//Old Histogram Equalization (HE)
	//equalizeHist(img_gray,img_contrast);

	//Contrast Limited Adaptive Histogram Equalization, newer method, does not require vignet removal, not as black as Histogram Equalization
	//Ptr<CLAHE> clahe = createCLAHE();
	//clahe->setClipLimit(3.0);
	//clahe->setTilesGridSize(Size(10, 10));
	//clahe->apply(img_gray,img_contrast);

	//Use the following instead of HE or CLAHE, in order to stay closer to the original Matlab implementation:
	//http://stackoverflow.com/questions/31647274/is-there-any-function-equivalent-to-matlabs-imadjust-in-opencv-with-c
	//Histogram Streching (HS) (with tolerance boundaries)
	imadjust(img_gray, img_contrast, 1); //1% tolerance/clipping

	//Vignette removal, due to imadjust()
	//Read vignette params (obtained from Matlab), it is an image of the estimated vignette
	//Mat vignette_params = get_vignette_params();
	//cout << (int)vignette_params.type() << endl;
	//cout << vignette_params.at<float>(400,400) << endl;


	//Obtain vignette-corrected image (also see Matlab implementation)
	img_contrast = img_contrast + VIGNETTE_SHIFT; //+shift to avoid log(0)
	img_contrast.convertTo(img_contrast, CV_32F);
	log(img_contrast, img_contrast);

#if USE_ROI
		img_vignetteless = img_contrast - vignette_params(ROI);
#else
		img_vignetteless = img_contrast - vignette_params;
#endif

	exp(img_vignetteless, img_vignetteless);
	img_vignetteless = img_vignetteless - VIGNETTE_SHIFT;
	img_vignetteless.convertTo(img_vignetteless, CV_8U);

	//Laplacian of Gaussian (LoG) filter
	//(Gradual) Edge detector
	//int kernel_size = 40;
	//float kernel_sigma = 5;
	kernel = LOGkernel(KERNEL_SIZE_LOG, KERNEL_SIGMA_LOG); //Calc LoG filter kernel
	cv::filter2D(img_vignetteless, img_LoG, -1, kernel, Point(-1, -1), 0, BORDER_DEFAULT);
	img_LoG.convertTo(img_LoG, CV_32F);

	//Binarize
	Mat img_bin_log = Mat::zeros(img_LoG.size(), CV_8U); //For Morph
	Mat img_bin_log_high = Mat::zeros(img_LoG.size(), CV_8U), img_bin_log_highest = Mat::zeros(img_LoG.size(), CV_8U); //For Blob
	double min, max;
	cv::minMaxLoc(img_LoG, &min, &max);
	//Select the threshold level based on the maximum number of levels present in the image
	if (max >= 0 && max <= 3)
	{
		img_bin_log = img_LoG > 0;
		img_bin_log_high = img_LoG > 1;
		if (max > 2){
			img_bin_log_highest = img_LoG == max;
		}
	}
	else if (max == 4)
	{
		img_bin_log = img_LoG > 1;
		img_bin_log_high = img_LoG > 2;
		if (max > 3){
			img_bin_log_highest = img_LoG == max;
		}
	}
	else if (max >= 5)
	{
		img_bin_log = img_LoG > 2;
		img_bin_log_high = img_LoG > 3;
		if (max > 4){
			img_bin_log_highest = img_LoG == max;
		}
	}
	else {
		cout << "Unexpected range for LoG filtered image!!! Max = " << max << endl;
	}
	/* --- END PRE-PROCESS --- */

	/* --- START MORPH --- */
	//Find jet-centroid candidates by using morphological operations

	//Morph
	Mat img_bin_log_thin, img_bin_log_high_thin, img_bin_log_thin_spur, img_bin_log_thin_spur_jets;
	bwmorph(img_bin_log, img_bin_log_thin, "Thin"); //Removes connected components's thickness
	bwmorph(img_bin_log_thin, img_bin_log_thin_spur, "Spur", &strel_weights, &LUT_spur); //Keeps circles
	//multiply(img_bin_log_thin, 255 - img_bin_log_thin_spur, img_bin_log_thin_spur_jets, 1, -1); //Multiply with the inverse (computationally more expensive, but same as subtraction; see next line)
	subtract(img_bin_log_thin, img_bin_log_thin_spur, img_bin_log_thin_spur_jets, noArray(), -1); //Subtract circles to maintain lines (presumably jets)

#if PLOT_DEBUG == 1
		normalize(img_LoG, img_LoG, 1, 0, NORM_MINMAX, img_LoG.type());
		imshow("img_LoG", img_LoG); moveWindow("img_LoG", DRAW_COL_2, DRAW_ROW_1);
		imshow("img_bin_log", img_bin_log); moveWindow("img_bin_log", DRAW_COL_1, DRAW_ROW_2);
		imshow("img_bin_log_thin", img_bin_log_thin); moveWindow("img_bin_log_thin", DRAW_COL_1, DRAW_ROW_3);
		imshow("img_bin_log_thin_spur_jets", img_bin_log_thin_spur_jets); moveWindow("img_bin_log_thin_spur_jets", DRAW_COL_1, DRAW_ROW_4);
#endif

	// Get jet contours (similar to connected components; only the outer edge)
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;
	findContours(img_bin_log_thin_spur_jets.clone(), contours, hierarchy, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE, Point(0, 0)); //Connected components

	//Remove (too) long jet candidates
	if (contours.size() != 0)
	{
		for (unsigned i = 0; i < contours.size();)
		{
			if (contours[i].size() > MAX_JET_LENGTH_MORPH)
			{
				//Jet shape too long, so remove
				contours.erase(contours.begin() + i);
			}
			else 
			{
				i++;
			}
		}
	}

#if PLOT_DEBUG == 1
		BW = Mat::zeros(img_bin_log_thin_spur_jets.size(), CV_8U);
		drawContours(BW, contours, -1, Scalar(255, 255, 255), CV_FILLED, 8, noArray(), 2, Point(0, 0));
		imshow("img_bin_log_thin_spur_jets_after_long_removal", BW); moveWindow("img_bin_log_thin_spur_jets_after_long_removal", DRAW_COL_1, DRAW_ROW_5);
#endif

	// Get the centroids
	vector<Scalar> centroids_morph;
	Scalar color(255, 255, 255);
	centroids_morph.resize(contours.size());
	for (unsigned i = 0; i < contours.size(); i++)
	{
		centroids_morph[i] = mean(contours[i]); //Scalar(x,y);
	}

	// Calculate eccentricity and orientation (from Matlab regionprops()) 
	vector<double> majorAxisLength_morph(contours.size()), minorAxisLength_morph(contours.size()), eccentricity_morph(contours.size()), orientation_morph(contours.size());
	computeBlobOrientation(contours, minorAxisLength_morph, majorAxisLength_morph, eccentricity_morph, orientation_morph);

	// Remove circular shaped and (too) long contours
	removeShapeBasedOnEccentricity(contours, eccentricity_morph, orientation_morph, majorAxisLength_morph, centroids_morph);

	/* --- END MORPH --- */

	/* --- START BLOB --- */
	//Find jet-centroid candidates by using blob analysis
	vector<vector<Point>> contours_high, contours_highest;

	//Find contours in high and highest binarized image
	findContours(img_bin_log_high.clone(), contours_high, hierarchy, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE, Point(0, 0)); //Connected components
	findContours(img_bin_log_highest.clone(), contours_highest, hierarchy, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE, Point(0, 0)); //Connected components

	//Combine them into one vectorarray
	contours.clear();
	contours.reserve(contours_high.size() + contours_highest.size()); //Preallocate memory
	contours.insert(contours.begin(), contours_high.begin(), contours_high.end());
	contours.insert(contours.begin() + contours_high.size(), contours_highest.begin(), contours_highest.end());

	//Remove contour components smaller than MIN_COUNTOUR_SIZE pixels
#if PLOT_DEBUG == 1
		//Do it separately, for visual representation only
		for (unsigned i = 0; i < contours_high.size();)
		{
			//High
			if (contours_high[i].size() < MIN_COUNTOUR_SIZE)
			{
				contours_high.erase(contours_high.begin() + i);
			}
			else
			{
				i++;
			}
		}

		for (unsigned int i = 0; i < contours_highest.size();)
		{
			//Highest
			if (contours_highest[i].size() < MIN_COUNTOUR_SIZE)
			{
				contours_highest.erase(contours_highest.begin() + i);
			}
			else
			{
				i++;
			}
		}
#endif

	for (unsigned i = 0; i < contours.size();)
	{
		// Do it all at once
		if (contours[i].size() < MIN_COUNTOUR_SIZE)
		{
			contours.erase(contours.begin() + i);
		}
		else
		{
			i++;
		}
	}

	//Calculate eccentricity and orientation (from Matlab regionprops())
	vector<double> majorAxisLength(contours.size()), minorAxisLength(contours.size()), eccentricity(contours.size()), orientation(contours.size());
	computeBlobOrientation(contours, minorAxisLength, majorAxisLength, eccentricity, orientation);
	
	//Remove circular shaped and (too) long contours
	removeShapeBasedOnEccentricity(contours, eccentricity, orientation, majorAxisLength);

#if PLOT_DEBUG == 1
		BW_high = Mat::zeros(img_bin_log_high.size(), CV_8U);
		drawContours(BW_high, contours_high, -1, color, CV_FILLED, 8, noArray(), 2, Point(0, 0));

		BW_highest = Mat::zeros(img_bin_log_highest.size(), CV_8U);
		drawContours(BW_highest, contours_highest, -1, color, CV_FILLED, 8, noArray(), 2, Point(0, 0));

		imshow("img_bin_log_high", img_bin_log_high); moveWindow("img_bin_log_high", DRAW_COL_2, DRAW_ROW_2);
		imshow("img_bin_log_highest", img_bin_log_highest); moveWindow("img_bin_log_highest", DRAW_COL_3, DRAW_ROW_2);
		imshow("Contours High After Small Removal", BW_high); moveWindow("Contours High After Small Removal", DRAW_COL_2, DRAW_ROW_3);
		imshow("Contours Highest After Small Removal", BW_highest); moveWindow("Contours Highest After Small Removal", DRAW_COL_3, DRAW_ROW_3);
#endif

#if PLOT_DEBUG == 1
		//Draw contours, give each contour a random graylevel color (because they can be on top of each other)
		BW = Mat::zeros(img_bin_log_high.size(), CV_8U);
		RNG rng(7522);
		int gray_value = 255;
		for (unsigned idx = 0; idx < contours.size(); idx++)
		{
			gray_value = rng.uniform(1, 255);
			drawContours(BW, contours, idx, Scalar(gray_value, gray_value, gray_value), CV_FILLED, 8, noArray(), 2, Point(0, 0));
		}
		imshow("Contours After Circle Removal (Combined)", BW); moveWindow("Contours After Circle Removal (Combined)", (DRAW_COL_2 + DRAW_COL_3) / 2, DRAW_ROW_4);
#endif
	
	//Get the centroids
	vector<Scalar> centroids_blob;
	centroids_blob.resize(contours.size());

	for (unsigned i = 0; i < contours.size(); i++)
	{
		centroids_blob[i] = mean(contours[i]);
	}

	/* --- END BLOB --- */

	/* --- START SELECTION --- */
	//Exclude unwanted jet-centroids from all centroids obtained above and pick a final jet location

	//Combine all centroids into one array
	vector<Scalar> centroids_combined;
	centroids_combined.reserve(centroids_morph.size() + centroids_blob.size()); //Preallocate memory
	centroids_combined.insert(centroids_combined.begin(), centroids_morph.begin(), centroids_morph.end());
	centroids_combined.insert(centroids_combined.begin() + centroids_morph.size(), centroids_blob.begin(), centroids_blob.end());

	//Combine orientation
	vector<double> orientation_combined;
	orientation_combined.reserve(orientation_morph.size() + orientation.size()); //Preallocate memory
	orientation_combined.insert(orientation_combined.begin(), orientation_morph.begin(), orientation_morph.end());
	orientation_combined.insert(orientation_combined.begin() + orientation_morph.size(), orientation.begin(), orientation.end());

	//Calculate distances from last known jet coordinate
	Point2f last_coord;
	vector<double> distances(centroids_combined.size());
	if (params.mPreviousCenter == Point2f(-1, -1))
	{
		last_coord = Point2f(ROI.x + ROI_WIDTH / 2, ROI.y + ROI_HEIGHT / 2);
	}
	else
	{
		last_coord = params.mPreviousCenter;
	}

	Point offset;
	Size wholesize;
	img_gray.locateROI(wholesize, offset);
	
	for (unsigned i = 0; i < centroids_combined.size(); i++)
	{
		// since the distance is used just for comparison
		// and since sqrt is a pain in the ass, we can skip it
		//distances[i] = sqrt(pow(last_coord.x - (centroids_combined[i][0] + offset.x), 2) + pow(last_coord.y - (centroids_combined[i][1] + offset.y), 2));
		double dx = last_coord.x - (centroids_combined[i][0] + offset.x);
		double dy = last_coord.y - (centroids_combined[i][1] + offset.y);
		distances[i] = dx*dx + dy*dy;
	}

	// ONLY FOR DEBUG
	//// SAVE BLOBS CENTERS
	//mCenterBlobs.clear();
	//for (unsigned i = 0; i < centroids_combined.size(); i++)
	//{
	//	mCenterBlobs.push_back(Point2f(centroids_combined[i][0] + offset.x, centroids_combined[i][1] + offset.y));
	//}
	//// save to file
	//ofstream outFile;
	//string tmpname = "dataset-0/" + to_string(counter) + ".txt";
	//outFile.open(tmpname);
	//for (unsigned i = 0; i < mCenterBlobs.size(); i++)
	//{
	//	outFile << mCenterBlobs[i].x << " " << mCenterBlobs[i].y << "\n";
	//}
	//outFile.close();
	//// SAVE NUMBER OF BLOBS
	//mNumBlobs.push_back(centroids_combined.size());


	//cout << "distances.size() " << distances.size() << endl;
	//cout << "centroids_combined.size() " << centroids_combined.size() << endl;
	//cout << "orientation_combined.size() " << orientation_combined.size() << endl;

	//Sanity check on distance (prevent relatively large jet-coordinate jumps)
	//Also determine plot colors:
	//	Yellow = centroid candidates found by morph method
	//	Blue = centroid candidates found by blob method
	//	Red = discarded centroid candidates (selection based on too far away from last known jet location)
	//	Green = final centroid (selection based on closest to last known jet location)
	vector<Scalar> line_colors(distances.size());
	unsigned i_color = 0;
	for (unsigned i = 0; i < distances.size(); i_color++)
	{
		if (i_color < centroids_morph.size())
		{
			line_colors[i_color] = Scalar(0, 255, 255); //Yellow
		}
		else
		{
			line_colors[i_color] = Scalar(255, 0, 0); //Blue
		}
		
		// if we do not use sqrt in the distances, we have to use MAX_CENTER_JUMP*MAX_CENTER_JUMP
		if (distances[i] > MAX_CENTER_JUMP*MAX_CENTER_JUMP)
		{
			//cout << "erase " << i << endl;

			distances.erase(distances.begin() + i);
			centroids_combined.erase(centroids_combined.begin() + i);

			// stef did this part
			orientation_combined.erase(orientation_combined.begin() + i);

			line_colors[i_color] = Scalar(0, 0, 255); //Red; discarded centroids
		}
		else 
		{
			i++;
		}
	}

	//Pick final jet centroid and set new ROI location
	if (!distances.empty())
	{
		//not empty == at least one jet found, else if no jets found, use 'old' data for next frame (do not update any value), coordinate will remain stationary
		//Get minimum
		Point min_loc, max_loc;
		minMaxLoc(distances, &min, &max, &min_loc, &max_loc);

		//Update coordinates
		params.mPreviousCenter = Point2d(centroids_combined[min_loc.x][0] + offset.x, centroids_combined[min_loc.x][1] + offset.y);
		params.mInitialROI = Rect_<double>(params.mPreviousCenter.x - ROI_WIDTH / 2, params.mPreviousCenter.y - ROI_HEIGHT / 2, ROI_WIDTH, ROI_HEIGHT);


//        // DEBUG
//        static double tracker_angle = -70*M_PI/180.0; // simulates the tracker -90, 90
//        static bool initTest = true;
//        if(initTest)
//        {
//            mActualOrientation = tracker_angle;
//            mPreviousOrientationTracker = tracker_angle;
//            mPreviousOrientation = tracker_angle;
//            initTest = false;
//        }
//        unwrapOrientation(mPreviousOrientationTracker, tracker_angle, mActualOrientation);

////        tracker_angle += 3*M_PI/180.0; // increment
////        if (tracker_angle > M_PI/2) // -90 and 90
////            tracker_angle -= M_PI;

//        tracker_angle -= 3*M_PI/180.0; // increment
//        if (tracker_angle < -M_PI/2) // -90 and 90
//            tracker_angle += M_PI;

//        cout << "tracker_angle: " << tracker_angle*180/M_PI << endl; // -90 and 90
//        cout << "mPreviousOrientationTracker: " << mPreviousOrientationTracker*180/M_PI << endl; // -180 and 180
//        cout << "mActualOrientation: " << mActualOrientation*180/M_PI << endl; // Real
//        // DEBUG


		//Unwrap the orientation
		//last argument is the output, I overwrite the input one
        unwrapOrientation(mPreviousOrientationTracker, orientation_combined[min_loc.x], mActualOrientation);

		//// DEBUG
		//mOrientation.push_back(mActualOrientation);
		//cout << " " << mActualOrientation << endl;

		isDetected = true;
	}

	/* --- END SELECTION --- */

	/* --- START PLOT --- */
	//Plot centroids data in the original image

#if DRAW_ALL_BLOBS == 1
	//Centroids from Morph (in yellow)
	for (unsigned i = 0; i < centroids_morph.size(); i++)
	{
		line(img_rgb, Point2d(centroids_morph[i][0] - LINE_LENGTH + offset.x, centroids_morph[i][1] + offset.y), Point2d(centroids_morph[i][0] + LINE_LENGTH + offset.x, centroids_morph[i][1] + offset.y), line_colors[i], LINE_THICKNESS, 8, 0); //Horizontal line of center cross
		line(img_rgb, Point2d(centroids_morph[i][0] + offset.x, centroids_morph[i][1] - LINE_LENGTH + offset.y), Point2d(centroids_morph[i][0] + offset.x, centroids_morph[i][1] + LINE_LENGTH + offset.y), line_colors[i], LINE_THICKNESS, 8, 0); //Vertical line of center cross
	}

	//Centroids from Blob (in blue)
	for (unsigned i = 0; i < centroids_blob.size(); i++)
	{
		line(img_rgb, Point2d(centroids_blob[i][0] - LINE_LENGTH + offset.x, centroids_blob[i][1] + offset.y), Point2d(centroids_blob[i][0] + LINE_LENGTH + offset.x, centroids_blob[i][1] + offset.y), line_colors[i + centroids_morph.size()], LINE_THICKNESS, 8, 0); //Horizontal line of center cross
		line(img_rgb, Point2d(centroids_blob[i][0] + offset.x, centroids_blob[i][1] - LINE_LENGTH + offset.y), Point2d(centroids_blob[i][0] + offset.x, centroids_blob[i][1] + LINE_LENGTH + offset.y), line_colors[i + centroids_morph.size()], LINE_THICKNESS, 8, 0); //Vertical line of center cross
	}
#endif

#if DRAW_ORIENTATION == 1
	// DEBUG
	Point2f endPnt;
	endPnt.x = static_cast<float>(ORIENTATION_LINE_LENGTH*cos(mActualOrientation) + params.mPreviousCenter.x);
	endPnt.y = static_cast<float>(ORIENTATION_LINE_LENGTH*sin(mActualOrientation) + params.mPreviousCenter.y);
	// Draw a line
	line(img_rgb, params.mPreviousCenter, endPnt, Scalar(255, 0, 0), LINE_THICKNESS, 8, 0); 
#endif

    // 21.10.2016 Added by Alper Denasi to draw the reference circular path
#if DRAW_CIRCLE_PATH == 1
    circle(img_rgb, Point2f(mDrawCircleCenterX+IMAGE_WIDTH/2,mDrawCircleCenterY+IMAGE_HEIGHT/2), (int) mDrawCircleRadius, Scalar(255, 0, 0), LINE_THICKNESS, 8, 0);
#endif

	//Final jet centroid (in green)
	Scalar line_color(0, 255, 0);
	line(img_rgb, Point2d(-LINE_LENGTH + params.mPreviousCenter.x, params.mPreviousCenter.y), Point2d(LINE_LENGTH + params.mPreviousCenter.x, params.mPreviousCenter.y), line_color, LINE_THICKNESS, 8, 0); //Horizontal line of center cross
	line(img_rgb, Point2d(params.mPreviousCenter.x, -LINE_LENGTH + params.mPreviousCenter.y), Point2d(params.mPreviousCenter.x, LINE_LENGTH + params.mPreviousCenter.y), line_color, LINE_THICKNESS, 8, 0); //Vertical line of center cross

	//draw ROI
	if (params.mDrawROI)
	{
		rectangle(img_rgb, Point(static_cast<int>(params.mPreviousCenter.x - ROI.width*0.5), static_cast<int>(params.mPreviousCenter.y - ROI.height*0.5)),
			Point(static_cast<int>(params.mPreviousCenter.x + ROI.width*0.5), static_cast<int>(params.mPreviousCenter.y + ROI.height*0.5)),
			Scalar(0, 0, 255));
	}
	/* --- END PLOT --- */

	//Out
	//scale(img_LoG, img_LoG);
	//cout << (int)img_bin_log_thin.type() << endl;
	outFrame = img_rgb;
	outCenter = Point2f(static_cast<float>(params.mPreviousCenter.x), static_cast<float>(params.mPreviousCenter.y));
    
    /**************** TO HERE ***********************/
        
    //Computation time
    nowComputationTime = boost::posix_time::microsec_clock::local_time();
	diffComputationTime = nowComputationTime - prevComputationTime;
    
#if SHOW_COMPUTATION_TIME
	cout << "processFrame: " << diffComputationTime.total_milliseconds() << " ms" << endl;
#endif
    
 //   //Store the detected center
	//mCenters.push_back(params.mPreviousCenter);
    
    //Boolean check variable
    mProcessStarted = true;
    
    return isDetected;
}

//bool TrackerMicrojetsCamera::saveNumBlobsToFile(const char* filename) const{
//	cout << "saveNumBlobsToFile...";
//	if (mNumBlobs.size() > 0)
//	{
//		ofstream outFile;
//		outFile.open(filename);
//		for (unsigned i = 0; i < mNumBlobs.size(); i++)
//		{
//			outFile << mNumBlobs[i] << "\n";
//		}
//		outFile.close();
//		cout << "done" << endl;
//		return true;
//	}
//	else
//	{
//		cout << "mNumBlobs is empty, enable save centers from params" << endl;
//		return false;
//	}
//}
