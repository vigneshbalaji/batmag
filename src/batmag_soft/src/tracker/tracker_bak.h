/*
 * Author: Stefano Scheggi
 * @2016
 *
 * This is the interface for the trackers
 * When adding a new tracker, inheritage from this class and implement 
 * the method processFrame
 */

#pragma once

#ifdef _WIN32
#include <windows.h> // for Sleep only
#else
#include <unistd.h> // for usleep obly
#endif

// basic stuff
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <fstream>

// opencv stuff
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

// only to compute time to segment the frame
#include <chrono>
#include <ctime>

// boost
#include <boost/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

// include Configuration file
#include "configuration.h"

using namespace std;
using namespace cv;

typedef struct Point4f { float x; float y; float z; float w; } Point4f;

//reference line parameters
int const ref_line_length=7;
int const ref_line_thickness=2;
const Scalar ref_line_color(0,0,255);//RED

class Tracker
{
protected:

//	// REMOVE
//#if INCREMENT_FRAME_IN_THREAD == 1
//	int counter;
//#endif

	vector<Point2f> mCenters;
	vector<double> mComputationTime; // stores milliseconds used to process each frame
	vector<double> mSamplingTime; // stores milliseconds of the real sampling time at each loop

	bool mProcessStarted;
	Rect mActualProcessROI;

	void updateROI(const Rect&, const Rect&, const Point2f&, Rect&);

#if USE_MULTITHREADING
	// multithreading for tracking
	bool mIsThreadRunning;
	boost::thread *mProcessThread;
    vector<Mat> *mVideoBuffer;
	Mat mInFrameThread, *mOutFrameThread;
	Point2f *mOutCenterThread;
	int *mActualFrame, mInitialFrame;
	TrackerParameters *mParamsThread;
	void processFrameThread(void);
#endif

public:
    Tracker();
    virtual ~Tracker();

    virtual bool processFrame(const Mat&, Mat&, Point2f&) = 0;
    virtual bool drawReference(const Mat&, Mat&, Point2f&);
    
	inline void setIsProcessStarted(bool value){ mProcessStarted = value; };
    bool saveCentersToFile(const char*) const;
    bool saveComputationTimeToFile(const char*) const;
    bool saveSamplingTimeToFile(const char*) const;

    // parameters used by the tracker
    struct TrackerParameters
    {
        bool mDrawContours;
        bool mDrawROI;
        bool mDrawCenter;
        bool mSaveCenter;
        bool mSaveComputationTime;
        bool mSaveSamplingTime;
        double mBlobMinDistance;
        double mBlobMinArea;
        double mThreshold;
        bool mUseKalmanFilter;
        double mSamplingTime;
        //Rect mInitialROI, mImageROI;
        Rect_<double> mInitialROI, mImageROI;
        Point2f mPreviousCenter;

        // set default values
        TrackerParameters() : mDrawContours(true), mDrawROI(true), mDrawCenter(true),
        mUseKalmanFilter(true), mSaveCenter(true), mSaveComputationTime(true),
        mSaveSamplingTime(true),mBlobMinDistance(60), mBlobMinArea(100),
        mThreshold(90), mSamplingTime(0.2)
        {
            // ROI used in the process function for detection
            mInitialROI = Rect(100, 100, 150, 150);
            // From the full image I want to display only the needed part
            mImageROI = Rect(50, 50, 650, 450);
            // previous center
            mPreviousCenter.x = static_cast<float>(mInitialROI.x);
            mPreviousCenter.y = static_cast<float>(mInitialROI.y);
        }
    } mTrackerParameters;

#if USE_MULTITHREADING
    boost::mutex mProcessMutex;
	// multithreading
	void startThread(void);
	inline void stopThread(const bool value){ mIsThreadRunning = !value; };
	// most of these functions link a pointer of this class to some variables 
	// this is used to manage things fast
	inline void setParameters(TrackerParameters &value){ mParamsThread = &value; };
	inline void setOutputFrame(Mat &value){ mOutFrameThread = &value; };
    inline void setFrameToGrab(int &value){ mActualFrame = &value; };
    inline void setVideoBuffer(vector<Mat> &value){ mVideoBuffer = &value; };
	inline void setInitialFrame(int value){ mInitialFrame = value; };
	inline void setTrackedCenter(Point2f &value){ mOutCenterThread = &value; };
#endif
};
