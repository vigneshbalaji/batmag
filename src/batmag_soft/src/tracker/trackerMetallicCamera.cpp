
/*
 * Author: Gert van de Steeg and Stefano Scheggi
 * @2016
 */

#include "trackerMetallicCamera.h"

TrackerMetallicCamera::TrackerMetallicCamera() : mUpsideDown(false), mFacingChecked(false)
{
    // used to calculate the computation time
    nowTime = boost::posix_time::microsec_clock::local_time();
	prevTime = nowTime;
}

TrackerMetallicCamera::~TrackerMetallicCamera(){}

bool TrackerMetallicCamera::processFrame(const Mat& inFrame, Mat& outFrame, Point2f& outCenter)
{
    bool isDetected = false;

	vector<vector<Point>> contours;
	vector<Scalar> centroids;
	Scalar color(255, 255, 255);

	double masked_mean;

	//Convert to HSV
	Mat img_bgr, img_hsv, channel[3], img_hSv, img_hsV, img_med_filt, img_bin, BW;
	img_bgr = inFrame.clone();

	cvtColor(inFrame, img_hsv, CV_BGR2HSV);

	// Split channels
	split(img_hsv, channel);

#if PLOT_DEBUG == 1
		imshow("Hue", channel[0]);
		imshow("Saturation", channel[1]);
		imshow("Value", channel[2]);
#endif

	// Apply median filter to reduce noise and smooth image
	if (!mUpsideDown)
	{
		img_hSv = channel[1];
		medianBlur(img_hSv, img_med_filt, BLUR_SIZE);
		img_bin = img_med_filt > THRESHOLD_NOTUPSIDEDOWN;
	}
	else
	{
		img_hsV = channel[2];
		medianBlur(img_hsV, img_med_filt, BLUR_SIZE);
		img_bin = img_med_filt < THRESHOLD_UPSIDEDOWN;
	}

#if PLOT_DEBUG == 1 
	imshow("img_median_filt", img_med_filt); 
	imshow("img_bin", img_bin); 
#endif

	//Remove small contours (<25 pixels)
	vector<Vec4i> hierarchy;
	findContours(img_bin.clone(), contours, hierarchy, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
	for (unsigned i = 0; i < contours.size();)
	{
		if (contours[i].size() < SMALL_CONTOUR_THRESHOLD)
		{
			contours.erase(contours.begin() + i);
		}
		else
		{
			i++;
		}
	}

	BW = Mat::zeros(img_bin.size(), CV_8U);
	drawContours(BW, contours, -1, color, CV_FILLED, 8, noArray(), 2, Point(0, 0));

#if PLOT_DEBUG == 1
	imshow("BW", BW); 
#endif
	
	BW = BW / 255; //Make it logical (0 || 1) mask

	if (!mFacingChecked)
	{
		img_hsV = channel[2];
		img_hsV = img_hsV.mul(BW);
	
#if PLOT_DEBUG == 1		
		imshow("masked_hsV", img_hsV);
#endif

		masked_mean = sum(img_hsV)[0] / sum(BW)[0];
		mUpsideDown = masked_mean < THRESHOLD_MASK; //It is dark in the Value channel, so gripper upsidedown
		mFacingChecked = true;

#if PLOT_DEBUG == 1	
		if (mUpsideDown)
		{
			cout << "The gripper is positioned upside down!" << endl;
		}
		else
		{
			cout << "The gripper is positioned rightside up!" << endl;
		}
#endif
	}

	// Plot, if found only one object
	if (contours.size() == 1)
	{
		centroids.resize(contours.size());

		for (unsigned i = 0; i < contours.size(); i++)
		{
			centroids[i] = mean(contours[i]);
		}

		Scalar line_color(0, 255, 0);

        mTrackerParameters.mPreviousCenter = Point2d(centroids[0][0], centroids[0][1]);
        line(img_bgr, Point2d(-line_length + mTrackerParameters.mPreviousCenter.x, mTrackerParameters.mPreviousCenter.y), Point2d(line_length + mTrackerParameters.mPreviousCenter.x, mTrackerParameters.mPreviousCenter.y), line_color, line_thickness, 8, 0); //Horizontal line of center cross
        line(img_bgr, Point2d(mTrackerParameters.mPreviousCenter.x, -line_length + mTrackerParameters.mPreviousCenter.y), Point2d(mTrackerParameters.mPreviousCenter.x, line_length + mTrackerParameters.mPreviousCenter.y), line_color, line_thickness, 8, 0); //Vertical line of center cross

#if PLOT_DEBUG == 1	
		imshow("result", img_bgr);
#endif

		isDetected = true;
	}
	else
	{
		cout << "More than one object detected." << endl;
	}

	outFrame = img_bgr;
    outCenter = Point2f(static_cast<float>(mTrackerParameters.mPreviousCenter.x), static_cast<float>(mTrackerParameters.mPreviousCenter.y));

    return isDetected;
}
