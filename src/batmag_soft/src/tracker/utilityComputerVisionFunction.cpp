#include "utilityComputerVisionFunction.h"

/**
* These definitions are used by the morphological operations bwmorph()
*/
//http://stackoverflow.com/questions/5093460/how-to-convert-an-enum-type-variable-to-a-string
#define X_DEFINE_ENUM_WITH_STRING_CONVERSIONS_TOSTRING_CASE(r, data, elem)				\
    case elem : return BOOST_PP_STRINGIZE(elem);

//Enum to String
#define DEFINE_ENUM_WITH_STRING_CONVERSIONS(name, enumerators)							\
	enum name {																			\
		BOOST_PP_SEQ_ENUM(enumerators)													\
	};																					\
																						\
	inline char* EnumToString(name v) {													\
		switch (v) {																	\
			BOOST_PP_SEQ_FOR_EACH(X_DEFINE_ENUM_WITH_STRING_CONVERSIONS_TOSTRING_CASE,name,enumerators)\
            default: return "[Unknown " BOOST_PP_STRINGIZE(name) "]";					\
		}																				\
	}

DEFINE_ENUM_WITH_STRING_CONVERSIONS(opDefs, (opShrink)(opSpur)(opThin)(opThicken)(opSkeleton)(opClose)(opOpen)
	(opDilate)(opErode)(opDiag)(opEndpoints)(opBothat)(opBranchpoints)(opBridge)(opClean)(opFatten)(opFill)
	(opHbreak)(opMajority)(opRemove)(opPerim4)(opPerim8));

/**
* Apply morphological operations to the src image
* Inspired by Matlab bwmorph(), not everything is implemented, but the options are there for expanding when needed
*
* Parameters:
* 		src    Binary image source
* 		dst    Binary image destination
* 		opStr    operation type
* 		param0    optional parameter1
* 		param1    optional parameter2
*/
void bwmorph(Mat& src, Mat& dst, String opStr, void *param0, void *param1)
{
	int opInt = -1;

	Mat local_src = src.clone();
	Mat local_dst = dst.clone();
	Mat local_comp;

	//Create bwmorph() String to Enum mapping, because switch-statement only works on integer values
	map<string, opDefs> dictionary;
	dictionary["Shrink"] = opShrink;
	dictionary["Spur"] = opSpur;
	dictionary["Thin"] = opThin;
	dictionary["Thicken"] = opThicken;
	dictionary["Skel"] = opSkeleton;
	dictionary["Close"] = opClose;
	dictionary["Open"] = opOpen;
	dictionary["Dilate"] = opDilate;
	dictionary["Erode"] = opErode;
	dictionary["Diag"] = opDiag;
	dictionary["Endpoints"] = opEndpoints;
	dictionary["Bothat"] = opBothat;
	dictionary["Branchpoints"] = opBranchpoints;
	dictionary["Bridge"] = opBridge;
	dictionary["Clean"] = opClean;
	dictionary["Fatten"] = opFatten;
	dictionary["Fill"] = opFill;
	dictionary["Hbreak"] = opHbreak;
	dictionary["Majority"] = opMajority;
	dictionary["Remove"] = opRemove;
	dictionary["Perim4"] = opPerim4;
	dictionary["Perim8"] = opPerim8;

	map<string, opDefs>::iterator it = dictionary.find(opStr);
	if (it != dictionary.end()){
		opInt = (it->second);
	}

	switch (opInt){//Which operation do you want to do?
	case opShrink://Shrinks objects to points. It removes pixels so that objects without holes shrink to a point, and objects with holes shrink to a connected ring halfway between each hole and the outer boundary.
		break;

	case opSpur: //Spur removal, aka pruning
		local_src = local_src / 255; //Convert to binary image range [0 - 1]
		local_src.convertTo(local_src, CV_32F); //Convert to floating point
		local_src = 1 - local_src; //Image complement

		//Iteratively remove endpoints until the image does not change anymore
		int i;
		for (i = 1; i < 500; i++)
		{
			// not sure about the mat class if the cast is ok
			spur(local_src, local_dst, *(Mat*)param0, *(Mat*)param1); //Detect and remove endpoints

			compare(local_src, local_dst, local_comp, CMP_NE);
			if (!(sum(local_comp)[0] > 0))
			{
				//local_src == local_dst, so done
				break;
			}
			local_src = local_dst.clone();
		}
		local_dst.convertTo(local_dst, CV_8U); //Convert to unsigned char
		local_dst = 1 - local_dst; //Reverse image complement
		local_dst = local_dst * 255; //Convert to 8U image, binary range [0 - 255]
		dst = local_dst.clone();

		/*
		//Already taken care of by adding [1 1 1; 1 0 1; 1 1 1] to LUT_spur, which is not in the Matlab implementation
		//Remove small 1-pixel CCs (noise)
		findContours(local_dst, contours, {}, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
		for (vector<vector<Point> >::iterator it = contours.begin(); it != contours.end(); it++)
		{
		if (it->size() < 2){ //Remove contours with size of 1 pixel
		it = contours.erase(it);
		it--;
		}
		}
		dst = Mat::zeros(src.size(), CV_8U);
		drawContours(dst, contours, -1, color, 1, 8, noArray(), 2, Point(0,0));
		*/
		break;

	case opThin: //Thins objects (CCs) to lines. It removes pixels such that an object without holes shrinks to a minimally connected stroke, and an object with holes shrinks to a connected ring halfway between each hole and the outer boundary
		thinning(src, dst);
		break;

	case opThicken: //Thickens objects by adding pixels to the exterior of objects until doing so would result in previously unconnected objects being 8-connected. 
		break;

	case opSkeleton: //Removes pixels on the boundaries of objects but does not allow objects to break apart.The pixels remaining make up the image skeleton.
		break;

	case opClose: //Performs morphological closing (dilation followed by erosion).
		break;

	case opOpen: //Performs morphological opening (erosion followed by dilation).
		break;

	case opDilate: //The value of the output pixel is the maximum value of all the pixels in the input pixel's neighborhood. In a binary image, if any of the pixels is set to the value 1, the output pixel is set to 1.
		break;

	case opErode: //The value of the output pixel is the minimum value of all the pixels in the input pixel's neighborhood. In a binary image, if any of the pixels is set to 0, the output pixel is set to 0.
		break;

	case opDiag: //Uses diagonal fill to eliminate 8-connectivity of the background.
		break;

	case opEndpoints: //Finds end points of skeleton.
		break;

	case opBothat: //Performs the morphological "bottom hat" operation, returning the image minus the morphological closing of the image (dilation followed by erosion).
		break;

	case opBranchpoints: //Find branch points of skeleton.
		break;

	case opBridge: //Bridges unconnected pixels, that is, sets 0-valued pixels to 1 if they have two nonzero neighbors that are not connected.
		break;

	case opClean: //Removes isolated pixels(individual 1s that are surrounded by 0s), such as the center pixel in this pattern.
		break;

	case opFatten:
		break;

	case opFill: //Fills isolated interior pixels (individual 0s that are surrounded by 1s), such as the center pixel in this pattern.
		break;

	case opHbreak: //Removes H-connected pixels.
		break;

	case opMajority: //Sets a pixel to 1 if five or more pixels in its 3-by-3 neighborhood are 1s; otherwise, it sets the pixel to 0.
		break;

	case opRemove: //Removes interior pixels. This option sets a pixel to 0 if all its 4-connected neighbors are 1, thus leaving only the boundary pixels on.
		break;

	case opPerim4:
		break;

	case opPerim8:
		break;

	default:
		cout << "bwmorph:unknownOperation" << opStr << endl;
	}
}

/**
* Perform one thinning iteration.
* Normally you wouldn't call this function directly from your code.
*
* Parameters:
* 		im    Binary image with range = [0,1]
* 		iter  0=even, 1=odd
*/
void thinningIteration(cv::Mat& img, int iter)
{
	CV_Assert(img.channels() == 1);
	CV_Assert(img.depth() != sizeof(uchar));
	CV_Assert(img.rows > 3 && img.cols > 3);

	cv::Mat marker = cv::Mat::zeros(img.size(), CV_8UC1);

	int nRows = img.rows;
	int nCols = img.cols;

	if (img.isContinuous()) 
	{
		nCols *= nRows;
		nRows = 1;
	}

	int x, y;
	uchar *pAbove;
	uchar *pCurr;
	uchar *pBelow;
	uchar *nw, *no, *ne; //North (pAbove)
	uchar *we, *me, *ea;
	uchar *sw, *so, *se; //South (pBelow)

	uchar *pDst;

	// Initialize row pointers
	pAbove = nullptr;
	pCurr = img.ptr<uchar>(0);
	pBelow = img.ptr<uchar>(1);

	for (y = 1; y < img.rows - 1; ++y) {
		//Shift the rows up by one
		pAbove = pCurr;
		pCurr = pBelow;
		pBelow = img.ptr<uchar>(y + 1);

		pDst = marker.ptr<uchar>(y);

		//Initialize col pointers
		no = &(pAbove[0]);
		ne = &(pAbove[1]);
		me = &(pCurr[0]);
		ea = &(pCurr[1]);
		so = &(pBelow[0]);
		se = &(pBelow[1]);

		for (x = 1; x < img.cols - 1; ++x) {
			//Shift col pointers left by one (scan left to right)
			nw = no;
			no = ne;
			ne = &(pAbove[x + 1]);
			we = me;
			me = ea;
			ea = &(pCurr[x + 1]);
			sw = so;
			so = se;
			se = &(pBelow[x + 1]);

			int A = (*no == 0 && *ne == 1) + (*ne == 0 && *ea == 1) +
				(*ea == 0 && *se == 1) + (*se == 0 && *so == 1) +
				(*so == 0 && *sw == 1) + (*sw == 0 && *we == 1) +
				(*we == 0 && *nw == 1) + (*nw == 0 && *no == 1);
			int B = *no + *ne + *ea + *se + *so + *sw + *we + *nw;
			int m1 = iter == 0 ? (*no * *ea * *so) : (*no * *ea * *we);
			int m2 = iter == 0 ? (*ea * *so * *we) : (*no * *so * *we);

			if (A == 1 && (B >= 2 && B <= 6) && m1 == 0 && m2 == 0)
				pDst[x] = 1;
		}
	}

	img &= ~marker;
}

/**
* Function for thinning the given binary image
* From: https://github.com/bsdnoobz/zhang-suen-thinning/blob/master/thinning.cpp
*
* Parameters:
* 		src  The source image, binary with range = [0,255]
* 		dst  The destination image
*/
void thinning(const cv::Mat& src, cv::Mat& dst)
{
	dst = src.clone();
	dst /= 255;         // convert to binary image

	Mat prev = cv::Mat::zeros(dst.size(), CV_8UC1);
	Mat diff;

	do {
		thinningIteration(dst, 0);
		thinningIteration(dst, 1);
		cv::absdiff(dst, prev, diff);
		dst.copyTo(prev);
	} while (cv::countNonZero(diff) > 0);

	dst *= 255;
}

/*
* Histogram stretching, to maximize contrast and use all available gray-levels
*
* Parameters:
* 		src
* 		dst
*       tolerance
*/
void imadjust(const Mat& src, Mat& dst, double tol, Vec2i in, Vec2i out)
{
	//src : input CV_8UC1 image
	//dst : output CV_8UC1 imge
	//tol : tolerance, from 0 to 100.
	//in  : src image bounds
	//out : dst image buonds

	dst = src.clone();

	tol = max(0.0, min(100.0, tol)) / 100.0;

	if (tol > 0)
	{
		//Histogram
		vector<unsigned char> hist(256, 0);
		for (int r = 0; r < src.rows; ++r) {
			for (int c = 0; c < src.cols; ++c) {
				hist[src.at<unsigned char>(r, c)]++;
			}
		}

		//Establish the number of bins
		int histSize = 256;

		//Set the ranges
		float range[] = { 0, 256 };
		const float* histRange = { range };
		bool uniform = true;
		bool accumulate = false;

		Mat img_hist;

		//Compute the histograms:
		calcHist(&src, 1, 0, Mat(), img_hist, 1, &histSize, &histRange, uniform, accumulate);

		//Mat to vector
		//(unsigned short)img_hist.at<float>(ii)
		std::vector<float> array_hist;
		img_hist.col(0).copyTo(array_hist);

		//(Normalized) Cumulative histogram
		vector<float> cum_new = array_hist;
		for (unsigned int i = 1; i < array_hist.size(); ++i) {
			cum_new[i] = cum_new[i - 1] + array_hist[i];
		}
		std::transform(cum_new.begin(), cum_new.end(), cum_new.begin(), std::bind2nd(std::multiplies<double>(), 1 / cum_new.at(cum_new.size() - 1))); // Divide each element by the sum of cum_new

		//Calc low and high boundaries
		int index_low = 0, index_high = 0;
		for (unsigned int i = 0; i < cum_new.size(); i++){
			if (cum_new[i] >= tol && index_low == 0){
				index_low = i - 1;
			}
			if (cum_new[i] >(1 - tol) && index_high == 0){
				index_high = i;
			}
		}
		if (index_low == index_high){
			index_low = 0;
			index_high = 255;
		}

		in[0] = index_low;
		in[1] = index_high;

		/*
		// Draw the histograms
		int hist_w = 256; int hist_h = 256;
		int bin_w = cvRound((double)hist_w / histSize);

		Mat histImage(hist_h, hist_w, CV_8UC3, Scalar(0, 0, 0));

		// Normalize the result to [ 0, histImage.rows ]
		normalize(img_hist, img_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat());

		// Draw for each channel
		for (int i = 1; i < histSize; i++)
		{
		line(histImage, Point(bin_w*(i - 1), hist_h - cvRound(img_hist.at<float>(i - 1))),
		Point(bin_w*(i), hist_h - cvRound(img_hist.at<float>(i))),
		Scalar(255, 0, 0), 2, 8, 0);
		}

		// Display
		namedWindow("calcHist", CV_WINDOW_AUTOSIZE);
		imshow("calcHist", histImage);

		// Cumulative histogram
		vector<unsigned char> cum = hist;
		for (int i = 1; i < hist.size(); ++i) {
		cum[i] = cum[i - 1] + hist[i];
		}

		showUCharGraph("Pixel Values", &cum[0], cum.size());

		// Compute bounds
		int total = src.rows * src.cols;
		int low_bound = total * tol / 100;
		int upp_bound = total * (100 - tol) / 100;
		in[0] = distance(cum.begin(), lower_bound(cum.begin(), cum.end(), low_bound));
		in[1] = distance(cum.begin(), lower_bound(cum.begin(), cum.end(), upp_bound));
		*/
	}

	//Apply Stretching
	float scale = float(out[1] - out[0]) / float(in[1] - in[0]);
	for (int r = 0; r < dst.rows; ++r)
	{
		for (int c = 0; c < dst.cols; ++c)
		{
			int vs = max(src.at<unsigned char>(r, c) - in[0], 0);
			int vd = min(int(vs * scale + 0.5f) + out[0], out[1]);
			dst.at<unsigned char>(r, c) = saturate_cast<uchar>(vd);
		}
	}
}

/*
* Create 2D gaussian
*
* Parameters:
* 		x
* 		y
*       sigma
*/
double LoG(int x, int y, float sigma)
{
	double xy = (x*x + y*y) / (2 * sigma*sigma);
	return -1.0 / (M_PI * pow(sigma, 4)) * (1.0 - xy) * exp(-xy);
}

/*
* Create Laplacian of Gaussian filter kernel
*
* Parameters:
* 		size of kernel
* 		sigma of kernel
*/
Mat LOGkernel(int size, float sigma)
{
	Mat kernel(size, size, CV_64F);
	int halfsize = size / 2;
	for (int x = -halfsize; x < halfsize; ++x) {
		for (int y = -halfsize; y < halfsize; ++y) {
			kernel.at<double>(x + halfsize, y + halfsize) = LoG(x, y, sigma);
		}
	}
    return kernel;
}

/*
* Function for removing spurs from the given binary image (aka pruning)
*
* Parameters:
* 		src  The floating point source image with binary with range = [0,1]
* 		dst  The destination image
*/
void spur(const Mat& src, Mat& dst, Mat& strel_weights, Mat& lut_spur)
{
	// See http://blogs.mathworks.com/steve/2008/05/07/lookup-tables-for-binary-image-processing/
	// See http://blogs.mathworks.com/steve/2008/05/13/lookup-tables-makelut-applylut/
	// See http://nl.mathworks.com/help/images/ref/bwlookup.html
	// See http://nl.mathworks.com/help/images/ref/applylut.html

	//Filter/convolve the image with the weights matrix
	Mat bw_conv;

	//int border = 2; //Add 0-border to image, because of convolution of the image edges
	//copyMakeBorder(src, bw_conv, border, border, border, border, BORDER_CONSTANT, Scalar(0));
	//filter2D(bw_conv, bw_conv, -1, this->get_strel_weights(), Point(-1, -1), 0, BORDER_DEFAULT);
	filter2D(src, bw_conv, -1, strel_weights, Point(-1, -1), 0, BORDER_DEFAULT);

	////Remove conv tail
	//bw_conv = bw_conv.colRange(border, bw_conv.cols - border);
	//bw_conv = bw_conv.rowRange(border, bw_conv.rows - border);

	////Find endpoints (in Matlab notation: endpoints = LUT_spur(bw_conv);) 
	//Mat endpoints = bw_conv.clone(); //Enforce continuous data, so looping is faster
	int nCols = bw_conv.cols * bw_conv.rows; //If border extension on, replace bw_conv for endpoints;
	float* dst_row_ptr;
	float* LUT_spur_ptr;
	dst_row_ptr = bw_conv.ptr<float>(0);
	LUT_spur_ptr = lut_spur.ptr<float>(0);
	for (int j = 0; j < nCols; j++){
		dst_row_ptr[j] = LUT_spur_ptr[(int)dst_row_ptr[j]];
	}

	//Remove endpoints
	bitwise_xor(src, bw_conv, dst);
}

/*
* Function for normalizing src values between 0-1
*
* Parameters:
* 		src  Input matrix that needs to be scaled
* 		dst  Scaled output matrix
*/
void scale_matrix(const Mat& src, Mat& dst){
    double min, max;
    cv::minMaxLoc(src, &min, &max);
    dst = src - min;
    cv::minMaxLoc(dst, &min, &max);
    dst = dst / max;
}

/*
 * 12.09.2016: Alper
 * I included the angle unwrap function here temporarily
 * Currently it is commented, once the software works it will be uncommented
 * The functions convertpixels2metres and camera2inertial should also be included to the utility functions
 */
/*
 * Function to unwrap an angle between 0 and 2*pi (similar to Matlab function)
 */
//void angle_unwrap(const double& prev, const double& act, double& corr)
//{
//    // define the switching angle
//    double cutoff = M_PI;

//    // incremental phase variation
//    double dp = act-prev;

//    // equivalent phase variation in [-pi, pi]
//    // MATLAB: dps = mod(dp+pi,2*pi) - pi;
//    double dps = (dp+M_PI)-floor((dp+M_PI)/(2*M_PI))*(2*M_PI)-M_PI;

//    // preserve variation sign for +pi vs. -pi
//    // MATLAB: dps(dps==-pi & dp>0,:) = pi;
//    if ((dps == -M_PI) && (dp > 0))
//    {
//        dps = M_PI;
//    }

//    // incremental phase correction
//    // MATLAB: dp_corr = dps - dp;
//    double dp_corr = dps-dp;

//    // Ignore correction when incremental variation is smaller than cutoff
//    // MATLAB: dp_corr(abs(dp)<cutoff,:) = 0;
//    if (fabs(dp) < cutoff)
//    {
//        dp_corr = 0;
//    }

//    // compute the corrected angle
//    corr = act + dp_corr;
//}

/*
 * 21.10.2016
 */
// Function to convert pixels to metres
void convertPixels2Metres(const double& pixels, double& metres)
{
    // convert the given unit from pixels to metres
    metres = pixels*(pixel2metre_ratio);
}

/*
 * 21.10.2016
 */
// Function to convert metres to pixels
void convertMetres2Pixels(const double& metres, double& pixels)
{
    // convert the given unit from metres to pixels
    pixels = metres/(pixel2metre_ratio);
}

/*
 * 04.11.2016
 */
void convertPixels2Millimetres(const double& pixels, double& millimeters)
{
    millimeters = pixels*(pixel2millimetre_ratio);
}
/*
 * 04.11.2016
 */
void convertMillimeters2Pixels(const double& millimeters, double& pixels)
{
    pixels = millimeters/(pixel2millimetre_ratio);
}
/*
 * 10.11.2016
 */
void drawSetPoint(const Point& SetPoint, Mat inFrame)
{
    Scalar line_color(0, 0, 255);
    line(inFrame, Point2d(-LINE_LENGTH_SETPOINT + SetPoint.x, SetPoint.y), Point2d(LINE_LENGTH_SETPOINT + SetPoint.x, SetPoint.y), line_color, LINE_THICKNESS_SETPOINT, 8, 0); //Horizontal line of center cross
    line(inFrame, Point2d(SetPoint.x, -LINE_LENGTH_SETPOINT + SetPoint.y), Point2d(SetPoint.x, LINE_LENGTH_SETPOINT + SetPoint.y), line_color, LINE_THICKNESS_SETPOINT, 8, 0); //Vertical line of center cross
}
/*
 * 11.11.2016
 */
void drawCenterAxes(Mat inFrame)
{
    int LINE_LENGTH_AXES = 100;
    Scalar line_color(0, 0, 0);
    line(inFrame, Point2d(-LINE_LENGTH_AXES + IMAGE_WIDTH/2, IMAGE_HEIGHT/2), Point2d(LINE_LENGTH_AXES + IMAGE_WIDTH/2, IMAGE_HEIGHT/2), line_color, 2, 8, 0); //Horizontal line of center cross
    line(inFrame, Point2d(IMAGE_WIDTH/2, -LINE_LENGTH_AXES + IMAGE_HEIGHT/2), Point2d(IMAGE_WIDTH/2, LINE_LENGTH_AXES + IMAGE_HEIGHT/2), line_color, 2, 8, 0); //Vertical line of center cross
}

int factorial(int number)
{
    int temp;
    if(number<=1)
    {
        return 1;
    }
    temp = number*factorial(number-1);
    return temp;
}
