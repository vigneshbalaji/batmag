
/*
 * Author: Stefano Scheggi
 * @2016
 */

#pragma once

// basic stuff
#include <iostream>
#include <string>

using namespace std;

// set some parameters here
#define SHOW_COMPUTATION_TIME 0 // Print the tracker computation time
#define FAST_DRAWING 1 // 1 = skip drawing contours, 0 it is possible to draw the contours
#define USE_MULTITHREADING 0
#define DRAW_CONTOURS 0
#define DRAW_ROI 1
#define USE_KALMAN_FILTER 1
#define SAMPLING_TIME 0.1 // 0.04 is not that bad, do not go faster than video framerate
#define SAVE_CENTERS 0
#define SAVE_COMPUTATION_TIME 0
#define SAVE_SAMPLING_TIME 0
#define THRESHOLD 90.0 // Binary threshold for detection
#define INITIAL_FRAME 0 // Initial frame to process
#define FINAL_FRAME 100 // Final frame to process, if -1 load untill the end of the video
#define SHOW_LOOP_TIME 1 // Print the effective loop sampling time NO MULTITHREADING
#define SHOW_PROCESS_TIME 0 // Print the effective tracker computation time NO MULTITHREADING
#define LOOP_VIDEO 1 // 1 = loop video, 0 = exit when the final frame of the video is processed
#define LINE_LENGTH_SETPOINT 7 //
#define LINE_THICKNESS_SETPOINT 2 //

#define M_PI 3.14159265358979323846  //pi
#define ROI_WIDTH 200
#define ROI_HEIGHT 200
#define IMAGE_WIDTH 2048 // in pixels
#define IMAGE_HEIGHT 2048 // in pixels
#define pixel2metre_ratio 0.000006493506494 // 0.0025/385.0 2.5 millimeters correspond to 385 pixels (100 micron microparticles)
#define pixel2millimetre_ratio 0.006493506494 // 2.5/385.0 2.5 millimeters correspond to 385 pixels (100 micron microparticles)

// Kalman noise values are set in TrackerHydrogelUS::KalmanInit

const int ROI_INITIAL[4] = {0, 0, ROI_WIDTH, ROI_HEIGHT};

// FOR OPENING/CLOSING
//const double[4] ROI_INITIAL = (250, 100, 100, 100);

const string FILENAME_CENTERS = "centers.txt";
const string FILENAME_COMPUTATION_TIME = "computationTime.txt";
const string FILENAME_SAMPLING_TIME = "samplingTime.txt";

// WINDOWS HD
//const string FILE_VIDEO = "../../../../dataset/dataset-ultrasound-1/raw.mp4";
//const string FILE_VIDEO = "../../../../dataset/dataset-ultrasound-2/raw.mp4";
//const string FILE_VIDEO = "../../../../dataset/dataset-ultrasound-closing-1/raw.mp4";
// MAC HD
//const string FILE_VIDEO = "../../../dataset-ultrasound-1/raw.mp4";
//const string FILE_VIDEO = "../../../dataset-ultrasound-2/raw.mp4";
//const string FILE_VIDEO = "../../../dataset-ultrasound-closing-1/raw.mp4";

// WINDOWS HD
//const string FILE_VIDEO = "../../../datasets/microjets_vid_1.avi";
// MAC HD

const string FILE_VIDEO = "D:/Ultrasound Enhancement/Data Set/2016-10-28 - MicroParticle Stefano/part1.avi";
//const string FILE_VIDEO = "D:/Ultrasound Enhancement/Data Set/2016-10-28 - MicroParticle Stefano/part2.avi";
//const string FILE_VIDEO = "D:/Ultrasound Enhancement/Data Set/2016-10-28 - MicroParticle Stefano/part3.avi";
//const string FILE_VIDEO = "D:/Ultrasound Enhancement/Data Set/2016-10-28 - MicroParticle Stefano/part4.avi";
//const string FILE_VIDEO = "D:/Ultrasound Enhancement/Data Set/2016-10-28 - MicroParticle Stefano/part5.avi";
//const string FILE_VIDEO = "D:/Ultrasound Enhancement/Data Set/2016-10-28 - MicroParticle Stefano/part6.avi";
//const string FILE_VIDEO = "D:/Ultrasound Enhancement/Data Set/2016-10-28 - MicroParticle Stefano/part7.avi";
//const string FILE_VIDEO = "D:/Ultrasound Enhancement/Data Set/2016-10-28 - MicroParticle Stefano/part8.avi";
