#pragma once

#define _USE_MATH_DEFINES

// basic stuff
#include <iostream>
#include <vector>
#include <string>
#include <cmath>
#include <functional>
#include <algorithm>

// opencv stuff
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

// boost
#include <boost/preprocessor.hpp>

#include "configuration.h"

using namespace std;
using namespace cv;

#define RADIANS_TO_DEGREES(radians) ((radians) * 57.2957795131f)
#define DEGREES_TO_RADIANS(angle) ((angle) * 0.01745329251f)

/**
* Apply morphological operations to the src image
* Inspired by Matlab bwmorph(), not everything is implemented, but the options are there for expanding when needed
*
* Parameters:
* 		src    Binary image source
* 		dst    Binary image destination
* 		opStr    operation type
* 		param0    optional parameter1
* 		param1    optional parameter2
*/
void bwmorph(Mat&, Mat&, String, void* = nullptr, void* = nullptr);

/**
* Perform one thinning iteration.
* Normally you wouldn't call this function directly from your code.
*
* Parameters:
* 		im    Binary image with range = [0,1]
* 		iter  0=even, 1=odd
*/
void thinningIteration(Mat&, int);

/**
* Function for thinning the given binary image
* From: https://github.com/bsdnoobz/zhang-suen-thinning/blob/master/thinning.cpp
*
* Parameters:
* 		src  The source image, binary with range = [0,255]
* 		dst  The destination image
*/
void thinning(const Mat&, Mat&);

/*
* Histogram stretching, to maximize contrast and use all available gray-levels
*
* Parameters:
* 		src
* 		dst
*       tolerance
*/
void imadjust(const Mat&, Mat&, double = 1, Vec2i = Vec2i(0, 255), Vec2i = Vec2i(0, 255));

/*
* Create 2D gaussian
*
* Parameters:
* 		x
* 		y
*       sigma
*/
double LoG(int, int, float);

/*
* Create Laplacian of Gaussian filter kernel
*
* Parameters:
* 		size of kernel
* 		sigma of kernel
*/
Mat LOGkernel(int, float);

/*
* Function for removing spurs from the given binary image (aka pruning)
*
* Parameters:
* 		src  The floating point source image with binary with range = [0,1]
* 		dst  The destination image
*       strel weights
*       lut spur matrix
*/
void spur(const Mat&, Mat&, Mat&, Mat&);

/*
* Function for normalizing src values between 0-1
*
* Parameters:
* 		src  Input matrix that needs to be scaled
* 		dst  Scaled output matrix
*/
void scale_matrix(const Mat&, Mat&);

/*
 * 21.10.2016
 */
// Function to convert pixels to metres
void convertPixels2Metres(const double&, double&);

/*
 * 21.10.2016
 */
// Function to convert metres to pixels
void convertMetres2Pixels(const double&, double&);

/*
 * 04.11.2016
 */
// Function to convert pixels to millimetres
void convertPixels2Millimetres(const double&, double&);

/*
 * 04.11.2016
 */
// Function to convert millimetres to pixels
void convertMillimeters2Pixels(const double&, double&);

/*
 * 04.11.2016
 */
// Function to convert to micrometers

/*
 * 10.11.2016
 */
// Function to draw setpoint on a given frame
void drawSetPoint(const Point&, Mat);

/*
 * 11.11.2016
 */
void drawCenterAxes(Mat inFrame);

// Function to compute the factorial of a given integer
int factorial(int number);
