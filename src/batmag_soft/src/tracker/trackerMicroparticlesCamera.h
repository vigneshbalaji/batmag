
/*
 * Authors: Stefano Scheggi, Gert van de Steeg
 * @2016
 */

#pragma once

#ifdef _WIN32
#include <windows.h> // for Sleep only
#else
#include <unistd.h> // for usleep obly
#endif

// defines
#define KERNEL_SIZE_LOG 50
#define KERNEL_SIGMA_LOG 5
#define PLOT_DEBUG 0
#define USE_ROI 1
#define LINE_LENGTH 7
#define LINE_THICKNESS 2

// Tracker interface
#include "tracker.h"

// basic stuff
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

// opencv stuff
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

// only to compute time to segment the frame
#include <boost/date_time/posix_time/posix_time.hpp>

// multithreading
#include <boost/thread.hpp>

// Utility functions
#include "utilityComputerVisionFunction.h"

// include Configuration file
#include "configuration.h"

using namespace std;
using namespace cv;

class TrackerMicroparticlesCamera:public Tracker
{
    // computation time
    boost::posix_time::ptime nowTime, prevTime;
    boost::posix_time::time_duration diffTime;
    vector<double> mComputationTime; // stores milliseconds used to process each frame
    vector<double> mSamplingTime; // stores milliseconds of the real sampling time at each loop
    
	int mFrameCounter;
    bool mProcessStarted;
    Rect mActualProcessROI;
    KalmanFilter mKF;
    vector<Point2f> mCenters;

#if USE_MULTITHREADING
	// multithreading for tracking
	bool mIsThreadRunning;
	boost::thread *mProcessThread;
    vector<Mat> *mVideoBuffer;
	Mat mInFrameThread, *mOutFrameThread;
	Point2f *mOutCenterThread;
	int *mActualFrame, mInitialFrame;
	TrackerParameters *mParamsThread;
	void processFrameThread(void);
#endif

public:
	TrackerMicroparticlesCamera();
	~TrackerMicroparticlesCamera();

	inline void setIsProcessStarted(bool value){ mProcessStarted = value; };
    bool processFrame(const Mat&, Mat&, Point2f&) override;
    bool saveCentersToFile(const char*) const;
    bool saveComputationTimeToFile(const char*) const;
    bool saveSamplingTimeToFile(const char*) const;

    Scalar line_color;
    double particleRadius;

#if USE_MULTITHREADING
    boost::mutex mProcessMutex;
	// multithreading
	void startThread(void);
	inline void stopThread(const bool value){ mIsThreadRunning = !value; };
	// most of these functions link a pointer of this class to some variables 
	// this is used to manage things fast
	inline void setParameters(TrackerParameters &value){ mParamsThread = &value; };
	inline void setOutputFrame(Mat &value){ mOutFrameThread = &value; };
    inline void setFrameToGrab(int &value){ mActualFrame = &value; };
    inline void setVideoBuffer(vector<Mat> &value){ mVideoBuffer = &value; };
	inline void setInitialFrame(int value){ mInitialFrame = value; };
	inline void setTrackedCenter(Point2f &value){ mOutCenterThread = &value; };
#endif
};
