#include "forcecurrentsettingswindow.h"
#include "ui_forcecurrentsettingswindow.h"

ForceCurrentSettingsWindow::ForceCurrentSettingsWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ForceCurrentSettingsWindow)
{
    ui->setupUi(this);

    fcPub= n.advertise<batmag_soft::ForceCurrentSet>("force_current_settings",1);

    msg.br.resize(1);
    msg.mur.resize(1);
    msg.radius.resize(1);

    msg.br[0]=ui->br_doubleSpinBox->value();
    msg.mur[0]=ui->permeability_doubleSpinBox->value();
    msg.radius[0]=ui->radius_doubleSpinBox->value()/1000;
    msg.density=ui->density_spinBox->value();




}

ForceCurrentSettingsWindow::~ForceCurrentSettingsWindow()
{
    delete ui;
}

void ForceCurrentSettingsWindow::on_diffAgent_checkBox_toggled(bool checked)
{
    //Used to set different parameters for different Agents
    differentAgents=checked;
    msg.br.resize(2);
    msg.mur.resize(2);
    msg.radius.resize(2);
}

void ForceCurrentSettingsWindow::on_permeability_doubleSpinBox_valueChanged(double arg1)
{
    ui->permeability_horizontalSlider->setValue(arg1<5?arg1*1000:5000);

    msg.mur[0]=arg1;

    if(ui->applyImmediately_checkBox->isChecked())
        fcPub.publish(msg);

    if(!differentAgents)//Setting only spinBox as the callback will change the slider
        ui->permeability_doubleSpinBox_2->setValue(arg1);

}

void ForceCurrentSettingsWindow::on_permeability_horizontalSlider_valueChanged(int value)
{
    ui->permeability_doubleSpinBox->setValue((double)value/1000);
}


void ForceCurrentSettingsWindow::on_radius_doubleSpinBox_valueChanged(double arg1)
{
    ui->radius_horizontalSlider->setValue(arg1<50?arg1*1000:50000);

    msg.radius[0]=arg1/1000;

    if(ui->applyImmediately_checkBox->isChecked())
        fcPub.publish(msg);

    if(!differentAgents)//Setting only spinBox as the callback will change the slider
        ui->radius_doubleSpinBox_2->setValue(arg1);
}

void ForceCurrentSettingsWindow::on_radius_horizontalSlider_valueChanged(int value)
{
    ui->radius_doubleSpinBox->setValue((double)value/1000);
}


void ForceCurrentSettingsWindow::on_br_doubleSpinBox_valueChanged(double arg1)
{
    ui->br_horizontalSlider->setValue(arg1<50?arg1*1000:5000);

    msg.br[0]=arg1*1e-6;

    if(ui->applyImmediately_checkBox->isChecked())
        fcPub.publish(msg);

    if(!differentAgents)//Setting only spinBox as the callback will change the slider
        ui->br_doubleSpinBox_2->setValue(arg1);
}

void ForceCurrentSettingsWindow::on_br_horizontalSlider_valueChanged(int value)
{
    ui->br_doubleSpinBox->setValue((double)value/1000);
}

//Agent2

void ForceCurrentSettingsWindow::on_permeability_doubleSpinBox_2_valueChanged(double arg1)
{
    ui->permeability_horizontalSlider_2->setValue(arg1<5?arg1*1000:5000);

    msg.mur[1]=arg1;

    if(ui->applyImmediately_checkBox->isChecked())
        fcPub.publish(msg);
}

void ForceCurrentSettingsWindow::on_permeability_horizontalSlider_2_valueChanged(int value)
{
    ui->permeability_doubleSpinBox_2->setValue((double)value/1000);
}


void ForceCurrentSettingsWindow::on_radius_doubleSpinBox_2_valueChanged(double arg1)
{
    ui->radius_horizontalSlider_2->setValue(arg1<50?arg1*1000:50000);

    msg.radius[1]=arg1/1000;

    if(ui->applyImmediately_checkBox->isChecked())
        fcPub.publish(msg);
}

void ForceCurrentSettingsWindow::on_radius_horizontalSlider_2_valueChanged(int value)
{
    ui->radius_doubleSpinBox_2->setValue((double)value/1000);
}


void ForceCurrentSettingsWindow::on_br_doubleSpinBox_2_valueChanged(double arg1)
{
    ui->br_horizontalSlider_2->setValue(arg1<50?arg1*1000:50000);

    msg.br[1]=arg1*1e-6;

    if(ui->applyImmediately_checkBox->isChecked())
        fcPub.publish(msg);
}

void ForceCurrentSettingsWindow::on_br_horizontalSlider_2_valueChanged(int value)
{
    ui->br_doubleSpinBox_2->setValue((double)value/1000);
}

void ForceCurrentSettingsWindow::on_ok_button_clicked()
{

    fcPub.publish(msg);

    this->close();
}

void ForceCurrentSettingsWindow::on_ApplyButton_clicked()
{
    fcPub.publish(msg);

}

void ForceCurrentSettingsWindow::on_density_spinBox_valueChanged(int arg1)
{
    msg.density=arg1;

    if(ui->applyImmediately_checkBox->isChecked())
        fcPub.publish(msg);
}
