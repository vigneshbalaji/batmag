#include "recorder.h"

Recorder::Recorder()
{

}

void Recorder::startCam0(int rows, int cols)
{
    QDateTime local_time = QDateTime::currentDateTime();
    QDir dir("/home/batmag/BatMag_Recordings/Videos");
    if(!initCam0)
    {
        if(dir.mkpath(local_time.toString("yy.MM.dd"))==0)
            ROS_ERROR_STREAM("Couldn't create the " <<local_time.toString("yy.MM.dd").toStdString() << " log folder");
        if(dir.exists())
        {
            logsdir = dir.absolutePath() + "/" + local_time.toString("yy.MM.dd") + "/";



            if(!camera0.isOpened())
            {
                if( camera0.open(logsdir.toStdString() +
                                 local_time.toString("yy.MM.dd-hh.mm.ss").toStdString()+ "-Camera0.avi",
                                 CV_FOURCC('M','J','P','G'), 25, Size(cols, rows), true)
                        )
                {
                    ROS_INFO_STREAM("Started camera 0 video recorder");
                    initCam0=true;
                }

                else

                    ROS_ERROR_STREAM("Couldn't open the camera log writers");



            }
        }
        else
            ROS_ERROR_STREAM("~/recording doesn't exist");
    }
    else
        ROS_WARN_STREAM("The recorders are already initialized");

}

void Recorder::startCam1(int rows, int cols)
{
    QDateTime local_time = QDateTime::currentDateTime();
    QDir dir("/home/batmag/BatMag_Recordings/Videos");
    if(!initCam1)
    {
        if(dir.mkpath(local_time.toString("yy.MM.dd"))==0)
            ROS_ERROR_STREAM("Couldn't create the " <<local_time.toString("yy.MM.dd").toStdString() << " log folder");
        if(dir.exists())
        {
            logsdir = dir.absolutePath() + "/" + local_time.toString("yy.MM.dd") + "/";



            if(!camera1.isOpened())
            {
                if( camera1.open(logsdir.toStdString() +
                                 local_time.toString("yy.MM.dd-hh.mm.ss").toStdString() + "-Camera1.avi",
                                 CV_FOURCC('M','J','P','G'), 25, Size(cols, rows), true)
                        )
                {
                    ROS_INFO_STREAM("Started the cameras video recorders");
                    initCam1=true;
                }

                else

                    ROS_ERROR_STREAM("Couldn't open the camera log writers");



            }
        }
        else
            ROS_ERROR_STREAM("~/recording doesn't exist");
    }
    else
        ROS_WARN_STREAM("The recorders are already initialized");

}


void Recorder::startUS(int rows, int cols)
{
    QDateTime local_time = QDateTime::currentDateTime();
    QDir dir("/home/batmag/BatMag_Recordings/Videos");
    if(!initUS)
    {
        if(dir.mkpath(local_time.toString("yy.MM.dd"))==0)
            ROS_ERROR_STREAM("Couldn't create the " <<local_time.toString("yy.MM.dd").toStdString() << " log folder");
        if(dir.exists())
        {
            logsdir = dir.absolutePath() + "/" + local_time.toString("yy.MM.dd") + "/";

            {
                if(!usRec.isOpened())
                {
                    if( usRec.open(logsdir.toStdString() +
                                   local_time.toString("yy.MM.dd-hh.mm.ss").toStdString()+ "-US.avi",
                                   CV_FOURCC('M','J','P','G'), 25, Size(cols, rows), true)
                            )
                    {
                        usCols=cols;
                        usRows=rows;
                        ROS_INFO_STREAM("Started us video recorder");
                        initUS=true;
                    }
                }
            }

            if(!initUS)
                ROS_ERROR_STREAM("Couldn't open the US log writers");

        }
        else
            ROS_ERROR_STREAM("~/recording doesn't exist");
    }
    else
        ROS_WARN_STREAM("The recorders are already initialized");

}


void Recorder::recordCamera0(Mat &Image)
{
    if(!initCam0)
        startCam0(Image.rows, Image.cols);
    camera0.write(Image);
}

void Recorder::recordCamera1(Mat &Image)
{
    if(!initCam1)
        startCam1(Image.rows, Image.cols);
    camera1.write(Image);
}

void Recorder::recordUS(Mat &Image)
{
    if(!initUS)
        startUS(Image.rows, Image.cols);
    else if (usRows!=Image.rows || usCols!=Image.cols)
    {
        usRec.release();
        initUS=false;
        startUS(Image.rows, Image.cols);
    }
    usRec.write(Image);
}

void Recorder::stop()
{
    if(initCam0)
        camera0.release();
    if(initCam1)
        camera1.release();
    if(initUS)
        usRec.release();
    initCam0=false;
    initCam1=false;
    initUS=false;
    ROS_INFO_STREAM("Released the VideoRecorder");

}

//bool Recorder::copyToHDD() {
//    clock_t start, end;
//    start = clock();


//    int source = open("from.ogv", O_RDONLY, 0);
//    int dest = open("/ClickMeIamData/BatMag_recordings"+, O_WRONLY | O_CREAT /*| O_TRUNC/**/, 0644);

//    // struct required, rationale: function stat() exists also
//    struct stat stat_source;
//    fstat(source, &stat_source);

//    sendfile(dest, source, 0, stat_source.st_size);

//    close(source);
//    close(dest);

//    end = clock();

//    cout << "CLOCKS_PER_SEC " << CLOCKS_PER_SEC << "\n";
//    cout << "CPU-TIME START " << start << "\n";
//    cout << "CPU-TIME END " << end << "\n";
//    cout << "CPU-TIME END - START " <<  end - start << "\n";
//    cout << "TIME(SEC) " << static_cast<double>(end - start) / CLOCKS_PER_SEC << "\n";

//    return 0;
