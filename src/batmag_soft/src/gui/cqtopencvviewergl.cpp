#include "cqtopencvviewergl.h"

#include <QOpenGLFunctions>
#include <opencv2/opencv.hpp>

CQtOpenCVViewerGl::CQtOpenCVViewerGl(QWidget *parent) :
    QOpenGLWidget(parent)
{
    mBgColor = QColor::fromRgb(150, 150, 150);
}

void CQtOpenCVViewerGl::initializeGL()
{
    makeCurrent();
    initializeOpenGLFunctions();

    float r = ((float)mBgColor.darker().red())/255.0f;
    float g = ((float)mBgColor.darker().green())/255.0f;
    float b = ((float)mBgColor.darker().blue())/255.0f;
    glClearColor(r,g,b,1.0f);
}

void CQtOpenCVViewerGl::resizeGL(int width, int height)
{
    makeCurrent();
    glViewport(0, 0, (GLint)width, (GLint)height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glOrtho(0, width, -height, 0, 0, 1);

    glMatrixMode(GL_MODELVIEW);

    recalculatePosition();

    emit imageSizeChanged(mRenderWidth, mRenderHeight);

    updateScene();
}

void CQtOpenCVViewerGl::updateScene()
{
    if (this->isVisible())
        update();
}

void CQtOpenCVViewerGl::paintGL()
{
    makeCurrent();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    renderImage();
}

void CQtOpenCVViewerGl::renderImage()
{
    makeCurrent();

    glClear(GL_COLOR_BUFFER_BIT);

    if (!mRenderQtImg.isNull())
    {
        glLoadIdentity();

        glPushMatrix();
        {
            if (mResizedImg.width() <= 0)
            {
                if (mRenderWidth == mRenderQtImg.width() && mRenderHeight == mRenderQtImg.height())
                    mResizedImg = mRenderQtImg;
                else
                    mResizedImg = mRenderQtImg.scaled(QSize(mRenderWidth, mRenderHeight),
                                                      Qt::IgnoreAspectRatio,
                                                      Qt::SmoothTransformation);
            }

            // ---> Centering image in draw area

            glRasterPos2i(mRenderPosX, mRenderPosY);

            glPixelZoom(1, -1);

            glDrawPixels(mResizedImg.width(), mResizedImg.height(), GL_RGBA, GL_UNSIGNED_BYTE, mResizedImg.bits());
        }
        glPopMatrix();

        // end
        glFlush();
    }
}

void CQtOpenCVViewerGl::recalculatePosition()
{
    mImgRatio = (float)mOrigImage.cols/(float)mOrigImage.rows;

    mRenderWidth = this->size().width();
    mRenderHeight = floor(mRenderWidth / mImgRatio);

    if (mRenderHeight > this->size().height())
    {
        mRenderHeight = this->size().height();
        mRenderWidth = floor(mRenderHeight * mImgRatio);
    }

    mRenderPosX = 0;// floor((this->size().width() - mRenderWidth) / 2);
    mRenderPosY = 0;// -floor((this->size().height() - mRenderHeight) / 2);

    mResizedImg = QImage();
}

bool CQtOpenCVViewerGl::showImage(const cv::Mat& image)
{
//    Centering Lines

//    cv::line(image, cv::Point(0,0),cv::Point(image.rows,image.cols), Scalar( 0, 0, 255 ));//Diagonal 1
//    cv::line(image, cv::Point(image.rows,0),cv::Point(0,image.cols), Scalar( 0, 0, 255 ));//Diagonal 2
//    cv::line(image, cv::Point(image.rows/2,0),cv::Point(image.rows/2,image.cols), Scalar( 0, 0, 255 ));//Vertical mid
//    cv::line(image, cv::Point(0,image.cols/2),cv::Point(image.rows,image.cols/2), Scalar( 0, 0, 255 ));//horizontal mid

    mutex.lock();
    if (image.channels() == 3)
    {
        cvtColor(image, mOrigImage, CV_BGR2RGBA);
//        ROS_INFO_STREAM("The image has 3 channels");
    }
    else if (image.channels() == 1)
    {
        cvtColor(image, mOrigImage, CV_GRAY2RGBA);
//        ROS_INFO_STREAM("The image has 1 channel");
    }
    else
    {
        ROS_ERROR_STREAM("The image has the wrong amount of channels; i.e. "<< image.channels()<< " channels.");
        return false;
    }

    mRenderQtImg = QImage((const unsigned char*)(mOrigImage.data),
                          mOrigImage.cols, mOrigImage.rows,
                          mOrigImage.step1(), QImage::Format_RGB32);

    recalculatePosition();

    updateScene();

    mutex.unlock();

    return true;
}

