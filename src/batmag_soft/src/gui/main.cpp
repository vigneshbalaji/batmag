#include "secondscreenimage.h"
#include "ctrlwindow.h"
#include "QApplication"
#include "qdesktopwidget.h"
#include "QWindow"
#include "batmag_soft/GetInitParameters.h"

#include "ros/ros.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    //Create the ros node and the handle
    ros::init(argc, argv, "bat_gui");
    ros::NodeHandle n;

    ROS_INFO_STREAM("The gui node is online");

    ros::AsyncSpinner myAsyncSpinner(0);


    while(!ros::ok())
        usleep(500);


    ROS_INFO_STREAM("Started the spinner");
    ROS_INFO_STREAM("What about this?");

    //Start a client to request the init parameters
    
    //Initiate the window
    CtrlWindow w(n);
    w.show();
    SecondScreenImage ssi(n);
    //Center the 2nd window in the second Screen
    ssi.move(QApplication::desktop()->screenGeometry(2).bottomLeft().x()+QApplication::desktop()->screenGeometry(2).width()/2-ssi.width()/2,
             (QApplication::desktop()->screenGeometry(2).height()-ssi.height())/2);
    ssi.show();
    ROS_INFO_STREAM("Showing the windows");

    return a.exec();
}
