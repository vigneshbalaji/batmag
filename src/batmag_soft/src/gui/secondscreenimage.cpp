#include "secondscreenimage.h"
#include "ui_secondscreenimage.h"

SecondScreenImage::SecondScreenImage(ros::NodeHandle nh, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::SecondScreenImage),
    it(nh)
{
//    this->setScreen(2);
    ui->setupUi(this);

    //Listen to camera messages
    imageCamSub[0]=it.subscribe("camera0", 1, &SecondScreenImage::imageCb0, this);
    imageCamSub[1]=it.subscribe("camera1", 1, &SecondScreenImage::imageCb1, this);


    cameraClickPub=n.advertise<batmag_soft::CameraClick>("camera_click", 1);

    startSub=n.subscribe<batmag_soft::StartStop>("gui_start_stop", 1, &SecondScreenImage::startCb, this);

    connect(this,SIGNAL(newCameraImage(int)),this, SLOT(on_newCameraImage(int)),Qt::QueuedConnection);

    ROS_INFO_STREAM("The second window started");


    connect(ui->camera0View2, SIGNAL(clicked(int, int, Qt::MouseButton)), this, SLOT(on_camera_0_clicked(int, int, Qt::MouseButton)));
    connect(ui->camera1View2, SIGNAL(clicked(int, int, Qt::MouseButton)), this, SLOT(on_camera_1_clicked(int, int, Qt::MouseButton)));
}

void SecondScreenImage::imageCb0(const sensor_msgs::ImageConstPtr& msg){
    imageMessageCallback(msg, 0);
}

void SecondScreenImage::imageCb1(const sensor_msgs::ImageConstPtr& msg){
    imageMessageCallback(msg, 1);
}


void SecondScreenImage::imageMessageCallback(const sensor_msgs::ImageConstPtr &msg, int cameraNum)
{
    ROS_DEBUG_STREAM("Receiving image from camera: "<< cameraNum);
    try
    {
        mutex.lock();
        cameraImage[cameraNum]=cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8)->image;
        mutex.unlock();
        emit newCameraImage(cameraNum);
    }
    catch (cv_bridge::Exception& e)
    {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }

}
//Actual update of the image
//The signal allows makes the image update as little blocking as possible
//This is done because we are actually hogging the ROS master's resources

//Here a signal with the whole image would also be possible, but I'm not sure if it would be efficient

void SecondScreenImage::on_newCameraImage(int camNum)
{
    mutex.lock();
    if(camNum==0)
    {
        ui->camera0View2->showImage(cameraImage[camNum]);
    }

    else
        if(camNum==1)
        {
            ui->camera1View2->showImage(cameraImage[camNum]);
        }
    mutex.unlock();
}

SecondScreenImage::~SecondScreenImage()
{
    delete ui;
}

void SecondScreenImage::startCb (const batmag_soft::StartStop::ConstPtr& msg)
{
    if(msg->pressedStart)
    {
        imageCamSub[0]=it.subscribe("trackerOut0", 1, &SecondScreenImage::imageCb0, this);
        imageCamSub[1]=it.subscribe("trackerOut1", 1, &SecondScreenImage::imageCb1, this);
    }

    if(msg->pressedStop)
        qApp->quit();

}

//Set setpoint and reference for clicks
void SecondScreenImage::on_camera_0_clicked(int x, int y, Qt::MouseButton button)
{
    ROS_DEBUG_STREAM("Position "<< x<<" - "<<y<<" was clicked with button " << button<< "on camera 0 calling on_clicked funtion");
    clickMutex.lock();
    on_camera_clicked(x,y,button, 0);
    clickMutex.unlock();

}

void SecondScreenImage::on_camera_1_clicked(int x, int y, Qt::MouseButton button)
{
    ROS_DEBUG_STREAM("Position "<< x<<" - "<<y<<" was clicked with button " << button<< " on camera 1 calling on_clicked funtion");
    clickMutex.lock();
    on_camera_clicked(x,y,button, 1);
    clickMutex.unlock();

}

void SecondScreenImage::on_camera_clicked(int x, int y, Qt::MouseButton button, int camNum)
{
    int bNum;
    if (button == Qt::LeftButton)    bNum=1;
    if (button == Qt::RightButton)   bNum=2;
    if (button == Qt::MiddleButton)  bNum=3;

    /* Please note that Qt Button #4 corresponds to button #8 on all
 * platforms which EMULATE wheel events by creating button events
 * (Button #4 = Scroll Up; Button #5 = Scroll Down; Button #6 = Scroll
 * Left; and Button #7 = Scroll Right.) This includes X11, with both
 * Xlib and XCB.  So, the "raw button" for "Qt::BackButton" is
 * usually described as "Button #8".

 * If your platform supports "smooth scrolling", then, for the cases of
 * Qt::BackButton and higher, this program will show "raw button" with a
 * value which is too large. Subtract 4 to get the correct button ID for
 * your platform.
 */

    if (button == Qt::BackButton)    bNum=8;
    if (button == Qt::ForwardButton) bNum=9;


    ROS_INFO_STREAM("On_camera_clicked here, position "<< x<<" - "<<y<<" was clicked with button " << bNum<< "on camera"<< camNum);
    batmag_soft::CameraClick msg;

    msg.x=x*800/950;//*ratio added in imageProcessing
    msg.y=y*800/950;//*ratio added in imageProcessing
    msg.button=bNum;
    msg.camera=camNum;
    cameraClickPub.publish(msg);
    ROS_DEBUG_STREAM("Click Message sent");


}

