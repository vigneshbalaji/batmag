#pragma once

//#include "ctrlwindow.h"

//OpenCv
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "recorder.h"

//For Image conversion
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>


//Ros and its messages
#include "ros/ros.h"
#include <ros/callback_queue.h>
#include"batmag_soft/StartStop.h"
#include "batmag_soft/CameraClick.h"

//Imaging
#include "cqtopencvviewergl.h"

//Qt stuff
#include <QWidget>
#include <QMutex>
#include <QTimer>
#include <QMainWindow>

namespace Ui {
class SecondScreenImage;
}

class SecondScreenImage : public QMainWindow
{
    Q_OBJECT

public:
    explicit SecondScreenImage(ros::NodeHandle nh, QWidget *parent = 0);
    virtual ~SecondScreenImage();

    //These images have to be public otherwise the windows can't access them
    cv::Mat cameraImage[2];


signals:
    void newCameraImage(int cameraNum);

private slots:

    void on_newCameraImage(int camNum);

    void on_camera_0_clicked(int x, int y, Qt::MouseButton button);

    void on_camera_1_clicked(int x, int y, Qt::MouseButton button);

private:
    Ui::SecondScreenImage *ui;

    //Image transport parameters
    image_transport::ImageTransport it;
    image_transport::Subscriber imageCamSub[2];
    ros::NodeHandle n;

    ros::Subscriber startSub;

    ros::Publisher cameraClickPub;

    //Callbacks for image reception
    void imageMessageCallback(const sensor_msgs::ImageConstPtr& msg, int cameraNum);
    void imageCb0(const sensor_msgs::ImageConstPtr& msg);
    void imageCb1(const sensor_msgs::ImageConstPtr& msg);

    //Callback on Start pressed
    void startCb (const batmag_soft::StartStop::ConstPtr& msg);

    void on_camera_clicked(int x, int y, Qt::MouseButton button, int camNum);


    QMutex mutex,clickMutex;

};
