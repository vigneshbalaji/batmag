#include "frequencyctrlwindow.h"
#include "ui_frequencyctrlwindow.h"

FrequencyCtrlWindow::FrequencyCtrlWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FrequencyCtrlWindow)
{
    ui->setupUi(this);

    QString labelText[3];

    ui->rot_label->setText("Clockwise");

    getInitParClient = n.serviceClient<batmag_soft::GetInitParameters>("get_ctrl_parameters");
    setInitParClient = n.serviceClient<batmag_soft::SetInitParameters>("set_ctrl_parameters");

    if (getInitParClient.call(getSrv))
    {
        ui->double_SpinBox_1->setValue(getSrv.response.value[0]);
        ui->double_SpinBox_2->setValue(getSrv.response.value[1]);
        ui->double_SpinBox_3->setValue(getSrv.response.value[2]);

        for(int i=0; i<3; ++i)
            labelText[i]=QString::fromStdString(getSrv.response.names[i]);

        ui->label->setText(QString::fromStdString(getSrv.response.names[0]));
        ui->label_2->setText(QString::fromStdString(getSrv.response.names[1]));
        ui->label_3->setText(QString::fromStdString(getSrv.response.names[2]));

    }
    else
    {
        ROS_ERROR_STREAM("Error while getting the current values of the Rotating Field");
    }

    ctrlSub=n.subscribe<batmag_soft::CtrlOutput>("ctrl_output", 1, &FrequencyCtrlWindow::onCtrlMsg, this);
    refSub=n.subscribe<batmag_soft::State>("state", 1, &FrequencyCtrlWindow::onStateMsg, this);
    stateSub=n.subscribe<batmag_soft::Reference>("reference", 1, &FrequencyCtrlWindow::onRefMsg, this);

    getInitParClient.shutdown();//No need for this anymore

    nowT = boost::posix_time::microsec_clock::local_time();
    prevT = nowT;

    img[0]=cv::Mat(ui->graphicsWid_0->width(), ui->graphicsWid_0->height(), CV_8UC3);
    img[1]=cv::Mat(ui->graphicsWid_1->width(), ui->graphicsWid_1->height(), CV_8UC3);

    mTimer.setTimerType(Qt::CoarseTimer);
    mTimer.setInterval(50);
    connect(&mTimer, SIGNAL(timeout()), this, SLOT(updateImages()), Qt::QueuedConnection);
    mTimer.start();

}

FrequencyCtrlWindow::~FrequencyCtrlWindow()
{
    ctrlSub.shutdown();
    refSub.shutdown();
    stateSub.shutdown();
    mTimer.stop();
    delete ui;
}

void FrequencyCtrlWindow::onCtrlMsg(const batmag_soft::CtrlOutput::ConstPtr& msg)
{
    ROS_DEBUG_STREAM("About to save the message");
    mRefMutex.lock();
    if(msg->ctrlout.size()>=3)
        for(int i=0;i<3;++i)
            ctrl[i]=msg->ctrlout[i];
    mRefMutex.unlock();
}

void FrequencyCtrlWindow::onStateMsg(const batmag_soft::State::ConstPtr& msg)
{
    ROS_INFO_STREAM("Reading State");
    mStateMutex.lock();
    if(msg->state.size()>=3)
        for(int i=0;i<3;++i)
            pos[i]=msg->state[i];
    mStateMutex.unlock();
}

void FrequencyCtrlWindow::onRefMsg(const batmag_soft::Reference::ConstPtr& msg)
{
    ROS_INFO_STREAM("Reading Ref");
    mRefMutex.lock();
    if(msg->ref.size()>=3)
        for(int i=0;i<3;++i)
            ref[i]=msg->ref[i];
    mRefMutex.unlock();
}

void FrequencyCtrlWindow::on_set_pushButton_pressed()
{
    ROS_INFO_STREAM("Setting References");

    setSrv.request.value[0]=ui->double_SpinBox_1->value();
    setSrv.request.value[1]=ui->double_SpinBox_2->value();
    setSrv.request.value[2]=ui->double_SpinBox_3->value();
    setSrv.request.value[3]=clockwise;

    if(!setInitParClient.call(setSrv))
    {
        ROS_ERROR_STREAM("An error occured during the change of Rotating Field Parameters");
    }

}

void FrequencyCtrlWindow::updateImages()
{

    ROS_INFO_STREAM("Getting Mutexes");

    mCbMutex.lock();
    mRefMutex.lock();
    mStateMutex.lock();
    ROS_INFO_STREAM("locking the mutexes");
    diff=ref-pos;
    sclDiff=diff/diff.norm()*ui->graphicsWid_0->height()/2; //Scale
    sclCtrl=ctrl/ctrl.norm()*ui->graphicsWid_0->height()/2;
    mRefMutex.unlock();
    mStateMutex.unlock();


    ROS_INFO_STREAM("Img processing");

    //Add ui->graphicsWid_0->height()/2  to center
    //img0
    imgMutex[0].lock();
    img[0] = cv::Scalar(0,0,0);
    cv::line(img[0],cv::Point(ui->graphicsWid_0->width()/2,ui->graphicsWid_0->height()/2), cv::Point(sclDiff[0]+ui->graphicsWid_0->width()/2 , ui->graphicsWid_0->height()/2-sclDiff[2] ), CV_RGB(255,0,0));
    cv::line(img[0],cv::Point(ui->graphicsWid_0->width()/2,ui->graphicsWid_0->height()/2), cv::Point(sclCtrl[0]+ui->graphicsWid_0->width()/2, ui->graphicsWid_0->height()/2-sclCtrl[2] ), CV_RGB(0,0,255));
    ROS_INFO_STREAM("Showing the image");
    ui->graphicsWid_0->showImage(img[0]);
    imgMutex[0].unlock();


    //img1
    imgMutex[1].lock();
    img[1] = cv::Scalar(0,0,0);
    cv::line(img[1],cv::Point(ui->graphicsWid_1->width()/2,ui->graphicsWid_1->height()/2), cv::Point(sclDiff[1]+ui->graphicsWid_1->width()/2 , ui->graphicsWid_1->height()/2-sclDiff[2] ), CV_RGB(255,0,0));
    cv::line(img[1],cv::Point(ui->graphicsWid_1->width()/2,ui->graphicsWid_1->height()/2), cv::Point(sclCtrl[1]+ui->graphicsWid_1->width()/2, ui->graphicsWid_1->height()/2-sclCtrl[2] ), CV_RGB(0,0,255));
    ui->graphicsWid_1->showImage(img[1]);
    imgMutex[1].unlock();

    ROS_INFO_STREAM("Image shown");
    mCbMutex.unlock();
}

void FrequencyCtrlWindow::on_buttonBox_accepted()
{
    FrequencyCtrlWindow::on_set_pushButton_pressed();
    delete ui;
    this->close();

}

void FrequencyCtrlWindow::on_buttonBox_rejected()
{
    delete ui;
    this->close();
}

void FrequencyCtrlWindow::on_invertRotation_pushButton_pressed()
{

    ROS_INFO_STREAM("Clockwise Change");
    clockwise=!clockwise;
    if(clockwise)
        ui->rot_label->setText("Clockwise");
    else
        ui->rot_label->setText("Counter-Clockwise");

}
