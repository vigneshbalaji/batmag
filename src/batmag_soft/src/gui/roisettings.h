#pragma once

//OpenCv
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

//For Image conversion
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>


//Ros and its messages
#include "ros/ros.h"
#include <ros/callback_queue.h>
#include"batmag_soft/StartStop.h"
#include "batmag_soft/getInitTrackerSettings.h"
#include "batmag_soft/TrackerSettings.h"

//Imaging
#include "cqtopencvviewergl.h"

//Qt stuff
#include <QDialog>
#include <QWidget>
#include <QMutex>
#include <QTimer>
#include <QMainWindow>

namespace Ui {
class ROIsettings;
}

class ROIsettings : public QDialog
{
    Q_OBJECT

public:
    explicit ROIsettings(QWidget *parent = 0);
    ~ROIsettings();

    cv::Mat roiImage[2];
    cv::Mat bsImage[2];

    cv_bridge::CvImageConstPtr cv_ptr;
signals:
    void newRoiImage(int cameraNum);

    void newBsImage(int cameraNum);

private slots:

    void on_newRoiImage(int camNum);

    void on_newBsImage(int camNum);

    void on_pushButton_clicked();

    void on_sizeROI_Slider_valueChanged(int value);

    void on_agentSize_Slider_valueChanged(int value);

    void on_maxHue_Slider_valueChanged(int value);

    void on_minHue_Slider_valueChanged(int value);

    void on_maxVal_Slider_valueChanged(int value);

    void on_minVal_Slider_valueChanged(int value);

    void on_maxSat_Slider_valueChanged(int value);

    void on_minSat_Slider_valueChanged(int value);

    void on_blur_Slider_valueChanged(int value);
    //Round 2

    void on_sizeROI_Slider_2_valueChanged(int value);

    void on_agentSize_Slider_2_valueChanged(int value);

    void on_maxHue_Slider_2_valueChanged(int value);

    void on_minHue_Slider_2_valueChanged(int value);

    void on_maxVal_Slider_2_valueChanged(int value);

    void on_minVal_Slider_2_valueChanged(int value);

    void on_maxSat_Slider_2_valueChanged(int value);

    void on_minSat_Slider_2_valueChanged(int value);

    void on_blur_Slider_2_valueChanged(int value);

    void on_fixed_checkBox_toggled(bool checked);

    void on_fixed_checkBox_2_toggled(bool checked);

    void on_bs0_button_pressed();

    void on_bs1_button_pressed();

    void on_bs0check_toggled(bool checked);

    void on_bs1check_toggled(bool checked);

    void on_pushButton_pressed();

private:
    Ui::ROIsettings *ui;

    ros::NodeHandle n;

    ros::Publisher setPub;


    ros::Publisher dennisTest;

    batmag_soft::TrackerSettings mMsg[2];

    //Image transport parameters
    image_transport::ImageTransport it;
    image_transport::Subscriber roiSub[2];
    image_transport::Subscriber bsSub[2];

    //Callbacks for image reception
    void imageMessageCallback(const sensor_msgs::ImageConstPtr& msg, int cameraNum);
    void imageCb0(const sensor_msgs::ImageConstPtr& msg);
    void imageCb1(const sensor_msgs::ImageConstPtr& msg);

    void bsMessageCallback(const sensor_msgs::ImageConstPtr& msg, int bsNum);
    void bsCb0(const sensor_msgs::ImageConstPtr& msg);
    void bsCb1(const sensor_msgs::ImageConstPtr& msg);



    QMutex mutex;

};

