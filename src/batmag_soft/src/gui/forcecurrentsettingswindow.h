#pragma once

#include <QDialog>
#include "ros/ros.h"
#include "batmag_soft/ForceCurrentSet.h"

namespace Ui {
class ForceCurrentSettingsWindow;
}

class ForceCurrentSettingsWindow : public QDialog
{
    Q_OBJECT

public:
    explicit ForceCurrentSettingsWindow(QWidget *parent = 0);
    ~ForceCurrentSettingsWindow();

private slots:
    void on_permeability_doubleSpinBox_valueChanged(double arg1);

    void on_permeability_horizontalSlider_valueChanged(int value);

    void on_radius_horizontalSlider_valueChanged(int value);

    void on_radius_doubleSpinBox_valueChanged(double arg1);

    void on_br_doubleSpinBox_valueChanged(double arg1);

    void on_br_horizontalSlider_valueChanged(int value);

    //Agent2

    void on_permeability_doubleSpinBox_2_valueChanged(double arg1);

    void on_permeability_horizontalSlider_2_valueChanged(int value);

    void on_radius_horizontalSlider_2_valueChanged(int value);

    void on_radius_doubleSpinBox_2_valueChanged(double arg1);

    void on_br_doubleSpinBox_2_valueChanged(double arg1);

    void on_br_horizontalSlider_2_valueChanged(int value);



    void on_diffAgent_checkBox_toggled(bool checked);

    void on_ok_button_clicked();

    void on_ApplyButton_clicked();

    void on_density_spinBox_valueChanged(int arg1);

private:
    Ui::ForceCurrentSettingsWindow *ui;

    ros::NodeHandle n;

    batmag_soft::ForceCurrentSet msg;

    ros::Publisher fcPub;

    bool differentAgents=false;
};
