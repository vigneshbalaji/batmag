#pragma once

//Qt stuff
#include <QMainWindow>
#include <QListWidget>
#include <vector>
#include <string>
#include <QString>
#include <QMutex>
#include <QTimer>
#include <QTime>
#include <QApplication>
#include <QDoubleSpinBox>
#include <QScrollBar>
#include <QKeyEvent>

//Math
#include <math.h>

//OpenCv
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/videoio.hpp"
#include "recorder.h"

//For Image conversion
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

//Ros and its messages
#include "batmag_soft/GetInitParameters.h"
#include "batmag_soft/SetInitParameters.h"
#include "batmag_soft/SaveImage.h"
#include "batmag_soft/StartStop.h"
#include "batmag_soft/CameraClick.h"
#include "batmag_soft/MoveCamera.h"
#include "batmag_soft/WriteText.h"
#include "batmag_soft/GUIpreferences.h"
#include "batmag_soft/MapOutput.h"
#include "batmag_soft/RecordRaw.h"
#include "batmag_soft/WriteText.h"
#include "batmag_soft/USroi.h"
#include "batmag_soft/refFilPar.h"
#include "batmag_soft/ResetElmos.h"
#include "ros/ros.h"
#include "std_msgs/Int8.h"
#include <ros/callback_queue.h>

//Imaging
#include "secondscreenimage.h"
#include "roisettings.h"
#include "cqtopencvviewergl.h"
#include "camerasettingswin.h"

//Extra Windows
#include "forcecurrentsettingswindow.h"
#include "frequencyctrlwindow.h"

#include "batmag_soft/Reference.h"
#include "batmag_soft/State.h"
#include "src/control/controller.h"



//Myshit


#include "src/control/controller.h"

const int MOVE_MESSAGE_DELAY=500;
const int HZ_25_IN_MS=1000;

namespace Ui {
class CtrlWindow;
}

class CtrlWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit CtrlWindow(ros::NodeHandle nh, QWidget *parent = 0);
    virtual ~CtrlWindow();

    std::string getCtrlType();

    //These images have to be public otherwise the windows can't access them
    cv::Mat cameraImage[2];


//    SecondScreenImage mSecondWindow;

signals:
    void newCameraImage(int cameraNum);

    void newCurrentCurrents();

private slots:

    void on_camera_0_clicked(int x, int y, Qt::MouseButton button);

    void on_camera_1_clicked(int x, int y, Qt::MouseButton button);

    void on_newCameraImage(int camNum);

    void on_stopButton_clicked();

    void on_stopButton_2_clicked();

    void moveCamMsg();


    void on_NewCurrentCurrents();

    inline void allowCurrentBarUpdate(){curBarEn=true;}






    //-----------SLIDERS AND SPINNERS SYNCRONIZATION FROM HERE...----------/

    virtual void setSliders(const std::vector<double> &value, const std::vector<double> &max, const std::vector<double> &min);
    virtual void setSliders(const std::vector<double> &value, const std::vector<double> &max, const std::vector<double> &min, const std::vector<std::string> names);

    void on_paramSlider1_valueChanged(int value);

    void on_paramSpin1_valueChanged(double arg1);

    void on_paramSlider2_valueChanged(int value);

    void on_paramSpin2_valueChanged(double arg1);

    void on_paramSlider3_valueChanged(int value);

    void on_paramSpin3_valueChanged(double arg1);

    void on_paramSlider4_valueChanged(int value);

    void on_paramSpin4_valueChanged(double arg1);

    void on_paramSlider5_valueChanged(int value);

    void on_paramSpin5_valueChanged(double arg1);

    void on_paramSlider6_valueChanged(int value);

    void on_paramSpin6_valueChanged(double arg1);

    void on_paramSlider7_valueChanged(int value);

    void on_paramSpin7_valueChanged(double arg1);

    void on_paramSlider8_valueChanged(int value);

    void on_paramSpin8_valueChanged(double arg1);

    void on_paramSlider9_valueChanged(int value);

    void on_paramSpin9_valueChanged(double arg1);

    //--------------------...TO HERE-------------------------------//

    void on_startButton_clicked();

    void on_controlAlgorithmList_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);

    void setParametersFromResp(batmag_soft::GetInitParameters srv);

    void sendSetParameters();

    void on_agentList_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);

    void publishStop();

    void publishStart();

    void on_camera0Speed_valueChanged(int value);

    void on_camera1Speed_valueChanged(int value);

    void on_camera0Speed_sliderReleased();

    void on_camera1Speed_sliderReleased();

    void on_autofocus0_checkBox_toggled(bool checked);

    void on_autofocus1_checkBox_toggled(bool checked);
    
    void on_record_checkBox_toggled(bool checked);

    void on_forceCurrentList_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);

    void on_ctrl_en_checkBox_toggled(bool checked);

    void on_zoomDoubleSpinBox_valueChanged(double arg1);

    void on_recordRaw_checkBox_toggled(bool checked);

    void on_secondWindowButton_clicked();

    void on_Cur1Debug_doubleSpinBox_valueChanged(double arg1);

    void on_Cur2Debug_doubleSpinBox_valueChanged(double arg1);
    
    void on_Cur3Debug_doubleSpinBox_valueChanged(double arg1);
    
    void on_Cur4Debug_doubleSpinBox_valueChanged(double arg1);
    
    void on_Cur5Debug_doubleSpinBox_valueChanged(double arg1);
    
    void on_Cur6Debug_doubleSpinBox_valueChanged(double arg1);
    
    void on_Cur7Debug_doubleSpinBox_valueChanged(double arg1);
    
    void on_Cur8Debug_doubleSpinBox_valueChanged(double arg1);
    
    void on_Cur9Debug_doubleSpinBox_valueChanged(double arg1);

    void on_deb_en_checkBox_toggled(bool checked);

    void sinusoidalCurDebug(void);

    void on_imagingModeList_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);

    inline void on_usROI_x_spinBox_valueChanged(int arg1){sendUSRoiSettings();}

    inline void on_usROI_y_spinBox_valueChanged(int arg1){sendUSRoiSettings();}

    inline void on_usROI_height_spinBox_valueChanged(int arg1){sendUSRoiSettings();}

    inline void on_usROI_width_spinBox_valueChanged(int arg1){sendUSRoiSettings();}

    void on_Pm_doubleSpinBox_valueChanged(double arg1);

    void on_PMenable_toggled(bool checked);

    void on_pushButton_clicked();

    void on_gravityEn_checkBox_toggled(bool checked);

    void on_refTau_SpinBox_valueChanged(double arg1);

    void on_refUb_SpinBox_valueChanged(double arg1);

    void on_refLb_SpinBox_valueChanged(double arg1);

    void on_saveImage_button_clicked();

    void on_grid_Button_clicked();

    void on_cameraSet_pushButton_pressed();

    void on_forceCur_button_clicked();

    void on_resetElmo_pushButton_pressed();

    void on_ctrlSettings_button_pressed();

    void on_omega_checkBox_toggled(bool checked);

    void on_Contro_Button_clicked();

protected:

    void keyPressEvent(QKeyEvent *event) override;
    vector<double> state;

private:
    Ui::CtrlWindow *ui;
    ROIsettings* mROIsettings;
    std::shared_ptr<CameraSettingsWin> mCamSetW;
    std::shared_ptr<ForceCurrentSettingsWindow> mFCwind;
    std::shared_ptr<FrequencyCtrlWindow> mFreqWind;

    QString selectedCtrl, selectedAgent, selectedMap;
    ros::ServiceClient getInitParClient;
    ros::ServiceClient setInitParClient;
    ros::ServiceClient saveImageClient;
    ros::Publisher startStopPub,cameraClick, moveCameraPub, guiPreferencesPub, recordRawPub, mapoutPub, usRoiPub, refFilPub, resetElmosPub, keyPub;


    void on_camera_clicked(int x, int y, Qt::MouseButton button, int camNum);

    //US ROI

    void sendUSRoiSettings();


    //Current updates
    ros::Subscriber mapOutSub;
    std::vector<int> currentCurrents;
    int iCurCur=0;
    void onMapMsg(const batmag_soft::MapOutput::ConstPtr& msg);



    //Image transport parameters
    image_transport::ImageTransport it;
    image_transport::Subscriber imageCamSub[2];

    //Callbacks for image reception
    void imageMessageCallback(const sensor_msgs::ImageConstPtr& msg, int cameraNum);
    void imageCb0(const sensor_msgs::ImageConstPtr& msg);
    void imageCb1(const sensor_msgs::ImageConstPtr& msg);
    Recorder mRecorder;



    void sendGUIPreferences();

    //The 25Hz handle has lower priority and can be used for tasks that do not require high responsivness (e.g. updates to the GUI)
    ros::NodeHandle n, nh25Hz;
    //With respective cb queue
    ros::CallbackQueue cb_queue_25Hz;

    //Writing text Subscriber
    ros::Subscriber writeTextSub;
    virtual void writeText(QString txt);
    virtual void writeText(const batmag_soft::WriteText::ConstPtr& msg);

    void requestParameters();

    QTimer cameraMoveTimer, vCoarseSlidersTimer,curDebTimer, imgTimer;
    QTime curDebTime;
    double curDebTimeSeconds;
    double const kDebTimerInt=5;

    bool spinInit=false;

    QMutex mutex, writeMutex, clickMutex, curMsgMutex, curMutex[10], guiPrefMutex, refSetMutex;

    std::shared_ptr<ros::AsyncSpinner> myAsyncSpinner;

    cv_bridge::CvImagePtr msgToCvPtr;

    //Private messages
    batmag_soft::MoveCamera cameraMoveMsg;
    batmag_soft::StartStop startStopMsg;
    batmag_soft::RecordRaw recordRawMsg;
    batmag_soft::USroi usRoiMsg;
    batmag_soft::refFilPar mRefFilMsg;
    std_msgs::Int8 mKey;

    //For current debug
    batmag_soft::MapOutput debugCurMsg;

    //Broadcast the GUI settings
    batmag_soft::GUIpreferences mGUIpref;

    int frame=0;

    bool zoomSet=false;

    bool curBarEn=false;

    QDoubleSpinBox * mACSpinArray[10], * mFreqArr[10], * mDCCurSpinArr[10];
    void arrayInit();

    //Second window
    //SecondScreenImage *mSecondWindow;



    ros::Subscriber stateSub;


/*boost::shared_ptr<Controller> myController;*/
};

