#pragma once

#include "opencv2/core/core.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"

#include "ros/ros.h"

#include <QDateTime>
#include <QDir>

#include <iostream>
#include <sys/sendfile.h>  // sendfile
#include <fcntl.h>         // open
#include <unistd.h>        // close
#include <sys/stat.h>      // fstat
#include <sys/types.h>     // fstat
#include <ctime>
using namespace std;

using namespace cv;
class Recorder
{
public:
    Recorder();
    void startCam0(int rows, int cols);
    void startCam1(int rows, int cols);
    void startUS(int rows, int cols);
    void stop();

    void recordCamera0(cv::Mat& Image);
    void recordCamera1(cv::Mat& Image);
    void recordUS(cv::Mat& Image);

//    virtual recordUS() = 0;//TODO

//    bool copyToHDD();

private:
    VideoWriter Camera0Raw,camera0,camera1,Camera1Raw, usRec, USRaw;

    int usRows, usCols;

    QString logsdir;
    bool initCam0=false, initCam1=false, initUS=false;
};
