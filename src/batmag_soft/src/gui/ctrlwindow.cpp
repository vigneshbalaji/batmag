﻿#include "ctrlwindow.h"
#include "ui_ctrlwindow.h"

CtrlWindow::CtrlWindow(ros::NodeHandle nh, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::CtrlWindow),
    n(nh),
    it(nh)
  //    mSecondWindow(nh)

{
    ROS_INFO_STREAM("Initalizing the main window");

    myAsyncSpinner=std::make_shared<ros::AsyncSpinner>(0);
    myAsyncSpinner->start();

    //Declare an handle for rosnode
    //declare services
    getInitParClient = n.serviceClient<batmag_soft::GetInitParameters>("get_init_parameters");
    setInitParClient = n.serviceClient<batmag_soft::SetInitParameters>("set_init_parameters");
    saveImageClient = n.serviceClient<batmag_soft::SaveImage>("save_image");

    //Timer to avoid overfloading the ros message system on the sliders position
    cameraMoveTimer.setInterval(MOVE_MESSAGE_DELAY);
    connect(&cameraMoveTimer, SIGNAL(timeout()), this, SLOT(moveCamMsg()), Qt::QueuedConnection);


    //Only allow the sliders to be update once every sec (they are very consuming)
    vCoarseSlidersTimer.setTimerType(Qt::VeryCoarseTimer);
    vCoarseSlidersTimer.setInterval(1000);
    connect(&vCoarseSlidersTimer, SIGNAL(timeout()), this, SLOT(allowCurrentBarUpdate()), Qt::QueuedConnection);

    cameraMoveMsg.cameraSpeed[0]=1;cameraMoveMsg.cameraSpeed[1]=0;

    //----------------------------//
    //Init Messages to default//
    //---------------------------//

    //StartStop
    startStopMsg.isUS[0]=0;
    startStopMsg.isUS[1]=0;
    startStopMsg.usOnly=0;

    mRefFilMsg.ub=-1;
    mRefFilMsg.lb=-1;


    //Publishers
    startStopPub= n.advertise<batmag_soft::StartStop>("gui_start_stop", 1);
    cameraClick=n.advertise<batmag_soft::CameraClick>("camera_click", 1);
    moveCameraPub = n.advertise<batmag_soft::MoveCamera>("move_camera",1);
    recordRawPub=n.advertise<batmag_soft::RecordRaw>("record_raw",1);
    mapoutPub=n.advertise<batmag_soft::MapOutput>("map_output",1);
    usRoiPub=n.advertise<batmag_soft::USroi>("us_roi",1);
    refFilPub=n.advertise<batmag_soft::refFilPar>("ref_filter_param", 1);
    resetElmosPub=n.advertise<batmag_soft::ResetElmos>("reset_elmos",1);
    keyPub=n.advertise<std_msgs::Int8>("key_press",10);

    //Subscribers
    currentCurrents.resize(10);
    mapOutSub=n.subscribe<batmag_soft::MapOutput>("map_output",1, &CtrlWindow::onMapMsg, this);
    writeTextSub=n.subscribe<batmag_soft::WriteText>("write_text", 100, &CtrlWindow::writeText, this);

    /****/
  /*  stateSub = n.subscribe<batmag_soft::State>("state", 1, &Controller::setStateFromMsg, myController);
    /****/




    //Connection of sliders
    connect(this, SIGNAL(newCurrentCurrents()),SLOT(on_NewCurrentCurrents()),Qt::QueuedConnection);


    guiPreferencesPub = n.advertise<batmag_soft::GUIpreferences>("gui_preferences",1);

    //Listen to camera messages
    imageCamSub[0]=it.subscribe("camera0", 1, &CtrlWindow::imageCb0, this);
    imageCamSub[1]=it.subscribe("camera1", 1, &CtrlWindow::imageCb1, this);

    ui->setupUi(this);

    connect(this,SIGNAL(newCameraImage(int)),this, SLOT(on_newCameraImage(int)),Qt::QueuedConnection);

    //Connect clicks on screen with respective messages
    connect(ui->camera0View, SIGNAL(clicked(int, int, Qt::MouseButton)), this, SLOT(on_camera_0_clicked(int, int, Qt::MouseButton)));
    connect(ui->camera1View, SIGNAL(clicked(int, int, Qt::MouseButton)), this, SLOT(on_camera_1_clicked(int, int, Qt::MouseButton)));

    connect(&curDebTimer, SIGNAL (timeout()), this, SLOT(sinusoidalCurDebug()));

    //sets the starting tab to the init one
    ui->tabWidget->setCurrentWidget(ui->initTab);
    ui->textBox->setTextColor("Blue");
    ui->textBox->setText("Welcome to BatMag control software. Please, select your preferences.");
    ui->textBox->append("Errors will be displayed in red.");

    //Start Timers
    vCoarseSlidersTimer.start();


    //Debug currents
    debugCurMsg.size=10;
    for(int i=0; i<10; ++i)
    {
        debugCurMsg.mapout[i]=0;
    };

    //PM mover


    arrayInit();
}


CtrlWindow::~CtrlWindow()
{
    delete ui;

    myAsyncSpinner->stop();
}



void CtrlWindow::arrayInit()
{
    mACSpinArray[0]=ui->AC1_SpinBox;
    mACSpinArray[1]=ui->AC2_SpinBox;
    mACSpinArray[2]=ui->AC3_SpinBox;
    mACSpinArray[3]=ui->AC4_SpinBox;
    mACSpinArray[4]=ui->AC5_SpinBox;
    mACSpinArray[5]=ui->AC6_SpinBox;
    mACSpinArray[6]=ui->AC7_SpinBox;
    mACSpinArray[7]=ui->AC8_SpinBox;
    mACSpinArray[8]=ui->AC9_SpinBox;
    mACSpinArray[9]=ui->AC10_SpinBox;

    mFreqArr[0]=ui->frequencySpinBox1;
    mFreqArr[1]=ui->frequencySpinBox2;
    mFreqArr[2]=ui->frequencySpinBox3;
    mFreqArr[3]=ui->frequencySpinBox4;
    mFreqArr[4]=ui->frequencySpinBox5;
    mFreqArr[5]=ui->frequencySpinBox6;
    mFreqArr[6]=ui->frequencySpinBox7;
    mFreqArr[7]=ui->frequencySpinBox8;
    mFreqArr[8]=ui->frequencySpinBox9;
    mFreqArr[9]=ui->frequencySpinBox10;

    mDCCurSpinArr[0]=ui->Cur1Debug_doubleSpinBox;
    mDCCurSpinArr[1]=ui->Cur2Debug_doubleSpinBox;
    mDCCurSpinArr[2]=ui->Cur3Debug_doubleSpinBox;
    mDCCurSpinArr[3]=ui->Cur4Debug_doubleSpinBox;
    mDCCurSpinArr[4]=ui->Cur5Debug_doubleSpinBox;
    mDCCurSpinArr[5]=ui->Cur6Debug_doubleSpinBox;
    mDCCurSpinArr[6]=ui->Cur7Debug_doubleSpinBox;
    mDCCurSpinArr[7]=ui->Cur8Debug_doubleSpinBox;
    mDCCurSpinArr[8]=ui->Cur9Debug_doubleSpinBox;
    mDCCurSpinArr[9]=ui->PmDebug_doubleSpinBox;

}
//--------------------------------------//
//-------Update Camera Image------------//
//--------------------------------------//

//Callback for new images from camera

void CtrlWindow::imageCb0(const sensor_msgs::ImageConstPtr& msg){
    imageMessageCallback(msg, 0);
}

void CtrlWindow::imageCb1(const sensor_msgs::ImageConstPtr& msg){
    imageMessageCallback(msg, 1);
}

void CtrlWindow::imageMessageCallback(const sensor_msgs::ImageConstPtr &msg, int cameraNum)
{
    ROS_DEBUG_STREAM("Receiving image from camera: "<< cameraNum);
    try
    {
        mutex.lock();
        cameraImage[cameraNum]=cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8)->image;
        mutex.unlock();
        emit newCameraImage(cameraNum);
    }
    catch (cv_bridge::Exception& e)
    {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }

}
//Actual update of the image
//The signal allows makes the image update as little blocking as possible
//This is done because we are actually hogging the ROS master's resources

//Here a signal with the whole image would also be possible, but I'm not sure if it would be efficient

void CtrlWindow::on_newCameraImage(int camNum)
{
    frame++;
    ROS_DEBUG_STREAM("Frame: "<<frame<< " from camera "<<camNum<<std::endl<< "Image width: "<< cameraImage[camNum].rows<< " height is: "<< cameraImage[camNum].cols<< " and the number of channels is: " << cameraImage[camNum].channels());
    mutex.lock();
    if(camNum==0)
    {
        ui->camera0View->showImage(cameraImage[camNum]);
        if(ui->record_checkBox->isChecked())
        {
            if(startStopMsg.isUS[0])
                mRecorder.recordUS(cameraImage[camNum]);
            else
                mRecorder.recordCamera0(cameraImage[camNum]);
        }
    }

    else
        if(camNum==1)
        {
            ui->camera1View->showImage(cameraImage[camNum]);
            if(ui->record_checkBox->isChecked() && !startStopMsg.usOnly)
            {
                if(startStopMsg.isUS[1])
                    mRecorder.recordUS(cameraImage[camNum]);
                else
                    mRecorder.recordCamera1(cameraImage[camNum]);
            }
        }
    mutex.unlock();
}

//Set setpoint and reference for clicks
void CtrlWindow::on_camera_0_clicked(int x, int y, Qt::MouseButton button)
{
    ROS_DEBUG_STREAM("Position "<< x<<" - "<<y<<" was clicked with button " << button<< "on camera 0 calling on_clicked funtion");
    clickMutex.lock();
    on_camera_clicked(x,y,button, 0);
    clickMutex.unlock();

}

void CtrlWindow::on_camera_1_clicked(int x, int y, Qt::MouseButton button)
{
    ROS_DEBUG_STREAM("Position "<< x<<" - "<<y<<" was clicked with button " << button<< " on camera 1 calling on_clicked funtion");
    clickMutex.lock();
    on_camera_clicked(x,y,button, 1);
    clickMutex.unlock();

}

void CtrlWindow::on_camera_clicked(int x, int y, Qt::MouseButton button, int camNum)
{
    int bNum;
    if (button == Qt::LeftButton)    bNum=1;
    if (button == Qt::RightButton)   bNum=2;
    if (button == Qt::MiddleButton)  bNum=3;

    /* Please note that Qt Button #4 corresponds to button #8 on all
 * platforms which EMULATE wheel events by creating button events
 * (Button #4 = Scroll Up; Button #5 = Scroll Down; Button #6 = Scroll
 * Left; and Button #7 = Scroll Right.) This includes X11, with both
 * Xlib and XCB.  So, the "raw button" for "Qt::BackButton" is
 * usually described as "Button #8".

 * If your platform supports "smooth scrolling", then, for the cases of
 * Qt::BackButton and higher, this program will show "raw button" with a
 * value which is too large. Subtract 4 to get the correct button ID for
 * your platform.
 */

    if (button == Qt::BackButton)    bNum=8;
    if (button == Qt::ForwardButton) bNum=9;


    ROS_INFO_STREAM("On_camera_clicked here, position "<< x<<" - "<<y<<" was clicked with button " << bNum<< "on camera"<< camNum);
    batmag_soft::CameraClick msg;

    msg.x=x;//*ratio added in imageProcessing
    msg.y=y;//*ratio added in imageProcessing
    msg.button=bNum;
    msg.camera=camNum;
    cameraClick.publish(msg);
    ROS_DEBUG_STREAM("Click Message sent");


}

void CtrlWindow::writeText(const batmag_soft::WriteText::ConstPtr &msg)
{
    writeText(QString::fromStdString(msg->text));
}


void CtrlWindow::writeText(QString srt)
{
    writeMutex.lock();
    ROS_DEBUG_STREAM("Starting to write text");
    if(ui->tabWidget->currentIndex()==0)//If in the init tab
    {
        ui->textBox->append(srt);
    }
    else if(ui->tabWidget->currentIndex()==1)//If in the control tab
    {

        ROS_DEBUG_STREAM("Going to append Z" << srt.toStdString());
        ui->ctrl_textBrowser->append(srt);

        ROS_DEBUG_STREAM("Appended stuff");
        ui->ctrl_textBrowser->verticalScrollBar()->setValue(ui->ctrl_textBrowser->verticalScrollBar()->maximum());//Scroll to the bottom

    }
    writeMutex.unlock();
    ROS_DEBUG_STREAM("Text Written");
}

//-----------------------------------------//
//Kill the app when pressing stop buttons--//
//-----------------------------------------//
void CtrlWindow::on_stopButton_clicked()
{
    publishStop();
    qApp->quit();
}


void CtrlWindow::on_stopButton_2_clicked()
{
    publishStop();
    qApp->quit();
}

void CtrlWindow::publishStop()
{
    startStopMsg.pressedStop=true;
    mutex.lock();
    startStopPub.publish(startStopMsg);
    mutex.unlock();
}

void CtrlWindow::publishStart()
{
    startStopMsg.pressedStart=true;
    startStopMsg.pressedStop=false;
    startStopMsg.agent=selectedAgent.toStdString();
    startStopMsg.control=selectedCtrl.toStdString();
    startStopMsg.map=selectedMap.toStdString();

    //    Set zoom considering the final lens
    if(ui->finalLens_comboBox->currentText().toStdString()=="1x")
        startStopMsg.zoom=ui->zoomDoubleSpinBox->value();
    else
        if (ui->finalLens_comboBox->currentText().toStdString()=="4x")
            startStopMsg.zoom=ui->zoomDoubleSpinBox->value()*4;

    ROS_INFO_STREAM("Zoom set to: "<<startStopMsg.zoom);

    mutex.lock();
    startStopPub.publish(startStopMsg);
    mutex.unlock();
    getInitParClient.shutdown();
    setInitParClient.shutdown();
}

//------------------------------------------------------------//
//----------SLIDERS AND SPINNERS FOR PARAMETERS---------------//
//--The following code synchronizes the sliders and spinners--//
//------------------------------------------------------------//

//For Parameter1
void CtrlWindow::on_paramSlider1_valueChanged(int value)
{
    ui->paramSpin1->setValue(value);
}
void CtrlWindow::on_paramSpin1_valueChanged(double arg1)
{
    ui->paramSlider1->setValue(arg1);
    if(spinInit) sendSetParameters();
}

//For Parameter2
void CtrlWindow::on_paramSlider2_valueChanged(int value)
{
    ui->paramSpin2->setValue(value);
}
void CtrlWindow::on_paramSpin2_valueChanged(double arg1)
{
    ui->paramSlider2->setValue(arg1);
    if(spinInit) sendSetParameters();
}

//For Parameter3
void CtrlWindow::on_paramSlider3_valueChanged(int value)
{
    ui->paramSpin3->setValue(value);
}
void CtrlWindow::on_paramSpin3_valueChanged(double arg1)
{
    ui->paramSlider3->setValue(arg1);
    if(spinInit) sendSetParameters();
}

//For Parameter4
void CtrlWindow::on_paramSlider4_valueChanged(int value)
{
    ui->paramSpin4->setValue(value);
}
void CtrlWindow::on_paramSpin4_valueChanged(double arg1)
{
    ui->paramSlider4->setValue(arg1);
    if(spinInit) sendSetParameters();
}

//For Parameter5
void CtrlWindow::on_paramSlider5_valueChanged(int value)
{
    ui->paramSpin5->setValue(value);
}
void CtrlWindow::on_paramSpin5_valueChanged(double arg1)
{
    ui->paramSlider5->setValue(arg1);
    if(spinInit) sendSetParameters();
}

//For Parameter6
void CtrlWindow::on_paramSlider6_valueChanged(int value)
{
    ui->paramSpin6->setValue(value);
}
void CtrlWindow::on_paramSpin6_valueChanged(double arg1)
{
    ui->paramSlider6->setValue(arg1);
    if(spinInit) sendSetParameters();
}

//For Parameter7
void CtrlWindow::on_paramSlider7_valueChanged(int value)
{
    ui->paramSpin7->setValue(value);
}
void CtrlWindow::on_paramSpin7_valueChanged(double arg1)
{
    ui->paramSlider7->setValue(arg1);
    if(spinInit) sendSetParameters();
}

//For Parameter8
void CtrlWindow::on_paramSlider8_valueChanged(int value)
{
    ui->paramSpin8->setValue(value);
}
void CtrlWindow::on_paramSpin8_valueChanged(double arg1)
{
    ui->paramSlider8->setValue(arg1);
    if(spinInit) sendSetParameters();
}

//For Parameter9
void CtrlWindow::on_paramSlider9_valueChanged(int value)
{
    ui->paramSpin9->setValue(value);
}
void CtrlWindow::on_paramSpin9_valueChanged(double arg1)
{
    ui->paramSlider9->setValue(arg1);
    if(spinInit) sendSetParameters();
}

//-------------------------------------------------------------------------------//
//--------------SLIDER AND SPINNERS SYNCHRONIZATION ENDS HERE------------------////
//-----From here on the value, max and min of spinners and sliders are set the same-----//
//-------------------------------------------------------------------------------//

void CtrlWindow::setSliders(const std::vector<double> & value,const std::vector<double>& max,const std::vector<double>& min){

    int stefanoIsTheBest=1;
    if (value.size()>=stefanoIsTheBest)
    {
        //stefanoIsTheBest initializes values, minimums and maxs of the bars
        //This is for 1st spinner and slider, the other ones work in the same way

        ui->paramSlider1->setMaximum(max[stefanoIsTheBest-1]);
        ui->paramSpin1->setMaximum(max[stefanoIsTheBest-1]);

        ui->paramSlider1->setMinimum(min[stefanoIsTheBest-1]);
        ui->paramSpin1->setMinimum(min[stefanoIsTheBest-1]);

        ui->paramSlider1->setValue(value[stefanoIsTheBest-1]);
        ui->paramSpin1->setValue(value[stefanoIsTheBest-1]);

        ui->paramSlider1->setEnabled(true);
        ui->paramSpin1->setEnabled(true);

    }

    //Federico is cool
    //But only Stefano is truly AWESOME

    stefanoIsTheBest=2;
    if (value.size()>=stefanoIsTheBest)
    {


        ui->paramSlider2->setMaximum(max[stefanoIsTheBest-1]);
        ui->paramSpin2->setMaximum(max[stefanoIsTheBest-1]);

        ui->paramSlider2->setMinimum(min[stefanoIsTheBest-1]);
        ui->paramSpin2->setMinimum(min[stefanoIsTheBest-1]);

        ui->paramSlider2->setValue(value[stefanoIsTheBest-1]);
        ui->paramSpin2->setValue(value[stefanoIsTheBest-1]);

        ui->paramSlider2->setEnabled(true);
        ui->paramSpin2->setEnabled(true);

    }

    stefanoIsTheBest=3;
    if (value.size()>=stefanoIsTheBest)
    {


        ui->paramSlider3->setMaximum(max[stefanoIsTheBest-1]);
        ui->paramSpin3->setMaximum(max[stefanoIsTheBest-1]);

        ui->paramSlider3->setMinimum(min[stefanoIsTheBest-1]);
        ui->paramSpin3->setMinimum(min[stefanoIsTheBest-1]);

        ui->paramSlider3->setEnabled(true);
        ui->paramSpin3->setEnabled(true);

        ui->paramSlider3->setValue(value[stefanoIsTheBest-1]);
        ui->paramSpin2->setValue(value[stefanoIsTheBest-1]);

    }

    stefanoIsTheBest=4;
    if (value.size()>=stefanoIsTheBest)
    {

        ui->paramSlider4->setMaximum(max[stefanoIsTheBest-1]);
        ui->paramSpin4->setMaximum(max[stefanoIsTheBest-1]);

        ui->paramSlider4->setMinimum(min[stefanoIsTheBest-1]);
        ui->paramSpin4->setMinimum(min[stefanoIsTheBest-1]);

        ui->paramSlider4->setValue(value[stefanoIsTheBest-1]);
        ui->paramSpin4->setValue(value[stefanoIsTheBest-1]);

        ui->paramSlider4->setEnabled(true);
        ui->paramSpin4->setEnabled(true);
    }




    stefanoIsTheBest=5;
    if (value.size()>=stefanoIsTheBest)
    {


        ui->paramSlider5->setMaximum(max[stefanoIsTheBest-1]);
        ui->paramSpin5->setMaximum(max[stefanoIsTheBest-1]);

        ui->paramSlider5->setMinimum(min[stefanoIsTheBest-1]);
        ui->paramSpin5->setMinimum(min[stefanoIsTheBest-1]);

        ui->paramSlider5->setValue(value[stefanoIsTheBest-1]);
        ui->paramSpin5->setValue(value[stefanoIsTheBest-1]);

        ui->paramSlider5->setEnabled(true);
        ui->paramSpin5->setEnabled(true);
    }

    stefanoIsTheBest=6;
    if (value.size()>=stefanoIsTheBest)
    {


        ui->paramSlider6->setMaximum(max[stefanoIsTheBest-1]);
        ui->paramSpin6->setMaximum(max[stefanoIsTheBest-1]);

        ui->paramSlider6->setMinimum(min[stefanoIsTheBest-1]);
        ui->paramSpin6->setMinimum(min[stefanoIsTheBest-1]);

        ui->paramSlider6->setValue(value[stefanoIsTheBest-1]);
        ui->paramSpin6->setValue(value[stefanoIsTheBest-1]);

        ui->paramSlider6->setEnabled(true);
        ui->paramSpin6->setEnabled(true);

    }

    stefanoIsTheBest=7;
    if (value.size()>=stefanoIsTheBest)
    {


        ui->paramSlider7->setMaximum(max[stefanoIsTheBest-1]);
        ui->paramSpin7->setMaximum(max[stefanoIsTheBest-1]);

        ui->paramSlider7->setMinimum(min[stefanoIsTheBest-1]);
        ui->paramSpin7->setMinimum(min[stefanoIsTheBest-1]);

        ui->paramSlider7->setValue(value[stefanoIsTheBest-1]);
        ui->paramSpin7->setValue(value[stefanoIsTheBest-1]);

        ui->paramSlider7->setEnabled(true);
        ui->paramSpin7->setEnabled(true);

    }

    stefanoIsTheBest=8;
    if (value.size()>=stefanoIsTheBest)
    {

        ui->paramSlider8->setMaximum(max[stefanoIsTheBest-1]);
        ui->paramSpin8->setMaximum(max[stefanoIsTheBest-1]);

        ui->paramSlider8->setMinimum(min[stefanoIsTheBest-1]);
        ui->paramSpin8->setMinimum(min[stefanoIsTheBest-1]);

        ui->paramSlider8->setValue(value[stefanoIsTheBest-1]);
        ui->paramSpin8->setValue(value[stefanoIsTheBest-1]);

        ui->paramSlider8->setEnabled(true);
        ui->paramSpin8->setEnabled(true);

    }

    stefanoIsTheBest=9;
    if (value.size()>=stefanoIsTheBest)
    {


        ui->paramSlider9->setMaximum(max[stefanoIsTheBest-1]);
        ui->paramSpin9->setMaximum(max[stefanoIsTheBest-1]);

        ui->paramSlider9->setMinimum(min[stefanoIsTheBest-1]);
        ui->paramSpin9->setMinimum(min[stefanoIsTheBest-1]);

        ui->paramSlider9->setValue(value[stefanoIsTheBest-1]);
        ui->paramSpin9->setValue(value[stefanoIsTheBest-1]);

        ui->paramSlider9->setEnabled(true);
        ui->paramSpin9->setEnabled(true);

    }
    spinInit=true;
}

//Version with string names

void CtrlWindow::setSliders(const std::vector<double> &value, const std::vector<double> &max, const std::vector<double> &min, const std::vector<std::string> names){
    //Calls the version w/out names

    setSliders(value,max,min);

    //and then assigns the variable names to the lables

    int paramnum=1;

    if(names.size()>=paramnum)
        ui->parameterLabel1->setText(QString::fromStdString(names[paramnum-1]));

    paramnum=2;
    if(names.size()>=paramnum)
        ui->parameterLabel2->setText(QString::fromStdString(names[paramnum-1]));

    paramnum=3;
    if(names.size()>=paramnum)
        ui->parameterLabel3->setText(QString::fromStdString(names[paramnum-1]));

    paramnum=4;
    if(names.size()>=paramnum)
        ui->parameterLabel4->setText(QString::fromStdString(names[paramnum-1]));

    paramnum=5;
    if(names.size()>=paramnum)
        ui->parameterLabel5->setText(QString::fromStdString(names[paramnum-1]));

    paramnum=6;
    if(names.size()>=paramnum)
        ui->parameterLabel6->setText(QString::fromStdString(names[paramnum-1]));

    paramnum=7;
    if(names.size()>=paramnum)
        ui->parameterLabel7->setText(QString::fromStdString(names[paramnum-1]));

    paramnum=8;
    if(names.size()>=paramnum)
        ui->parameterLabel8->setText(QString::fromStdString(names[paramnum-1]));

    paramnum=9;
    if(names.size()>=paramnum)
        ui->parameterLabel9->setText(QString::fromStdString(names[paramnum-1]));
}

//---------------------------------------------------------------//
//----------------------END OF THE SLIDERS INIT------------------//
//------No Stefano was harmed during the writing of this code----//
//---------------------------------------------------------------//




//-----------------------------------------------------------------------------//
//Here there should be a service communicating init info and disabling buttons when pressing start---///
//TODO


void CtrlWindow::on_startButton_clicked()
{
    if(selectedMap!="Vector Map" || zoomSet)
    {
        mutex.lock();
        ui->textBox->append("Warning: you haven't set a zooming factor");
        mutex.unlock();
    }

    publishStart();

    mutex.lock();
    //changes the tab to the ctrl one
    ui->tabWidget->setCurrentWidget(ui->ctrlTab);
    ui->controlAlgorithmList->setEnabled(false);
    ui->agentList->setEnabled(false);
    ui->paramSlider1->setEnabled(false);
    ui->paramSpin1->setEnabled(false);
    ui->paramSlider2->setEnabled(false);
    ui->paramSpin2->setEnabled(false);
    ui->paramSlider3->setEnabled(false);
    ui->paramSpin3->setEnabled(false);
    ui->paramSlider4->setEnabled(false);
    ui->paramSpin4->setEnabled(false);
    ui->paramSlider5->setEnabled(false);
    ui->paramSpin5->setEnabled(false);
    ui->paramSlider6->setEnabled(false);
    ui->paramSpin6->setEnabled(false);
    ui->paramSlider7->setEnabled(false);
    ui->paramSpin7->setEnabled(false);
    ui->paramSlider8->setEnabled(false);
    ui->paramSpin8->setEnabled(false);
    ui->paramSlider9->setEnabled(false);
    ui->paramSpin9->setEnabled(false);

    mutex.unlock();

    //Listen to camera messages
    imageCamSub[0]=it.subscribe("trackerOut0", 1, &CtrlWindow::imageCb0, this);
    imageCamSub[1]=it.subscribe("trackerOut1", 1, &CtrlWindow::imageCb1, this);



}

//------------------------------------------------//
//------Request param when item is selected-------//
//------------------------------------------------//


//When the ctrl window is changed the ctrl name is assigned to selectedCtrl
void CtrlWindow::on_controlAlgorithmList_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
    selectedCtrl=current->text();
    requestParameters();
}

void CtrlWindow::on_agentList_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
    selectedAgent=current->text();
    requestParameters();
}

void CtrlWindow::sendSetParameters(){
    batmag_soft::SetInitParameters srv;
    if (ui->paramSpin1->isEnabled()) srv.request.value[0]=ui->paramSpin1->value();
    if (ui->paramSpin2->isEnabled())srv.request.value[1]=ui->paramSpin2->value();
    if (ui->paramSpin3->isEnabled())srv.request.value[2]=ui->paramSpin3->value();
    if (ui->paramSpin4->isEnabled())srv.request.value[3]=ui->paramSpin4->value();
    if (ui->paramSpin5->isEnabled())srv.request.value[4]=ui->paramSpin5->value();
    if (ui->paramSpin6->isEnabled())srv.request.value[5]=ui->paramSpin6->value();
    if (ui->paramSpin7->isEnabled())srv.request.value[6]=ui->paramSpin7->value();
    if (ui->paramSpin8->isEnabled())srv.request.value[7]=ui->paramSpin8->value();
    if (ui->paramSpin9->isEnabled())srv.request.value[8]=ui->paramSpin9->value();
    
    if(setInitParClient.call(srv))
    {
        if(srv.response.done){
            QString txtBoxMsg("The selected parameters have been set");
            mutex.lock();
            ui->textBox->append(txtBoxMsg);
            mutex.unlock();
        }
        else
        {
            mutex.lock();
            ui->textBox->setTextColor("red");
            ui->textBox->append("There was an error setting the parameters. But that's not my project.");
            ui->textBox->setTextColor("blue");
            mutex.unlock();
        }
    }
    else
    {
        mutex.lock();
        ui->textBox->setTextColor("red");
        ui->textBox->append("There was an error setting the parameters. But that's not my project.");
        ui->textBox->setTextColor("blue");
        mutex.unlock();
    }

}

void CtrlWindow::requestParameters()
{
    if(selectedCtrl.size()>0 && selectedAgent.size()>0)
    {
        batmag_soft::GetInitParameters srv;
        srv.request.ctrl=selectedCtrl.toStdString();
        srv.request.agent=selectedAgent.toStdString();
        if (getInitParClient.call(srv))
        {
            //ADD A BOOLEAN RESPONSE TO SAY IF THE APPLICATION WAS SUCCESSFUL
            setParametersFromResp(srv);
            if(srv.response.success)
            {
                mutex.lock();
                QString txtBoxMsg("Added standard parameters for "); txtBoxMsg.append(selectedAgent);txtBoxMsg.append(" with "); txtBoxMsg.append(selectedCtrl);txtBoxMsg.append(" control.");
                ui->textBox->append(txtBoxMsg);
                mutex.unlock();
            }
            else
            {
                mutex.lock();
                ui->textBox->setTextColor("red");
                ui->textBox->append("There was an error getting the parameters. But that's not my project.");
                ui->textBox->setTextColor("blue");
                mutex.unlock();
            }

        }
        else
        {
            mutex.lock();
            ROS_ERROR("Client failed to call service set Ctrl Parameters");
            ui->textBox->setTextColor("red");
            ui->textBox->append("There was an error getting the parameters. But that's not my project.");
            ui->textBox->setTextColor("blue");
            mutex.unlock();
        }

    }
}


void CtrlWindow::setParametersFromResp(batmag_soft::GetInitParameters srv){

    //Set parameters to the response
    std::vector<double>
            value(srv.response.value.begin(),srv.response.value.end()),
            max(srv.response.max.begin(),srv.response.max.end()),
            min(srv.response.min.begin(),srv.response.min.end()) ;
    std::vector<std::string> names(srv.response.names.begin(),srv.response.names.end());

    //Call one version of setSliders if with names, the other one otherwise
    if(names.size()>0)
        setSliders(value, max, min, names);
    else
        setSliders(value, max, min);

}

void CtrlWindow::on_camera0Speed_valueChanged(int value)
{
    if(!cameraMoveTimer.isActive())
        cameraMoveTimer.start(MOVE_MESSAGE_DELAY);
}

void CtrlWindow::on_camera1Speed_valueChanged(int value)
{
    if(!cameraMoveTimer.isActive())
        cameraMoveTimer.start(MOVE_MESSAGE_DELAY);
}

void CtrlWindow::on_camera0Speed_sliderReleased()
{
    mutex.lock();
    ui->camera0Speed->setValue(0);
    cameraMoveTimer.stop();
    mutex.unlock();
    moveCamMsg();

}

void CtrlWindow::on_camera1Speed_sliderReleased()
{

    mutex.lock();
    ui->camera1Speed->setValue(0);
    //if (ui->camera1Speed->value()==0) //can be added here but how can He be clicking both of them simoultaneously?
    cameraMoveTimer.stop();
    mutex.unlock();
    moveCamMsg();
}

void CtrlWindow::on_autofocus0_checkBox_toggled(bool checked)
{
    ui->camera0Speed->setDisabled(checked);
}

void CtrlWindow::on_autofocus1_checkBox_toggled(bool checked)
{
    ui->camera1Speed->setDisabled(checked);
}

void CtrlWindow::moveCamMsg()
{
    QString toWrite;
    mutex.lock();
    cameraMoveMsg.cameraSpeed[0]=ui->camera0Speed->value();
    cameraMoveMsg.cameraSpeed[1]=ui->camera1Speed->value();

    moveCameraPub.publish(cameraMoveMsg);
    mutex.unlock();

    ROS_INFO_STREAM("Sending move msg \n Camera 0 speed: "<< cameraMoveMsg.cameraSpeed[0] <<" Camera 1 speed: "<< cameraMoveMsg.cameraSpeed[1]);
}

void CtrlWindow::on_record_checkBox_toggled(bool checked)
{
    guiPrefMutex.lock();
    mGUIpref.recordOn=checked;
    guiPrefMutex.unlock();
    sendGUIPreferences();

}

void CtrlWindow::sendGUIPreferences()
{
    guiPrefMutex.lock();
    guiPreferencesPub.publish(mGUIpref);
    guiPrefMutex.unlock();
}

void CtrlWindow::on_forceCurrentList_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
    mutex.lock();
    selectedMap=current->text();
    mutex.unlock();
}

void CtrlWindow::on_ctrl_en_checkBox_toggled(bool checked)
{
    guiPrefMutex.lock();
    mGUIpref.ctrlEn=checked;
    guiPreferencesPub.publish(mGUIpref);
    guiPrefMutex.unlock();

}

void CtrlWindow::onMapMsg(const batmag_soft::MapOutput::ConstPtr& msg)
{
    if(curBarEn)
    {
        curBarEn=false;
        for(int i=0; i<10;++i)
        {
            currentCurrents[i]=abs(msg->mapout[i]*1000);
            ROS_DEBUG_STREAM("Current"<<i+1<<" is "<< currentCurrents[i]);
        }

        currentCurrents[9]=abs(msg->mapout[9]);
        emit newCurrentCurrents();
    }
}


void CtrlWindow::on_NewCurrentCurrents()
{
    for(int i=0; i<9;++i)
        if(currentCurrents[i] >10000)
            currentCurrents[i]=10000;
    ui->coil_progressBar_1->setValue(currentCurrents[0]);
    ui->coil_progressBar_2->setValue(currentCurrents[1]);
    ui->coil_progressBar_3->setValue(currentCurrents[2]);
    ui->coil_progressBar_4->setValue(currentCurrents[3]);
    ui->coil_progressBar_5->setValue(currentCurrents[4]);
    ui->coil_progressBar_6->setValue(currentCurrents[5]);
    ui->coil_progressBar_7->setValue(currentCurrents[6]);
    ui->coil_progressBar_8->setValue(currentCurrents[7]);
    ui->coil_progressBar_9->setValue(currentCurrents[8]);
    ui->PermMag_progressBar->setValue(currentCurrents[9]);
    ui->PermMag_progressBar_2->setValue(currentCurrents[9]);

}

void CtrlWindow::on_zoomDoubleSpinBox_valueChanged(double arg1)
{
    zoomSet=true;
}

void CtrlWindow::on_recordRaw_checkBox_toggled(bool checked)
{
    mutex.lock();
    recordRawMsg.recordRaw=checked;

    recordRawMsg.recordWhere = QDateTime::currentDateTime().toString("dd.MM.yy").toStdString();

    recordRawPub.publish(recordRawMsg);
    mutex.unlock();

}

void CtrlWindow::on_secondWindowButton_clicked()
{
    //     mSecondWindow.show();
}


//Debug Mode
void CtrlWindow::on_Cur1Debug_doubleSpinBox_valueChanged(double arg1)
{
    int i=0;
    curMutex[i].lock();
    debugCurMsg.mapout[i]=arg1;
    curMutex[i].unlock();

    if(ui->deb_en_checkBox->isChecked())
    {
        mapoutPub.publish(debugCurMsg);
        ROS_INFO_STREAM("Updated Current 1 to "<<debugCurMsg.mapout[i]);
    }


}

void CtrlWindow::on_Cur2Debug_doubleSpinBox_valueChanged(double arg1)
{
    int i=1;
    curMutex[i].lock();
    debugCurMsg.mapout[i]=arg1;
    curMutex[i].unlock();
    if(ui->deb_en_checkBox->isChecked())
    {
        mapoutPub.publish(debugCurMsg);
        ROS_INFO_STREAM("Updated Current 2 to "<<debugCurMsg.mapout[i]);
    }

}

void CtrlWindow::on_Cur3Debug_doubleSpinBox_valueChanged(double arg1)
{
    int i=2;
    curMutex[i].lock();
    debugCurMsg.mapout[i]=arg1;
    curMutex[i].unlock();
    if(ui->deb_en_checkBox->isChecked())
    {
        mapoutPub.publish(debugCurMsg);
        ROS_INFO_STREAM("Updated Current 3 to "<<debugCurMsg.mapout[i]);
    }
}

void CtrlWindow::on_Cur4Debug_doubleSpinBox_valueChanged(double arg1)
{
    int i=3;
    curMutex[i].lock();
    debugCurMsg.mapout[i]=arg1;
    curMutex[i].unlock();
    if(ui->deb_en_checkBox->isChecked())
    {
        mapoutPub.publish(debugCurMsg);
        ROS_INFO_STREAM("Updated Current 4 to "<<debugCurMsg.mapout[i]);
    }
}

void CtrlWindow::on_Cur5Debug_doubleSpinBox_valueChanged(double arg1)
{
    int i=4;
    curMutex[i].lock();
    debugCurMsg.mapout[i]=arg1;
    curMutex[i].unlock();;
    if(ui->deb_en_checkBox->isChecked())
    {
        mapoutPub.publish(debugCurMsg);
        ROS_INFO_STREAM("Updated Current 5 to "<<debugCurMsg.mapout[i]);
    }
}

void CtrlWindow::on_Cur6Debug_doubleSpinBox_valueChanged(double arg1)
{
    int i=5;
    curMutex[i].lock();
    debugCurMsg.mapout[i]=arg1;
    curMutex[i].unlock();
    if(ui->deb_en_checkBox->isChecked())
    {
        mapoutPub.publish(debugCurMsg);
        ROS_INFO_STREAM("Updated Current 6 to "<<debugCurMsg.mapout[i]);
    }
}

void CtrlWindow::on_Cur7Debug_doubleSpinBox_valueChanged(double arg1)
{
    int i=6;
    curMutex[i].lock();
    debugCurMsg.mapout[i]=arg1;
    curMutex[i].unlock();
    if(ui->deb_en_checkBox->isChecked())
    {
        mapoutPub.publish(debugCurMsg);
        ROS_INFO_STREAM("Updated Current 7 to "<<debugCurMsg.mapout[i]);
    }
}

void CtrlWindow::on_Cur8Debug_doubleSpinBox_valueChanged(double arg1)
{
    int i=7;
    curMutex[i].lock();
    debugCurMsg.mapout[i]=arg1;
    curMutex[i].unlock();
    if(ui->deb_en_checkBox->isChecked())
    {
        mapoutPub.publish(debugCurMsg);
        ROS_INFO_STREAM("Updated Current 8 to "<<debugCurMsg.mapout[i]);
    }
}

void CtrlWindow::on_Cur9Debug_doubleSpinBox_valueChanged(double arg1)
{
    int i=8;
    curMutex[i].lock();
    debugCurMsg.mapout[i]=arg1;
    curMutex[i].unlock();
    if(ui->deb_en_checkBox->isChecked())
    {
        mapoutPub.publish(debugCurMsg);
        ROS_INFO_STREAM("Updated Current 9 to "<<debugCurMsg.mapout[i]);
    }
}



void CtrlWindow::on_deb_en_checkBox_toggled(bool checked)
{
    if(checked)
    {
        curDebTime.restart();
        curDebTimer.start(kDebTimerInt);

    }

    else
    {

        curDebTimer.stop();

        batmag_soft::MapOutput offMsg;
        offMsg.size=10;
        for(int i=0; i<9; ++i)
        {
            offMsg.mapout[i]=0;
        }
        offMsg.mapout[9]=1;

        mapoutPub.publish(offMsg);
    }

    guiPrefMutex.lock();
    mGUIpref.debugEn=checked;
    guiPreferencesPub.publish(mGUIpref);
    guiPrefMutex.unlock();


}

void CtrlWindow::sinusoidalCurDebug(void)
{
    curDebTimeSeconds=(float)curDebTime.elapsed()/1000;

    for(int i=0; i<10; ++i)
    { //TODO ADD SINUSOIDAL function for sinusoidal current debug
        curMutex[i].lock();
        debugCurMsg.mapout[i]=mDCCurSpinArr[i]->value() +(mACSpinArray[i]->value()*sin(curDebTimeSeconds*mFreqArr[i]->value()));
        curMutex[i].unlock();
    }

    ROS_DEBUG_STREAM("ref[9] is " <<debugCurMsg.mapout[9]);

    curMsgMutex.lock();
    mapoutPub.publish(debugCurMsg);
    curMsgMutex.unlock();

}

void CtrlWindow::on_imagingModeList_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
    QString imagingModeTxt= current->text();

    if (imagingModeTxt=="2 Cameras")
    {
        startStopMsg.isUS[0]=0;
        startStopMsg.isUS[1]=0;
    }

    if (imagingModeTxt=="Camera 0 & Ultrasound")
    {
        startStopMsg.isUS[0]=0;
        startStopMsg.isUS[1]=1;
    }
    else if(imagingModeTxt=="Camera 1 & Ultrasound")
    {
        startStopMsg.isUS[0]=1;
        startStopMsg.isUS[1]=0;
    }

    else if (imagingModeTxt=="Ultrasound Only (2D)")
    {

        startStopMsg.isUS[0]=1;
        startStopMsg.isUS[1]=0;
        startStopMsg.usOnly=1;
    }


}


void CtrlWindow::sendUSRoiSettings()
{
    usRoiMsg.height=ui->usROI_height_spinBox->value();
    usRoiMsg.width=ui->usROI_width_spinBox->value();
    usRoiMsg.topleftx=ui->usROI_x_spinBox->value();
    usRoiMsg.toplefty=ui->usROI_y_spinBox->value();

    usRoiPub.publish(usRoiMsg);
}

void CtrlWindow::on_Pm_doubleSpinBox_valueChanged(double arg1)
{
    if(ui->PMenable->isChecked())
    {
        mGUIpref.PMposition=arg1;
        mutex.lock();
        guiPreferencesPub.publish(mGUIpref);
        mutex.unlock();

        ROS_INFO_STREAM("Publishing move command at " << mGUIpref.PMposition);
    }

}

void CtrlWindow::on_PMenable_toggled(bool checked)
{
    if(checked)
        mGUIpref.PMposition=ui->Pm_doubleSpinBox->value();
    else
        mGUIpref.PMposition=1;

    mGUIpref.PMenable=checked;
    mutex.lock();
    guiPreferencesPub.publish(mGUIpref);
    mutex.unlock();

    ROS_INFO_STREAM("Publishing move command at " << mGUIpref.PMposition);

}

void CtrlWindow::on_pushButton_clicked()
{
    mROIsettings=new ROIsettings(this);

    mROIsettings->show();

}

void CtrlWindow::on_gravityEn_checkBox_toggled(bool checked)
{
    mGUIpref.gravityEn=checked;

    guiPreferencesPub.publish(mGUIpref);

}

void CtrlWindow::on_refTau_SpinBox_valueChanged(double arg1)
{
    refSetMutex.lock();

    mRefFilMsg.tau=arg1;
    refFilPub.publish(mRefFilMsg);

    refSetMutex.unlock();

}

void CtrlWindow::on_refUb_SpinBox_valueChanged(double arg1)
{
    refSetMutex.lock();

    mRefFilMsg.ub=arg1;
    refFilPub.publish(mRefFilMsg);

    refSetMutex.unlock();
}

void CtrlWindow::on_refLb_SpinBox_valueChanged(double arg1)
{

    refSetMutex.lock();

    mRefFilMsg.lb=arg1;
    refFilPub.publish(mRefFilMsg);

    refSetMutex.unlock();
}

void CtrlWindow::on_saveImage_button_clicked()
{
    batmag_soft::SaveImage srv;

    saveImageClient.call(srv);
}

void CtrlWindow::on_grid_Button_clicked()
{
    if(mGUIpref.gridOn)
        mGUIpref.gridOn=false;

    else
        mGUIpref.gridOn=true;

    guiPreferencesPub.publish(mGUIpref);

}

void CtrlWindow::keyPressEvent(QKeyEvent *event)
{
    ROS_INFO_STREAM("Key Event");
    switch (event->key())
    {

    case Qt::Key_X:
        ui->PMenable->toggle();
        break;

    case Qt::Key_T:
        if(!ui->ctrl_en_checkBox->isChecked() && !ui->gravityEn_checkBox->isChecked())
        {
            ui->gravityEn_checkBox->toggle();
        }
        else
        {
            if(ui->ctrl_en_checkBox->isChecked())
            {
                ui->ctrl_en_checkBox->toggle();
            }
            else
            {
                ui->ctrl_en_checkBox->toggle();
                ui->gravityEn_checkBox->toggle();
            }

        }
        break;

    case Qt::Key_E:
        ui->ctrl_en_checkBox->toggle();
        break;

    case Qt::Key_B:
        ui->ctrl_en_checkBox->toggle();
        ui->gravityEn_checkBox->toggle();
        break;

    case Qt::Key_G:
        ui->gravityEn_checkBox->toggle();
        break;

    case Qt::Key_R:
        ui->record_checkBox->toggle();
        break;

    case Qt::Key_PageUp:
        ui->Pm_doubleSpinBox->setValue(ui->Pm_doubleSpinBox->value()-5);
        ROS_INFO_STREAM("Deceasing PM spinbox value");
        break;

    case Qt::Key_PageDown:
        ui->Pm_doubleSpinBox->setValue(ui->Pm_doubleSpinBox->value()+5);
        ROS_INFO_STREAM("Increasing PM spinbox value");
        break;

    case Qt::Key_4:
        mKey.data=1;
        keyPub.publish(mKey);
        break;

    case Qt::Key_6:
        mKey.data=2;
        keyPub.publish(mKey);
        break;

    case Qt::Key_2:
        mKey.data=3;
        keyPub.publish(mKey);
        break;

    case Qt::Key_8:
        mKey.data=4;
        keyPub.publish(mKey);
        break;

    case Qt::Key_S:
        mKey.data=5;
        keyPub.publish(mKey);
        break;

    case Qt::Key_W:
        mKey.data=6;
        keyPub.publish(mKey);

        break;

    default:
        QMainWindow::keyPressEvent(event);
    }
}

void CtrlWindow::on_cameraSet_pushButton_pressed()
{
    mCamSetW=std::make_shared<CameraSettingsWin>(this);

    mCamSetW->show();
}

void CtrlWindow::on_forceCur_button_clicked()
{
    mFCwind=std::make_shared<ForceCurrentSettingsWindow>(this);
    mFCwind->show();

}

void CtrlWindow::on_resetElmo_pushButton_pressed()
{
    batmag_soft::ResetElmos msg;
    msg.reset=true;
    resetElmosPub.publish(msg);

}

void CtrlWindow::on_ctrlSettings_button_pressed()
{
    mFreqWind=std::make_shared<FrequencyCtrlWindow>(this);
    mFreqWind->show();
}

void CtrlWindow::on_omega_checkBox_toggled(bool checked)
{
    guiPrefMutex.lock();

    mGUIpref.omegaCtrl=checked;
    guiPreferencesPub.publish(mGUIpref);

    guiPrefMutex.unlock();
}


/*

void Controller::setStateFromMsg(const batmag_soft::State::ConstPtr& msg){
    std::vector<double> s;
    s.resize(6);
    for(int i=0; i<6; i++)
        s[i]=msg->state[i];
    setState(s);
    ROS_DEBUG_STREAM("State set at x: "<<state[0]<<", y: "<<state[1]<<", z: "<<state[2]);


}*/


void CtrlWindow::on_Contro_Button_clicked()
{

        int i=3;
        curMutex[i].lock();
        debugCurMsg.mapout[i]=500;
        curMutex[i].unlock();

            mapoutPub.publish(debugCurMsg);
            ROS_INFO_STREAM("Updated Current 4 to "<<debugCurMsg.mapout[i]);
           ROS_INFO_STREAM("STATES!"<<state[0]);


}
