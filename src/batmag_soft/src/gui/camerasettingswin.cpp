#include "camerasettingswin.h"
#include "ui_camerasettingswin.h"

CameraSettingsWin::CameraSettingsWin(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CameraSettingsWin)
{
    ui->setupUi(this);

    ros::ServiceClient getCameraSetClient = n.serviceClient<batmag_soft::getCameraSettings> ("get_camera_settings");


    setPub=n.advertise<batmag_soft::CameraSettings>("camera_settings",1);
    batmag_soft::getCameraSettings srv;
    if(getCameraSetClient.call(srv))
    {
        mCameraSettings.Brightness=srv.response.Brightness;
        mCameraSettings.Exposure=srv.response.Exposure;
        mCameraSettings.Framerate=srv.response.Framerate;
        mCameraSettings.Gain=srv.response.Gain;
        mCameraSettings.Gamma=srv.response.Gamma;
        mCameraSettings.Hue=srv.response.Hue;
        mCameraSettings.Saturation=srv.response.Saturation;
        mCameraSettings.Sharpness=srv.response.Sharpness;
        mCameraSettings.Shutter=srv.response.Shutter;
        mCameraSettings.wbb=srv.response.wbb;
        mCameraSettings.wbr=srv.response.wbr;
    }

    //Set spinners at right value
    ui->brightness_doubleSpinBox->setValue(mCameraSettings.Brightness);
    ui->exposure_doubleSpinBox->setValue(mCameraSettings.Exposure);
    ui->framerate_doubleSpinBox->setValue(mCameraSettings.Framerate);
    ui->gain_doubleSpinBox->setValue(mCameraSettings.Gain);
    ui->gamma_doubleSpinBox->setValue(mCameraSettings.Gamma);
    ui->hue_doubleSpinBox->setValue(mCameraSettings.Hue);
    ui->sat_doubleSpinBox->setValue(mCameraSettings.Saturation);
    ui->sharp_doubleSpinBox->setValue(mCameraSettings.Sharpness);
    ui->shutter_doubleSpinBox->setValue(mCameraSettings.Shutter);
    ui->wbb_doubleSpinBox->setValue(mCameraSettings.wbb);
    ui->wbr_doubleSpinBox->setValue(mCameraSettings.wbr);


}

CameraSettingsWin::~CameraSettingsWin()
{
    delete ui;
}
void CameraSettingsWin::on_brightness_doubleSpinBox_valueChanged(double arg1)
{
    mCameraSettings.Brightness=arg1;

    if(ui->immediate_checkBox->isChecked())
        setPub.publish(mCameraSettings);
}

void CameraSettingsWin::on_exposure_doubleSpinBox_valueChanged(double arg1)
{
    mCameraSettings.Exposure=arg1;
    if(ui->immediate_checkBox->isChecked())
        setPub.publish(mCameraSettings);

}

void CameraSettingsWin::on_sharp_doubleSpinBox_valueChanged(double arg1)
{
    mCameraSettings.Sharpness=arg1;
    if(ui->immediate_checkBox->isChecked())
        setPub.publish(mCameraSettings);

}

void CameraSettingsWin::on_hue_doubleSpinBox_valueChanged(double arg1)
{

    mCameraSettings.Hue=arg1;
    if(ui->immediate_checkBox->isChecked())
        setPub.publish(mCameraSettings);
}

void CameraSettingsWin::on_sat_doubleSpinBox_valueChanged(double arg1)
{

    mCameraSettings.Saturation=arg1;
    if(ui->immediate_checkBox->isChecked())
        setPub.publish(mCameraSettings);
}

void CameraSettingsWin::on_gamma_doubleSpinBox_valueChanged(double arg1)
{
    mCameraSettings.Gamma=arg1;
    if(ui->immediate_checkBox->isChecked())
        setPub.publish(mCameraSettings);

}

void CameraSettingsWin::on_shutter_doubleSpinBox_valueChanged(double arg1)
{

    mCameraSettings.Shutter=arg1;
    if(ui->immediate_checkBox->isChecked())
        setPub.publish(mCameraSettings);
}

void CameraSettingsWin::on_gain_doubleSpinBox_valueChanged(double arg1)
{

    mCameraSettings.Gain=arg1;
    if(ui->immediate_checkBox->isChecked())
        setPub.publish(mCameraSettings);
}

void CameraSettingsWin::on_framerate_doubleSpinBox_valueChanged(double arg1)
{
    mCameraSettings.Framerate=arg1;
    if(ui->immediate_checkBox->isChecked())
        setPub.publish(mCameraSettings);

}

void CameraSettingsWin::on_wbb_doubleSpinBox_valueChanged(double arg1)
{
    mCameraSettings.wbb=arg1;
    if(ui->immediate_checkBox->isChecked())
        setPub.publish(mCameraSettings);

}

void CameraSettingsWin::on_wbr_doubleSpinBox_valueChanged(double arg1)
{
    mCameraSettings.wbr=arg1;
    if(ui->immediate_checkBox->isChecked())
        setPub.publish(mCameraSettings);

}

void CameraSettingsWin::on_apply_Button_pressed()
{
        setPub.publish(mCameraSettings);
}

void CameraSettingsWin::on_close_Button_pressed()
{
    this->close();
}
