#pragma once

#include <QWidget>
#include <QImage>
#include <QMouseEvent>
#include <QPainter>
#include <QDebug>
#include <opencv2/opencv.hpp>

#include "ros/ros.h"

#define MARGIN 50

using namespace cv;

class CvImageWidget : public QWidget
{
    Q_OBJECT

public:
    explicit CvImageWidget(QWidget *parent = 0);

    QSize sizeHint() const { return _qimage.size(); }
    QSize minimumSizeHint() const { return _qimage.size(); }
    QImage getImage() const { return _qimage; }
    double getScale() const { return _scale; }

    void mousePressEvent(QMouseEvent *event);

signals:
    void clicked(int x, int y, Qt::MouseButton button);

public slots:
    void showImage(const Mat& image);

    void showImage(const QImage& image);

    void clear();

protected:

    void paintEvent(QPaintEvent *);

    QImage _qimage;
    Mat _tmp;
    double _scale = 1.0;
};
