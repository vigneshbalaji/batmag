#pragma once

#include "ctrlwindow.h"

//OpenCv
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "recorder.h"

//For Image conversion
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>


//Ros and its messages
#include "ros/ros.h"
#include <ros/callback_queue.h>

//Imaging
#include "cqtopencvviewergl.h"

//Qt stuff
#include <QWidget>
#include <QMutex>
#include <QTimer>

namespace Ui {
class SecondScreenImage;
}

class SecondScreenImage : public QMainWindow
{
    Q_OBJECT

public:
    explicit SecondScreenImage(QWidget *parent = 0);
    ~SecondScreenImage();

    //These images have to be public otherwise the windows can't access them
    cv::Mat cameraImage[2];


signals:
    void newCameraImage(int cameraNum);

private:
    Ui::SecondScreenImage *ui;

    //Image transport parameters
    image_transport::ImageTransport it;
    image_transport::Subscriber imageCamSub[2];

    //Callbacks for image reception
    void imageMessageCallback(const sensor_msgs::ImageConstPtr& msg, int cameraNum);
    void imageCb0(const sensor_msgs::ImageConstPtr& msg);
    void imageCb1(const sensor_msgs::ImageConstPtr& msg);

    void on_newCameraImage(int camNum);

   ros::NodeHandle nh25Hz;


    QMutex mutex;

};
