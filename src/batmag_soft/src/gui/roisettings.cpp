#include "roisettings.h"
#include "ui_roisettings.h"

ROIsettings::ROIsettings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ROIsettings),
    it(n)
{
    ui->setupUi(this);


    ros::ServiceClient getInitSettingsClient0 = n.serviceClient<batmag_soft::getInitTrackerSettings> ("get_tracker_settings0");
    ros::ServiceClient getInitSettingsClient1 = n.serviceClient<batmag_soft::getInitTrackerSettings> ("get_tracker_settings1");


    dennisTest = n.advertise<batmag_soft::TrackerSettings>("dennisTest",1);

    setPub=n.advertise<batmag_soft::TrackerSettings>("tracker_settings",1);
    batmag_soft::getInitTrackerSettings srv;


    if (getInitSettingsClient0.call(srv))
    {
        mMsg[0].displayOn=true;

        //Hue
        ui->maxHue_Slider->setValue(srv.response.maxH);
        ui->minHue_Slider->setValue(srv.response.minH);
        //Saturation
        ui->maxSat_Slider->setValue(srv.response.maxS);
        ui->minSat_Slider->setValue(srv.response.minS);
        //Value
        ui->maxVal_Slider->setValue(srv.response.maxV);
        ui->minVal_Slider->setValue(srv.response.minV);
        //ROI
        ui->agentSize_Slider->setValue(srv.response.agentSize);
        ui->sizeROI_Slider->setValue(srv.response.roiSize);

        mMsg[0].maxH=srv.response.maxH;
        mMsg[0].minH=srv.response.minH;

        mMsg[0].maxS=srv.response.maxS;
        mMsg[0].minS=srv.response.minS;

        mMsg[0].maxV=srv.response.maxV;
        mMsg[0].minV=srv.response.minV;

        mMsg[0].agentSize=srv.response.agentSize;
        mMsg[0].roiSize=srv.response.roiSize;
        mMsg[0].fixed=false;

    }

    if (getInitSettingsClient1.call(srv))
    {
        mMsg[1].displayOn=true;

        //Hue
        ui->maxHue_Slider_2->setValue(srv.response.maxH);
        ui->minHue_Slider_2->setValue(srv.response.minH);
        //Saturation
        ui->maxSat_Slider_2->setValue(srv.response.maxS);
        ui->minSat_Slider_2->setValue(srv.response.minS);
        //Value
        ui->maxVal_Slider_2->setValue(srv.response.maxV);
        ui->minVal_Slider_2->setValue(srv.response.minV);
        //ROI
        ui->agentSize_Slider_2->setValue(srv.response.agentSize);
        ui->sizeROI_Slider_2->setValue(srv.response.roiSize);

        mMsg[1].maxH=srv.response.maxH;
        mMsg[1].minH=srv.response.minH;

        mMsg[1].maxS=srv.response.maxS;
        mMsg[1].minS=srv.response.minS;

        mMsg[1].maxV=srv.response.maxV;
        mMsg[1].minV=srv.response.minV;

        mMsg[1].agentSize=srv.response.agentSize;
        mMsg[1].roiSize=srv.response.roiSize;
        mMsg[1].fixed=false;

    }

    //Listen to camera messages
    roiSub[0]=it.subscribe("roi0", 1, &ROIsettings::imageCb0, this);
    roiSub[1]=it.subscribe("roi1", 1, &ROIsettings::imageCb1, this);


    //Listen to tracker messages
    bsSub[0]=it.subscribe("bs0", 1, &ROIsettings::bsCb0, this);
    bsSub[1]=it.subscribe("bs1", 1, &ROIsettings::bsCb1, this);

    connect(this,SIGNAL(newRoiImage(int)),this, SLOT(on_newRoiImage(int)),Qt::QueuedConnection);
    connect(this,SIGNAL(newBsImage(int)),this, SLOT(on_newBsImage(int)),Qt::QueuedConnection);

    mMsg[0].id=0;
    mMsg[1].id=1;

    mMsg[0].thisIsBack=false;
    mMsg[1].thisIsBack=false;
    mMsg[0].backSubOn=false;
    mMsg[1].backSubOn=false;


    setPub.publish(mMsg[0]);
    setPub.publish(mMsg[1]);


}

ROIsettings::~ROIsettings()
{
    delete ui;
}

void ROIsettings::imageCb0(const sensor_msgs::ImageConstPtr& msg)
{
    imageMessageCallback(msg, 0);
}

void ROIsettings::imageCb1(const sensor_msgs::ImageConstPtr& msg)
{
    imageMessageCallback(msg, 1);
}

void ROIsettings::imageMessageCallback(const sensor_msgs::ImageConstPtr &msg, int roiNum)
{
    ROS_DEBUG_STREAM("Receiving image from camera: "<< roiNum);
    try
    {
        mutex.lock();

        cv_ptr=cv_bridge::toCvCopy(msg);
        roiImage[roiNum] =cv::Mat(cv_ptr->image.size(), CV_8UC1);
        if(cv_ptr->image.rows<=400)
            cv::convertScaleAbs(cv_ptr->image, roiImage[roiNum],100 , 0.0);
        else

            cv::convertScaleAbs(cv_ptr->image, roiImage[roiNum],100*400/cv_ptr->image.rows , 0.0);//Scale Roi to fit window


        mutex.unlock();

        emit newRoiImage(roiNum);
    }
    catch (cv_bridge::Exception& e)
    {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }

}


void ROIsettings::on_newRoiImage(int camNum)
{

    ROS_DEBUG_STREAM("Publishing image from camera: "<< camNum);
    ROS_DEBUG_STREAM("The image has "<< roiImage[camNum].channels()<< " channels ");

    mutex.lock();
    if(camNum==0){
        ui->roi0Image->showImage(roiImage[camNum]);
    }

    else
        if(camNum==1)
        {
            ui->roi1Image->showImage(roiImage[camNum]);
        }
    mutex.unlock();
}



void ROIsettings::bsCb0(const sensor_msgs::ImageConstPtr& msg)
{
    bsMessageCallback(msg, 0);
}

void ROIsettings::bsCb1(const sensor_msgs::ImageConstPtr& msg)
{
    bsMessageCallback(msg, 1);
}

void ROIsettings::bsMessageCallback(const sensor_msgs::ImageConstPtr &msg, int bsNum)
{
    ROS_DEBUG_STREAM("Receiving bs from camera: "<< bsNum);
    try
    {
        mutex.lock();

        cv_ptr=cv_bridge::toCvCopy(msg);
        bsImage[bsNum] =cv::Mat(cv_ptr->image.size(), CV_8UC3);
        if(cv_ptr->image.rows<=400)
            cv::convertScaleAbs(cv_ptr->image, bsImage[bsNum],100 , 0.0);
        else
            cv::convertScaleAbs(cv_ptr->image, bsImage[bsNum],100*400/cv_ptr->image.rows , 0.0);//Scale bs to fit window

        mutex.unlock();

        emit newBsImage(bsNum);
    }
    catch (cv_bridge::Exception& e)
    {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }

}


void ROIsettings::on_newBsImage(int camNum)
{

    ROS_DEBUG_STREAM("Publishing BS image from camera: "<< camNum);
    ROS_DEBUG_STREAM("The BS image has "<< bsImage[camNum].channels()<< " channels ");

    mutex.lock();
    if(camNum==0)
    {
        ui->bs0Image->showImage(bsImage[camNum]);
    }

    else
        if(camNum==1)
        {
            ui->bs1Image->showImage(bsImage[camNum]);
        }
    mutex.unlock();
}


void ROIsettings::on_pushButton_clicked()
{

    mMsg[0].displayOn=false;
    mMsg[1].displayOn=false;


    setPub.publish(mMsg[0]);
    setPub.publish(mMsg[1]);

    dennisTest.publish(mMsg[0]);

    this->close();

}

void ROIsettings::on_sizeROI_Slider_valueChanged(int value)
{
    mMsg[0].roiSize=value;
    setPub.publish(mMsg[0]);
}

void ROIsettings::on_agentSize_Slider_valueChanged(int value)
{
    mMsg[0].agentSize=value;
    setPub.publish(mMsg[0]);
}

void ROIsettings::on_maxHue_Slider_valueChanged(int value)
{
    mMsg[0].maxH=value;
    setPub.publish(mMsg[0]);
}

void ROIsettings::on_minHue_Slider_valueChanged(int value)
{
    mMsg[0].minH=value;
    setPub.publish(mMsg[0]);
}

void ROIsettings::on_maxVal_Slider_valueChanged(int value)
{
    mMsg[0].maxV=value;
    setPub.publish(mMsg[0]);
}

void ROIsettings::on_minVal_Slider_valueChanged(int value)
{
    mMsg[0].minV=value;
    setPub.publish(mMsg[0]);
}

void ROIsettings::on_maxSat_Slider_valueChanged(int value)
{
    mMsg[0].maxS=value;
    setPub.publish(mMsg[0]);
}

void ROIsettings::on_minSat_Slider_valueChanged(int value)
{
    mMsg[0].minS=value;
    setPub.publish(mMsg[0]);
}

void ROIsettings::on_blur_Slider_valueChanged(int value)
{
    mMsg[0].blurSize=value;
    setPub.publish(mMsg[0]);
}

void ROIsettings::on_fixed_checkBox_toggled(bool checked)
{
    mMsg[0].fixed=checked;
    setPub.publish(mMsg[0]);
}


void ROIsettings::on_bs0_button_pressed()
{
    mMsg[0].thisIsBack=true;
    setPub.publish(mMsg[0]);
    mMsg[0].thisIsBack=false;
}


void ROIsettings::on_bs0check_toggled(bool checked)
{
    mMsg[0].backSubOn =checked;
    setPub.publish(mMsg[0]);
}

//Same stuff for 2


void ROIsettings::on_sizeROI_Slider_2_valueChanged(int value)
{
    mMsg[1].roiSize=value;
    setPub.publish(mMsg[1]);
    ROS_INFO_STREAM("This is connected");
}

void ROIsettings::on_agentSize_Slider_2_valueChanged(int value)
{
    mMsg[1].agentSize=value;
    setPub.publish(mMsg[1]);
}

void ROIsettings::on_maxHue_Slider_2_valueChanged(int value)
{
    mMsg[1].maxH=value;
    setPub.publish(mMsg[1]);
}

void ROIsettings::on_minHue_Slider_2_valueChanged(int value)
{
    mMsg[1].minH=value;
    setPub.publish(mMsg[1]);
}

void ROIsettings::on_maxVal_Slider_2_valueChanged(int value)
{
    mMsg[1].maxV=value;
    setPub.publish(mMsg[1]);
}

void ROIsettings::on_minVal_Slider_2_valueChanged(int value)
{
    mMsg[1].minV=value;
    setPub.publish(mMsg[1]);
}

void ROIsettings::on_maxSat_Slider_2_valueChanged(int value)
{
    mMsg[1].maxS=value;
    setPub.publish(mMsg[1]);
}

void ROIsettings::on_minSat_Slider_2_valueChanged(int value)
{
    mMsg[1].minS=value;
    setPub.publish(mMsg[1]);
}

void ROIsettings::on_blur_Slider_2_valueChanged(int value)
{
    mMsg[1].blurSize=value;
    setPub.publish(mMsg[1]);
}

void ROIsettings::on_fixed_checkBox_2_toggled(bool checked)
{
    mMsg[1].fixed=checked;
    setPub.publish(mMsg[1]);
}

void ROIsettings::on_bs1_button_pressed()
{
    mMsg[1].thisIsBack=true;
    setPub.publish(mMsg[1]);
    mMsg[1].thisIsBack=false;

}

void ROIsettings::on_bs1check_toggled(bool checked)
{
    mMsg[1].backSubOn =checked;
    setPub.publish(mMsg[1]);
}

void ROIsettings::on_pushButton_pressed()
{
    mMsg[0].displayOn=false;
    mMsg[1].displayOn=false;
    setPub.publish(mMsg[0]);
    setPub.publish(mMsg[1]);

}
