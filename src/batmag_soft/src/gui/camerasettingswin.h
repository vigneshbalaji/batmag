#pragma once
#include "batmag_soft/getCameraSettings.h"
#include "batmag_soft/CameraSettings.h"

#include "ros/ros.h"

#include <QDialog>

namespace Ui {
class CameraSettingsWin;
}

class CameraSettingsWin : public QDialog
{
    Q_OBJECT

public:
    explicit CameraSettingsWin(QWidget *parent = 0);
    ~CameraSettingsWin();

private:
    Ui::CameraSettingsWin *ui;

    ros::NodeHandle n;

    ros::Publisher setPub;

    batmag_soft::CameraSettings mCameraSettings;

private slots:
    void on_brightness_doubleSpinBox_valueChanged(double arg1);

    void on_exposure_doubleSpinBox_valueChanged(double arg1);

    void on_sharp_doubleSpinBox_valueChanged(double arg1);

    void on_hue_doubleSpinBox_valueChanged(double arg1);

    void on_sat_doubleSpinBox_valueChanged(double arg1);

    void on_gamma_doubleSpinBox_valueChanged(double arg1);

    void on_shutter_doubleSpinBox_valueChanged(double arg1);

    void on_gain_doubleSpinBox_valueChanged(double arg1);

    void on_framerate_doubleSpinBox_valueChanged(double arg1);

    void on_wbb_doubleSpinBox_valueChanged(double arg1);

    void on_wbr_doubleSpinBox_valueChanged(double arg1);

    void on_apply_Button_pressed();

    void on_close_Button_pressed();


};

