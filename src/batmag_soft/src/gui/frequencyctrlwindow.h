#pragma once
#include "ros/ros.h"
#include "batmag_soft/SetInitParameters.h"
#include "batmag_soft/GetInitParameters.h"

#include "batmag_soft/CtrlOutput.h"
#include "batmag_soft/State.h"
#include "batmag_soft/Reference.h"

#include <QString>
#include <QDialog>
#include <QMutex>
#include <QTimer>
#include <QTime>

// opencv stuff
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include <Eigen/Core>

namespace Ui {
class FrequencyCtrlWindow;
}

class FrequencyCtrlWindow : public QDialog
{
    Q_OBJECT

public:
    explicit FrequencyCtrlWindow(QWidget *parent = 0);
    ~FrequencyCtrlWindow();

    void onCtrlMsg(const batmag_soft::CtrlOutput::ConstPtr& msg);
    void onStateMsg(const batmag_soft::State::ConstPtr& msg);
    void onRefMsg(const batmag_soft::Reference::ConstPtr& msg);

private slots:
    void on_set_pushButton_pressed();

    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

    void on_invertRotation_pushButton_pressed();

    void updateImages();

private:
    Ui::FrequencyCtrlWindow *ui;


    ros::NodeHandle n;

    QMutex mRefMutex, mStateMutex, mCbMutex, imgMutex[2];

    ros::ServiceClient getInitParClient;
    ros::ServiceClient setInitParClient;

    batmag_soft::SetInitParameters setSrv;
    batmag_soft::GetInitParameters getSrv;

    //Plotting stuff for graphics
    ros::Subscriber ctrlSub, stateSub, refSub;
    Eigen::Vector3d pos, ref, ctrl,diff, sclDiff,sclCtrl;

    cv::Mat img[2];

    bool clockwise=1;


    // Time
    boost::posix_time::ptime nowT, prevT;
    boost::posix_time::time_duration diffT;
    double secTime;
    QTimer mTimer;

    ros::Publisher rotFieldPub;
};
