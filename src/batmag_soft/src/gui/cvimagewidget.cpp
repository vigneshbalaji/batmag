#include "cvimagewidget.h"

CvImageWidget::CvImageWidget(QWidget *parent): QWidget(parent)
{

}

void CvImageWidget::mousePressEvent(QMouseEvent *event) {
    if(event->button() == Qt::LeftButton || event->button() == Qt::RightButton)
        emit clicked(event->x(), event->y(), event->button());
}

void CvImageWidget::showImage(const Mat& image) {
    // Convert to RGB format

    char key;
    cv::imshow("image", image);
    key = cv::waitKey(30);

    ROS_INFO_STREAM("About to convert the image to be shown. It has format: "<< image.type());
    switch(image.type()) {
    case CV_8UC1:
        cvtColor(image, _tmp, CV_GRAY2RGB);
        ROS_INFO_STREAM("The image is in black and white");
        break;
    case CV_8UC3:
        ROS_INFO_STREAM("The BGR image is being converted");
        cvtColor(image, _tmp, CV_BGR2RGB);
        break;
    default:
        ROS_ERROR_STREAM("The image does not belong to any of the predefined formats");
        break;
    }
    // QImage needs data to be continously stored in memory
    assert(_tmp.isContinuous());
    // Assign OpenCV's image buffer to the QImage. Each pixel has three bytes
    _qimage = QImage(_tmp.data, _tmp.cols, _tmp.rows, _tmp.cols*3, QImage::Format_RGB888);

    if(!_qimage.isNull())//(_qimage.width() > parentWidget()->width() - MARGIN)
    {
        _scale = ((double)(parentWidget()->width() - MARGIN))/((double) _qimage.width());
        _qimage = _qimage.scaledToWidth(parentWidget()->width() - MARGIN);
    }

    if(!_qimage.isNull())//(_qimage.height() > parentWidget()->height() - MARGIN)
    {
        _scale *= ((double)(parentWidget()->height() - MARGIN))/((double) _qimage.height());
        _qimage = _qimage.scaledToHeight(parentWidget()->height() - MARGIN);
    }

    this->setFixedSize(_qimage.width(), _qimage.height());

    repaint();
}

void CvImageWidget::showImage(const QImage& image) {
    _qimage = image;

    if(_qimage.width() > parentWidget()->width() - MARGIN)
    {
        _scale = ((double)(parentWidget()->width() - MARGIN))/((double) _qimage.width());
        _qimage = _qimage.scaledToWidth(parentWidget()->width() - MARGIN);
    }

    if(_qimage.height() > parentWidget()->height() - MARGIN)
    {
        _scale *= ((double)(parentWidget()->height() - MARGIN))/((double) _qimage.height());
        _qimage = _qimage.scaledToHeight(parentWidget()->height() - MARGIN);
    }

    this->setFixedSize(_qimage.width(), _qimage.height());

    repaint();
}

void CvImageWidget::clear() {
    _qimage = QImage();
    repaint();
}

void CvImageWidget::paintEvent(QPaintEvent *) {
    QPainter painter(this);
    painter.drawImage(QPoint(0,0), _qimage);
    painter.end();
}
