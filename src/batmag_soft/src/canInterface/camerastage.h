#pragma once

#include <sys/time.h>
#include <PCANBasic.h>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread/thread.hpp>

#include "ros/ros.h"
#include "canadapter.h"


class CameraStage
{
public:
    CameraStage(int id=10, CANadapter * Can=NULL);
    virtual ~CameraStage();
    virtual void init();
    void setSpeed(float value);
    void off();

protected:
    int id;
    CANadapter * myCan;
    bool terminated=false;
    float oldSetp,setp;
};

