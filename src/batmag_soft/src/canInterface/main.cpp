#include "ros/duration.h"
#include "ros/spinner.h"
#include "ros/ros.h"
#include "batmag_soft/MapOutput.h"
#include "batmag_soft/MoveCamera.h"
#include "batmag_soft/StartStop.h"
#include "batmag_soft/ResetElmos.h"
#include "batmag_soft/GUIpreferences.h"
#include "std_msgs/Float64.h"
#include "allelmos.h"

#include <boost/function.hpp>
#include <boost/bind.hpp>

void pressedStart(const batmag_soft::StartStop::ConstPtr& msg, bool & startPressed, bool & stopPressed)
{
    startPressed=msg->pressedStart;
    stopPressed=msg->pressedStop;

}

void mapOutCb(const batmag_soft::MapOutput::ConstPtr& msg, std::vector<double> &ref, bool & newRef)
{
    if(!newRef)
        newRef=true;
    int size=msg->size;
    ref.resize(size);
    for(int i=0; i<size; i++)
    {

        ROS_DEBUG_STREAM("Writing Map Message #"<<i<<" up to "<< size<< " with value "<< msg->mapout[i]);
        ref[i]=msg->mapout[i];
    }


}

//void guiPrefCb(const batmag_soft::GUIpreferences::ConstPtr & msg, AllElmos & mElmos)
//{
//    if(msg->PMenable)
//        mElmos.myPMmoveTo(msg->PMposition, false);
//}

void resetElmos(const batmag_soft::ResetElmos::ConstPtr & msg, AllElmos & mElmos)
{
    if(msg->reset)
        for(int i=0; i<9;++i)
        {
            mElmos.coil[i].off();
            mElmos.coil[i].init();
        }

    ROS_WARN_STREAM("Resetting elmos");

}

int main(int argc, char *argv[])
{   
    double canFreq=50;
    ros::init(argc, argv, "CAN_node");
    ros::NodeHandle n;
    ROS_INFO_STREAM("The CAN node has started");

    //Wait for the node to be online
    while(!ros::ok())
        usleep(500);


    std::vector<double> references;
    for(int i=0;i<10;i++)
    {
        references.push_back(0);
    }

    bool newRef=false;

    //Subscribe to Start/Stop messages
    bool startPressed=false, stopPressed=false;
    ros::Subscriber startStopSub = n.subscribe<batmag_soft::StartStop>("gui_start_stop", 1, std::bind(pressedStart, std::placeholders::_1, std::ref(startPressed), std::ref(stopPressed)));
    ros::Subscriber mapOutSub=n.subscribe<batmag_soft::MapOutput>("map_output",1,  std::bind(mapOutCb, std::placeholders::_1, std::ref(references), std::ref(newRef)));

    //Main Elmo Class
    AllElmos mAllElmos;
    //Move the cameras on GUI Request
    ros::Subscriber movCamSub= n.subscribe("move_camera",5, &AllElmos::moveCameraCallback, &mAllElmos);
    //Move the US stage when requested
    ros::Subscriber usMoveSub=n.subscribe("us_move", 1, &AllElmos::moveUSCallback, &mAllElmos);
    //Reset Coil Elmos
    ros::Subscriber resetElmosSub=n.subscribe<batmag_soft::ResetElmos>("reset_elmos",1, std::bind(resetElmos, std::placeholders::_1, std::ref(mAllElmos)));

    ros::Rate canRate(canFreq);

    while(ros::ok() && !stopPressed)
    {
        ros::spinOnce();
        if(newRef)
        {
            mAllElmos.setRef(references);

            newRef=false;

//            ROS_INFO_STREAM("New references Set");
//            for(int i=0; i<10; ++i)
//                ROS_INFO_STREAM("The "<<i<<"-th reference is: "<<references[i]);
        }
        canRate.sleep();
    }
    


    mAllElmos.terminate();
    return 0;

}
