#pragma once

// CAN message buffer size and data acquisition
#define MSG_BUF_SIZE			128
#define WITH_SYNCHRONIZATION

#include <stdio.h>
#include <sstream>
#include <exception>
#include <iostream>

// for _beginthread()
//#include <opencv/cv.h>// for _beginthread()

// CAN bus includes
// CAN bus includes
#include<mutex>
#include <PCANBasic.h>


typedef TPCANMsg CANMsg;

/**
 * CAN message buffer
 *
 * Stores a number of CAN messages received from channel into a buffer.
 *
 * @author      G.J. Vrooijink (Guus)
 * @since       2011-12-30
 */
class MessageBuffer
{
        // For multithreading purposes to prevent data corruption
        #ifdef WITH_SYNCHRONIZATION
                std::mutex  m_hMutex;	// Used to limit access to specific data
        #endif

        public:
                /**
                 * CAN message buffer Constructor
                 *
                 * By default the CAN message buffer will created with buffer size specified
                 * in (#define MSG_BUF_SIZE)
                 *
                 * @author		G.J. Vrooijink (Guus)
                 */
                MessageBuffer();
                /**
                 * CAN message buffer Destructor
                 *
                 * @author		G.J. Vrooijink (Guus)
                 */
                ~MessageBuffer();

                /**
                 * Put message in message buffer
                 *
                 * This function puts a received CAN message in the message buffer
                 *
                 * @author		G.J. Vrooijink (Guus)
                 * @param		msgSingle	Message to be put into the message buffer
                 */
                void setMessage(CANMsg msgSingle);
                /**
                 * Seach in message buffer for specific integer data (Elmo related)
                 *
                 * This function searches for a specific received message in the message buffer.
                 * The integer value of a specific parameter received from a Elmo controller can be
                 * retreived by using this function. The most recent received data will be returned.
                 * NOTE: Function is related to ELMO motor controllers.
                 * NOTE: Whether  the data is integer or float can be found in the command reference
                 * manual of the Elmo motor controller.
                 *
                 * @author		G.J. Vrooijink (Guus)
                 * @param		temp		The requested data to retreive from the message buffer
                 * @param		*pValue		Returns a integer with the requested parameter data
                 * @return		Returns		0 no message found and 1 on success.
                 */
                int LookUp(CANMsg temp, int *pValue);
                /**
                 * Seach in message buffer for specific float data (Elmo related)
                 *
                 * This function searches for a specific received message in the message buffer.
                 * The float value of a specific parameter received from a Elmo controller can be
                 * retreived by using this function. The most recent received data will be returned.
                 * NOTE: Function is related to ELMO motor controllers
                 * NOTE: Whether  the data is integer or float can be found in the command reference
                 * manual of the Elmo motor controller.
                 *
                 * @author		G.J. Vrooijink (Guus)
                 * @param		temp		The requested data to retreive from the message buffer
                 * @param		*pValue		Returns a float with the requested parameter data
                 * @return		Returns		0 no message found and 1 on success.
                 */
                int LookUp(CANMsg temp, float *pValue);

        private:
                CANMsg msgBuf[MSG_BUF_SIZE];	// CAN message buffer
                int msgCnt, msgSearch;			// Variables related to specific message search in buffer
};
