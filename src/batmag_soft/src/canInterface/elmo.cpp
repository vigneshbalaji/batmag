#include "elmo.h"

Elmo::Elmo(int id, elmoType initType, CANadapter * Can):setp(0), oldSetp(0),type(initType)
{
    ROS_INFO_STREAM("Elmo constructor id" << id);
    myCan=Can;
    //Type 0 is for coils 1 is for motors
    if (id > 0 && id <= 127)
        this->id = id;
    else
        ROS_INFO_STREAM("The Elmo id "<<id<< "is not acceptable");
    //    if(myCan->canOk())
    if(myCan!=NULL)
        init();
    //    else
    //        ROS_ERROR_STREAM("Elmo initialization failed. The can bus is not initiliazied");
}

Elmo::~Elmo()
{
    ROS_INFO_STREAM("Now Elmo id "<<id<<" is getting destroyed");
//    setRef(0);

}

void Elmo::init()
{
        unsigned char msg[2];
        int frc;
        int Identifier = COB_RPDO2 + id;
        if (type == COIL_ELMO)
        {
            //Disable motor
            //            For details look end of file
            msg[0] = 'M';
            msg[1] = 'O';
            frc = myCan->sendInt(Identifier, msg, 0);

            //Unit Mode 1
            //            Torque control mode
            //            In this mode, the motor current command is set directly by the TC
            //            software command or by an analog reference signal. This mode is
            //            useful for torque or force control when the servo drive is used only as
            //            an inner device within an external feedback loop.
            msg[0] = 'U';
            msg[1] = 'M';
            frc = myCan->sendInt(Identifier, msg, 1);

            //Enable motor
            msg[0] = 'M';
            msg[1] = 'O';
            frc = myCan->sendInt(Identifier, msg, 1);

            ROS_INFO_STREAM("The Elmo for coil with id "<<id<< " is initialized with type" <<type);

        } else if (type == MAXON_MOTOR_ELMO)
        {
            msg[0] = 'M';
            msg[1] = 'O';
            frc = myCan->sendInt(Identifier, msg, 0);

            //Unit Mode 2
            //              Speed control mode
            //              In this mode, the motor speed command is set directly by the JV
            //              software jogging command or by an analog reference signal.
            msg[0] = 'U';
            msg[1] = 'M';
            frc = myCan->sendInt(Identifier, msg, 2);

            //              PM - Profiler Mode
            //              Purpose:
            //              Specifies, in UM=2, if the software speed command generator applies acceleration,
            //              deceleration and smoothing limits.
            //              The values of PM are outlined in the following table:
            //              PM Value Description
            //              0 For UM=2, AC, DC and SF are not used.
            //              1 For UM=2, AC, DC and SF are used normally.
            msg[0] = 'P';
            msg[1] = 'M';
            frc = myCan->sendInt(Identifier, msg, 1);

            //              AC - Acceleration
            //              Purpose:
            //              Defines the maximum acceleration in counts/second 2 . This parameter is used in speed
            //              mode (UM=2) and position control modes (UM=3, 4, 5) in PTP (PA, PR) and jogging (JV)
            //              reference modes.
            msg[0] = 'A';
            msg[1] = 'C';
            frc = myCan->sendFloat(Identifier, msg, 500000.0);

            //              DC - Deceleration
            //              Purpose:
            //              Defines the maximum deceleration in counts/seconds2. This parameter is used in
            //              profiled speed control mode (UM=2, PM=1) and in position point-to-point (PA, PR) and
            //              jogging (JV) motions (UM=3, UM=4 and UM=5). The DC parameter does not affect the
            //              present motion. It is used to plan the next motion, initiated by a BG command.
            msg[0] = 'D';
            msg[1] = 'C';
            frc = myCan->sendFloat(Identifier, msg, 500000.0);

            msg[0] = 'M';
            msg[1] = 'O';
            frc = myCan->sendInt(Identifier, msg, 1);

            ROS_INFO_STREAM("The Elmo for Maxon Motor with id "<<id<< " is initialized with type" <<type);
        }

        else
        {
            ROS_ERROR("Unknown Elmo id %i type %i.", id, type);
            throw(1);
        }
}

void Elmo::setRef(float value)
{
    //    if(myCan->canOk())
    //    {
    //Only send reference if the setpoint is new to avoid overloading the channel
    if (value != oldSetp) {
        setp = value;
        if(myCan==NULL)
            ROS_ERROR_STREAM("There is no CAN adapter");
        ROS_INFO_STREAM("The current value to be set is: "<< value);
        oldSetp = setp;
        //                newSetPoint = true;

            unsigned char msg[2];
            int frc;
            int Identifier = COB_RPDO2 + id;
            if (type == COIL_ELMO)
            {

                //            TC - Torque Command
                //            Purpose:
                //            Sets the torque (motor current) command, in amperes, for the torque-control software-
                //            reference modes (UM=1 and UM=3). TC commands are accepted in the range permitted
                //            by the present torque command limits (refer to the PL[N] and CL[N] commands).
                //            If TC is set greater than CL[1], after a few seconds, the current limit of the servo drive
                //            will drop to CL[1].
                //            TC defines the reference value IQ (the ID comma

                msg[0] = 'T';
                msg[1] = 'C';
                frc = myCan->sendFloat(Identifier, msg, value);
            }
            else if (type == MAXON_MOTOR_ELMO)
            {
                //                ST - Stop Motion
                //                Purpose:
                //                Stops the software motion. The software commands decelerate to a complete stop using
                //                the SD deceleration. ST does not affect the external position reference and has no effect
                //                for the following conditions: MO=0 and UM=1.
                msg[0] = 'S';
                msg[1] = 'T';
                frc = myCan->sendInt(Identifier, msg, 0, 4);

                //                JV- Jogging Velocity
                //                Purpose:
                //                Sets the motor speed. In speed control mode (UM=2), the JV parameter specifies the
                //                software speed command. In un-profiled mode (PM=0), the speed command is set to JV
                //                immediately. In profiled mode (PM=1), the speed command is gradually changed to JV,
                //                according to the AC, DC and SF parameters.
                //                In the position control modes (UM=4, 5), the JV setting defines a constant speed software
                //                command. The value of JV defines the speed of the motion.
                //                The parameters AC, DC and SF determine the acceleration limits for reaching final speed.
                //                In position-jogging mode (JV), and if the position feedback sensor is set to modulo
                //                counting (refer to XM[N] and YM[N]), a position-controlled motor can rotate forever. The
                //                position reading will jump each modulo count according to the last modulo setting, but
                //                the speed will remain steady.
                msg[0] = 'J';
                msg[1] = 'V';
                frc = myCan->sendFloat(Identifier, msg, value);

                //                BG - Begin Motion
                //                Purpose:
                //                Immediately starts the next programmed motion.
                //                 In software speed mode (UM=2), BG activates the latest JV, and also the new smooth
                //                factor (SF), acceleration (AC) and deceleration (DC).
                //                 In stepper or position mode (UM=3, 4 or 5), BG starts the latest position mode
                //                programmed: a point-to-point motion (PA), a jogging motion (JV) or any type of
                //                tabulated motion (PVT or PT).
                //                Each motion mode starts with its entire set of parameters. For example, starting a point-
                //                to-point motion activates the present of acceleration (AC), deceleration (DC), smooth
                //                factor (SF) and speed (SP).
                //                The BG command may be used to modify the parameters of the present mode, and not
                //                only to program new modes. For example, a BG command in point-to-point mode
                //                modifies the active AC parameters (and all other active motion parameters) with its last
                //                programmed value.
                //                A “hardware BG” can be accepted via one of the digital inputs (refer to the IL[N]
                //                command).
                msg[0] = 'B';
                msg[1] = 'G';
                frc = myCan->sendInt(Identifier, msg, 0, 4);
            }
            ROS_INFO_STREAM("Set current reference to " << value << " A on Elmo ID"<<id);
        }
//    }
//    else
//        ROS_ERROR_STREAM("The CAN-bus is not ok");
}

//End of File

//MO - Motor Enable/Disable
//Purpose:
//Enables and disables (freewheels) the motor power.
//
//Disabling the motor
//MO=0 disables the motor. This is the idle state of the drive. The power stage is
//disabled and no current flows in the motor. In this mode, the servo drive can perform
//various tasks that are impossible when the motor is on:
// Boot on power up
// Calculate and check the integrity of the drive database
// Download new firmware and user programs
// Save parameters in the flash memory
// Modify setup data that cannot be modified on-the-fly, such as commutation
//parameters (CA[N]) and unit mode (UM)
//The servo driver is automatically disabled when a motor fault is captured (MF). An
//attempt to enable the motor may fail if the conditions of the fault still exist.
//If the brake function is activated, the MO=0 command duration is extended
//according to the BP[N] command specification.
//
//Enabling the motor
//MO=1 is the operative state of the servo drive, driving the motor and activating and
//executing the programmed motion. The software runs a set of tests to ensure that all
//conditions for running the motor are met.
//If MO is set to 1 and the motor is already on, nothing happens.
//When the motor is enabled, the drive reinitializes the internal parameters and motion
//drivers. The drive may fail to start if the setup data is found to be inconsistent (for
//example, CA[4]=CA[5]). In this case, CD commands indicate the reason for the
//failure.
//The last captured motor fault (MF) is reset to zero.
//In the position control modes (UM=4, 5), the motor is always started so that it does
//not jump. The total position control command — which consists of the internal
//position command and the external position command — is set to the actual present
//position of the motor in order to prevent the motor from jumping.
//Notes:
//
//When RM=1 the servo drive will attempt to start (set MO=1) automatically after
//power on. A servo drive that has been shut down for any reason will attempt an
//automatic restart every few milliseconds. Automatic restart will occur only if the
//Enable switch (INH) is active (refer to the IL[N] command). In all other modes, the
//MO=0 state is maintained until MO is explicitly changed by a software command.
//3-93SimplIQ Command Reference Manual
//Alphabetical Listing
//MAN-SIMCR (Ver. 4.5)
// In encoder-only systems (in which no digital Hall sensors are present), the
//commutation is calculated only once after power up upon the first MO=1
//command. The motor moves a few encoder counts during the automatic
//commutation search.
// When the brake function is enabled, the dedicated output is released after the
//duration defined in BP[N]. During this time, all motion reference commands are
//ignored.
// After a motor fault (MF>0), the motor can be re-enabled only when the cause of
//the fault is no longer present.
// If MO=1 fails with the “Amplifier not ready” error code (EC=66), the exact reason
//for the fault can be found by querying the MF command.
// MO=0 disables the motor for 200 sampling time (about 10 milliseconds) until a
//new MO=1 can restart the motor.
// In the “Ready to switch on”, “Switch on disabled” and “Fault” states (refer to the
//Elmo CANopen Implementation Manual), MO=1 is blocked and returns an error.
//These states can be command controlled only by a CAN master using the DS402
//standard control word (object 0x6040).
// Setting MO=1 in the “Switched on” CAN state transfer the state of the state
//machine to “Operation enabled.” Setting MO=0 in “Operation Enabled” or “Quick
//stop active” state will transfer the state machine to “Switched on.” (Refer to CAN
//object 0x6040 in the Elmo CANopen Implementation Manual).
