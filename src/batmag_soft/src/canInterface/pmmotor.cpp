#include "pmmotor.h"

PMMotor::PMMotor(int elmoid, CANadapter * Can, bool isUS): usMotor(isUS)
{
    // Init of variables
    myCan=Can;
    init=false;
    InitPz=0;
    position=0;

    id=elmoid;
    usMotor=isUS;

    if(isUS)
    {
        encZstageUNT=kUsEncZstageUNT;
        leadZstageUNT=kUsLeadZstageUNT;
        zmaxUNT=kUsZmaxUNT;
    }
    else
    {
        encZstageUNT=kPmEncZstageUNT;
        leadZstageUNT=kPmLeadZstageUNT;
        zmaxUNT=kPmZmaxUNT;
    }

//    // Init pose
//    H_PD_TO_PDI = (Mat_<double>(4,4) << 1, 0, 0, 0,
//                                        0, 1, 0, 0,
//                                        0, 0, 1, 0,
//                                        0, 0, 0, 1);

//    // Jacobian
//    Jp = (Mat_<double>(3,3) << 1, 0,  0,
//                               0, 1,  0,
//                               0, 0, -1);
}

PMMotor::~PMMotor()
{
}

void PMMotor::initialize()
{
    // Beginning with initialization
    ROS_INFO_STREAM( "==========================================================");
    ROS_INFO_STREAM("Start with initialization of Permanent Magnet Motor with id."<< id);


    // Initialization velocities in mm/sec
    if(!usMotor)
        Vz = 25;
    else
        Vz= 35;
    // Initialization velocities in mm/(enc. pulses sec)
    Zv = Vz*encZstageUNT/leadZstageUNT;


//    PositionMode();
    // Turn motors on and wait 500 mSec
    //Add address for RPDO2
    myCan->sendInt(id,"MO",1);

    // Set velocity and far initial position for Z-stage and begin motion

    myCan->sendInt(id,"SP",Zv);

    myCan->sendInt(id,"PA",-1000000);

    myCan->cmd(id,"BG");

    double timeWait=((zmaxUNT/Vz)*1000+500)*1000; //usleep(((zmaxUNT*1500/Vz)+500)*1000);
    // Wait until limit switch is reached (Time calculated from worse case + 500ms).
    usleep(timeWait);
    ROS_INFO_STREAM("Waiting [ms]: " << timeWait<<" the motor speed is "<<Vz<<" the stroke length is "<< zmaxUNT<< " and the estimated time would be: "<< zmaxUNT/Vz);
    myCan->cmd(id,"ST");

    ROS_INFO_STREAM("Moving 1mm");
    // Move 1 mm forward to release limit switches
    Pz=1;
    Zp = Pz*encZstageUNT/leadZstageUNT;
    myCan->sendInt(id,"PR",Zp);
    myCan->cmd(id,"BG");
    sleep(1);

    // Turn motors off
    myCan->sendInt(id,"MO",0);

    // Set the initial positions for all encoders
    myCan->sendInt(id,"PX",0);
    usleep(200000);

    MotorsON();

    init=true;


    // Done
    ROS_INFO_STREAM("Initialization of Permanent Magnet motor done.");
    ROS_INFO_STREAM("==========================================================");
}

void PMMotor::requestPositions()
{
    myCan->cmd(id,"PX");
}

void PMMotor::requestVelocities()
{
    myCan->cmd(id,"VX");
}

int PMMotor::getEncValues()
{
    myCan->read(id,"PX",&encoderValue);

    return (encoderValue);
}

double PMMotor::getPosition()
{
    getEncValues();
    position=((double)(encoderValue))*leadZstageUNT/encZstageUNT;

    return (position);
}

double PMMotor::getVelocities()
{
//    myCan->read("VX",&encoderValue);

//    position=((double)(encoderValue))*LeadZstageUNT/EncZstageUNT;

    return (Vz);
}


void PMMotor::CurrentMode()
{
    // Putting Permanent Magnet Motor  in current mode
    ROS_INFO_STREAM("==========================================================");
    ROS_INFO_STREAM("Setting the Permanent Magnet Motor  in current control mode.");
    ROS_INFO_STREAM( "Please wait...");

    // Enable current mode
    myCan->sendInt(id,"MO",0); // Elmo's off
    myCan->sendInt(id,"UM",1); // Current mode
    myCan->sendInt(id,"MO",1); // Elmo's on

    // Done
    ROS_INFO_STREAM( "Permanent Magnet linear stage is now in current control mode.");
    ROS_INFO_STREAM( "==========================================================");
}

void PMMotor::PositionMode()
{
    // Putting Permanent Magnet Motor  in position control mode
    ROS_INFO_STREAM( "==========================================================");
    ROS_INFO_STREAM( "Setting the Permanent Magnet Motor  in current position mode.");
    ROS_INFO_STREAM( "Please wait...\n");

    // Disable current mode
    myCan->sendInt(id,"MO",0); // Elmo's off
    myCan->sendInt(id,"UM",5); // Single loop position mode
    myCan->sendInt(id,"MO",1); // Elmo's on

    // Done
    ROS_INFO_STREAM( "Permanent Magnet linear stage is now in position control mode.");
    ROS_INFO_STREAM( "==========================================================");
}

void PMMotor::VelocityMode()
{
    // Putting Permanent Magnet Motor  in position control mode
    ROS_INFO_STREAM( "==========================================================");
    ROS_INFO_STREAM( "Setting the Permanent Magnet Motor  in velocity control mode.");
    ROS_INFO_STREAM( "Please wait...\n");

    // Disable current mode
    myCan->sendInt(id,"MO",0); // Elmo's off
    myCan->sendInt(id,"UM",2); // Velocity mode
    myCan->sendInt(id,"MO",1); // Elmo's on

    // Done
    ROS_INFO_STREAM( "Permanent Magnet linear stage is now in velocity control mode.");
    ROS_INFO_STREAM( "==========================================================");
}

void PMMotor::setSpeed(double speed)
{
    myCan->sendInt(id,"SP",speed*encZstageUNT/leadZstageUNT);
}

void PMMotor::setAbsolutePos(double Z)
{
    if(Z==0)//Avoid setting the motor to 0 (gives problems at init)
        Z=1;
    // Set absolute position and begin motion
    myCan->sendInt(id,"PA",Z*encZstageUNT/leadZstageUNT);
    ROS_INFO_STREAM("Moving US stage to"<<Z*encZstageUNT/leadZstageUNT<< "counts");
    myCan->cmd(id,"BG");
}

void PMMotor::setRelativePos(double Z)
{
    // Set absolute position and begin motion
    myCan->sendInt(id,"PR",Z*encZstageUNT/leadZstageUNT);
    myCan->cmd(id,"BG");
}

void PMMotor::setVelocity(double Z)
{
    // Set velocity and begin motion
    myCan->sendInt(id,"JV",Z*encZstageUNT/leadZstageUNT);
    myCan->cmd(id,"BG");
}

void PMMotor::MotorsON()
{

    myCan->sendInt(id,"MO",1);
}

void PMMotor::MotorsOFF()
{
    setAbsolutePos(1);

    // Wait until limit switch is reached (Time calculated from worse case + 500ms).
    usleep(((zmaxUNT*1500/Vz)+500)*1000);
    myCan->cmd(id,"ST");

    myCan->sendInt(id,"MO",0);
    ROS_INFO_STREAM("PM Motor off");
}

void PMMotor::StopMotion()
{
    myCan->cmd(id,"ST");
}
