#pragma once

#include "canadapter.h"
#include "coilelmo.h"
#include "ros/ros.h"
#include "pmmotor.h"
#include "camerastage.h"
#include "batmag_soft/MoveCamera.h"
#include "std_msgs/Float64.h"

#include <mutex>

#define WAIT_POS 5000

class AllElmos
{
public:
    AllElmos();
    virtual ~AllElmos();

    //Set currents for coil elmos
    void setCurrent(int coilVectorId, float current);

//Moves the motor with Id to a certain position
//    if wait=true returns when the motion is complete
//    if wait=false it return after sending the command
//    returns true if there were no errors
    bool myPMmoveTo(float pos, bool wait=true);
    bool myUSmoveTo(float pos, bool wait=true);

    //Set references from force current map output
    void setRef(std::vector<double> ref);
    double pmRef=1;

    //The coils Elmo Controllers
    CoilElmo coil[9];

    //The Permanent Magnet Elmo Controller (CAN ID =12)
    PMMotor mPermMag, mUSstage;
    //Linear Stages Controllers (CAN ID = 10 & 11)
    CameraStage mCameraStage[2];

    //return the position of the PM
    // If wait=true it returns an updated value
    // if wait=false it sends a request for an updated value and returns the previously read one
    // if wait=false Federico reccomends to run the function a second time after a few instructions
    double getPMPosition(bool wait=true);
    double getUSPosition(bool wait=true);

    //sets the moving velocity (in mm/s)
    void setPMVelocity(double velocity);
    void setUSVelocity(double velocity);

    //return the moving velocity
    // If wait=true it returns an updated value
    // if wait=false it sends a request for an updated value and returns the previously read one
    // if wait=false Federico reccomends to run the function a second time after a few instructions
    double getPMVelocity(bool wait=true);
    double getUSVelocity(bool wait=true);

    void moveCameraCallback(const batmag_soft::MoveCamera::ConstPtr &msg);
    void moveUSCallback(const std_msgs::Float64::ConstPtr &msg);

    //Kills all the elmos
    void terminate();

private:
    //The canAdapter
    CANadapter myCan;


    std::mutex mMutex;

};
