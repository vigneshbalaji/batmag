#pragma once

#include <sys/time.h>
#include <PCANBasic.h>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread/thread.hpp>

#include "ros/ros.h"
#include "canadapter.h"


class CoilElmo
{
public:
    CoilElmo(int id=1, CANadapter * Can=NULL);
    virtual ~CoilElmo();
    virtual void init();
    void setRef(float value);
    void off();

protected:
    int id;
    bool newSetPoint;
    double oldSetp,setp;
    bool hasNewSetp=false;
    CANadapter * myCan;
};

