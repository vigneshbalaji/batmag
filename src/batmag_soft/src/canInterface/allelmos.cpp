#include "allelmos.h"

AllElmos::AllElmos(): myCan()
{
    myCan.openChannel();
    //        Elmos IDs in std vectors are from 0, but in CAN they start from 1
    //        So we have to add 1 for can comunications
    for(int i=0; i<9; ++i)
        coil[i]=CoilElmo(i+1, &myCan);


//    ROS_INFO_STREAM("The motor is at position:" << getPMPosition(true));
    for(int i=0;i<2;++i)
    {
        mCameraStage[i]= CameraStage(10+i, &myCan);
    }

    mPermMag=PMMotor(12, &myCan);
    mPermMag.initialize();

    mUSstage=PMMotor(13, &myCan, true);
    mUSstage.initialize();


}

void AllElmos::setCurrent(int coilVectorId, float current)
{
    if(coilVectorId>=0 && coilVectorId<9)
    {
        coil[coilVectorId].setRef(current);
    }
    else
        ROS_ERROR_STREAM("Not CoilElmo ID. Current could not be set.");
}



void AllElmos::moveCameraCallback(const batmag_soft::MoveCamera::ConstPtr& msg)
{
    for (int i=0; i<2;i++)
    {
        mCameraStage[i].setSpeed(msg->cameraSpeed[i]);
    }
}


void AllElmos::moveUSCallback(const std_msgs::Float64::ConstPtr &msg)
{
    mUSstage.setAbsolutePos(msg->data);
}


bool AllElmos::myPMmoveTo(float pos, bool wait)
{
    mPermMag.setAbsolutePos(pos);
    if (!wait)
        return true;
    else
    {
        double cpos=-1;
        while(cpos>=pos+0.05 || cpos<=pos-0.05)
        {
            mPermMag.requestPositions();
            usleep(WAIT_POS);
            cpos=mPermMag.getPosition();
        }
    }
    return true;
}

bool AllElmos::myUSmoveTo(float pos, bool wait)
{
    mUSstage.setAbsolutePos(pos);
    if (!wait)
        return true;
    else
    {
        double cpos=-1;
        while(cpos>=pos+0.05 || cpos<=pos-0.05)
        {
            mUSstage.requestPositions();
            usleep(WAIT_POS);
            cpos=mPermMag.getPosition();
        }
    }
    return true;
}

double AllElmos::getPMPosition(bool wait)
{
    mPermMag.requestPositions();
    if(wait)  usleep(WAIT_POS);
    return mPermMag.getPosition();

}

double AllElmos::getUSPosition(bool wait)
{
    mUSstage.requestPositions();
    if(wait)  usleep(WAIT_POS);
    return mUSstage.getPosition();

}

void AllElmos::setRef(std::vector<double> ref)
{

    int size=ref.size();
    ROS_DEBUG_STREAM("Size of ref is "<<size);
    for(int i=0; i<9;++i)
    {
        ROS_DEBUG_STREAM("Setting current "<<i<<" that is: "<<ref[i]);
        if(ref[i]<=5 && ref[i]>=-5)
            setCurrent(i, ref[i]);
        if(ref[i]>5)
            setCurrent(i, 5);
        if(ref[i]<-5)
            setCurrent(i, -5);
    }
    ROS_DEBUG_STREAM("Value of ref[9] is: "<<ref[9]);
    if(size>9 && ref[9]!=pmRef)
    {

        ROS_INFO_STREAM("Moving PM to: "<<ref[9]);
        pmRef=ref[9];
        mPermMag.setAbsolutePos(ref[9]);
//        mUSstage.setAbsolutePos(ref[9]);
    }
}

void AllElmos::setPMVelocity(double velocity)
{
    mPermMag.setVelocity(velocity);
}

void AllElmos::setUSVelocity(double velocity)
{
    mUSstage.setVelocity(velocity);
}
double AllElmos::getPMVelocity(bool wait)
{
    mPermMag.requestVelocities();
    if(wait)  usleep(WAIT_POS);
    return mPermMag.getVelocities();
}

double AllElmos::getUSVelocity(bool wait)
{
    mUSstage.requestVelocities();
    if(wait)  usleep(WAIT_POS);
    return mUSstage.getVelocities();
}

void AllElmos::terminate()
{
    if(myCan.canOk())
    {
    //A separate command should be used to close the channel
    for(int i=0; i<9; i++)
        coil[i].off();
    mPermMag.MotorsOFF();
    mUSstage.MotorsOFF();
    mCameraStage[0].off();
    mCameraStage[1].off();
    }

    else
        ROS_ERROR_STREAM("ERROR terminating the Elmo Controllers. The CAN bus is not ok");


    myCan.closeChannel();
}

AllElmos::~AllElmos()
{

    ROS_INFO_STREAM("Destructing all elmos");
}

