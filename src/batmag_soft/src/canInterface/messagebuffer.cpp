#include "messagebuffer.h"

MessageBuffer::MessageBuffer()
{
        msgCnt=0;													// Buffer position
        msgSearch=0;												// Buffer search variable
}

MessageBuffer::~MessageBuffer()
{
}

void MessageBuffer::setMessage(CANMsg msgSingle)
{
        #ifdef WITH_SYNCHRONIZATION
                        m_hMutex.lock();
        #endif

        if(127==msgCnt)
        {
                msgCnt=0;									// Start overwriting at the first message
        }

        msgBuf[msgCnt]=msgSingle;						// Add message to buffer
        msgCnt++;										// Update buffer position

        #ifdef WITH_SYNCHRONIZATION
                        m_hMutex.unlock();
        #endif
}

int MessageBuffer::LookUp(CANMsg temp, int *pValue)
{
        // Variables used
        BYTE Temp[4];		// Requested parameter from buffer
        int fndCode=0;		// 0 = not (yet) found a message

        #ifdef WITH_SYNCHRONIZATION
                        // Wait for Mutex to be released
                        m_hMutex.lock();
        #endif

        // Search in message buffer
        for(msgSearch=msgCnt-1;msgSearch!=msgCnt;msgSearch--)
        {
                // Reset search index number
                if(0==msgSearch)
                {
                        msgSearch=MSG_BUF_SIZE-1;
                }

                // Compare requisted parameters with data in message buffer
                if(msgBuf[msgSearch].ID==temp.ID &&
                        msgBuf[msgSearch].DATA[0]==temp.DATA[0] &&
                        msgBuf[msgSearch].DATA[1]==temp.DATA[1] &&
                        msgBuf[msgSearch].DATA[2]==temp.DATA[2] &&
                        msgBuf[msgSearch].DATA[3]==temp.DATA[3])
                {
                        Temp[0]=msgBuf[msgSearch].DATA[4];	// Retreive data byte 4
                        Temp[1]=msgBuf[msgSearch].DATA[5];	// Retreive data byte 5
                        Temp[2]=msgBuf[msgSearch].DATA[6];	// Retreive data byte 6
                        Temp[3]=msgBuf[msgSearch].DATA[7];	// Retreive data byte 7
                        *pValue = *(int*)Temp;				// Convert data to integer
                        fndCode=1;							// 1 = message found
                        msgSearch=msgCnt+1;					// The recent message is found in at this buffer position
                }
        }

        #ifdef WITH_SYNCHRONIZATION
               m_hMutex.unlock();
        #endif

        return fndCode;
}
int MessageBuffer::LookUp(CANMsg temp, float *pValue)
{
        // Variables used
        BYTE Temp[4];		// Requested parameter from buffer
        int fndCode=0;		// 0 = not (yet) found a message

        #ifdef WITH_SYNCHRONIZATION
                        // Wait for Mutex to be released
                        m_hMutex.lock();
        #endif

        // Search in message buffer
        for(msgSearch=msgCnt-1;msgSearch!=msgCnt;msgSearch--)
        {
                // Reset search index number
                if(0==msgSearch)
                {
                        msgSearch=MSG_BUF_SIZE-1;
                }

                // Compare requisted parameters with data in message buffer
                if(msgBuf[msgSearch].ID==temp.ID &&
                        msgBuf[msgSearch].DATA[0]==temp.DATA[0] &&
                        msgBuf[msgSearch].DATA[1]==temp.DATA[1] &&
                        msgBuf[msgSearch].DATA[2]==temp.DATA[2] &&
                        msgBuf[msgSearch].DATA[3]==temp.DATA[3])
                {
                        Temp[0]=msgBuf[msgSearch].DATA[4];	// Retreive data byte 4
                        Temp[1]=msgBuf[msgSearch].DATA[5];	// Retreive data byte 5
                        Temp[2]=msgBuf[msgSearch].DATA[6];	// Retreive data byte 6
                        Temp[3]=msgBuf[msgSearch].DATA[7];	// Retreive data byte 7
                        *pValue = *(int*)Temp;				// Convert data to float
                        fndCode=1;							// 1 = message found
                        msgSearch=msgCnt+1;					// The recent message is found in at this buffer position
                }
        }

        #ifdef WITH_SYNCHRONIZATION
                m_hMutex.unlock();
        #endif

        return fndCode;
}
