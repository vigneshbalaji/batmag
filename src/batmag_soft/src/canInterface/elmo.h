#pragma once

#include <sys/time.h>
#include <PCANBasic.h>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread/thread.hpp>

#include "ros/ros.h"
#include "canadapter.h"

enum elmoType
{
    COIL_ELMO, MAXON_MOTOR_ELMO
};

class Elmo
{
public:
    Elmo(int id=1, elmoType initType=COIL_ELMO, CANadapter * Can=NULL);
    virtual ~Elmo();
    virtual void init();
    void setRef(float value);

protected:
    int id;
    bool newSetPoint;
    double oldSetp,setp;
    elmoType type;
    bool hasNewSetp=false;
    CANadapter * myCan;
};

