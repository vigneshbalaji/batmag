#pragma once

#include <stdio.h>
#include <unistd.h>
#include <asm/types.h>


#include <sys/time.h>
#include <byteswap.h>
#include <PCANBasic.h>
#include <string>

//For sleep commands
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread/thread.hpp>

#include <mutex>

#include"ros/ros.h"
#include "messagebuffer.h"

const int COB_NMT		= 0<<7;
const int COB_TPDO2		= 5<<7;
const int COB_RPDO2		= 6<<7;
const int MASK_ID		= 0x7f;
const int MASK_COB		= 0x780;
const int NMT_CMD_RESET	= 0x82;//0x81 is node reset, 0x82 is communication reset (node reset+ reset of communication parameters and enter preoperational mode)
const int NMT_CMD_START	= 0x1;//Go operational
const int NMT_CMD_STOP	= 0x2;//Stop remote node

const float AFTER_MSG_DELAY =3; //Milliseconds to wait after the can message has beeen sent to allow its propagation (theoretical limit 0.2ms)

class CANadapter
{
public:
    CANadapter();
    ~CANadapter();

    bool canOk();

    bool closeChannel();

    bool openChannel();



    DWORD sendInt(int id, const char * cmd, int data, int dataLength=8);
    DWORD sendFloat(int id, const char * msg, float data, int dataLength=8);
    

    /**
     * Get an actual value of specific parameter
     *
     * Request the value of a specific parameter of the Elmo motor controller.
     *
     * @author		G.J. Vrooijink (Guus)
     * @param		*param		The requested parameter in Elmo motor controller.
     * @param		index		Index number of parameter in case of a array, by default 0.
     */
    void get(int id, const char *param,unsigned int index=0);
    /**
     * Send a command to the Elmo motor controller
     *
     * This sends a specific command to the Elmo motor controller.
     *
     * @author		G.J. Vrooijink (Guus)
     * @param		*param		The command parameter to send to the Elmo motor controller.
     */
    void cmd(int id, const char *param);
    /**
     * Read received messages from Elmo motor controller
     *
     * This reads a specific requested message from received message buffer, which contains integer data.
     *
     * @author		G.J. Vrooijink (Guus)
     * @param		*param		The requested parameter of the Elmo motor controller.
     * @param		*pValue		The actual value of the requested parameter of the Elmo motor controller.
     * @param		index		Index number of parameter in case of a array, by default 0.
     */
    int read(int id, const char *param,int *pValue, unsigned int index=0);
    /**
     * Read received messages from Elmo motor controller
     *
     * This reads a specific requested message from received message buffer, which contains float data.
     *
     * @author		G.J. Vrooijink (Guus)
     * @param		*param		The requested parameter of the Elmo motor controller.
     * @param		*pValue		The actual value of the requested parameter of the Elmo motor controller.
     * @param		index		Index number of parameter in case of a array, by default 0.
     */
    int read(int id, const char *param,float *pValue, unsigned int index=0);


    //DEBUGGING ONLY
    //Sends an 0x6040 bit7 rising-edge to the elmo
    void reset(int id);

    //Sends 128 packages of 11 consecutive recessive bits (1)
    void busRecovery();

protected:
    int fd;
    unsigned char p_XMT_data[8];
    int frc;//Function Return Code


    void nmtStart();
    void nmtReset();

    TPCANMsg msg;

    MessageBuffer msgBuf;
    std::mutex mMutex;

    void printMsg(TPCANMsg msg);
};
