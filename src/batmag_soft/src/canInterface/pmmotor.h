#pragma once

//G.J. Vrooijink (Guus)'s Ultrasound Guided Needle Tracker

#include "coilelmo.h"
#include "ros/ros.h"
#include "canadapter.h"


// Stage parameters for PM  motor
double const kPmEncZstageUNT=28672;   // Number of encoder pulses per lead screw rotation of Z-stage
double const kPmLeadZstageUNT=5;       // Lead of the lead screw in mm/rotation (Z-stage)
double const kPmZmaxUNT=73;		// 82 mm max stroke on Z-stage

// Stage parameter for US Motor

double const kUsEncZstageUNT=8769.23;   // Number of encoder pulses per lead screw rotation of Z-stage
double const kUsLeadZstageUNT=10;       // Lead of the lead screw in mm/rotation (Z-stage)
double const kUsZmaxUNT=50;		// 82 mm max stroke on Z-stage


class PMMotor
{
public:

    //Variables

    double encZstageUNT;
    double leadZstageUNT;
    double zmaxUNT;

    PMMotor(int elmoid=12, CANadapter *Can = NULL, bool isUS=false);
    ~PMMotor();

    /**
                 * Initialize the stage
                 *
                 * All stages of the UgNT will be moved from an unknown initial position to a
                 * known position. At this position all encoders will be initialized.
                 *
                 * @author      G.J. Vrooijink (Guus)
                 */
    void initialize();
    /**
                 * Request position information of Ultrasound-Guided Needle Tracker
                 *
                 * Request encoder values of all motor controllers. To get the encoder values use
                 * 'getEncValues()' function or the 'getPositions()' for real mm values.
                 *
                 * @author      G.J. Vrooijink (Guus)
                 */
    void requestPositions();
    /**
                 * Request velocities information of Ultrasound-Guided Needle Tracker
                 *
                 * Request velocity values of all motor controllers.
                 *
                 * @author      G.J. Vrooijink (Guus)
                 */
    void requestVelocities();
    /**
                 * Get the encoder values
                 *
                 * Acquire the requested encoder values of all motor controllers.
                 *
                 * @author      G.J. Vrooijink (Guus)
                 * @return		Returns		the encoder values
                 */
    int getEncValues();
    /**
                 * Get the positions
                 *
                 * Converts encoder values to proper mm values for every stage.
                 *
                 * @author      G.J. Vrooijink (Guus)
                 * @return		Returns		the position vector in mm for X,Y & Z stages [x,y,z,1]^T
                 */
    double getPosition();
    /**
                 * Get the velocities
                 *
                 * Obtain the actual velocities for every stage in mm/s values.
                 *
                 * @author      G.J. Vrooijink (Guus)
                 * @return		Returns		the velocity vector in mm/s for X,Y & Z stages [x,y,z]^T
                 */
    double getVelocities();
    /**
                 * Position the Ultrasound-Guided Needle Tracker out of the view of the CCD cameras
                 *
                 * @author      G.J. Vrooijink (Guus)
                 */
    void outofview();
    /**
                 * Find the needle insertion location
                 *
                 * @author      G.J. Vrooijink (Guus)
                 * @return		Returns		TRUE if the needle is found and FALSE if not.
                 */
    bool findNeedleIns();
    /**
                 * Goto the needle insertion location
                 *
                 * NOTE: The needle insertion location needs to be determined first by using findNeedleIns().
                 *
                 * @author      G.J. Vrooijink (Guus)
                 */
    void gotoNeedleIns();
    /**
                 * Put all Elmo motor controller in current mode
                 *
                 * Enables it to directly control the outputted motor current.
                 *
                 * @author      G.J. Vrooijink (Guus)
                 */
    void CurrentMode();
    /**
                 * Put all Elmo motor controller in position control mode
                 *
                 * Enables the position controller within the Elmo motor controller.
                 *
                 * @author      G.J. Vrooijink (Guus)
                 */
    void PositionMode();
    /**
                 * Put all Elmo motor controller in velocity control mode
                 *
                 * Enables the velocity controller within the Elmo motor controller.
                 *
                 * @author      G.J. Vrooijink (Guus)
                 */
    void VelocityMode();
    /**
                 * Turn all motors ON
                 *
                 * @author      G.J. Vrooijink (Guus)
                 */
    void MotorsON();
    /**
                 * Turn all motors OFF
                 *
                 * @author      G.J. Vrooijink (Guus)
                 */
    void MotorsOFF();
    /**
                 * Stop motion of all linear stages
                 *
                 * @author      G.J. Vrooijink (Guus)
                 */
    void StopMotion();
    /**
                 * Set the speed of all stages
                 *
                 * @author      G.J. Vrooijink (Guus)
                 * @param		speed		Speed of XYZ-stage in mm/sec
                 */
    void setSpeed(double speed);
    /**
                 * Set absolute position of X-stage
                 *
                 * Sets the position of the X-stage in mm with respect to its initialized position.
                 * NOTE: This function will start the motion directly.
                 *
                 * @author      G.J. Vrooijink (Guus)
                 * @param		x			Absolute position of the X-stage in mm
                 */
    void setAbsolutePos(double z);

    /**
                 * Set relative position of Z-stage
                 *
                 * Sets the position of the Z-stage in mm with respect to its current position.
                 * NOTE: This function will start the motion directly.
                 *
                 * @author      G.J. Vrooijink (Guus)
                 * @param		z			Relative position of the Z-stage in mm
                 */
    void setRelativePos(double z);

    /**
                 * Set velocity oft he stage
                 *
                 * Sets velocity of the Z-stage in mm/sec
                 * NOTE: This function will start the motion directly.
                 * NOTE: Overloaded function
                 *
                 * @author      G.J. Vrooijink (Guus)
                 * @param		velocity	Velocity vector (mm/sec)
                 */
    void setVelocity(double velocity);
//    /**
//                 * Updates the end-effector of the Ultrasound-Guided Needle Tracker
//                 *
//                 * A reference frame for the UgNT is set to allow coordinate transformations between
//                 * this and other objects that are also used during the procedure.
//                 *
//                 * @author      G.J. Vrooijink (Guus)
//                 * @param		pos			All joint positions in mm
//                 * @return		Returns		The transformation matrix from end-effector to init position
//                 */
//    Mat getPose(Mat pos);
//    /**
//                 * Get the Jacobian of the Ultrasound-Guided Needle Tracker
//                 *
//                 * @author      G.J. Vrooijink (Guus)
//                 * @return		Returns			the Jacobian matrix of the Ultrasound-Guided Needle Tracker
//                 */
//    Mat getJacobian();

private:
    int encoderValue;			// Encoder values of XYZ stages
    double position;			// Position values for XYZ stages in (absolute mm)
    double Zv;				// Velocities of X, Y and Z stages in mm/(enc. pulses sec)
    double Vz;				// Velocities of X, Y and Z stages in mm
    double Zp;				// Absolute or relative positions for position control in mm/(enc. pulses)
    double Pz;				// Absolute or relative positions for position control in mm
    double InitPz;	// Initial position for ultrasound probe for needle insertion
    bool init;						// Initialization flag if needle insertion point is defined
    CANadapter * myCan;
    int id;
    bool usMotor;
//    Mat H_PD_TO_PDI;				// Homogeneous transformation matrix end-effector to initialization
//    Mat Jp;							// Jacobian of ultrasound-guided needle tracker
};
