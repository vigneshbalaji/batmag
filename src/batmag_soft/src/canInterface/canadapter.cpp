#include "canadapter.h"

CANadapter::CANadapter(): msgBuf()
{

    ROS_INFO_STREAM("CAN channel created");




}

//First sleep is commented as it is not necessary to avoid flooding the bus (as long as every message is followed by a sleep


bool CANadapter::openChannel()
{
    TPCANStatus status;
    char ErrorDescription[255];
    boost::this_thread::sleep(boost::posix_time::milliseconds(AFTER_MSG_DELAY));

    mMutex.lock();
    status = CAN_Initialize(PCAN_PCIBUS1, PCAN_BAUD_1M, 0, 0, 0);
    mMutex.unlock();
    boost::this_thread::sleep(boost::posix_time::milliseconds(AFTER_MSG_DELAY));

    if(status!=PCAN_ERROR_OK)
    {
        CAN_GetErrorText(status,0x09, ErrorDescription);
        ROS_ERROR_STREAM("Open Channel function ERROR:" <<ErrorDescription);

        ROS_ERROR_STREAM("Trying again");
        mMutex.lock();
        status=CAN_Uninitialize(PCAN_PCIBUS1);
        mMutex.unlock();
        boost::this_thread::sleep(boost::posix_time::milliseconds(AFTER_MSG_DELAY));

        mMutex.lock();
        status = CAN_Initialize(PCAN_PCIBUS1, PCAN_BAUD_1M, 0, 0, 0);
        mMutex.unlock();
        boost::this_thread::sleep(boost::posix_time::milliseconds(AFTER_MSG_DELAY));

            if(status!=PCAN_ERROR_OK)
            {
                ROS_ERROR_STREAM("Open Channel function ERROR:" <<ErrorDescription);


        return false;
            }
            else
                ROS_INFO_STREAM("Channel correctly opened");
    }
    else
        ROS_INFO_STREAM("Channel correctly opened");

    nmtReset();

    nmtStart();


    return true;
}


CANadapter::~CANadapter()
{

    ROS_INFO_STREAM("Can adapter destroyed");
}

bool CANadapter::closeChannel()
{
    TPCANStatus status;
    char ErrorDescription[255];

    //Reset can channel
    nmtReset();


    mMutex.lock();
    status=CAN_Uninitialize(PCAN_PCIBUS1);
    mMutex.unlock();

    boost::this_thread::sleep(boost::posix_time::milliseconds(AFTER_MSG_DELAY));

    if(status!=PCAN_ERROR_OK)
    {
        CAN_GetErrorText(status,0x09, ErrorDescription);
        ROS_ERROR_STREAM("Close Channel function ERROR:" <<ErrorDescription);
        return false;
    }
    else
        ROS_INFO_STREAM("Channel correctly closed");
}

void CANadapter::nmtStart()
{
    TPCANStatus status;
    char ErrorDescription[255];
    TPCANMsg msg;

    msg.ID = COB_NMT;
    msg.MSGTYPE = MSGTYPE_STANDARD;
    msg.LEN = 8;

    msg.DATA[0] =NMT_CMD_START;
    for(int i=1; i<8; i++)
        msg.DATA[i] =0x00;

    //Avoid overflooding the can bus
//    boost::this_thread::sleep(boost::posix_time::milliseconds(AFTER_MSG_DELAY));

    mMutex.lock();
    status = CAN_Write(PCAN_PCIBUS1, &msg);
    mMutex.unlock();

    boost::this_thread::sleep(boost::posix_time::milliseconds(AFTER_MSG_DELAY));

    printMsg(msg);

    if(status!=PCAN_ERROR_OK)
    {
        CAN_GetErrorText(status,0x09, ErrorDescription);
        ROS_ERROR_STREAM("There was an error Starting NMT: " <<ErrorDescription);
    }

}

void CANadapter::nmtReset()
{
    TPCANStatus status;
    char ErrorDescription[255];
    TPCANMsg msg;

    msg.ID = COB_NMT;
    msg.MSGTYPE = MSGTYPE_STANDARD;
    msg.LEN = 8;

    msg.DATA[0] =NMT_CMD_RESET;
    for(int i=1; i<8; i++)
        msg.DATA[i] =0x00;

    //Avoid overflooding the can bus
//    boost::this_thread::sleep(boost::posix_time::milliseconds(AFTER_MSG_DELAY));

    mMutex.lock();
    status = CAN_Write(PCAN_PCIBUS1, &msg);
    mMutex.unlock();
    int id = id;

    boost::this_thread::sleep(boost::posix_time::milliseconds(AFTER_MSG_DELAY));

    printMsg(msg);

    if(status!=PCAN_ERROR_OK)
    {
        CAN_GetErrorText(status,0x09, ErrorDescription);
        ROS_ERROR_STREAM("There was an error Resetting NMT: " <<ErrorDescription);
    }

}

void CANadapter::busRecovery()
{
    int i=0;
    TPCANStatus status;
    char ErrorDescription[255];
    TPCANMsg msg;

    msg.ID = 0xFF;
    msg.MSGTYPE = MSGTYPE_STANDARD;
    msg.LEN = 8;
    for(int i=0; i<8; i++)
        msg.DATA[i] =0xFF;

    for(i=0;i<129; i++)
    {
    //Avoid overflooding the can bus
//    boost::this_thread::sleep(boost::posix_time::milliseconds(AFTER_MSG_DELAY));

    mMutex.lock();
    status = CAN_Write(PCAN_PCIBUS1, &msg);
    mMutex.unlock();
    int id = id;

    boost::this_thread::sleep(boost::posix_time::milliseconds(AFTER_MSG_DELAY));

    ROS_INFO("Sent Recovery Message #%i:  - R ID:%4x LEN:%1x DATA:%02x %02x %02x %02x %02x %02x %02x %02x\n",
             i,
             (int)msg.ID, (int)msg.LEN,
             (int)msg.DATA[0], (int)msg.DATA[1],
            (int)msg.DATA[2], (int)msg.DATA[3],
            (int)msg.DATA[4], (int)msg.DATA[5],
            (int)msg.DATA[6], (int)msg.DATA[7]);
    }
}

bool CANadapter::canOk()
{

    mMutex.lock();
    TPCANStatus status=CAN_GetStatus(PCAN_PCIBUS1);
    mMutex.unlock();
    if (status != PCAN_ERROR_OK) {
        ROS_ERROR("The canbus is not Ok. Error %x\n",(int)status);
        return false;
    }
    else
        return true;

}

void CANadapter::reset(int id){


    TPCANStatus status;
    TPCANMsg msg;
    msg.ID = 0x6040 + id;
    msg.MSGTYPE = MSGTYPE_STANDARD;
    msg.LEN = 8;

    msg.DATA[0] =0;
    msg.DATA[1] =0;
    msg.DATA[2] = 0;
    msg.DATA[3] = 0;
    msg.DATA[4] = 0;
    msg.DATA[5] = 0;
    msg.DATA[6] = 0;
    msg.DATA[7] = 1;

    //Avoid overflooding the can bus
//    boost::this_thread::sleep(boost::posix_time::milliseconds(AFTER_MSG_DELAY));

    mMutex.lock();
    status = CAN_Write(PCAN_PCIBUS1, &msg);
    mMutex.unlock();

    boost::this_thread::sleep(boost::posix_time::milliseconds(AFTER_MSG_DELAY));

    msg.DATA[7]=0;


//    boost::this_thread::sleep(boost::posix_time::milliseconds(AFTER_MSG_DELAY));

    mMutex.lock();
    status = CAN_Write(PCAN_PCIBUS1, &msg);
    mMutex.unlock();

    boost::this_thread::sleep(boost::posix_time::milliseconds(AFTER_MSG_DELAY));


    msg.DATA[7] = 1;



//    boost::this_thread::sleep(boost::posix_time::milliseconds(AFTER_MSG_DELAY));

    mMutex.lock();
    status = CAN_Write(PCAN_PCIBUS1, &msg);
    mMutex.unlock();

    boost::this_thread::sleep(boost::posix_time::milliseconds(AFTER_MSG_DELAY));

}


void CANadapter::printMsg(TPCANMsg msg)
{
    ROS_DEBUG("Sent Message:  - R ID:%4x LEN:%1x DATA:%02x %02x %02x %02x %02x %02x %02x %02x\n",
             (int)msg.ID, (int)msg.LEN,
             (int)msg.DATA[0], (int)msg.DATA[1],
            (int)msg.DATA[2], (int)msg.DATA[3],
            (int)msg.DATA[4], (int)msg.DATA[5],
            (int)msg.DATA[6], (int)msg.DATA[7]);

}


TPCANStatus CANadapter::sendInt(int id, const char * cmd, int data, int dataLength)
{

    int LittleEndianIndex, LittleEndianValue, myindex, value;

    char ErrorDescription[255];
    TPCANStatus status;
    TPCANMsg msg;

    msg.ID = id+COB_RPDO2;
    msg.MSGTYPE = MSGTYPE_STANDARD;
    msg.LEN = dataLength;

    msg.DATA[0] =(unsigned short) cmd[0];
    msg.DATA[1] =(unsigned short) cmd[1];

    // Taking care of endians for 16 bit index number
    //But my index is of 32 bits. So is this correct?
    myindex = 0;
    LittleEndianIndex = bswap_16(myindex);
    msg.DATA[2] = (int) ((LittleEndianIndex >> 8) & 0XFF);
    msg.DATA[3] = (int) ((LittleEndianIndex & 0XFF));

    // Taking care of endians for 32 bit value numbers:
    value = data;
    LittleEndianValue = bswap_32(value);
    msg.DATA[4] = (int) ((LittleEndianValue >> 24) & 0xFF);
    msg.DATA[5] = (int) ((LittleEndianValue >> 16) & 0xFF);
    msg.DATA[6] = (int) ((LittleEndianValue >> 8) & 0XFF);
    msg.DATA[7] = (int) ((LittleEndianValue & 0XFF));

    //Avoid overflooding the can bus
//    boost::this_thread::sleep(boost::posix_time::milliseconds(AFTER_MSG_DELAY));

    mMutex.lock();
    status = CAN_Write(PCAN_PCIBUS1, &msg);
    mMutex.unlock();

    boost::this_thread::sleep(boost::posix_time::milliseconds(AFTER_MSG_DELAY));

    printMsg(msg);

    if(status!=PCAN_ERROR_OK)
    {
        CAN_GetErrorText(status,0x09, ErrorDescription);
        ROS_ERROR_STREAM("There was an error sending an integer message: " <<ErrorDescription);
    }

    return status;
}

TPCANStatus CANadapter::sendFloat(int id, const char * cmd, float data, int dataLength)
{
    TPCANMsg msg;
    TPCANStatus status;
    int LittleEndianIndex, LittleEndianValue, myindex, floatData, floatToLong;
    float valuef;

    char ErrorDescription[255];


    msg.ID = id+COB_RPDO2;
    msg.LEN = dataLength;
    msg.MSGTYPE = MSGTYPE_STANDARD;

    msg.DATA[0] =(unsigned int) cmd[0];
    msg.DATA[1] =(unsigned int) cmd[1];

    // Setting float value
    floatData = 128;

    // Taking care of endians for 16 bit index number
    myindex = 0;
    LittleEndianIndex = bswap_16(myindex);
    msg.DATA[2] = (int) ((LittleEndianIndex >> 8) & 0XFF);
    msg.DATA[3] = (int) ((LittleEndianIndex & 0XFF)) + floatData;

    // Taking care of endians for 32 bit value numbers:
    valuef = data;
    floatToLong = *(unsigned long*) (&valuef);
    LittleEndianValue = bswap_32(floatToLong);
    msg.DATA[4] = (int) ((LittleEndianValue >> 24) & 0xFF);
    msg.DATA[5] = (int) ((LittleEndianValue >> 16) & 0xFF);
    msg.DATA[6] = (int) ((LittleEndianValue >> 8) & 0XFF);
    msg.DATA[7] = (int) ((LittleEndianValue & 0XFF));

    //Avoid overflooding the can bus
//    boost::this_thread::sleep(boost::posix_time::milliseconds(AFTER_MSG_DELAY));

    mMutex.lock();
    status = CAN_Write(PCAN_PCIBUS1, &msg);
    mMutex.unlock();

    boost::this_thread::sleep(boost::posix_time::milliseconds(AFTER_MSG_DELAY));

    printMsg(msg);

    if(status!=PCAN_ERROR_OK)
    {
        CAN_GetErrorText(status,0x09, ErrorDescription);
        ROS_ERROR_STREAM("There was an error sending the float message: " <<ErrorDescription);
    }

    return status;
}


void CANadapter::get(int id, const char *param,unsigned int index)
{
    TPCANMsg msg;
    msg.ID = id+COB_RPDO2;
    msg.MSGTYPE = MSGTYPE_STANDARD;

    int LittleEndianIndex;
    // COB and ID, length of bytes flags and timestamp
    msg.ID=COB_RPDO2+id;
    msg.LEN=4;

    // Two command character in ASCII
    msg.DATA[0]=param[0];
    msg.DATA[1]=param[1];

    // Taking care of endians for 16 bit index number
    LittleEndianIndex = bswap_16(index);
    msg.DATA[2] = (int)((LittleEndianIndex >> 8) & 0XFF);
    msg.DATA[3] = (int)((LittleEndianIndex & 0XFF));

    // NO DATA
    msg.DATA[4] = 0;
    msg.DATA[5] = 0;
    msg.DATA[6] = 0;
    msg.DATA[7] = 0;

    // Writing ELMO CAN node command
    boost::this_thread::sleep(boost::posix_time::milliseconds(AFTER_MSG_DELAY));

    mMutex.lock();
    CAN_Write(PCAN_PCIBUS1, &msg);
    mMutex.unlock();

    boost::this_thread::sleep(boost::posix_time::milliseconds(AFTER_MSG_DELAY));
}

void CANadapter::cmd(int id, const char *param)
{
    TPCANMsg msg;
    msg.MSGTYPE = MSGTYPE_STANDARD;
    // COB and ID, length of bytes flags and timestamp
    msg.ID=COB_RPDO2+id;
    msg.LEN=4;

    // Two command character in ASCII
    msg.DATA[0]=param[0];
    msg.DATA[1]=param[1];

    // NO DATA
    msg.DATA[2] = 0;
    msg.DATA[3] = 0;
    msg.DATA[4] = 0;
    msg.DATA[5] = 0;
    msg.DATA[6] = 0;
    msg.DATA[7] = 0;

    // Writing ELMO CAN node command
    boost::this_thread::sleep(boost::posix_time::milliseconds(AFTER_MSG_DELAY));

    mMutex.lock();
    CAN_Write(PCAN_PCIBUS1, &msg);
    mMutex.unlock();

    boost::this_thread::sleep(boost::posix_time::milliseconds(AFTER_MSG_DELAY));
}

int CANadapter::read(int id, const char *param, int *pValue, unsigned int index)
{
    TPCANMsg temp;
    temp.MSGTYPE = MSGTYPE_STANDARD;

    int LittleEndianIndex, floatData;
    // Setting float value
    floatData=128;

    // COB and ID, length of bytes flags and timestamp
    temp.ID=COB_TPDO2+id;
    temp.LEN=4;

    // Two command character in ASCII
    temp.DATA[0]=param[0];
    temp.DATA[1]=param[1];

    // Taking care of endians for 16 bit index number
    LittleEndianIndex = bswap_16(index);
    temp.DATA[2] = (int)((LittleEndianIndex >> 8) & 0XFF);
    temp.DATA[3] = (int)((LittleEndianIndex & 0XFF));

    // Check if the requested message is in the message buffer
    int fndCode = msgBuf.LookUp(temp, pValue);

    return fndCode;	 // Requested message found? 0=NO, 1=YES
}
int CANadapter::read(int id, const char *param, float *pValue, unsigned int index)
{

    int LittleEndianIndex,floatData;
    TPCANMsg msg, temp;
    temp.MSGTYPE = MSGTYPE_STANDARD;
    // Setting float value
    floatData=128;

    // COB and ID, length of bytes flags and timestamp
    temp.ID=COB_TPDO2+id;
    temp.LEN=4;

    // Two command character in ASCII
    temp.DATA[0]=param[0];
    temp.DATA[1]=param[1];

    // Taking care of endians for 16 bit index number
    LittleEndianIndex = bswap_16(index);
    temp.DATA[2] = (int)((LittleEndianIndex >> 8) & 0XFF);
    temp.DATA[3] = (int)((LittleEndianIndex & 0XFF))+floatData;

    // Check if the requested message is in the message buffer

    int fndCode = msgBuf.LookUp(temp, pValue);

    return fndCode;	 // Requested message found? 0=NO, 1=YES
}
