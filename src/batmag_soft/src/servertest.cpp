#include "ros/ros.h"
#include "batmag_soft/InitParameters.h"

bool add(batmag_soft::InitParameters::Request  &req,
         batmag_soft::InitParameters::Response &res)
{
  res.value={50,-20,-65,55,300};
res.max={200,0,-10,100,1000};
res.min={0,-50,-140,20,250};
res.names={"P","I","D","randomName"};
  ROS_INFO("Inserted values");
  return true;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "init_parameters_server");
  ros::NodeHandle n;

  ros::ServiceServer service = n.advertiseService("init_parameters", add);
  ROS_INFO("Ready to setInfo.");
  ros::spin();

  return 0;
}
