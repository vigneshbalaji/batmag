#include "FrameGrabber.h"
#include <mutex>

#include <unistd.h>


void pressedStart(const batmag_soft::StartStop::ConstPtr& msg, bool & startPressed, bool & stopPressed)
{
    startPressed=msg->pressedStart;
    stopPressed=msg->pressedStop;
    if(startPressed)
        ROS_INFO_STREAM("Start was pressed");
    if(stopPressed)
        ROS_INFO_STREAM("Stop was pressed");
}


void setUSroi(const batmag_soft::USroi::ConstPtr& msg, int & width, int & height, int & x, int & y, FrameGrabber & fg, std::mutex & mutex)
{
    width=msg->width;
    height=msg->height;
    x=msg->topleftx;
    y=msg->toplefty;

    ROS_INFO_STREAM("US cb done");

    mutex.lock();
    fg.cmdline.cropRect.x = x;
    fg.cmdline.cropRect.y = y;
    fg.cmdline.cropRect.height = height;
    fg.cmdline.cropRect.width = width;


    ROS_INFO_STREAM("updated framegrabber");
    mutex.unlock();

}

int main(int argc, char** argv)
{
    //Create rosnode
    ros::init(argc, argv, "ultrasound_node");
    ros::NodeHandle n;


    ROS_INFO_STREAM("The ultrasound was created");
    double framerate=30;//Maximum framerate=60
    ros::Rate captureRate(framerate);

    std::mutex mFgMutex;
    //Recorder Stuff

    bool recordOn=false, prevRec=false;


    //Subscribe to Start/Stop messages
    bool startPressed=false, stopPressed=false; //startPressed is not used but a while !pressed loop can be used to show images only after it has been pressed
    ros::Subscriber startStopSub = n.subscribe<batmag_soft::StartStop>("gui_start_stop", 1, std::bind(pressedStart, std::placeholders::_1, std::ref(startPressed), std::ref(stopPressed)));

    //Subscribe to US ROI settings

    int us_width=570 ,us_height=537, tl_x=413, tl_y=56;    //  default  ROI cropping values (tl= topleft)

    FrameGrabber mFrameGrabber(tl_x, tl_y, us_width, us_height);

    ros::Subscriber roiSub= n.subscribe<batmag_soft::USroi>("us_roi", 1, std::bind(setUSroi, std::placeholders::_1, std::ref(us_width), std::ref(us_height), std::ref(tl_x), std::ref(tl_y), std::ref(mFrameGrabber), std::ref(mFgMutex)));

    //Image transporter
    image_transport::ImageTransport it(n);

    // Publish output video feed
    image_transport::Publisher image_pub_US = it.advertise("us_image", 1);

    //    Creating images for cameras

    cv::Mat usImage;
    cv_bridge::CvImage out_msg;

    ROS_INFO_STREAM("Starting to capture frames");
    while(ros::ok() && !stopPressed)
        //    for ( int j = 0; j < k_numImages; j++ )
    {
        mFgMutex.lock();
        mFrameGrabber.getFrame(usImage);
        mFgMutex.unlock();

        //            ROS_DEBUG_STREAM("Filling message");
        out_msg.encoding = sensor_msgs::image_encodings::BGR8; // Or whatever
        out_msg.image    = usImage; // Your cv::Mat
        image_pub_US.publish(out_msg.toImageMsg());
        ros::spinOnce();
        captureRate.sleep();
    }


    ROS_DEBUG_STREAM("Exiting");


    return 0;
}
