#include "FrameGrabber.h"

FrameGrabber::FrameGrabber(int x, int y ,int height, int width)
{
    FrmGrab_Init();

    fg = FrmGrabLocal_Open();

    int count = FrmGrabLocal_Count();

    ROS_INFO_STREAM("Initiated a new Frame Ggrabber with x: "<<x<<" y: "<<y<<" Height: "<<height<<" and width: "<<width);

    FrmGrab_Start(fg);

    cmdline.cropRect.x = x;
    cmdline.cropRect.y = y;
    cmdline.cropRect.height = height;
    cmdline.cropRect.width = width;

}

FrameGrabber::~FrameGrabber()
{
     FrmGrab_Deinit();
}

void FrameGrabber::getFrame(cv::Mat& frameMatrix)
{

     chrono::high_resolution_clock::time_point tStart = chrono::high_resolution_clock::now();
     currentFrame = FrmGrab_Frame(fg, V2U_GRABFRAME_FORMAT_BGR24, &cmdline.cropRect);
     chrono::high_resolution_clock::time_point tNow = chrono::high_resolution_clock::now();

     if(currentFrame)
     {
     int h = currentFrame->crop.height;
     int w = currentFrame->crop.width;

     cv::Mat temporaryFrame(h,w,CV_8UC3);

     temporaryFrame.data = static_cast<uchar*>(currentFrame->pixbuf);
     temporaryFrame.copyTo(frameMatrix);
     }

     FrmGrab_Release(fg,currentFrame);
     chrono::milliseconds ms_timestamp = chrono::duration_cast<chrono::milliseconds>(tNow-tStart);

}
