#pragma once

#include <iostream>
#include <chrono>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "libs/epiphan/frmgrab/include/frmgrab.h"

#include "ros/duration.h"
#include "ros/spinner.h"
#include "ros/ros.h"


#include <boost/function.hpp>
#include <boost/bind.hpp>
#include <string>
#include <stdlib.h>

#include "batmag_soft/StartStop.h"
#include "batmag_soft/CameraClick.h"
#include "batmag_soft/USroi.h"


#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>


typedef struct _v2u_cmdline_context {
    V2URect cropRect;           /* Crop rectangle */
    V2U_UINT32 captureFlags;    /* Capture format and flags */
    V2U_UINT32 frameCount;      /* Frame count */
    V2U_BOOL noStreaming;       /* Streaming flag */
    V2U_BOOL actionPerformed;   /* Something meaningful has been done */
} v2u_cmdline_context;

using namespace std;

class FrameGrabber {

    FrmGrabber* fg;

    V2U_GrabFrame2* currentFrame;


public:

    FrameGrabber(int x, int y ,int height, int width);
    ~FrameGrabber();

    v2u_cmdline_context cmdline;

    void getFrame(cv::Mat& frameMatrix);

};
