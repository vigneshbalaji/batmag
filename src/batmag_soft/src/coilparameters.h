#pragma once

#include <Eigen/Dense>

namespace coil{
//Normalized Coil Position of the coils wrt the center of the workspace (has to be multiplied by the distance for the actual value)
//TODO rotate these to fit a frame fixed on the camera

//Alpha is equal to sin of the angle between the coils *sin 45 for the camera rotation (normalized) and is used to represent the unit vector of the coil direction
const double alpha=0.5774;

const Eigen::Vector3d kNormalizedPos[9]={
    {alpha, alpha, alpha},//1
    {alpha, -alpha, alpha},//2
    {-alpha,-alpha, alpha},//3
    {-alpha, alpha, alpha},//4
    {alpha,  alpha, -alpha},//5
    {alpha, -alpha, -alpha},//6
    {-alpha,-alpha, -alpha},//7
    {-alpha, alpha, -alpha},//8
    {0, 0, -1} //9
};

//Number of notches after first one(5mm each)

const int kNotches[9]={
     7,//1
    7,//2
    7,//3
    7,//4
    7,//5
    7,//6
    7,//7
    7,//8
    7//9
};

const double kBaseDist=25; //mm

//Position of the coils wrt the center of the workspace
const Eigen::Vector3d kPos[9]={
    kNormalizedPos[0]*(kBaseDist+5*kNotches[0]),//1
    kNormalizedPos[1]*(kBaseDist+5*kNotches[1]),//2
    kNormalizedPos[2]*(kBaseDist+5*kNotches[2]),//3
    kNormalizedPos[3]*(kBaseDist+5*kNotches[3]),//4
    kNormalizedPos[4]*(kBaseDist+5*kNotches[4]),//5
    kNormalizedPos[5]*(kBaseDist+5*kNotches[5]),//6
    kNormalizedPos[6]*(kBaseDist+5*kNotches[6]),//7
    kNormalizedPos[7]*(kBaseDist+5*kNotches[7]),//8
    kNormalizedPos[8]*(kBaseDist+5*kNotches[8]) //9
};
}
