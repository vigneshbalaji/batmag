#include "typeHeader.h"

agentType setUseAgent(string selectedAgent)
{
    if(selectedAgent=="Microparticle") return microparticle;

    if(selectedAgent=="Microjet") return microjet;

    if(selectedAgent=="Hydrogel Gripper") return hydroGripper;

    if(selectedAgent=="Metallic Gripper") return metallicGripper;

    if(selectedAgent=="1mm Spherical PM") return sphericalPM1mm;

    if(selectedAgent=="CircClip") return circClip;

    if(selectedAgent=="Microsphere") return microsphere;


}
