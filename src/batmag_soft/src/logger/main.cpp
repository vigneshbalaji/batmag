#include <functional>

#include "ros/duration.h"
#include "ros/spinner.h"
#include "ros/ros.h"
#include "batmag_soft/GetInitParameters.h"
#include "batmag_soft/SetInitParameters.h"
#include "batmag_soft/StartStop.h"
#include "batmag_soft/CtrlOutput.h"
#include "batmag_soft/Reference.h"
#include "batmag_soft/State.h"
#include "batmag_soft/GUIpreferences.h"
#include "batmag_soft/MapOutput.h"
#include "std_msgs/String.h"
#include "../typeHeader.h"

#include "Eigen/Core"

#include <sys/stat.h>

#include "time.h"
#include <boost/function.hpp>
#include <boost/bind.hpp>
#include <fstream>
#include <iostream>

using boost::shared_ptr;

const double kLoggerFrequency=30;//Hz
const unsigned int kDOF=6;

void pressedStart(const batmag_soft::StartStop::ConstPtr& msg, bool & startPressed, bool & stopPressed)
{
    startPressed=msg->pressedStart;
    stopPressed=msg->pressedStop;

}

void guiPrefCb(const batmag_soft::GUIpreferences::ConstPtr& msg, bool & ctrlEn)
{
    ctrlEn=msg->ctrlEn;
}


void refCb(const batmag_soft::Reference::ConstPtr& msg, Eigen::Matrix<double, 1,9> & ref)
{
    for(int i=0; i<kDOF; ++i)
        ref[i]=msg->ref[i];

}

void  stateCb(const batmag_soft::State::ConstPtr& msg, Eigen::Matrix<double, 1,9> & state)
{
    for(int i=0; i<kDOF; ++i)
        state[i]=msg->state[i];

}

void ctrlCb(const batmag_soft::CtrlOutput::ConstPtr &msg, Eigen::Matrix<double, 1,9> & ctrl)
{
    for(int i=0; i<kDOF; ++i)
        ctrl[i]=msg->ctrlout[i];

}

void mapoutCb(const batmag_soft::MapOutput::ConstPtr & msg, Eigen::Matrix<double, 1,10> & currents)
{


    for(int i=0; i<msg->size; ++i)
        currents[i]=msg->mapout[i];
}

int main(int argc, char *argv[])
{
    ROS_INFO_STREAM("The logging node has started");
    //Create the ros node and the handle
    ros::init(argc, argv, "logger_node");
    ros::NodeHandle n;


    //Frequency of this node
    ros::Rate spinRate(kLoggerFrequency);

    bool init=false;


    time_t now=time(0);

    std::ofstream myFile;

    std::string recDir= "/home/batmag/BatMag_Recordings/Logs/";


    //Wait for the node to be online
    while(!ros::ok())
        usleep(500);

    Eigen::Matrix<double, 1,9> ref, state, ctrl;
    Eigen::Matrix<double, 1,10> map;

    //Defining callbacks
    //Subscribe to Start/Stop messages
    bool startPressed=false, stopPressed=false;
    ros::Subscriber startStopSub = n.subscribe<batmag_soft::StartStop>("gui_start_stop", 1, std::bind(pressedStart, std::placeholders::_1, std::ref(startPressed), std::ref(stopPressed)));
    //Subscribe to GUI preferences
    bool ctrlEnable=false;
    ros::Subscriber guiPref= n.subscribe<batmag_soft::GUIpreferences>("gui_preferences", 1, std::bind(guiPrefCb, std::placeholders::_1, std::ref(ctrlEnable)));

    while(!startPressed && ros::ok() && !stopPressed)
    {
        ros::spinOnce();
        spinRate.sleep();
    }

    if(!stopPressed && ros::ok())
    {
        //Subscribe to messages on reference and state
        ros::Subscriber referenceSub = n.subscribe<batmag_soft::Reference>("reference", 1, std::bind(refCb, std::placeholders::_1, std::ref(ref)));
        ros::Subscriber stateSub = n.subscribe<batmag_soft::State>("state", 1, std::bind(stateCb, std::placeholders::_1, std::ref(state)));
        ros::Subscriber ctrlSub = n.subscribe<batmag_soft::CtrlOutput>("ctrl_output", 1, std::bind(ctrlCb, std::placeholders::_1, std::ref(ctrl)));
        ros::Subscriber mapOutSub=n.subscribe<batmag_soft::MapOutput>("map_output",1,  std::bind(mapoutCb, std::placeholders::_1, std::ref(map)));


        while(!stopPressed && ros::ok()){
            //            Only record if ctrl is enabled
            //Update values
            ros::spinOnce();

            struct timeval stop, start;

            if(ctrlEnable)
            {

                if(!init)
                {

                    ROS_INFO_STREAM("Creating Log file") ;
                    gettimeofday(&start, NULL);

                    now=time(0);

                    tm *ltm = localtime(&now);


                    ROS_DEBUG_STREAM("Localtime is "<< std::to_string(ltm->tm_year)+"M"+std::to_string(ltm->tm_mon)+"D"+std::to_string(ltm->tm_mday)+"."+std::to_string(ltm->tm_hour)+"."+std::to_string(ltm->tm_min)+"."+std::to_string(ltm->tm_sec));



                    std::string fileDir=recDir+"Y"+std::to_string(ltm->tm_year)+"M"+std::to_string(ltm->tm_mon)+"D"+std::to_string(ltm->tm_mday);
                    int dir_err = mkdir(fileDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
                    if (dir_err ==-1)
                    {
                        ROS_ERROR_STREAM("Error creating directory!");
                    }

                    std::string filename=fileDir+"/Log-Y"+std::to_string(ltm->tm_year)+"M"+std::to_string(ltm->tm_mon)+"D"+std::to_string(ltm->tm_mday)+"."+std::to_string(ltm->tm_hour)+"."+std::to_string(ltm->tm_min)+"."+std::to_string(ltm->tm_sec)+".txt";
                    myFile.open(filename);
                    init=true;
                    ROS_INFO_STREAM("Saving data to "<< filename);
                }


                gettimeofday(&stop, NULL);

                myFile<<"Time [s] | [ms]: "<<(stop.tv_sec - start.tv_sec)<< " | " <<(stop.tv_usec - start.tv_usec)%1000000;

                myFile<<" State: "<<state;

                myFile<<" Reference: "<<ref;

                myFile<<" Currents: "<<map;

                myFile<<" Control Action: "<<ctrl<<" \n";


            }
            else
            {
                init=false;
                if(myFile.is_open())
                    myFile.close();
            }

            spinRate.sleep();
        }

        if(myFile.is_open())
            myFile.close();


    }


}

//ultrasound holder

