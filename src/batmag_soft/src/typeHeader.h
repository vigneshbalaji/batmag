#pragma once

#include <string>

using std::string;

enum ctrlType{
    PID3Dtype,
    PPCtype,
    Rotatingtype,
    PID5Dtype,
    Errortype
};

enum agentType{
    microparticle,
    microjet,
    hydroGripper,
    metallicGripper,
    sphericalPM1mm,
    circClip,
    microsphere
};

agentType setUseAgent(string selectedAgent);
