#include "fieldonlymap.h"

FieldOnlyMap::FieldOnlyMap(agentType selectedAgent)
{
    ROS_INFO_STREAM("Started field only Map");

}

FieldOnlyMap::~FieldOnlyMap()
{
    //nothing to do here
    delete this;
}

Eigen::Matrix<double,Eigen::Dynamic,1> FieldOnlyMap::evaluate()
{
    MagneticField::curToFieldD1(posAgent1*1e-3, curToField);
    dynCurToField=curToField;

    fieldToCurMap=pseudoInverse(dynCurToField);

    curOut=fieldToCurMap*ctrlOut.head(3);

    saturateAndPolarizeCurrents(curOut);
    return curOut;
}
