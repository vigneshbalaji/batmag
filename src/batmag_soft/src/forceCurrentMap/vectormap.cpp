#include "vectormap.h"

VectorMap::VectorMap(agentType selectedAgent)
{
    currentToForceMap.resize(3, 9);
    curOut.resize(9,1);

    //TODO a function to detect paramagnetic agents
    isParamagnetic=false;


    /*The field decreases with the power of 3 of the distance => therefore the
    gradient decreases with the power of 4 of distance.
    If the object is paramagnetic its magnetic dipole moment is also proportional
    to the field.
    Hence the weighted distance is ^4 for magnets and ^7 for paramagnetic objects.*/

    if(isParamagnetic)
        distancePower=7;
    else
        distancePower=4;

    ROS_DEBUG_STREAM("Vector Map Constructed");
}

VectorMap::~VectorMap()
{
    //Nothing to do here
}

//void VectorMap::positionUpdate(const batmag_soft::State &msg)
//{
//    ROS_DEBUG_STREAM("About top update position");
//    //-------//
//    //WARNING//
//    //As of now this only works with 3D controllers (three states max)

//    if(msg.state.size()>2)
//        ROS_DEBUG_STREAM("Message size: "<< msg.state.size());
//    for(int i=0; i<3; i++)
//    {

//        ROS_DEBUG_STREAM("State "<<i<<" is: "<<msg.state[i]);
//        position[i]=msg.state[i];
//    }

//    ROS_DEBUG_STREAM("Position updated");
//Z}


Eigen::Matrix<double, Eigen::Dynamic, 1> VectorMap::evaluate(){

    ROS_DEBUG_STREAM("Computing distance");
    for(int i=0; i<9; ++i)
    {
        //        mmPosition=
        dist[i]=(coil::kPos[i]-posAgent1);//Distance in mM
    }

    ROS_DEBUG_STREAM("Distance computation ok");
    //    //Current to Force Map
    //        currentToForceMap<<dist[0]/pow(dist[0].norm(),distancePower),
    //                dist[1]/pow(dist[1].norm(),distancePower),
    //                dist[2]/pow(dist[2].norm(),distancePower),
    //                dist[3]/pow(dist[3].norm(),distancePower),
    //                dist[4]/pow(dist[4].norm(),distancePower),
    //                dist[5]/pow(dist[5].norm(),distancePower),
    //                dist[6]/pow(dist[6].norm(),distancePower),
    //                dist[7]/pow(dist[7].norm(),distancePower),
    //                dist[8]/pow(dist[8].norm(),distancePower);
    //                Eigen::Vector3d(0,0,0); //TODO add line for PermanentMagnet

    //Current to Force Map with normalized measures
    currentToForceMap<<dist[0]/dist[0].norm(),
            dist[1]/dist[1].norm(),
            dist[2]/dist[2].norm(),
            dist[3]/dist[3].norm(),
            dist[4]/dist[4].norm(),
            dist[5]/dist[5].norm(),
            dist[6]/dist[6].norm(),
            dist[7]/dist[7].norm(),
            dist[8]/dist[8].norm();
    //            Eigen::Vector3d(0,0,0); //TODO add line for PermanentMagnet


    ROS_DEBUG_STREAM("Current to force: "<<currentToForceMap);

    forceCurMap= pseudoInverse(currentToForceMap);

    //    for(int i=0; i<forceCurMap.rows(); ++i)
    //    {
    //        forceCurMap(i,0)*=pow(dist[i].norm(),distancePower);
    //        forceCurMap(i,1)*=pow(dist[i].norm(),distancePower);
    //        forceCurMap(i,2)*=pow(dist[i].norm(),distancePower);
    //    }

    ROS_DEBUG_STREAM("Force to current: "<<forceCurMap << " \n control output: "<< ctrlOut);

    curOut=/*forceScalingFactor*/forceCurMap*ctrlOut; //


    ROS_INFO_STREAM("The projection is: \n"<< curOut);

    std::ptrdiff_t i;
    double maxCur = curOut.maxCoeff(&i);

    curOut[i]=maxCur/curOut.norm()*ctrlOut.norm();

    for(int j=0; j<9; ++j)
    {
        if(i!=j)
        curOut[j]=0;
    }


    ROS_DEBUG_STREAM(ctrlOut);

    ROS_INFO_STREAM("Current output is: \n"<< curOut);

    ROS_DEBUG_STREAM("Nominal force: " << ctrlOut <<"Force with this current:"<< currentToForceMap*curOut/forceScalingFactor);

    saturateAndPolarizeCurrents(curOut);

    //    curOut[9]=1;//TODO ACTUALLY ADD EQUATION FOR PERM MAG


    return curOut;



}
