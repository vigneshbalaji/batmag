#include "magneticdipolemodel.h"

MagneticDipoleModel::MagneticDipoleModel(double samplingTime, double inputTau)
{
    ts=samplingTime;

    for(int i=0; i<2; i++)
    {
        stateX[i]=0;
        stateY[i]=0;
        stateZ[i]=0;
    }
    tau=inputTau;

    recompMatrices();

    ROS_INFO_STREAM("Constructor done, ts is: "<<ts<<" tau: "<<inputTau);


}

void MagneticDipoleModel::update(const Eigen::Vector3d finalRef, Eigen::Vector3d &dipoleMoment)
{
    fRef=finalRef;


    //    Update X
    prevStateX=stateX;
    stateX=Ad*prevStateX+Bd*fRef[0];

    //    Y
    prevStateY=stateY;
    stateY=Ad*prevStateY+Bd*fRef[1];

    //    and Z
    prevStateZ=stateZ;
    stateZ=Ad*prevStateZ+Bd*fRef[2];


    ROS_DEBUG_STREAM("Final Reference: "<<fRef<< "X state: "<<prevStateX<<"Y state: "<<stateY<<"Z state: "<<stateZ);
    mdm[0]=stateX[0];     mdm[1]=stateY[0];     mdm[2]=stateZ[0];

    dipoleMoment=mdm;

}


void MagneticDipoleModel::resetInitialState(Eigen::Vector3d newDipole)
{
    stateX[0]=newDipole[0];
    stateY[0]=newDipole[1];
    stateZ[0]=newDipole[2];
    stateX[1]=0;
    stateY[1]=0;
    stateZ[1]=0;
}


void MagneticDipoleModel::recompMatrices()
{
    A<<     0, 1,
            -1/pow(tau,2), -2/tau;

    B<<     0,
            1/pow(tau,2);


    ROS_DEBUG_STREAM("A is: \n"<<A<<" While B is: \n"<<B);
    exactDiscretization();

}

void MagneticDipoleModel::setTimeConstant(double timeConst)
{

    tau=timeConst;
    recompMatrices();
}


void MagneticDipoleModel::exactDiscretization()
{
    // compute the exact discretization of the A matrix
    // multiply with the sampling time
    Ad = (A*ts).exp();

    // compute the exact discretization of the B matrix
    // define the identity matrix
    int A_size = A.rows();
    Eigen::MatrixXd Idn(A_size,A_size);
    Idn = Eigen::Matrix2d::Identity();

    // compute the discretized version
    Bd = A.inverse()*(Ad - Idn)*B;

    ROS_INFO_STREAM("Ad is: "<<Ad<<" While Bd is: "<<Bd);
}
