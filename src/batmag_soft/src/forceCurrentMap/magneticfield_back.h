#pragma once


#include <Eigen/StdVector>
#include <Eigen/Dense>


#include <math.h>
#include <stddef.h>
#include <stdlib.h>

#include "Matlab/rt_nonfinite.h"
#include "Matlab/rtwtypes.h"
#include "Matlab/rtGetInf.h"
#include "Matlab/rtGetNaN.h"

#include "ros/ros.h"

namespace MagneticField
{
//    MagneticField();


//void bNormGradmm(std::vector<double> pos, double pmPos, const  double currents[], Eigen::Vector3d & gradBmod);

//Gradient of Norm of B
void bNormGradmm(Eigen::Vector3d pos, double pmPos, const Eigen::Matrix<double, 9, 1> & currents,  Eigen::Vector3d &gradBmod);

//Gradient of Norm of B (std::vector)
inline void bNormGradmm(std::vector<double> pos, double pmPos, const double currents[],  std::vector<double> & gradBmod)
{
    Eigen::Vector3d epos={pos[0], pos[1], pos[2]}, grad= {gradBmod[0], gradBmod[1], gradBmod[2]};
    Eigen::Matrix<double, 9,1> ecurrents;
    for (int k=0; k<0; k++)
        ecurrents[k]= currents[k];
    bNormGradmm(epos,pmPos,ecurrents,grad);
    for(int i=0; i<3; ++i)
        gradBmod[i]=grad[i];
}


inline Eigen::Vector3d bNormGradmm(Eigen::Vector3d pos, double pmPos, const Eigen::Matrix<double, 9, 1> & currents)
{
    ROS_INFO_STREAM("Starting to compute the gradient");
    Eigen::Vector3d output;
    bNormGradmm(pos,pmPos,currents, output);

    ROS_INFO_STREAM("About to return");
    return output;

}

//Magnetic field due to currents at 2nd notch (D1)
void BtildaFuncD1(double x, double y, double z, Eigen::Matrix<float, 3, 9> & B_tilda);

//Magnetic field due to perm mag
void BPermMagFunc(double x, double y, double z, Eigen::Vector3d & BPermMag);

//Gradient of Bx (currents only)
void BxHatFuncD1(double x, double y, double z, Eigen::Matrix<double, 3,9> & Bx_hat);

//Gradient of By (currents only)
void ByHatFuncD1(double x, double y, double z, Eigen::Matrix<double, 3,9> & By_hat);

//Gradient of Bz (currents only)
void BzHatFuncD1(double x, double y, double z,Eigen::Matrix<double, 3,9> & Bz_hat);


//Gradient of Permanent Magnet
void GradBPermMagFunc(Eigen::Vector3d pos, Eigen::Vector3d & gradBxPM, Eigen::Vector3d & gradByPM, Eigen::Vector3d & gradBzPM);


//Current derivatives of Grad Norm B
void bNormGradCurDer(std::vector<double>, double pmPos,const  double currents[], int curNumFromZ, std::vector<double> &thisCurrentDer);


//Acutal derivatives

void partialDerI1Func(std::vector<double> pos, double pmPos, const double currents[], std::vector<double> & partialDer);

void partialDerI2Func(std::vector<double> pos, double pmPos, const double currents[], std::vector<double> & partialDer);

void partialDerI3Func(std::vector<double> pos, double pmPos, const double currents[], std::vector<double> & partialDer);

void partialDerI4Func(std::vector<double> pos, double pmPos, const double currents[], std::vector<double> & partialDer);

void partialDerI5Func(std::vector<double> pos, double pmPos, const double currents[], std::vector<double> & partialDer);

void partialDerI6Func(std::vector<double> pos, double pmPos, const double currents[], std::vector<double> & partialDer);

void partialDerI7Func(std::vector<double> pos, double pmPos, const double currents[], std::vector<double> & partialDer);

void partialDerI8Func(std::vector<double> pos, double pmPos, const double currents[], std::vector<double> & partialDer);

void partialDerI9Func(std::vector<double> pos, double pmPos, const double currents[], std::vector<double> & partialDer);

//    //Gradient of Norm of B, y component
//    double bNormGrady(std::vector<double> pos, double pmPos,const  std::vector<double> & currents);

//    //Current derivatives of Grad Norm B, y component
//    double bNormGradyCurDer(std::vector<double>, double pmPos, const std::vector<double> & currents, int curNumFromZ);

//    //Gradient of Norm of B, z component
//    double bNormGradz(std::vector<double> pos, double pmPos,const  std::vector<double> & currents);

//    //Current derivatives of Grad Norm B, y component
//    double bNormGradzCurDer(std::vector<double>, double pmPos, const std::vector<double> & currents, int curNumFromZ);

const double kCoilFieldScalingFactor=3.6673, kPermMagFieldScalingFactor=0.91;

};
