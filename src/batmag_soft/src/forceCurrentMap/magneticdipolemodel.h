#pragma once

#include <Eigen/Dense>
#include <eigen3/unsupported/Eigen/MatrixFunctions>
#include <math.h>


#include "ros/ros.h"

class MagneticDipoleModel

{
public:
    MagneticDipoleModel(double samplingTime=0.05, double inputTau=20);

    //Used to change the current/initial state
    void resetInitialState(Eigen::Vector3d newDipole);

    void update(const Eigen::Vector3d finalRef, Eigen::Vector3d & dipoleMoment);

    inline void setSamplingTime(double sampTime){ts=sampTime; exactDiscretization();}

    //Used to change the time constant tau
    void setTimeConstant(double timeConst);


private:

    //Recomputes the matrices if tau is changed
    void recompMatrices();


    void exactDiscretization();

    Eigen::Vector2d stateX, prevStateX, stateY, prevStateY, stateZ, prevStateZ;
    double tau;// tau AKA time constant

    Eigen::Vector3d fRef, mdm;

    //State space Matrices
    Eigen::Matrix<double, 2,2> A, Ad, Idn;
    Eigen::Matrix<double, 2,1> B, Bd;
    Eigen::Matrix<double, 1,2> C;

    //sampling time
    double ts;
};
