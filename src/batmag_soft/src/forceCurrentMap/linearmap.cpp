#include "linearmap.h"

LinearMap::LinearMap(agentType selectedAgent)
{

    switch(selectedAgent)
    {
    case microparticle:
    case microjet:
    case hydroGripper:
    case metallicGripper:
    default:
        ROS_WARN_STREAM("Using default agent for linear map");
        currentToForceMap.resize(kStateSize,9);
        ROS_INFO_STREAM("Maps resized");
        break;
    }
    mdmCurrent<< 0,0, 0;

    Iprev<< 0.1,0.1,0.1,0.1,0,0,0,0,0;

    ROS_INFO_STREAM("Agent for Linear Map initialized \n Radius is: "<<radius<<"\n Volume: "<<volume<<"\n Kmag: "<< kmag);

    const double kSamplingTime=(double)1/60, kTau=0.2;

    ROS_INFO_STREAM("LinearMap ts: "<<  kSamplingTime<< " tau: "<<kTau);


    mdmModel=std::make_shared<MagneticDipoleModel>(kSamplingTime, kTau);

    mdmBasic<<0,0, bR;

    mdmModel->resetInitialState(mdmBasic);

    mServer=n.advertiseService("force_current_settings", &LinearMap::settingsCb, this);
}

LinearMap::~LinearMap()
{
    //Destroy me!
}

bool LinearMap::settingsCb(batmag_soft::ForceCurrentSettings::Request &req,
                           batmag_soft::ForceCurrentSettings::Response &res)
{
    if(req.isSetRequest)
    {
        bR=req.br[0];
        muR=req.mur[0];
        radius=req.radius[0];

        ROS_INFO_STREAM("Parameters changed to\n Br: "<<bR<<"\n mur: "<<muR<<"\n radius: "<<radius);

        volume=(double)4/3*M_PI*pow(radius,3);
        chi=muR/(4*M_PI*10e-7)-1;
        kmag=chi*volume;
    }
    else
    {
        ROS_INFO_STREAM("Processing Request");
        res.br.resize(1);
        res.mur.resize(1);
        res.radius.resize(1);


        res.br[0]=bR;
        res.mur[0]=muR;
        res.radius[0]=radius;
    }
    return true;
}

Eigen::Matrix<double, Eigen::Dynamic, 1> LinearMap::evaluate()
{
    ROS_DEBUG_STREAM("Copying positions. kStateSize is: "<<kStateSize);

    for(int i=0; i<kStateSize; ++i)
    {
        pos1[i]=state[i];
        if(kStateSize>3 && i<6)
            pos2[i-3]=state[i];
    }

    ROS_DEBUG_STREAM("Computed position");

    if(pos1.maxCoeff()<50 && pos1.minCoeff()>-50)
    {

        //These functions are in meters => conversion mm2m
        MagneticField::BgradXD1(pos1/1000, hatBx);
        MagneticField::BgradYD1(pos1/1000, hatBy);
        MagneticField::BgradZD1(pos1/1000, hatBz);
        MagneticField::curToFieldD1(pos1/1000, bTilda);

        ROS_DEBUG_STREAM("Computed gradients");

        if(Iprev.squaredNorm()!=0)
        {
            bField=(bTilda*Iprev).transpose();
            mdmK=bField*kmag;
            mdmBasic=bField*bR/bField.norm();
        }

        mdmCurrent=mdmK+mdmBasic;

        ROS_DEBUG_STREAM("Computed mdm("<<mdmCurrent<<"). It has kmag component: "<<mdmK.norm()<<" and basic component: "<<mdmBasic.norm());

        mdmModel->update(mdmCurrent, mdmFinal);


        ROS_DEBUG_STREAM("Computed mdm: "<<mdmCurrent<<" Norm: "<<mdmCurrent.norm()<<" Model: "<<mdmFinal<<" Norm: " <<mdmFinal.norm());

        currentToForceMap.row(0)=mdmFinal.transpose()*hatBx;
        currentToForceMap.row(1)=mdmFinal.transpose()*hatBy;
        currentToForceMap.row(2)=mdmFinal.transpose()*hatBz;


        ROS_DEBUG_STREAM("Computed F2C map:"<< currentToForceMap<< "\n epsilon is: "<<std::numeric_limits<double>::epsilon());
        ROS_DEBUG_STREAM("Matrix Size: "<< currentToForceMap.rows() <<", "<<currentToForceMap.cols()<<"Inverted matrix"<< pseudoInverse(currentToForceMap));

        ROS_ERROR_STREAM_COND(std::isnan(mdmCurrent[0]), "ERROR MDM is NAN,\n Computed F2C map:"<< currentToForceMap<<"\n hatBx: \n"<< hatBx<<"\n hatBy: \n"<< hatBy<<"\n hatBz: \n"<< hatBz<<"\n Prev Current is: "<< Iprev);

        forceCurMap=pseudoInverse(currentToForceMap);

        ROS_DEBUG_STREAM("Force to current map set"<<forceCurMap);

        //    //CtrlOut is updated because of the Cb in main.cpp
        curOut=forceCurMap*ctrlOut;

        saturateAndPolarizeCurrents(curOut);


        Iprev=curOut;
        ROS_DEBUG_STREAM("Previous current updated");
    }

    else
        ROS_ERROR_STREAM("Position out of bounds!");

    return curOut;

}
