#pragma once

#include "ros/ros.h"
#include "fcmap.h"
#include "magneticfield.h"
#include <Eigen/Dense>
#include <math.h>


#include "batmag_soft/ForceCurrentSettings.h"

class FieldOnlyMap: public FCMap
{
public:
    FieldOnlyMap(agentType selectedAgent);


    virtual ~FieldOnlyMap();

    //All maps must have an evaluate to be used as Cb for control messages
    virtual     Eigen::Matrix<double,Eigen::Dynamic,1>  evaluate();

protected:

    ros::NodeHandle n;


    Eigen::Matrix<double,9,1> Iprev;

    Eigen::MatrixXd fieldToCurMap, dynCurToField;
    Eigen::Matrix<double,3,9>  curToField;


};
