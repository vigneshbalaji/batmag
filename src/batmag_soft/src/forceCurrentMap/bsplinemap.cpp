#include "bsplinemap.h"

BSplineMap::BSplineMap(agentType selectedAgent)
{
    switch(selectedAgent)
    {
    case microparticle:
    case microjet:
    case hydroGripper:
    case metallicGripper:
    default:
        ROS_WARN_STREAM("Using default agent for B-Spline map");
        currentToForceMap.resize(8,9);
        ROS_DEBUG_STREAM("Maps resized");
        break;
    }

    for(int i=1; i<10; ++i)
    {
        filename=directory+std::to_string(i)+".txt";
        strcpy(str, filename.c_str());
        if(!MagneticField::readBC(str,m_BC[i-1]))
            ROS_ERROR_STREAM("Couldn't read Bspline control point. Issue with: "
                             << filename);
        //        else
        //            ROS_INFO_STREAM("BC"<<i<<" is: \n"<<BC[i-1]);
    }
}


BSplineMap::~BSplineMap()
{
    //Nothing to do here
}


void BSplineMap::computeCFmap()
{
    Eigen::Matrix<double, 1,3> cB, cBdx, cBdy, cBdz;

    ROS_DEBUG_STREAM("Basis Func");

    //Prevent state from being >7
    if(fabs(state[0])<7)
        MagneticField::N7to7(state[0], m_N);
    else
        MagneticField::N7to7( copysign(7,state[0]), m_N);

    //Prevent state from being >7
    if(fabs(state[1])<7)
        MagneticField::N7to7(state[1], m_M);
    else
        MagneticField::N7to7( copysign(7,state[1]), m_M);

    //Prevent state from being >7
    if(fabs(state[2])<7)
        MagneticField::N7to7(state[2], m_P);
    else
        MagneticField::N7to7( copysign(7,state[2]), m_P);

    ROS_DEBUG_STREAM("Basis Dev");
    //Prevent state from being >7
    if(fabs(state[1])<7)
        MagneticField::diffN7to7(state[0], m_Ndev);
    else
        MagneticField::diffN7to7(copysign(7,state[0]), m_Ndev);

    //Prevent state from being >7
    if(fabs(state[1])<7)
        MagneticField::diffN7to7(state[1], m_Mdev);
    else
        MagneticField::diffN7to7(copysign(7,state[1]), m_Mdev);

    //Prevent state from being >7
    if(fabs(state[2])<7)
        MagneticField::diffN7to7(state[2], m_Pdev);
    else
        MagneticField::diffN7to7(copysign(7,state[2]), m_Pdev);


    ROS_DEBUG_STREAM("Build matrices");
    for(int w=0; w<9; ++w)
    {
        cB.setZero(1,3);
        cBdx.setZero(1,3);
        cBdy.setZero(1,3);
        cBdz.setZero(1,3);

        ROS_DEBUG_STREAM("Cleared");
        for(int k=0; k<10;++k)
        {
            for(int j=0; j<10;++j)
            {
                for(int i=0; i<10;++i)
                {
                    cB+=m_BC[w].row(i+j*10+k*100)*m_N[i]*m_M[j]*m_P[k];
                    cBdx+=m_BC[w].row(i+j*10+k*100)*m_Ndev[i]*m_M[j]*m_P[k];
                    cBdy+=m_BC[w].row(i+j*10+k*100)*m_N[i]*m_Mdev[j]*m_P[k];
                    cBdz+=m_BC[w].row(i+j*10+k*100)*m_N[i]*m_M[j]*m_Pdev[k];


                }
            }
        }
        m_BGtilda(0,w)=cB(0,0);
        m_BGtilda(1,w)=cB(0,1);
        m_BGtilda(2,w)=cB(0,2);


        m_BGtilda(3,w)=cBdx(0,0);
        m_BGtilda(4,w)=cBdy(0,0);
        m_BGtilda(5,w)=cBdz(0,0);
        m_BGtilda(6,w)=cBdy(0,1);
        m_BGtilda(7,w)=cBdz(0,1);

        //        ROS_INFO_STREAM("BGtilda #"<<w<<" is: \n"<< m_BGtilda);
    }
}

void BSplineMap::computeCmap()
{

    ROS_DEBUG_STREAM("Computing C");
    m_C.topLeftCorner(3,3).setIdentity();
    m_C.topRightCorner(3,5).setZero();
    m_C.bottomLeftCorner(3,3).setZero();
    m_C.topRightCorner(3,5).setZero();

    ROS_DEBUG_STREAM("Computing C Pt. II");
    m_C(3,3)=kmag*ctrlOut[0];
    m_C(3,4)=kmag*ctrlOut[1];
    m_C(3,5)=kmag*ctrlOut[2];
    m_C(4,4)=kmag*ctrlOut[0];
    m_C(4,6)=kmag*ctrlOut[1];
    m_C(4,7)=kmag*ctrlOut[2];
    m_C(5,3)=-kmag*ctrlOut[2];
    m_C(5,5)=kmag*ctrlOut[0];
    m_C(5,6)=-kmag*ctrlOut[2];
    m_C(5,7)=kmag*ctrlOut[1];

    ROS_DEBUG_STREAM("C out");
    ROS_DEBUG_STREAM("Cmap is: \n"<< m_C);


}

Eigen::Matrix<double,Eigen::Dynamic,1> BSplineMap::evaluate()
{
    Eigen::Matrix<double,Eigen::Dynamic,1> output;

    computeCFmap();
    computeCmap();


    ROS_DEBUG_STREAM("CS");
    m_CSmap=m_C*m_BGtilda;

    ROS_DEBUG_STREAM("CS is: \n"<< m_CSmap);

    m_SCmap=pseudoInverse(m_CSmap);

    ROS_DEBUG_STREAM("And its inverse is: \n"<< m_SCmap);

    ROS_DEBUG_STREAM("finalProd");
    output=m_SCmap*ctrlOut;

    if (output.maxCoeff()>k_MaxCurrent)
    {
        ROS_WARN_STREAM("Input is: \n"<<ctrlOut<<"\n The unsaturated Output is: \n"<< output);
        output=output/output.maxCoeff()*k_MaxCurrent;
    }
    if (output.minCoeff()<-k_MaxCurrent)
    {
        ROS_WARN_STREAM("Input is: \n"<<ctrlOut<<"\n The unsaturated Output is: \n"<< output);
        output=output/output.minCoeff()*k_MaxCurrent;
    }

    return output;

}



bool BSplineMap::settingsCb(batmag_soft::ForceCurrentSettings::Request &req,
                            batmag_soft::ForceCurrentSettings::Response &res)
{
    if(req.isSetRequest)
    {
        bR=req.br[0];
        muR=req.mur[0];
        radius=req.radius[0];

        ROS_INFO_STREAM("Parameters changed to\n Br: "<<bR<<"\n mur: "<<muR<<"\n radius: "<<radius);

        volume=(double)4/3*M_PI*pow(radius,3);
        chi=muR/(4*M_PI*10e-7)-1;
        kmag=chi*volume;
    }
    else
    {
        ROS_INFO_STREAM("Processing Request");
        res.br.resize(1);
        res.mur.resize(1);
        res.radius.resize(1);


        res.br[0]=bR;
        res.mur[0]=muR;
        res.radius[0]=radius;
    }
    return true;
}
