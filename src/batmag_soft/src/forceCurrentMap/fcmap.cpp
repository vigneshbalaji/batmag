#include "fcmap.h"

FCMap::FCMap()
{

    stateSub=nh.subscribe("state",1, &FCMap::positionUpdate, this);
}

FCMap::~FCMap(){

    ROS_INFO_STREAM("The map has been killed");
}

//CB for when a control message is received

void FCMap::readMsg(const batmag_soft::CtrlOutput::ConstPtr& msg)
{
    for(int i=0; i<msg->size && i<kStateSize; i++)
    {
        ctrlOut[i]=msg->ctrlout[i];
        ROS_DEBUG_STREAM("Control output #" << i << "is" << ctrlOut[i]);
    }
    newAction=true;
}

void FCMap::positionUpdate(const batmag_soft::State &msg)
{
    ROS_DEBUG_STREAM("About top update position");
    //-------//
    //WARNING//
    //As of now this only works with 3D controllers (three states max)

    if(msg.state.size()>2)
        ROS_DEBUG_STREAM("Message size: "<< msg.state.size());
    for(int i=0; i<kStateSize; i++)
    {

        ROS_DEBUG_STREAM("State "<<i<<" is: "<<msg.state[i]);
        state[i]=msg.state[i];
        if(i<3)
            posAgent1[i]=msg.state[i];
        else
            if(i<6)
                posAgent2[i-3]=msg.state[i];
    }

    ROS_DEBUG_STREAM("Position updated");
}


void FCMap::saturateAndPolarizeCurrents(Eigen::Matrix<double,Eigen::Dynamic,1> & currents)
{
    for(int i=0; i<9; i++)
    {
        //        ROS_INFO_STREAM("PreFiltered current "<<i<<" from MapNode is "<<curents[i]);
        if(currents[i]>kMaxCurrent[i])
            currents[i]=kMaxCurrent[i];
        else if (currents[i]<-kMaxCurrent[i])
            currents[i]=-kMaxCurrent[i];
        if(polarity[i]>0)
            currents[i]=currents[i];
        else
            currents[i]=-currents[i];
        //        ROS_INFO_STREAM("Current "<<i<<" from MapNode is "<<currents[i]);
    }
}
