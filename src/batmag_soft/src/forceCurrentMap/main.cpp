#include <functional>

//Ros Includes
#include "ros/spinner.h"
#include "ros/ros.h"

//Messages
#include "batmag_soft/CtrlOutput.h"
#include "batmag_soft/MapOutput.h"
#include "batmag_soft/StartStop.h"
#include "std_msgs/String.h"
#include "batmag_soft/GUIpreferences.h"
#include "batmag_soft/pmMove.h"

//Subclasses
#include "linearmap.h"
#include "vectormap.h"
#include "bsplinemap.h"
#include "../typeHeader.h"
#include "magneticfield.h"
#include "fieldonlymap.h"

//Boost+std
#include <boost/function.hpp>
#include <boost/bind.hpp>
#include <string>

using std::string;


shared_ptr<FCMap> mapInit(string selectedMap, string selectedAgent){
    shared_ptr<FCMap> map;

    agentType agent=setUseAgent(selectedAgent);

    if(selectedMap=="Linear Map") return boost::make_shared<LinearMap>(agent);

    if(selectedMap=="Vector Map") return boost::make_shared<VectorMap>(agent);

    if(selectedMap=="Field Only") return boost::make_shared<FieldOnlyMap>(agent);

    if(selectedMap=="Bspline Map") return boost::make_shared<BSplineMap>(agent);

//    if(selectedMap=="NL Optimization") return boost::make_shared<ROBOPTIMOptimization>(agent);

    else
        ROS_ERROR_STREAM("The selected type of map doesn't exist");


    ROS_INFO_STREAM("A "<<selectedMap<< " force-current map has been initialized");
    return NULL;
}

void pressedStartMap(const batmag_soft::StartStop::ConstPtr& msg, bool & startPressed, bool & stopPressed, string & selectedMap, string & selectedAgent, double zoomMag)
{
    startPressed=msg->pressedStart;
    stopPressed=msg->pressedStop;
    selectedMap=msg->map;
    selectedAgent=msg->agent;
    zoomMag=msg->zoom;
}

void guiPrefCb(const batmag_soft::GUIpreferences::ConstPtr& msg, bool & ctrlEn, bool & debugEn, double & pmPos)
{
    ctrlEn=msg->ctrlEn||msg->gravityEn;
    debugEn=msg->debugEn;
    pmPos=msg->PMposition;
}


using boost::shared_ptr;

int main(int argc, char *argv[])
{
    ROS_INFO_STREAM("The force-current node has started");
    //Create the ros node and the handle
    ros::init(argc, argv, "force_current_node");
    ros::NodeHandle n;



    //Here you can set the frequency of this node
    double mapFrequency=60;
    ros::Rate mapRate(mapFrequency);

    while(!ros::ok())
        mapRate.sleep();


    //Start an asynchronous spinner active on as many cores as the cpu has
    ros::AsyncSpinner myAsyncSpinner(0);
    myAsyncSpinner.start();

    string selMap,selAgent;

    double zoom=1;

    //Subscribe to Start/Stop messages
    bool startPressed=false, stopPressed=false;
    ros::Subscriber startStopSub = n.subscribe<batmag_soft::StartStop>("gui_start_stop", 1, std::bind(&pressedStartMap, std::placeholders::_1, std::ref(startPressed), std::ref(stopPressed), std::ref(selMap),  std::ref(selAgent), std::ref(zoom)));

    //Subscribe to GUI preferences
    bool ctrlOrGravityEnable=false, debugEnable=false;
    double pmPos=1;
    ros::Subscriber guiPref= n.subscribe<batmag_soft::GUIpreferences>("gui_preferences", 1, std::bind(guiPrefCb, std::placeholders::_1, std::ref(ctrlOrGravityEnable),std::ref(debugEnable), std::ref(pmPos)));

    while(ros::ok() && !startPressed && !stopPressed)
    {
        mapRate.sleep();
    }

    batmag_soft::MapOutput msgOut;

    //If start has been pressed initialize the map
    //Declare the pointer for the map
    shared_ptr<FCMap> myMap= mapInit(selMap, selAgent);

    //Publisher and subscriber to input ctrl action and out currents
    ros::Subscriber ctrlOutputSub = n.subscribe<batmag_soft::CtrlOutput>("ctrl_output", 1, &FCMap::readMsg, myMap);
    ros::Publisher mapOutPub=n.advertise<batmag_soft::MapOutput>("map_output",1);


    Eigen::Matrix<double, Eigen::Dynamic,1>  output;

    while(ros::ok() && !stopPressed)
    {
        //Set PM position
        msgOut.size=10;
        msgOut.mapout[9]=pmPos;

        //        only evaluate the map if there are new value
        if(ctrlOrGravityEnable)
        {
            if(myMap->newAction)
            {

                output= myMap->evaluate();
                myMap->newAction=false;
                msgOut.size=output.size();
                ROS_DEBUG_STREAM("Output size is"<<output.size());
                for(int i=0; i<output.size(); i++)
                {
                    ROS_DEBUG_STREAM("Output #"<<i<<" is "<<output[i]);
                    msgOut.mapout[i]=output[i];
                }

                mapOutPub.publish(msgOut);
                ROS_INFO_STREAM_ONCE("Started to send Force-Current Messages");
            }
        }


        else if (!debugEnable) //if debugEnable is on the GUI is already sending current outputs directly this avoids race conditions
        {
            ROS_DEBUG_STREAM("Output size is"<<output.size());
            for(int i=0; i<9; i++)
            {
                ROS_DEBUG_STREAM("Output #"<<i<<" is "<<output[i]);
                msgOut.mapout[i]=0;
            }
            mapOutPub.publish(msgOut);
        }


        //The asynchronous spinner will wake up the map when there is a new message
        mapRate.sleep();
    }

    myAsyncSpinner.stop();


}

//////ultrasound holder

