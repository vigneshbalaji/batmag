#pragma once

#include "ros/ros.h"
#include "fcmap.h"
#include "magneticfield.h"
#include <Eigen/Dense>
#include <math.h>
#include <string>

#include"magneticdipolemodel.h"
#include "batmag_soft/ForceCurrentSettings.h"

class BSplineMap : public FCMap
{
public:
    BSplineMap(agentType selectedAgent);
    ~BSplineMap();

    //All maps must have an evaluate to be used as Cb for control messages
    virtual     Eigen::Matrix<double,Eigen::Dynamic,1>  evaluate();

    virtual bool settingsCb(batmag_soft::ForceCurrentSettings::Request &req,
                       batmag_soft::ForceCurrentSettings::Response &res);

protected:

    double k_MaxCurrent=4;

    ros::NodeHandle n;

    Eigen::MatrixXd forceCurMap,  currentToForceMap;//Even though this matrix could be a 10xDyn the pesudoinverse function only works with fully dynamic matrices

    Eigen::Matrix<double,9,1> m_Iprev;

    Eigen::Matrix<double, 1000,3> m_BC[9];

    //Basis Functions...
    Eigen::Matrix<double, 10,1> m_M,m_N,m_P;
    //..and their derivatives
    Eigen::Matrix<double, 10,1> m_Mdev, m_Ndev, m_Pdev;

    //Matrix of B_tilda and G_hat
    Eigen::Matrix<double, 8,9> m_BGtilda;
    void computeCFmap();

    Eigen::Matrix<double,Eigen::Dynamic, Eigen::Dynamic> m_CSmap, m_SCmap;

    //Matrix C (mapping field and gradient to mdm and forces
    Eigen::Matrix<double, 6,8> m_C;

    void computeCmap();

    std::string filename,
    directory="/home/batmag/batmag_ws/src/batmag_soft/src/forceCurrentMap/BCtext/BC";
    char str[100];

    Eigen::Matrix<double,3,9> hatBx,hatBy,hatBz, bTilda;

    Eigen::Vector3d mdmFinal, mdmCurrent, mdmK, mdmBasic, pos1, pos2, bField;

    std::shared_ptr<MagneticDipoleModel> mdmModel;

    ros::ServiceServer mServer;


    //Parameters
    double radius=0.0004;//[m]
    double muR=2, volume=(double)4/3*M_PI*pow(radius,3);
    double chi=muR/(4*M_PI*10e-7)-1;
    double kmag=chi*volume, bR=1e-5;

    //Output and input vectors are declared in the super class
};
