#pragma once

#include "ros/ros.h"
#include "fcmap.h"
#include "batmag_soft/State.h"

class VectorMap : public FCMap
{
public:
    VectorMap(agentType selectedAgent);

    virtual ~VectorMap();

    //All maps must have an evaluate to be used as Cb for control messages
    virtual Eigen::Matrix<double,Eigen::Dynamic,1>  evaluate();

protected:

    ros::NodeHandle n;

    double forceScalingFactor=2;//Scale the force to avoid extremely high force outputs

    Eigen::MatrixXd forceCurMap,  currentToForceMap;//Even though this matrix could be a 10xDyn the pesudoinverse function only works with fully dynamic matrices
//    Eigen::Matrix<double, Eigen::Dynamic, 10>  currentToForceMap;
    Eigen::Vector3d dist[9];
    int distancePower;

    bool isParamagnetic;
    //Output and input vectors are declared in the super class

    Eigen::Vector3d mmPosition;
    double scale;

    //Zooming magnitude to convert pixels in mm
    double zoomMagnitude;

//    //Ros stuff
//    ros::NodeHandle nh;
//    ros::Subscriber stateSub;

//    //Functions to read state msgs
//    virtual void positionUpdate(const batmag_soft::State &msg);


};

