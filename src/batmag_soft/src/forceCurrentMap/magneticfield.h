#pragma once

#include <fstream>

#include <Eigen/StdVector>
#include <Eigen/Dense>


#include <math.h>
#include <stddef.h>
#include <stdlib.h>

#include "Matlab/rt_nonfinite.h"
#include "Matlab/rtwtypes.h"
#include "Matlab/rtGetInf.h"
#include "Matlab/rtGetNaN.h"

#include "ros/ros.h"



namespace MagneticField
{

bool readBC(char *filename, Eigen::Matrix<double, 1000, 3> & dest);

void BgradXD1(const Eigen::Vector3d pos, Eigen::Matrix<double,3,9> & BgradX);

void BgradYD1(const Eigen::Vector3d pos, Eigen::Matrix<double,3,9> & BgradY);

void BgradZD1(const Eigen::Vector3d pos, Eigen::Matrix<double,3,9> & BgradZ);

void curToFieldD1(const Eigen::Vector3d pos, Eigen::Matrix<double,3,9> & CurToField);//Position in m and field in T

void N7to7(double x, Eigen::Matrix<double, 10,1> & basisFunc);

void diffN7to7(double x, Eigen::Matrix<double, 10,1> & basisDev);

static double rt_powd_snf(double u0, double u1);
}
