#pragma once

#include "nlopt.hpp"
#include <Eigen/StdVector>
#include <Eigen/Dense>

#include <random>

//Time library
#include <boost/date_time/posix_time/posix_time.hpp>

#include "../fcmap.h"
#include "../magneticfield.h"

#include "ros/ros.h"

//Objective function

 double currentsNorm(const std::vector<double> &currents, std::vector<double> &grad, void *my_func_data);


 double makeRandom(double low, double high);


//Force constraints
void mAgent1ForceConstraint (unsigned m, double *result,
                unsigned n, const double *variables,
                 double *grad, /* NULL if not needed */
                 void *func_data);

void mAgent2ForceConstraint (unsigned m, double *result,
                unsigned n, const double *variables,
                 double *grad, /* NULL if not needed */
                 void *func_data);

class NLOPTOptimization: public FCMap
{
public:
    NLOPTOptimization(agentType mAgent);

//    add virtual destructor

    virtual Eigen::Matrix<double,Eigen::Dynamic,1>  evaluate();

    //Norm for objective function
     double norm;

     void test();

     double agent1Forces[3], agent2Forces[3];//milliNewton
     const unsigned int nConstraints=3;
     const double kForceScaling=1;//e6;
     const double kInitCur=0.1;
     const double kScaledInitCur=kInitCur/kForceScaling;

     double const kMagneticDipoleMoment=5.5784e4;//Multiplied to scale to milliNewton

     double bNormGradx,bNormGrady, bNormGradz;

     std::vector<double> currents={kInitCur,kInitCur,kInitCur,kInitCur,kInitCur,kInitCur,kInitCur,kInitCur,kInitCur};
     Eigen::Vector3d agent1Pos, agent2Pos; //mm

     // Time to avoid framerates times for both cameras
     boost::posix_time::ptime endT, initT;
     //The could be just one diff but then it'd need a mutex
     boost::posix_time::time_duration diff;
private:

    const double kCurrentLimit=5;

    void forcesToCurrents1agent();

    void forcesToCurrents2agents();

    bool highError=false;


    Eigen::Vector3d bNormGrad;

    std::vector<double> force1Error, force2Error; //milliNewton


    const double kAbsConstraintsTolerance=1e-3, kRelVariablesTolerance=1e-2, kRelFuncTolerance=0.1;
    const std::vector<double> kRelVariablesTolVec={kRelVariablesTolerance,kRelVariablesTolerance,kRelVariablesTolerance};//,kRelConstraintsTolerance,kRelConstraintsTolerance,kRelConstraintsTolerance,kRelConstraintsTolerance,kRelConstraintsTolerance,kRelConstraintsTolerance};
    const std::vector<double> kAbsTolVec={kAbsConstraintsTolerance,kAbsConstraintsTolerance,kAbsConstraintsTolerance,kAbsConstraintsTolerance,kAbsConstraintsTolerance,kAbsConstraintsTolerance};//,kAbsConstraintsTolerance,kAbsConstraintsTolerance,kAbsConstraintsTolerance};
//    const double constrLimit[3]={kAbsConstraintsTolerance, kAbsConstraintsTolerance, kAbsConstraintsTolerance};
    nlopt::opt mOpt;

    std::vector<double> prevCurr;

    Eigen::Matrix<double,Eigen::Dynamic,1> curOut;

};
