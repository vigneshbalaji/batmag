#include "nloptoptimization.h"


NLOPTOptimization::NLOPTOptimization(agentType mAgent): mOpt(nlopt::LN_COBYLA, 9) //Algorithm, # of variables LD_SLSQP //LN_COBYLA// GN_ISRES
{
    ROS_INFO_STREAM("Starting NLOpt");
    currents.resize(9);
    agent1Pos.resize(kStateSize);

}

Eigen::Matrix<double,Eigen::Dynamic,1> NLOPTOptimization::evaluate()
{

    ROS_INFO_STREAM("Starting optimization");

    initT = boost::posix_time::microsec_clock::local_time();


    for(int i=0; i<kStateSize; ++i)
    {
        if(i<3)
        {
            agent1Pos[i]=position[i];
            agent1Forces[i]=ctrlOut[i];
        }
        else
        {
            agent2Pos[i]=position[i];
            agent2Forces[i]=ctrlOut[i];
        }
    }

    if (kStateSize==3)
    {
        forcesToCurrents1agent();
    }
    else
        if (kStateSize==6)
        {
            forcesToCurrents2agents();
        }
    for(int i=0; i<9; i++)
        curOut[i]=currents[i];


    endT = boost::posix_time::microsec_clock::local_time();

    diff = endT-initT;

    ROS_INFO_STREAM("Obtained the following current values: "<< curOut);

    ROS_INFO_STREAM("The optimization took: " << diff.total_milliseconds() << " ms");


    return curOut;

}

void NLOPTOptimization::test()
{

    ROS_INFO_STREAM("Starting the test");

    force1Error.resize(3);force2Error.resize(3);
    double minWS=-13.5, maxWS=13.5, flimit=2e-6;
    for(int h=0; h<100; ++h)
    {
        pmPos=makeRandom(5, 14);


        agent1Pos={makeRandom(minWS,maxWS), makeRandom(minWS,maxWS), makeRandom(minWS,maxWS)};

        agent2Pos={makeRandom(minWS,maxWS), makeRandom(minWS,maxWS), makeRandom(minWS,maxWS)};




        agent1Forces[0]=makeRandom(-flimit ,flimit ); agent1Forces[1]=makeRandom(-flimit ,flimit ); agent1Forces[2]=makeRandom(-flimit ,flimit );

        agent2Forces[0]=makeRandom(-flimit ,flimit ); agent2Forces[1]=makeRandom(-flimit ,flimit ); agent2Forces[2]=-makeRandom(-flimit ,flimit );

        initT = boost::posix_time::microsec_clock::local_time();

        forcesToCurrents2agents();


        endT = boost::posix_time::microsec_clock::local_time();

        diff = endT-initT;


        if(highError)
        {
            ROS_INFO_STREAM("The optimization took: " << diff.total_milliseconds() << " ms ("<< 1e3/diff.total_milliseconds() <<" Hz)");
            highError=false;
            for(int i=0; i<9; i++)
            {
                //        curOut[i]=currents[i];

                ROS_INFO_STREAM("Current"<< i<< " is :" <<currents[i]);
            }
        }





    }




}

void NLOPTOptimization::forcesToCurrents1agent()
{


    mOpt.set_min_objective(currentsNorm, (void*) this);


    mOpt.add_equality_mconstraint(mAgent1ForceConstraint, (void*)this, kAbsTolVec);


//    mOpt.set_xtol_rel(-1);
//    mOpt.set_xtol_rel(kRelVariablesTolerance);
//    mOpt.set_ftol_abs(0.1);

    mOpt.set_lower_bounds(-kCurrentLimit/kForceScaling);//Scale currents limits
    mOpt.set_upper_bounds(kCurrentLimit/kForceScaling);

    mOpt.set_maxtime(20);


    double minf;

    currents= {kScaledInitCur, kScaledInitCur, kScaledInitCur, kScaledInitCur, kScaledInitCur, kScaledInitCur, kScaledInitCur, kScaledInitCur, kScaledInitCur};


    ROS_INFO_STREAM("Starting the Non Linear optimization for 1 agent");

    try{
        nlopt::result result = mOpt.optimize(currents, minf);
    }
    catch(const std::exception& e)
    {
        ROS_ERROR_STREAM("Exception caught: "<< e.what());
    }


    ROS_INFO_STREAM("Norm: "<< minf);

    for(int i=0; i<currents.size(); ++i)
        currents[i]=currents[i]; //Rescaling currents


    prevCurr=currents;

    double carray[9];

    for(int i=0; i<9; ++i)
    {
        carray[i]=currents[i];
    }
    
    //    carray

    //    for(int i=0; i<9; ++i)
    //    {
    //        carray[i]=carray[i]*1e-2;
    //    }


    Eigen::Vector3d bnormG;

    MagneticField::gradModBFuncD11_mm(agent1Pos, pmPos, carray, bnormG);


    for(int i=0; i<3;++i)
    {
        ROS_INFO_STREAM("Resulting force agent 1 comp : "<<i<<" is: " << bnormG[i]*kMagneticDipoleMoment<<" (Error: "<< bnormG[i]*kMagneticDipoleMoment-agent1Forces[i]<< " , Prescribed: "<<agent1Forces[i]<<  " )");
    }


    //    for(int i=0; i<3;++i)
    //    {
    //        ROS_INFO_STREAM("Resulting force comp : "<<i<<" is: " << bnormG[i]*kMagneticDipoleMoment);
    //    }

    ROS_INFO_STREAM("Removing constraints");

    //Clean the constraints for next iteration
    mOpt.remove_equality_constraints();


}



void NLOPTOptimization::forcesToCurrents2agents()

{

    mOpt.set_min_objective(currentsNorm, (void*) this);


    mOpt.add_equality_mconstraint(mAgent2ForceConstraint, (void*)this, kAbsTolVec);


//    mOpt.set_xtol_rel(kRelVariablesTolerance);
    mOpt.set_ftol_abs(0.1);
//    mOpt.set_stopval(5);

//    mOpt.set_lower_bounds(-kCurrentLimit/kForceScaling);//Scale currents limits
//    mOpt.set_upper_bounds(kCurrentLimit/kForceScaling);

    mOpt.set_maxtime(10);


    //    mOpt.set_maxtime(0.03);



    double minf;

    currents= {kScaledInitCur, kScaledInitCur, kScaledInitCur, kScaledInitCur, kScaledInitCur, kScaledInitCur, kScaledInitCur, kScaledInitCur, kScaledInitCur};


    ROS_DEBUG_STREAM("Starting the Non Linear optimization for 2 agents");

    try{
        mOpt.optimize(currents, minf);
    }
    catch(const std::exception& e)
    {
        ROS_ERROR_STREAM("Exception caught" << e.what());
    }


    ROS_DEBUG_STREAM("Norm: "<< minf);


    prevCurr=currents;

    double carray[9];

    for(int i=0; i<9; ++i)
    {
        carray[i]=currents[i];
    }


    Eigen::Vector3d bnormG1,bnormG2;

    MagneticField::gradModBFuncD11_mm(agent1Pos, pmPos, carray, bnormG1);


    for(int i=0; i<3;++i)
    {

        force1Error[i]=bnormG1[i]*kMagneticDipoleMoment-agent1Forces[i];
        //        ROS_INFO_STREAM("Resulting force agent 1 comp : "<<i<<" is: " << bnormG1[i]*kMagneticDipoleMoment<<" (Error: "<< bnormG1[i]*kMagneticDipoleMoment-agent1Forces[i]<< " , Prescribed: "<<agent1Forces[i]<<  " )");
    }


    MagneticField::gradModBFuncD11_mm(agent2Pos, pmPos, carray, bnormG2);


    for(int i=0; i<3;++i)
    {

        force2Error[i]=bnormG2[i]*kMagneticDipoleMoment-agent2Forces[i];
        //        ROS_INFO_STREAM("Resulting force agent 2 comp : "<<i<<" is: " << bnormG2[i]*kMagneticDipoleMoment<<" (Error: "<< bnormG2[i]*kMagneticDipoleMoment-agent2Forces[i]<< " , Prescribed: "<<agent2Forces[i]<<  " )");
    }


    ROS_DEBUG_STREAM("Removing constraints");

//    if((*std::max_element(force1Error.begin(), force1Error.end()))>1e-4 || (*std::max_element(force2Error.begin(), force2Error.end()))>1e-4)
//    {

        //        ROS_ERROR_STREAM("Unreliable result");


        ROS_ERROR_STREAM("Position 1; x: "<< agent1Pos[0]<< ", y: "<< agent1Pos[1]<< ", z: "<< agent1Pos[2]);
        ROS_ERROR_STREAM("Position 2; x: "<< agent2Pos[0]<< ", y: "<< agent2Pos[1]<< ", z: "<< agent2Pos[2]);
        ROS_ERROR_STREAM("PMpos: "<< pmPos);

        for(int i=0; i<3;++i)
        {

            ROS_ERROR_STREAM("Resulting force agent 1 comp : "<<i<<" is: " << bnormG1[i]*kMagneticDipoleMoment<<" (Error: "<< bnormG1[i]*kMagneticDipoleMoment-agent1Forces[i]<< " , Prescribed: "<<agent1Forces[i]<< " , Relative: "<<100*(bnormG1[i]*kMagneticDipoleMoment-agent1Forces[i])/(bnormG1[i]*kMagneticDipoleMoment)<<  " %)");
            ROS_ERROR_STREAM("Resulting force agent 2 comp : "<<i<<" is: " << bnormG2[i]*kMagneticDipoleMoment<<" (Error: "<< bnormG2[i]*kMagneticDipoleMoment-agent2Forces[i]<< " , Prescribed: "<<agent2Forces[i]<< " , Relative: "<<100*(bnormG2[i]*kMagneticDipoleMoment-agent2Forces[i])/(bnormG2[i]*kMagneticDipoleMoment)<<  " %)");


        }
        highError=true;
//    }
//    else
//        ROS_WARN_STREAM("Result Ok");

    //Clean the constraints for next iteration
    mOpt.remove_equality_constraints();
}







double currentsNorm(const std::vector<double> &x, std::vector<double> &grad, void *my_func_data)
{
    NLOPTOptimization *myOpt = reinterpret_cast<NLOPTOptimization*>(my_func_data);
    double norm=0;

    for (int i=0; i<x.size(); ++i)
        norm=norm+(x[i]*x[i]);

    ROS_DEBUG_STREAM("The norm is: "<<myOpt->norm);

    if (!grad.empty()) {
        for(int j=0; j<x.size(); ++j)
        {
            grad[j]=2*x[j];
        }
    }
    return myOpt->norm;
}


void mAgent1ForceConstraint(unsigned m, double *result, unsigned n, const double *variables, double *grad, void *data)
{
    NLOPTOptimization *myOpt = reinterpret_cast<NLOPTOptimization*>(data);


//    std::vector<double> thisGradient(m), thisCurrentDer(m);
    Eigen::Vector3d thisGradient, thisCurrentDer;

    MagneticField::gradModBFuncD11_mm(myOpt->agent1Pos, myOpt->pmPos, variables, thisGradient);
    ROS_DEBUG_STREAM("# of Currents: "<< n);


//        for(int i=0; i<9; ++i)
//            ROS_INFO_STREAM_COND(variables[i]>0.1, "Current "<< i<< " is bigger than 0.1 and has value :"<<variables[i]);

    //    Computing results

    for(int i=0; i<m; i++)
    {
//        ROS_INFO_STREAM("The "<< i<< " component of the gradient of Bnorm is :"<<thisGradient[i]);
        result[i]=(thisGradient[i]*myOpt->kMagneticDipoleMoment)-myOpt->agent1Forces[i];
//        ROS_INFO_STREAM("Result of the equality constraint component "<< i<< " is :"<<result[i]);
    }

    //Computing partial derivatives
    if (grad) {
//        for(int i=0; i<9; ++i)
//        {
//            MagneticField::bNormGradCurDer(myOpt->agent1Pos, myOpt->pmPos, variables,i, thisCurrentDer);
//            //The n dimension of grad is stored contiguously,
//            //            so that \part c_i / \part x_j is stored in grad[i*n + j].
//            grad[i]=thisCurrentDer[0];
//            grad[i+9]=thisCurrentDer[1];
//            grad[i+9*2]=thisCurrentDer[2];
//            ROS_DEBUG_STREAM("Parital derivatives wrt current "<< i<< " are x: "<<grad[i]<<" , y: "<<grad[i+9]<<" , z: "<<grad[i+9*2]);

//        }

    }

}

void mAgent2ForceConstraint(unsigned m, double *result, unsigned n, const double *variables, double *grad, void *data)
{
    NLOPTOptimization *myOpt = reinterpret_cast<NLOPTOptimization*>(data);


//    std::vector<double> thisGradient(m), thisCurrentDer(m);
    Eigen::Vector3d thisGradient;

    MagneticField::gradModBFuncD11_mm(myOpt->agent1Pos, myOpt->pmPos, variables, thisGradient);
    ROS_DEBUG_STREAM("# of Currents: "<< n);


    //    Computing results

    for(int i=0; i<3; i++)
    {
//        ROS_INFO_STREAM("The "<< i<< " component of the gradient of Bnorm is :"<<thisGradient[i]);
        result[i]=(thisGradient[i]*myOpt->kMagneticDipoleMoment);
        result[i]=result[i]-myOpt->agent1Forces[i];
//        ROS_INFO_STREAM("Result of the equality constraint component "<< i<< " is :"<<result[i]);
    }

    MagneticField::gradModBFuncD11_mm(myOpt->agent2Pos, myOpt->pmPos, variables, thisGradient);
    ROS_DEBUG_STREAM("# of Currents: "<< n);


    //    Computing results

    for(int i=0; i<3; i++)
    {
        ROS_DEBUG_STREAM("The "<< i<< " component of the gradient of Bnorm is :"<<thisGradient[i]);
        result[i+3]=(thisGradient[i]*myOpt->kMagneticDipoleMoment);
        result[i+3]=result[i+3]-myOpt->agent2Forces[i];
//        ROS_INFO_STREAM("Result of the equality constraint component "<< i<< " is :"<<myOpt->kMagneticDipoleMoment);
    }



    //Computing partial derivatives
    if (grad) {
//        for(int i=0; i<9; ++i)
//        {
//            MagneticField::bNormGradCurDer(myOpt->agent1Pos, myOpt->pmPos, variables,i, thisCurrentDer);
//            //The n dimension of grad is stored contiguously,
//            //            so that \part c_i / \part x_j is stored in grad[i*n + j].
//            grad[i]=thisCurrentDer[0];
//            grad[i+9]=thisCurrentDer[1];
//            grad[i+9*2]=thisCurrentDer[2];
//            ROS_DEBUG_STREAM("Parital derivatives wrt current "<< i<< " are x: "<<grad[i]<<" , y: "<<grad[i+9]<<" , z: "<<grad[i+9*2]);

//        }

    }

}

double makeRandom(double low, double high)
{
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_real_distribution<double> dis(low, high);

    return dis(gen);
}

