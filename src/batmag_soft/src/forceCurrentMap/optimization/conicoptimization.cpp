#include "conicoptimization.h"

ConicOptimization::ConicOptimization(agentType selectedAgent)
{

}



ConicOptimization::~ConicOptimization()
{
//    delete M;
}

Eigen::Matrix<double,Eigen::Dynamic,1> ConicOptimization::evaluate()
{
    ;
};


void ConicOptimization::solve_currentsSDP()
{
    boost::posix_time::ptime TimeStart, TimeEnd;
    boost::posix_time::time_duration TimeDiff;

    TimeStart = boost::posix_time::microsec_clock::local_time();
    computeConstraintMatrices();
    TimeEnd = boost::posix_time::microsec_clock::local_time();
    TimeDiff = TimeEnd - TimeStart;

    ROS_INFO_STREAM("compute matrices time: " << TimeDiff.total_microseconds() << " microseconds");

    Model::t M = new Model("sdo1"); //TODO check if you can reuse always the same model only changing the constraints
    auto _M = finally([&]() { M->dispose(); });

    // Setting up the variables
    Variable::t X = M->variable("", Domain::inPSDCone(9));//Declare 9 variables

    mosek::fusion::Matrix::t C = mosek::fusion::Matrix::eye(9);

    mosek::fusion::Matrix::t A1 = eigenToFusionMat(masterConstMatX);

    mosek::fusion::Matrix::t A2 = eigenToFusionMat(masterConstMatY);

    mosek::fusion::Matrix::t A3 = eigenToFusionMat(masterConstMatZ);

    mosek::fusion::Matrix::t A4 = eigenToFusionMat(slaveConstMatX);

    mosek::fusion::Matrix::t A5 = eigenToFusionMat(slaveConstMatY);

    mosek::fusion::Matrix::t A6 = eigenToFusionMat(slaveConstMatZ);

    // Objective
    M->objective(ObjectiveSense::Minimize, Expr::dot(C, X));

    // Constraints
    M->constraint("c1", Expr::dot(A1, X), Domain::equalsTo(xForceMaster));
    M->constraint("c2", Expr::dot(A2, X), Domain::equalsTo(yForceMaster));
    M->constraint("c3", Expr::dot(A3, X), Domain::equalsTo(zForceSlave));
    M->constraint("c4", Expr::dot(A4, X), Domain::equalsTo(xForceSlave));
    M->constraint("c5", Expr::dot(A5, X), Domain::equalsTo(yForceSlave));
    M->constraint("c6", Expr::dot(A6, X), Domain::equalsTo(zForceSlave));

    // Run the optimization
    initT = boost::posix_time::microsec_clock::local_time();
    M->solve();
    endT = boost::posix_time::microsec_clock::local_time();
    diffT = endT-initT;
    ROS_INFO_STREAM("optimization time: " << diffT.total_milliseconds() << " microseconds");

    ROS_INFO_STREAM("Solution : ");
    ROS_INFO_STREAM(" X = " << *(X->level()));
    // TODO: Find a way to extract the matrix
    auto test = *(X->level());
    //    ROS_INFO_STREAM("test: " << test[15]);
    for(int i=0; i<9; i++)
    {
        for(int j=0; j<9; j++)
        {
            optimSDP(i,j) = test[9*i+j];
        }
    }
    ROS_INFO_STREAM("OptimSDP: \n" << optimSDP);

    Eigen::SelfAdjointEigenSolver<Eigen::Matrix<double, 9, 9>> eigensolver(optimSDP);
    ROS_INFO_STREAM("The eigenvalues of A are:\n" << eigensolver.eigenvalues());
    ROS_INFO_STREAM("Here's a matrix whose columns are eigenvectors of A \n"
         << "corresponding to these eigenvalues:\n"
         << eigensolver.eigenvectors());
    optimCurrent = sqrt(eigensolver.eigenvalues()[3])*eigensolver.eigenvectors().col(3);
    ROS_INFO_STREAM("OptimCurrent \n" << optimCurrent);
    //    Eigen::EigenSolver<Eigen::MatrixXd> eigval(OptimSDP,false);
    //    ROS_INFO_STREAM("The eigenvalues of the solution matrix: \n" << eigval.eigenvalues());
}


void ConicOptimization::computeConstraintMatrices()
{



    //TODO add PM magnetic dipole moment

    // compute the magnetic field per unit current matrix
    /* Master */
//    MasterMagFieldperUnitCurrMat = Eigen::MatrixXd(2,4);
//    mMagneticField.MagneticFieldPerUnitCurrentPolynomialMatrix(xMaster,yMaster,MasterMagFieldperUnitCurrMat);
//    /* Slave */
//    SlaveMagFieldperUnitCurrMat = Eigen::MatrixXd(2,4);
//    mMagneticField.MagneticFieldPerUnitCurrentPolynomialMatrix(xSlave,ySlave,SlaveMagFieldperUnitCurrMat);

//    // compute the magnetic field per unit current gradient matrix
//    /* Master */
//    MasterMagFieldGradperUnitCurrMatX = Eigen::MatrixXd(2,4);
//    MasterMagFieldGradperUnitCurrMatY = Eigen::MatrixXd(2,4);
//    mMagneticField.MagneticFieldPerUnitCurrentGradientPolynomialMatrix(xMaster,yMaster,MasterMagFieldGradperUnitCurrMatX,MasterMagFieldGradperUnitCurrMatY);
//    /* Slave */
//    SlaveMagFieldGradperUnitCurrMatX = Eigen::MatrixXd(2,4);
//    SlaveMagFieldGradperUnitCurrMatY = Eigen::MatrixXd(2,4);
//    mMagneticField.MagneticFieldPerUnitCurrentGradientPolynomialMatrix(xSlave,ySlave,SlaveMagFieldGradperUnitCurrMatX,SlaveMagFieldGradperUnitCurrMatY);

//    // compute the constraint matrices
//    /* Master */
//    MasterconstmatX = MasterMagneticConstant*(MasterMagFieldGradperUnitCurrMatX.transpose()*MasterMagFieldperUnitCurrMat + MasterMagFieldperUnitCurrMat.transpose()*MasterMagFieldGradperUnitCurrMatX);
//    MasterconstmatY = MasterMagneticConstant*(MasterMagFieldGradperUnitCurrMatY.transpose()*MasterMagFieldperUnitCurrMat + MasterMagFieldperUnitCurrMat.transpose()*MasterMagFieldGradperUnitCurrMatY);
////    cout << "MasterconstmatX: \n" << MasterconstmatX << endl;
////    cout << "MasterconstmatY: \n" << MasterconstmatY << endl;

//    /* Slave */
//    SlaveconstmatX = SlaveMagneticConstant*(SlaveMagFieldGradperUnitCurrMatX.transpose()*SlaveMagFieldperUnitCurrMat + SlaveMagFieldperUnitCurrMat.transpose()*SlaveMagFieldGradperUnitCurrMatX);
//    SlaveconstmatY = SlaveMagneticConstant*(SlaveMagFieldGradperUnitCurrMatY.transpose()*SlaveMagFieldperUnitCurrMat + SlaveMagFieldperUnitCurrMat.transpose()*SlaveMagFieldGradperUnitCurrMatY);
////    cout << "SlaveconstmatX: \n" << SlaveconstmatX << endl;
////    cout << "SlaveconstmatY: \n" << SlaveconstmatY << endl;
}


template<typename _Scalar, int _Rows, int _Cols>
mosek::fusion::Matrix::t ConicOptimization::eigenToFusionMat(const Eigen::Matrix<double, 9, 9> &eigMat)
{

    //This is horrible but fusion matrices' single elements are not accessible and it's not possible to initialize the matrices with anything but proprietary formats

   return  mosek::fusion::Matrix::dense(new_array_ptr<double,2>({{eigMat(0,0),eigMat(0,1),eigMat(0,2),eigMat(0,3), eigMat(0,4),eigMat(0,5),eigMat(0,6),eigMat(0,7), eigMat(0,8)},
                                                                                                  {eigMat(1,0),eigMat(1,1),eigMat(1,2),eigMat(1,3), eigMat(1,4),eigMat(1,5),eigMat(1,6),eigMat(1,7), eigMat(1,8)},
                                                                                                  {eigMat(2,0),eigMat(2,1),eigMat(2,2),eigMat(2,3), eigMat(2,4),eigMat(2,5),eigMat(2,6),eigMat(2,7), eigMat(2,8)},
                                                                                                  {eigMat(3,0),eigMat(3,1),eigMat(3,2),eigMat(3,3), eigMat(3,4),eigMat(3,5),eigMat(3,6),eigMat(3,7), eigMat(3,8)},
                                                                                                  {eigMat(4,0),eigMat(4,1),eigMat(4,2),eigMat(4,3), eigMat(4,4),eigMat(4,5),eigMat(4,6),eigMat(4,7), eigMat(4,8)},
                                                                                                  {eigMat(5,0),eigMat(5,1),eigMat(5,2),eigMat(5,3), eigMat(5,4),eigMat(5,5),eigMat(5,6),eigMat(5,7), eigMat(5,8)},
                                                                                                  {eigMat(6,0),eigMat(6,1),eigMat(6,2),eigMat(6,3), eigMat(6,4),eigMat(6,5),eigMat(6,6),eigMat(6,7), eigMat(6,8)},
                                                                                                  {eigMat(7,0),eigMat(7,1),eigMat(7,2),eigMat(7,3), eigMat(7,4),eigMat(7,5),eigMat(7,6),eigMat(7,7), eigMat(7,8)},
                                                                                                  {eigMat(8,0),eigMat(8,1),eigMat(8,2),eigMat(8,3), eigMat(8,4),eigMat(8,5),eigMat(8,6),eigMat(8,7), eigMat(8,8)}}));;
}

void ConicOptimization::test()
{


}
