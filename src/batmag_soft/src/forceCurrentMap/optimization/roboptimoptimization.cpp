#include "roboptimoptimization.h"

ROBOPTIMOptimization::ROBOPTIMOptimization(agentType agent)
{

};


Eigen::Matrix<double,Eigen::Dynamic,1> ROBOPTIMOptimization::evaluate()
{

    ROS_DEBUG_STREAM("Starting optimization");

    initT = boost::posix_time::microsec_clock::local_time();






    endT = boost::posix_time::microsec_clock::local_time();

    diff = endT-initT;


    ROS_DEBUG_STREAM("The optimization took: " << diff.total_milliseconds() << " ms");


    return curOut;

}


int ROBOPTIMOptimization::test ()
{

//    for(int i=0; i<kStateSize; ++i)
//    {
//        if(i<3)
//        {
//            posAgent1[i]=position[i];
//            forcesAgent1[i]=ctrlOut[i];
//        }
//        else
//        {
//            posAgent2[i-3]=position[i];
//            forcesAgent2[i-3]=ctrlOut[i];
//        }
//    }

    //Assign Forces



    ROS_INFO_STREAM("Starting test");


    srand((unsigned)time(0));

    initT = boost::posix_time::microsec_clock::local_time();

    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()

    const double minWS=-13.5, maxWS=13.5, flimit=5e-6;

    std::uniform_real_distribution<double> disWS(minWS, maxWS), disf(-flimit,flimit), disPM(5,14);

    pmPos=disPM(gen);


    posAgent1={disWS(gen), disWS(gen), disWS(gen)};

    posAgent2={disWS(gen), disWS(gen), disWS(gen)};




    forcesAgent1[0]= disf(gen); forcesAgent1[1]=disf(gen); forcesAgent1[2]=disf(gen);

    forcesAgent2[0]=disf(gen); forcesAgent2[1]=disf(gen); forcesAgent2[2]=disf(gen);



    ROS_INFO_STREAM("Position 1: "<< posAgent1<< " Position 2 "<< posAgent2<< " PM magnet "<< pmPos);

    ROS_DEBUG_STREAM("Set parameters");


  // Create cost function.
  boost::shared_ptr<F> f (new F ());

  // Create problem.
  solver_t::problem_t pb (f);

  // Set bounds for all optimization parameters.
//   -5. < x_i < 5. (x_i in [-5.;5.])
  for (Function::size_type i = 0; i < pb.function ().inputSize (); ++i)
    pb.argumentBounds ()[i] = Function::makeInterval (-5., 5.);

//  // Set the starting point.
  Function::vector_t start (pb.function ().inputSize ());
////  start[0] = 1., start[1] = 5., start[2] = 5., start[3] = 1.; TODO

//  solver_t::problem_t::startingPoint_t & start=pb.startingPoint();

  for(int i=0; i<9; ++i)
  {
      start[i]=0.1;
      ROS_DEBUG_STREAM("Setting strarting point "<< i);
  }


  pb.startingPoint()=start;

//  start[0] = .1, start[1] = .1, start[2] = .1, start[3] = .1, start[4] = .1, start[5] = .1, start[6] = .1, start[7] = .1, start[8] = .1;



  ROS_DEBUG_STREAM("Set problem, initiating functions");


  // Create constraints.
//  boost::shared_ptr<constr1Agent> constr1 (new constr1Agent ());
  boost::shared_ptr<constr2Agents> constr2 (new constr2Agents ());

  F::intervals_t bounds;
  solver_t::problem_t::scaling_t scaling;

  //Set position of agents and permanent magnets
//  constr1->setParam(posAgent1, pmPos);
  constr2->setParam(posAgent1, posAgent2, pmPos);

  //  pb.addConstraint
  //    (boost::static_pointer_cast<Function> (constr1),
  //     bounds, scaling);

  //  bounds.clear ();
  //  scaling.clear ();

  //  scaling.push_back (1.);
    // Add constraints


  // Set intervals for constraints
  bounds.push_back(Function::makeInterval (forcesAgent1[2],forcesAgent1[2]));
  bounds.push_back(Function::makeInterval (forcesAgent1[1],forcesAgent1[1]));
  bounds.push_back(Function::makeInterval (forcesAgent1[0],forcesAgent1[0]));

  bounds.push_back(Function::makeInterval (forcesAgent2[2],forcesAgent2[2]));
  bounds.push_back(Function::makeInterval (forcesAgent2[1],forcesAgent2[1]));
  bounds.push_back(Function::makeInterval (forcesAgent2[0],forcesAgent2[0]));



  scaling.push_back (1e8);scaling.push_back (1e8);scaling.push_back (1e8);scaling.push_back (1e8);scaling.push_back (1e8);scaling.push_back (1e8);
  pb.addConstraint
    (boost::static_pointer_cast<Function> (constr2),
     bounds, scaling);


  ROS_DEBUG_STREAM("Set constratints, starting the solver");

  // Initialize solver.

  // Here we are relying on a dummy solver.
  // You may change this string to load the solver you wish to use:
  //  - Ipopt: "ipopt", "ipopt-sparse", "ipopt-td"
  //  - Eigen: "eigen-levenberg-marquardt"
  //  etc.
  // The plugin is built for a given solver type, so choose it adequately.
  SolverFactory<solver_t> factory ("ipopt", pb);
  solver_t& solver = factory ();

  //Set Parameters
  solver.parameters()["max-iterations"].value=3000;
  solver.parameters()["ipopt.acceptable_iter"].value=0;
  solver.parameters()["ipopt.tol"].value=0.5;
  solver.parameters()["ipopt.acceptable_tol"].value=0.8;
  solver.parameters()["ipopt.constr_viol_tol"].value=1e-8;
  solver.parameters()["ipopt.acceptable_constr_viol_tol"].value=1e-6;
  solver.parameters()["ipopt.print_user_options"].value="yes";
  solver.parameters()["ipopt.print_options_documentation"].value="yes";
  solver.parameters()["ipopt.print_level"].value=12;
//  solver.parameters()["ipopt.linear_solver"].value="mumps";
  solver.parameters()["derivative_test_print_all"].value="yes";
//  solver.parameters()["ipopt.expect_infeasible_problem"].value="yes";
//  solver.parameters()["ipopt.derivative_test"].value="second-order";
//  solver.parameters()["derivative_test_print_all"].value="yes";


  // Compute the minimum and retrieve the result.
  solver_t::result_t res = solver.minimum ();

  endT = boost::posix_time::microsec_clock::local_time();

  diff = endT-initT;

  // Display solver information.
  ROS_INFO_STREAM(solver);

  ROS_INFO_STREAM("The optimization took: " << diff.total_milliseconds() << " ms ("<< 1e3/diff.total_milliseconds() <<" Hz)");

  // Check if the minimization has succeeded.

  // Process the result
  switch (res.which ())
    {
    case solver_t::SOLVER_VALUE:
      {
        // Get the result.
        Result& result = boost::get<Result> (res);

        // Display the result.
        ROS_INFO_STREAM("A solution has been found: " << std::endl << result);


//        Eigen::Vector3d error1, error2;
        for(int i=0; i<3;++i)
            ROS_INFO_STREAM(i<<" Requested:"<< forcesAgent1[i] <<" got "<< result.constraints[i]<<" error is: "<<result.constraints[i]-forcesAgent1[i]<< " percent: "<<100*(result.constraints[i]-forcesAgent1[i])/forcesAgent1[i]);

        for(int i=0; i<3;++i)
            ROS_INFO_STREAM(i+3<<" Requested:"<< forcesAgent2[i] <<" got "<< result.constraints[i+3]<<" error is: "<<" is: "<<result.constraints[i+3]-forcesAgent2[i]<< " percent: "<<100*(result.constraints[i+3]-forcesAgent2[i])/forcesAgent2[i]);
//        double error1= MagneticField::bNormGradmm(posAgent1, pmPos, result.x)*kMagneticDipoleMoment - forcesAgent1;
//        double error2= MagneticField::bNormGradmm(posAgent2, pmPos, result.x)*kMagneticDipoleMoment - forcesAgent2;

//        ROS_INFO_STREAM("Requested force 1: "<<forcesAgent1<<" obtained "<< MagneticField::bNormGradmm(posAgent1, pmPos, result.x)*kMagneticDipoleMoment<< " with error "<< error1);
//        ROS_INFO_STREAM("Requested force 2: "<<forcesAgent2<<" obtained "<< MagneticField::bNormGradmm(posAgent2, pmPos, result.x)*kMagneticDipoleMoment<< " with error "<< error2);

        return 0;
      }

    case solver_t::SOLVER_ERROR:
      {
        ROS_INFO_STREAM("A solution should have been found. Failing..."
                  << std::endl
                  << boost::get<SolverError> (res).what ()
                 );

        return 2;
      }

    case solver_t::SOLVER_NO_SOLUTION:
      {
        ROS_INFO_STREAM("The problem has not been solved yet."
                 );

        return 2;
      }
    }

  // Should never happen.
  assert (0);
  return 42;
}



double makeRandom(double low, double high)
{
    std::uniform_int_distribution<> dis(0, 1e5);
    double mRand;
    mRand=(low + (high-low)*(double)(static_cast <double> (rand()) /( static_cast <double> (RAND_MAX))));
    ROS_INFO_STREAM ("random number is "<<mRand);
    return mRand;
}


