#pragma once

#include <boost/shared_ptr.hpp>
# include <random>

#include <roboptim/core.hh>
#include <roboptim/core/plugin/dummy.hh>
#include <roboptim/core/plugin/dummy-td.hh>

#include <boost/shared_ptr.hpp>

#include <roboptim/core/twice-differentiable-function.hh>
#include <roboptim/core/io.hh>
#include <roboptim/core/solver.hh>
#include <roboptim/core/solver-factory.hh>


#include <roboptim/core/derivable-parametrized-function.hh>

//Time library
#include <boost/date_time/posix_time/posix_time.hpp>


#include "../fcmap.h"
#include "../../typeHeader.h"
#include "../magneticfield.h"

#include "ros/ros.h"

using namespace roboptim;

double constexpr kMagneticDipoleMoment=5.5784e-4;//unit measure A/m^2


double makeRandom(double low, double high);

typedef Solver<EigenMatrixDense> solver_t;

class ROBOPTIMOptimization: public FCMap
{

private:

    // Time to avoid framerates times for both cameras
    boost::posix_time::ptime endT, initT;
    //The could be just one diff but then it'd need a mutex
    boost::posix_time::time_duration diff;


public:
    ROBOPTIMOptimization(agentType agent);


    virtual Eigen::Matrix<double,Eigen::Dynamic,1>  evaluate();


    int test();


};


struct F : public TwiceDifferentiableFunction
{
    F () : TwiceDifferentiableFunction (9, 1, "Cost Function: I'*I")
    {
    }

    void impl_compute (result_ref result, const_argument_ref x) const
    {
        result[0] = x[0]*x[0] + x[1]*x[1]+ x[2]*x[2]+ x[3]*x[3]+ x[4]*x[4]+ x[5]*x[5]+ x[6]*x[6]+ x[7]*x[7]+ x[8]*x[8];
    }

    void impl_gradient (gradient_ref grad, const_argument_ref x, size_type) const //void impl_gradient (gradient_ref grad, const_argument_ref x, size_type) const
    {
        grad << 2*x[0], 2*x[1], 2*x[2], 2*x[3], 2*x[4], 2*x[5], 2*x[6], 2*x[7], 2*x[8];
    }

    void impl_hessian (hessian_ref h, const_argument_ref x, size_type) const
    {
        h <<    2, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 2, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 2, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 2, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 2, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 2, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 2, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 2, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 2;
    }
};


//struct constr1Agent : public TwiceDifferentiableFunction
//{
//    Eigen::Vector3d agent1Pos;
//    double pmPos;

//    constrAgent1 () : TwiceDifferentiableFunction (9, 3, "Forces on agent 1")
//    {
//    }


//    inline void setParam(Eigen::Vector3d pos, double pm) {agent1Pos=pos; pmPos=pm;}

//    void impl_compute (result_ref result, const_argument_ref x) const
//    {
//        Eigen::Vector3d grad1;

//        MagneticField::gradModBFuncD11_mm(agent1Pos, pmPos, x, grad1);


//        ROS_DEBUG_STREAM("Computing constr 1");

//        for(int i=0; i<3; ++i)
//        {
//            result[i] = grad1[i]*kMagneticDipoleMoment;
//        }

//    }

//    void impl_gradient (gradient_ref grad, const_argument_ref x, size_type coord) const //void impl_gradient (gradient_ref grad, const_argument_ref x, size_type) const
//    {
//        double partDer[27];

//        MagneticField::partialDerFuncD11_mm(agent1Pos, pmPos, x, partDer);

//        ROS_DEBUG_STREAM("Computing gradient constr 1");

//        for(int i=0; i<9; ++i)
//            grad[i]=partDer[3*i+coord];

//    }

//    void impl_hessian (hessian_ref h, const_argument_ref x, size_type coord) const
//    {
//        double partDer[27], grad[9], incrParDev[27], incrGrad[9];
//        double epsilon=1e-2;


//        ROS_DEBUG_STREAM("Computing hessian constr 1");

//        MagneticField::partialDerFuncD11_mm(agent1Pos, pmPos, x, partDer);
//        for(int i=0; i<9; ++i)
//            grad[i]=partDer[3*i+coord];
//        Eigen::Matrix<double, 9, 1> increm;
//        for(int i=0; i<9;++i)
//        {
//            increm=x;
//            increm(i)=increm(i)+epsilon;
//            MagneticField::partialDerFuncD11_mm(agent1Pos, pmPos, increm, incrParDev);
//            for(int j=0; j<9; ++i)
//            {
//                incrGrad[j]=incrParDev[3*j+coord];
//                h(i,j)=(incrGrad[j]-grad[j])/epsilon;
//            }
//        }
//    }

//};


struct constr2Agents : public TwiceDifferentiableFunction
{
    Eigen::Vector3d agent1Pos,agent2Pos;
    double pmPos;

    constr2Agents () :TwiceDifferentiableFunction (9, 6, "Forces on 2 agents")
    {
    }

    inline void setParam(Eigen::Vector3d pos1, Eigen::Vector3d pos2, double pm) {agent1Pos=pos1; agent2Pos=pos2; pmPos=pm;}

    void impl_compute (result_ref result, const_argument_ref x) const
    {
        Eigen::Vector3d grad1,grad2;

        MagneticField::gradModBFuncD11_mm(agent1Pos, pmPos, x, grad1);

        MagneticField::gradModBFuncD11_mm(agent2Pos, pmPos, x, grad2);

//        ROS_INFO_STREAM("CURRENTS"<< x);

        for(int i=0; i<3; ++i)
        {
            result[i] = grad1[i]*kMagneticDipoleMoment;

            ROS_DEBUG_STREAM("Force 1 on coord"<< i<< " is "<<result[i]);
        }
        for(int i=0; i<3; ++i)
        {
            result[i+3] = grad2[i]*kMagneticDipoleMoment;

            ROS_DEBUG_STREAM("Force 1 on coord"<< i<< " is "<<result[i]);
        }
    }

    void impl_gradient (gradient_ref grad, const_argument_ref x, size_type coord) const //void impl_gradient (gradient_ref grad, const_argument_ref x, size_type) const
    {
        double partDer[9];

        int comp=coord;
        if(coord<3)
            MagneticField::partialDerD11_mm(comp, agent1Pos, pmPos, x, partDer);
        else
            MagneticField::partialDerD11_mm(comp-3, agent2Pos, pmPos, x, partDer);

        for(int i=0; i<9; ++i)
        {
            grad[i]=partDer[i];
        }

    }

    void impl_hessian (hessian_ref h, const_argument_ref x, size_type coord) const
    {


        int comp=coord;
        ROS_DEBUG_STREAM("Computing hessian constr 2");

        double partDer[9],incrParDev[9];
        double epsilon=1e-2;

        if(coord<3)
            MagneticField::partialDerD11_mm(comp, agent1Pos, pmPos, x, partDer);
        else
            MagneticField::partialDerD11_mm(comp-3, agent2Pos, pmPos, x, partDer);

        Eigen::Matrix<double, 9, 1> increm;
        for(int i=0; i<9;++i)
        {
            increm=x;
            increm(i)=increm(i)+epsilon;

            if(coord<3)
                MagneticField::partialDerD11_mm(comp, agent1Pos, pmPos, increm, incrParDev);
            else
                MagneticField::partialDerD11_mm(comp-3, agent2Pos, pmPos, increm, incrParDev);


            for(int j=i; j<9; ++i)
            {
                h(i,j)=(incrParDev[j]-partDer[j])/epsilon;
                h(j,i)=h(i,j);
            }

        }

    }

};
