#pragma once

#include <memory>
#include <iostream>
#include <math.h>

#include "ros/ros.h"
#include "../fcmap.h"

#include "../magneticfield.h"


//POSIX stuff
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>

//Mosek for optimization
#include "monty.h"
#include "fusion.h"

// Eigenvalues

#include <Eigen/StdVector>
#include <Eigen/Dense>
#include <Eigen/Eigenvalues>

// mosek C definitions
#define NUMCON    6   /* Number of constraints.              */
#define NUMVAR    0   /* Number of conic quadratic variables */
#define NUMANZ    3   /* Number of non-zeros in A            */
#define NUMBARVAR 1   /* Number of semidefinite variables    */
#define DIMSDC    4   /* Dimension of semidefinite cone      */
#define NUMSDCV   10  /* Number of scalar SD variables       */


using namespace mosek::fusion;
using namespace monty;

class ConicOptimization: public FCMap
{
public:
    ConicOptimization(agentType selectedAgent);

    virtual ~ConicOptimization();

        virtual Eigen::Matrix<double,Eigen::Dynamic,1>  evaluate();

    // Vacuum permeability constant
    constexpr static double kVacuumPermeability = 4*M_PI*1e-1; // [picoNewton/milliAmpere^2]

    void solve_currentsSDP();

    void test();

protected:

    // Position to compute the magnetic field parameters
    /* Master */
    double xMaster, yMaster, zMaster;

    /* Slave */
    double xSlave, ySlave, zSlave;

    //Prescribed forces:

    double xForceMaster, yForceMaster, zForceMaster,
            xForceSlave, yForceSlave, zForceSlave;


    // Time to avoid framerates times for both cameras
    boost::posix_time::ptime endT, initT;
    //The could be just one diff but then it'd need a mutex
    boost::posix_time::time_duration diffT;


    //Class for obtaining magnetic field and gradient values
//    MagneticField mMagneticField;

    // Optimal solution for the currents [MilliAmperes]
    Eigen::Matrix<double, 9 ,1> optimCurrent;
    Eigen::Matrix<double, 9 ,9> optimSDP;



    const double kMagneticDipolePM=1;//TODO Put right value

    // compute the magnetic field per unit current matrix
    /* Master */
    Eigen::MatrixXd MasterMagFieldperUnitCurrMat;
    /* Slave */
    Eigen::MatrixXd SlaveMagFieldperUnitCurrMat;

    // compute the magnetic field per unit current gradient matrix
    /* Master */
    Eigen::MatrixXd MasterMagFieldGradperUnitCurrMatX;
    Eigen::MatrixXd MasterMagFieldGradperUnitCurrMatY;
    /* Slave */
    Eigen::MatrixXd SlaveMagFieldGradperUnitCurrMatX;
    Eigen::MatrixXd SlaveMagFieldGradperUnitCurrMatY;

    //
    /* Master */
    Eigen::Matrix<double, 9 ,9> masterConstMatX;
    Eigen::Matrix<double, 9 ,9> masterConstMatY;
    Eigen::Matrix<double, 9 ,9> masterConstMatZ;
    /* Slave */
    Eigen::Matrix<double, 9 ,9> slaveConstMatX;
    Eigen::Matrix<double, 9 ,9> slaveConstMatY;
    Eigen::Matrix<double, 9 ,9> slaveConstMatZ;
    /* */
    Eigen::Matrix<double, 9 ,9> Costmat;

    void computeConstraintMatrices();

    template<typename _Scalar, int _Rows, int _Cols>

    mosek::fusion::Matrix::t eigenToFusionMat(const Eigen::Matrix<double, 9, 9> & eigMat);


};
