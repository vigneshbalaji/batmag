#pragma once

#include "batmag_soft/CtrlOutput.h"
#include "batmag_soft/State.h"

#include "ros/ros.h"
#include "../typeHeader.h"
#include "../coilparameters.h"

#include <Eigen/StdVector>
#include <Eigen/Dense>
#include <Eigen/QR>

using std::vector;
using boost::shared_ptr;


//PseudoInverse Function (Moore-Penrose)
template<typename _Matrix_Type_>
_Matrix_Type_ pseudoInverse(const _Matrix_Type_ &a, double epsilon = std::numeric_limits<double>::epsilon())
{
    Eigen::JacobiSVD< _Matrix_Type_ > svd(a ,Eigen::ComputeThinU | Eigen::ComputeThinV);
    double tolerance = epsilon * std::max(a.cols(), a.rows()) *svd.singularValues().array().abs()(0);
    return svd.matrixV() *  (svd.singularValues().array().abs() > tolerance).select(svd.singularValues().array().inverse(), 0).matrix().asDiagonal() * svd.matrixU().adjoint();
}


//Max Currents the elmo can output 10A for 1-6, 5A for 7-9
const double kActualMaxCurrent[9]={10,10,10,10,10,10,5,5,5};
//Acutal limits for the code (can be changed up to kActualMaxCurrent
const double kMaxCurrent[9]={4,4,4,4,4,4,4,4,4};

//Size of the state variable.
//Can be change by the control
const int kStateSize=3;


class FCMap
{
public:
    FCMap();
    virtual ~FCMap();

    virtual Eigen::Matrix<double,Eigen::Dynamic,1>  evaluate() =0;


    //Functions to read msgs

    virtual void readMsg(const batmag_soft::CtrlOutput::ConstPtr& msg);

    virtual void positionUpdate(const batmag_soft::State &msg);

    bool newAction=false;

    void saturateAndPolarizeCurrents(Eigen::Matrix<double, Eigen::Dynamic, 1> &currents);

    inline void setpmPos(double x){pmPos=x;}

    inline double getpmPos(){return pmPos;}

    Eigen::Matrix<double,kStateSize,1> ctrlOut, state;

    Eigen::Vector3d posAgent1, posAgent2, forcesAgent1, forcesAgent2;
    double pmPos;

protected:

    Eigen::Matrix<double,Eigen::Dynamic,1> curOut;



    //Ros stuff
    ros::NodeHandle nh;
    ros::Subscriber stateSub;


    // ////////////////////////////////////////////////// //
    //----------------------------------------------------//
    //Clockwise or counterclockwise windings DO NOT CHANGE//
    const vector<int> polarity={-1, +1, -1, -1, -1, +1, -1, -1, -1};
    //----------------------------------------------------//

};
