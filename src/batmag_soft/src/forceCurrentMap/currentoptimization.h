#pragma once

#include <memory>
#include <iostream>
#include <math.h>

#include "ros/ros.h"
#include "fcmap.h"

#include "magneticfield.h"


//POSIX stuff
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>

//Mosek for optimization
#include "fusion.h"
#include "monty.h"

// Eigenvalues

#include <Eigen/StdVector>
#include <Eigen/Dense>
#include <Eigen/Eigenvalues>

// mosek C definitions
#define NUMCON    4   /* Number of constraints.              */
#define NUMVAR    0   /* Number of conic quadratic variables */
#define NUMANZ    3   /* Number of non-zeros in A            */
#define NUMBARVAR 1   /* Number of semidefinite variables    */
#define DIMSDC    4   /* Dimension of semidefinite cone      */
#define NUMSDCV   10  /* Number of scalar SD variables       */


using namespace mosek::fusion;
using namespace monty;

class CurrentOptimization: public FCMap
{
public:
    CurrentOptimization(agentType selectedAgent);

    virtual ~CurrentOptimization();

    // Vacuum permeability constant
    constexpr static double kVacuumPermeability = 4*M_PI*1e-1; // [picoNewton/milliAmpere^2]

    void solve_currentsSDP();

private:

    // Position to compute the magnetic field parameters
    /* Master */
    double xMaster, yMaster, zMaster;

    /* Slave */
    double xSlave, ySlave, zSlave;

    MagneticField mMagneticField;

    const double kMagneticDipolePM=1;//TODO Put right value

    // compute the magnetic field per unit current matrix
    /* Master */
    Eigen::MatrixXd MasterMagFieldperUnitCurrMat;
    /* Slave */
    Eigen::MatrixXd SlaveMagFieldperUnitCurrMat;

    // compute the magnetic field per unit current gradient matrix
    /* Master */
    Eigen::MatrixXd MasterMagFieldGradperUnitCurrMatX;
    Eigen::MatrixXd MasterMagFieldGradperUnitCurrMatY;
    /* Slave */
    Eigen::MatrixXd SlaveMagFieldGradperUnitCurrMatX;
    Eigen::MatrixXd SlaveMagFieldGradperUnitCurrMatY;

    //
    /* Master */
    Eigen::Matrix<float, 9 ,9> MasterconstmatX;
    Eigen::Matrix<float, 9 ,9> MasterconstmatY;
    Eigen::Matrix<float, 9 ,9> MasterconstmatZ;
    /* Slave */
    Eigen::Matrix<float, 9 ,9> SlaveconstmatX;
    Eigen::Matrix<float, 9 ,9> SlaveconstmatY;
    Eigen::Matrix<float, 9 ,9> SlaveconstmatZ;
    /* */
    Eigen::Matrix<float, 9 ,9> Costmat;

    void computeConstraintMatrices();


};
