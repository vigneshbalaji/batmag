#include "secondscreenimage.h"
#include "ui_secondscreenimage.h"

SecondScreenImage::SecondScreenImage(ros::NodeHandle nh, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::SecondScreenImage),
    it(nh)
{
//    this->setScreen(2);
    ui->setupUi(this);

    //Listen to camera messages
    imageCamSub[0]=it.subscribe("camera0", 1, &SecondScreenImage::imageCb0, this);
    imageCamSub[1]=it.subscribe("camera1", 1, &SecondScreenImage::imageCb1, this);

    startSub=n.subscribe<batmag_soft::StartStop>("gui_start_stop", 1, &SecondScreenImage::startCb, this);

    connect(this,SIGNAL(newCameraImage(int)),this, SLOT(on_newCameraImage(int)),Qt::QueuedConnection);

    ROS_INFO_STREAM("The second window started");
}

void SecondScreenImage::imageCb0(const sensor_msgs::ImageConstPtr& msg){
    imageMessageCallback(msg, 0);
}

void SecondScreenImage::imageCb1(const sensor_msgs::ImageConstPtr& msg){
    imageMessageCallback(msg, 1);
}


void SecondScreenImage::imageMessageCallback(const sensor_msgs::ImageConstPtr &msg, int cameraNum)
{
    ROS_DEBUG_STREAM("Receiving image from camera: "<< cameraNum);
    try
    {
        mutex.lock();
        cameraImage[cameraNum]=cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8)->image;
        mutex.unlock();
        emit newCameraImage(cameraNum);
    }
    catch (cv_bridge::Exception& e)
    {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }

}
//Actual update of the image
//The signal allows makes the image update as little blocking as possible
//This is done because we are actually hogging the ROS master's resources

//Here a signal with the whole image would also be possible, but I'm not sure if it would be efficient

void SecondScreenImage::on_newCameraImage(int camNum)
{
    mutex.lock();
    if(camNum==0)
    {
        ui->camera0View2->showImage(cameraImage[camNum]);
    }

    else
        if(camNum==1)
        {
            ui->camera1View2->showImage(cameraImage[camNum]);
        }
    mutex.unlock();
}

SecondScreenImage::~SecondScreenImage()
{
    delete ui;
}

void SecondScreenImage::startCb (const batmag_soft::StartStop::ConstPtr& msg)
{
    if(msg->pressedStart)
    {
        imageCamSub[0]=it.subscribe("trackerOut0", 1, &SecondScreenImage::imageCb0, this);
        imageCamSub[1]=it.subscribe("trackerOut1", 1, &SecondScreenImage::imageCb1, this);
    }

    if(msg->pressedStop)
        qApp->quit();

}
