#include "tracker3d.h"


Tracker3D::Tracker3D(ros::NodeHandle nh,agentType agent): n(nh), it(nh), myAsyncSpinner(0)
{
    //Here you have to include a subscription to the configuration message
    //Depending on the kind of agent

    // Create a signal to avoid blocking callbacks
    newImageSig.connect(boost::bind(&Tracker3D::on_newImage, this, _1));
//    ROS_INFO_STREAM("Slot to signal connection returned "<<res);

    //TODO add imaging kind
    for(int i=0; i<2;i++)
        trackerCamera[i]=selectTracker(agent);

    //Create a writing client
    writeReq= n.serviceClient<batmag_soft::WriteText>("write_text");

    //Listen to camera messages
    cameraClickSub=n.subscribe("camera_click",1,&Tracker3D::clickCb,this);
    //To avoid overhead the images are not processed at a fixed frequency but as soon as they arrive
    //Hence the frequency of the tracker is the same as the cameras'
    imageCamSub[0]=it.subscribe("camera0", 1, &Tracker3D::imageCb0, this);
    imageCamSub[1]=it.subscribe("camera1", 1, &Tracker3D::imageCb1, this);

    //Publish references
    referencePub=n.advertise<batmag_soft::Reference>("reference",1);

    // Publish output video feed
    image_pub_cam0 = it.advertise("trackerOut0", 1);
    image_pub_cam1 = it.advertise("trackerOut1", 1);


    myAsyncSpinner.start();

}

Tracker3D::~Tracker3D()
{
    trackerCamera[0]->~Tracker();
    trackerCamera[1]->~Tracker();
    myAsyncSpinner.stop();
}

void Tracker3D::get3Dpose(Point3f &pos, Point3f &orient)
{

        if(position.x>=0 && position.y>=0 && position.z>=0)

        pos=position;

        else
            ROS_ERROR_STREAM("The position is incomplete");

        //TODO implement orientation



}

std::shared_ptr<Tracker> Tracker3D::selectTracker(agentType agent){
    std::shared_ptr<Tracker> myTracker;
    //TODO add the imaging kind
    switch (agent)
    {
    case metallicGripper:
        myTracker=std::make_shared<TrackerMetallicCamera>();
        break;
    case hydroGripper://TODO Implement the right tracker here
        myTracker=std::make_shared<TrackerMetallicCamera>();//WRONG!!!!
        break;
//    case microjet:
//        myTracker=std::make_shared<TrackerMicrojetsCamera>();
//        break;
    default:
        ROS_ERROR_STREAM("The selected kind is not available");
        break;
    }
    return myTracker;


}

//Callback for new images from camera

void Tracker3D::imageCb0(const sensor_msgs::ImageConstPtr& msg){
    imageMessageCallback(msg, 0);
}

void Tracker3D::imageCb1(const sensor_msgs::ImageConstPtr& msg){
    imageMessageCallback(msg, 1);
}

void Tracker3D::imageMessageCallback(const sensor_msgs::ImageConstPtr &msg, int cameraNum)
{
    ROS_INFO_STREAM("Receiving image from camera: "<< cameraNum);
    try
    {
        mutex.lock();
        cameraImage[cameraNum]=cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8)->image;
        mutex.unlock();
//        on_newImage(cameraNum);
        newImageSig(cameraNum);

//        emit newImage(cameraNum); //I'd love if this worked
        ROS_INFO_STREAM("Emmited the signal"<< cameraNum);
    }
    catch (cv_bridge::Exception& e)
    {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }

}

void Tracker3D::on_newImage(int cameraNum){
    int i=cameraNum;
    ROS_INFO_STREAM("Processing the received image");
    Point2f camPosition[2];
    if(cameraImage[i].cols>2 && cameraImage[i].cols>2)
    {

    mutex.lock();
    trackerCamera[i]->processFrame(cameraImage[i],trackerOutputImage[i], paramCam[i], camPosition[i]);

    if(camera0Priority)
        position={camPosition[0].x, camPosition[1].x, camPosition[0].y};
    else
        position={camPosition[0].x, camPosition[1].x, camPosition[1].y};
    mutex.unlock();

    cv_bridge::CvImage out_msg;

    out_msg.encoding = sensor_msgs::image_encodings::BGR8; // Or whatever
    out_msg.image    = cameraImage[i]; // Your cv::Mat

    switch (i){
    case 0:
        image_pub_cam0.publish(out_msg.toImageMsg());
        ROS_INFO_STREAM("Sending image from tracker 0");
        break;
    case 1:
        image_pub_cam1.publish(out_msg.toImageMsg());
        ROS_INFO_STREAM("Sending image from tracker 1");
        break;
    default:
        ROS_ERROR_STREAM("The selected camera does not exist!");
        break;
    }
    }
    else
    {
        ROS_ERROR_STREAM("The camera images haven't been initialized.");

    }

}

void Tracker3D::clickCb(const batmag_soft::CameraClickConstPtr &msg)
{

    //If the click is a right one set the position...
    if(msg->button==1)
    {
        //Set the postion depending on the camera image that was clicked
        if(msg->camera == 0)
        {
            mutex.lock();
            //If camera priority0= true take the z coordinate from cam0
            if(camera0Priority)
                position={(float)msg->x, position.y, (float)msg->y};
            else
                //                Else take it form cam1
                position={(float)msg->x, position.y, position.z};
            mutex.unlock();
        }
        else
            if(msg->camera ==1)
            {
                mutex.lock();
                if(camera0Priority)
                    position={position.x,(float) msg->x, (float)msg->y};
                else
                    position={position.x, (float)msg->x, position.z};
                mutex.unlock();
            }
        //Publish a message about the updated position
        myTextReq.request.text="Position set to x: "+ std::to_string(position.x)+" y: "+std::to_string(position.y)+" z: "+std::to_string(position.z);

        if(!writeReq.call(myTextReq))
            ROS_ERROR_STREAM("There was an error contacting the gui");
    }
    else//Set the reference
        if(msg->button==2)
        {
            mutex.lock();
            //Here the z is always taken from the clicked camera (no matter priority)
            if(msg->camera == 0)
                reference={(float)msg->x, reference.y, (float)msg->y};
            else
                if(msg->camera==1)
                    reference={reference.x,(float) msg->x, (float)msg->y};
            //The last number of a reference is always a negative number.
            //In this way the receiver knows when the array ends
            mutex.unlock();
            batmag_soft::Reference msg;
            msg.ref[0]=reference.x;
            msg.ref[1]=reference.y;
            msg.ref[2]=reference.z;
            msg.ref[3]=-1;
            mutex.lock();
            referencePub.publish(msg);
            mutex.unlock();
            //Publish a message about the updated reference
            std::string str="Reference set to x: "+ std::to_string(reference.x)+" y: "+std::to_string(reference.y)+" z: "+std::to_string(reference.z);
            myTextReq.request.text=str;
            if(!writeReq.call(myTextReq))
                ROS_ERROR_STREAM("There was an error contacting the gui");
        }
}
