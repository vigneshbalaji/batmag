
/*
 * Author: Stefano Scheggi
 * @2016
 */

#include "trackerHydrogelUS.h"

TrackerHydrogelUS::TrackerHydrogelUS() : mProcessStarted(false), mFrameCounter(0)
{
    // used to calculate the computation time
    nowTime = boost::posix_time::microsec_clock::local_time();
	prevTime = nowTime;

#if USE_MULTITHREADING
	mActualFrame = nullptr;
	mProcessThread = nullptr; 
	mVideoBuffer = nullptr;
#endif
}

TrackerHydrogelUS::~TrackerHydrogelUS()
{
#if USE_MULTITHREADING
	if (mProcessThread != nullptr)
	{
		// how to stop and destroy the thread
		mProcessThread->join();
		cout << "mProcessThread->join()...ok\n";
		delete mProcessThread;
		mProcessThread = nullptr;
		cout << "mProcessThread deleted...ok" << endl;
	}
#endif
}

#if USE_MULTITHREADING
void TrackerHydrogelUS::startThread(void)
{
	// destroy previous thread and create a new one
	if (mProcessThread != nullptr)
	{
		//cout << "mProcessThread deleted...ok" << endl;
		delete mProcessThread;
		mProcessThread = nullptr;
	}

	// create a new one
	//cout << "mProcessThread created...ok" << endl;
	mIsThreadRunning = true;
	mProcessThread = new boost::thread(boost::bind(&TrackerHydrogelUS::processFrameThread, this));
}

void TrackerHydrogelUS::processFrameThread(void)
{
	Mat tmpOutFrame, tmpFrameIn;
	Point4f tmpCenter;
    Point2f tmpCenterDraw;

	mComputationTime.clear();

	nowTime = boost::posix_time::microsec_clock::local_time();
	prevTime = nowTime;

	while (mIsThreadRunning)
	{
		nowTime = boost::posix_time::microsec_clock::local_time();
		diffTime = nowTime - prevTime;

		if (diffTime.total_milliseconds() >= mParamsThread->mSamplingTime) // milliseconds
		{
			// store the computation time for debugging
            if (mParamsThread->mSaveSamplingTime)
            {
                mSamplingTime.push_back(static_cast<double>(diffTime.total_milliseconds()));
            }
            
            //// just for testing, it just visualize the input, uncomment this line and comment the processFrame
            //mProcessMutex.lock();
            //*mOutFrameThread = *(&mVideoBuffer->at(*mActualFrame - mInitialFrame));
            //mProcessMutex.unlock();

			// make sure that we have the requested video
			// since we are loading frames while processing them, it may happen that we 
			// try to process a video frame which is not still loaded, if the tracking sampling time is small
			// in this case just restart teh processing from the first frame
			// if data is saved is better to load teh whole video before and then process
			if ((*mActualFrame - mInitialFrame) >= mVideoBuffer->size())
			{
				*mActualFrame = *(&mInitialFrame);
                // here is possible to add delays which simulate the grabbing time
			}

			// get the video frame from the buffer
            tmpFrameIn = mVideoBuffer->at(*mActualFrame - mInitialFrame).clone();

            // process the frame
			processFrame(tmpFrameIn, tmpOutFrame, *mParamsThread, tmpCenter);
            tmpCenterDraw.x = tmpCenter.x;
            tmpCenterDraw.y = tmpCenter.y;

            // update the shared variables
            mProcessMutex.lock();
			*mOutFrameThread = *(&tmpOutFrame);
            *mOutCenterThread = *(&tmpCenterDraw);
            mProcessMutex.unlock();

			// update the loop time
			prevTime = nowTime;
		}
	}
}
#endif

bool TrackerHydrogelUS::processFrame(const Mat& inFrame, Mat& outFrame, Point4f& outCenter)
{
    // computation time
    prevTime = boost::posix_time::microsec_clock::local_time();
    
    Mat chR, roiFrame, chRThresholdFrame, detectedFrame, croppedFrame;
    vector<Mat> channels(3);
    vector<vector<Point> > contour, contours_poly;
    Rect processROI;
    float radius = 0;
    Point4f center, centerKalman, centerPredicted;
    bool isDetected = false;
    outFrame = inFrame;
    
    // crop the original frame, just for display purpose
    // display the important part only
    croppedFrame = inFrame(mTrackerParameters.mImageROI);
    
    // manage the ROI
    if (!mProcessStarted)
    {
        processROI = mTrackerParameters.mInitialROI;
        
        if (mTrackerParameters.mUseKalmanFilter)
        {
            Point4f tmpCenter;
            
            tmpCenter.x = static_cast<float>(mTrackerParameters.mInitialROI.x + mTrackerParameters.mInitialROI.width*0.5);
            tmpCenter.y = static_cast<float>(mTrackerParameters.mInitialROI.y + mTrackerParameters.mInitialROI.height*0.5);
            tmpCenter.z = 0;
            tmpCenter.w = 0;
                                             
            // init kalman filter
            KalmanInit(tmpCenter, mTrackerParameters.mSamplingTime);
        }
    }
    else
    {
        processROI = mActualProcessROI;
    }
    
    // get the ROI and process it
    roiFrame = croppedFrame(processROI);
    
    // split the RGB channels, "channels"
    split(roiFrame, channels);
    
    // binary thresholding on Red channel (channels[2] is the red channel)
    threshold(channels[2], chRThresholdFrame, mTrackerParameters.mThreshold, 255, 0);
    
    // extract blobs
    int num_good_blobs = removeSmallBlobs(mTrackerParameters.mBlobMinArea, mTrackerParameters.mBlobMinDistance, chRThresholdFrame);
    
    if (num_good_blobs <= 0)
    {
        // use the previous center and hope to detect it again
        cout << "Unable to detect the agent " << endl;
        
        if (mTrackerParameters.mUseKalmanFilter)
        {
            centerKalman.x = mTrackerParameters.mPreviousCenter.x;
            centerKalman.y = mTrackerParameters.mPreviousCenter.y;
            
            outCenter = centerKalman;
        }
        else
        {
            center.x = mTrackerParameters.mPreviousCenter.x;
            center.y = mTrackerParameters.mPreviousCenter.y;
            
            outCenter = center;
        }
        
        isDetected = false;
    }
    else
    {
        // find the center of the blobs
        Mat frameContour = chRThresholdFrame.clone();
        findContours(frameContour, contour, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
        contours_poly.clear();
        contours_poly.resize(contour.size());
        
        for (unsigned i = 0; i < contour.size(); i++)
        {
            // new code, merge big contours
            copy(contour[i].begin(), contour[i].end(), back_inserter(contours_poly[0]));
        }
        
        // merge big contours and find the center
        Point2f tmpCircleCenter;
        minEnclosingCircle((Mat)contours_poly[0], tmpCircleCenter, radius);
        center.x = tmpCircleCenter.x;
        center.y = tmpCircleCenter.y;
        
        isDetected = true;
    }
    
    if (mTrackerParameters.mUseKalmanFilter)
    {
        // transform the center from ROI to big image
        center.x += processROI.x;
        center.y += processROI.y;
        // apply Kalman filter
        KalmanProcess(center, centerKalman, centerPredicted);
        // update ROI based on kalman center
        // calculate the center from ROI to the big image
        updateROI(Rect(0, 0, processROI.width, processROI.height),
                  mTrackerParameters.mImageROI, Point2f(centerKalman.x, centerKalman.y), mActualProcessROI);
        mTrackerParameters.mPreviousCenter.x = centerKalman.x;
        mTrackerParameters.mPreviousCenter.y = centerKalman.y;
        outCenter = centerKalman;
    }
    else
    {
        // update ROI based on actual center
        // calculate the center from ROI to the big image
        updateROI(processROI, mTrackerParameters.mImageROI, Point2f(center.x, center.y), mActualProcessROI);
        // transform the center from ROI to big image
        center.x += processROI.x;
        center.y += processROI.y;
        // update previous center
        mTrackerParameters.mPreviousCenter.x = center.y;
        mTrackerParameters.mPreviousCenter.x = center.y;
        outCenter = center;
    }
    
#if FAST_DRAWING
    outFrame = inFrame(mTrackerParameters.mImageROI).clone();
#else
    // this part can be skipped since it jsut draw the detection, contours and
    // create output image generate detection image from grayscale to RGB, it has the dimension of the ROI
    Mat frameinS = Mat(croppedFrame.rows, croppedFrame.cols, croppedFrame.type(), 0.0);
    Mat zeroChannel = Mat(chRThresholdFrame.rows, chRThresholdFrame.cols, chRThresholdFrame.type(), 0.0);
    Mat detected3ch[] = {zeroChannel, chRThresholdFrame, zeroChannel};
    merge(detected3ch, 3, detectedFrame);
    
    // draw contours in RGB
    if (params.mDrawContours && contours_poly.size() > 0)
        drawContours(detectedFrame, contours_poly, 0, Scalar(0, 0, 255), 1, 8, CV_RETR_EXTERNAL, 0, Point());
    
    // apply the ROI image to the big image
    Mat(detectedFrame).copyTo(Mat(frameinS, processROI));
    // merge frames with blending
    addWeighted(croppedFrame, 1, frameinS, 0.7, 0.0, outFrame);
#endif
    
    // draw center
    if (mTrackerParameters.mDrawCenter)
    {
        if (mTrackerParameters.mUseKalmanFilter)
            circle(outFrame, Point2f(centerKalman.x, centerKalman.y), 3, Scalar(0,255,0), -1);
        else
            circle(outFrame, Point2f(center.x, center.y), 3, Scalar(0,0,255), -1);
    }
    
    // draw ROI
    if (mTrackerParameters.mDrawROI)
    {
        if (mTrackerParameters.mUseKalmanFilter)
        {
            rectangle(outFrame, Point(processROI.x, processROI.y),
                      Point(processROI.x+processROI.width, processROI.y+processROI.height),
                      Scalar(0,255,0));
        }
        else
        {
            rectangle(outFrame, Point(processROI.x, processROI.y),
                      Point(processROI.x+processROI.width, processROI.y+processROI.height),
                      Scalar(0,0,255));
        }
    }
    
    // save centers in a vector
    if (mTrackerParameters.mSaveCenter)
    {
        // save centers wrt the intial big image
        // at the beginning the initial image iscropped to remove US info and useless stuff
        // then the cropped image is cropped again to get the ROI
        Point4f tmpCenter = outCenter;
        tmpCenter.x += mTrackerParameters.mImageROI.x;
        tmpCenter.y += mTrackerParameters.mImageROI.y;
        mCenters.push_back(tmpCenter);
        //mCenters[mFrameCounter] = outCenter + Point2f(static_cast<float>(params.mImageROI.x), static_cast<float>(params.mImageROI.y));
    }
    
    // computation time
    nowTime = boost::posix_time::microsec_clock::local_time();
    diffTime = nowTime - prevTime;
    
#if SHOW_COMPUTATION_TIME
    cout << "processFrame: " << diffTime.total_milliseconds() << " ms." << endl;
#endif
    
    // save computation time for each frame in a vector
    if (mTrackerParameters.mSaveComputationTime)
    {
        //cout << "Frame: " << mFrameCounter << " microsec. " << diffTime.total_microseconds() << " num blobs: " << num_good_blobs << endl;
        mComputationTime.push_back(static_cast<double>(diffTime.total_milliseconds()));
        //mComputationTime[mFrameCounter] = diffTime.total_microseconds();
    }
    
    // increase frame counter for buffered vectors mCenters and mComputationTime
    mFrameCounter++;
    
    // boolean check variable
    mProcessStarted = true;
    
    return isDetected;
}

int TrackerHydrogelUS::removeSmallBlobs(const double& minArea, const double& maxDist, Mat& frameIn)
{
    int num_good_blobs = 0;
    vector<vector<Point> > contours;
    
    // remove blob not connected and distant from the main one
    vector<int> blobindex;
    unsigned maxIndex = -1;
    double maxarea = std::numeric_limits<double>::min();
    Point2f maxcenter;
    // centers
    vector<Point2f> mc;
    double dist2 = maxDist * maxDist;
    
    //// Only accept CV_8UC1
    //if (frameIn.channels() != 1 || frameIn.type() != CV_8U)
    //    return 0;
    
    // Find all contours
    findContours(frameIn.clone(), contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
    
    // remove small blobs
    for (unsigned i = 0; i < contours.size(); i++)
    {
        // Calculate contour area
        double area = contourArea(contours[i]);
        
        // Remove small objects by drawing the contour with black color
        if (area <= minArea)
        {
            // remove contour
            drawContours(frameIn, contours, i, CV_RGB(255, 0, 0), -1);
        }
        else
        {
            // compute cluster center
            Moments mo = cv::moments(contours[i], true);
			mc.push_back(Point2f(static_cast<float>(mo.m10 / mo.m00), static_cast<float>(mo.m01 / mo.m00)));
            
            // update biggest blob
            if (area > maxarea)
            {
                maxarea = area;
                maxIndex = i;
                maxcenter.x = mc.back().x;
                maxcenter.y = mc.back().y;
            }
            
            // save indices of big blobs
            blobindex.push_back(i);
            
            num_good_blobs++;
        }
    }
    
    // remove blobs which are distant from the main one
    if (blobindex.size() > 1)
    {
        // for all the other blobs check their distance from the main one
        for (unsigned i = 0; i < blobindex.size(); i++)
        {
            if (blobindex[i] != maxIndex)
            {
                double dx = maxcenter.x - mc[i].x;
                double dy = maxcenter.y - mc[i].y;
                
                if (dx*dx + dy*dy > dist2)
                {
                    // remove this contour because it is too far
                    drawContours(frameIn, contours, blobindex[i], CV_RGB(255, 0, 0), -1);
                    num_good_blobs--;
                }
            }
        }
    }
    
    return num_good_blobs;
}

void TrackerHydrogelUS::updateROI(const Rect& oldROI, const Rect& imageROI, const Point2f& center, Rect& newROI)
{
    newROI = oldROI;
	newROI.x = oldROI.x + static_cast<int>(center.x - oldROI.width*0.5f);
	newROI.y = oldROI.y + static_cast<int>(center.y - oldROI.height*0.5f);
    
    // check if the ROI is inside the image or not, if not update properly
    if (newROI.x < 0)
    {
        newROI.x = 0;
    }
    if (newROI.x + newROI.width > imageROI.width)
    {
        int diff = newROI.x + newROI.width - imageROI.width;
        newROI.x -= diff;
    }
    
    if (newROI.y < 0)
    {
        newROI.y = 0;
    }
    if (newROI.y + newROI.height > imageROI.height)
    {
        int diff = newROI.y + newROI.height - imageROI.height;
        newROI.x -= diff;
    }
}

void TrackerHydrogelUS::KalmanInit(const Point4f& center, const double& timeIntervalSec)
{
    // continuous system: xdot = A*x
    // x = [x y vx vy]
    // A = [0 0 1 0
    //      0 0 0 1
    //      0 0 0 0
    //      0 0 0 0]
    //
    // Adisc = [ 1, 0, dt,  0]
    //         [ 0, 1,  0, dt]
    //         [ 0, 0,  1,  0]
    //         [ 0, 0,  0,  1]
    
    // dynamParams, measureParams, controlParams, type
    // initialize matrices with their dimension
    mKF.init(4, 2, 0);
    
    // then set all the matrices
    mKF.measurementMatrix = (Mat_<float>(2, 4) <<
                            1, 0, 0, 0,
                            0, 1, 0, 0);
    
    mKF.transitionMatrix = (Mat_<float>(4, 4) <<
                           1, 0, timeIntervalSec, 0,
                           0, 1, 0, timeIntervalSec,
                           0, 0, 1, 0,
                           0, 0, 0, 1);

    mKF.statePre.at<float>(0) = center.x;
    mKF.statePre.at<float>(1) = center.y;
    mKF.statePre.at<float>(2) = center.z;
    mKF.statePre.at<float>(3) = center.w;
    
    mKF.statePost.at<float>(0) = center.x;
    mKF.statePost.at<float>(1) = center.y;
    mKF.statePost.at<float>(2) = center.z;
    mKF.statePost.at<float>(3) = center.w;
    
    // [x, dx/dt] then a common process noise model (called almost constant velocity)
    // is Q=[T^3/3, T^2/2; T^2/2, T]q
    float qx = 1, qy = 1;
    setIdentity(mKF.processNoiseCov, Scalar::all(0));
    mKF.processNoiseCov.at<float>(0,0) = (float)(qx*pow(timeIntervalSec,3)/3);
	mKF.processNoiseCov.at<float>(0, 2) = (float)(qx*pow(timeIntervalSec, 2) / 2);
	mKF.processNoiseCov.at<float>(2, 0) = (float)(qx*pow(timeIntervalSec, 2) / 2);
	mKF.processNoiseCov.at<float>(2, 2) = (float)(qx*timeIntervalSec);
    
	mKF.processNoiseCov.at<float>(1, 1) = (float)(qy*pow(timeIntervalSec, 3) / 3);
	mKF.processNoiseCov.at<float>(1, 3) = (float)(qy*pow(timeIntervalSec, 2) / 2);
	mKF.processNoiseCov.at<float>(3, 1) = (float)(qy*pow(timeIntervalSec, 2) / 2);
	mKF.processNoiseCov.at<float>(3, 3) = (float)(qy*timeIntervalSec);
    
    // measurement
    setIdentity(mKF.measurementNoiseCov, Scalar::all(3));
    
    // transitionMatrix*errorCovPost = A*P(k)
    setIdentity(mKF.errorCovPost, Scalar::all(0.5));
}

void TrackerHydrogelUS::KalmanProcess(const Point4f& center, Point4f& centerKalman, Point4f& centerKalmanPredicted)
{
    // https://github.com/Itseez/opencv/blob/master/modules/video/src/kalman.cpp
    Mat_<float> measurement(2, 1);
    measurement(0) = center.x;
    measurement(1) = center.y;
    
    // First predict, to update the internal statePre variable
    Mat prediction = mKF.predict();

    // use this for ROI
    // x y vx vy
    centerKalmanPredicted.x = prediction.at<float>(0);
    centerKalmanPredicted.y = prediction.at<float>(1);
    centerKalmanPredicted.z = prediction.at<float>(3);
    centerKalmanPredicted.w = prediction.at<float>(3);
    
    // The "correct" phase that is going to use the predicted value and our measurement
    Mat estimated = mKF.correct(measurement);
    
    centerKalman.x = estimated.at<float>(0);
    centerKalman.y = estimated.at<float>(1);
    centerKalman.z = estimated.at<float>(2);
    centerKalman.w = estimated.at<float>(3);
}

bool TrackerHydrogelUS::saveCentersToFile(const char* filename) const
{
    cout << "saveCentersToFile...";
    if (mCenters.size() > 0)
    {
        ofstream outFile;
        outFile.open (filename);
        for (unsigned i = 0; i < mCenters.size(); i++)
        {
            outFile << mCenters[i].x << " " << mCenters[i].y << " " << mCenters[i].z << " " << mCenters[i].w << "\n";
        }
        outFile.close();
        cout << "done" << endl;
        return true;
    }
    else
    {
        cout << "mCenters is empty, enable save centers from params" << endl;
        return false;
    }
}

bool TrackerHydrogelUS::saveComputationTimeToFile(const char* filename) const
{
    if (mComputationTime.size() > 0)
    {
        ofstream outFile;
        outFile.open (filename);
        for (unsigned i = 0; i < mComputationTime.size(); i++)
        {
            outFile << mComputationTime[i] << "\n";
        }
        outFile.close();
        return true;
    }
    else
    {
        cout << "TrackerHydrogelUS::mComputationTime is empty, enable save computation time from params" << endl;
        return false;
    }
}

bool TrackerHydrogelUS::saveSamplingTimeToFile(const char* filename) const
{
    if (mSamplingTime.size() > 0)
    {
        ofstream outFile;
        outFile.open (filename);
        for (unsigned i = 0; i < mSamplingTime.size(); i++)
        {
            outFile << mSamplingTime[i] << "\n";
        }
        outFile.close();
        return true;
    }
    else
    {
        cout << "TrackerHydrogelUS::mSamplingTime is empty, enable save sampling time from params" << endl;
        return false;
    }
}
