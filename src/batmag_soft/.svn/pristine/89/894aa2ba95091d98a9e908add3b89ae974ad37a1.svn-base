cmake_minimum_required(VERSION 2.8.8)
project(batmag_soft)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages

#log4j.logger.ros.batmag_soft=INFO in /opt/ros/kinetic/share/ros/config/rosconsole.config to remove debug messages

# set the path to the library folder
#link_directories(/usr/local/lib)

#Request c++14
set(CMAKE_CXX_STANDARD 14)

 # As moc files are generated in the binary dir, tell CMake
 # to always look for includes there:
set(CMAKE_INCLUDE_CURRENT_DIR ON)

# Add the custom findpackages to the cmake path
list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/cmake/Modules")


# Tell CMake to run moc when necessary:
set(CMAKE_AUTOMOC ON)



find_package(message_generation)

find_package(catkin REQUIRED COMPONENTS
  roscpp
  rospy
  std_msgs
  message_generation
  sensor_msgs
  cv_bridge
  image_transport
  cmake_modules
  actionlib_msgs
)

# Widgets finds its own dependencies (QtGui and QtCore).
find_package(Qt5Widgets REQUIRED )

find_package(Eigen3 REQUIRED)
#cmake modules
find_package(cmake_modules REQUIRED)


## System dependencies are found with CMake's conventions
find_package(Boost REQUIRED COMPONENTS system signals)


#flycapture for
#include_directories("/usr/include/flycapture")
#find_library(FLYCAPTURE2 REQUIRED flycapture)
find_package(FlyCapture2 REQUIRED)
include_directories(${FlyCapture2_INCLUDE_DIRS})

#Mosek-Fusion64
find_package(Fusion64 REQUIRED)
include_directories(${FUSION64_INCLUDE_DIR})
#message("${FUSION64_LIBRARIES}")

#NLOpt
find_package(NLopt REQUIRED)
include_directories(${NLOPT_INCLUDE_DIR})
#message("${NLOPT_LIBRARIES}" )

#opencv for
find_package( OpenCV REQUIRED )
include_directories(${OpenCV_INCLUDE_DIRS})

#pcan for
find_package(PCANBasic REQUIRED)
include_directories(${PCANBasic_INCLUDE_DIRS})

## Uncomment this if the package has a setup.py. This macro ensures
## modules and global scripts declared therein get installed
## See http://ros.org/doc/api/catkin/html/user_guide/setup_dot_py.html
# catkin_python_setup()

################################################
## Declare ROS messages, services and actions ##
################################################

## To declare and build messages, services or actions from within this
## package, follow these steps:
## * Let MSG_DEP_SET be the set of packages whose message types you use in
##   your messages/services/actions (e.g. std_msgs, actionlib_msgs, ...).
## * In the file package.xml:
##   * add a build_depend tag for "message_generation"
##   * add a build_depend and a run_depend tag for each package in MSG_DEP_SET
##   * If MSG_DEP_SET isn't empty the following dependency has been pulled in
##     but can be declared for certainty nonetheless:
##     * add a run_depend tag for "message_runtime"
## * In this file (CMakeLists.txt):
##   * add "message_generation" and every package in MSG_DEP_SET to
##     find_package(catkin REQUIRED COMPONENTS ...)
##   * add "message_runtime" and every package in MSG_DEP_SET to
##     catkin_package(CATKIN_DEPENDS ...)
##   * uncomment the add_*_files sections below as needed
##     and list every .msg/.srv/.action file to be processed
##   * uncomment the generate_messages entry below
##   * add every package in MSG_DEP_SET to generate_messages(DEPENDENCIES ...)

## Generate messages in the 'msg' folder
 add_message_files(
  FILES
  MapOutput.msg
  CtrlOutput.msg
  StartStop.msg
  Reference.msg
  State.msg
  CameraClick.msg
  MoveCamera.msg
  GUIpreferences.msg
  RecordRaw.msg
  WriteText.msg
  USroi.msg
  pmMove.msg

#   Message1.msg
#   Message2.msg
)

## Generate services in the 'srv' folder
 add_service_files(
   FILES
    GetInitParameters.srv
    SetInitParameters.srv
#   Service1.srv
#   Service2.srv
 )

## Generate actions in the 'action' folder
# add_action_files(
#   FILES
#   Action1.action
#   Action2.action
# )

## Generate added messages and services with any dependencies listed here
 generate_messages(
   DEPENDENCIES
   std_msgs
   actionlib_msgs
   sensor_msgs
 )

################################################
## Declare ROS dynamic reconfigure parameters ##
################################################

## To declare and build dynamic reconfigure parameters within this
## package, follow these steps:
## * In the file package.xml:
##   * add a build_depend and a run_depend tag for "dynamic_reconfigure"
## * In this file (CMakeLists.txt):
##   * add "dynamic_reconfigure" to
##     find_package(catkin REQUIRED COMPONENTS ...)
##   * uncomment the "generate_dynamic_reconfigure_options" section below
##     and list every .cfg file to be processed

## Generate dynamic reconfigure parameters in the 'cfg' folder
# generate_dynamic_reconfigure_options(
#   cfg/DynReconf1.cfg
#   cfg/DynReconf2.cfg
# )

###################################
## catkin specific configuration ##
###################################
## The catkin_package macro generates cmake config files for your package
## Declare things to be passed to dependent projects
## INCLUDE_DIRS: uncomment this if you package contains header files
## LIBRARIES: libraries you create in this project that dependent projects also need
## CATKIN_DEPENDS: catkin_packages dependent projects also need
## DEPENDS: system dependencies of this project that dependent projects also need
catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES batmag_soft
   CATKIN_DEPENDS roscpp rospy std_msgs message_runtime
   DEPENDS system_lib
   message_generation
   cv_bridge
   image_transport
   cmake_modules
   actionlib_msgs
   opencv
 )

###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
# include_directories(include)
include_directories(include ${catkin_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS} ${flycapture2_INCLUDE_DIRS}
)

# The Qt5Widgets_INCLUDES also includes the include directories for
# dependencies QtCore and QtGui
include_directories(${Qt5Widgets_INCLUDES})

#This includes Eigen. Since it's headers only including the directory is sufficient
include_directories(${EIGEN3_INCLUDE_DIRS})

# We need add -DQT_WIDGETS_LIB when using QtWidgets in Qt 5.
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${Qt5Widgets_EXECUTABLE_COMPILE_FLAGS}")

# Executables fail to build with Qt 5 in the default configuration
# without -fPIE. We add that here.
#set(CMAKE_CXX_FLAGS "${Qt5Widgets_EXECUTABLE_COMPILE_FLAGS}")

## Declare a C++ library
# add_library(batmag_soft
#   src/${PROJECT_NAME}/batmag_soft.cpp
# )

## Add cmake target dependencies of the library
## as an example, code may need to be generated before libraries
## either from message generation or dynamic reconfigure
# add_dependencies(batmag_soft ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

## Declare a C++ executable
# add_executable(batmag_soft_node src/batmag_soft_node.cpp)

##----------------------------------##
##-------Creating Qt gui node-------##
##----------------------------------##

set(GUI_HEADER_FILES
  src/gui/ctrlwindow.h
  src/gui/secondscreenimage.h
  src/gui/cqtopencvviewergl.h
  src/gui/recorder.h
)
#include gui so that the system can find cqtopencvviewergl.cpp/h
include_directories(src/gui)

set(GUI_SOURCE_FILES
  src/gui/ctrlwindow.cpp
  src/gui/secondscreenimage.cpp
  src/gui/main.cpp
  src/gui/cqtopencvviewergl.cpp
  src/gui/recorder.cpp
  ${MOC_FILES}
)

set(GUI_FORM_FILES
  src/gui/ctrlwindow.ui
  src/gui/secondscreenimage.ui
)

# Generate headers from ui files
qt5_wrap_ui(QT_MOC_GUI ${GUI_FORM_FILES})
# Generate moc files from headers
# qt5_generate_moc(QT_MOC_HPP ${GUI_HEADER_FILES})

add_executable(gui ${GUI_SOURCE_FILES} ${QT_MOC_GUI} ${GUI_HEADER_FILES})#${QT_MOC_HPP}  ${QT_RESOURCES_CPP}

add_dependencies(gui ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

qt5_use_modules(gui Widgets)
# The Qt5Widgets_LIBRARIES variable also includes QtGui and QtCore
 target_link_libraries(gui Qt5::Widgets
   ${catkin_LIBRARIES}
 )

##----------------------------------##
##----Creating the control node-----##
##----------------------------------##

set(CONTROL_HEADER_FILES
  src/typeHeader.h
  src/coilparameters.h
  src/control/pid3d.h
  src/control/controller.h
  src/control/controlinit.h
  src/control/observer.h
  src/control/fourthorderfilter.h

)

set(CONTROL_SOURCE_FILES
  src/control/pid3d.cpp
  src/control/controller.cpp
  src/control/controlinit.cpp
  src/control/main.cpp
  src/typeHeader.cpp
  src/control/observer.cpp
  src/control/fourthorderfilter.cpp
)


add_executable(controlnode  ${CONTROL_HEADER_FILES} ${CONTROL_SOURCE_FILES})
add_dependencies(controlnode ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

 target_link_libraries(controlnode
   ${catkin_LIBRARIES} ${Eigen3_LIBRARIES}
 )

##----------------------------------##
##-Creating force-current map node--##
##----------------------------------##


set(MAP_HEADER_FILES
  src/typeHeader.h
  src/coilparameters.h
  src/forceCurrentMap/fcmap.h
  src/forceCurrentMap/linearmap.h
  src/forceCurrentMap/vectormap.h
  src/forceCurrentMap/currentoptimization.h
  src/forceCurrentMap/magneticfield.h
  src/forceCurrentMap/optimization/nloptimization.h

)

set(MAP_SOURCE_FILES
  src/typeHeader.cpp
  src/forceCurrentMap/main.cpp
  src/forceCurrentMap/fcmap.cpp
  src/forceCurrentMap/linearmap.cpp
  src/forceCurrentMap/vectormap.cpp
  src/forceCurrentMap/currentoptimization.cpp
  src/forceCurrentMap/magneticfield.cpp
  src/forceCurrentMap/optimization/nloptimization.cpp
)


add_executable(forceCurrentMap  ${MAP_HEADER_FILES} ${MAP_SOURCE_FILES})
add_dependencies(forceCurrentMap ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

 target_link_libraries(forceCurrentMap
   ${catkin_LIBRARIES} ${Eigen3_LIBRARIES} ${NLOPT_LIBRARIES} ${FUSION64_LIBRARIES}
)


## Add cmake target dependencies of the executable
## same as for the library above
# add_dependencies(batmag_soft_node ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

## Specify libraries to link a library or executable target against
# target_link_libraries(batmag_soft_node
#   ${catkin_LIBRARIES}
# )


##-------------------------------------##
##-Creating CAN-bus communication node-##
##-------------------------------------##

set(CAN_HEADER_FILES
  src/canInterface/canadapter.h
  src/canInterface/coilelmo.h
  src/canInterface/allelmos.h
  src/canInterface/messagebuffer.h
  src/canInterface/pmmotor.h
  src/canInterface/camerastage.h

)

set(CAN_SOURCE_FILES
  src/canInterface/main.cpp
  src/canInterface/canadapter.cpp
  src/canInterface/coilelmo.cpp
  src/canInterface/allelmos.cpp
  src/canInterface/messagebuffer.cpp
  src/canInterface/pmmotor.cpp
  src/canInterface/camerastage.cpp
)


add_executable(canInterface  ${CAN_HEADER_FILES} ${CAN_SOURCE_FILES})
add_dependencies(canInterface ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

 target_link_libraries(canInterface
   ${catkin_LIBRARIES} ${PCANBasic_LIBRARIES}
)
##----------------------------------##
##-Creating Point Grey cameras node-##
##----------------------------------##

set(PTGREY_HEADER_FILES
  src/ptGreyCameras/camera_param.h
)

set(PTGREY_SOURCE_FILES
  src/ptGreyCameras/main.cpp
  src/ptGreyCameras/camera_param.cpp
)


add_executable(ptGreyCameras  ${PTGREY_HEADER_FILES} ${PTGREY_SOURCE_FILES})
add_dependencies(ptGreyCameras ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
 target_link_libraries(ptGreyCameras
   ${catkin_LIBRARIES} ${FlyCapture2_LIBRARIES} ${OpenCV_LIBRARIES}
)



##----------------------------------##
##-----Creating 3D tracker node-----##
##----------------------------------##

set(TRACKER_HEADER_FILES
  src/tracker/configuration.h
  src/tracker/imageprocessing.h
  src/tracker/Tracker.h
  src/tracker/ColorTracker/TrackerColor.h
  src/tracker/ColorTracker/TrackerColorGeneral.h
  src/tracker/ColorTracker/TrackerColorMetalGripper.h
  src/tracker/ColorTracker/TrackerColorUS.h
  src/tracker/Triangulation/Triangulation.h

  src/typeHeader.h


#  src/tracker/Utility/UtilityComputerVisionFunction.h
#  src/tracker/trackerHydrogelUS.h
#  src/tracker/trackerMetallicCamera.h
#  src/tracker/trackerMicrojetsCamera.h
#  src/tracker/trackerMicroparticlesCamera.h
#  src/tracker/utilityComputerVisionFunction.h
#  src/tracker/tracker.h
)

set(TRACKER_SOURCE_FILES
  src/tracker/main.cpp
  src/tracker/imageprocessing.cpp
  src/tracker/Tracker.cpp
  src/tracker/utilityComputerVisionFunction.cpp
  src/tracker/ColorTracker/TrackerColor.cpp
  src/tracker/ColorTracker/TrackerColorGeneral.cpp
  src/tracker/ColorTracker/TrackerColorMetalGripper.cpp
  src/tracker/ColorTracker/TrackerColorUS.cpp
  src/tracker/Triangulation/Triangulation.cpp

  src/typeHeader.cpp
#  src/tracker/Utility/UtilityComputerVisionFunction.cpp
#  src/tracker/tracker.cpp
#  src/tracker/trackerHydrogelUS.cpp
#  src/tracker/trackerMetallicCamera.cpp
#  src/tracker/trackerMicroparticlesCamera.cpp
#  src/tracker/trackerMicrojetsCamera.cpp
)

qt5_generate_moc(TRACKER_QT_MOC_HPP ${TRACKER_HEADER_FILES})

add_executable(tracker  ${TRACKER_QT_MOC_HPP} ${TRACKER_SOURCE_FILES} ${QT_RESOURCES_CPP})
add_dependencies(tracker ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
 target_link_libraries(tracker Qt5::Widgets
   ${catkin_LIBRARIES} ${OpenCV_LIBRARIES}
)


##----------------------------------##
##-Creating Ultrasound Imaging node-##
##----------------------------------##

set(US_HEADER_FILES

)

set(US_SOURCE_FILES
  src/ultrasound/main.cpp
)


add_executable(ultrasound  ${US_HEADER_FILES} ${US_SOURCE_FILES})
add_dependencies(ultrasound ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
 target_link_libraries(ultrasound
   ${catkin_LIBRARIES} ${OpenCV_LIBRARIES}
)


#############
## Install ##
#############

# all install targets should use catkin DESTINATION variables
# See http://ros.org/doc/api/catkin/html/adv_user_guide/variables.html

## Mark executable scripts (Python etc.) for installation
## in contrast to setup.py, you can choose the destination
# install(PROGRAMS
#   scripts/my_python_script
#   DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
# )

## Mark executables and/or libraries for installation
# install(TARGETS batmag_soft batmag_soft_node
#   ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
#   LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
#   RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
# )

## Mark cpp header files for installation
# install(DIRECTORY include/${PROJECT_NAME}/
#   DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
#   FILES_MATCHING PATTERN "*.h"
#   PATTERN ".svn" EXCLUDE
# )

## Mark other files for installation (e.g. launch and bag files, etc.)
# install(FILES
#   # myfile1
#   # myfile2
#   DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
# )

#############
## Testing ##
#############

## Add gtest based cpp test target and link libraries
# catkin_add_gtest(${PROJECT_NAME}-test test/test_batmag_soft.cpp)
# if(TARGET ${PROJECT_NAME}-test)
#   target_link_libraries(${PROJECT_NAME}-test ${PROJECT_NAME})
# endif()

## Add folders to be run by python nosetests
# catkin_add_nosetests(test)
