#pragma once

#include "tracker.h"
#include "../typeHeader.h"
#include <mutex>

//ROS
#include "ros/duration.h"
#include "ros/spinner.h"
#include "ros/ros.h"

//To bind callbacks and parameters
#include <boost/function.hpp>
#include <boost/bind.hpp>
#include <boost/signals2.hpp>

//For Image massages conversion
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>


#include "batmag_soft/Reference.h"
#include "batmag_soft/CameraClick.h"
#include "batmag_soft/State.h"
#include "batmag_soft/WriteText.h"

//Tracker kinds
#include"trackerHydrogelUS.h"
#include "trackerMetallicCamera.h"
#include "trackerMicrojetsCamera.h"
#include "trackerMicroparticlesCamera.h"

class Tracker3D
{
public:
    Tracker3D(ros::NodeHandle,agentType);
    virtual ~Tracker3D();

    void get3Dpose(Point3f &pos, Point3f &orient);

    cv::Mat cameraImage[2], trackerOutputImage[2], processingImage[2];


protected:
    void on_newImage(int);



    Point3f position ={-1,-1,-1}, orientation={-1,-1,-1}, reference={-1,-1,-1};
    Point2f camRef[2];

    std::shared_ptr<Tracker> trackerCamera[2];

    std::mutex mutex;

    //ros handle
    ros::NodeHandle n;
    ros::AsyncSpinner myAsyncSpinner;

    ros::Subscriber cameraClickSub;
    ros::Publisher statePub, referencePub;
    batmag_soft::WriteText myTextReq;
    ros::ServiceClient writeReq;

//    Messages

    batmag_soft::State mState;

    //Image transport parameters
    image_transport::ImageTransport it;
    image_transport::Subscriber imageCamSub[2];
    image_transport::Publisher image_pub_cam0;
    image_transport::Publisher image_pub_cam1;


    //Callbacks

    void clickCb(const batmag_soft::CameraClickConstPtr &msg);
    void imageCb0(const sensor_msgs::ImageConstPtr& msg);
    void imageCb1(const sensor_msgs::ImageConstPtr& msg);
    void imageMessageCallback(const sensor_msgs::ImageConstPtr &msg, int cameraNum);

//    Says if the z coordinate is taken from camera 0 or camera 1
    bool camera0Priority= true;

    //Stores recorded position for cameras
    Point2f camPosition[2];


    boost::signals2::signal<void (int nc)> newImageSig;
    std::shared_ptr<Tracker> selectTracker(agentType agent);
};
