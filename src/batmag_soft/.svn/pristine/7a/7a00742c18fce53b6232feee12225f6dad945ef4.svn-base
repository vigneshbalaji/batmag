#pragma once

#include <stdio.h>
#include <unistd.h>
#include <asm/types.h>


#include <sys/time.h>
#include <byteswap.h>
#include <PCANBasic.h>
#include <string>

//For sleep commands
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread/thread.hpp>

#include <mutex>

#include"ros/ros.h"
#include "messagebuffer.h"

const int COB_NMT		= 0<<7;
const int COB_TPDO2		= 5<<7;
const int COB_RPDO2		= 6<<7;
const int MASK_ID		= 0x7f;
const int MASK_COB		= 0x780;
const int NMT_CMD_RESET	= 0x81;
const int NMT_CMD_START	= 0x1;
const int NMT_CMD_STOP	= 0x2;

const int AFTER_MSG_DELAY =500; //Milliseconds to wait after the can message has beeen sent to allow its propagation (theoretical limit 0.2ms)

class CANadapter
{
public:
    CANadapter();
    ~CANadapter();

    bool canOk();

    bool closeChannel();

    bool openChannel();

    DWORD sendInt(int id, unsigned char * cmd, int data, int dataLength=8);
    DWORD sendInt(int id, char * cmd, int data, int dataLength=8);
    DWORD sendFloat(int id, unsigned char * msg, float data, int dataLength=8);
    

    /**
     * Get an actual value of specific parameter
     *
     * Request the value of a specific parameter of the Elmo motor controller.
     *
     * @author		G.J. Vrooijink (Guus)
     * @param		*param		The requested parameter in Elmo motor controller.
     * @param		index		Index number of parameter in case of a array, by default 0.
     */
    void get(int id, const char *param,unsigned int index=0);
    /**
     * Send a command to the Elmo motor controller
     *
     * This sends a specific command to the Elmo motor controller.
     *
     * @author		G.J. Vrooijink (Guus)
     * @param		*param		The command parameter to send to the Elmo motor controller.
     */
    void cmd(int id, const char *param);
    /**
     * Read received messages from Elmo motor controller
     *
     * This reads a specific requested message from received message buffer, which contains integer data.
     *
     * @author		G.J. Vrooijink (Guus)
     * @param		*param		The requested parameter of the Elmo motor controller.
     * @param		*pValue		The actual value of the requested parameter of the Elmo motor controller.
     * @param		index		Index number of parameter in case of a array, by default 0.
     */
    int read(int id, const char *param,int *pValue, unsigned int index=0);
    /**
     * Read received messages from Elmo motor controller
     *
     * This reads a specific requested message from received message buffer, which contains float data.
     *
     * @author		G.J. Vrooijink (Guus)
     * @param		*param		The requested parameter of the Elmo motor controller.
     * @param		*pValue		The actual value of the requested parameter of the Elmo motor controller.
     * @param		index		Index number of parameter in case of a array, by default 0.
     */
    int read(int id, const char *param,float *pValue, unsigned int index=0);

protected:
    int fd;
    unsigned char p_XMT_data[8];
    int frc;//Function Return Code

    TPCANMsg msg;

    MessageBuffer msgBuf;
    std::mutex mMutex;

    void printMsg(TPCANMsg msg);
};
